package fr.ifremer.dali.ui.swing.util.map;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.action.SafeAction;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.FeatureType;
import org.opengis.feature.type.PropertyDescriptor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author peck7 on 30/05/2017.
 */
public class CartoTest {

    private JMapFrame frame;
    private MapContent map;
    private static final Log log = LogFactory.getLog(CartoTest.class);

    public static void main(String[] args) throws Exception {

        // customize frame
        CartoTest test = new CartoTest();
        test.displayFrame();
    }

    private void displayFrame() {

        // Set up a MapContent with the two layers
        map = new MapContent();
        map.setTitle("ImageLab");


        // Create a JMapFrame with a menu to choose the display style for the
        frame = new JMapFrame(map);
        frame.setSize(800, 600);
        frame.enableStatusBar(true);
        frame.enableToolBar(true);
        frame.enableLayerTable(true);

        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        JMenu menu = new JMenu("Layer");
        menuBar.add(menu);

        menu.add( new SafeAction("Add layer") {
            public void action(ActionEvent e) throws Throwable {
                Layer layer = addLayer();
                if (layer != null) {
                    map.addLayer(layer);
                    frame.repaint();
                }
            }
        });

        // Finally display the map frame.
        // When it is closed the app will exit.
        frame.setVisible(true);

    }

    private Layer addLayer() throws IOException {

        // display a data store file chooser dialog for shapefiles
        File file = JFileDataStoreChooser.showOpenFile("shp", null);
        if (file == null) {
            return null;
        }

        FileDataStore store = FileDataStoreFinder.getDataStore(file);
        SimpleFeatureSource featureSource = store.getFeatureSource();

        // CachingFeatureSource is deprecated as experimental (not yet production ready)
//        CachingFeatureSource cache = new CachingFeatureSource(featureSource);

//        Style style = SLD.createSimpleStyle(featureSource.getSchema());
//        Style style = SLD.createPolygonStyle(Color.BLACK, randomColor(), 1f, "name", null);
        Style style = createStyle(featureSource.getSchema());
        return new FeatureLayer(featureSource /*cache*/, style);
    }

    private Style createStyle(FeatureType schema) {
        if (schema instanceof SimpleFeatureType) {

            log.debug("schema descriptors :");
            List<String> attr = new ArrayList<>();
            for (PropertyDescriptor descriptor : schema.getDescriptors()) {
                attr.add(descriptor.getName().toString());
            }
            log.debug(attr);

            if (attr.contains("name")) {
                return SLD.createPolygonStyle(Color.BLACK, randomColor(), 1f, "name", null);
            } else if (attr.contains("NAME")) {
                return SLD.createPolygonStyle(Color.BLACK, randomColor(), 1f, "NAME", null);
            } else {
                return SLD.createPolygonStyle(Color.BLACK, randomColor(), 1f);
            }
        }

        return SLD.createSimpleStyle(schema);
    }

    private Color randomColor() {
        Random random = new Random();
        return new Color(
                100 + random.nextInt(155),
                100 + random.nextInt(155),
                100 + random.nextInt(155)
        );
    }
}
