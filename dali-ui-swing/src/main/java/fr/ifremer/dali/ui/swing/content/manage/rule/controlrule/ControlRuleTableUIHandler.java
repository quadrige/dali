package fr.ifremer.dali.ui.swing.content.manage.rule.controlrule;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.control.ControlFeatureDTO;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.dali.dto.enums.ControlElementValues;
import fr.ifremer.dali.dto.enums.ControlFeatureMeasurementValues;
import fr.ifremer.dali.dto.enums.ControlFunctionValues;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.qualitativevalue.SelectQualitativeValueUI;
import fr.ifremer.dali.ui.swing.content.manage.rule.RulesUI;
import fr.ifremer.dali.ui.swing.content.manage.rule.controlrule.precondition.RulePreconditionUI;
import fr.ifremer.dali.ui.swing.content.manage.rule.controlrule.precondition.RulePreconditionUIModel;
import fr.ifremer.dali.ui.swing.content.manage.rule.rulelist.RuleListRowModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterTypeId;
import fr.ifremer.quadrige3.core.dao.technical.decorator.DecoratorComparator;
import fr.ifremer.quadrige3.ui.swing.table.AbstractRowUIModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ButtonCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.FilterableComboBoxCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.PredicatedCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.renderer.ButtonCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.renderer.MultipleCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.renderer.PredicatedCellRenderer;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.springframework.dao.DataRetrievalFailureException;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Dimension;
import java.util.*;
import java.util.function.Predicate;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des regles
 */
public class ControlRuleTableUIHandler extends
        AbstractDaliTableUIHandler<ControlRuleRowModel, ControlRuleTableUIModel, ControlRuleTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ControlRuleTableUIHandler.class);

    private FilterableComboBoxCellEditor<ControlFeatureDTO> controlFeatureEditor;

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ControlRuleTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlRuleTableUIModel model = new ControlRuleTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ControlRuleTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Desactivation des boutons
        getUI().getRemoveControlRuleButton().setEnabled(false);

        // Initialisation du tableau
        initTable();

        // Initialisation des listeners
        initListeners();

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // Code
        TableColumnExt codeCol = addColumn(ControlRuleTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // Fonction
        TableColumnExt functionCol = addFilterableComboDataColumnToModel(
                ControlRuleTableModel.FUNCTION,
                getContext().getSystemService().getFunctionsControlSystem(),
                false);
        functionCol.setSortable(true);
        functionCol.setPreferredWidth(200);

        // Element Controle
        TableColumnExt controlElementCol = addFilterableComboDataColumnToModel(
                ControlRuleTableModel.CONTROL_ELEMENT,
                getContext().getSystemService().getControlElements(),
                false);
        controlElementCol.setSortable(true);
        controlElementCol.setPreferredWidth(200);

        // Caracteristique Controle
        controlFeatureEditor = newFilterableComboBoxCellEditor(new ArrayList<>(), ControlFeatureDTO.class, false);
        TableColumnExt controleFeatureCol = addColumn(
                controlFeatureEditor,
                newTableCellRender(ControlFeatureDTO.class),
                ControlRuleTableModel.CONTROL_FEATURE);
        controleFeatureCol.setSortable(true);
        controleFeatureCol.setPreferredWidth(200);

        // Actif
        final TableColumnExt activeCol = addBooleanColumnToModel(ControlRuleTableModel.ACTIVE, table);
        activeCol.setSortable(true);

        // Bloquante
        final TableColumnExt blockingCol = addBooleanColumnToModel(ControlRuleTableModel.BLOCKING, table);
        blockingCol.setSortable(true);

        // Min
        TableColumnExt minColumn = addColumn(ControlRuleTableModel.MIN);
        minColumn.setSortable(true);

        // Max
        TableColumnExt maxColumn = addColumn(ControlRuleTableModel.MAX);
        maxColumn.setSortable(true);

        // specific renderer for min & max columns
        Map<Class, TableCellRenderer> minMaxRenderers = Maps.newHashMap();
        minMaxRenderers.put(Double.class, newNumberCellRenderer(10));
        minMaxRenderers.put(Date.class, newDateCellRenderer(getConfig().getDateFormat()));
        MultipleCellRenderer smartCellRenderer = new MultipleCellRenderer(minMaxRenderers);
        minColumn.setCellRenderer(smartCellRenderer);
        maxColumn.setCellRenderer(smartCellRenderer);

        // specific editors for min & max columns
        Map<Predicate<ControlRuleRowModel>, TableCellEditor> minMaxEditors = Maps.newHashMap();
        minMaxEditors.put(controlRule -> ControlFunctionValues.MIN_MAX_DATE.equals(controlRule.getFunction()), newDateCellEditor(getConfig().getDateFormat()));
        minMaxEditors.put(controlRule -> ControlFunctionValues.MIN_MAX.equals(controlRule.getFunction()), newNumberCellEditor(Double.class, true, DaliUI.SIGNED_HIGH_DECIMAL_DIGITS_PATTERN));
        PredicatedCellEditor<ControlRuleRowModel> minMaxCellEditor = new PredicatedCellEditor<>(minMaxEditors);
        minColumn.setCellEditor(minMaxCellEditor);
        maxColumn.setCellEditor(minMaxCellEditor);
        // TODO maybe choose Integer or Double (with precision) depending on witch feature is controlled

        // Valeurs Autorisees
        TableColumnExt allowedValuesCol = addColumn(ControlRuleTableModel.ALLOWED_VALUES);
        allowedValuesCol.setSortable(true);

        allowedValuesCol.setCellEditor(new PredicatedCellEditor<>(buildAllowedValuesEditors()));

        // the same predicate for renderers
        allowedValuesCol.setCellRenderer(new PredicatedCellRenderer<>(buildAllowedValuesRenderers()));

        ControlRuleTableModel tableModel = new ControlRuleTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Initialisation du tableau
        initTable(table);

    }

    private Map<Predicate<ControlRuleRowModel>, TableCellEditor> buildAllowedValuesEditors() {
        Map<Predicate<ControlRuleRowModel>, TableCellEditor> allowedValuesEditors = Maps.newHashMap();

        // specific editor for allowed values if controlled attribute is a qualitative value
        allowedValuesEditors.put(ControlRuleRowModel.qualitativeValuePredicate, new ButtonCellEditor() {

            private String allowedValues;

            @Override
            public void onButtonCellAction(int row, int column) {
                int rowModelIndex = getTable().convertRowIndexToModel(row);
                ControlRuleRowModel rowModel = getTableModel().getEntry(rowModelIndex);
                allowedValues = rowModel.getAllowedValues();

                if (CollectionUtils.isNotEmpty(rowModel.getRulePmfms())) {

                    // build available qualitative values
                    Set<QualitativeValueDTO> availableList = Sets.newHashSet();
                    for (RulePmfmDTO rulePmfm : rowModel.getRulePmfms()) {
                        // the PmfmDTO list is not from referential but from RulePmfm, so we need to get the real pmfm
                        List<PmfmDTO> refPmfms = getContext().getReferentialService().searchPmfms(StatusFilter.ALL,
                                rulePmfm.getPmfm().getParameter().getCode(), // only parameter is mandatory
                                rulePmfm.getPmfm().getMatrix() != null ? rulePmfm.getPmfm().getMatrix().getId() : null,
                                rulePmfm.getPmfm().getFraction() != null ? rulePmfm.getPmfm().getFraction().getId() : null,
                                rulePmfm.getPmfm().getMethod() != null ? rulePmfm.getPmfm().getMethod().getId() : null,
                                rulePmfm.getPmfm().getUnit() != null ? rulePmfm.getPmfm().getUnit().getId() : null,
                                null,
                                null);
                        // gather all qualitative values
                        for (PmfmDTO refPmfm : refPmfms) {
                            availableList.addAll(refPmfm.getQualitativeValues());
                        }
                    }
                    // build selected
                    List<QualitativeValueDTO> selectedList = Lists.newArrayList();
                    if (StringUtils.isNotBlank(allowedValues)) {
                        Set<Integer> qvIds = DaliBeans.getIntegerSetFromString(allowedValues, getConfig().getValueSeparator());
                        selectedList.addAll(getContext().getReferentialService().getQualitativeValues(qvIds));
                    }

                    // open select qualitative values ui
                    SelectQualitativeValueUI selectQualitativeValueUI = new SelectQualitativeValueUI(getContext());
                    List<QualitativeValueDTO> orderedAvailableList = new ArrayList<>(availableList);
                    orderedAvailableList.sort(new DecoratorComparator<>(getDecorator(QualitativeValueDTO.class, null)));
                    selectQualitativeValueUI.getModel().setAvailableList(orderedAvailableList);
                    selectQualitativeValueUI.getModel().setSelectedList(selectedList);
                    selectQualitativeValueUI.getHandler().setEnabled(getRuleList().isEditable());
                    openDialog(selectQualitativeValueUI, new Dimension(640, 480));

                    if (getRuleList().isEditable() && selectQualitativeValueUI.getModel().isValid()) {
                        // if user validates, build allowed values from qualitative value ids
                        allowedValues = DaliBeans.joinIds(selectQualitativeValueUI.getModel().getSelectedList(), getConfig().getValueSeparator());
                    }
                    getTable().editingStopped(null);
                }
            }

            @Override
            public Object getCellEditorValue() {
                return allowedValues;
            }
        });

        // specific editor for allowed values if controlled attribute is a person
        allowedValuesEditors.put(ControlRuleRowModel.observersPredicate, new ButtonCellEditor() {

            private String allowedValues;

            @Override
            public void onButtonCellAction(int row, int column) {
                int rowModelIndex = getTable().convertRowIndexToModel(row);
                ControlRuleRowModel rowModel = getTableModel().getEntry(rowModelIndex);
                allowedValues = rowModel.getAllowedValues();

                SelectFilterUI userUI = new SelectFilterUI(getContext(), FilterTypeId.QUSER.getValue());
                List<PersonDTO> selectedPersons = Lists.newArrayList();
                if (StringUtils.isNotBlank(allowedValues)) {
                    Set<Integer> userIds = DaliBeans.getIntegerSetFromString(allowedValues, getConfig().getValueSeparator());
                    for (Integer userId : userIds) {
                        try {
                            selectedPersons.add(getContext().getUserService().getUser(userId));
                        } catch (DataRetrievalFailureException ignored) {
                        }
                    }
                }
                userUI.getModel().setSelectedElements(selectedPersons);
                userUI.getHandler().setEnabled(getRuleList().isEditable());

                openDialog(userUI);

                if (getRuleList().isEditable() && userUI.getModel().isValid()) {
                    // if user validates, build allowed values from qualitative value ids
                    allowedValues = DaliBeans.joinIds(userUI.getModel().getSelectedElements(), getConfig().getValueSeparator());
                }
                getTable().editingStopped(null);
            }

            @Override
            public Object getCellEditorValue() {
                return allowedValues;
            }
        });

        // specific editor for preconditions
        allowedValuesEditors.put(ControlRuleRowModel.preconditionPredicate, new ButtonCellEditor() {
            @Override
            public void onButtonCellAction(int row, int column) {
                int rowModelIndex = getTable().convertRowIndexToModel(row);
                ControlRuleRowModel rowModel = getTableModel().getEntry(rowModelIndex);

                if (rowModel.sizeRulePmfms() != 2) return;

                RulePreconditionUI rulePreconditionUI = new RulePreconditionUI(getContext());
                RulePreconditionUIModel model = rulePreconditionUI.getModel();
                model.setQvMap(getContext().getRuleListService().buildQualitativeValueMapFromPreconditions(rowModel.getPreconditions()));
                model.setBasePmfm(rowModel.getRulePmfms(0).getPmfm());
                model.setUsedPmfm(rowModel.getRulePmfms(1).getPmfm());
                rulePreconditionUI.getHandler().setEnabled(getRuleList().isEditable());
                openDialog(rulePreconditionUI, new Dimension(600, 800));

                if (getRuleList().isEditable() && model.isModify() && model.isValid()) {
                    getContext().getRuleListService().buildPreconditionsFromQualitativeValueMap(rowModel, model.getQvMap());
                    getModel().setModify(true);
                }

            }
        });

        return allowedValuesEditors;
    }

    private Map<Predicate<ControlRuleRowModel>, TableCellRenderer> buildAllowedValuesRenderers() {

        Map<Predicate<ControlRuleRowModel>, TableCellRenderer> allowedValuesRenderers = Maps.newHashMap();

        // renderer for qualitative values
        allowedValuesRenderers.put(ControlRuleRowModel.qualitativeValuePredicate, new AllowedValuesCellRenderer("qualitative-value"));

        // renderer for users
        allowedValuesRenderers.put(ControlRuleRowModel.observersPredicate, new AllowedValuesCellRenderer("person"));

        // renderer for preconditions
        allowedValuesRenderers.put(ControlRuleRowModel.preconditionPredicate, new ButtonCellRenderer(SwingUtil.createActionIcon("associated-qualitative-value"), true));

        return allowedValuesRenderers;
    }

    private void initListeners() {

        // Table listener
        getModel().addPropertyChangeListener(ControlRuleTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            // Si un seul element a ete selectionne
            final ControlRuleRowModel controlRule = getModel().getSingleSelectedRow();

            configureColumns(controlRule);

            boolean editable = controlRule != null && controlRule.isEditable();

            // Chargement des PSFMControle
            loadPmfmTable(controlRule, editable);

            // Chargement des commentaires/messages affiches
            loadControlRuleInformation(controlRule, editable);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{ControlRuleRowModel.PROPERTY_ERRORS, ControlRuleRowModel.PROPERTY_PMFM_VALID};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, ControlRuleRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        if (ControlRuleRowModel.PROPERTY_FUNCTION.equals(propertyName) || ControlRuleRowModel.PROPERTY_CONTROL_ELEMENT.equals(propertyName)) {
            configureColumns(row);
        }

        if (ControlRuleRowModel.PROPERTY_FUNCTION.equals(propertyName) && row.getFunction() != null) {
            // reset min max or allowed values
            if (!ControlFunctionValues.MIN_MAX.equals(row.getFunction()) && !ControlFunctionValues.MIN_MAX_DATE.equals(row.getFunction())) {
                row.setMin(null);
                row.setMax(null);
            }
            if (!ControlFunctionValues.IS_AMONG.equals(row.getFunction())) {
                row.setAllowedValues(null);
            }
            // reset preconditions
            if (!DaliBeans.isPreconditionRule(row)) {
                row.setPreconditions(null);
            }
            // reset rulePmfm ids (Mantis #45625)
            resetRulePmfmIds(row);

        }

        if (ControlRuleRowModel.PROPERTY_CONTROL_ELEMENT.equals(propertyName)) {
            row.setControlFeature(null);
        }

        if (ControlRuleRowModel.PROPERTY_CONTROL_FEATURE.equals(propertyName)) {

            // reset pmfm list if not mandatory
            if (!DaliBeans.isPmfmMandatory(row) && !DaliBeans.isPreconditionRule(row)) {
                row.setRulePmfms(null);
            }

            // reload pmfms
            loadPmfmTable(row, row.isEditable());
        }

        if (ControlRuleRowModel.PROPERTY_RULE_PMFMS.equals(propertyName) && ControlFunctionValues.PRECONDITION.equals(row.getFunction())) {
            // Clear preconditions when the pmfms are not valid (Mantis #42773)
            if (CollectionUtils.size(newValue) < 2) {
                row.getPreconditions().clear();
            }
        }

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        saveToParentModel();
    }

    private void resetRulePmfmIds(ControlRuleRowModel row) {
        if (row == null) return;
        row.getRulePmfms().forEach(rulePmfm -> rulePmfm.setId(null));
    }

    private void loadPmfmTable(ControlRuleDTO controlRule, boolean editable) {
        getRulesUI().getControlPmfmTableUI().getHandler().loadPmfms(controlRule, getTable().isEditable() && editable);
    }

    private void saveToParentModel() {
        getRuleList().setControlRules(getModel().getBeans());
        recomputeRowsValidState(false);

        getModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<ControlRuleRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            ControlRuleRowModel row = addedRows.get(0);

            // create a new rule by service
            ControlRuleDTO newRule = getContext().getRuleListService().newControlRule(getRuleList());
            // convert it to the added row
            row.fromBean(newRule);

            getModel().setModify(true);
            setFocusOnCell(row);
        }
    }

    private RuleListRowModel getRuleList() {
        return getModel().getParentModel().getRuleListUIModel().getSingleSelectedRow();
    }

    /**
     * <p>removeRules.</p>
     */
    public void removeRules() {
        if (getContext().getDialogHelper().showConfirmDialog(
                t("dali.rule.controlRule.delete.message"),
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

            getModel().deleteSelectedRows();
            saveToParentModel();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(ControlRuleRowModel row) {
        return super.isRowValid(row) && isControlRuleValid(row);
    }

    private boolean isControlRuleValid(ControlRuleRowModel row) {

        row.getErrors().clear();

        // check code length (rule_cd : 40 characters max)
        if (StringUtils.length(row.getCode()) > 40) {
            DaliBeans.addError(row, t("dali.rule.controlRule.code.tooLong", 40), ControlRuleRowModel.PROPERTY_CODE);
        }

        // check mandatory PMFM
        row.setPmfmValid(true);
        if (DaliBeans.isPmfmMandatory(row)) {

            if (ControlFunctionValues.PRECONDITION.equals(row.getFunction())) {
                if (row.sizeRulePmfms() != 2 || (!(row.getRulePmfms(0).getPmfm().getParameter().isQualitative() && row.getRulePmfms(1).getPmfm().getParameter().isQualitative()))) {
                    row.setPmfmValid(false);
                    DaliBeans.addError(row, t("dali.rule.rulePrecondition.invalidQualitativePmfm"), ControlRuleRowModel.PROPERTY_CODE);
                }
            } else {
                if (row.isRulePmfmsEmpty()) {
                    row.setPmfmValid(false);
                    DaliBeans.addError(row, t("dali.rule.controlRule.noPmfm"), ControlRuleRowModel.PROPERTY_CODE);
                }
            }

        }

        // check min max
        if (ControlFunctionValues.MIN_MAX.equals(row.getFunction())) {
            Number min = (row.getMin() instanceof Number) ? (Number) row.getMin() : null;
            Number max = (row.getMax() instanceof Number) ? (Number) row.getMax() : null;
            if (min == null && max == null) {
                DaliBeans.addError(row, t("dali.rule.controlRule.minMaxEmpty"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            } else if (min != null && max != null && min.doubleValue() > max.doubleValue()) {
                DaliBeans.addError(row, t("dali.rule.controlRule.minGreaterThanMax"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            }
        }

        // check min max date
        if (ControlFunctionValues.MIN_MAX_DATE.equals(row.getFunction())) {
            Date min = (row.getMin() instanceof Date) ? (Date) row.getMin() : null;
            Date max = (row.getMax() instanceof Date) ? (Date) row.getMax() : null;
            if (min == null && max == null) {
                DaliBeans.addError(row, t("dali.rule.controlRule.minMaxDateEmpty"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            } else if (min != null && max != null && min.after(max)) {
                DaliBeans.addError(row, t("dali.rule.controlRule.minDateGreaterThanMaxDate"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            }
        }

        // check allowed values separator
        if (ControlFunctionValues.IS_AMONG.equals(row.getFunction())) {
            if (StringUtils.isNotBlank(row.getAllowedValues())) {
                if (row.getAllowedValues().contains(" ") && !row.getAllowedValues().contains(getConfig().getValueSeparator())) {
                    // the value separator is not present in allowedValues
                    DaliBeans.addError(row,
                            t("dali.rule.controlRule.allowedValues.noSeparator", getConfig().getValueSeparator()),
                            ControlRuleRowModel.PROPERTY_ALLOWED_VALUES);
                }
            } else {
                // no allowed values
                DaliBeans.addWarning(row,
                        t("dali.rule.controlRule.allowedValues.empty"),
                        ControlRuleRowModel.PROPERTY_ALLOWED_VALUES);

            }
        }

        // check preconditions validity to avoid save action to be performed with empty preconditions (Mantis #41964)
        if (ControlFunctionValues.PRECONDITION.equals(row.getFunction()) && CollectionUtils.isEmpty(row.getPreconditions()) && row.isPmfmValid()) {
            DaliBeans.addError(row,
                    t("dali.rule.controlRule.preconditions.empty"),
                    ControlRuleRowModel.PROPERTY_ALLOWED_VALUES);
        }

        return DaliBeans.hasNoBlockingError(row);
    }

    private void configureColumns(ControlRuleDTO rule) {
        if (rule == null) {
            return;
        }

        // configure control feature combo
        if (rule.getControlElement() != null) {
            List<ControlFeatureDTO> controlFeatures = getContext().getSystemService().getControlFeatures(rule.getControlElement());
            controlFeatureEditor.getCombo().setData(controlFeatures);
        } else {
            controlFeatureEditor.getCombo().setData(null);
        }

        // special case for precondition
        if (ControlFunctionValues.PRECONDITION.equals(rule.getFunction())) {
            rule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            rule.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());
        }

        getTable().repaint();
    }

    /**
     * <p>loadControlRuleInformation.</p>
     *
     * @param controlRule a {@link fr.ifremer.dali.dto.configuration.control.ControlRuleDTO} object.
     */
    private void loadControlRuleInformation(final ControlRuleDTO controlRule, boolean editable) {

        final RulesUI rulesUI = getRulesUI();

        rulesUI.getModel().setLoading(true);

        if (controlRule != null) {
            rulesUI.getRuleMessageEditor().setEnabled(getTable().isEditable() && editable);
            rulesUI.getRuleDescriptionEditor().setEnabled(getTable().isEditable() && editable);
            rulesUI.getModel().setRuleMessage(controlRule.getMessage());
            rulesUI.getModel().setRuleDescription(controlRule.getDescription());
        } else {
            rulesUI.getRuleMessageEditor().setEnabled(false);
            rulesUI.getRuleDescriptionEditor().setEnabled(false);
            rulesUI.getModel().setRuleMessage(null);
            rulesUI.getModel().setRuleDescription(null);
        }
        rulesUI.getModel().setLoading(false);
    }

    /**
     * <p>clearControlRuleInformation.</p>
     */
    public void clearControlRuleInformation() {

        final RulesUI rulesUI = getRulesUI();
        rulesUI.getModel().setLoading(true);

        rulesUI.getRuleMessageEditor().setEnabled(false);
        rulesUI.getRuleDescriptionEditor().setEnabled(false);

        rulesUI.getModel().setRuleMessage(null);
        rulesUI.getModel().setRuleDescription(null);

        rulesUI.getModel().setLoading(false);
    }

    /**
     * <p>loadReglesControle.</p>
     *
     * @param controlRules a {@link Collection} object.
     * @param readOnly     read only ?
     */
    public void loadControlRules(final Collection<ControlRuleDTO> controlRules, boolean readOnly) {

        // Stop table edition to avoid rendering exception (Mantis #49268)
        getTable().editingStopped(null);

        // Load control rules
        getModel().setBeans(controlRules);
        getModel().getRows().forEach(controlRuleRowModel -> controlRuleRowModel.setEditable(!readOnly));

        // Activate add button
        getUI().getAddControlRuleButton().setEnabled(!readOnly);

        recomputeRowsValidState(false);
    }

    public boolean canDeleteSelectedRows() {
        return getModel().getSelectedRows().stream().allMatch(AbstractRowUIModel::isEditable);
    }

    /**
     * <p>supprimerReglesControle.</p>
     */
    public void clearTable() {

        getModel().setBeans(null);

        getUI().getAddControlRuleButton().setEnabled(false);
    }

    /**
     * <p>editRuleListCode.</p>
     */
    public void editRuleCode() {
        ControlRuleRowModel row = getModel().getSingleSelectedRow();
        if (row != null && row.isNewCode()) {
            if (checkNewCode(row)) {
                getRuleList().setDirty(true);
            }
        }
    }

    private boolean checkNewCode(ControlRuleRowModel row) {

        boolean edit = StringUtils.isNotBlank(row.getCode());

        // ask for new code
        String newCode = (String) getContext().getDialogHelper().showInputDialog(
                getUI(),
                t("dali.rule.controlRule.setCode"),
                edit ? t("dali.rule.controlRule.editCode.title") : t("dali.rule.controlRule.setCode.title"),
                null,
                row.getCode());

        if (checkCodeDuplicates(newCode, row, edit)) {
            row.setCode(newCode);
            return true;
        }

        return false;
    }

    /**
     * <p>checkCodeDuplicates.</p>
     *
     * @param newCode    a {@link java.lang.String} object.
     * @param currentRow a {@link ControlRuleRowModel} object.
     * @param edit       a boolean.
     * @return a boolean.
     */
    private boolean checkCodeDuplicates(String newCode, ControlRuleRowModel currentRow, boolean edit) {

        if (StringUtils.isBlank(newCode)) {
            return false;
        }

        newCode = newCode.trim();

        // check if new code is already in UI
        for (ControlRuleRowModel otherRow : getModel().getRows()) {
            if (currentRow == otherRow) continue;
            if (newCode.equalsIgnoreCase(otherRow.getCode())) {
                getContext().getDialogHelper().showErrorDialog(
                        t("dali.error.alreadyExists.referential", t("dali.rule.controlRule.title"), newCode, t("dali.property.referential.national")),
                        edit ? t("dali.rule.controlRule.editCode.title") : t("dali.rule.controlRule.setCode.title")
                );
                return false;
            }
        }

        // check if new code is already in bdd
        if (getContext().getRuleListService().ruleCodeExists(newCode)) {
            getContext().getDialogHelper().showErrorDialog(
                    t("dali.error.alreadyExists.referential", t("dali.rule.controlRule.title"), newCode, t("dali.property.referential.national")),
                    edit ? t("dali.rule.controlRule.editCode.title") : t("dali.rule.controlRule.setCode.title")
            );
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<ControlRuleRowModel> getTableModel() {
        return (ControlRuleTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getControlRuleTable();
    }

    /**
     * <p>getRulesUI.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.manage.rule.RulesUI} object.
     */
    private RulesUI getRulesUI() {
        return getUI().getParentContainer(RulesUI.class);
    }

    private class AllowedValuesCellRenderer extends ButtonCellRenderer<String> {

        AllowedValuesCellRenderer(String actionIconName) {
            super(SwingUtil.createActionIcon(actionIconName), false);
        }

        @Override
        protected String getText(JTable table, String value, int row, int column) {
            return value == null ? "0" : String.valueOf(new StringTokenizer(value, getConfig().getValueSeparator()).countTokens());
        }
    }
}
