package fr.ifremer.dali.ui.swing.content.manage.referential.taxon.taxonsDialog;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Handler.
 */
public class TaxonsDialogUIHandler extends AbstractDaliUIHandler<TaxonsDialogUIModel, TaxonsDialogUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(TaxonsDialogUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new TaxonsDialogUIModel());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(TaxonsDialogUI ui) {
        initUI(ui);

        getModel().addPropertyChangeListener(evt -> {

            if (TaxonsDialogUIModel.PROPERTY_TAXON_GROUP.equals(evt.getPropertyName())) {

                List<TaxonDTO> taxons = getModel().getTaxonGroup() != null ? new ArrayList<>(getModel().getTaxonGroup().getTaxons()) : null;
                getUI().getTaxonsTable().getModel().setBeans(taxons);

            } else if (TaxonsDialogUIModel.PROPERTY_TAXONS.equals(evt.getPropertyName())) {

                getUI().getTaxonsTable().getModel().setBeans(getModel().getTaxons());

            }
        });

        // hide menu
        ui.getTaxonsTable().getTaxonsNationalMenuUI().setVisible(false);
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
