package fr.ifremer.dali.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.control.RuleListDTO;
import fr.ifremer.dali.ui.swing.content.manage.rule.RulesUI;
import fr.ifremer.dali.ui.swing.content.manage.rule.RulesUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SwingUtilities;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des listes de regles.
 */
public class RuleListUIHandler extends AbstractDaliTableUIHandler<RuleListRowModel, RuleListUIModel, RuleListUI> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final RuleListUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final RuleListUIModel model = new RuleListUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final RuleListUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Disable buttons
        getUI().getDuplicateRuleListButton().setEnabled(false);
        getUI().getDeleteRuleListButton().setEnabled(false);

        // Initialisation du tableau
        initTable();

        // Initialisation des listeners
        initListeners();

    }

    /**
     * Chargement des regles.
     *
     * @param ruleLists Les regles
     */
    public void loadRuleLists(final List<RuleListDTO> ruleLists) {

        getModel().setBeans(ruleLists);
        getModel().getRows().forEach(ruleListRowModel -> ruleListRowModel.setEditable(getModel().isSaveEnabled()));

        // auto select unique row
        if (getModel().getRowCount() == 1) {
            RuleListRowModel rowModel = getModel().getRows().get(0);
            SwingUtilities.invokeLater(() -> {
                selectRow(rowModel);
                getModel().setSingleSelectedRow(rowModel);
            });
        }
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {

        loadRuleLists(null);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // Code
        TableColumnExt codeCol = addColumn(RuleListTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // Actif
        final TableColumnExt activeCol = addBooleanColumnToModel(RuleListTableModel.ACTIVE, table);
        activeCol.setSortable(true);

        // Mois debut
//        TableColumnExt startMonthCol = addDatePickerColumnToModel(RuleListTableModel.START_MONTH, "MMMM");
        TableColumnExt startMonthCol = addSimpleComboDataColumnToModel(RuleListTableModel.START_MONTH, getContext().getSystemService().getMonths());
        startMonthCol.setSortable(true);

        // Mois fin
//        TableColumnExt endMonthCol = addDatePickerColumnToModel(RuleListTableModel.END_MONTH, "MMMM");
        TableColumnExt endMonthCol = addSimpleComboDataColumnToModel(RuleListTableModel.END_MONTH, getContext().getSystemService().getMonths());
        endMonthCol.setSortable(true);

        // Description
        TableColumnExt descriptionCol = addColumn(RuleListTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        TableColumnExt creationDateCol = addDatePickerColumnToModel(RuleListTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(RuleListTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        RuleListTableModel tableModel = new RuleListTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Initialisation du tableau
        initTable(table);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        table.setVisibleRowCount(3);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{RuleListRowModel.PROPERTY_ERRORS};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(RuleListRowModel row) {
        return super.isRowValid(row) && isRuleListValid(row);
    }

    private boolean isRuleListValid(RuleListRowModel row) {

        if (row == getModel().getSingleSelectedRow()) {

            // remove previous errors
            row.getErrors().clear();

            // both start and end dates must be null or not null
            if (row.getStartMonth() == null && row.getEndMonth() != null) {
                DaliBeans.addError(row, t("dali.rule.ruleList.startMonth.null"), RuleListRowModel.PROPERTY_START_MONTH);

            } else if (row.getStartMonth() != null && row.getEndMonth() == null) {
                DaliBeans.addError(row, t("dali.rule.ruleList.endMonth.null"), RuleListRowModel.PROPERTY_END_MONTH);
            }

            // check no program
            if (row.isProgramsEmpty()) {
                DaliBeans.addError(row, t("dali.rule.ruleList.error.noProgram"), RuleListRowModel.PROPERTY_CODE);
            }

            // check no department
            if (row.isDepartmentsEmpty()) {
                DaliBeans.addError(row, t("dali.rule.ruleList.error.noDepartment"), RuleListRowModel.PROPERTY_CODE);
            }

            // check no control rule
            if (row.isControlRulesEmpty()) {
                DaliBeans.addError(row, t("dali.rule.ruleList.error.noControlRule"), RuleListRowModel.PROPERTY_CODE);
            }

            // check sub-models
            RulesUIModel rulesUIModel = getRulesUI().getModel();
            if (!rulesUIModel.getProgramsUIModel().isValid()
                    || !rulesUIModel.getDepartmentsUIModel().isValid()
                    || !rulesUIModel.getControlRuleUIModel().isValid()
                    || !rulesUIModel.getPmfmUIModel().isValid()) {

                DaliBeans.addError(row, t("dali.rule.ruleList.error"), RuleListRowModel.PROPERTY_CODE);
            }

        }
        return row.isErrorsEmpty();
    }

    /**
     * <p>getRulesUI.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.manage.rule.RulesUI} object.
     */
    public RulesUI getRulesUI() {
        return getUI().getParentContainer(RulesUI.class);
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(RuleListUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            final RulesUI rulesUI = getRulesUI();
            // On efface les informations sur le PSFM vu qu'il n'est pas sélectioné par default
            rulesUI.getControlPmfmTableUI().getHandler().clearTable();
            rulesUI.getControlRuleTableUI().getHandler().clearControlRuleInformation();

            final RuleListRowModel ruleList = getModel().getSingleSelectedRow();
            // Si un seul element a ete selectionne
            if (ruleList != null) {

                // Chargement des ProgrammesControle
                rulesUI.getControlProgramTableUI().getHandler().loadPrograms(ruleList.getPrograms(), !ruleList.isEditable());

                // Chargement des ServicesControle
                rulesUI.getControlDepartmentTableUI().getHandler().loadDepartments(ruleList.getDepartments(), !ruleList.isEditable());

                //Chargement des ReglesControle
                rulesUI.getControlRuleTableUI().getHandler().loadControlRules(ruleList.getControlRules(), !ruleList.isEditable());

            } else {
                rulesUI.getControlProgramTableUI().getHandler().clearTable();
                rulesUI.getControlDepartmentTableUI().getHandler().clearTable();
                rulesUI.getControlRuleTableUI().getHandler().clearTable();
            }

        });

        // Add listener on saveEnabled property to disable change (Mantis #47532)
        getModel().addPropertyChangeListener(RuleListUIModel.PROPERTY_SAVE_ENABLED, evt -> {
            getModel().getRows().forEach(ruleListRowModel -> ruleListRowModel.setEditable(getModel().isSaveEnabled()));
            getUI().getRulesTableUI().invalidate();
            getUI().getRulesTableUI().repaint();
            if (getModel().getSingleSelectedRow() != null) {
                getModel().setSingleSelectedRow(getModel().getSingleSelectedRow());
            }
        });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, RuleListRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<RuleListRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            final RuleListRowModel row = addedRows.get(0);

            // if the new code is already set (means that the new row is a duplication), don't ask a new code (Mantis #40662)
            if (!row.isNewCode()) {

                if (checkNewCode(row)) {

                    row.setActive(true);

                    // add the department of the recorder
                    // Removed (see Mantis #40474)
//                if (getContext().getDataContext().getRecorderDepartmentId() != null) {
//                    DepartmentDTO department = getContext().getReferentialService().getDepartmentById(getContext().getDataContext().getRecorderDepartmentId());
//                    if (!row.containsDepartments(department)) {
//                        row.addDepartments(department);
//                    }
//                }

                    row.setNewCode(true);
                    setFocusOnCell(row);

                } else {

                    // if code is invalid, remove this row
                    SwingUtilities.invokeLater(() -> {
                        getModel().deleteRow(row);
                        getModel().setSingleSelectedRow(null);
                    });

                }
            }
        }
    }

    private boolean checkNewCode(RuleListRowModel row) {

        boolean edit = StringUtils.isNotBlank(row.getCode());

        // ask for new code
        String newCode = (String) getContext().getDialogHelper().showInputDialog(
                getUI(),
                t("dali.rule.ruleList.setCode"),
                edit ? t("dali.rule.ruleList.editCode.title") : t("dali.rule.ruleList.setCode.title"),
                null,
                row.getCode());

        if (checkCodeDuplicates(newCode, row, edit)) {
            row.setCode(newCode);
            return true;
        }

        return false;
    }

    /**
     * <p>checkCodeDuplicates.</p>
     *
     * @param newCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean checkCodeDuplicates(String newCode) {
        return checkCodeDuplicates(newCode, null, false);
    }

    /**
     * <p>checkCodeDuplicates.</p>
     *
     * @param newCode    a {@link java.lang.String} object.
     * @param currentRow a {@link RuleListRowModel} object.
     * @param edit       a boolean.
     * @return a boolean.
     */
    private boolean checkCodeDuplicates(String newCode, RuleListRowModel currentRow, boolean edit) {

        if (StringUtils.isBlank(newCode)) {
            return false;
        }

        newCode = newCode.trim();

        // check if new code is already in UI
        for (RuleListRowModel otherRow : getModel().getRows()) {
            if (currentRow == otherRow) continue;
            if (newCode.equalsIgnoreCase(otherRow.getCode())) {
                getContext().getDialogHelper().showErrorDialog(
                        t("dali.error.alreadyExists.referential", t("dali.rule.ruleList.title"), newCode, t("dali.property.referential.national")),
                        edit ? t("dali.rule.ruleList.editCode.title") : t("dali.rule.ruleList.setCode.title")
                );
                return false;
            }
        }

        // check if new code is already in bdd
        if (getContext().getRuleListService().ruleListCodeExists(newCode)) {
            getContext().getDialogHelper().showErrorDialog(
                    t("dali.error.alreadyExists.referential", t("dali.rule.ruleList.title"), newCode, t("dali.property.referential.national")),
                    edit ? t("dali.rule.ruleList.editCode.title") : t("dali.rule.ruleList.setCode.title")
            );
            return false;
        }

        return true;
    }

    /**
     * <p>editRuleListCode.</p>
     */
    public void editRuleListCode() {
        RuleListRowModel row = getModel().getSingleSelectedRow();
        if (row != null && row.isNewCode()) {
            if (checkNewCode(row)) {
                row.setDirty(true);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<RuleListRowModel> getTableModel() {
        return (RuleListTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getRulesTableUI();
    }
}
