package fr.ifremer.dali.ui.swing.content.extraction.config;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionParameterDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.ui.swing.content.extraction.ExtractionUIModel;
import fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsRowModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;

/**
 * @author peck7 on 21/11/2017.
 */
public class ExtractionConfigUIModel extends AbstractDaliBeanUIModel<ExtractionParameterDTO, ExtractionConfigUIModel> implements ExtractionParameterDTO {

    private static final Binder<ExtractionParameterDTO, ExtractionConfigUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(ExtractionParameterDTO.class, ExtractionConfigUIModel.class);
    private static final Binder<ExtractionConfigUIModel, ExtractionParameterDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ExtractionConfigUIModel.class, ExtractionParameterDTO.class);

    private boolean enabled;
    public static final String PROPERTY_ENABLED = "enabled";

    public ExtractionConfigUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    private ExtractionUIModel extractionUIModel;

    public void setExtractionUIModel(ExtractionUIModel extractionUIModel) {
        this.extractionUIModel = extractionUIModel;
    }

    public ExtractionsRowModel getSelectedExtraction() {
        return extractionUIModel == null ? null : extractionUIModel.getSelectedExtraction();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        boolean oldValue = isEnabled();
        this.enabled = enabled;
        firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    }

    // DELEGATE METHODS
    @Override
    protected ExtractionParameterDTO newBean() {
        return DaliBeanFactory.newExtractionParameterDTO();
    }

    @Override
    public boolean isFillZero() {
        return delegateObject.isFillZero();
    }

    @Override
    public void setFillZero(boolean fillZero) {
        delegateObject.setFillZero(fillZero);
        // force modify state change
        firePropertyChange(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);
    }

    @Override
    public PmfmPresetDTO getPmfmPresets(int index) {
        return delegateObject.getPmfmPresets(index);
    }

    @Override
    public boolean isPmfmPresetsEmpty() {
        return delegateObject.isPmfmPresetsEmpty();
    }

    @Override
    public int sizePmfmPresets() {
        return delegateObject.sizePmfmPresets();
    }

    @Override
    public void addPmfmPresets(PmfmPresetDTO pmfmPresets) {
        delegateObject.addPmfmPresets(pmfmPresets);
    }

    @Override
    public void addAllPmfmPresets(Collection<PmfmPresetDTO> pmfmPresets) {
        delegateObject.addAllPmfmPresets(pmfmPresets);
    }

    @Override
    public boolean removePmfmPresets(PmfmPresetDTO pmfmPresets) {
        return delegateObject.removePmfmPresets(pmfmPresets);
    }

    @Override
    public boolean removeAllPmfmPresets(Collection<PmfmPresetDTO> pmfmPresets) {
        return delegateObject.removeAllPmfmPresets(pmfmPresets);
    }

    @Override
    public boolean containsPmfmPresets(PmfmPresetDTO pmfmPresets) {
        return delegateObject.containsPmfmPresets(pmfmPresets);
    }

    @Override
    public boolean containsAllPmfmPresets(Collection<PmfmPresetDTO> pmfmPresets) {
        return delegateObject.containsAllPmfmPresets(pmfmPresets);
    }

    @Override
    public Collection<PmfmPresetDTO> getPmfmPresets() {
        return delegateObject.getPmfmPresets();
    }

    @Override
    public void setPmfmPresets(Collection<PmfmPresetDTO> pmfmPresets) {
        delegateObject.setPmfmPresets(pmfmPresets);
    }

    @Override
    public PmfmDTO getPmfmResults(int index) {
        return delegateObject.getPmfmResults(index);
    }

    @Override
    public boolean isPmfmResultsEmpty() {
        return delegateObject.isPmfmResultsEmpty();
    }

    @Override
    public int sizePmfmResults() {
        return delegateObject.sizePmfmResults();
    }

    @Override
    public void addPmfmResults(PmfmDTO pmfmResults) {
        delegateObject.addPmfmResults(pmfmResults);
    }

    @Override
    public void addAllPmfmResults(Collection<PmfmDTO> pmfmResults) {
        delegateObject.addAllPmfmResults(pmfmResults);
    }

    @Override
    public boolean removePmfmResults(PmfmDTO pmfmResults) {
        return delegateObject.removePmfmResults(pmfmResults);
    }

    @Override
    public boolean removeAllPmfmResults(Collection<PmfmDTO> pmfmResults) {
        return delegateObject.removeAllPmfmResults(pmfmResults);
    }

    @Override
    public boolean containsPmfmResults(PmfmDTO pmfmResults) {
        return delegateObject.containsPmfmResults(pmfmResults);
    }

    @Override
    public boolean containsAllPmfmResults(Collection<PmfmDTO> pmfmResults) {
        return delegateObject.containsAllPmfmResults(pmfmResults);
    }

    @Override
    public Collection<PmfmDTO> getPmfmResults() {
        return delegateObject.getPmfmResults();
    }

    @Override
    public void setPmfmResults(Collection<PmfmDTO> pmfmResults) {
        delegateObject.setPmfmResults(pmfmResults);
    }
}
