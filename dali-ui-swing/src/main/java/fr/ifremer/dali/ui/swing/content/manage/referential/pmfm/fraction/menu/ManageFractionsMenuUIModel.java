package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.configuration.filter.FilterCriteriaDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;
import java.util.List;

/**
 * Modele du menu pour la gestion des Fractions au niveau local
 */
public class ManageFractionsMenuUIModel extends AbstractDaliBeanUIModel<FilterCriteriaDTO, ManageFractionsMenuUIModel> implements FilterCriteriaDTO {

    /** Constant <code>PROPERTY_FRACTION="fraction"</code> */
    public static final String PROPERTY_FRACTION = "fraction";
    private static final Binder<ManageFractionsMenuUIModel, FilterCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ManageFractionsMenuUIModel.class, FilterCriteriaDTO.class);
    private static final Binder<FilterCriteriaDTO, ManageFractionsMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(FilterCriteriaDTO.class, ManageFractionsMenuUIModel.class);
    private FractionDTO fraction;

    /**
     * <p>Constructor for ManageFractionsMenuUIModel.</p>
     */
    public ManageFractionsMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /**
     * <p>getStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStatusCode() {
        return getStatus() == null ? null : getStatus().getCode();
    }

    /**
     * <p>getFractionId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFractionId() {
        return getFraction() == null ? null : getFraction().getId();
    }

    /**
     * <p>Getter for the field <code>fraction</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.FractionDTO} object.
     */
    public FractionDTO getFraction() {
        return fraction;
    }

    /**
     * <p>Setter for the field <code>fraction</code>.</p>
     *
     * @param fraction a {@link fr.ifremer.dali.dto.referential.pmfm.FractionDTO} object.
     */
    public void setFraction(FractionDTO fraction) {
        this.fraction = fraction;
        firePropertyChange(PROPERTY_FRACTION, null, fraction);
    }

    /** {@inheritDoc} */
    @Override
    public List<? extends QuadrigeBean> getResults() {
        return delegateObject.getResults();
    }

    /** {@inheritDoc} */
    @Override
    public void setResults(List<? extends QuadrigeBean> results) {
        delegateObject.setResults(results);
//        firePropertyChange(PROPERTY_RESULTS, null, results);
    }

    /** {@inheritDoc} */
    @Override
    protected FilterCriteriaDTO newBean() {
        return DaliBeanFactory.newFilterCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void setDirty(boolean dirty) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }

    @Override
    public Date getCreationDate() {
        return null;
    }

    @Override
    public void setCreationDate(Date date) {

    }

    @Override
    public Date getUpdateDate() {
        return null;
    }

    @Override
    public void setUpdateDate(Date date) {

    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isStrictName() {
        return delegateObject.isStrictName();
    }

    /** {@inheritDoc} */
    @Override
    public void setStrictName(boolean strictName) {
        delegateObject.setStrictName(strictName);
    }

}
