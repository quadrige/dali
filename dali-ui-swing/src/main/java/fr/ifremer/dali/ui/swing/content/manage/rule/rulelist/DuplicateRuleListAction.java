package fr.ifremer.dali.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.control.RuleListDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import static org.nuiton.i18n.I18n.t;

/**
 * Rule list duplicate action
 */
public class DuplicateRuleListAction extends AbstractDaliAction<RuleListUIModel, RuleListUI, RuleListUIHandler> {

    private String newCode;
    private boolean duplicateControlRules;
    private RuleListDTO duplicatedRuleList;

    /**
     * Constructor.
     *
     * @param handler the handler
     */
    public DuplicateRuleListAction(RuleListUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction() || getModel().getSelectedRows().size() != 1) {
            return false;
        }

        String title = t("dali.action.duplicate.ruleList.title");
        String message = t("dali.action.duplicate.ruleList.message");
        JTextField codeField = new JTextField();

        JCheckBox checkBox = new JCheckBox(t("dali.action.duplicate.ruleList.controlRules"));
        Object[] messageObjects = {message, codeField, checkBox};

        int result = getContext().getDialogHelper().showOptionDialog(getUI(), messageObjects, title, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.NO_OPTION || result == JOptionPane.CLOSED_OPTION) {
            return false;
        }

        duplicateControlRules = checkBox.isSelected();
        newCode = codeField.getText();

        // check code duplicates
        return getHandler().checkCodeDuplicates(newCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        RuleListRowModel selectedRuleList = getModel().getSelectedRows().iterator().next();
        if (selectedRuleList != null) {

            duplicatedRuleList = getContext().getRuleListService().duplicateRuleList(selectedRuleList.toBean(), newCode, duplicateControlRules);
            duplicatedRuleList.setDirty(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getModel().setModify(true);

        getHandler().setFocusOnCell(getModel().addNewRow(duplicatedRuleList));
    }
}
