package fr.ifremer.dali.ui.swing.content.manage.program.pmfms.edit;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

/**
 * <p>EditPmfmDialogUIHandler class.</p>
 *
 */
public class EditPmfmDialogUIHandler extends AbstractDaliUIHandler<EditPmfmDialogUIModel, EditPmfmDialogUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(EditPmfmDialogUI ui) {
        super.beforeInit(ui);

        EditPmfmDialogUIModel model = new EditPmfmDialogUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(EditPmfmDialogUI editPmfmDialogUI) {
        initUI(ui);

        getModel().addPropertyChangeListener(EditPmfmDialogUIModel.PROPERTY_TABLE_MODEL, evt -> {

            // initialize ui with selected pmfm strategy
            PmfmStrategyDTO pmfmStrategy = getModel().getTableModel().getSingleSelectedRow() != null ? getModel().getTableModel().getSingleSelectedRow().toBean() : null;
            getModel().fromBean(pmfmStrategy);
        });
    }


    /**
     * <p>valid.</p>
     */
    public void valid() {

        // set properties to all selected pmfm strategies
        for (final PmfmStrategyDTO psfmProgStra : getModel().getTableModel().getSelectedRows()) {
            psfmProgStra.setSurvey(getModel().isSurvey());
            psfmProgStra.setSampling(getModel().isSampling());
            psfmProgStra.setGrouping(getModel().isGrouping());
            psfmProgStra.setUnique(getModel().isUnique());
        }

        // Quitter la dialogue
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
