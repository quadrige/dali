package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.associatedMatrices;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;

/**
 * Modele pour l administration des Fractions au niveau national
 */
public class AssociatedMatricesUIModel extends AbstractDaliTableUIModel<MatrixDTO, MatricesTableRowModel, AssociatedMatricesUIModel> {

    private FractionDTO fraction;
    /** Constant <code>PROPERTY_FRACTION="fraction"</code> */
    public static final String PROPERTY_FRACTION = "fraction";

    /**
     * <p>Getter for the field <code>fraction</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.FractionDTO} object.
     */
    public FractionDTO getFraction() {
        return fraction;
    }

    /**
     * <p>Setter for the field <code>fraction</code>.</p>
     *
     * @param fraction a {@link fr.ifremer.dali.dto.referential.pmfm.FractionDTO} object.
     */
    public void setFraction(FractionDTO fraction) {
        this.fraction = fraction;
        firePropertyChange(PROPERTY_FRACTION, null, fraction);
    }

}
