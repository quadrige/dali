<!--
  #%L
  Dali :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JPanel decorator='help' implements='fr.ifremer.dali.ui.swing.content.manage.referential.menu.ReferentialMenuUI&lt;TaxonGroupMenuUIModel, TaxonGroupMenuUIHandler&gt;'>
  <import>
    fr.ifremer.dali.ui.swing.DaliHelpBroker
    fr.ifremer.dali.ui.swing.DaliUIContext
    fr.ifremer.dali.ui.swing.util.DaliUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
    fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUI

    fr.ifremer.dali.dto.referential.TaxonGroupDTO
    fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO

    javax.swing.Box
    javax.swing.BoxLayout
    java.awt.BorderLayout
    javax.swing.SwingConstants

    fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox

    static org.nuiton.i18n.I18n.*
  </import>

  <!--getContextValue est une méthode interne JAXX-->
  <TaxonGroupMenuUIModel id='model' initializer='getContextValue(TaxonGroupMenuUIModel.class)'/>

  <DaliHelpBroker id='broker' constructorParams='"dali.home.help"'/>

  <script><![CDATA[
		//Le parent est très utile pour intervenir sur les frères
		public TaxonGroupMenuUI(ApplicationUI parentUI) {
		    ApplicationUIUtil.setParentUI(this, parentUI);
		}
	]]></script>

  <JPanel id="menuPanel" layout='{new BoxLayout(menuPanel, BoxLayout.PAGE_AXIS)}'>

    <ApplyFilterUI id='applyFilterUI' constructorParams='this'/>

    <Table id="selectionPanel" fill="both">
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='parentTaxonGroupLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='parentTaxonGroupCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='TaxonGroupDTO'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='labelLabel' constraints='BorderLayout.PAGE_START'/>
            <JTextField id='labelEditor' onKeyReleased='handler.setText(event, "label")'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='nameLabel' constraints='BorderLayout.PAGE_START'/>
            <JTextField id='nameEditor' onKeyReleased='handler.setText(event, "name")'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='statusLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='statusCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='StatusDTO'/>
          </JPanel>
        </cell>
      </row>
    </Table>

    <JPanel id='selectionButtonsPanel' layout='{new GridLayout(1,0)}'>
      <JButton id='clearButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
      <JButton id='searchButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
    </JPanel>

  </JPanel>
</JPanel>