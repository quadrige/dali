package fr.ifremer.dali.ui.swing.content.extraction;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.enums.ExtractionFilterTypeValues;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.dali.ui.swing.action.QuitScreenAction;
import fr.ifremer.dali.ui.swing.content.extraction.filters.ExtractionFiltersUIModel;
import fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsRowModel;
import fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsTableUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingUtilities;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Extraction handler.
 */
public class ExtractionUIHandler extends AbstractDaliUIHandler<ExtractionUIModel, ExtractionUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ExtractionUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ExtractionUI ui) {
        super.beforeInit(ui);

        // Ajout du model pour la page d acceuil
        final ExtractionUIModel model = new ExtractionUIModel();
        ui.setContextValue(model);

        ui.setContextValue(SwingUtil.createActionIcon("export"));

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ExtractionUI ui) {
        initUI(ui);
        ui.getSelectExtractionLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getSelectProgramLabel().setForeground(getConfig().getColorThematicLabel());

        // Save models
        getModel().setExtractionsTableUIModel(getUI().getExtractionsTable().getModel());
        getModel().setExtractionFiltersUIModel(getUI().getFiltersTable().getModel());
        getModel().setExtractionConfigUIModel(getUI().getConfigUI().getModel());

        // Initialisation des combobox
        initComboBox();

        // Initialisation des listeners
        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

        // init image
        SwingUtil.setComponentWidth(getUI().getLeftImage(), ui.getExtractionMenu().getPreferredSize().width * 9 / 10);
        getUI().getLeftImage().setScaled(true);

        getModel().setModify(false);
    }

    /**
     * Initialisation des composants de gauche
     */
    private void initComboBox() {

        // Init context combo
        initBeanFilterableComboBox(
                getUI().getContextComboBox(),
                getContext().getContextService().getAllContexts(),
                getContext().getSelectedContext());

        // Init extractions combo
        initBeanFilterableComboBox(
                getUI().getSelectExtractionCombo(),
                getContext().getExtractionService().getAllLightExtractions(),
                null);

        // Init programs combo
        initBeanFilterableComboBox(
                getUI().getSelectProgramCombo(),
                getContext().getObservationService().getAvailablePrograms(null, null, false, false),
                null);

        // Modification des largeurs des combobox
        DaliUIs.forceComponentSize(getUI().getContextComboBox());
        DaliUIs.forceComponentSize(getUI().getSelectExtractionCombo());
        DaliUIs.forceComponentSize(getUI().getSelectProgramCombo());

        getUI().getSelectExtractionCombo().getComboBoxModel().addWillChangeSelectedItemListener(event -> {
            if (getModel().isLoading()) return;
            if (event.getNextSelectedItem() != null) SwingUtilities.invokeLater(() -> getUI().getSearchButton().getAction().actionPerformed(null));
        });
    }

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        getModel().setLoading(true);
        getUI().getSelectExtractionCombo().setData(getContext().getExtractionService().getAllLightExtractions());
        getModel().setLoading(false);
    }

    /**
     * Initialiser les listeners.
     */
    private void initListeners() {

        // Listen modify property and set dirty to the selected program
        listenModelModify(getModel().getExtractionsTableUIModel());
        getModel().getExtractionFiltersUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_MODIFY, evt -> {
            Boolean modify = (Boolean) evt.getNewValue();
            if (!getModel().getExtractionFiltersUIModel().isLoading() && modify != null) {
                getModel().setModify(modify);
                if (modify && getModel().getSelectedExtraction() != null) {
                    getModel().getSelectedExtraction().setDirty(true);
                }
            }
        });
        getModel().getExtractionConfigUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_MODIFY, evt -> {
            Boolean modify = (Boolean) evt.getNewValue();
            if (!getModel().getExtractionConfigUIModel().isLoading() && modify != null) {
                getModel().setModify(modify);
                ExtractionsRowModel selectedExtraction = getModel().getSelectedExtraction();
                if (modify && selectedExtraction != null) {
                    selectedExtraction.setParameter(getModel().getExtractionConfigUIModel().toBean());
                    selectedExtraction.setDirty(true);
                }
            }
        });

        // Listen when a extraction is selected by table
        // Listener on selection
        getModel().getExtractionsTableUIModel().addPropertyChangeListener(ExtractionsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {
            getUI().getFiltersTable().getHandler().loadFilters();
            getUI().getConfigUI().getHandler().loadConfig();
        });

        getModel().getExtractionsTableUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_VALID, evt -> getValidator().doValidate());
        getModel().getExtractionFiltersUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_VALID, evt -> {
            Boolean valid = (Boolean) evt.getNewValue();
            ExtractionsRowModel extractionRow = getModel().getSelectedExtraction();
            if (extractionRow != null) {

                // set filters valid state on current selected extraction
                extractionRow.setFiltersValid(valid);

                // enable config UI
                getModel().getExtractionConfigUIModel().setEnabled(valid);

                getUI().getExtractionsTable().getHandler().recomputeRowValidState(extractionRow);
                getValidator().doValidate();
            } else {

                // disable config UI
                getModel().getExtractionConfigUIModel().setEnabled(false);

            }
        });

        // listener on program selection
        getModel().addPropertyChangeListener(ExtractionUIModel.PROPERTY_PROGRAM, evt -> {
            // TODO ?
        });

        // propagate loading property
        getModel().getExtractionFiltersUIModel().addPropertyChangeListener(ExtractionFiltersUIModel.PROPERTY_LOADING, evt -> {
            if (evt.getNewValue() instanceof Boolean) {
                // TODO FiltersLoading pas utiliser !
                getModel().getExtractionsTableUIModel().setFiltersLoading((Boolean) evt.getNewValue());
            }
        });

        // Change context value
        getModel().addPropertyChangeListener(ExtractionUIModel.PROPERTY_CONTEXT, evt -> getContext().setSelectedContext(getModel().getContext()));

    }

    /**
     * Methode permettant le changement des extractions.
     *
     * @param extractions La liste des observations
     */
    public void loadExtractions(final List<ExtractionDTO> extractions) {
        getModel().getExtractionsTableUIModel().setBeans(extractions);

        // auto select unique row
        if (getModel().getExtractionsTableUIModel().getRowCount() == 1) {
            ExtractionsRowModel rowModel = getModel().getExtractionsTableUIModel().getRows().get(0);
            SwingUtilities.invokeLater(() -> {
                getUI().getExtractionsTable().getHandler().selectRow(rowModel);
                getModel().getExtractionsTableUIModel().setSingleSelectedRow(rowModel);
            });
        }
    }

    /**
     * return a ordered set of PMFM available for the specified program list and periods
     * need a Set instead of List (Mantis #44115)
     *
     * @param currentProgramCodes the program codes
     * @param currentPeriods the periods
     * @return a ordered set of PMFM
     */
    public Set<PmfmDTO> getPmfmsForSelectedExtraction(Collection<String> currentProgramCodes, Collection<ExtractionPeriodDTO> currentPeriods) {

        if (getModel().getSelectedExtraction() == null) return null;

        Collection<String> programCodes = currentProgramCodes != null
                ? currentProgramCodes
                : DaliBeans.getFilterElementsIds(getModel().getSelectedExtraction(), ExtractionFilterTypeValues.PROGRAM);
        Collection<ExtractionPeriodDTO> extractionPeriods = currentPeriods != null
                ? currentPeriods
                : DaliBeans.getExtractionPeriods(getModel().getSelectedExtraction());
        Set<PmfmStrategyDTO> pmfmStrategies = Sets.newHashSet();

        for (ExtractionPeriodDTO period : extractionPeriods) {
            pmfmStrategies.addAll(getContext().getProgramStrategyService().getPmfmStrategiesByProgramsAndDates(programCodes, period.getStartDate(), period.getEndDate()));
        }

        return pmfmStrategies.stream().filter(pmfmStrategy -> pmfmStrategy.isSampling() && pmfmStrategy.isGrouping())
                .sorted(Comparator.comparing(PmfmStrategyDTO::getRankOrder))
                .map(PmfmStrategyDTO::getPmfm).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<ExtractionUIModel> getValidator() {
        return getUI().getValidator();
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }
}
