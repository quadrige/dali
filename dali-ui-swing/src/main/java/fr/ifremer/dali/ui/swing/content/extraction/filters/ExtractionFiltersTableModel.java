package fr.ifremer.dali.ui.swing.content.extraction.filters;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * extractions table model
 */
public class ExtractionFiltersTableModel extends AbstractDaliTableModel<ExtractionFiltersRowModel> {

    /** Constant <code>FILTER_TYPE</code> */
    public static final DaliColumnIdentifier<ExtractionFiltersRowModel> FILTER_TYPE = DaliColumnIdentifier.newId(
            ExtractionFiltersRowModel.PROPERTY_FILTER_TYPE,
            n("dali.extraction.filters.type.short"),
            n("dali.extraction.filters.type.tip"),
            FilterTypeDTO.class);

    /** Constant <code>FILTER</code> */
    public static final DaliColumnIdentifier<ExtractionFiltersRowModel> FILTER = DaliColumnIdentifier.newId(
            ExtractionFiltersRowModel.PROPERTY_FILTER,
            n("dali.extraction.filters.elements.short"),
            n("dali.extraction.filters.elements.tip"),
            FilterDTO.class);

    /**
     * <p>Constructor for ExtractionFiltersTableModel.</p>
     *
     */
    public ExtractionFiltersTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public ExtractionFiltersRowModel createNewRow() {
        return new ExtractionFiltersRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<ExtractionFiltersRowModel> getFirstColumnEditing() {
        return null;
    }

}
