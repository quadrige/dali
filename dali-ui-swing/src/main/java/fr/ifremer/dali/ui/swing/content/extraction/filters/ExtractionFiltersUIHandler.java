package fr.ifremer.dali.ui.swing.content.extraction.filters;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.enums.ExtractionFilterTypeValues;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionParameterDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.dali.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.ui.swing.content.extraction.ExtractionUI;
import fr.ifremer.dali.ui.swing.content.extraction.filters.period.ExtractionPeriodUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des observations.
 */
public class ExtractionFiltersUIHandler extends
        AbstractDaliTableUIHandler<ExtractionFiltersRowModel, ExtractionFiltersUIModel, ExtractionFiltersUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ExtractionFiltersUIHandler.class);
    private FilterLoader filterLoader = new FilterLoader();

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ExtractionFiltersUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ExtractionFiltersUIModel model = new ExtractionFiltersUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ExtractionFiltersUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Initialisation du tableau
        initTable();
        SwingUtil.setLayerUI(ui.getExtractionsFiltersScrollPane(), ui.getTableBlockLayer());

        // Initialisation des listeners
        initListeners();

        // first load (empty filters)
        loadFilters();
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // filter type
        TableColumnExt filterTypeCol = addColumn(ExtractionFiltersTableModel.FILTER_TYPE);
        fixColumnWidth(filterTypeCol, 150);

        // filter
        addColumn(
                null,
                new FilterElementsCellRenderer(getContext().getDecoratorService()),
                ExtractionFiltersTableModel.FILTER);

        ExtractionFiltersTableModel tableModel = new ExtractionFiltersTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable(), true);

        // no sort
        getTable().setSortable(false);
        getTable().setEditable(false);

        // border
        addEditionPanelBorder();

        // add double click auto edit line
        getTable().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    getUI().getEditFilterButton().doClick();
                }
            }
        });

    }

    private void initListeners() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(ExtractionFiltersRowModel row) {
        return getModel().getSelectedExtraction() == null || (super.isRowValid(row) && isFilterValid(row));
    }

    private boolean isFilterValid(ExtractionFiltersRowModel row) {

        row.getErrors().clear();
        ExtractionFilterTypeValues extractionFilter = ExtractionFilterTypeValues.getExtractionFilterType(row.getFilterTypeId());
        Assert.notNull(extractionFilter);

        switch (extractionFilter) {
            case PERIOD:
                if (row.isFilterEmpty()) {
                    DaliBeans.addError(row, t("dali.extraction.filters.error.period.empty.message"), ExtractionFiltersRowModel.PROPERTY_FILTER);
                }
                break;

            case PROGRAM:
                if (row.isFilterEmpty()) {
                    DaliBeans.addError(row, t("dali.extraction.filters.error.program.empty.message"), ExtractionFiltersRowModel.PROPERTY_FILTER);
                }
                break;

        }

        return row.getErrors().isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<ExtractionFiltersRowModel> getTableModel() {
        return (ExtractionFiltersTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getExtractionsFiltersTable();
    }

    /**
     * <p>loadFilters.</p>
     */
    public void loadFilters() {

        if (!filterLoader.isDone()) {
            filterLoader.cancel(true);
        }

        getModel().setLoading(true);
        filterLoader = new FilterLoader();
        filterLoader.execute();
    }

    /**
     * <p>editFilter.</p>
     */
    @SuppressWarnings("unchecked")
    public void editFilter() {

        final ExtractionFiltersRowModel rowModel = getModel().getSingleSelectedRow();

        if (rowModel == null) {
            LOG.warn("no filter selected");
            return;
        }

        SwingUtilities.invokeLater(() -> {

            if (ExtractionFilterTypeValues.getExtractionFilterType(rowModel.getFilterTypeId()) == ExtractionFilterTypeValues.PERIOD) {

                // open period editor
                ExtractionPeriodUI periodUI = new ExtractionPeriodUI(getContext());
                periodUI.getModel().setBeans((Collection<ExtractionPeriodDTO>) rowModel.getFilteredElements());
                periodUI.getHandler().recomputeRowsValidState(false);
                openDialog(periodUI);

                // affect to model if valid
                if (periodUI.getModel().isValid()) {
                    FilterDTO periodFilter = rowModel.getFilter();
                    if (periodFilter == null) {
                        periodFilter = DaliBeanFactory.newFilterDTO();
                        periodFilter.setFilterTypeId(ExtractionFilterTypeValues.PERIOD.getFilterTypeId());
                    }

                    List<ExtractionPeriodDTO> newPeriods = periodUI.getModel().getBeans();

                    if (isPmfmsConfigValid(
                            DaliBeans.getFilterElementsIds(getModel().getSelectedExtraction(), ExtractionFilterTypeValues.PROGRAM),
                            newPeriods)) {

                        // Filter is valid with extraction parameter : save it
                        periodFilter.setElements(newPeriods);
                        rowModel.setFilter(periodFilter);
                        getModel().getSelectedExtraction().replaceFilter(periodFilter);
                    }

                }

            } else {

                // open filter ui
                SelectFilterUI filterUI = new SelectFilterUI(getContext(), rowModel.getFilterTypeId());
                filterUI.getModel().setSelectedElements(rowModel.getFilteredElements());
                filterUI.setTitle(t("dali.extraction.filters.select.title", decorate(rowModel.getFilterType())));
                openDialog(filterUI, new Dimension(1000, 600));

                if (filterUI.getModel().isValid()) {
                    FilterDTO filter = rowModel.getFilter();
                    if (filter == null) {
                        filter = DaliBeanFactory.newFilterDTO();
                        filter.setFilterTypeId(rowModel.getFilterTypeId());
                    }

                    List<? extends QuadrigeBean> newValue = filterUI.getModel().getSelectedElements();
                    boolean valid = true;

                    if (filter.getFilterTypeId().equals(ExtractionFilterTypeValues.PROGRAM.getFilterTypeId())) {
                        // validate program filter with extraction parameter
                        valid = isPmfmsConfigValid(
                                newValue.stream().map(bean -> ((ProgramDTO) bean).getCode()).collect(Collectors.toList()),
                                DaliBeans.getExtractionPeriods(getModel().getSelectedExtraction()));
                    }

                    if (valid) {
                        rowModel.setFilter(filter);
                        filter.setFilterLoaded(true);
                        filter.setElements(newValue);
                        filter.setDirty(true);
                        getModel().getSelectedExtraction().replaceFilter(filter);
                    }
                }

            }

            recomputeRowsValidState();
            getModel().firePropertyChanged(ExtractionFiltersUIModel.PROPERTY_VALID, null, getModel().isValid());
            getTable().repaint();

        });

    }

    /**
     * Check pmfms from configuration (if exists)
     *
     * @param programCodes program codes
     * @param periods      periods
     */
    private boolean isPmfmsConfigValid(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periods) {

        if (getModel().getSelectedExtraction().getParameter() == null) return true;

        ExtractionParameterDTO parameter = getModel().getSelectedExtraction().getParameter();

        // get all pmfm strategies for the actual filters
        Set<PmfmDTO> allPmfms = getParentContainer(ExtractionUI.class).getHandler().getPmfmsForSelectedExtraction(programCodes, periods);

        boolean valid = parameter.getPmfmPresets().stream().map(PmfmPresetDTO::getPmfm).allMatch(allPmfms::contains)
                && allPmfms.containsAll(parameter.getPmfmResults());

        if (!valid) {
            if (getContext().getDialogHelper().showConfirmDialog(
                    t("dali.extraction.filters.invalidParameter.message"),
                    t("dali.extraction.filters.title"),
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

                // Reset current parameter and continue
                getModel().getSelectedExtraction().setParameter(null);
                getParentContainer(ExtractionUI.class).getConfigUI().getHandler().loadConfig();
                return true;
            } else {

                // User cancel
                return false;
            }
        } else {

            // Pmfms config is valid
            return true;
        }

    }

    private List<FilterTypeDTO> getFilterTypes() {
        if (getModel().getFilterTypes() == null) {
            getModel().setFilterTypes(getContext().getExtractionService().getFilterTypes());
        }
        return getModel().getFilterTypes();
    }

    private class FilterLoader extends SwingWorker<List<ExtractionFiltersRowModel>, Void> {

        @Override
        protected List<ExtractionFiltersRowModel> doInBackground() {
            List<ExtractionFiltersRowModel> rows = Lists.newArrayList();
            Map<Integer, FilterDTO> filtersByFilterTypeId = null;
            if (getModel().getSelectedExtraction() != null) {
                // load filters
                getContext().getExtractionService().loadFilteredElements(getModel().getSelectedExtraction());
                // split by filter type id
                filtersByFilterTypeId = DaliBeans.mapByProperty(getModel().getSelectedExtraction().getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID);
            }

            for (FilterTypeDTO filterType : getFilterTypes()) {
                ExtractionFiltersRowModel rowModel = getTableModel().createNewRow();
                rowModel.setValid(true);
                rowModel.setFilterType(filterType);

                if (filtersByFilterTypeId != null) {
                    rowModel.setFilter(filtersByFilterTypeId.get(filterType.getId()));
                }

                rows.add(rowModel);
            }

            return rows;
        }

        @Override
        protected void done() {
            try {
                if (isCancelled()) {
                    return;
                }
                List<ExtractionFiltersRowModel> rows = get();
                getModel().setLoading(true);
                getTable().setVisibleRowCount(rows.size());
                getModel().setRows(rows);
                recomputeRowsValidState();
                if (rowsHaveFilteredElements(rows)) {
                    getModel().firePropertyChanged(ExtractionFiltersUIModel.PROPERTY_VALID, null, getModel().isValid());
                }

            } catch (InterruptedException | ExecutionException e) {
                throw new DaliTechnicalException(e.getMessage(), e);
            } finally {
                getModel().setLoading(false);
            }
        }

        private boolean rowsHaveFilteredElements(List<ExtractionFiltersRowModel> rows) {
            for (ExtractionFiltersRowModel row : rows) {
                if (!row.isFilterEmpty()) return true;
            }
            return false;
        }
    }
}
