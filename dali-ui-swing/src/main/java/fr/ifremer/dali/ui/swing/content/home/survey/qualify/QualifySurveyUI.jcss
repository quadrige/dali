/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
#qualifySurveyUI {
	modal: true;
	title: "dali.action.qualify.survey.title";
	resizable: false;
}

#commentLabel {
    text: "dali.action.qualify.survey.askForComment";
    labelFor: {commentEditor};
}

#commentEditor {
    text: {model.getComment()};
}

#qualityLevelLabel {
    text: "dali.property.qualityFlag";
    labelFor: {qualityLevelCombo};
}

#qualityLevelCombo {
	showReset: false;
	filterable: true;
	bean: {model};
	showDecorator: false;
    property: qualityLevel;
    selectedItem: {model.getQualityLevel()};
}

#confirmationLabel {
    text: "dali.action.qualify.survey.message";
}

#qualifyButton {
	actionIcon: accept;
	text: "dali.common.qualify";
	toolTipText: "dali.common.qualify";
	i18nMnemonic: "dali.common.qualify.mnemonic";
	enabled: {model.isValid()};
}

#cancelButton {
	actionIcon: cancel;
	text: "dali.common.cancel";
	toolTipText: "dali.common.cancel";
	i18nMnemonic: "dali.common.cancel.mnemonic";
}
