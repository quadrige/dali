package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.ErrorAware;
import fr.ifremer.dali.dto.ErrorDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.MethodDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>MethodsTableRowModel class.</p>
 *
 * @author Antoine
 */
public class MethodsTableRowModel extends AbstractDaliRowUIModel<MethodDTO, MethodsTableRowModel> implements MethodDTO, ErrorAware {

    private static final Binder<MethodDTO, MethodsTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(MethodDTO.class, MethodsTableRowModel.class);

    private static final Binder<MethodsTableRowModel, MethodDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(MethodsTableRowModel.class, MethodDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for MethodsTableRowModel.</p>
     */
    public MethodsTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    protected MethodDTO newBean() {
        return DaliBeanFactory.newMethodDTO();
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String mnemonique) {
        delegateObject.setName(mnemonique);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    /** {@inheritDoc} */
    @Override
    public String getReference() {
        return delegateObject.getReference();
    }

    /** {@inheritDoc} */
    @Override
    public void setReference(String reference) {
        delegateObject.setReference(reference);
    }

    /** {@inheritDoc} */
    @Override
    public String getNumber() {
        return delegateObject.getNumber();
    }

    /** {@inheritDoc} */
    @Override
    public void setNumber(String number) {
        delegateObject.setNumber(number);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescriptionPackaging() {
        return delegateObject.getDescriptionPackaging();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescriptionPackaging(String descriptionPackaging) {
        delegateObject.setDescriptionPackaging(descriptionPackaging);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescriptionPreparation() {
        return delegateObject.getDescriptionPreparation();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescriptionPreparation(String descriptionPreparation) {
        delegateObject.setDescriptionPreparation(descriptionPreparation);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescriptionPreservation() {
        return delegateObject.getDescriptionPreservation();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescriptionPreservation(String descriptionPreservation) {
        delegateObject.setDescriptionPreservation(descriptionPreservation);
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }
}
