package fr.ifremer.dali.ui.swing.content.synchro.changes;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.quadrige3.ui.swing.table.renderer.IconCellRenderer;
import org.nuiton.i18n.I18n;

import javax.swing.Icon;

/**
 * Renderer for operation type icon
 */
public class SynchroOperationTypeIconCellRenderer extends IconCellRenderer<String> {

    // Reserved I18n keys
    static {
        I18n.n("dali.property.synchro.operationType.INSERT");
        I18n.n("dali.property.synchro.operationType.UPDATE");
        I18n.n("dali.property.synchro.operationType.DELETE");
    }

    /**
	 * Le contexte.
	 */
	private DaliUIContext contexte;
	
	/**
	 * Constructor.
	 *
	 * @param contexte a {@link DaliUIContext} object.
	 */
	public SynchroOperationTypeIconCellRenderer(final DaliUIContext contexte) {
		setContexte(contexte);
	}

    /** {@inheritDoc} */
    @Override
    protected Icon getIcon(String operationType) {
        if (operationType == null) {
            return null;
        }
        return getContexte().getObjectStatusIcon("synchro-" + operationType.toLowerCase(), null);
    }
    
    /** {@inheritDoc} */
    @Override
    protected String getToolTipText(String operationType) {
        if (operationType == null) {
            return null;
        }
        return I18n.t("dali.property.synchro.operationType." + operationType);
    }

	/** {@inheritDoc} */
	@Override
	protected String getText(String operationType) {
        // No text (icon only)
		return null;
	}

	private DaliUIContext getContexte() {
		return contexte;
	}

	private void setContexte(DaliUIContext contexte) {
		this.contexte = contexte;
	}
}
