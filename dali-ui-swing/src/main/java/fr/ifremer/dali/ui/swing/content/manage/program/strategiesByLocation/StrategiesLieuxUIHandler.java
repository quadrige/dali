package fr.ifremer.dali.ui.swing.content.manage.program.strategiesByLocation;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.ui.swing.action.QuitScreenAction;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

/**
 * Controlleur pour l'onglet prelevements mesures.
 */
public class StrategiesLieuxUIHandler extends AbstractDaliUIHandler<StrategiesLieuxUIModel, StrategiesLieuxUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(StrategiesLieuxUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(StrategiesLieuxUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        StrategiesLieuxUIModel model = new StrategiesLieuxUIModel();
        ui.setContextValue(model);

        ui.setContextValue(SwingUtil.createActionIcon("program"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(StrategiesLieuxUI ui) {
        initUI(ui);

        getModel().setTableUIModel(getUI().getStrategiesProgrammeTableUI().getModel());
        listenModelModify(getModel().getTableUIModel());
        getModel().getTableUIModel().addPropertyChangeListener(AbstractDaliTableUIModel.PROPERTY_VALID, evt -> getValidator().doValidate());

        listenValidatorValid(getValidator(), getModel());
        registerValidators(getValidator());

        // Load les tableaux
        load();
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<StrategiesLieuxUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * Load le tableau.
     */
    public void load() {
    	
    	// Le programme
    	final ProgramDTO programme = getContext().getProgramStrategyService().getReadableProgramByCode(getContext().getSelectProgramCode());
    	if (programme != null) {
    		
    		// Chargement des lieux
    		getUI().getLieuxProgrammeTableUI().getHandler().load(programme);
    	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

}
