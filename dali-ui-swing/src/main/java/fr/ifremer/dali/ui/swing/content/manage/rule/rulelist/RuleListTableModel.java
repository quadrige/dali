package fr.ifremer.dali.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.MonthDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class RuleListTableModel extends AbstractDaliTableModel<RuleListRowModel> {

	/** Constant <code>CODE</code> */
	public static final DaliColumnIdentifier<RuleListRowModel> CODE = DaliColumnIdentifier.newId(
			RuleListRowModel.PROPERTY_CODE,
			n("dali.property.code"),
			n("dali.property.code"),
			String.class,
            true);
	
	/** Constant <code>ACTIVE</code> */
	public static final DaliColumnIdentifier<RuleListRowModel> ACTIVE = DaliColumnIdentifier.newId(
			RuleListRowModel.PROPERTY_ACTIVE,
			n("dali.rule.ruleList.active.short"),
			n("dali.rule.ruleList.active.tip"),
			Boolean.class,
            false);
	
	/** Constant <code>START_MONTH</code> */
	public static final DaliColumnIdentifier<RuleListRowModel> START_MONTH = DaliColumnIdentifier.newId(
			RuleListRowModel.PROPERTY_START_MONTH,
			n("dali.rule.ruleList.startMonth.short"),
			n("dali.rule.ruleList.startMonth.tip"),
			MonthDTO.class);
	
	/** Constant <code>END_MONTH</code> */
	public static final DaliColumnIdentifier<RuleListRowModel> END_MONTH = DaliColumnIdentifier.newId(
			RuleListRowModel.PROPERTY_END_MONTH,
			n("dali.rule.ruleList.endMonth.short"),
			n("dali.rule.ruleList.endMonth.tip"),
			MonthDTO.class);
	
	/** Constant <code>DESCRIPTION</code> */
	public static final DaliColumnIdentifier<RuleListRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
			RuleListRowModel.PROPERTY_DESCRIPTION,
			n("dali.property.description"),
			n("dali.rule.ruleList.description.tip"),
			String.class,
            true);

	public static final DaliColumnIdentifier<RuleListRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
		RuleListRowModel.PROPERTY_CREATION_DATE,
		n("dali.property.date.creation"),
		n("dali.property.date.creation"),
		Date.class);

	public static final DaliColumnIdentifier<RuleListRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
		RuleListRowModel.PROPERTY_UPDATE_DATE,
		n("dali.property.date.modification"),
		n("dali.property.date.modification"),
		Date.class);


	/**
	 * <p>Constructor for RuleListTableModel.</p>
	 *
	 */
	public RuleListTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public RuleListRowModel createNewRow() {
		return new RuleListRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<RuleListRowModel> getFirstColumnEditing() {
		return START_MONTH;
	}
}
