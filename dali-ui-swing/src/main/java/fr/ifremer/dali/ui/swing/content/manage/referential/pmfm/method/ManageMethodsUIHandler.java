package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import jaxx.runtime.SwingUtil;

/**
 * Controlleur pour la gestion des Methods
 */
public class ManageMethodsUIHandler extends AbstractDaliUIHandler<ManageMethodsUIModel, ManageMethodsUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageMethodsUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageMethodsUIModel model = new ManageMethodsUIModel();
        ui.setContextValue(model);

        ui.setContextValue(SwingUtil.createActionIcon("config"));

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ManageMethodsUI ui) {
        initUI(ui);

        // Initialiser les parametres des ecrans Observation et prelevemnts
        getContext().clearObservationIds();

    }

}
