package fr.ifremer.dali.ui.swing.util.table.renderer;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 24/01/2020.
 */
public class MultipleValueCellRenderer implements TableCellRenderer {

    private final TableCellRenderer delegate;
    private final JLabel multiValueLabel;

    public MultipleValueCellRenderer(TableCellRenderer delegate) {
        this.delegate = delegate;
        multiValueLabel = createMultiValueLabel();
    }

    private JLabel createMultiValueLabel() {
        JLabel label = new JLabel(t("dali.measurement.grouped.multiEdit.multiValueLabel"));
        label.setOpaque(true);
        return label;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return value == null
            ? multiValueLabel
            : delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}
