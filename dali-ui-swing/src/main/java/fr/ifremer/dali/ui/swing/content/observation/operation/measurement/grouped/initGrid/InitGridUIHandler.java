package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import jaxx.runtime.validator.swing.SwingValidator;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>InitGridUIHandler class.</p>
 */
public class InitGridUIHandler extends AbstractDaliTableUIHandler<InitGridRowModel, InitGridUIModel, InitGridUI> implements Cancelable {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(InitGridUI ui) {
        super.beforeInit(ui);

        InitGridUIModel model = new InitGridUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(InitGridUI ui) {
        initUI(ui);

        initTable();

        initBeanList(ui.getValueDoubleList(), null, null);
        ui.getValueDoubleList().getHandler().addAdditionalControls();

        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

    }

    @Override
    public SwingValidator<InitGridUIModel> getValidator() {
        return getUI().getValidator();
    }

    private void initTable() {

        addColumn(InitGridTableModel.PMFM);

        TableColumnExt valuesCol = addColumn(InitGridTableModel.QUALITATIVE_VALUES);
        fixColumnWidth(valuesCol, 50);

        InitGridTableModel tableModel = new InitGridTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        initTable(getTable(), true);

        getTable().setHorizontalScrollEnabled(false);
        getTable().setVisibleRowCount(6);
        getTable().setEditable(false);
    }

    private void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {
                case InitGridUIModel.PROPERTY_AVAILABLE_PMFMS:

                    loadTable();
                    break;

                case InitGridUIModel.PROPERTY_PRESET:

                    // preset is loaded but only ids, so load the nested beans
                    buildRowsWithPreset();
                    break;

                case InitGridUIModel.PROPERTY_SINGLE_ROW_SELECTED:

                    updateDoubleList();
                    break;

                case InitGridUIModel.PROPERTY_SELECTED_VALUES:

                    if (getModel().getSingleSelectedRow() != null) {
                        // update row values
                        getModel().getSingleSelectedRow().setQualitativeValues(
                                new ArrayList<>(getModel().getSelectedValues()));
                    }
                    break;
            }
        });
    }

    private void loadTable() {

        getModel().setLoading(true);
        List<InitGridRowModel> rows = Lists.newArrayList();

        for (PmfmDTO pmfm : getModel().getAvailablePmfms()) {
            InitGridRowModel row = getTableModel().createNewRow();
            row.setPmfm(pmfm);
            rows.add(row);
        }

        getModel().setRows(rows);
        recomputeRowsValidState();
        getModel().setLoading(false);
    }

    private void buildRowsWithPreset() {

        if (getModel().getRowCount() == 0) return; // should not happens

        getModel().setLoading(true);
        ReferentialService service = getContext().getReferentialService();

        for (Integer pmfmId : getModel().getPreset().getPmfmIds()) {
            InitGridRowModel row = DaliBeans.findByProperty(getModel().getRows(), InitGridRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_ID, pmfmId);
            if (row == null) continue;
            row.setQualitativeValues(service.getQualitativeValues(getModel().getPreset().getQualitativeValueIds(pmfmId)));
        }

        recomputeRowsValidState();
        getModel().setLoading(false);
    }

    private void updateDoubleList() {

        List<QualitativeValueDTO> universeList = null;
        List<QualitativeValueDTO> selectedList = null;

        if (getModel().getSingleSelectedRow() != null) {

            universeList = getModel().getSingleSelectedRow().getPmfm() == null
                    ? null
                    : getModel().getSingleSelectedRow().getPmfm().getQualitativeValues();
            selectedList = getModel().getSingleSelectedRow().getQualitativeValues();

        }

        getUI().getValueDoubleList().getHandler().setUniverse(universeList);
        getUI().getValueDoubleList().getHandler().setSelected(selectedList);
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        if (getModel().isValid()) {

            // Build the preset from rows
            buildPresetWithRows();

            // Save to preset file
            savePreset();

            closeDialog();
        }
    }

    private void buildPresetWithRows() {

        getModel().getPreset().clearPmfmPresets();

        for (InitGridRowModel row: getModel().getRows()) {

            getModel().getPreset().addPmfmPreset(
                    row.getPmfm().getId(),
                    DaliBeans.collectIds(row.getQualitativeValues())
            );
        }

    }

    private void savePreset() {

        PresetFileLoader.save(getModel().getPreset());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel() {
        stopListenValidatorValid(getValidator());
        getModel().setValid(false);
        closeDialog();
    }

    @Override
    public AbstractTableModel<InitGridRowModel> getTableModel() {
        return (InitGridTableModel) getTable().getModel();
    }

    @Override
    public SwingTable getTable() {
        return getUI().getPmfmTable();
    }

}
