package fr.ifremer.dali.ui.swing.content.extraction;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.dali.dto.configuration.context.ContextDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.ui.swing.content.extraction.config.ExtractionConfigUIModel;
import fr.ifremer.dali.ui.swing.content.extraction.filters.ExtractionFiltersUIModel;
import fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsRowModel;
import fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsTableUIModel;

/**
 * Home model.
 */
public class ExtractionUIModel extends AbstractEmptyUIModel<ExtractionUIModel> {

    /** Constant <code>PROPERTY_PROGRAM="program"</code> */
    public static final String PROPERTY_PROGRAM = "program";
    /** Constant <code>PROPERTY_CONTEXT="context"</code> */
    public static final String PROPERTY_CONTEXT = "context";
    private ProgramDTO program;
    private ExtractionDTO extraction;
    private ExtractionsTableUIModel extractionsTableUIModel;
    private ExtractionFiltersUIModel extractionFiltersUIModel;
    private ExtractionConfigUIModel extractionConfigUIModel;
    private ContextDTO context;

    /** {@inheritDoc} */
    @Override
    public boolean isModify() {
        return super.isModify();
    }

    /** {@inheritDoc} */
    @Override
    public void setModify(boolean modify) {
        super.setModify(modify);
    }

    /**
     * <p>Getter for the field <code>program</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public ProgramDTO getProgram() {
        return program;
    }

    /**
     * <p>Setter for the field <code>program</code>.</p>
     *
     * @param program a {@link fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public void setProgram(ProgramDTO program) {
        ProgramDTO oldValue = getProgram();
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, oldValue, program);
    }

    /**
     * <p>getProgramCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProgramCode() {
        return getProgram() == null ? null : getProgram().getCode();
    }

    /**
     * <p>Getter for the field <code>extraction</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.system.extraction.ExtractionDTO} object.
     */
    public ExtractionDTO getExtraction() {
        return extraction;
    }

    /**
     * <p>Setter for the field <code>extraction</code>.</p>
     *
     * @param extraction a {@link fr.ifremer.dali.dto.system.extraction.ExtractionDTO} object.
     */
    public void setExtraction(ExtractionDTO extraction) {
        this.extraction = extraction;
    }

    /**
     * <p>getExtractionId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getExtractionId() {
        return extraction == null ? null : extraction.getId();
    }

    /**
     * <p>Getter for the field <code>selectedExtraction</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsRowModel} object.
     */
    public ExtractionsRowModel getSelectedExtraction() {
        return getExtractionsTableUIModel().getSingleSelectedRow();
    }

    /**
     * <p>Getter for the field <code>extractionsTableUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsTableUIModel} object.
     */
    public ExtractionsTableUIModel getExtractionsTableUIModel() {
        return extractionsTableUIModel;
    }

    /**
     * <p>Setter for the field <code>extractionsTableUIModel</code>.</p>
     *
     * @param extractionsTableUIModel a {@link fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsTableUIModel} object.
     */
    public void setExtractionsTableUIModel(ExtractionsTableUIModel extractionsTableUIModel) {
        this.extractionsTableUIModel = extractionsTableUIModel;
        extractionsTableUIModel.setExtractionUIModel(this);
    }

    /**
     * <p>Getter for the field <code>extractionFiltersUIModel</code>.</p>
     *
     * @return a {@link ExtractionFiltersUIModel} object.
     */
    public ExtractionFiltersUIModel getExtractionFiltersUIModel() {
        return extractionFiltersUIModel;
    }

    /**
     * <p>Setter for the field <code>extractionFiltersUIModel</code>.</p>
     *
     * @param extractionFiltersUIModel a {@link ExtractionFiltersUIModel} object.
     */
    public void setExtractionFiltersUIModel(ExtractionFiltersUIModel extractionFiltersUIModel) {
        this.extractionFiltersUIModel = extractionFiltersUIModel;
        extractionFiltersUIModel.setExtractionUIModel(this);
    }

    public ExtractionConfigUIModel getExtractionConfigUIModel() {
        return extractionConfigUIModel;
    }

    public void setExtractionConfigUIModel(ExtractionConfigUIModel extractionConfigUIModel) {
        this.extractionConfigUIModel = extractionConfigUIModel;
        extractionConfigUIModel.setExtractionUIModel(this);
    }

    /**
     * <p>Getter for the field <code>context</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    public ContextDTO getContext() {
        return context;
    }

    /**
     * <p>Setter for the field <code>context</code>.</p>
     *
     * @param context a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    public void setContext(ContextDTO context) {
        ContextDTO oldContext = getContext();
        this.context = context;
        firePropertyChange(PROPERTY_CONTEXT, oldContext, context);
    }
}
