package fr.ifremer.dali.ui.swing.content.extraction.list;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.service.DaliBusinessException;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Abstract extract action
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public abstract class AbstractInternalExtractAction extends AbstractExtractAction {

    private static final Log LOG = LogFactory.getLog(AbstractInternalExtractAction.class);

    /**
     * Constructor.
     *
     * @param handler Handler
     */
    protected AbstractInternalExtractAction(ExtractionsTableUIHandler handler) {
        super(handler);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getOutputType().isExternal()) {
            LOG.error("wrong ExtractionOutputType (should be not external");
            return false;
        }

        // Compute default output file
        String date = DateUtil.formatDate(new Date(), "yyyy-MM-dd-HHmmss");
        String fileName = String.format("%s-%s-%s-%s",
                t("dali.service.extraction.file.prefix"),
                getOutputType().getLabel(),
                getSelectedExtraction().getName(),
                date);
        String fileExtension = getConfig().getExtractionResultFileExtension();

        // ask user file where to export db
        setOutputFile(saveFile(
                fileName,
                fileExtension,
                t("dali.extraction.choose.file.title"),
                t("dali.extraction.choose.file.buttonLabel"),
                "^.*\\." + fileExtension,
                t("dali.common.file." + fileExtension)));

        return getOutputFile() != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        // Will check data updates
        super.doAction();

        // Try open a stream to verify file availability
        try {
            OutputStream out = FileUtils.openOutputStream(getOutputFile());
            IOUtils.closeQuietly(out);
        } catch (IOException e) {
            throw new DaliBusinessException(e.getLocalizedMessage());
        }

        // Launch extraction
        performExtraction();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {

        String text = t("dali.action.extract.done",
                decorate(Objects.requireNonNull(getSelectedExtraction())),
                getOutputType().getLabel(),
                getOutputFile().getAbsoluteFile().toURI(),
                getOutputFile().getAbsolutePath());

        JEditorPane editorPane = new JEditorPane("text/html", text);
        editorPane.setEditable(false);
        editorPane.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();
                if (LOG.isInfoEnabled()) {
                    LOG.info("open url: " + url);
                }
                ApplicationUIUtil.openLink(url);
            }
        });

        getContext().getDialogHelper().showOptionDialog(
                getUI(),
                editorPane,
                t("dali.action.extract.title", getOutputType().getLabel()),
                JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);

        super.postSuccessAction();
    }

    @Override
    public void postFailedAction(Throwable error) {

        // Delete empty file (Mantis #40285)
        if (getOutputFile() != null && getOutputFile().exists() && getOutputFile().length() == 0) {
            getOutputFile().delete();
        }

        super.postFailedAction(error);
    }

}
