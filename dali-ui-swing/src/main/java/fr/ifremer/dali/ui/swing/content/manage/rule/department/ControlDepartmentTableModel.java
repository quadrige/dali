package fr.ifremer.dali.ui.swing.content.manage.rule.department;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlDepartmentTableModel extends AbstractDaliTableModel<ControlDepartmentTableRowModel> {

	/** Constant <code>CODE</code> */
	public static final DaliColumnIdentifier<ControlDepartmentTableRowModel> CODE = DaliColumnIdentifier.newId(
			ControlDepartmentTableRowModel.PROPERTY_CODE,
			n("dali.property.code"),
			n("dali.rule.department.code.tip"),
			String.class);
	
	/** Constant <code>NAME</code> */
	public static final DaliColumnIdentifier<ControlDepartmentTableRowModel> NAME = DaliColumnIdentifier.newId(
			ControlDepartmentTableRowModel.PROPERTY_NAME,
			n("dali.property.name"),
			n("dali.rule.department.name.tip"),
			String.class);
	
	/**
	 * <p>Constructor for ControlDepartmentTableModel.</p>
	 *
	 */
	public ControlDepartmentTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ControlDepartmentTableRowModel createNewRow() {
		return new ControlDepartmentTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ControlDepartmentTableRowModel> getFirstColumnEditing() {
		return CODE;
	}
}
