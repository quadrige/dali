package fr.ifremer.dali.ui.swing.content.home.survey;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.content.home.HomeUI;
import fr.ifremer.dali.ui.swing.content.home.HomeUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.renderer.StateIconCellRenderer;
import fr.ifremer.dali.ui.swing.util.table.renderer.SynchronizationStatusIconCellRenderer;
import fr.ifremer.quadrige3.ui.swing.component.coordinate.CoordinateEditor;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.LocalTimeCellEditor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;
import javax.swing.SortOrder;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des observations.
 */
public class SurveysTableUIHandler extends
        AbstractDaliTableUIHandler<SurveysTableRowModel, SurveysTableUIModel, SurveysTableUI> {

    private ExtendedComboBoxCellEditor<ProgramDTO> programCellEditor;
    private ExtendedComboBoxCellEditor<CampaignDTO> campaignCellEditor;
    private ExtendedComboBoxCellEditor<LocationDTO> locationCellEditor;
    private PropertyChangeListener authenticationListener;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{
                SurveysTableRowModel.PROPERTY_SAMPLING_OPERATIONS,
                SurveysTableRowModel.PROPERTY_SAMPLING_OPERATIONS_LOADED,
                SurveysTableRowModel.PROPERTY_DIRTY,
                SurveysTableRowModel.PROPERTY_ERRORS,
                SurveysTableRowModel.PROPERTY_CONTROL_DATE,
                SurveysTableRowModel.PROPERTY_VALIDATION_DATE,
                SurveysTableRowModel.PROPERTY_QUALIFICATION_COMMENT,
                SurveysTableRowModel.PROPERTY_SYNCHRONIZATION_STATUS,
                SurveysTableRowModel.PROPERTY_COORDINATE
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final SurveysTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SurveysTableUIModel model = new SurveysTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final SurveysTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Désactivation des boutons
        getUI().getDuplicateButton().setEnabled(false);
        getUI().getEditCombo().setEnabled(false);
        getUI().getStateCombo().setEnabled(false);
        getUI().getDeleteButton().setEnabled(false);

        enablePrivilegedControls();

        createProgramCellEditor();
        createCampaignCellEditor();
        createLocationCellEditor();

        // Initialisation du tableau
        initTable();

        // Initialisation de la combobox Editer
        initActionComboBox(getUI().getEditCombo());

        // Intialisation de la combobox Changer etat
        initActionComboBox(getUI().getStateCombo());

        // Initialisation des listeners
        initListeners();

        // button border
        getUI().getNextButton().setBorder(
                BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), ui.getNextButton().getBorder())
        );

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(final SurveysTableRowModel row) {

        return (super.isRowValid(row) || !row.isEditable()) && isSurveyRowValid(row);
    }

    private boolean isSurveyRowValid(SurveysTableRowModel row) {

        // Name length
        if (StringUtils.length(row.getName()) > 50) {
            DaliBeans.addError(row, t("dali.validator.error.mnemonic.too.long.50"), SurveysTableRowModel.PROPERTY_NAME);
        }

        // coordinates
        if (row.getLatitudeMin() == null ^ row.getLongitudeMin() == null) {
            DaliBeans.addError(row, t("dali.validator.error.coordinate.invalid"), SurveysTableRowModel.PROPERTY_LATITUDE_MIN, SurveysTableRowModel.PROPERTY_LONGITUDE_MIN);
        }
        if (row.getLatitudeMax() == null ^ row.getLongitudeMax() == null) {
            DaliBeans.addError(row, t("dali.validator.error.coordinate.invalid"), SurveysTableRowModel.PROPERTY_LATITUDE_MAX, SurveysTableRowModel.PROPERTY_LONGITUDE_MAX);
        }
        if (row.getLatitudeMin() == null && row.getLatitudeMax() != null) {
            DaliBeans.addError(row, t("dali.validator.error.coordinate.invalid"), SurveysTableRowModel.PROPERTY_LATITUDE_MIN, SurveysTableRowModel.PROPERTY_LATITUDE_MAX);
        }
        if (row.getLongitudeMin() == null && row.getLongitudeMax() != null) {
            DaliBeans.addError(row, t("dali.validator.error.coordinate.invalid"), SurveysTableRowModel.PROPERTY_LONGITUDE_MIN, SurveysTableRowModel.PROPERTY_LONGITUDE_MAX);
        }

        // positioning
        if (row.getLatitudeMin() != null && row.getLongitudeMin() != null && row.getPositioning() == null) {
            DaliBeans.addError(row, t("dali.validator.error.positioning.required"), SurveysTableRowModel.PROPERTY_POSITIONING);
        }

        // dummy error if operations invalid
        if (!row.isOperationsValid()) {
            DaliBeans.addError(row, t("dali.home.survey.table.operationsInvalid"));
        }

        boolean hasNoError = DaliBeans.hasNoBlockingError(row);

        if (!hasNoError) {
            ensureColumnsWithErrorAreVisible(row);
        }

        return hasNoError;
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Program cell editor
        TableColumnExt programCol = addColumn(
                programCellEditor,
                newTableCellRender(SurveysTableModel.PROGRAM),
                SurveysTableModel.PROGRAM);
        programCol.setSortable(true);
        programCol.setPreferredWidth(200);

        // Campaign
        TableColumnExt campaignCol = addColumn(
                campaignCellEditor,
                newTableCellRender(SurveysTableModel.CAMPAIGN),
                SurveysTableModel.CAMPAIGN);
        campaignCol.setSortable(true);
        campaignCol.setPreferredWidth(200);

        // lieu
        TableColumnExt locationCol = addColumn(
                locationCellEditor,
                newTableCellRender(SurveysTableModel.LOCATION),
                SurveysTableModel.LOCATION);
        locationCol.setSortable(true);
        locationCol.setPreferredWidth(200);

        // Date
        TableColumnExt dateCol = addLocalDatePickerColumnToModel(SurveysTableModel.DATE, getConfig().getDateFormat());
        dateCol.setSortable(true);
        dateCol.setPreferredWidth(50);

        // mnemonique
        TableColumnExt nameCol = addColumn(SurveysTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setPreferredWidth(100);

        // Commentaire
        addCommentColumn(SurveysTableModel.COMMENT);

        // Partage
        TableColumnExt synchroStatusCol = addColumn(
                null,
                new SynchronizationStatusIconCellRenderer(getContext()),
                SurveysTableModel.SYNCHRONIZATION_STATUS);
        synchroStatusCol.setSortable(true);
        synchroStatusCol.setPreferredWidth(20);

        // Etat
        TableColumnExt stateCol = addColumn(
                null,
                new StateIconCellRenderer(getContext()),
                SurveysTableModel.STATE);
        stateCol.setSortable(true);
        stateCol.setPreferredWidth(100);

        // Profondeur precise
        TableColumnExt bottomDepthCol = addColumn(SurveysTableModel.BOTTOM_DEPTH);
        bottomDepthCol.setCellEditor(newNumberCellEditor(Double.class, false, DaliUI.DECIMAL2_PATTERN));
        bottomDepthCol.setCellRenderer(newNumberCellRenderer(2));
        bottomDepthCol.setSortable(true);
        bottomDepthCol.setPreferredWidth(100);

        // Latitude Min
        TableColumnExt latitudeMinCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MIN,
                SurveysTableModel.LATITUDE_MIN);
        latitudeMinCol.setSortable(true);
        latitudeMinCol.setPreferredWidth(100);

        // longitude Min
        TableColumnExt longitudeMinCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MIN,
                SurveysTableModel.LONGITUDE_MIN);
        longitudeMinCol.setSortable(true);
        longitudeMinCol.setPreferredWidth(100);

        // Latitude Max
        TableColumnExt latitudeMaxCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MAX,
                SurveysTableModel.LATITUDE_MAX);
        latitudeMaxCol.setSortable(true);
        latitudeMaxCol.setPreferredWidth(100);

        // longitude Max
        TableColumnExt longitudeMaxCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MAX,
                SurveysTableModel.LONGITUDE_MAX);
        longitudeMaxCol.setSortable(true);
        longitudeMaxCol.setPreferredWidth(100);

        // positionnement
        TableColumnExt positioningCol = addExtendedComboDataColumnToModel(
                SurveysTableModel.POSITIONING,
                getContext().getReferentialService().getPositioningSystems(),
                false);
        positioningCol.setSortable(true);
        positioningCol.setPreferredWidth(100);

        // positionnement
        TableColumnExt positioningPrecisionCol = addColumn(
                SurveysTableModel.POSITIONING_PRECISION);
        positioningPrecisionCol.setSortable(true);
        positioningPrecisionCol.setPreferredWidth(100);
        positioningPrecisionCol.setEditable(false);

        // Service Saisisseur
        TableColumnExt recorderDepartmentCol = addColumn(
                null, //departmentCellEditor,
                newTableCellRender(SurveysTableModel.RECORDER_DEPARTMENT),
                SurveysTableModel.RECORDER_DEPARTMENT
        );
        recorderDepartmentCol.setSortable(true);
        recorderDepartmentCol.setPreferredWidth(200);
        recorderDepartmentCol.setEditable(false);

        // Colonne Heure
        final TableColumnExt timeCol = addColumn(
                new LocalTimeCellEditor(),
                newTableCellRender(Integer.class, DecoratorService.TIME_IN_HOURS_MINUTES),
                SurveysTableModel.TIME);
        timeCol.setMaxWidth(100);
        timeCol.setWidth(100);
        timeCol.setSortable(true);

        // Date Modif
        TableColumnExt updateDateCol = addColumn(null, newDateCellRenderer(getConfig().getDateTimeFormat()), SurveysTableModel.UPDATE_DATE);
        updateDateCol.setSortable(true);
        updateDateCol.setPreferredWidth(50);

        // Date Control
        TableColumnExt controlDateCol = addColumn(null, newDateCellRenderer(getConfig().getDateTimeFormat()), SurveysTableModel.CONTROL_DATE);
        controlDateCol.setSortable(true);
        controlDateCol.setPreferredWidth(50);

        // Date Valid
        TableColumnExt validationDateCol = addColumn(null, newDateCellRenderer(getConfig().getDateTimeFormat()), SurveysTableModel.VALIDATION_DATE);
        validationDateCol.setSortable(true);
        validationDateCol.setPreferredWidth(50);

        // Commentaire Valid
        TableColumnExt validationCommentCol = addCommentColumn(SurveysTableModel.VALIDATION_COMMENT, SurveysTableRowModel.PROPERTY_VALIDATION_COMMENT, false);

        // Date Qualif
        TableColumnExt qualificationDateCol = addColumn(null, newDateCellRenderer(getConfig().getDateTimeFormat()), SurveysTableModel.QUALIFICATION_DATE);
        qualificationDateCol.setSortable(true);
        qualificationDateCol.setPreferredWidth(50);

        // Commentaire Qualif
        TableColumnExt qualificationCommentCol = addCommentColumn(SurveysTableModel.QUALIFICATION_COMMENT, SurveysTableRowModel.PROPERTY_QUALIFICATION_COMMENT, false);

        // Quality flag
        TableColumnExt qualityFlagCol = addColumn(SurveysTableModel.QUALITY_FLAG);
        qualityFlagCol.setPreferredWidth(50);

        SurveysTableModel tableModel = new SurveysTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Colonnes non editable
        tableModel.setNoneEditableCols(
                SurveysTableModel.SYNCHRONIZATION_STATUS,
                SurveysTableModel.STATE,
                SurveysTableModel.UPDATE_DATE,
                SurveysTableModel.CONTROL_DATE,
                SurveysTableModel.VALIDATION_DATE,
                SurveysTableModel.QUALIFICATION_DATE,
                SurveysTableModel.QUALITY_FLAG);

        // Les colonnes obligatoire sont toujours presentes
        dateCol.setHideable(false);
        programCol.setHideable(false);

        // Initialisation du tableau
        initTable(getTable());

        // Les colonnes optionnelles sont invisibles
        recorderDepartmentCol.setVisible(false);
        bottomDepthCol.setVisible(false);
        latitudeMinCol.setVisible(false);
        longitudeMinCol.setVisible(false);
        latitudeMaxCol.setVisible(false);
        longitudeMaxCol.setVisible(false);
        positioningCol.setVisible(false);
        positioningPrecisionCol.setVisible(false);
        timeCol.setVisible(false);
        updateDateCol.setVisible(false);
        controlDateCol.setVisible(false);
        validationDateCol.setVisible(false);
        validationCommentCol.setVisible(false);
        qualificationDateCol.setVisible(false);
        qualificationCommentCol.setVisible(false);
        qualityFlagCol.setVisible(false);

        // Tri par defaut
        getTable().setSortOrder(SurveysTableModel.LOCATION, SortOrder.ASCENDING);

        // border
        addEditionPanelBorder();

    }

    private void createProgramCellEditor() {

        programCellEditor = newExtendedComboBoxCellEditor(null, ProgramDTO.class, false);

        programCellEditor.setAction("unfilter", "dali.common.unfilter", e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter programs
            updateProgramCellEditor(getModel().getSingleSelectedRow(), true);
            getTable().requestFocus();
        });

    }

    private void updateProgramCellEditor(SurveysTableRowModel row, boolean forceNoFilter) {

        updateProgramCellEditor(row, forceNoFilter, true);
    }

    private void updateProgramCellEditor(SurveysTableRowModel row, boolean forceNoFilter, boolean canResetValue) {

        // Keep the forceNoFilter on this row (Mantis #46655)
        if (forceNoFilter) row.setForceNoProgramFilter(true);
        if (row.isForceNoProgramFilter()) forceNoFilter = true;

        programCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.PROGRAM));

        List<ProgramDTO> availablePrograms =
                getContext().getObservationService().getAvailablePrograms(
                        row.getLocation() == null ? null : row.getLocation().getId(),
                        row.getDate(), forceNoFilter, true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        if (canResetValue && row.isEditable() && row.getProgram() != null && !availablePrograms.contains(row.getProgram())) {
            row.setProgram(null);
        }
        programCellEditor.getCombo().setData(availablePrograms);

    }

    private void createCampaignCellEditor() {

        campaignCellEditor = newExtendedComboBoxCellEditor(null, CampaignDTO.class, false);

        campaignCellEditor.setAction("unfilter", "dali.common.unfilter", e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter campaigns
            updateCampaignCellEditor(getModel().getSingleSelectedRow(), true);
            getTable().requestFocus();
        });

    }

    private void updateCampaignCellEditor(SurveysTableRowModel row, boolean forceNoFilter) {

        updateCampaignCellEditor(row, forceNoFilter, true);
    }

    private void updateCampaignCellEditor(SurveysTableRowModel row, boolean forceNoFilter, boolean canResetValue) {

        // Keep the forceNoFilter on this row (Mantis #46655)
        if (forceNoFilter) row.setForceNoCampaignFilter(true);
        if (row.isForceNoCampaignFilter()) forceNoFilter = true;

        campaignCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.CAMPAIGN));

        List<CampaignDTO> availableCampaigns =
                getContext().getObservationService().getAvailableCampaigns(row.getDate(), forceNoFilter);

        // Avoid Campaign/Date incompatibility
        if (canResetValue && row.isEditable() && row.getCampaign() != null && !availableCampaigns.contains(row.getCampaign())) {
            row.setCampaign(null);
        }

        campaignCellEditor.getCombo().setData(availableCampaigns);

    }

    private void createLocationCellEditor() {

        locationCellEditor = newExtendedComboBoxCellEditor(null, LocationDTO.class, false);

        locationCellEditor.setAction("unfilter", "dali.common.unfilter", e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateLocationCellEditor(getModel().getSingleSelectedRow(), true);
            getTable().requestFocus();
        });

    }

    private void updateLocationCellEditor(SurveysTableRowModel row, boolean forceNoFilter) {

        updateLocationCellEditor(row, forceNoFilter, true);
    }

    private void updateLocationCellEditor(SurveysTableRowModel row, boolean forceNoFilter, boolean canResetValue) {

        // Keep the forceNoFilter on this row (Mantis #46655)
        if (forceNoFilter) row.setForceNoLocationFilter(true);
        if (row.isForceNoLocationFilter()) forceNoFilter = true;

        locationCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.LOCATION));

        List<LocationDTO> availableLocations =
                getContext().getObservationService().getAvailableLocations(
                        null, // TODO pas de filtrage sur campaign ? row.getCampaign() == null ? null : row.getCampaign().getId(),
                        row.getProgram() == null ? null : row.getProgram().getCode(),
                        forceNoFilter, true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        if (canResetValue && row.isEditable() && row.getLocation() != null && !availableLocations.contains(row.getLocation())) {
            row.setLocation(null);
        }
        locationCellEditor.getCombo().setData(availableLocations);
    }

    private void initListeners() {

        // listen to this JTable property
        getTable().addPropertyChangeListener("tableCellEditor", evt -> {
            if (evt.getNewValue() != null) {

                // a table edition begins
                final SurveysTableRowModel survey = getModel().getSingleSelectedRow();

                // update cell editors because its too long
                if (survey != null) {
                    updateProgramCellEditor(survey, false, false);
                    updateCampaignCellEditor(survey, false, false);
                    updateLocationCellEditor(survey, false, false);
                }

            }
        });

        // add special listener if the privileges of the authenticated user have changed (see Mantis #38841)
        authenticationListener = evt -> enablePrivilegedControls();
        // Change access to these function if privilege changed
        getContext().addPropertyChangeListener(DaliUIContext.PROPERTY_AUTHENTICATION_TOOLTIPTEXT, authenticationListener);
        getModel().addPropertyChangeListener(SurveysTableUIModel.PROPERTY_SELECTED_ROWS, authenticationListener);

        // propagate the 'sorting' property of the table to the map model
        getTable().addPropertyChangeListener(SwingTable.PROPERTY_SORTING, evt ->
                getModel().getMainUIModel().getMapUIModel().setBuildMapOnSelectionChanged(!getTable().isSorting()));
    }

    private void enablePrivilegedControls() {
        // enable or not these controls depending on user privileges
        DaliUIContext.Permissions permissions = getContext().getPermissions(getModel().getSelectedBeans());

        getUI().getNewButton().setEnabled(CollectionUtils.isNotEmpty(
            getContext().getObservationService().getAvailablePrograms(null, null, false, true)
        ));
        getUI().getDuplicateButton().setEnabled(getModel().getSelectedRows().size() == 1 && permissions.isProgramRecorder);
        getUI().getDeleteButton().setEnabled(!getModel().getSelectedRows().isEmpty() && permissions.isSurveyRecorder);
        getUI().getControlButton().setEnabled(permissions.isSurveyRecorder);
        getUI().getValidateButton().setEnabled(permissions.isValidator);
        getUI().getUnvalidateButton().setEnabled(permissions.isValidator);
        getUI().getQualifyButton().setEnabled(permissions.isQualifier);
    }

    @Override
    public void onCloseUI() {

        // remove special listener to prevent memory leaks (Mantis #48757)
        getContext().removePropertyChangeListener(DaliUIContext.PROPERTY_AUTHENTICATION_TOOLTIPTEXT, authenticationListener);

        super.onCloseUI();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, SurveysTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // Set row dirty first
        row.setDirty(true);

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        // TODO Pas de filtrage sur campaign ?
//        if (SurveysTableRowModel.PROPERTY_CAMPAIGN.equals(propertyName)) {
//
//            updateProgramCellEditor(row, false);
//            updateLocationCellEditor(row, false);
//
//        }

        // Listen to PROGRAM modification to load sampling operations table
        if (SurveysTableRowModel.PROPERTY_PROGRAM.equals(propertyName)) {

            updateLocationCellEditor(row, false);

            // use context variable to trigger PROPERTY_SELECTED_SURVEY event
            selectSurvey(row);
        }

        // Listen to LOCATION and DATE modification to load available programs and sampling operations table
        if (SurveysTableRowModel.PROPERTY_LOCATION.equals(propertyName) || SurveysTableRowModel.PROPERTY_DATE.equals(propertyName)) {

            updateProgramCellEditor(row, false);

            if (SurveysTableRowModel.PROPERTY_DATE.equals(propertyName)) {
                updateCampaignCellEditor(row, false);
            }

            // use context variable to trigger PROPERTY_SELECTED_SURVEY event
            selectSurvey(row);
        }

        // Listen to latitude or longitude to clear positioning when both are null
        if (SurveysTableRowModel.PROPERTY_LATITUDE_MIN.equals(propertyName)
                || SurveysTableRowModel.PROPERTY_LATITUDE_MAX.equals(propertyName)
                || SurveysTableRowModel.PROPERTY_LONGITUDE_MIN.equals(propertyName)
                || SurveysTableRowModel.PROPERTY_LONGITUDE_MAX.equals(propertyName)
                ) {

            if (row.getLatitudeMin() == null && row.getLongitudeMin() == null
                    && row.getLatitudeMax() == null && row.getLongitudeMax() == null) {
                row.setPositioning(null);
                getTable().repaint();
            }
        }

        forceRevalidateModel();
    }

    private void selectSurvey(SurveysTableRowModel survey) {

        getModel().getMainUIModel().setSelectedSurvey(survey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<SurveysTableRowModel> getTableModel() {
        return (SurveysTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getSurveysTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<SurveysTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        // should be only one row
        if (addedRows.size() == 1) {
            SurveysTableRowModel row = addedRows.get(0);

            // Ajout d'un mode de partage local pour une ligne ajoutee en local
            row.setSynchronizationStatus(getContext().getSystemService().getLocalShare());

            // Add actual department of current user
            Integer recorderDepId = getContext().getDataContext().getRecorderDepartmentId();
            if (recorderDepId != null) {
                row.setRecorderDepartment(getContext().getReferentialService().getDepartmentById(recorderDepId));
            }

            // Current user is recorder
            row.setCurrentUserIsRecorder(true);

            // Ajouter le focus sur la cellule de la ligne cree
            setFocusOnCell(row);
        }
    }

    /**
     * <p>onNext.</p>
     */
    public void onNext() {

        if (getTable().isEditing()) {
            getTable().getCellEditor().stopCellEditing();
        }

        getModel().getMainUIModel().getOperationsTableUIModel().addNewRow();

    }

    public HomeUIHandler getHomeUIHandler() {
        return getParentContainer(HomeUI.class).getHandler();
    }

    public void showMap() {
        getHomeUIHandler().showSurveysMap();
    }

    public void hideMap() {
        getHomeUIHandler().showSamplingOperations();
    }

    public void beforeLoad() {
        uninstallSaveTableStateListener();
    }

    public void afterLoad() {
        restoreTableState();
        installSaveTableStateListener();

        // Determine if current user is allowed to edit survey
        getModel().getRows().forEach(row -> {
            boolean userIsRecorder = getContext().getDataContext().isSurveyEditable(row);
            row.setCurrentUserIsRecorder(userIsRecorder);
        });

    }
}
