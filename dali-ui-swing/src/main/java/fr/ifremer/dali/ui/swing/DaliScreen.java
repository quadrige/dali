package fr.ifremer.dali.ui.swing;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.Screen;

/**
 * Enumeration of any internal screen of application.
 */
public class DaliScreen extends Screen {

    protected DaliScreen(String screenName) {
        super(screenName);
    }

    /**
     * Configuration.
     */
    public static final DaliScreen CONFIGURATION = new DaliScreen("CONFIGURATION");

    /**
     * Ecran observation general.
     */
    public static final DaliScreen OBSERVATION = new DaliScreen("OBSERVATION");

    /**
     * Onglet observation general.
     */
    public static final DaliScreen OBSERVATION_GENERAL = new DaliScreen("OBSERVATION_GENERAL");

    /**
     * Onglet prelevements mesures.
     */
    public static final DaliScreen OPERATION_MEASUREMENTS = new DaliScreen("OPERATION_MEASUREMENTS");

    /**
     * Onglet phtos.
     */
    public static final DaliScreen PHOTOS = new DaliScreen("PHOTOS");

    /**
     * Configurations des programmes.
     */
    public static final DaliScreen PROGRAM = new DaliScreen("PROGRAM");

    /**
     * Configurations des strategies et lieux.
     */
    public static final DaliScreen STRATEGY_LOCATION = new DaliScreen("STRATEGY_LOCATION");

    /**
     * Configurations des campagnes.
     */
    public static final DaliScreen CAMPAIGNS = new DaliScreen("CAMPAIGNS");

    /**
     * Configurations des regles.
     */
    public static final DaliScreen RULE_LIST = new DaliScreen("RULE_LIST");

    /**
     * Configurations des lieux de surveillance.
     */
    public static final DaliScreen LOCATION = new DaliScreen("LOCATION");

    /**
     * Configurations des groupes de taxons.
     */
    public static final DaliScreen TAXON_GROUP = new DaliScreen("TAXON_GROUP");

    /**
     * Configurations des taxons.
     */
    public static final DaliScreen TAXON = new DaliScreen("TAXON");

    /**
     * Users management
     */
    public static final DaliScreen USER = new DaliScreen("USER");

    /**
     * Departments management
     */
    public static final DaliScreen DEPARTMENT = new DaliScreen("DEPARTMENT");

    /**
     * PMFM parameters management
     */
    public static final DaliScreen PARAMETER = new DaliScreen("PARAMETER");

    /**
     * PMFM methods management
     */
    public static final DaliScreen METHOD = new DaliScreen("METHOD");

    /**
     * PMFM fractions management
     */
    public static final DaliScreen FRACTION = new DaliScreen("FRACTION");

    /**
     * PMFM matrices management
     */
    public static final DaliScreen MATRIX = new DaliScreen("MATRIX");

    /**
     * PMFM quadruplets management
     */
    public static final DaliScreen PMFM = new DaliScreen("PMFM");

    /**
     * Referentiel des unites
     */
    public static final DaliScreen UNIT = new DaliScreen("UNIT");

    /**
     * Referentiel des engins de prelevement
     */
    public static final DaliScreen SAMPLING_EQUIPMENT = new DaliScreen("SAMPLING_EQUIPMENT");

    /**
     * Referential Analysis Instruments
     */
    public static final DaliScreen ANALYSIS_INSTRUMENT = new DaliScreen("ANALYSIS_INSTRUMENT");

    /**
     * Configurations des contextes.
     */
    public static final DaliScreen CONTEXT = new DaliScreen("CONTEXT");

    /**
     * Configurations des filtres des lieux.
     */
    public static final DaliScreen FILTER_LOCATION = new DaliScreen("FILTER_LOCATION");

    /**
     * Configurations des filtres des programmes.
     */
    public static final DaliScreen FILTER_PROGRAM = new DaliScreen("FILTER_PROGRAM");

    /**
     * Configurations des filtres des campagnes.
     */
    public static final DaliScreen FILTER_CAMPAIGN = new DaliScreen("FILTER_CAMPAIGN");

    /**
     * Department filter.
     */
    public static final DaliScreen FILTER_DEPARTMENT = new DaliScreen("FILTER_DEPARTMENT");

    /**
     * Instrument filter.
     */
    public static final DaliScreen FILTER_ANALYSIS_INSTRUMENT = new DaliScreen("FILTER_ANALYSIS_INSTRUMENT");

    /**
     * Equipment filter.
     */
    public static final DaliScreen FILTER_SAMPLING_EQUIPMENT = new DaliScreen("FILTER_SAMPLING_EQUIPMENT");

    /**
     * Pmfm filter.
     */
    public static final DaliScreen FILTER_PMFM = new DaliScreen("FILTER_PMFM");

    /**
     * Taxon filter.
     */
    public static final DaliScreen FILTER_TAXON = new DaliScreen("FILTER_TAXON");

    /**
     * Taxon group filter.
     */
    public static final DaliScreen FILTER_TAXON_GROUP = new DaliScreen("FILTER_TAXON_GROUP");

    /**
     * User filter.
     */
    public static final DaliScreen FILTER_USER = new DaliScreen("FILTER_USER");

    /**
     * Ecran d'extration des données
     */
    public static final DaliScreen EXTRACTION = new DaliScreen("EXTRACTION");

}
