package fr.ifremer.dali.ui.swing.content.home.map;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.vividsolutions.jts.geom.Envelope;
import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.map.MapMode;
import fr.ifremer.dali.map.Maps;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.map.DataMapPane;
import fr.ifremer.dali.ui.swing.util.map.DataSelectionAdapter;
import fr.ifremer.dali.ui.swing.util.map.DataSelectionEvent;
import fr.ifremer.dali.ui.swing.util.map.layer.DataFeatureLayer;
import fr.ifremer.dali.ui.swing.util.map.layer.DataLayerCollection;
import fr.ifremer.dali.ui.swing.util.map.layer.InfoPanelLayer;
import fr.ifremer.dali.ui.swing.util.map.layer.LegendLayer;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.ui.swing.action.ActionFactory;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.referencing.operation.projection.ProjectionException;
import org.geotools.renderer.RenderListener;
import org.geotools.swing.event.MapMouseAdapter;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.event.MapPaneAdapter;
import org.geotools.swing.event.MapPaneEvent;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.opengis.feature.simple.SimpleFeature;

import javax.swing.SwingWorker;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des prelevements de l'ecran d'accueil
 */
public class SurveysMapUIHandler extends AbstractDaliUIHandler<SurveysMapUIModel, SurveysMapUI> implements Cancelable {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SurveysMapUIHandler.class);
    private ThreadPoolExecutor executor;
    private PropertyChangeListener mapListener;

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final SurveysMapUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SurveysMapUIModel model = new SurveysMapUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(SurveysMapUI ui) {

        initUI(ui);

//        executor = ActionFactory.createSingleThreadExecutor(ActionFactory.ExecutionMode.LATEST);

        SurveysMapBuilder mapBuilder = new SurveysMapBuilder(getConfig());
        ui.getMapPane().setMapBuilder(mapBuilder);
        boolean checkOnline = mapBuilder.checkOnlineReachable();
        ui.getOfflineButton().setEnabled(checkOnline);
        ui.getOfflineButton().setSelected(!checkOnline);

        SwingUtil.setLayerUI(ui.getMapPane(), ui.getMapBlockLayer());

        initListeners();
    }

    // Create thread executor for map loader (always execute the latest call)
    private ThreadPoolExecutor getExecutor() {
        if (executor == null || executor.isShutdown())
            executor = ActionFactory.createSingleThreadExecutor(ActionFactory.ExecutionMode.LATEST);
        return executor;
    }

    private void shutdownExecutor() {
        if (executor != null)
            executor.shutdown();
    }

    @Override
    public void onCloseUI() {

//        executor.shutdownNow();
        shutdownExecutor();
        getUI().getMapPane().clearMapContent();
        super.onCloseUI();
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        getUI().getMapPane().addMapPaneListener(new MapPaneAdapter() {

            @Override
            public void onRenderingStarted(MapPaneEvent ev) {
                getUI().getLoadingIndicator().setVisible(true);
            }

            @Override
            public void onRenderingStopped(MapPaneEvent ev) {
                getModel().setLoading(false);
                getUI().getLoadingIndicator().setVisible(false);
            }

            @Override
            public void onDisplayAreaChanged(MapPaneEvent ev) {
                ReferencedEnvelope envelope = (ReferencedEnvelope) ev.getData();
                checkVisibleEnvelope(Maps.transformReferencedEnvelope(envelope, DefaultGeographicCRS.WGS84));

                // for test
//                DataMapPane map = getUI().getMapPane();
//                double scale = RendererUtilities.calculateOGCScale(envelope, map.getWidth(), map.getRenderer().getRendererHints());
//                getUI().getMapEnvelopeLabel().setText(envelope.toString() + " | scale=" + scale);
            }
        });

        getUI().getMapPane().addMouseListener(new MapMouseAdapter() {
            @Override
            public void onMouseEntered(MapMouseEvent ev) {
                displayCoordinates(ev.getWorldPos());
            }

            @Override
            public void onMouseExited(MapMouseEvent ev) {
                displayCoordinates(null);
            }

            @Override
            public void onMouseMoved(MapMouseEvent ev) {
                displayCoordinates(ev.getWorldPos());
            }
        });

        getUI().getMapPane().addDataSelectionListener(new DataSelectionAdapter() {

            boolean previouslySelected = false;

            @Override
            public void onEmptySelection(DataSelectionEvent event) {
                if (previouslySelected) {

                    InfoPanelLayer infoPanelLayer = event.getSource().getInfoPanelLayer();
                    if (infoPanelLayer != null) {
                        infoPanelLayer.clear();
                        event.getSource().redrawLayers();
                    }
                    previouslySelected = false;
                }
            }

            @Override
            public void onDataLayerSelected(DataSelectionEvent event) {

                InfoPanelLayer infoPanelLayer = event.getSource().getInfoPanelLayer();
                if (infoPanelLayer == null) {
                    // Don't process if info panel is not present
                    return;
                }

                // Filter layers on survey or operation and get the first one (if many selected at the same point)
                Optional<DataFeatureLayer> layerOptional = event.getSelectedDataLayers().stream().filter(
                        layer -> SurveysMapBuilder.isSurveyLayer(layer) || SurveysMapBuilder.isOperationLayer(layer)).findFirst();
                if (!layerOptional.isPresent()) {
                    infoPanelLayer.clear();
                    event.getSource().redrawLayers();
                    return;
                }
                DataFeatureLayer selectedLayer = layerOptional.get();

                List<String> texts = Lists.newArrayList();

                boolean isSurvey = SurveysMapBuilder.isSurveyLayer(selectedLayer);
                boolean isOperation = SurveysMapBuilder.isOperationLayer(selectedLayer);

                if (isSurvey || isOperation) {

                    Integer surveyId = isSurvey ? selectedLayer.getId() : ((SurveyLayerCollection) selectedLayer.getDataLayerCollection()).getSurveyId();
                    if (surveyId != null) {
                        SurveyDTO survey = DaliBeans.findById(getModel().getSelectedSurveys(), surveyId);
                        if (survey != null) {

                            // Build texts
                            texts.add(String.format("%s: %s", t("dali.property.program"), decorate(survey.getProgram())));
                            texts.add(String.format("%s: %s", t("dali.property.location"), decorate(survey.getLocation())));
                            texts.add(String.format("%s: %s", t("dali.property.date"), Dates.formatDate(survey.getDate(), getConfig().getDateFormat())));
                            texts.add(String.format("%s: %s", t("dali.property.mnemonic"), survey.getName()));

                            if (isOperation) {
                                Integer operationId = selectedLayer.getId();
                                if (operationId != null) {
                                    SamplingOperationDTO operation = DaliBeans.findById(survey.getSamplingOperations(), operationId);
                                    if (operation != null) {

                                        // Add operation text
                                        texts.add(String.format("%s: %s", t("dali.property.samplingOperation"), decorate(operation, DecoratorService.CONCAT)));

                                    } else {
                                        if (LOG.isWarnEnabled()) {
                                            LOG.warn(String.format("The operation with id=%s should be found in survey's operations list", operationId));
                                            return;
                                        }
                                    }

                                } else {
                                    // impossible to build operation info
                                }
                            }

                        } else {
                            if (LOG.isWarnEnabled()) {
                                LOG.warn(String.format("The survey with id=%s should be found in table", surveyId));
                                return;
                            }
                        }

                    } else {
                        // impossible to build survey info
                    }

                } else {

                    // Not implemented
                }

//                LOG.info(String.format("selected survey: %s at %s coordinate %s", event.getSelectedDataLayers().stream()
//                        .filter(dataLayer -> dataLayer.getDataLayerCollection() != null)
//                        .map(dataLayer -> ((SurveyLayerCollection) dataLayer.getDataLayerCollection()).getSurveyId()).collect(Collectors.toSet()),
//                        event.getScreenPoint(), event.getWorldPos()));

                if (CollectionUtils.isNotEmpty(texts)) {

                    infoPanelLayer.setInfo(event.getScreenPoint(), texts);
                    event.getSource().redrawLayers();
                    previouslySelected = true;
                }
            }
        });

        getUI().getMapPane().getRenderer().addRenderListener(new RenderListener() {
            @Override
            public void featureRenderer(SimpleFeature feature) {

            }

            @Override
            public void errorOccurred(Exception e) {
                if (e.getCause() instanceof ProjectionException)
                    getUI().getMapPane().fixFullExtend();
            }
        });
    }

    private void displayCoordinates(DirectPosition2D p) {
        DirectPosition2D position = Maps.transformDirectPosition(p, DefaultGeographicCRS.WGS84);
        getUI().getMapPositionLabel().setText(position != null ? String.format("[ %.4f, %.4f ]", position.getX(), position.getY()) : "[ ]");
    }

    private void checkVisibleEnvelope(ReferencedEnvelope visibleEnvelope) {

        // If visible envelope does not include the data envelope, show Warning
        ReferencedEnvelope overallEnvelope = getUI().getMapPane().getOverallEnvelope();
        if (!overallEnvelope.isEmpty() && !visibleEnvelope.contains((Envelope) overallEnvelope)) {
            getUI().getInfoLabel().setText(t("dali.home.map.warning.dataOutsideEnvelope"));
        } else {
            getUI().getInfoLabel().setText(null);
        }
    }

    public void openMap() {

        if (getModel().getSelectionModel() != null) {
            getModel().getSelectionModel().addPropertyChangeListener(AbstractDaliTableUIModel.PROPERTY_SELECTED_ROWS, getMapListener());
        }
        buildMapPositions();
        displayCoordinates(null);
    }

    public void closeMap() {

        getUI().getMapPane().clearMapContent();

        if (getModel().getSelectionModel() != null) {
            getModel().getSelectionModel().removePropertyChangeListener(AbstractDaliTableUIModel.PROPERTY_SELECTED_ROWS, getMapListener());
        }
    }

    private PropertyChangeListener getMapListener() {
        if (mapListener == null) {
            mapListener = evt -> SurveysMapUIHandler.this.buildMapPositions();
        }
        return mapListener;
    }

    public void buildMapPositions() {

        // TEST
//        mapBuilder.setForceOffline(true);

        // don't build map if selection change is disabled (eg table sorting)
        if (!getModel().isBuildMapOnSelectionChanged()) return;

        Collection<? extends SurveyDTO> selectedSurveys = getModel().getSelectedSurveys();

        if (selectedSurveys.size() > getConfig().getMaxSurveyInMap()) {
            getContext().getDialogHelper().showWarningDialog(t("dali.home.map.warning.tooManySurveys", getConfig().getMaxSurveyInMap()));
            return;
        }

        getModel().setLoading(true);
        getExecutor().execute(new MapLoader(selectedSurveys));
        LOG.debug("MapLoader ask execute");
    }

    public void zoomFit() {

        // get overall envelope of data
        ReferencedEnvelope displayEnvelope = getUI().getMapPane().getOverallDisplayEnvelope();
        if (!displayEnvelope.isEmpty()) {
            getUI().getMapPane().setDisplayArea(displayEnvelope);
        }
    }

    public void toggleOffline() {

        boolean selected = getUI().getOfflineButton().isSelected();
        getModel().setMapMode(selected ? MapMode.EMBEDDED_SHAPE_MAP_MODE : null);
        buildMapPositions();

    }

    public void toggleGraticule() {

        getUI().getMapPane().toggleGraticuleVisibility();

    }

    public void toggleLegend() {

        getUI().getMapPane().toggleLegendVisibility();

    }

    public void fullScreen() {

        getUI().getFullScreenButton().setVisible(false);

        getModel().getParentUIModel().fireOpenFullScreenEvent();

    }

    @Override
    public void cancel() {

        closeMap();

        getUI().getFullScreenButton().setVisible(true);

        getModel().getParentUIModel().fireCloseFullScreenEvent();

    }

    class MapLoader extends SwingWorker<MapContent, Void> {

        private final Collection<? extends SurveyDTO> selectedSurveys;

        MapLoader(Collection<? extends SurveyDTO> selectedSurveys) {
            this.selectedSurveys = selectedSurveys;
        }

        @Override
        protected MapContent doInBackground() {

//            Thread.sleep(100);

            if (LOG.isDebugEnabled()) LOG.debug("MapLoader BEGIN");

            DataMapPane mapPane = getUI().getMapPane();

            // dispose old content
            mapPane.clearMapContent();

            // build new content
            MapContent mapContent = mapPane.getMapBuilder().buildNewMapContent(getModel().getMapMode());

            mapPane.clearDataLayerCollections();

            boolean noData = false;

            if (CollectionUtils.isNotEmpty(selectedSurveys)) {

                Set<Layer> allLayers = Sets.newLinkedHashSet();

                // get Geometries from survey location, survey itself, and sampling operations
                for (SurveyDTO survey : selectedSurveys) {

                    // Prevent new line to be rendered without location
                    if (selectedSurveys.size() == 1 && survey.getLocation() == null) {
                        noData = true;
                        break;
                    }

                    // Skip unsaved survey
                    // FIXME Not saved surveys can't be handle completely because survey id and operation ids are used in DataLayerCollection
                    if (survey.getId() == null) {
                        continue;
                    }

                    // clone the survey to avoid concurrency access
                    SurveyDTO clonedSurvey = getContext().getObservationService().duplicateSurvey(survey, false, true);
                    clonedSurvey.setId(survey.getId());

                    getContext().getObservationService().loadSamplingOperationsFromSurvey(clonedSurvey, false);

                    // Create a layer collection for this survey
                    DataLayerCollection dataLayerCollection;
                    try {
                        dataLayerCollection = mapPane.newDataLayerCollection(clonedSurvey);
                    } catch (Exception e) {
                        LOG.error(String.format("Error when create data layer collection for survey '%s'", decorate(clonedSurvey)), e);
                        noData = true;
                        break;
                    }

                    List<Layer> dataLayers = null;

                    if (dataLayerCollection != null) {
                        // Get layers
                        dataLayers = dataLayerCollection.getLayers();
                    }

                    if (CollectionUtils.isNotEmpty(dataLayers)) {
                        // Add layers to new content
                        allLayers.addAll(dataLayers);
                    } else {
                        // Maybe a problem
                        if (LOG.isWarnEnabled()) {
                            LOG.warn(String.format("No data layers built for survey '%s'", decorate(clonedSurvey)));
                        }
                    }
                }

                if (CollectionUtils.isNotEmpty(allLayers)) {
                    // Add all data layers
                    mapContent.addLayers(allLayers);

                    // Add info panel layer
                    mapContent.addLayer(new InfoPanelLayer());

                    // Add legend layer
                    mapContent.addLayer(new LegendLayer());

                    // compute display area
                    ReferencedEnvelope displayEnvelope = mapPane.getOverallDisplayEnvelope();
                    if (!displayEnvelope.isEmpty()) {

                        // compute layer visibility at first render
                        mapPane.setLayersVisible(mapContent.layers(), displayEnvelope);

                        // apply display area
                        mapPane.setDisplayArea(displayEnvelope);
                    }
                } else {
                    noData = true;
                }

            } else {
                noData = true;
            }

            if (noData) {
                mapPane.setLayersVisible(mapContent.layers(), mapPane.getDisplayArea());
            }

            if (LOG.isDebugEnabled()) LOG.debug("MapLoader END");

            return mapContent;
        }

        @Override
        protected void done() {
            try {
                if (isCancelled())
                    return;

                try {
                    MapContent mapContent = get();

                    DataMapPane mapPane = getUI().getMapPane();

                    // fix Mantis #38358
                    getUI().getOfflineButton().setEnabled(mapPane.getMapBuilder().checkOnlineReachable());
                    getModel().setMapMode(mapPane.getMapBuilder().getCurrentMapMode());

                    mapPane.setMapContent(mapContent);

                    if (LOG.isDebugEnabled()) LOG.debug("MapLoader done OK");

                } catch (InterruptedException | ExecutionException e) {
                    throw new DaliTechnicalException(e);
                }

            } finally {
                getModel().setLoading(false);
            }
        }
    }

}
