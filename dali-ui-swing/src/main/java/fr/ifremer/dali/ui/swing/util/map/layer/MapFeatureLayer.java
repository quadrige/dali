package fr.ifremer.dali.ui.swing.util.map.layer;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.map.FeatureLayer;
import org.geotools.styling.Style;

/**
 * A FeatureLayer for map
 *
 * Used to indicate if this FeatureLayer a map layer (!= DataFeatureLayer)
 *
 * @author peck7 on 05/09/2017.
 */
public class MapFeatureLayer extends FeatureLayer implements MapLayer {

    public MapFeatureLayer(FeatureSource featureSource, Style style) {
        super(featureSource, style);
    }

    public MapFeatureLayer(FeatureSource featureSource, Style style, String title) {
        super(featureSource, style, title);
    }

    public MapFeatureLayer(FeatureCollection collection, Style style) {
        super(collection, style);
    }

    public MapFeatureLayer(FeatureCollection collection, Style style, String title) {
        super(collection, style, title);
    }
}
