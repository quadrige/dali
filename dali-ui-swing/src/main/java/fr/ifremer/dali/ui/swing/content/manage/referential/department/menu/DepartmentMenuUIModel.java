/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.dali.ui.swing.content.manage.referential.department.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.configuration.filter.department.DepartmentCriteriaDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * <p>DepartmentMenuUIModel class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class DepartmentMenuUIModel extends AbstractReferentialMenuUIModel<DepartmentCriteriaDTO, DepartmentMenuUIModel> implements DepartmentCriteriaDTO {

    private static final Binder<DepartmentMenuUIModel, DepartmentCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(DepartmentMenuUIModel.class, DepartmentCriteriaDTO.class);

    private static final Binder<DepartmentCriteriaDTO, DepartmentMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(DepartmentCriteriaDTO.class, DepartmentMenuUIModel.class);

    /**
     * <p>Constructor for DepartmentMenuUIModel.</p>
     */
    public DepartmentMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected DepartmentCriteriaDTO newBean() {
        return DaliBeanFactory.newDepartmentCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return delegateObject.getCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setCode(String code) {
        delegateObject.setCode(code);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getParentDepartment() {
        return delegateObject.getParentDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setParentDepartment(DepartmentDTO parentDepartment) {
        delegateObject.setParentDepartment(parentDepartment);
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setCode(null);
        setParentDepartment(null);
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void setDirty(boolean dirty) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }
}
