package fr.ifremer.dali.ui.swing.content.manage.program.strategies.duplicate;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.JComponent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>SelectProgramUIHandler class.</p>
 *
 */
public class SelectProgramUIHandler extends AbstractDaliUIHandler<SelectProgramUIModel, SelectProgramUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SelectProgramUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SelectProgramUIModel model = new SelectProgramUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final SelectProgramUI ui) {
        initUI(ui);

        // listener on default close action = cancel
        ui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                getModel().setTargetProgram(null);
            }
        });

        getModel().addPropertyChangeListener(evt -> {

            if (SelectProgramUIModel.PROPERTY_SOURCE_PROGRAM.equals(evt.getPropertyName())) {
                initBeanFilterableComboBox(getUI().getProgramCombo(), getAllowedPrograms(), getModel().getSourceProgram());
                updateMessageLabel();
            } else if (SelectProgramUIModel.PROPERTY_SOURCE_STRATEGY.equals(evt.getPropertyName())) {
                updateMessageLabel();
            }
        });

    }

    private List<ProgramDTO> getAllowedPrograms() {

        Integer userId = getContext().getDataContext().getRecorderPersonId();
        if (userId == null) {
            return null; // not connected
        }

        // return programs where current user is manager,
        return getContext().getProgramStrategyService().getManagedProgramsByUser(userId);
    }

    private void updateMessageLabel() {
        if (getModel().getSourceProgram() != null && getModel().getSourceStrategy() != null) {
            getUI().getMessageLabel().setText(t("dali.action.duplicate.strategy.selectProgram.message",
                    decorate(getModel().getSourceStrategy()), decorate(getModel().getSourceProgram())));
        }
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        getModel().setTargetProgram(null);
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getProgramCombo();
    }

}
