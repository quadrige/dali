package fr.ifremer.dali.ui.swing.util.tab;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Les index des onglets.
 */
public interface DaliTabIndexes {

	/**
	 * Onglet observations general.
	 */
    int OBSERVATION_GENERAL = 0;
    
    /**
     * Onglet prelevement mesures.
     */
    int OPERATION_MEASUREMENTS = 1;
    
    /**
     * Onglet photos.
     */
    int PHOTOS = 2;
}
