package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;

/**
 * Controlleur du menu pour la gestion des Matrices au niveau local
 */
public class ManageMatricesMenuUIHandler extends AbstractDaliUIHandler<ManageMatricesMenuUIModel, ManageMatricesMenuUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageMatricesMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageMatricesMenuUIModel model = new ManageMatricesMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageMatricesMenuUI ui) {
        initUI(ui);

        // Initialiser les combobox
        initComboBox();
    }

	/**
	 * Initialisation des combobox
	 */
	private void initComboBox() {
		initBeanFilterableComboBox(
				getUI().getLabelCombo(),
				getContext().getReferentialService().getMatrices(StatusFilter.ALL),
				null);
		
		initBeanFilterableComboBox(
				getUI().getStatusCombo(),
				getContext().getReferentialService().getStatus(StatusFilter.ALL),
				null);

        DaliUIs.forceComponentSize(getUI().getLabelCombo());
        DaliUIs.forceComponentSize(getUI().getStatusCombo());
	}

}
