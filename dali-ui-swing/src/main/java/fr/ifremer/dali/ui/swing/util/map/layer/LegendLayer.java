package fr.ifremer.dali.ui.swing.util.map.layer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.jts.LiteShape;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.legend.Drawer;
import org.geotools.map.DirectLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;
import org.geotools.styling.*;
import org.opengis.feature.simple.SimpleFeature;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author peck7 on 11/09/2018.
 */
public class LegendLayer extends DirectLayer {

    private static final Log LOG = LogFactory.getLog(LegendLayer.class);

    private static final int GLYPH_LEFT_GAP = 5;
    private static final int TEXT_LEFT_GAP = 20;
    private static final int TEXT_RIGHT_GAP = 5;
    private static final int TEXT_TOP_GAP = 2;
    private static final int TEXT_BOTTOM_GAP = 2;
    private static final int LEGEND_MARGIN = 10;

    private Drawer drawer = Drawer.create();
    private AffineTransform defaultTransform = new AffineTransform();
    private LiteShape defaultShape = new LiteShape(null, defaultTransform, false);

    public LegendLayer() {
        setTitle("legendLayer");
    }

    @Override
    public void draw(Graphics2D graphics, MapContent map, MapViewport viewport) {

        List<DataFeatureLayer> dataFeatureLayers = map.layers().stream()
                .filter(layer -> layer instanceof DataFeatureLayer)
                .map(layer -> (DataFeatureLayer) layer)
                .collect(Collectors.toList());

        // nothing to draw
        if (dataFeatureLayers.isEmpty()) return;

        Set<NamedBufferedImage> items = new LinkedHashSet<>();

        Stroke oldStroke = graphics.getStroke();
        Paint oldPaint = graphics.getPaint();
        Color oldBackground = graphics.getBackground();

        try {
            try {

                for (Layer layer : map.layers()) {
                    if (layer instanceof DataFeatureLayer) {
                        DataFeatureLayer dataFeatureLayer = (DataFeatureLayer) layer;

                        Style style = dataFeatureLayer.getStyle();
                        if (style != null) {

                            List<Symbolizer> symbolizers = style.featureTypeStyles().stream()
                                    .flatMap(featureTypeStyle -> featureTypeStyle.rules().stream())
                                    .flatMap(rule -> rule.symbolizers().stream())
                                    .filter(symbolizer -> symbolizer.getDescription() != null && symbolizer.getDescription().getTitle() != null)
                                    .collect(Collectors.toList());
                            for (Symbolizer symbolizer : symbolizers) {
                                if (symbolizer instanceof PointSymbolizer) {

                                    NamedBufferedImage item = createItem(graphics, symbolizer);
                                    drawFeature(
                                            item,
                                            symbolizer,
                                            drawer.feature(drawer.point(GLYPH_LEFT_GAP * 2, item.getHeight() / 2)));
                                    items.add(item);

                                } else if (symbolizer instanceof LineSymbolizer) {

                                    NamedBufferedImage item = createItem(graphics, symbolizer);
                                    drawFeature(
                                            item,
                                            symbolizer,
                                            drawer.feature(drawer.line(new int[]{
                                                    GLYPH_LEFT_GAP, item.getHeight() / 2, GLYPH_LEFT_GAP + 10, item.getHeight() / 2
                                            })));
                                    items.add(item);

                                } else if (symbolizer instanceof PolygonSymbolizer) {

                                    NamedBufferedImage item = createItem(graphics, symbolizer);
                                    int glyphTopGap = (item.getHeight() - 10) / 2;
                                    drawFeature(
                                            item,
                                            symbolizer,
                                            drawer.feature(drawer.polygon(new int[]{
                                                    GLYPH_LEFT_GAP, glyphTopGap,
                                                    GLYPH_LEFT_GAP + 10, glyphTopGap,
                                                    GLYPH_LEFT_GAP + 10, glyphTopGap + 10,
                                                    GLYPH_LEFT_GAP, glyphTopGap + 10,
                                                    GLYPH_LEFT_GAP, glyphTopGap
                                            })));
                                    items.add(item);

                                }
                            }
                        }

                    }
                }

                // nothing to draw
                if (items.isEmpty()) return;

                int legendHeight = items.stream().mapToInt(NamedBufferedImage::getHeight).sum();
                int legendWidth = items.stream().mapToInt(NamedBufferedImage::getWidth).max().getAsInt();
                BufferedImage legendImage = new BufferedImage(legendWidth, legendHeight, BufferedImage.TYPE_INT_RGB);
                Graphics2D g2d = legendImage.createGraphics();
                g2d.setPaint(Color.WHITE);
                g2d.fillRect(0, 0, legendWidth, legendHeight);

                int y = 0;
                for (BufferedImage item : items) {
                    legendImage.getGraphics().drawImage(item, 0, y, null);
                    y += item.getHeight();
                }

                graphics.drawImage(legendImage, viewport.getScreenArea().width - legendWidth - LEGEND_MARGIN, LEGEND_MARGIN, null);

            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }

        } finally {

            graphics.setStroke(oldStroke);
            graphics.setPaint(oldPaint);
            graphics.setBackground(oldBackground);

        }
    }

    private NamedBufferedImage createItem(Graphics2D graphics, Symbolizer symbolizer) {
        String text = symbolizer.getDescription().getTitle().toString();
        Rectangle2D textRect = graphics.getFontMetrics().getStringBounds(text, graphics);
        int width = (int) (textRect.getWidth() + 0.5) + TEXT_LEFT_GAP + TEXT_RIGHT_GAP;
        int height = (int) (textRect.getHeight() + 0.5) + TEXT_TOP_GAP + TEXT_BOTTOM_GAP;
        NamedBufferedImage item = new NamedBufferedImage(symbolizer.getName(), width, height);
        Graphics2D g2d = item.createGraphics();
        g2d.setPaint(Color.WHITE);
        g2d.fillRect(0, 0, width, height);
        g2d.setPaint(Color.BLACK);
        g2d.drawString(text, TEXT_LEFT_GAP, (float) -textRect.getY() + TEXT_TOP_GAP);
        return item;
    }

    private void drawFeature(BufferedImage item, Symbolizer symbolizer, SimpleFeature simpleFeature) {
        drawer.drawFeature(item, simpleFeature, defaultTransform, false, symbolizer, null, defaultShape);
    }

    @Override
    public ReferencedEnvelope getBounds() {
        return null;
    }

    class NamedBufferedImage extends BufferedImage {

        private final String name;

        NamedBufferedImage(String name, int width, int height) {
            super(width, height, BufferedImage.TYPE_INT_ARGB);
            this.name = name;
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof NamedBufferedImage && hashCode() == obj.hashCode();
        }
    }
}
