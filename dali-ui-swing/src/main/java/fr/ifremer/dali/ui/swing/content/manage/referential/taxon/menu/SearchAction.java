package fr.ifremer.dali.ui.swing.content.manage.referential.taxon.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant la recherche de taxons au niveau national
 */
public class SearchAction extends AbstractDaliAction<TaxonMenuUIModel, TaxonMenuUI, TaxonMenuUIHandler> {

    private List<TaxonDTO> result;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final TaxonMenuUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getLevel() == null && StringUtils.isBlank(getModel().getName())) {

            getContext().getDialogHelper().showWarningDialog(t("dali.action.search.missingCriteria"));
            return false;
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        result = getContext().getReferentialService().searchTaxons(getModel());
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().setResults(result);

        super.postSuccessAction();
    }

}
