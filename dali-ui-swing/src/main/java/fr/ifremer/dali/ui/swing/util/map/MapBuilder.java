package fr.ifremer.dali.ui.swing.util.map;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.map.MapMode;
import fr.ifremer.dali.ui.swing.util.map.layer.DataLayerCollection;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.MapContent;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.awt.Color;
import java.util.List;

/**
 * Map builder interface
 * Initialise map content, behaviors, etc...
 *
 * @author peck7 on 02/06/2017.
 */
public interface MapBuilder {

    /**
     * Generate a new MapContent with map layers
     *
     * @param preferredMode the preferred map mode
     * @return the new map content
     */
    MapContent buildNewMapContent(MapMode preferredMode);

    CoordinateReferenceSystem getTargetCRS();

    MapMode getCurrentMapMode();

    /**
     * Build the data layer collection from a survey
     *
     * @param dataObject the source object
     * @return the collection of data layers
     */
    DataLayerCollection buildDataLayerCollection(Object dataObject) throws Exception;

    /**
     * Retrieve the list of layer names that should be visible for the provided envelope
     *
     * @param envelope the envelope
     * @return list of displayable layer names
     */
    List<String> getDisplayableLayerNames(ReferencedEnvelope envelope);

    /**
     * Check online connectivity
     *
     * @return true if online available
     */
    boolean checkOnlineReachable();

    /**
     * Get the background color
     *
     * @return the background color
     */
    Color getBackgroundColor();

}
