package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped;

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dto.referential.UnitDTO;
import fr.ifremer.dali.ui.swing.util.DaliUIs;
import fr.ifremer.dali.ui.swing.util.table.DaliPmfmColumnIdentifier;
import fr.ifremer.dali.ui.swing.util.table.PmfmTableColumn;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.event.TableColumnModelExtListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * Action to show/hide total footer on operation's grouped measurements
 * Need to be a AbstractActionExt with state action set to have a check box menu item in table control
 *
 * @author peck7 on 24/04/2019.
 */
public class OperationMeasurementsTotalFooterAction extends AbstractActionExt {

    private final OperationMeasurementsGroupedTableUIHandler handler;
    private JLabel label;
    private FooterTable footer;

    OperationMeasurementsTotalFooterAction(OperationMeasurementsGroupedTableUIHandler handler) {
        super(t("dali.samplingOperation.measurement.grouped.total"));
        this.handler = handler;
        setStateAction();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (isSelected()) {
            getFooterTable().createCalculators();
            showTotal();
        } else {
            hideTotal();
        }
    }

    private void showTotal() {

        // footer label
        addIfAbsent(getFooterPanel(), getFooterLabel());
        getFooterLayout().putConstraint(SpringLayout.NORTH, getFooterLabel(), 0, SpringLayout.NORTH, getFooterPanel());
        getFooterLayout().putConstraint(SpringLayout.SOUTH, getFooterLabel(), 0, SpringLayout.SOUTH, getFooterPanel());
        getFooterLayout().putConstraint(SpringLayout.WEST, getFooterLabel(), 0, SpringLayout.WEST, getFooterPanel());
        getFooterLayout().putConstraint(SpringLayout.EAST, getFooterLabel(), getRowHeaderSize(), SpringLayout.WEST, getFooterPanel());

        // footer table
        addIfAbsent(getFooterPanel(), getFooterTable());
        getFooterLayout().putConstraint(SpringLayout.NORTH, getFooterTable(), 0, SpringLayout.NORTH, getFooterPanel());
        getFooterLayout().putConstraint(SpringLayout.SOUTH, getFooterTable(), 0, SpringLayout.SOUTH, getFooterPanel());
        getFooterLayout().putConstraint(SpringLayout.WEST, getFooterTable(), getScrollPosition(), SpringLayout.WEST, getFooterPanel());
        getFooterLayout().putConstraint(SpringLayout.EAST, getFooterTable(), 0, SpringLayout.EAST, getFooterPanel());

        // adjust size
        getFooterTable().setPreferredSize(new Dimension(getFooterTable().getPreferredSize().width, getFooterLabel().getPreferredSize().height));
        getFooterPanel().setPreferredSize(getFooterTable().getPreferredSize());

        SwingUtilities.invokeLater(getFooterPanel()::revalidate);

    }

    private JLabel getFooterLabel() {
        if (label == null) {
            label = new JLabel(t("dali.samplingOperation.measurement.grouped.total.label") + " ");
            label.setHorizontalAlignment(SwingConstants.RIGHT);
            // Set Foreground (Mantis #47353)
            label.setForeground(Optional.ofNullable(UIManager.getColor("thematicLabelColor")).orElse(label.getForeground()));

            label.setBackground(getTableScrollPane().getBackground());
            label.setOpaque(true);

            // init size
            label.setPreferredSize(new Dimension(label.getPreferredSize().width, DaliUIs.DALI_COMPONENT_HEIGHT));
        }
        return label;
    }

    private FooterTable getFooterTable() {
        if (footer == null) {
            footer = new FooterTable(getTable().getColumnModel());
            footer.setTable(new JTable(null, getTable().getColumnModel()));
            initListeners();
        }
        return footer;
    }

    private void initListeners() {

        // add listener on column model for column events
        getTable().getColumnModel().addColumnModelListener(new TableColumnModelExtListener() {

            @Override
            public void columnAdded(TableColumnModelEvent e) {
                updateUI();
            }

            @Override
            public void columnRemoved(TableColumnModelEvent e) {
                updateUI();
            }

            @Override
            public void columnMoved(TableColumnModelEvent e) {
                updateUI();
            }

            @Override
            public void columnMarginChanged(ChangeEvent e) {
                updateUI();
            }

            @Override
            public void columnSelectionChanged(ListSelectionEvent e) {

            }

            @Override
            public void columnPropertyChange(PropertyChangeEvent event) {

            }
        });

        // add listener on scroll pane's viewport
        getTableScrollPane().getViewport().addChangeListener(e -> updateUI());

        // add listener on table row filter
        getTable().addPropertyChangeListener(SwingTable.PROPERTY_ROW_FILTER, evt -> updateTotals());

        // add listener on table model
        getTableModel().addTableModelListener(evt -> updateUI());

    }

    private void addIfAbsent(JPanel panel, Component field) {
        if (Arrays.stream(panel.getComponents()).noneMatch(component -> component == field)) panel.add(field);
    }

    private int getScrollPosition() {
        int scrollPosition = getTableScrollPane().getViewport().getViewPosition().x;

        // eventually remove row header width
        scrollPosition -= getRowHeaderSize();

        return -scrollPosition;
    }

    private int getRowHeaderSize() {
        if (getTableScrollPane().getRowHeader() != null) {
            return getTableScrollPane().getRowHeader().getViewSize().width;
        }
        return 0;
    }

    private void hideTotal() {

        getFooterPanel().remove(getFooterTable());

        // hide footer panel
        getFooterPanel().setPreferredSize(new Dimension(0, 0));

        SwingUtilities.invokeLater(getFooterPanel()::revalidate);

    }

    private void updateUI() {
        if (isSelected())
            SwingUtilities.invokeLater(() -> {
                getFooterTable().createCalculators();
                showTotal();
            });
    }

    private void updateTotals() {
        if (isSelected()) {
            getFooterTable().updateTotals();
            getFooterTable().repaint();
        }
    }

    private DaliConfiguration getConfig() {
        return handler.getConfig();
    }

    private SwingTable getTable() {
        return handler.getTable();
    }

    private OperationMeasurementsGroupedTableModel getTableModel() {
        return handler.getTableModel();
    }

    private JScrollPane getTableScrollPane() {
        return (JScrollPane) SwingUtilities.getAncestorOfClass(JScrollPane.class, getTable());
    }

    private JPanel getFooterPanel() {
        return handler.getUI().getFooterPanel();
    }

    private SpringLayout getFooterLayout() {
        return handler.getUI().getFooterLayout();
    }

    private class FooterTable extends JTableHeader {

        private Map<Integer, TotalCalculator> calculators = null;

        FooterTable(TableColumnModel columnModel) {
            super(columnModel);

            setDefaultRenderer(new FooterRenderer(getDefaultRenderer(), this));
        }

        public void createCalculators() {
            calculators = new HashMap<>();

            for (int i = 0; i < getColumnModel().getColumnCount(); i++) {
                TableColumn column = getColumnModel().getColumn(i);
                if (column instanceof PmfmTableColumn) {
                    PmfmTableColumn pmfmColumn = (PmfmTableColumn) column;
                    if (pmfmColumn.isNumerical()) {
                        calculators.put(pmfmColumn.getModelIndex(), new TotalCalculator(pmfmColumn.getPmfmIdentifier()));
                    }
                }
            }
        }

        public void updateTotals() {
            if (calculators == null)
                createCalculators();
            if (calculators.isEmpty())
                return;
            calculators.values().forEach(TotalCalculator::calculate);
        }

        public TotalCalculator getCalculator(int index) {
            return calculators.get(index);
        }
    }

    private class FooterRenderer implements TableCellRenderer {

        private final TableCellRenderer delegate;
        private final FooterTable footer;

        FooterRenderer(TableCellRenderer delegate, FooterTable footer) {
            this.delegate = delegate;
            this.footer = footer;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component component = delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            if (component instanceof JLabel) {
                JLabel label = (JLabel) component;
                label.setHorizontalAlignment(SwingConstants.RIGHT);
                if (getColumn(column) instanceof PmfmTableColumn) {
                    PmfmTableColumn pmfmTableColumn = (PmfmTableColumn) getColumn(column);
                    TotalCalculator calculator = getCalculator(column);
                    if (calculator != null) {
                        UnitDTO unit = pmfmTableColumn.getPmfmIdentifier().getPmfm().getUnit();
                        String unitSymbol = (unit == null || getConfig().getExtractionUnitIdsToIgnore().contains(unit.getId())) ? "" : " " + unit.getSymbol();
                        label.setText(StringUtils.isNotBlank(calculator.getValue()) ? calculator.getValue() + unitSymbol : "");
                    } else {
                        label.setText("");
                    }
                } else {
                    label.setText("");
                }
            } else {
                component = new JLabel();
            }
            return component;
        }

        private TableColumn getColumn(int viewColumnIndex) {
            return footer.getColumnModel().getColumn(viewColumnIndex);
        }

        private TotalCalculator getCalculator(int viewColumnIndex) {
            return footer.getCalculator(getColumn(viewColumnIndex).getModelIndex());
        }

    }

    private class TotalCalculator implements PropertyChangeListener {

        private final DaliPmfmColumnIdentifier<OperationMeasurementsGroupedRowModel> identifier;
        private String value;

        TotalCalculator(DaliPmfmColumnIdentifier<OperationMeasurementsGroupedRowModel> identifier) {
            this.identifier = identifier;

            // add listener on identifier to listen to measurement's value changes
            identifier.addPropertyChangeListener(this);

            calculate();
        }

        void calculate() {
            BigDecimal total = new BigDecimal(0);
            boolean add = false;
            for (int i = 0; i < getTable().getRowCount(); i++) {
                Object value = identifier.getValue(getTableModel().getEntry(getTable().convertRowIndexToModel(i)));
                if (value instanceof BigDecimal) {
                    total = total.add((BigDecimal) value);
                    add = true;
                }
            }
            value = add ? total.toString() : "";
        }

        String getValue() {
            return value;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            if (evt instanceof DaliPmfmColumnIdentifier.PmfmChangeEvent) {
                calculate();
                getFooterTable().repaint();
            }
        }
    }
}

