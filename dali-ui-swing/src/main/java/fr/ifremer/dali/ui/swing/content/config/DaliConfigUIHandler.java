package fr.ifremer.dali.ui.swing.content.config;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.config.DaliConfigurationOption;
import fr.ifremer.dali.config.DaliSecuredConfigurationOption;
import fr.ifremer.dali.map.MapMode;
import fr.ifremer.dali.map.MapProjection;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.action.GoToPreviousScreenAction;
import fr.ifremer.dali.ui.swing.action.ReloadApplicationAction;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.quadrige3.core.config.QuadrigeConfigurationOption;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfigurationOption;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.ui.swing.table.editor.DatePatternCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.EnumValueCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.NumberCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.renderer.CheckBoxRenderer;
import fr.ifremer.quadrige3.ui.swing.table.renderer.EnumValueCellRenderer;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.config.ConfigUI;
import jaxx.runtime.swing.config.ConfigUIHelper;
import jaxx.runtime.swing.config.model.MainCallBackFinalizer;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.BorderLayout;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * <p>DaliConfigUIHandler class.</p>
 *
 * @since 1.0
 */
public class DaliConfigUIHandler extends AbstractDaliUIHandler<DaliUIContext, DaliConfigUI> {

    /**
     * Constant <code>CALLBACK_APPLICATION="application"</code>
     */
    private static final String CALLBACK_APPLICATION = "application";

    /**
     * Constant <code>CALLBACK_UI="ui"</code>
     */
    private static final String CALLBACK_UI = "ui";

    @Override
    public void beforeInit(DaliConfigUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(SwingUtil.createActionIcon("config"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(DaliConfigUI ui) {

        initUI(ui);

        getContext().getSwingSession().addUnsavedRootComponentByName(ui.getName());

        DaliConfiguration config = getConfig();

        ConfigUIHelper helper = new ConfigUIHelper(config.getApplicationConfig(), config.getApplicationConfig(), config.getConfigFile());

        helper.registerCallBack(
                CALLBACK_UI, n("dali.config.action.reload.ui"),
                SwingUtil.createActionIcon("reload-ui"),
                this::reloadUI
        ).registerCallBack(
                CALLBACK_APPLICATION, n("dali.config.action.reload.application"),
                SwingUtil.createActionIcon("reload-application"),
                this::reloadApplication
        );

        // Application category
        helper.addCategory(n("dali.config.category.application"), n("dali.config.category.application.description"))
                .addOption(DaliConfigurationOption.CONTROL_SURVEY_LOCATION_MINIMUM_DISTANCE_IN_METER)
                .setOptionEditor(new NumberCellEditor<>(Integer.class, false, DaliUI.INT_6_DIGITS_PATTERN))
                .setOptionShortLabel(t("dali.config.option.control.survey.location.minDistance.meter.shortLabel"));

        // Data category
        List<QuadrigeUserAuthority> neededRoles = Lists.newArrayList(QuadrigeUserAuthority.ADMIN);
        helper.addCategory(n("dali.config.category.data"), n("dali.config.category.data.description"), CALLBACK_APPLICATION)
                // Authentication URL :
                .addOption(new DaliSecuredConfigurationOption(QuadrigeCoreConfigurationOption.AUTHENTICATION_INTRANET_SITE_URL, neededRoles))
                .setOptionEditor(new DefaultCellEditor(new JTextField()))
                .setOptionShortLabel(t("dali.config.option.authentication.intranet.site.url.shortLabel"))
                .addOption(new DaliSecuredConfigurationOption(QuadrigeCoreConfigurationOption.AUTHENTICATION_EXTRANET_SITE_URL, neededRoles))
                .setOptionShortLabel(t("dali.config.option.authentication.extranet.site.url.shortLabel"))
                .addOption(new DaliSecuredConfigurationOption(QuadrigeConfigurationOption.SYNCHRONIZATION_SITE_URL, neededRoles))
                .setOptionShortLabel(t("dali.config.option.synchronization.site.url.shortLabel"))
                .addOption(new DaliSecuredConfigurationOption(DaliConfigurationOption.SITE_URL, neededRoles))
                .setOptionShortLabel(t("dali.config.option.site.url.shortLabel"))
                .addOption(new DaliSecuredConfigurationOption(QuadrigeCoreConfigurationOption.UPDATE_APPLICATION_URL, neededRoles))
                .setOptionShortLabel(t("dali.config.option.update.application.url.shortLabel"))
                .addOption(new DaliSecuredConfigurationOption(QuadrigeCoreConfigurationOption.UPDATE_DATA_URL, neededRoles))
                .setOptionShortLabel(t("dali.config.option.update.data.url.shortLabel"))
                .addOption(new DaliSecuredConfigurationOption(DaliConfigurationOption.SISMER_WEBSITE_URL, neededRoles))
                .setOptionShortLabel(t("dali.config.option.campaign.sismer.url.shortLabel"));

        // UI category
        helper.addCategory(n("dali.config.category.ui"), n("dali.config.category.ui.description"), CALLBACK_UI)
                .addOption(DaliConfigurationOption.COLOR_ALTERNATE_ROW)
                .setOptionShortLabel(t("dali.config.option.ui.color.alternateRow.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_SELECTED_ROW)
                .setOptionShortLabel(t("dali.config.option.ui.color.selectedRow.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_ROW_INVALID)
                .setOptionShortLabel(t("dali.config.option.ui.color.rowInvalid.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_ROW_READ_ONLY)
                .setOptionShortLabel(t("dali.config.option.ui.color.rowReadOnly.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_CELL_WITH_VALUE)
                .setOptionShortLabel(t("dali.config.option.ui.color.cellWithValue.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_COMPUTED_WEIGHTS)
                .setOptionShortLabel(t("dali.config.option.ui.color.computedWeights.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_BLOCKING_LAYER)
                .setOptionShortLabel(t("dali.config.option.ui.color.blockingLayer.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_SELECTED_CELL)
                .setOptionShortLabel(t("dali.config.option.ui.color.selectedCell.shortLabel"))
                .addOption(DaliConfigurationOption.COLOR_UNUSED_EDITOR_BACKGROUND)
                .setOptionShortLabel(t("dali.config.option.ui.color.unusedEditorBackground.shortLabel"))
                .addOption(DaliConfigurationOption.DATE_FORMAT).setOptionEditor(new DatePatternCellEditor())
                .setOptionShortLabel(t("dali.config.option.ui.dateFormat.shortLabel"))
                .addOption(DaliConfigurationOption.DATE_TIME_FORMAT).setOptionEditor(new DatePatternCellEditor())
                .setOptionShortLabel(t("dali.config.option.ui.dateTimeFormat.shortLabel"))
                .addOption(DaliConfigurationOption.TABLES_CHECKBOX).setOptionRenderer(new CheckBoxRenderer())
                .setOptionShortLabel(t("dali.config.option.ui.table.showCheckbox.shortLabel"));

        // Map category
        helper.addCategory(n("dali.config.category.map"), n("dali.config.category.map.description"), CALLBACK_UI)
                .addOption(DaliConfigurationOption.MAP_BASE_LAYER_DEFAULT)
                .setOptionShortLabel(t("dali.config.option.survey.map.baseLayer.default.shortLabel"))
                .setOptionEditor(new EnumValueCellEditor(MapMode.class))
                .setOptionRenderer(new EnumValueCellRenderer(MapMode.class))
                .addOption(DaliConfigurationOption.MAP_PROJECTION_CODE)
                .setOptionShortLabel(t("dali.config.option.survey.map.projection.code.shortLabel"))
                .setOptionEditor(new EnumValueCellEditor(MapProjection.class))
                .setOptionRenderer(new EnumValueCellRenderer(MapProjection.class))
//                .addOption(DaliConfigurationOption.MAP_SEXTANT_WMS_URL)
//                .setOptionShortLabel(t("dali.config.option.survey.map.sextantWMS.url.shortLabel"))
                .addOption(DaliConfigurationOption.MAP_SEXTANT_WMTS_URL)
                .setOptionShortLabel(t("dali.config.option.survey.map.sextantWMTS.url.shortLabel"))
                .setOptionEditor(new DefaultCellEditor(new JTextField())).setOptionRenderer(new DefaultTableCellRenderer()) // reset for below options
                .addOption(DaliConfigurationOption.MAP_OSM_URL)
                .setOptionShortLabel(t("dali.config.option.survey.map.openStreetMap.url.shortLabel"))
                .addOption(DaliConfigurationOption.MAP_OTM_URL)
                .setOptionShortLabel(t("dali.config.option.survey.map.openTopoMap.url.shortLabel"))
                .addOption(DaliConfigurationOption.MAP_CARTO_BASE_URL)
                .setOptionShortLabel(t("dali.config.option.survey.map.cartoBase.url.shortLabel"))
                .addOption(DaliConfigurationOption.MAP_SATELLITE_URL)
                .setOptionShortLabel(t("dali.config.option.survey.map.satellite.url.shortLabel"))
                .addOption(DaliConfigurationOption.MAP_MAX_SELECTION)
                .setOptionShortLabel(t("dali.config.option.survey.map.maxSelection.shortLabel"))
        ;

        // Technical category
        helper.addCategory(n("dali.config.category.technical"), n("dali.config.category.technical.description"), CALLBACK_APPLICATION)
                .addOption(DaliConfigurationOption.BASEDIR)
                .setOptionShortLabel(t("dali.config.option.basedir.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DATA_DIRECTORY)
                .setOptionShortLabel(t("dali.config.option.data.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.TMP_DIRECTORY)
                .setOptionShortLabel(t("dali.config.option.tmp.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.I18N_DIRECTORY)
                .setOptionShortLabel(t("dali.config.option.i18n.directory.shortLabel"))
                .addOption(DaliConfigurationOption.HELP_DIRECTORY)
                .setOptionShortLabel(t("dali.config.option.help.directory.shortLabel"))
                .addOption(QuadrigeCoreConfigurationOption.CONFIG_DIRECTORY)
                .setOptionShortLabel(t("dali.config.option.config.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.JDBC_URL)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.jdbc.url.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_BACKUP_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.backup.directory.shortLabel"))
                .addOption(DaliConfigurationOption.DB_BACKUP_EXTERNAL_DIRECTORY)
                .setOptionShortLabel(t("dali.config.option.persistence.db.backup.external.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_ATTACHMENT_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.attachment.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_CACHE_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.cache.directory.shortLabel"))
                .addOption(DaliConfigurationOption.UI_CONFIG_FILE)
                .setOptionShortLabel(t("dali.config.option.ui.config.file.shortLabel"));

        helper.setFinalizer(new MainCallBackFinalizer(CALLBACK_APPLICATION));

        helper.setCloseAction(() -> getContext().getActionEngine().runInternalAction(
                DaliConfigUIHandler.this,
                GoToPreviousScreenAction.class));
        ConfigUI configUI = helper.buildUI(
                getUI(),
                n("dali.config.category.application"));

        configUI.getHandler().setTopContainer(getUI());
        getUI().add(configUI, BorderLayout.CENTER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI();
    }

    /**
     * <p>reloadApplication.</p>
     */
    private void reloadApplication() {
        ReloadApplicationAction action = getContext().getActionFactory().createLogicAction(this, ReloadApplicationAction.class);
        getContext().getActionEngine().runAction(action);
    }

    /**
     * <p>reloadUI.</p>
     */
    private void reloadUI() {
        getContext().getMainUI().getHandler().reloadUI();
    }

}
