package fr.ifremer.dali.ui.swing.content.manage.referential.unit.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.configuration.filter.FilterCriteriaDTO;
import fr.ifremer.dali.dto.referential.UnitDTO;
import fr.ifremer.dali.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Modele du menu pour la gestion des Units au niveau local
 */
public class ReferentialUnitsMenuUIModel extends AbstractReferentialMenuUIModel<FilterCriteriaDTO, ReferentialUnitsMenuUIModel> implements FilterCriteriaDTO {

    /** Constant <code>PROPERTY_UNIT="unit"</code> */
    public static final String PROPERTY_UNIT = "unit";
    private static final Binder<ReferentialUnitsMenuUIModel, FilterCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ReferentialUnitsMenuUIModel.class, FilterCriteriaDTO.class);
    private static final Binder<FilterCriteriaDTO, ReferentialUnitsMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(FilterCriteriaDTO.class, ReferentialUnitsMenuUIModel.class);
    private UnitDTO unit;

    /**
     * <p>Constructor for ReferentialUnitsMenuUIModel.</p>
     */
    public ReferentialUnitsMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /**
     * <p>getUnitId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getUnitId() {
        return getUnit() == null ? null : getUnit().getId();
    }

    /**
     * <p>Getter for the field <code>unit</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.referential.UnitDTO} object.
     */
    public UnitDTO getUnit() {
        return unit;
    }

    /**
     * <p>Setter for the field <code>unit</code>.</p>
     *
     * @param unit a {@link fr.ifremer.dali.dto.referential.UnitDTO} object.
     */
    public void setUnit(UnitDTO unit) {
        this.unit = unit;
        firePropertyChange(PROPERTY_UNIT, null, unit);
    }

    /** {@inheritDoc} */
    @Override
    protected FilterCriteriaDTO newBean() {
        return DaliBeanFactory.newFilterCriteriaDTO();
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void setDirty(boolean dirty) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }

}
