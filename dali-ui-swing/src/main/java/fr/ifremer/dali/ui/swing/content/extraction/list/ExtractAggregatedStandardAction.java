package fr.ifremer.dali.ui.swing.content.extraction.list;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.enums.ExtractionOutputType;
import fr.ifremer.dali.dto.system.extraction.ExtractionParameterDTO;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by Ludovic on 03/12/2015.
 */
public class ExtractAggregatedStandardAction extends AbstractInternalExtractAction {
    /**
     * Constructor.
     *
     * @param handler Handler
     */
    public ExtractAggregatedStandardAction(ExtractionsTableUIHandler handler) {
        super(handler);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ExtractionOutputType getOutputType() {
        return ExtractionOutputType.AGGREGATED_STANDARD;
    }

    @Override
    public boolean prepareAction() throws Exception {

        // check configuration is filled
        ExtractionParameterDTO parameter = getSelectedExtraction().getParameter();
        if (parameter == null || parameter.isPmfmResultsEmpty() || parameter.isPmfmPresetsEmpty()) {
            getHandler().getContext().getDialogHelper().showWarningDialog(
                    t("dali.extraction.config.empty.message"),
                    getActionDescription()
            );
            return false;
        }

        return super.prepareAction();
    }
}
