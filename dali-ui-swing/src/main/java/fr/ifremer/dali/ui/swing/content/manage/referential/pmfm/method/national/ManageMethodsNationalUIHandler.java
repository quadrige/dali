package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.MethodDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.menu.ManageMethodsMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.table.MethodsTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.table.MethodsTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Methods au niveau national
 */
public class ManageMethodsNationalUIHandler extends AbstractDaliTableUIHandler<MethodsTableRowModel, ManageMethodsNationalUIModel, ManageMethodsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageMethodsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageMethodsNationalUIModel model = new ManageMethodsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(ManageMethodsNationalUI ui) {
        initUI(ui);

        ManageMethodsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();

        menuUIModel.addPropertyChangeListener(ManageMethodsMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((Collection<MethodDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(MethodsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        final TableColumnExt descriptionCol = addColumn(MethodsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // reference
        final TableColumnExt referenceCol = addColumn(MethodsTableModel.REFERENCE);
        referenceCol.setSortable(true);
        referenceCol.setEditable(false);

        // number
        final TableColumnExt numberCol = addColumn(MethodsTableModel.NUMBER);
        numberCol.setSortable(true);
        numberCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(MethodsTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(MethodsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(MethodsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                MethodsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // description packaging
        final TableColumnExt descriptionPackagingCol = addColumn(MethodsTableModel.DESCRIPTIONPACKAGING);
        descriptionPackagingCol.setSortable(true);
        descriptionPackagingCol.setEditable(false);

        // description preparation
        final TableColumnExt descriptionPreparationCol = addColumn(MethodsTableModel.DESCRIPTIONPREPARATION);
        descriptionPreparationCol.setSortable(true);
        descriptionPreparationCol.setEditable(false);

        // description preservation
        final TableColumnExt descriptionPreservationCol = addColumn(MethodsTableModel.DESCRIPTIONPRESERVATION);
        descriptionPreservationCol.setSortable(true);
        descriptionPreservationCol.setEditable(false);

        MethodsTableModel tableModel = new MethodsTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.pmfm.method"));

        // Initialisation du tableau
        initTable(table, true);

        // Les colonnes optionnelles sont invisibles
        descriptionPackagingCol.setVisible(false);
        descriptionPreparationCol.setVisible(false);
        descriptionPreservationCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<MethodsTableRowModel> getTableModel() {
        return (MethodsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageMethodsNationalTable();
    }
}
