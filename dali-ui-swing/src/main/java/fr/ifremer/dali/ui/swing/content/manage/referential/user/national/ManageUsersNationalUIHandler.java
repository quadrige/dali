package fr.ifremer.dali.ui.swing.content.manage.referential.user.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.user.menu.UserMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.user.privileges.PrivilegesDialogUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.user.table.UserRowModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.user.table.UserTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.user.table.UserTableUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ButtonCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.renderer.ButtonCellRenderer;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Users au niveau national
 */
public class ManageUsersNationalUIHandler extends AbstractDaliTableUIHandler<UserRowModel, UserTableUIModel, ManageUsersNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageUsersNationalUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        UserTableUIModel model = new UserTableUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(ManageUsersNationalUI ui) {
        initUI(ui);

        // hide 'apply filter'
        ui.getUserMenuUI().getHandler().enableContextFilter(false);

        // listen to search results
        ui.getUserMenuUI().getModel().addPropertyChangeListener(UserMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<PersonDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // lastname
        TableColumnExt lastnameCol = addColumn(UserTableModel.LASTNAME);
        lastnameCol.setSortable(true);

        // firstname
        TableColumnExt firstnameCol = addColumn(UserTableModel.FIRSTNAME);
        firstnameCol.setSortable(true);


        // department
        TableColumnExt depCol = addFilterableComboDataColumnToModel(
                UserTableModel.DEPARTMENT,
                getContext().getReferentialService().getDepartments(StatusFilter.ALL),
                true);
        depCol.setSortable(true);

        // email
        TableColumnExt emailCol = addColumn(UserTableModel.EMAIL);
        emailCol.setSortable(true);

        // phone
        TableColumnExt phoneCol = addColumn(UserTableModel.PHONE);
        phoneCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(UserTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(UserTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(UserTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                UserTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);

        // privileges
        TableColumnExt privilegesCol = addColumn(
                new ButtonCellEditor() {
                    @Override
                    public void onButtonCellAction(int row, int column) {
                        // show popup + set beans
                        PrivilegesDialogUI dialog = new PrivilegesDialogUI(getContext());
                        int rowModelIndex = getTable().convertRowIndexToModel(row);
                        UserRowModel rowModel = getTableModel().getEntry(rowModelIndex);
                        dialog.getModel().setUser(rowModel);
                        dialog.getModel().setEditable(false);
                        openDialog(dialog, new Dimension(600, 200));
                    }

                },
                new ButtonCellRenderer<Integer>(),
                UserTableModel.PRIVILEGES);
        privilegesCol.setMaxWidth(100);
        privilegesCol.setWidth(100);
        privilegesCol.setSortable(false);

        /* non visible cols */

        // matricule
        TableColumnExt idCol = addColumn(UserTableModel.REG_CODE);
        idCol.setSortable(true);
        fixColumnWidth(idCol, 80);

        // intranet login
        TableColumnExt intranetLoginCol = addColumn(UserTableModel.INTRANET_LOGIN);
        intranetLoginCol.setSortable(true);

        // extranet login
        TableColumnExt extranetLoginCol = addColumn(UserTableModel.EXTRANET_LOGIN);
        extranetLoginCol.setSortable(true);

        // address
        TableColumnExt addressCol = addColumn(UserTableModel.ADDRESS);
        addressCol.setSortable(true);

        // organism
        TableColumnExt organismCol = addColumn(UserTableModel.ORGANISM);
        organismCol.setSortable(true);

        // admin center
        TableColumnExt adminCenterCol = addColumn(UserTableModel.ADMIN_CENTER);
        adminCenterCol.setSortable(true);

        // site
        TableColumnExt siteCol = addColumn(UserTableModel.SITE);
        siteCol.setSortable(true);


        UserTableModel tableModel = new UserTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.user"), UserTableModel.PRIVILEGES);

        // Initialisation du tableau
        initTable(table, true);

        // all columns cannot be edited
        tableModel.setNoneEditableCols(
                UserTableModel.REG_CODE,
                UserTableModel.FIRSTNAME,
                UserTableModel.LASTNAME,
                UserTableModel.DEPARTMENT,
                UserTableModel.EMAIL,
                UserTableModel.PHONE,
                UserTableModel.STATUS,
                UserTableModel.ADDRESS,
                UserTableModel.INTRANET_LOGIN,
                UserTableModel.EXTRANET_LOGIN,
                UserTableModel.ORGANISM,
                UserTableModel.ADMIN_CENTER,
                UserTableModel.SITE);

        // optionnal columns are hidden
        idCol.setVisible(false);
        intranetLoginCol.setVisible(false);
        extranetLoginCol.setVisible(false);
        addressCol.setVisible(false);
        organismCol.setVisible(false);
        adminCenterCol.setVisible(false);
        siteCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);


        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<UserRowModel> getTableModel() {
        return (UserTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageUsersNationalTable();
    }

}
