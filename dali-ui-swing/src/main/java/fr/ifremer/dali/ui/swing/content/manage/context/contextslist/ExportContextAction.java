package fr.ifremer.dali.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.context.ContextDTO;
import fr.ifremer.dali.service.administration.context.ContextService;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;


/**
 * <p>ExportContextAction class.</p>
 *
 */
public class ExportContextAction extends AbstractDaliAction<ManageContextsListTableUIModel, ManageContextsListTableUI, ManageContextsListTableUIHandler> {

    private List<ContextDTO> contextsToExport;
    private File targetDirectory;
    private File exportFile;
    
	/**
	 * <p>Constructor for ExportContextAction.</p>
	 *
	 * @param handler a {@link ManageContextsListTableUIHandler} object.
	 */
	public ExportContextAction(ManageContextsListTableUIHandler handler) {
		super(handler, false);
	}
    
    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        contextsToExport = null;
        targetDirectory = null;
        exportFile = null;
        
        if (super.prepareAction()) {
            // Choose directory
            targetDirectory = chooseDirectory(
                    t("dali.action.context.export.title"),
                    t("dali.action.common.chooseDirectory.buttonLabel"));

            contextsToExport = getModel().getSelectedRows().stream().map(AbstractBeanUIModel::toBean).collect(Collectors.toList());
        }
        return (targetDirectory != null && CollectionUtils.isNotEmpty(contextsToExport));
    }

	/** {@inheritDoc} */
	@Override
	public void doAction() throws Exception {
        // create new filename
        String fileName = String.format(ContextService.EXPORT_FILE_FORMAT, Times.getFileSuffix());
        exportFile = new File(targetDirectory, fileName);

        // Export context
        getContext().getContextService().exportContexts(contextsToExport, exportFile);
	}
    
    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        // display success message
        displayInfoMessage(t("dali.common.success"), t("dali.action.context.export.success", exportFile.getAbsolutePath()));
    }
}
