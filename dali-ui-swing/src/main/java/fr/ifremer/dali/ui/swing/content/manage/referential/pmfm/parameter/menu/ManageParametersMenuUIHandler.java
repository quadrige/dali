package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.parameter.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;

import java.util.List;

/**
 * Controlleur du menu pour la gestion des Parameters au niveau local
 */
public class ManageParametersMenuUIHandler extends AbstractDaliUIHandler<ManageParametersMenuUIModel, ManageParametersMenuUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageParametersMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageParametersMenuUIModel model = new ManageParametersMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageParametersMenuUI ui) {
        initUI(ui);

        // Initialiser les combobox
        initComboBox();
    }

	/**
	 * Initialisation des combobox
	 */
	private void initComboBox() {

        List<ParameterDTO> parameters = getContext().getReferentialService().getParameters(StatusFilter.ALL);

		initBeanFilterableComboBox(
				getUI().getLabelCombo(),
                parameters,
				null,
                DecoratorService.NAME);
		
		initBeanFilterableComboBox(
				getUI().getCodeCombo(),
                parameters,
				null,
                null, // {code] is default
                DecoratorService.NAME); // Transcribed name as tooltip (Mantis #47110)
		
		initBeanFilterableComboBox(getUI().getStatusCombo(),
				getContext().getReferentialService().getStatus(StatusFilter.ALL),
				null);
		
        initBeanFilterableComboBox(getUI().getParameterGroupCombo(), 
				getContext().getReferentialService().getParameterGroup(StatusFilter.ACTIVE),
				null);

        DaliUIs.forceComponentSize(getUI().getLabelCombo());
        DaliUIs.forceComponentSize(getUI().getCodeCombo());
        DaliUIs.forceComponentSize(getUI().getStatusCombo());
        DaliUIs.forceComponentSize(getUI().getParameterGroupCombo());
	}

}
