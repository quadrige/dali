package fr.ifremer.dali.ui.swing.content.observation.operation.measurement;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsFilter;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableUIModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.ungrouped.OperationMeasurementsUngroupedTableUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controlleur pour l'onglet prelevements mesures.
 */
public class OperationMeasurementsTabUIHandler extends AbstractDaliUIHandler<OperationMeasurementsTabUIModel, OperationMeasurementsTabUI> implements TabHandler {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(OperationMeasurementsTabUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final OperationMeasurementsTabUIModel model = new OperationMeasurementsTabUIModel();
        ui.setContextValue(model);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(OperationMeasurementsTabUI ui) {
        initUI(ui);

        // init comboboxes
        initComboBox();

        getModel().setUngroupedTableUIModel(getUI().getUngroupedTable().getModel());
        getModel().setGroupedTableUIModel(getUI().getGroupedTable().getModel());

        // init image
        SwingUtil.setComponentWidth(getUI().getLeftImage(), ui.getMenuPanel().getPreferredSize().width * 9 / 10);
        getUI().getLeftImage().setScaled(true);

        // Initialiser listeners
        initListeners();

        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        // Initialisation des prelevements
        initBeanFilterableComboBox(
            getUI().getSelectionPrelevementsCombo(),
            getModel().getSamplings(),
            getModel().getSampling(),
            DecoratorService.CONCAT);

        /*
        // Initialisation des groupe de taxons
        initBeanFilterableComboBox(
                getUI().getSelectionGroupeTaxonCombo(),
                null,
                null, DecoratorService.NAME);

        getUI().getSelectionGroupeTaxonCombo().setActionEnabled(getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON_GROUP));
        getUI().getSelectionGroupeTaxonCombo().setActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                    return;
                }
                updateTaxonGroupComboBox(getModel().getTaxon(), true);
            }
        });
        updateTaxonGroupComboBox(null, false);

        // Intialisation des taxons
        initBeanFilterableComboBox(
                getUI().getSelectionTaxonCombo(),
                null,
                null);

        getUI().getSelectionTaxonCombo().setActionEnabled(getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON));
        getUI().getSelectionTaxonCombo().setActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                    return;
                }
                updateTaxonComboBox(getModel().getTaxonGroup(), true);
            }
        });
        updateTaxonComboBox(null, false);
*/

        DaliUIs.forceComponentSize(getUI().getSelectionPrelevementsCombo());
//        DaliUIs.forceComponentSize(getUI().getSelectionGroupeTaxonCombo());
//        DaliUIs.forceComponentSize(getUI().getSelectionTaxonCombo());
    }

    /**
     * Initialiser listeners de l ecran.
     */
    private void initListeners() {

        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_OBSERVATION_MODEL, evt -> load(getModel().getObservationModel()));

        // add model listener on selected sampling operation
        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_SAMPLING, evt -> {

            if (getModel().isAdjusting()) return;
            // reset context value
            getContext().setSelectedSamplingOperationId(getModel().getSampling() != null ? getModel().getSampling().getId() : null);
        });

        // add model listener on taxon group to filter taxons
//        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_TAXON_GROUP, new PropertyChangeListener() {
//
//            @Override
//            public void propertyChange(PropertyChangeEvent evt) {
//                if (getModel().isAdjusting()) return;
//                getModel().setAdjusting(true);
//                updateTaxonComboBox(evt.getNewValue() == null ? null : (TaxonGroupDTO) evt.getNewValue(), false);
//                getModel().setAdjusting(false);
//            }
//        });
//        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_TAXON, new PropertyChangeListener() {
//            @Override
//            public void propertyChange(PropertyChangeEvent evt) {
//                if (getModel().isAdjusting()) return;
//                getModel().setAdjusting(true);
//                updateTaxonGroupComboBox(evt.getNewValue() == null ? null : (TaxonDTO) evt.getNewValue(), false);
//                getModel().setAdjusting(false);
//            }
//        });

        // Add listeners on PROPERTY_ROWS_IN_ERROR to catch modifications and revalidate
        getModel().getUngroupedTableUIModel().addPropertyChangeListener(OperationMeasurementsUngroupedTableUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            // re validate
            getValidator().doValidate();
        });
        getModel().getGroupedTableUIModel().addPropertyChangeListener(OperationMeasurementsGroupedTableUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            // re validate
            getValidator().doValidate();
        });

        // Add listener on loading property and propagate to children
        getModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_LOADING, evt -> {
            getModel().getUngroupedTableUIModel().setLoading((Boolean) evt.getNewValue());
            getModel().getGroupedTableUIModel().setLoading((Boolean) evt.getNewValue());
        });

        // Listen tables models
        listenModelModify(getModel().getUngroupedTableUIModel());
        listenModelModify(getModel().getGroupedTableUIModel());

    }

    /*
    private void updateTaxonGroupComboBox(TaxonDTO taxon, boolean forceNoFilter) {

        getUI().getSelectionGroupeTaxonCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON_GROUP));

        List<TaxonGroupDTO> taxonGroups = getContext().getObservationService().getAvailableTaxonGroups(taxon, forceNoFilter);
        getUI().getSelectionGroupeTaxonCombo().setData(taxonGroups);

        if (CollectionUtils.isEmpty(taxonGroups) && getModel().getTaxonGroup() != null) {
            getModel().setTaxonGroup(null);
        } else if (taxonGroups.size() == 1) {
            getModel().setTaxonGroup(taxonGroups.get(0));
        }

    }

    private void updateTaxonComboBox(TaxonGroupDTO taxonGroup, boolean forceNoFilter) {

        getUI().getSelectionTaxonCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON));

        List<TaxonDTO> taxons = getContext().getObservationService().getAvailableTaxons(taxonGroup, forceNoFilter);
        getUI().getSelectionTaxonCombo().setData(taxons);

        if (CollectionUtils.isEmpty(taxons) && getModel().getTaxon() != null) {
            getModel().setTaxon(null);
        } else if (taxons.size() == 1) {
            getModel().setTaxon(taxons.get(0));
        }

    }
    */

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingValidator<OperationMeasurementsTabUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * Load observation.
     *
     * @param survey Observation
     */
    private void load(final ObservationUIModel survey) {

        if (survey == null || survey.getId() == null) {
            return;
        }

        getModel().setAdjusting(true);

        List<PmfmStrategyDTO> pmfmStrategies = DaliBeans.filterCollection(survey.getPmfmStrategies(), (Predicate<PmfmStrategyDTO>) input -> input != null && input.isSampling());

        // list available samplings
        getUI().getSelectionPrelevementsCombo().setData(getModel().getSamplings());

        // Load ungrouped data (up table)
        {

            // load pmfms for ungrouped measurements
            List<PmfmDTO> pmfms = pmfmStrategies.stream()
                .filter(pmfmStrategy -> !pmfmStrategy.isGrouping())
                .map(PmfmStrategyDTO::getPmfm)
                .collect(Collectors.toList());

            // Load other Pmfms in sampling operations
            if (CollectionUtils.isNotEmpty(getModel().getSamplings())) {
                // populate pmfms from strategy to sampling operation and vice versa
                getModel().getSamplings().forEach(samplingOperation -> DaliBeans.fillListsEachOther(samplingOperation.getPmfms(), pmfms));
            }

            // filter out pmfms under moratorium
            filterPmfmsUnderMoratorium(pmfms, survey);

            // Set model properties
            getModel().getUngroupedTableUIModel().setPmfms(pmfms);

            getModel().getUngroupedTableUIModel().setSurvey(survey);
        }

        // Load grouped data (down table)
        {

            List<PmfmDTO> individualPmfms = Lists.newArrayList();
            // find pmfms having taxon unique constraint in strategy
            List<PmfmDTO> uniquePmfms = Lists.newArrayList();
            for (PmfmStrategyDTO pmfmStrategy : pmfmStrategies) {
                if (pmfmStrategy.isGrouping()) {
                    individualPmfms.add(pmfmStrategy.getPmfm());
                    if (pmfmStrategy.isUnique()) {
                        uniquePmfms.add(pmfmStrategy.getPmfm());
                    }
                }
            }

            // Load other Pmfms in sampling operations
            if (CollectionUtils.isNotEmpty(getModel().getSamplings())) {
                // populate pmfms from strategy to sampling operation and vice versa
                getModel().getSamplings().forEach(samplingOperation -> DaliBeans.fillListsEachOther(samplingOperation.getIndividualPmfms(), individualPmfms));
            }

            // filter out pmfms under moratorium
            filterPmfmsUnderMoratorium(individualPmfms, survey);

            // Set model properties
            getModel().getGroupedTableUIModel().setPmfms(individualPmfms);
            getModel().getGroupedTableUIModel().setUniquePmfms(uniquePmfms);

            getModel().getGroupedTableUIModel().setSurvey(survey);
        }

        //update selected sampling
        if (getContext().getSelectedSamplingOperationId() != null && getModel().getSamplings() != null) {
            for (SamplingOperationDTO samplingOperation : getModel().getSamplings()) {
                if (getContext().getSelectedSamplingOperationId().equals(samplingOperation.getId())) {
                    getModel().setSampling(samplingOperation);
                    break;
                }
            }
        }

        // Disable this Tab to avoid bad behavior is no sampling found
        if (CollectionUtils.isEmpty(getModel().getSamplings())) {
            setEnabled(getUI(), false);
        }

        // execute doSearch to simulate loading measurements data
        doSearch();

        getModel().setAdjusting(false);
    }

    private void filterPmfmsUnderMoratorium(List<PmfmDTO> pmfms, ObservationUIModel survey) {
        if (CollectionUtils.isEmpty(survey.getPmfmsUnderMoratorium()))
            return;

        pmfms.removeIf(pmfm -> survey.getPmfmsUnderMoratorium().stream().anyMatch(moratoriumPmfm -> DaliBeans.isPmfmEquals(pmfm, moratoriumPmfm)));
    }

    /**
     * <p>clearSearch.</p>
     */
    public void clearSearch() {

        // Suppression choix du groupe de taxons
//        getModel().setTaxonGroup(null);

        // Suppression choix du taxon
//        getModel().setTaxon(null);

        // Suppression choix prelevement
        getModel().setSampling(null);

    }

    /**
     * <p>doSearch.</p>
     */
    public void doSearch() {

        // set taxon group and taxon filter
        getModel().getUngroupedTableUIModel().setSamplingFilter(getModel().getSampling());

        OperationMeasurementsFilter measurementFilter = new OperationMeasurementsFilter();
//        measurementFilter.setTaxonGroup(getModel().getTaxonGroup());
//        measurementFilter.setTaxon(getModel().getTaxon());
        measurementFilter.setSamplingOperation(getModel().getSampling());
        getModel().getGroupedTableUIModel().setMeasurementFilter(measurementFilter);
    }

    public void save() {

        try {

            // Disable filter
            getModel().getUngroupedTableUIModel().setSamplingFilter(null);
            getModel().getGroupedTableUIModel().setMeasurementFilter(null);

            // save ungrouped measurements
            getUI().getUngroupedTable().getHandler().save();

            // save grouped measurements
            getUI().getGroupedTable().getHandler().save();

        } finally {

            doSearch();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onShowTab(int currentIndex, int newIndex) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onRemoveTab() {
        return false;
    }

}
