package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;

import java.util.List;

/**
 * Controlleur du menu pour la gestion des Quadruplets au niveau National
 */
public class PmfmMenuUIHandler extends ReferentialMenuUIHandler<PmfmMenuUIModel, PmfmMenuUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final PmfmMenuUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(new PmfmMenuUIModel());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final PmfmMenuUI ui) {
        super.afterInit(ui);

        // Initialiser les combobox
        initComboBox();
    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getNameEditor().setEnabled(enabled);
        getUI().getParametersCombo().setEnabled(enabled);
        getUI().getMatricesCombo().setEnabled(enabled);
        getUI().getFractionsCombo().setEnabled(enabled);
        getUI().getMethodsCombo().setEnabled(enabled);
        getUI().getUnitsCombo().setEnabled(enabled);
        getUI().getStatusCombo().setEnabled(enabled);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllPmfmFilters();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        initBeanFilterableComboBox(
                getUI().getParametersCombo(),
                getContext().getReferentialService().getParameters(StatusFilter.ALL),
                null,
                DecoratorService.CODE_NAME);

        initBeanFilterableComboBox(
                getUI().getMatricesCombo(),
                getContext().getReferentialService().getMatrices(StatusFilter.ALL),
                null);

        initBeanFilterableComboBox(
                getUI().getFractionsCombo(),
                getContext().getReferentialService().getFractions(StatusFilter.ALL),
                null);

        initBeanFilterableComboBox(
                getUI().getMethodsCombo(),
                getContext().getReferentialService().getMethods(StatusFilter.ALL),
                null);

        initBeanFilterableComboBox(
                getUI().getUnitsCombo(),
                getContext().getReferentialService().getUnits(StatusFilter.ALL),
                null,
                DecoratorService.WITH_SYMBOL);

        initBeanFilterableComboBox(getUI().getStatusCombo(),
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                null);

        DaliUIs.forceComponentSize(getUI().getNameEditor());
        DaliUIs.forceComponentSize(getUI().getParametersCombo());
        DaliUIs.forceComponentSize(getUI().getMatricesCombo());
        DaliUIs.forceComponentSize(getUI().getFractionsCombo());
        DaliUIs.forceComponentSize(getUI().getMethodsCombo());
        DaliUIs.forceComponentSize(getUI().getUnitsCombo());
        DaliUIs.forceComponentSize(getUI().getStatusCombo());
    }
}
