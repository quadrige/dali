package fr.ifremer.dali.ui.swing.action;

import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.DialogHelper;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 27/06/2019.
 */
public abstract class AbstractUpdateReferentialAction<M extends AbstractBeanUIModel, UI extends DaliUI<M, ?>, H extends AbstractDaliUIHandler<M, UI>>
        extends AbstractDaliAction<M, UI, H> {

    private static final Log LOG = LogFactory.getLog(AbstractUpdateReferentialAction.class);

    /**
     * <p>Constructor for AbstractDaliAction.</p>
     *
     * @param handler  a H object.
     * @param hideBody a boolean.
     */
    public AbstractUpdateReferentialAction(H handler, boolean hideBody) {
        super(handler, hideBody);
        setActionDescription(t("quadrige3.action.synchro.import.title"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("Referential update have been proposed");
        }

        return getContext().getDialogHelper().showOptionDialog(null,
                ApplicationUIUtil.getHtmlString(t("dali.action.common.import.referential.beforeModify.confirm")),
                t("dali.action.common.import.referential.beforeModify.title"),
                JOptionPane.WARNING_MESSAGE,
                DialogHelper.CUSTOM_OPTION,
                t("dali.common.import"),
                t("dali.common.cancel")
        ) == JOptionPane.OK_OPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Referential update have been confirmed");
        }

        // Import referential
        doImportReferential();

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Referential update done.");
        }
    }
}
