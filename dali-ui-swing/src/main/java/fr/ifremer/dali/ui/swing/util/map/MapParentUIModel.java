package fr.ifremer.dali.ui.swing.util.map;

/**
 * @author peck7 on 28/07/2020.
 */
public interface MapParentUIModel {

    String EVENT_OPEN_FULLSCREEN = "eventOpenFullscreen";
    String EVENT_CLOSE_FULLSCREEN = "eventCloseFullscreen";

    MapUIModel getMapUIModel();

    void fireOpenFullScreenEvent();
//    default void fireOpenFullScreenEvent() {
//        fireEvent(EVENT_OPEN_FULLSCREEN);
//    }

    void fireCloseFullScreenEvent();
//    default void fireCloseFullScreenEvent() {
//        fireEvent(EVENT_CLOSE_FULLSCREEN);
//    }

//    default void fireEvent(String event) {
//        if (this instanceof AbstractBeanUIModel) {
//            AbstractBeanUIModel thisModel = (AbstractBeanUIModel) this;
//            boolean modify = thisModel.isModify();
//            thisModel.firePropertyChanged(event, null, true);
//            thisModel.setModify(modify);
//        }
//    }
}
