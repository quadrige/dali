package fr.ifremer.dali.ui.swing.content.home.map;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.technical.Geometries;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.map.*;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.ui.swing.util.map.MapBuilder;
import fr.ifremer.dali.ui.swing.util.map.layer.*;
import fr.ifremer.dali.ui.swing.util.map.layer.tile.MapTileLayer;
import fr.ifremer.dali.ui.swing.util.map.layer.wms.GraticuleWMSLayer;
import fr.ifremer.dali.ui.swing.util.map.layer.wms.WMSMapLayer;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.collection.CollectionFeatureSource;
import org.geotools.data.ows.CRSEnvelope;
import org.geotools.data.ows.WMSCapabilities;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.wms.WMSUtils;
import org.geotools.data.wmts.client.WMTSTileService;
import org.geotools.data.wmts.model.WMTSCapabilities;
import org.geotools.data.wmts.model.WMTSLayer;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.map.WMSLayer;
import org.geotools.ows.ServiceException;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.styling.*;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.geometry.BoundingBox;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CRSAuthorityFactory;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import javax.swing.UIManager;
import java.awt.Color;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Surveys Map builder.
 * Initialise map content, behaviors, etc...
 *
 * @author peck7 on 13/09/2017.
 */
public class SurveysMapBuilder implements MapBuilder {

    private static final Log LOG = LogFactory.getLog(SurveysMapBuilder.class);

    // configuration
    private final DaliConfiguration configuration;

    private MapMode currentMapMode;
    private CoordinateReferenceSystem targetCRS;

    // constants
    private static final String GEOMETRY = "geometry";
    private static final String LABEL = "label";
    private static final String POINT_TYPE = "pointType";
    private static final String POINT_TYPE_START = "pointTypeStart";
    private static final String POINT_TYPE_END = "pointTypeEnd";
    private static final String INTERNAL = "internal";
    private static final String LOCATION_LAYER_NAME_PREFIX = "location_";
    private static final String SURVEY_LAYER_NAME_PREFIX = "survey_";
    private static final String OPERATION_LAYER_NAME_PREFIX = "operation_";
    private static final String LAYER_NAME_SUFFIX = "_Layer";
    private static final String POINT_LAYER_NAME_SUFFIX = "_PointLayer";
    private static final String LINE_LAYER_NAME_SUFFIX = "_LineLayer";

    // shape mode constants
    private static final String SHAPE_FILE_EXTENSION = ".shp";
    private static final String SHAPE_RESOURCE_DIRECTORY = "shapes";

    // wms mode constants

    // wmts mode constants

    // osm mode constants

    // builders & factories
    private static StyleBuilder SB = new StyleBuilder();
    private static StyleFactory SF = CommonFactoryFinder.getStyleFactory(null);
    private static FilterFactory2 FF = CommonFactoryFinder.getFilterFactory2(null);

    // Default style, color, ...
    private SimpleFeatureType locationFeatureType;
    private SimpleFeatureType surveyFeatureType;
    private SimpleFeatureType operationFeatureType;

    private static Color colorEarth = Color.decode("#8C8C8C");
    private static Color colorWater = Color.decode("#B6EDF0");

    private static Fill fillBlack = SF.createFill(FF.literal(Color.BLACK));
    private static Stroke strokeBlack = SF.createStroke(FF.literal(Color.BLACK), FF.literal(1.0f));
    private static Halo haloWhite = SB.createHalo(Color.WHITE, 3);

    private static PointPlacement middleTopPlacement = SB.createPointPlacement(SB.createAnchorPoint(0.5, 0), SB.createDisplacement(0, 10), SB.literalExpression(0));
    private static LinePlacement labelLinePlacement = SB.createLinePlacement(16);

    private Style styleLocationPoint;
    private Style styleLocationLine;
    private Style styleLocationPolygon;
    private Style styleSurveyPoint;
    private Style styleSurveyLine;
    private Style styleSurveyLinePoints;
    private Style styleOperationPoint;

    public SurveysMapBuilder(DaliConfiguration configuration) {
        this.configuration = configuration;
        init();
    }

    public static void main(String... args) {
        // TEST
        SurveysMapBuilder mapBuilder = new SurveysMapBuilder(null);
        mapBuilder.buildNewMapContent(MapMode.EMBEDDED_SHAPE_MAP_MODE);
    }

    private void init() {

        // Initialize types
        {
            // - location
            SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
            typeBuilder.setName("locationFeatureType");
            typeBuilder.setCRS(DefaultGeographicCRS.WGS84);
            typeBuilder.setDefaultGeometry(GEOMETRY);
            typeBuilder.add(GEOMETRY, Geometry.class);
            typeBuilder.add(LABEL, String.class);
            locationFeatureType = typeBuilder.buildFeatureType();
        }
        {
            // - survey
            SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
            typeBuilder.setName("surveyFeatureType");
            typeBuilder.setCRS(DefaultGeographicCRS.WGS84);
            typeBuilder.setDefaultGeometry(GEOMETRY);
            typeBuilder.add(GEOMETRY, Geometry.class);
            typeBuilder.add(LABEL, String.class);
            typeBuilder.add(POINT_TYPE, String.class);
            surveyFeatureType = typeBuilder.buildFeatureType();
        }
        {
            // - operation
            SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
            typeBuilder.setName("operationFeatureType");
            typeBuilder.setCRS(DefaultGeographicCRS.WGS84);
            typeBuilder.setDefaultGeometry(GEOMETRY);
            typeBuilder.add(GEOMETRY, Geometry.class);
            typeBuilder.add(LABEL, String.class);
            operationFeatureType = typeBuilder.buildFeatureType();
        }

        // Initialize font
        java.awt.Font uiFont = UIManager.getFont("Label.font");
        Font font;
        if (uiFont != null) {
            font = SF.createFont(FF.literal(uiFont.getFamily()), FF.literal("normal"), FF.literal("bold"), FF.literal(uiFont.getSize()));
        } else {
            font = SF.getDefaultFont();
            font.setSize(FF.literal(14));
        }

        // Initialize graphic styles
        {
            // location style
            Color color = Color.BLUE;
            Stroke stroke = SF.createStroke(FF.literal(color), FF.literal(1.0f));
            Fill fill = SF.createFill(FF.literal(color.brighter().brighter()), FF.literal(.5f));

            Mark mark = SF.createMark(FF.literal(StyleBuilder.MARK_CIRCLE), stroke, fill, null, null);
            Graphic graphic = SF.createDefaultGraphic();
            graphic.graphicalSymbols().clear();
            graphic.graphicalSymbols().add(mark);
            graphic.setSize(FF.literal(10));

            // location Point style
            TextSymbolizer textPointSymbolizer = SF.createTextSymbolizer(fillBlack, new Font[]{font}, haloWhite, FF.property(LABEL), middleTopPlacement, null);
            textPointSymbolizer.setPriority(FF.literal(1));
            styleLocationPoint = createStyle(
                    "styleLocationPoint",
                    createPointSymbolizer("symbolizerLocationPoint", graphic),
                    textPointSymbolizer);

            // location line style
            TextSymbolizer textLineSymbolizer = SF.createTextSymbolizer(fillBlack, new Font[]{font}, haloWhite, FF.property(LABEL), labelLinePlacement, null);
            textLineSymbolizer.setPriority(FF.literal(1));
            // Invert both symbolizer to conserve text rendering on too small line (Mantis #51902)
            styleLocationLine = createStyle(
                    "styleLocationLine",
                textLineSymbolizer,
                createLineSymbolizer("symbolizerLocationLine", stroke));

            // location Polygon style
            TextSymbolizer textPolygonSymbolizer = SF.createTextSymbolizer(fillBlack, new Font[]{font}, haloWhite, FF.property(LABEL), middleTopPlacement, null);
            textPolygonSymbolizer.setPriority(FF.literal(1));
            textPolygonSymbolizer.getOptions().put(TextSymbolizer.GOODNESS_OF_FIT_KEY, String.valueOf(0.0d));
            styleLocationPolygon = createStyle(
                    "styleLocationPolygon",
                    createPolygonSymbolizer("symbolizerLocationPolygon", stroke, fill),
                    textPolygonSymbolizer);

        }

        {
            // survey style
            Color colorLine = Color.GREEN;
            Color colorPoint = Color.BLUE;
            Color colorStart = Color.GREEN;
            Color colorEnd = Color.RED;
            Stroke stroke = SF.createStroke(FF.literal(colorLine), FF.literal(3));
            Fill fillPoint = SF.createFill(FF.literal(colorPoint), FF.literal(1.0f));
            Fill fillStart = SF.createFill(FF.literal(colorStart), FF.literal(1.0f));
            Fill fillEnd = SF.createFill(FF.literal(colorEnd), FF.literal(1.0f));

            // survey Line style
            TextSymbolizer textSymbolizer = SF.createTextSymbolizer(fillBlack, new Font[]{font}, haloWhite, FF.property(LABEL), labelLinePlacement, null);
            textSymbolizer.setPriority(FF.literal(2));
            styleSurveyLine = createStyle(
                    "styleSurveyLine",
                    createLineSymbolizer("symbolizerSurveyLine", stroke),
                    textSymbolizer);

            // survey Line Points style

            // start point of the line
            Mark markStart = SF.createMark(FF.literal(StyleBuilder.MARK_TRIANGLE), strokeBlack, fillStart, null, null);
            Graphic graphicStart = SF.createDefaultGraphic();
            graphicStart.graphicalSymbols().clear();
            graphicStart.graphicalSymbols().add(markStart);
            graphicStart.setSize(FF.literal(10));
            Symbolizer pointSymbolizerStart = createPointSymbolizer("symbolizerSurveyPointStart", graphicStart);

            // end point of the line
            Mark markEnd = SF.createMark(FF.literal(StyleBuilder.MARK_X), strokeBlack, fillEnd, null, null);
            Graphic graphicEnd = SF.createDefaultGraphic();
            graphicEnd.graphicalSymbols().clear();
            graphicEnd.graphicalSymbols().add(markEnd);
            graphicEnd.setSize(FF.literal(10));
            Symbolizer pointSymbolizerEnd = createPointSymbolizer("symbolizerSurveyPointEnd", graphicEnd);

            // Create start/end rule
            Rule ruleStart = SB.createRule(pointSymbolizerStart);
            Filter filterStart = SB.getFilterFactory().equals(FF.property(POINT_TYPE), FF.literal(POINT_TYPE_START));
            ruleStart.setFilter(filterStart);

            Rule ruleEnd = SB.createRule(pointSymbolizerEnd);
            Filter filterEnd = SB.getFilterFactory().equals(FF.property(POINT_TYPE), FF.literal(POINT_TYPE_END));
            ruleEnd.setFilter(filterEnd);

            // Create the style
            styleSurveyLinePoints = createStyle("styleSurveyLinePoints", ruleStart, ruleEnd);

            // survey Point only style
            TextSymbolizer textPointSymbolizer = SF.createTextSymbolizer(fillBlack, new Font[]{font}, haloWhite, FF.property(LABEL), middleTopPlacement, null);
            textPointSymbolizer.setPriority(FF.literal(2));
            Mark markPoint = SF.createMark(FF.literal(StyleBuilder.MARK_TRIANGLE), strokeBlack, fillPoint, null, null);
            Graphic graphicPoint = SF.createDefaultGraphic();
            graphicPoint.graphicalSymbols().clear();
            graphicPoint.graphicalSymbols().add(markPoint);
            graphicPoint.setSize(FF.literal(10));

            styleSurveyPoint = createStyle("styleSurveyPoint", createPointSymbolizer("symbolizerSurveyPoint", graphicPoint), textPointSymbolizer);
        }

        {
            // operation Point style
            Color color = Color.YELLOW;
            Fill fill = SF.createFill(FF.literal(color.brighter().brighter()), FF.literal(1.0f));

            Mark mark = SF.createMark(FF.literal(StyleBuilder.MARK_SQUARE), strokeBlack, fill, null, null);
            Graphic graphic = SF.createDefaultGraphic();
            graphic.graphicalSymbols().clear();
            graphic.graphicalSymbols().add(mark);
            graphic.setSize(FF.literal(10));

            TextSymbolizer textSymbolizer = SF.createTextSymbolizer(fillBlack, new Font[]{font}, haloWhite, FF.property(LABEL), middleTopPlacement, null);
            textSymbolizer.setPriority(FF.literal(3));
            styleOperationPoint = createStyle("styleOperationPoint", createPointSymbolizer("symbolizerOperationPoint", graphic), textSymbolizer);

            // no operation Line style
        }
    }

    @Override
    public MapContent buildNewMapContent(MapMode preferredMode) {

        MapContent mapContent = new MapContent();

        try {

            currentMapMode = Optional.ofNullable(preferredMode).orElse(configuration.getPreferredMapMode());
            if (currentMapMode == null) {
                throw new DaliTechnicalException("Unable to determine map mode");
            }

            MapConfiguration mapConfiguration = currentMapMode.getMapConfiguration();

            if (mapConfiguration.isOnline()) {

                // if server not reachable, try another one
                if (!Maps.isMapServerReachable(currentMapMode)) return buildNewMapContent(getFirstOfflineMode());

                if (mapConfiguration instanceof WMSMapConfiguration) {

                    // Call server capabilities and get layers
                    WMSCapabilities wmsCapabilities = ((WMSMapConfiguration) mapConfiguration).getCapabilities();
                    org.geotools.data.ows.Layer[] layers = WMSUtils.getNamedLayers(wmsCapabilities);

                    // Create layers
                    for (MapLayerDefinition layerDefinition : currentMapMode.getMapLayerDefinitions()) {
                        for (org.geotools.data.ows.Layer layer : layers) {
                            if (layerDefinition.getName().equals(layer.getName())) {

                                WMSLayer wmsLayer;
                                if (layerDefinition instanceof GraticuleLayerDefinition) {
                                    wmsLayer = new GraticuleWMSLayer(((WMSMapConfiguration) mapConfiguration).getWebService(), layer);
                                } else {
                                    wmsLayer = new WMSMapLayer(((WMSMapConfiguration) mapConfiguration).getWebService(), layer);
                                }
                                wmsLayer.setTitle(layer.getName());

                                mapContent.addLayer(wmsLayer);
                            }
                        }
                    }
                    mapContent.addLayer(new ScaleLayer());

                } else if (mapConfiguration instanceof WMTSMapConfiguration) {

                    WMTSCapabilities wmtsCapabilities = ((WMTSMapConfiguration) mapConfiguration).getCapabilities();
                    WMTSLayer layer = wmtsCapabilities.getLayer(currentMapMode.getBaseLayerName());

                    // Restrict bbox
                    // BoundingBoxes doit est <= à LatLonBoundingBox pour que le rendu passe en zoom out en dehors des limites
                    CRSEnvelope limitEnvelope = new CRSEnvelope(configuration.getMapProjection().getEnvelope());
                    layer.setBoundingBoxes(limitEnvelope);
                    layer.setLatLonBoundingBox(limitEnvelope);

                    WMTSTileService service = new WMTSTileService(
                            currentMapMode.getUrl(),
                            wmtsCapabilities.getType(),
                            layer,
                            "",
                            wmtsCapabilities.getMatrixSet(configuration.getMapProjection().getCode()));
                    // Si on utilise le jeu de tuile EPSG:4326 (WGS84 standard) ça déconne grave sur certaine partie de la carte,
                    // alors que le EPSG:3857 (WGS84 pseudo mercator) ça passe bien.

                    mapContent.addLayer(new MapTileLayer(service, currentMapMode.getBaseLayerName()));

                    // add direct graticule because wmts don't have it
                    mapContent.addLayer(new GraticuleDirectLayer());
                    mapContent.addLayer(new ScaleLayer());

                } else if (mapConfiguration instanceof OSMBasedMapConfiguration) {

                    // OpenStreetMap
                    mapContent.addLayer(new MapTileLayer(((OSMBasedMapConfiguration) mapConfiguration).getTileService(), currentMapMode.getBaseLayerName()));

                    // add direct graticule because wmts don't have it
                    mapContent.addLayer(new GraticuleDirectLayer());
                    mapContent.addLayer(new ScaleLayer());

                }

            } else {

                // Create layers
                for (MapLayerDefinition layerDefinition : currentMapMode.getMapLayerDefinitions()) {
                    Layer layer = newMapLayer(layerDefinition.getName(), colorEarth.darker(), 2, colorEarth, 1f);
                    mapContent.addLayer(layer);
                }

                // Graticule and scale
                mapContent.addLayer(new GraticuleDirectLayer());
                mapContent.addLayer(new ScaleLayer());

            }

            // Apply view CRS
            CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
            MapProjection targetProjection = configuration.getMapProjection();

            // FIXME : for Sextant WMS, the PSEUDO MERCATOR fails to project
//            if (currentMapMode == MapMode.SEXTANT_WMS_MAP_MODE)
//                targetProjection = MapProjection.WGS84;

            targetCRS = factory.createCoordinateReferenceSystem(targetProjection.getCode());
            mapContent.getViewport().setCoordinateReferenceSystem(targetCRS);

        } catch (IOException | ServiceException | FactoryException e) {
            throw new DaliTechnicalException(e);
        }

        return mapContent;
    }

    @Override
    public CoordinateReferenceSystem getTargetCRS() {
        return targetCRS;
    }

    @Override
    public MapMode getCurrentMapMode() {
        if (currentMapMode == null) currentMapMode = configuration.getPreferredMapMode();
        return currentMapMode;
    }

    @Override
    public boolean checkOnlineReachable() {
        return Maps.isMapServerReachable(getCurrentMapMode())
                || Maps.isMapServerReachable(configuration.getPreferredMapMode());
    }

    @Override
    public Color getBackgroundColor() {
        return colorWater;
    }

    private MapMode getFirstOfflineMode() {
        return Arrays.stream(MapMode.values()).filter(mapMode -> !mapMode.isOnline()).findFirst().orElse(null);
    }

    private Layer newMapLayer(String shapeName, Color outlineColor, int outlineWidth, Color fillColor, float fillOpacity) throws IOException {

        FileDataStore store = FileDataStoreFinder.getDataStore(getShapeURL(shapeName));
        SimpleFeatureSource source = store.getFeatureSource();

        Stroke stroke = SF.createStroke(FF.literal(outlineColor), FF.literal(outlineWidth));
        Fill fill = Fill.NULL;
        if (fillColor != null) {
            fill = SF.createFill(FF.literal(fillColor), FF.literal(fillOpacity));
        }

        Style style = createStyle("styleMapPolygon", createPolygonSymbolizer("symbolizerMapPolygon", stroke, fill));
        return new MapFeatureLayer(source, style, shapeName);

    }

    private URL getShapeURL(String shapeName) {

        Assert.notNull(shapeName);
        if (!shapeName.endsWith(SHAPE_FILE_EXTENSION)) shapeName += SHAPE_FILE_EXTENSION;

        // Find resource
        URL shapeUrl = SurveysMapUIHandler.class.getResource(String.format("/%s/%s", SHAPE_RESOURCE_DIRECTORY, shapeName));
        if (shapeUrl == null) {
            throw new DaliTechnicalException(shapeName + " not found");
        }
        return shapeUrl;
    }

    /**
     * Build the data layer collection from a survey
     *
     * @param dataObject the data object (the survey)
     * @return the collection of data layer
     */
    @Override
    public DataLayerCollection buildDataLayerCollection(Object dataObject) {

        Assert.notNull(dataObject);
        Assert.isInstanceOf(SurveyDTO.class, dataObject);

        SurveyDTO survey = (SurveyDTO) dataObject;

        SurveyLayerCollection dataLayerCollection = new SurveyLayerCollection();

        // Location layer
        LocationDTO location = survey.getLocation();
        if (Geometries.isValid(location.getCoordinate())) {

            Geometry geometry = Geometries.getGeometry(location.getCoordinate().getWkt()); // Mantis #40640 : use initial WKT geometry
            SimpleFeatureBuilder builder = new SimpleFeatureBuilder(locationFeatureType);
            builder.add(geometry);
            builder.add(location.getName());
            SimpleFeature feature = builder.buildFeature(location.getId().toString());
            DefaultFeatureCollection collection = new DefaultFeatureCollection(INTERNAL, locationFeatureType);
            collection.add(feature);
            SimpleFeatureSource source = new CollectionFeatureSource(collection);

            int dimension = geometry.getDimension();
            Style style = dimension == 2 ? styleLocationPolygon : dimension == 1 ? styleLocationLine : styleLocationPoint;

            dataLayerCollection.setLocationLayer(
                    new DataFeatureLayer(dataLayerCollection, location.getId(), source, style, LOCATION_LAYER_NAME_PREFIX + location.getId() + LAYER_NAME_SUFFIX));
        }

        // survey layer
        if (Geometries.isValid(survey.getCoordinate())) {

            DataFeatureLayer layerLine = null;
            DataFeatureLayer layerPoint;

            if (Geometries.isPoint(survey.getCoordinate())) {
                // Only a point
                Point pointStart = Geometries.createPoint(survey.getCoordinate().getMinLongitude(), survey.getCoordinate().getMinLatitude());
                DefaultFeatureCollection collection = new DefaultFeatureCollection(INTERNAL, surveyFeatureType);
                SimpleFeatureBuilder builder = new SimpleFeatureBuilder(surveyFeatureType);
                builder.add(pointStart);
                builder.add(getSurveyGeometryLabel(survey));
                builder.add(POINT_TYPE_START);
                collection.add(builder.buildFeature(POINT_TYPE_START + "_" + survey.getId().toString()));
                SimpleFeatureSource source = new CollectionFeatureSource(collection);
                layerPoint = new DataFeatureLayer(dataLayerCollection, survey.getId(), source, styleSurveyPoint, SURVEY_LAYER_NAME_PREFIX + survey.getId().toString() + POINT_LAYER_NAME_SUFFIX);

            } else {
                {
                    // survey line
                    Geometry geometry = Geometries.getGeometry(survey.getCoordinate());
                    DefaultFeatureCollection collection = new DefaultFeatureCollection(INTERNAL, surveyFeatureType);
                    SimpleFeatureBuilder builder = new SimpleFeatureBuilder(surveyFeatureType);
                    builder.add(geometry);
                    builder.add(getSurveyGeometryLabel(survey));
                    collection.add(builder.buildFeature(survey.getId().toString()));
                    SimpleFeatureSource source = new CollectionFeatureSource(collection);
                    layerLine = new DataFeatureLayer(dataLayerCollection, survey.getId(), source, styleSurveyLine, SURVEY_LAYER_NAME_PREFIX + survey.getId().toString() + LINE_LAYER_NAME_SUFFIX);
                }
                {
                    // survey points
                    Point pointStart = Geometries.createPoint(survey.getCoordinate().getMinLongitude(), survey.getCoordinate().getMinLatitude());
                    Point pointEnd = Geometries.createPoint(survey.getCoordinate().getMaxLongitude(), survey.getCoordinate().getMaxLatitude());
                    DefaultFeatureCollection collection = new DefaultFeatureCollection(INTERNAL, surveyFeatureType);
                    SimpleFeatureBuilder builder = new SimpleFeatureBuilder(surveyFeatureType);
                    builder.add(pointStart);
                    builder.add(null);
                    builder.add(POINT_TYPE_START);
                    collection.add(builder.buildFeature(POINT_TYPE_START + "_" + survey.getId().toString()));
                    builder = new SimpleFeatureBuilder(surveyFeatureType);
                    builder.add(pointEnd);
                    builder.add(null);
                    builder.add(POINT_TYPE_END);
                    collection.add(builder.buildFeature(POINT_TYPE_END + "_" + survey.getId().toString()));
                    SimpleFeatureSource source = new CollectionFeatureSource(collection);
                    layerPoint = new DataFeatureLayer(dataLayerCollection, survey.getId(), source, styleSurveyLinePoints, SURVEY_LAYER_NAME_PREFIX + survey.getId().toString() + POINT_LAYER_NAME_SUFFIX);
                }
            }

            dataLayerCollection.setSurveyLayer(layerLine, layerPoint);

        }

        // operations layer
        if (!survey.isSamplingOperationsEmpty()) {
            for (SamplingOperationDTO operation : survey.getSamplingOperations()) {

                if (Geometries.isValid(operation.getCoordinate())) {

                    Point point = Geometries.createPoint(operation.getCoordinate().getMinLongitude(), operation.getCoordinate().getMinLatitude());
                    DefaultFeatureCollection collection = new DefaultFeatureCollection(INTERNAL, operationFeatureType);
                    SimpleFeatureBuilder builder = new SimpleFeatureBuilder(operationFeatureType);
                    builder.add(point);
                    builder.add(getOperationGeometryLabel(operation));
                    collection.add(builder.buildFeature(operation.getId().toString()));
                    SimpleFeatureSource source = new CollectionFeatureSource(collection);

                    dataLayerCollection.addOperationLayer(
                            null,
                            new DataFeatureLayer(dataLayerCollection, operation.getId(), source, styleOperationPoint, OPERATION_LAYER_NAME_PREFIX + operation.getId().toString() + POINT_LAYER_NAME_SUFFIX));
                }
            }
        }

        return dataLayerCollection;
    }

    private String getSurveyGeometryLabel(SurveyDTO survey) {
        return survey.getName() + " - " + Dates.formatDate(survey.getDate(), configuration.getDateFormat());
    }

    private String getOperationGeometryLabel(SamplingOperationDTO operation) {
        return operation.getName();
    }

    private Symbolizer createPolygonSymbolizer(String name, Stroke stroke, Fill fill) {
        return createNamedSymbolizer(name, SF.createPolygonSymbolizer(stroke, fill, null));
    }

    private Symbolizer createLineSymbolizer(String name, Stroke stroke) {
        return createNamedSymbolizer(name, SF.createLineSymbolizer(stroke, null));
    }

    private Symbolizer createPointSymbolizer(String name, Graphic graphic) {
        return createNamedSymbolizer(name, SF.createPointSymbolizer(graphic, null));
    }

    private Symbolizer createNamedSymbolizer(String name, Symbolizer symbolizer) {
        symbolizer.setName(name);
        String title = "dali.home.map.legend." + name;
        if (symbolizer.getDescription() == null) symbolizer.setDescription(new DescriptionImpl());
        symbolizer.getDescription().setTitle(t(title));
        return symbolizer;
    }

    private Style createStyle(String name, Symbolizer... symbolizers) {

        Rule rule = SF.createRule();

        if (symbolizers != null) {
            for (Symbolizer sym : symbolizers) {
                rule.symbolizers().add(sym);
            }
        }

        return createStyle(name, rule);
    }

    private Style createStyle(String name, Rule... rules) {
        Style style = SF.createStyle();
        style.featureTypeStyles().add(SF.createFeatureTypeStyle(rules));
        style.setName(name);
        return style;
    }

    /**
     * Get the name of displayable layers.
     * Use the reverse order of layer name to bring the finest map first.
     * The visibility is defined by the view envelope from the MapLayerDefinition
     *
     * @param displayArea the display area
     * @return list of displayable layers
     */
    @Override
    public List<String> getDisplayableLayerNames(ReferencedEnvelope displayArea) {

        List<String> layerNames = new ArrayList<>();
        String defaultLayerName = null;
        boolean addDefaultLayer = true;
        boolean allowAddLayer = true;
        // Get layer definitions depending on map type
        List<MapLayerDefinition> layers = new ArrayList<>(currentMapMode.getMapLayerDefinitions());

        displayArea = Maps.transformReferencedEnvelope(displayArea, DefaultGeographicCRS.WGS84);

        // Reverse the list to begin with the finest layers
        Collections.reverse(layers);
        for (MapLayerDefinition layerDefinition : layers) {

            // Detect the default layer
            if (layerDefinition.isDefaultLayer()) {
                defaultLayerName = layerDefinition.getName();
            } else {

                // Add all global layer
                if (layerDefinition.isGlobalLayer()) {
                    layerNames.add(layerDefinition.getName());
                }

                // Or check if the display area is entirely inside the view envelope
                else if (allowAddLayer && layerDefinition.getViewEnvelope().contains((BoundingBox) displayArea)) {

                    // Add the layer to display
                    layerNames.add(layerDefinition.getName());

                    // If the layer don't allow overlap, Stop adding layer
                    allowAddLayer = layerDefinition.isAllowOverlap();

                    // The default is not attempt to be display now
                    addDefaultLayer = false;
                }
            }
        }
        // add default layer if no other layer displayable
        if (addDefaultLayer && defaultLayerName != null) {
            layerNames.add(defaultLayerName);
        }
        return layerNames;
    }

    public static boolean isLocationLayer(DataFeatureLayer layer) {
        return layer != null && layer.getTitle() != null && layer.getTitle().startsWith(LOCATION_LAYER_NAME_PREFIX);
    }

    public static boolean isSurveyLayer(DataFeatureLayer layer) {
        return layer != null && layer.getTitle() != null && layer.getTitle().startsWith(SURVEY_LAYER_NAME_PREFIX);
    }

    public static boolean isOperationLayer(DataFeatureLayer layer) {
        return layer != null && layer.getTitle() != null && layer.getTitle().startsWith(OPERATION_LAYER_NAME_PREFIX);
    }
}
