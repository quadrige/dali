package fr.ifremer.dali.ui.swing.util.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.ErrorAware;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.decorator.BorderHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;
import java.awt.Component;
import java.util.List;

/**
 * Highlighter using an ErrorAwareDTO row to show a border around a particular cell and messages as tooltip
 * <p/>
 * Created by Ludovic on 29/05/2015.
 */
public abstract class ErrorHighlighter extends BorderHighlighter {

    private final SwingTable table;
    private final String title;
    private List<String> message;
    private static final Border innerBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

    /**
     * <p>getMessages.</p>
     *
     * @param bean         a {@link fr.ifremer.dali.dto.ErrorAware} object.
     * @param propertyName a {@link java.lang.String} object.
     * @param pmfmId       a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    public abstract List<String> getMessages(ErrorAware bean, String propertyName, Integer pmfmId);

    /**
     * <p>Constructor for ErrorHighlighter.</p>
     *
     * @param table  a {@link SwingTable} object.
     * @param border a {@link javax.swing.border.Border} object.
     * @param title  a {@link java.lang.String} object.
     */
    public ErrorHighlighter(SwingTable table, Border border, String title) {
        super();

        this.table = table;
        this.title = title;
        setHighlightPredicate(newPredicate());
        setBorder(BorderFactory.createCompoundBorder(border, innerBorder));
        setCompound(false);

    }

    private HighlightPredicate newPredicate() {
        return (renderer, adapter) -> {
            AbstractApplicationTableModel model = (AbstractApplicationTableModel) table.getModel();
            int modelRow = adapter.convertRowIndexToModel(adapter.row);
            AbstractDaliRowUIModel row = (AbstractDaliRowUIModel) model.getEntry(modelRow);
            if (row instanceof ErrorAware) {

                // get column identifier
                ColumnIdentifier identifier = (ColumnIdentifier) adapter.getColumnIdentifierAt(adapter.convertColumnIndexToModel(adapter.column));
                if (identifier == null)
                    return false;
                String propertyName = identifier.getPropertyName();
                Integer pmfmId = (identifier instanceof DaliPmfmColumnIdentifier) ? ((DaliPmfmColumnIdentifier) identifier).getPmfmId() : null;
                message = getMessages((ErrorAware) row, propertyName, pmfmId);
                return CollectionUtils.isNotEmpty(message);
            } else {
                return false;
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Component doHighlight(Component renderer, ComponentAdapter adapter) {
        Component component = super.doHighlight(renderer, adapter);

        // message as tooltip
        ((JComponent) component).setToolTipText(ApplicationUIUtil.getHtmlString(title, message));

        return component;
    }

}
