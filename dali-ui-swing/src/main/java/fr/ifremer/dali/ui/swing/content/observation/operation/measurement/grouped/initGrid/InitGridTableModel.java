package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * @author peck7 on 19/07/2017.
 */
public class InitGridTableModel extends AbstractDaliTableModel<InitGridRowModel> {

    public static final DaliColumnIdentifier<InitGridRowModel> PMFM = DaliColumnIdentifier.newId(
            InitGridRowModel.PROPERTY_PMFM,
            n("dali.property.pmfm"),
            n("dali.property.pmfm"),
            PmfmDTO.class
    );

    public static final DaliColumnIdentifier<InitGridRowModel> QUALITATIVE_VALUES = DaliColumnIdentifier.newId(
            InitGridRowModel.PROPERTY_QUALITATIVE_VALUES,
            n("dali.property.qualitativeValues.count"),
            n("dali.property.qualitativeValues.count"),
            QualitativeValueDTO.class,
            DecoratorService.COLLECTION_SIZE
    );

    public InitGridTableModel(SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    @Override
    public DaliColumnIdentifier<InitGridRowModel> getFirstColumnEditing() {
        return PMFM;
    }

    @Override
    public InitGridRowModel createNewRow() {
        return new InitGridRowModel();
    }
}
