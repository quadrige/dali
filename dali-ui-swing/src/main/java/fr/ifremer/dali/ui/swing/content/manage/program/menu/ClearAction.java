package fr.ifremer.dali.ui.swing.content.manage.program.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.dali.ui.swing.action.AbstractDaliSaveAction;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUIModel;
import fr.ifremer.dali.ui.swing.content.manage.program.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

/**
 * Action permettant d effacer la recherche les mesures.
 */
public class ClearAction extends AbstractCheckModelAction<ProgramsMenuUIModel, ProgramsMenuUI, ProgramsMenuUIHandler> {

	/**
	 * Constructor.
	 *
	 * @param handler Controller
	 */
	public ClearAction(final ProgramsMenuUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public void doAction() throws Exception {
		
		getModel().clear();
        // remove selected item directly because model binding can't works here
		getUI().getProgramMnemonicCombo().setSelectedItem(null);
		getUI().getProgramCodeCombo().setSelectedItem(null);

    	// Suppression des informations du contexte
    	getContext().setSelectedProgramCode(null);
    	getContext().setSelectedLocationId(null);
    	
    	// Suppression des donnees dans les tableaux
		ProgramsUI ui = getHandler().getProgramsUI();
		if (ui != null) {
			ui.getModel().getProgramsUIModel().clear();
			ui.getModel().getStrategiesUIModel().clear();
			ui.getModel().getLocationsUIModel().clear();
			ui.getModel().getPmfmsUIModel().clear();
		}
	}

	/** {@inheritDoc} */
	@Override
	protected Class<? extends AbstractDaliSaveAction> getSaveActionClass() {
		return SaveAction.class;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelModify() {
        final ProgramsUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

	/** {@inheritDoc} */
	@Override
	protected void setModelModify(boolean modelModify) {
		ProgramsUIModel model = getLocalModel();
		if (model != null) {
			model.setModify(modelModify);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelValid() {
        final ProgramsUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

	/** {@inheritDoc} */
	@Override
	protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
		return getHandler().getProgramsUI().getHandler();
	}

	private ProgramsUIModel getLocalModel() {
		final ProgramsUI ui = getHandler().getProgramsUI();
		if (ui != null) {
			return ui.getModel();
		}
		return null;
	}

}
