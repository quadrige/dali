package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.ErrorDTO;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.enums.ControlElementValues;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.SamplingOperationComparator;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.dali.ui.swing.util.table.PmfmTableColumn;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.AllowedQualitativeValuesMap;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.SwingUtilities;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour les mesures des prelevements (ecran prelevements/mesure).
 */
public abstract class AbstractOperationMeasurementsGroupedTableUIHandler<
    R extends OperationMeasurementsGroupedRowModel,
    M extends AbstractOperationMeasurementsGroupedTableUIModel<MeasurementDTO, R, M>,
    UI extends DaliUI<M, ?>
    >
    extends AbstractDaliTableUIHandler<R, M, UI> {

    private static final Log LOG = LogFactory.getLog(AbstractOperationMeasurementsGroupedTableUIHandler.class);

    // editor for sampling operations column
    protected ExtendedComboBoxCellEditor<SamplingOperationDTO> samplingOperationCellEditor;
    // editor for taxon group column
    protected ExtendedComboBoxCellEditor<TaxonGroupDTO> taxonGroupCellEditor;
    // editor for taxon column
    protected ExtendedComboBoxCellEditor<TaxonDTO> taxonCellEditor;
    // editor for analyst column
    protected ExtendedComboBoxCellEditor<DepartmentDTO> departmentCellEditor;
    protected ExtendedComboBoxCellEditor<AnalysisInstrumentDTO> analysisInstrumentCellEditor;

    /**
     * <p>Constructor for OperationMeasurementsGroupedTableUIHandler.</p>
     */
    public AbstractOperationMeasurementsGroupedTableUIHandler(String... properties) {
        super(properties);
    }

    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{
            R.PROPERTY_INPUT_TAXON_ID,
            R.PROPERTY_INPUT_TAXON_NAME
        };
    }

    @Override
    public abstract AbstractOperationMeasurementsGroupedTableModel<R> getTableModel();

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final UI ui) {
        super.beforeInit(ui);
        ui.setContextValue(createNewModel());
    }

    protected abstract M createNewModel();

    protected abstract R createNewRow(boolean readOnly, SamplingOperationDTO parentBean);

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final UI ui) {

        // Init UI
        initUI(ui);

        // Create editors
        createTaxonGroupCellEditor();
        createTaxonCellEditor();
        createDepartmentCellEditor();
        createAnalysisInstrumentCellEditor();
        resetCellEditors();

        // Init table
        initTable();

        // Init listeners
        initListeners();

    }

    /**
     * Initialiser les listeners
     */
    protected void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {
                case AbstractOperationMeasurementsGroupedTableUIModel.PROPERTY_SURVEY:

                    // update sampling operation cell combo box
                    if (samplingOperationCellEditor != null)
                        samplingOperationCellEditor.getCombo().setData(getModel().getSamplingOperations());

                    // load available pmfms
                    loadMeasurements();
                    break;

                case AbstractOperationMeasurementsGroupedTableUIModel.PROPERTY_MEASUREMENTS_LOADED:

                    detectPreconditionedPmfms();
                    break;

                case AbstractOperationMeasurementsGroupedTableUIModel.PROPERTY_MEASUREMENT_FILTER:

                    // filter if measurements are loaded
                    if (getModel().isMeasurementsLoaded())
                        filterMeasurements();
                    break;

                case AbstractDaliTableUIModel.PROPERTY_SINGLE_ROW_SELECTED:

                    if (getModel().getSingleSelectedRow() != null && !getModel().getSingleSelectedRow().isEditable()) {
                        return;
                    }

                    // update taxonCellEditor
                    updateTaxonCellEditor(getModel().getSingleSelectedRow(), false);
                    updateTaxonGroupCellEditor(getModel().getSingleSelectedRow(), false);
                    updateDepartmentCellEditor(false);
                    updateAnalysisInstrumentCellEditor(false);
                    updatePmfmCellEditors(getModel().getSingleSelectedRow(), null, false);
                    break;

            }
        });

    }

    /**
     * Update all editors on measurements if they have preconditions
     *
     * @param row               the actual selected row
     * @param firstPmfmId       the first pmfm Id to treat (can be null)
     * @param resetValueAllowed allow or not the target value to be reset
     */
    private void updatePmfmCellEditors(R row, Integer firstPmfmId, boolean resetValueAllowed) {

        if (row == null || CollectionUtils.isEmpty(row.getIndividualPmfms())) return;

        // Build list of pmfms to process (if firstPmfmId is specified, it will be threat first)
        List<PmfmDTO> pmfms;
        if (firstPmfmId == null) {
            pmfms = row.getIndividualPmfms();
        } else {
            pmfms = new ArrayList<>(row.getIndividualPmfms());
            PmfmDTO firstPmfm = DaliBeans.findById(pmfms, firstPmfmId);
            if (firstPmfm != null) {
                pmfms.remove(firstPmfm);
                // add this pmfm at first position
                pmfms.add(0, firstPmfm);
            }
        }

        // clear previous allowed values map
        row.getAllowedQualitativeValuesMap().clear();
        List<Integer> pmfmIds = pmfms.stream().map(PmfmDTO::getId).collect(Collectors.toList());

        for (PmfmDTO pmfm : pmfms) {

            // if there is a preconditioned rule for this pmfm
            if (getModel().isPmfmIdHasPreconditions(pmfm.getId())) {

                // get the measurement with this pmfm
                MeasurementDTO measurement = DaliBeans.getIndividualMeasurementByPmfmId(row, pmfm.getId());
                if (measurement == null) {
                    // create empty measurement if needed
                    measurement = DaliBeanFactory.newMeasurementDTO();
                    measurement.setPmfm(pmfm);
                    row.getIndividualMeasurements().add(measurement);
                }

                if (pmfm.getParameter().isQualitative()) {

                    // Process allowed qualitative values
                    getContext().getRuleListService().buildAllowedValuesByPmfmId(
                        pmfm.getId(),
                        measurement.getQualitativeValue(),
                        pmfmIds,
                        getModel().getPreconditionRulesByPmfmId(pmfm.getId()),
                        row.getAllowedQualitativeValuesMap()
                    );

                    // update combos of affected pmfms
                    for (Integer targetPmfmId : row.getAllowedQualitativeValuesMap().getTargetIds()) {
                        updateQualitativePmfmCellEditors(row, targetPmfmId, resetValueAllowed);
                    }

                } else {

                    // Process allowed numerical values TODO when used
                    // buildAllowedValuesByPmfmId(pmfm.getId(), measurement.getQualitativeValue(), pmfmIds, row.getAllowedQualitativeValuesMap());
                }
            }
        }

        // then update all pmfm editors a last time
        for (PmfmDTO pmfm : row.getIndividualPmfms()) {
            updateQualitativePmfmCellEditors(row, pmfm.getId(), resetValueAllowed);
        }

    }

    @SuppressWarnings("unchecked")
    private void updateQualitativePmfmCellEditors(R row, int targetPmfmId, boolean resetValueAllowed) {

        PmfmDTO targetPmfm = DaliBeans.findById(row.getIndividualPmfms(), targetPmfmId);
        PmfmTableColumn targetPmfmColumn = findPmfmColumnByPmfmId(getModel().getPmfmColumns(), targetPmfmId);

        // cellEditor must be a ExtendedComboBoxCellEditor
        if (targetPmfmColumn == null || !(targetPmfmColumn.getCellEditor() instanceof ExtendedComboBoxCellEditor)) return; // simply skip
        ExtendedComboBoxCellEditor<QualitativeValueDTO> comboBoxCellEditor = (ExtendedComboBoxCellEditor<QualitativeValueDTO>) targetPmfmColumn.getCellEditor();

        AllowedQualitativeValuesMap.AllowedValues allowedTargetIds = new AllowedQualitativeValuesMap.AllowedValues();

        Collection<Integer> sourcePmfmIds = row.getAllowedQualitativeValuesMap().getExistingSourcePmfmIds(targetPmfmId);
        for (Integer sourcePmfmId : sourcePmfmIds) {

            // get existing measurement for this source pmfm
            MeasurementDTO sourceMeasurement = DaliBeans.getIndividualMeasurementByPmfmId(row, sourcePmfmId);

            Integer sourceValueId = sourceMeasurement == null ? null : sourceMeasurement.getQualitativeValue() == null ? null : sourceMeasurement.getQualitativeValue().getId();
            AllowedQualitativeValuesMap.AllowedValues list = row.getAllowedQualitativeValuesMap().getAllowedValues(targetPmfmId, sourcePmfmId, sourceValueId);
            allowedTargetIds.addOrRetain(list);
        }

        // Compute allowed values
        List<QualitativeValueDTO> allowedQualitativeValues = DaliBeans.filterCollection(targetPmfm.getQualitativeValues(),
            qualitativeValue -> allowedTargetIds.isAllowed(qualitativeValue.getId()));
        comboBoxCellEditor.getCombo().setData(allowedQualitativeValues);
        // fix the target value
        if (resetValueAllowed) {
            QualitativeValueDTO actualValue = (QualitativeValueDTO) targetPmfmColumn.getPmfmIdentifier().getValue(row);
            if (allowedQualitativeValues.size() == 1) {
                if (!allowedQualitativeValues.get(0).equals(actualValue)) {
                    // force a 1:1 relation
                    getModel().setAdjusting(true);
                    targetPmfmColumn.getPmfmIdentifier().setValue(row, allowedQualitativeValues.get(0));
                    getModel().setAdjusting(false);
                }
            } else {
                if (actualValue != null && !allowedQualitativeValues.contains(actualValue)) {
                    // reset a bad 1:n relation
                    getModel().setAdjusting(true);
                    targetPmfmColumn.getPmfmIdentifier().setValue(row, null);
                    getModel().setAdjusting(false);
                }
            }
        }
    }

    private void detectPreconditionedPmfms() {

        if (CollectionUtils.isEmpty(getModel().getSurvey().getPreconditionedRules())) return;

        for (ControlRuleDTO preconditionedRule : getModel().getSurvey().getPreconditionedRules()) {

            for (PreconditionRuleDTO precondition : preconditionedRule.getPreconditions()) {

                int basePmfmId = precondition.getBaseRule().getRulePmfms(0).getPmfm().getId();
                int usedPmfmId = precondition.getUsedRule().getRulePmfms(0).getPmfm().getId();

                getModel().addPreconditionRuleByPmfmId(basePmfmId, precondition);
                if (precondition.isBidirectional())
                    getModel().addPreconditionRuleByPmfmId(usedPmfmId, precondition);
            }
        }

    }

    private void createTaxonCellEditor() {

        taxonCellEditor = newExtendedComboBoxCellEditor(null, TaxonDTO.class, DecoratorService.WITH_CITATION_AND_REFERENT, false);

        taxonCellEditor.setAction("unfilter-taxon", "dali.common.unfilter.taxon", e -> {
            // unfilter taxon
            updateTaxonCellEditor(getModel().getSingleSelectedRow(), true);
        });

    }

    private void updateTaxonCellEditor(R row, boolean forceNoFilter) {

        taxonCellEditor.getCombo().setActionEnabled(!forceNoFilter);

        List<TaxonDTO> taxons = row != null
            ? getContext().getObservationService().getAvailableTaxons(forceNoFilter ? null : row.getTaxonGroup(), false)
            : null;

        taxonCellEditor.getCombo().setData(taxons);

    }

    private void createTaxonGroupCellEditor() {

        taxonGroupCellEditor = newExtendedComboBoxCellEditor(null, OperationMeasurementsGroupedTableModel.TAXON_GROUP, false);

        taxonGroupCellEditor.setAction("unfilter-taxon", "dali.common.unfilter.taxon", e -> {
            // unfilter taxon groups
            updateTaxonGroupCellEditor(getModel().getSingleSelectedRow(), true);
        });

    }

    private void updateTaxonGroupCellEditor(R row, boolean forceNoFilter) {

        taxonGroupCellEditor.getCombo().setActionEnabled(!forceNoFilter);

        List<TaxonGroupDTO> taxonGroups = row != null
            ? getContext().getObservationService().getAvailableTaxonGroups(forceNoFilter ? null : row.getTaxon(), false)
            : null;

        taxonGroupCellEditor.getCombo().setData(taxonGroups);

    }

    private void createDepartmentCellEditor() {

        departmentCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        departmentCellEditor.setAction("unfilter", "dali.common.unfilter", e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            updateDepartmentCellEditor(true);
        });

    }

    private void updateDepartmentCellEditor(boolean forceNoFilter) {

        departmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
            && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        departmentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    private void createAnalysisInstrumentCellEditor() {

        analysisInstrumentCellEditor = newExtendedComboBoxCellEditor(null, AnalysisInstrumentDTO.class, false);

        analysisInstrumentCellEditor.setAction("unfilter", "dali.common.unfilter", e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            updateAnalysisInstrumentCellEditor(true);
        });

    }

    private void updateAnalysisInstrumentCellEditor(boolean forceNoFilter) {

        analysisInstrumentCellEditor.getCombo().setActionEnabled(!forceNoFilter
            && getContext().getDataContext().isContextFiltered(FilterTypeValues.ANALYSIS_INSTRUMENT));

        analysisInstrumentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableAnalysisInstruments(forceNoFilter));
    }

    /**
     * <p>resetCellEditors.</p>
     */
    public void resetCellEditors() {

        updateTaxonGroupCellEditor(null, false);
        updateTaxonCellEditor(null, false);
        updateDepartmentCellEditor(false);
        updateAnalysisInstrumentCellEditor(false);
    }

    /**
     * Load mesurements
     */
    private void loadMeasurements() {

        SwingUtilities.invokeLater(() -> {

            // Uninstall save state listener
            uninstallSaveTableStateListener();

            // Build dynamic columns
            // TODO manage column position depending on criteria (parametrized list or specific attribute)
            // maybe split pmfms list in 2 separated list or move afterward
            DaliColumnIdentifier<R> insertPosition = getTableModel().getPmfmInsertPosition();
            addPmfmColumns(
                getModel().getPmfms(),
                SamplingOperationDTO.PROPERTY_INDIVIDUAL_PMFMS,
                DecoratorService.NAME_WITH_UNIT,
                insertPosition);

            boolean notEmpty = CollectionUtils.isNotEmpty(getModel().getPmfms());

            // Build rows
            getModel().setRows(buildRows(!getModel().getSurvey().isEditable()));
            getTableModel().setReadOnly(!getModel().getSurvey().isEditable());

            recomputeRowsValidState();

            // restore table from swing session
            restoreTableState();

            // Apply measurement filter
            filterMeasurements();

            // hide analyst if no pmfm
//            forceColumnVisibleAtLastPosition(OperationMeasurementsGroupedTableModel.ANALYST, notEmpty);
            // Don't force position (Mantis #49939)
            forceColumnVisible(OperationMeasurementsGroupedTableModel.ANALYST, notEmpty);

            // set columns with errors visible (Mantis #40752)
            ensureColumnsWithErrorAreVisible(getModel().getRows());

            // Install save state listener
            SwingUtilities.invokeLater(this::installSaveTableStateListener);

            getModel().setMeasurementsLoaded(true);
        });

    }

    protected abstract void filterMeasurements();

    protected List<R> buildRows(boolean readOnly) {
        List<R> rows = Lists.newArrayList();

        List<SamplingOperationDTO> samplingOperations = Lists.newArrayList(getModel().getSamplingOperations());
        // sort by name
        samplingOperations.sort(SamplingOperationComparator.instance);

        // Iterate over sampling operation
        for (SamplingOperationDTO samplingOperation : samplingOperations) {

            // build rows
            R row = null;
            List<MeasurementDTO> measurements = Lists.newArrayList(samplingOperation.getIndividualMeasurements());
            // sort by individual id
            measurements.sort(Comparator.comparingInt(MeasurementDTO::getIndividualId));

            for (final MeasurementDTO measurement : measurements) {
                // check previous row
                if (row != null) {

                    // if individual id differs = new row
                    if (!row.getIndividualId().equals(measurement.getIndividualId())) {
                        row = null;
                    }

                    // if taxon group or taxon differs, should update current row (see Mantis #0026647)
                    else if (!Objects.equals(row.getTaxonGroup(), measurement.getTaxonGroup())
                        || !Objects.equals(row.getTaxon(), measurement.getTaxon())
                        || !Objects.equals(row.getInputTaxonId(), measurement.getInputTaxonId())
                        || !Objects.equals(row.getInputTaxonName(), measurement.getInputTaxonName())) {

                        // update taxon group and taxon if previously empty
                        if (row.getTaxonGroup() == null) {
                            row.setTaxonGroup(measurement.getTaxonGroup());
                        } else if (measurement.getTaxonGroup() != null && !row.getTaxonGroup().equals(measurement.getTaxonGroup())) {
                            // the taxon group in measurement differs
                            LOG.error(String.format("taxon group in measurement (id=%s) differs with taxon group in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                        if (row.getTaxon() == null) {
                            row.setTaxon(measurement.getTaxon());
                        } else if (measurement.getTaxon() != null && !row.getTaxon().equals(measurement.getTaxon())) {
                            // the taxon in measurement differs
                            LOG.error(String.format("taxon in measurement (id=%s) differs with taxon in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                        if (row.getInputTaxonId() == null) {
                            row.setInputTaxonId(measurement.getInputTaxonId());
                        } else if (measurement.getInputTaxonId() != null && !row.getInputTaxonId().equals(measurement.getInputTaxonId())) {
                            // the input taxon Id in measurement differs
                            LOG.error(String.format("input taxon id in measurement (id=%s) differs with input taxon id in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                        if (StringUtils.isBlank(row.getInputTaxonName())) {
                            row.setInputTaxonName(measurement.getInputTaxonName());
                        } else if (measurement.getInputTaxonName() != null && !row.getInputTaxonName().equals(measurement.getInputTaxonName())) {
                            // the input taxon name in measurement differs
                            LOG.error(String.format("input taxon name in measurement (id=%s) differs with input taxon name in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                    }
                }

                // build a new row
                if (row == null) {
                    row = createNewRow(readOnly, samplingOperation);
                    row.setIndividualPmfms(new ArrayList<>(getModel().getPmfms()));
                    row.setIndividualId(measurement.getIndividualId());
                    row.setTaxonGroup(measurement.getTaxonGroup());
                    row.setTaxon(measurement.getTaxon());
                    row.setInputTaxonId(measurement.getInputTaxonId());
                    row.setInputTaxonName(measurement.getInputTaxonName());
                    row.setAnalyst(measurement.getAnalyst());
                    row.setAnalysisInstrument(measurement.getAnalysisInstrument());
                    // Mantis #26724 or #29130: don't take only one comment but a concatenation of all measurements comments
//                    row.setComment(measurement.getComment());

                    row.setValid(true);

                    rows.add(row);

                    // add errors on new row if samplingOperations have some
                    List<ErrorDTO> errors = DaliBeans.filterCollection(samplingOperation.getErrors(),
                        input -> (ControlElementValues.MEASUREMENT.getCode().equals(input.getControlElementCode())
                            || ControlElementValues.TAXON_MEASUREMENT.getCode().equals(input.getControlElementCode()))
                            && Objects.equals(input.getIndividualId(), measurement.getIndividualId())
                    );
                    DaliBeans.addUniqueErrors(row, errors);
                }

                // add measurement on current row
                row.getIndividualMeasurements().add(measurement);

                // add errors on row if measurement have some
                DaliBeans.addUniqueErrors(row, measurement.getErrors());

            }
        }

        // Mantis #26724 or #29130: don't take only one comment but a concatenation of all measurements comments
        for (R row : rows) {

            // join and affect
            row.setComment(DaliBeans.getUnifiedCommentFromIndividualMeasurements(row));
        }

        return rows;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, R row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // if a value of individual pmfm changes
        if (R.PROPERTY_INDIVIDUAL_PMFMS.equals(propertyName)) {

            // no need to tell the table is modified if no pmfms value change
            if (oldValue == newValue) return;

            // if a value has been changed, update other editor with preconditions
            if (!getModel().isAdjusting())
                updatePmfmCellEditors(row, propertyIndex, newValue != null);
        }

        // update individual id when sampling operation changes
        if (R.PROPERTY_SAMPLING_OPERATION.equals(propertyName)) {

            // remove measurement from old sampling
            if (oldValue != null) {
                SamplingOperationDTO oldSamplingOperation = (SamplingOperationDTO) oldValue;
                oldSamplingOperation.removeAllIndividualMeasurements(row.getIndividualMeasurements());
                oldSamplingOperation.setDirty(true);

                resetIndividualMeasurementIds(row);
            }

            // recalculate individual id
            if (newValue != null) {
                // fire event for parent listener
                getModel().firePropertyChanged(AbstractOperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION, null, newValue);
            }
        }

        // update taxon when taxon group is updated
        if (R.PROPERTY_TAXON_GROUP.equals(propertyName)) {
            // filter taxon
            updateTaxonCellEditor(row, false);

            // reset measurementId if no more taxon group or new taxon group
            if ((newValue == null ^ oldValue == null) && row.getTaxon() == null) {

                // before, remove those measurements from sampling operation
                if (row.getSamplingOperation() != null) {
                    row.getSamplingOperation().removeAllIndividualMeasurements(row.getIndividualMeasurements());
                }

                resetIndividualMeasurementIds(row);
            }
        }

        if (R.PROPERTY_TAXON.equals(propertyName)) {
            // filter taxon
            updateTaxonGroupCellEditor(row, false);

            // reset measurementId if no more taxon or new taxon
            if ((newValue == null ^ oldValue == null) && row.getTaxonGroup() == null) {

                // before, remove those measurements from sampling operation
                if (row.getSamplingOperation() != null) {
                    row.getSamplingOperation().removeAllIndividualMeasurements(row.getIndividualMeasurements());
                }

                resetIndividualMeasurementIds(row);
            }

            // update input taxon
            TaxonDTO taxon = (TaxonDTO) newValue;
            row.setInputTaxonId(taxon != null ? taxon.getId() : null);
            row.setInputTaxonName(taxon != null ? taxon.getName() : null);
        }

        if (oldValue != newValue) {

            // recompute valid state on all rows
            recomputeRowsValidState();

            row.setInitialized(false);
        }

        // fire modify event at the end
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
    }

    private PmfmTableColumn findPmfmColumnByPmfmId(List<PmfmTableColumn> pmfmTableColumns, int pmfmId) {

        if (pmfmTableColumns != null) {
            for (PmfmTableColumn pmfmTableColumn : pmfmTableColumns) {
                if (pmfmTableColumn.getPmfmId() == pmfmId) return pmfmTableColumn;
            }
        }

        return null;
    }

    public void duplicateSelectedRow() {

        if (getModel().getSelectedRows().size() == 1 && getModel().getSingleSelectedRow() != null) {
            R rowToDuplicate = getModel().getSingleSelectedRow();
            R row = createNewRow(false, rowToDuplicate.getSamplingOperation());
            row.setTaxonGroup(rowToDuplicate.getTaxonGroup());
            row.setTaxon(rowToDuplicate.getTaxon());
            row.setInputTaxonId(rowToDuplicate.getInputTaxonId());
            row.setInputTaxonName(rowToDuplicate.getInputTaxonName());
            row.setComment(rowToDuplicate.getComment());
            row.setIndividualPmfms(rowToDuplicate.getIndividualPmfms());
            // add also analyst (Mantis #45565)
            row.setAnalyst(rowToDuplicate.getAnalyst());
            row.setAnalysisInstrument(rowToDuplicate.getAnalysisInstrument());
            // duplicate measurements
            row.setIndividualMeasurements(DaliBeans.duplicate(rowToDuplicate.getIndividualMeasurements()));

            // Add duplicate measurement to table
            getModel().insertRowAfterSelected(row);

            if (row.getSamplingOperation() != null) {
                setDirty(row.getSamplingOperation());
                getModel().firePropertyChanged(AbstractOperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION, null, row.getSamplingOperation());
            }
            recomputeRowsValidState();
            getModel().setModify(true);
            setFocusOnCell(row);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(R row) {
        boolean valid = super.isRowValid(row);

        if (!valid && !row.isMandatoryValid()) {
            // check invalid mandatory errors
            new ArrayList<>(row.getInvalidMandatoryIdentifiers()).forEach(invalidIdentifier -> {
                if (row.getMultipleValuesOnIdentifier().contains(invalidIdentifier)) {
                    // if this identifier has multiple value, remove error
                    row.getErrors().removeIf(error -> error.getPropertyName().size() == 1 && error.getPropertyName().contains(invalidIdentifier.getPropertyName()));
                    row.getInvalidMandatoryIdentifiers().remove(invalidIdentifier);
                }
            });
            valid = row.isMandatoryValid();
        }

        hasNoPerfectDuplicates(row);

        return valid && hasAnalyst(row) && hasNoUnicityDuplicates(row);
    }

    private boolean hasAnalyst(R row) {

        if (!row.getMultipleValuesOnIdentifier().contains(AbstractOperationMeasurementsGroupedTableModel.ANALYST) &&
            row.getAnalyst() == null &&
            row.getIndividualMeasurements().stream()
                .filter(measurement -> getModel().getSurvey().getPmfmsUnderMoratorium().stream().noneMatch(moratoriumPmfm -> DaliBeans.isPmfmEquals(measurement.getPmfm(), moratoriumPmfm)))
                .anyMatch(measurement -> !DaliBeans.isMeasurementEmpty(measurement))
        ) {
            DaliBeans.addError(row,
                t("dali.validator.error.analyst.required"),
                R.PROPERTY_ANALYST);
            return false;
        }
        return true;

    }

    private void hasNoPerfectDuplicates(R row) {

        // Mantis #37234 : this test must be executed even if there is no taxon
        if (getModel().getRowCount() < 2 /*|| (row.getTaxonGroup() == null && row.getTaxon() == null)*/) {
            // no need to check duplicates
            return;
        }

        for (R otherRow : getModel().getRows()) {
            if (otherRow == row /*|| (otherRow.getTaxonGroup() == null && otherRow.getTaxon() == null)*/) continue;

            if (Objects.equals(row.getSamplingOperation(), otherRow.getSamplingOperation())
                && Objects.equals(row.getTaxonGroup(), otherRow.getTaxonGroup())
                && Objects.equals(row.getTaxon(), otherRow.getTaxon())
                && haveSameMeasurements(row, otherRow) // Both measurements must have same non empty measurements (Mantis #42170)
            ) {
                // if sampling, taxon group and taxon equals, check all measurement values
                boolean allValuesEquals = true;
                Set<Integer> notNullMeasurementPmfmIds = Sets.newHashSet();
                for (MeasurementDTO measurement : row.getIndividualMeasurements()) {

                    // find the measurement with same pmfm in the other row
                    MeasurementDTO otherMeasurement = DaliBeans.getIndividualMeasurementByPmfmId(otherRow, measurement.getPmfm().getId());

                    // fix Mantis #38335
                    if (otherMeasurement == null) continue;

                    if (measurement.getPmfm().getParameter().isQualitative()) {
                        if (!Objects.equals(measurement.getQualitativeValue(), otherMeasurement.getQualitativeValue())) {
                            allValuesEquals = false;
                            break;
                        }
                    } else {
                        if (!Objects.equals(measurement.getNumericalValue(), otherMeasurement.getNumericalValue())) {
                            allValuesEquals = false;
                            break;
                        }
                    }

                    // collect pmfm ids
                    if (!DaliBeans.isMeasurementEmpty(measurement)) {
                        notNullMeasurementPmfmIds.add(measurement.getPmfm().getId());
                    }
                }
                if (allValuesEquals) {

                    // The taxonGroup and taxon column are highlighted only if filled (Mantis #44713)
                    List<String> propertiesToHighlight = new ArrayList<>();
                    propertiesToHighlight.add(R.PROPERTY_SAMPLING_OPERATION);
                    if (row.getTaxonGroup() != null)
                        propertiesToHighlight.add(R.PROPERTY_TAXON_GROUP);
                    if (row.getTaxon() != null)
                        propertiesToHighlight.add(R.PROPERTY_TAXON);

                    // Mantis #0026890 The check for perfect duplicates is now a warning
                    DaliBeans.addWarning(row,
                        t("dali.samplingOperation.measurement.grouped.duplicate"),
                        propertiesToHighlight.toArray(new String[0]));

                    for (Integer pmfmId : notNullMeasurementPmfmIds) {
                        DaliBeans.addWarning(row,
                            t("dali.samplingOperation.measurement.grouped.duplicate"),
                            pmfmId,
                            R.PROPERTY_INDIVIDUAL_PMFMS);
                    }
                    return;
                }
            }
        }
    }

    private boolean haveSameMeasurements(R row, R otherRow) {
        List<Integer> pmfmIds = row.getIndividualMeasurements().stream()
            .filter(measurement -> !DaliBeans.isMeasurementEmpty(measurement))
            .map(measurement -> measurement.getPmfm().getId())
            .collect(Collectors.toList());
        List<Integer> otherPmfmIds = otherRow.getIndividualMeasurements().stream()
            .filter(measurement -> !DaliBeans.isMeasurementEmpty(measurement))
            .map(measurement -> measurement.getPmfm().getId())
            .collect(Collectors.toList());
        return pmfmIds.containsAll(otherPmfmIds) && otherPmfmIds.containsAll(pmfmIds);
    }

    private boolean hasNoUnicityDuplicates(R row) {

        if (getModel().getRowCount() < 2
            || (row.getTaxonGroup() == null && row.getTaxon() == null)
            || CollectionUtils.isEmpty(getModel().getUniquePmfms())) {
            // no need to check duplicates
            return true;
        }

        for (R otherRow : getModel().getRows()) {
            if (otherRow == row || (otherRow.getTaxonGroup() == null && otherRow.getTaxon() == null)) continue;

            if (Objects.equals(row.getSamplingOperation(), otherRow.getSamplingOperation())
                && Objects.equals(row.getTaxonGroup(), otherRow.getTaxonGroup())
                && Objects.equals(row.getTaxon(), otherRow.getTaxon())
            ) {
                // if sampling, taxon group and taxon equals, check measurement values with unique pmfms

                for (PmfmDTO uniquePmfm : getModel().getUniquePmfms()) {

                    // find the measurement with this pmfm in the row
                    MeasurementDTO measurement = DaliBeans.getIndividualMeasurementByPmfmId(row, uniquePmfm.getId());

                    if (measurement == null) {
                        continue;
                    }

                    // find the measurement with this pmfm in the other row
                    MeasurementDTO otherMeasurement = DaliBeans.getIndividualMeasurementByPmfmId(otherRow, uniquePmfm.getId());

                    if (otherMeasurement == null) {
                        continue;
                    }

                    if ((measurement.getPmfm().getParameter().isQualitative() && Objects.equals(measurement.getQualitativeValue(), otherMeasurement.getQualitativeValue()))
                        || (!measurement.getPmfm().getParameter().isQualitative() && Objects.equals(measurement.getNumericalValue(), otherMeasurement.getNumericalValue()))) {

                        // duplicate value found
                        DaliBeans.addError(row,
                            t("dali.samplingOperation.measurement.grouped.duplicate.taxonUnique", decorate(uniquePmfm, DecoratorService.NAME_WITH_UNIT)),
                            uniquePmfm.getId(),
                            R.PROPERTY_SAMPLING_OPERATION,
                            R.PROPERTY_TAXON_GROUP,
                            R.PROPERTY_TAXON,
                            R.PROPERTY_INDIVIDUAL_PMFMS);
                        return false;

                    }
                }
            }
        }

        return true;
    }

    private void resetIndividualMeasurementIds(R row) {
        if (row != null && CollectionUtils.isNotEmpty(row.getIndividualMeasurements())) {
            for (MeasurementDTO individualMeasurement : row.getIndividualMeasurements()) {
                individualMeasurement.setId(null);
            }
        }
    }

    protected abstract void initTable();

    @Override
    protected void installSortController() {
        super.installSortController();

        // set alpha numeric comparator (Mantis #47541)
        if (getFixedTable() != null) {
            getSortController().setComparator(
                getFixedTable().getColumnModel().getColumnExt(OperationMeasurementsGroupedTableModel.SAMPLING).getModelIndex(),
                SamplingOperationComparator.instance
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<R> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            R newRow = addedRows.get(0);

            // If newRow has no pmfm and no measurement (a real new row)
            if (CollectionUtils.isEmpty(newRow.getIndividualPmfms()) && CollectionUtils.isEmpty(newRow.getIndividualMeasurements())) {

                // Affect individual pmfms from parent model
                newRow.setIndividualPmfms(new ArrayList<>(getModel().getPmfms()));

                // Create empty measurements
                DaliBeans.createEmptyMeasurements(newRow);

                // Set default value if filters are set
                if (getModel().getSamplingFilter() != null) {
                    newRow.setSamplingOperation(getModel().getSamplingFilter());
                }
//                if (getModel().getTaxonGroupFilter() != null) {
//                    newRow.setTaxonGroup(getModel().getTaxonGroupFilter());
//                }
//                if (getModel().getTaxonFilter() != null) {
//                    newRow.setTaxon(getModel().getTaxonFilter());
//                }

                // set default analyst from pmfm strategies (Mantis #42617)
                newRow.setAnalyst(getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(getModel().getSurvey()));

            }

            // reset the cell editors
            resetCellEditors();

            getModel().setModify(true);

            // Ajouter le focus sur la cellule de la ligne cree
            setFocusOnCell(newRow);
        }
    }

    /**
     * <p>removeIndividualMeasurements.</p>
     */
    public void removeIndividualMeasurements() {

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("No row selected");
            return;
        }

        if (askBeforeDelete(t("dali.action.delete.survey.measurement.titre"), t("dali.action.delete.survey.measurement.message"))) {

            // collect sampling operation to update
            Set<SamplingOperationDTO> samplingOperationsToUpdate = getModel().getSelectedRows().stream()
                // a row with measurements
                .filter(row -> CollectionUtils.isNotEmpty(row.getIndividualMeasurements()))
                // collect them
                .flatMap(row -> row.getIndividualMeasurements().stream())
                // filter with existing sapling operation
                .filter(measurement -> measurement.getSamplingOperation() != null)
                // remove each measurement from sampling operation
                .peek(measurement -> measurement.getSamplingOperation().removeIndividualMeasurements(measurement))
                // return the sampling operation
                .map(MeasurementDTO::getSamplingOperation)
                .collect(Collectors.toSet());

            getModel().setModify(true);

            // keep selected rows in a new list
            List<R> rowsToDelete = new ArrayList<>(getModel().getSelectedRows());
            // unselect them to prevent selection restore problem (Mantis #52738)
            unselectAllRows();
            // delete rows
            getModel().deleteRows(rowsToDelete);

            // final update
            samplingOperationsToUpdate.forEach(samplingOperation -> {
                samplingOperation.setDirty(true);
                getModel().firePropertyChanged(AbstractOperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION, null, samplingOperation);
            });

            recomputeRowsValidState();
        }
    }

    /**
     * Save grouped measurements on parent model(s)
     * This replace inline save (see Mantis #52401)
     */
    public void save() {

        // Get all existing measurements in beans
        List<SamplingOperationDTO> beansToSave = getModel().getSamplingOperations().stream()
            .sorted(SamplingOperationComparator.instance)
            .collect(Collectors.toList());

        List<MeasurementDTO> existingMeasurements = beansToSave.stream()
            .flatMap(bean -> bean.getIndividualMeasurements().stream())
            .collect(Collectors.toList());

        // get the minimum negative measurement id
        AtomicInteger minNegativeId = new AtomicInteger(
            Math.min(
                0,
                existingMeasurements.stream()
                    .filter(measurement -> measurement != null && measurement.getId() != null)
                    .mapToInt(MeasurementDTO::getId)
                    .min()
                    .orElse(0)
            )
        );

        Multimap<SamplingOperationDTO, R> rowsByBean = ArrayListMultimap.create();

        // Get ordered rows by bean
        IntStream.range(0, getTable().getRowCount())
            .mapToObj(i -> getTableModel().getEntry(getTable().convertRowIndexToModel(i)))
            // skip non dirty row (Mantis #52658)
            .filter(row -> !row.isInitialized())
            .forEach(row -> {
                SamplingOperationDTO bean = row.getSamplingOperation();
                if (bean == null) {
                    throw new DaliTechnicalException("The parent bean is null for a grouped measurements row");
                }
                rowsByBean.put(bean, row);
            });

        rowsByBean.keySet().forEach(bean -> {

            AtomicInteger individualId = new AtomicInteger();
            List<MeasurementDTO> beanMeasurements = new ArrayList<>();

            // Iterate over each row
            rowsByBean.get(bean).stream()
                // Test is this row is to save or not
                .filter(this::isRowToSave)
                .forEach(row -> {

                    // Affect row individual ids according this order
                    row.setIndividualId(individualId.incrementAndGet());

                    // Iterate over row's measurements
                    row.getIndividualMeasurements().forEach(measurement -> {
                        if (measurement.getId() == null) {
                            // this measurement was not in bean, so add it with next negative id
                            measurement.setId(minNegativeId.decrementAndGet());
                        }
                        // update this measurement
                        updateMeasurementFromRow(measurement, row);
                        // Add to new list
                        beanMeasurements.add(measurement);
                    });

                });

            // Affect new list of measurements
            bean.getIndividualMeasurements().clear();
            bean.getIndividualMeasurements().addAll(beanMeasurements);

            setDirty(bean);
            // Remove this bean from the list
            beansToSave.remove(bean);
        });

        // Clean remaining beans
        beansToSave.forEach(bean -> {
            // If a bean still in this map, it means there is no row of this bean, so remove all
            bean.getIndividualMeasurements().clear();
            setDirty(bean);
        });
    }

    private void updateMeasurementFromRow(MeasurementDTO measurement, R row) {
        measurement.setSamplingOperation(row.getSamplingOperation());
        measurement.setIndividualId(row.getIndividualId());
        measurement.setTaxonGroup(row.getTaxonGroup());
        measurement.setTaxon(row.getTaxon());
        measurement.setInputTaxonId(row.getInputTaxonId());
        measurement.setInputTaxonName(row.getInputTaxonName());
        measurement.setComment(row.getComment());
        measurement.setAnalyst(row.getAnalyst());
        measurement.setAnalysisInstrument(row.getAnalysisInstrument());
    }

    private void setDirty(SamplingOperationDTO bean) {
        bean.setDirty(true);
        getModel().firePropertyChanged(AbstractOperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION, null, bean);
    }

    /**
     * Determine if this row is to be saved
     *
     * @param row to test
     * @return true if to save (default)
     */
    protected boolean isRowToSave(R row) {
        return
            // The row must exists
            row != null
                // and contains at least 1 non empty individual measurement or a taxon group or a taxon
                && (row.hasTaxonInformation() || row.hasNonEmptyMeasurements());
    }

}
