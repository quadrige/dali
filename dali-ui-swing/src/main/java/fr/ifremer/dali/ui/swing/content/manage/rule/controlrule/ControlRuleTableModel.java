package fr.ifremer.dali.ui.swing.content.manage.rule.controlrule;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.FunctionDTO;
import fr.ifremer.dali.dto.configuration.control.ControlElementDTO;
import fr.ifremer.dali.dto.configuration.control.ControlFeatureDTO;
import fr.ifremer.dali.dto.enums.ControlFunctionValues;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlRuleTableModel extends AbstractDaliTableModel<ControlRuleRowModel> {

    /**
     * Constant <code>CODE</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> CODE = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_CODE,
            n("dali.property.code"),
            n("dali.rule.controlRule.code.tip"),
            String.class);

    /**
     * Constant <code>FUNCTION</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> FUNCTION = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_FUNCTION,
            n("dali.rule.controlRule.function.short"),
            n("dali.rule.controlRule.function.tip"),
            FunctionDTO.class, true);

    /**
     * Constant <code>CONTROL_ELEMENT</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> CONTROL_ELEMENT = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_CONTROL_ELEMENT,
            n("dali.rule.controlRule.controlElement.short"),
            n("dali.rule.controlRule.controlElement.tip"),
            ControlElementDTO.class, true);

    /**
     * Constant <code>CONTROL_FEATURE</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> CONTROL_FEATURE = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_CONTROL_FEATURE,
            n("dali.rule.controlRule.controlFeature.short"),
            n("dali.rule.controlRule.controlFeature.tip"),
            ControlFeatureDTO.class, true);

    /**
     * Constant <code>ACTIVE</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> ACTIVE = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_ACTIVE,
            n("dali.rule.controlRule.active.short"),
            n("dali.rule.controlRule.active.tip"),
            Boolean.class);

    /**
     * Constant <code>BLOCKING</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> BLOCKING = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_BLOCKING,
            n("dali.rule.controlRule.blocking.short"),
            n("dali.rule.controlRule.blocking.tip"),
            Boolean.class);

    /**
     * Constant <code>MIN</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> MIN = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_MIN,
            n("dali.rule.controlRule.min.short"),
            n("dali.rule.controlRule.min.tip"),
            Object.class);

    /**
     * Constant <code>MAX</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> MAX = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_MAX,
            n("dali.rule.controlRule.max.short"),
            n("dali.rule.controlRule.max.tip"),
            Object.class);

    /**
     * Constant <code>ALLOWED_VALUES</code>
     */
    public static final DaliColumnIdentifier<ControlRuleRowModel> ALLOWED_VALUES = DaliColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_ALLOWED_VALUES,
            n("dali.rule.controlRule.allowedValues.short"),
            n("dali.rule.controlRule.allowedValues.tip"),
            String.class);

    /**
     * <p>Constructor for ControlRuleTableModel.</p>
     */
    public ControlRuleTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, true, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControlRuleRowModel createNewRow() {
        return new ControlRuleRowModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DaliColumnIdentifier<ControlRuleRowModel> getFirstColumnEditing() {
        return FUNCTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<ControlRuleRowModel> propertyName) {
        boolean editable = super.isCellEditable(rowIndex, columnIndex, propertyName);

        if (editable) {
            if (MIN.equals(propertyName) || MAX.equals(propertyName)) {
                ControlRuleRowModel rowModel = getEntry(rowIndex);
                FunctionDTO function = rowModel.getFunction();
                editable = ControlFunctionValues.MIN_MAX.equals(function) || ControlFunctionValues.MIN_MAX_DATE.equals(function);
//            } else if (ALLOWED_VALUES.equals(propertyName)) {
//                ControlRuleRowModel rowModel = getEntry(rowIndex);
//                FunctionDTO function = rowModel.getFunction();
//                editable = ControlFunctionValues.IS_AMONG.equals(function) || (ControlFunctionValues.PRECONDITION.equals(function) && rowModel.isPmfmValid());
            } else if (CONTROL_ELEMENT.equals(propertyName)
                    || CONTROL_FEATURE.equals(propertyName)
                    || BLOCKING.equals(propertyName)) {
                ControlRuleRowModel rowModel = getEntry(rowIndex);
                editable = !ControlFunctionValues.PRECONDITION.equals(rowModel.getFunction());
            }
        }

        if (ALLOWED_VALUES.equals(propertyName)) {
                ControlRuleRowModel rowModel = getEntry(rowIndex);
                FunctionDTO function = rowModel.getFunction();
                editable = ControlFunctionValues.IS_AMONG.equals(function) || (ControlFunctionValues.PRECONDITION.equals(function) && rowModel.isPmfmValid());
        }

        return editable;
    }
}
