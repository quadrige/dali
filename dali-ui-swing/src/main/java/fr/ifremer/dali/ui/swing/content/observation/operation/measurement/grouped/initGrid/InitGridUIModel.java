package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import fr.ifremer.dali.vo.PresetVO;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Created by Ludovic on 26/05/2015.
 */
public class InitGridUIModel extends AbstractDaliTableUIModel<QuadrigeBean, InitGridRowModel, InitGridUIModel> {

    private List<PmfmDTO> availablePmfms;
    public static final String PROPERTY_AVAILABLE_PMFMS = "availablePmfms";

    private List<QualitativeValueDTO> selectedValues;
    public static final String PROPERTY_SELECTED_VALUES = "selectedValues";

    private PresetVO preset;
    public static final String PROPERTY_PRESET = "preset";

    public List<PmfmDTO> getAvailablePmfms() {
        return availablePmfms;
    }

    public void setAvailablePmfms(List<PmfmDTO> availablePmfms) {
        this.availablePmfms = availablePmfms;
        firePropertyChange(PROPERTY_AVAILABLE_PMFMS, null, availablePmfms);
    }

    public List<QualitativeValueDTO> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<QualitativeValueDTO> selectedValues) {
        this.selectedValues = selectedValues;
        firePropertyChange(PROPERTY_SELECTED_VALUES, null, selectedValues);
    }

    public PresetVO getPreset() {
        return preset;
    }

    public void setPreset(PresetVO preset) {
        this.preset = preset;
        firePropertyChange(PROPERTY_PRESET, null, preset);
    }

    public boolean isValuesValid() {
        for (InitGridRowModel rowModel : getRows()) {
            if (CollectionUtils.isNotEmpty(rowModel.getQualitativeValues())) return true;
        }
        return false;
    }

    public void setValuesValid(boolean valuesValid) {
        // dummy setter
    }

}
