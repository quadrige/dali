package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class MatricesTableModel extends AbstractDaliTableModel<MatricesTableRowModel> {

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<MatricesTableRowModel> NAME = DaliColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class,
            true);

    /** Constant <code>DESCRIPTION</code> */
    public static final DaliColumnIdentifier<MatricesTableRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_DESCRIPTION,
            n("dali.property.description"),
            n("dali.property.description"),
            String.class);

    /** Constant <code>STATUS</code> */
    public static final DaliColumnIdentifier<MatricesTableRowModel> STATUS = DaliColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class,
            true);

    /** Constant <code>ASSOCIATED_FRACTIONS</code> */
    public static final DaliColumnIdentifier<MatricesTableRowModel> ASSOCIATED_FRACTIONS = DaliColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_FRACTIONS,
            n("dali.property.pmfm.matrix.associatedFractions"),
            n("dali.property.pmfm.matrix.associatedFractions"),
            FractionDTO.class,
            DecoratorService.COLLECTION_SIZE,
            true);

    public static final DaliColumnIdentifier<MatricesTableRowModel> COMMENT = DaliColumnIdentifier.newId(
        MatricesTableRowModel.PROPERTY_COMMENT,
        n("dali.property.comment"),
        n("dali.property.comment"),
        String.class,
        false);

    public static final DaliColumnIdentifier<MatricesTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        MatricesTableRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<MatricesTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        MatricesTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);


    /**
     * <p>Constructor for MatricesTableModel.</p>
     *
     * @param createNewRowAllowed a boolean.
     */
    public MatricesTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
    }

    /** {@inheritDoc} */
    @Override
    public MatricesTableRowModel createNewRow() {
        return new MatricesTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<MatricesTableRowModel> getFirstColumnEditing() {
        return NAME;
    }
}
