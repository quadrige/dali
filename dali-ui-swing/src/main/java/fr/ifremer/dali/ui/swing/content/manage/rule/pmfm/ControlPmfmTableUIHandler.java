package fr.ifremer.dali.ui.swing.content.manage.rule.pmfm;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.dali.dto.enums.ControlFunctionValues;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des PSFM.
 */
public class ControlPmfmTableUIHandler extends
        AbstractDaliTableUIHandler<ControlPmfmRowModel, ControlPmfmTableUIModel, ControlPmfmTableUI> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ControlPmfmTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlPmfmTableUIModel model = new ControlPmfmTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ControlPmfmTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Initialisation du tableau
        initTable();
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // pmfm id
        TableColumnExt idCol = addColumn(ControlPmfmTableModel.PMFM_ID);
        idCol.setSortable(true);
        idCol.setEditable(false);

        // Libelle
        TableColumnExt nameCol = addColumn(ControlPmfmTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // parametre
        final TableColumnExt parameterCol = addColumn(
                null,
                newTableCellRender(
                        ControlPmfmTableModel.PARAMETER.getPropertyType(),
                        ControlPmfmTableModel.PARAMETER.getDecoratorName(),
                        DecoratorService.NAME // Transcribed name as tooltip (Mantis #47110)
                ),
                ControlPmfmTableModel.PARAMETER);
        parameterCol.setSortable(true);
        parameterCol.setEditable(false);

        // support
        final TableColumnExt matrixCol = addColumn(ControlPmfmTableModel.MATRIX);
        matrixCol.setSortable(true);
        matrixCol.setEditable(false);

        // fraction
        final TableColumnExt fractionCol = addColumn(ControlPmfmTableModel.FRACTION);
        fractionCol.setSortable(true);
        fractionCol.setEditable(false);

        // methode
        final TableColumnExt methodCol = addColumn(ControlPmfmTableModel.METHOD);
        methodCol.setSortable(true);
        methodCol.setEditable(false);

        // unit
        final TableColumnExt unitCol = addColumn(ControlPmfmTableModel.UNIT);
        unitCol.setSortable(true);
        unitCol.setEditable(false);

        // Le model
        ControlPmfmTableModel tableModel = new ControlPmfmTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Initialisation du tableau
        initTable(table);
        idCol.setVisible(false);

        // Number rows visible
        table.setVisibleRowCount(3);
    }

    /**
     * <p>loadPSFMControle.</p>
     *
     * @param controlRule a {@link fr.ifremer.dali.dto.configuration.control.ControlRuleDTO} object.
     */
    public void loadPmfms(ControlRuleDTO controlRule, boolean editable) {

        if (controlRule == null) {

            // Empty table
            getModel().setBeans(null);

            // Set pmfm is not mandatory to avoid bad validation behaviour (Mantis #41992)
            getModel().setPmfmsMandatory(false);

            editable = false;

        } else {

            // if pmfm is mandatory
            getModel().setPmfmsMandatory(DaliBeans.isPmfmMandatory(controlRule));

            // disable table sorting for precondition rule
            if (ControlFunctionValues.PRECONDITION.equals(controlRule.getFunction())) {
                getTable().resetSortOrder();
                getTable().setSortable(false);
            } else {
                getTable().setSortable(true);
            }

            // Load table
            getModel().setBeans(controlRule.getRulePmfms());

        }

        getTable().setEditable(editable);
        recomputeRowsValidState(!getModel().isPmfmsMandatory());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, ControlPmfmRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        saveToParentModel();
    }

    private void saveToParentModel() {
        getModel().getParentModel().getControlRuleUIModel().getSingleSelectedRow().setRulePmfms(getModel().getBeans());

        getModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);

    }

    /**
     * <p>supprimerPSFMControle.</p>
     */
    public void clearTable() {

        // clear table
        getModel().setBeans(null);
        getModel().setPmfmsMandatory(false);
        getModel().setValid(true);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<ControlPmfmRowModel> getTableModel() {
        return (ControlPmfmTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getControlPmfmTable();
    }

    /**
     * <p>openAddPmfmDialog.</p>
     */
    public void openAddPmfmDialog() {
        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.PMFM.getFilterTypeId());
        dialog.setTitle(t("dali.filter.pmfm.addDialog.title"));
        List<PmfmDTO> pmfms = getModel().getBeans().stream().map(RulePmfmDTO::getPmfm)
                .sorted(getDecorator(PmfmDTO.class, DecoratorService.NAME_WITH_ID).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(pmfm -> pmfm.setReadOnly(true))
                .collect(Collectors.toList());
        dialog.getModel().setSelectedElements(pmfms);

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            List<PmfmDTO> newPmfms = dialog.getModel().getSelectedElements().stream()
                    .map(element -> ((PmfmDTO) element))
                    .filter(newPmfm -> getModel().getBeans().stream().noneMatch(rulePmfm -> rulePmfm.getPmfm().equals(newPmfm)))
                    .collect(Collectors.toList());

            if (!newPmfms.isEmpty()) {

                getModel().addBeans(newPmfms.stream().map(DaliBeans::pmfmToRulePmfm).collect(Collectors.toList()));
                getModel().setModify(true);
                saveToParentModel();
            }
        }
    }

    /**
     * <p>removePmfm.</p>
     */
    public void removePmfm() {
        getModel().deleteSelectedRows();
        saveToParentModel();
        recomputeRowsValidState(false);
    }
}
