package fr.ifremer.dali.ui.swing.content.manage.filter;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliSaveAction;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;

import java.util.List;

/**
 * Save action.
 */
public class SaveAction extends AbstractDaliSaveAction<FilterUIModel, FilterUI, FilterUIHandler> {

    private List<? extends FilterDTO> filtersToSave;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final FilterUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction() || !getModel().isModify() || !getModel().isValid()) {
            return false;
        }

        filtersToSave = getUI().getFilterListUI().getModel().getRows();
        return !filtersToSave.isEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        // Save Filters by generic method
        getContext().getContextService().saveFilters(filtersToSave);
    }

    /** {@inheritDoc} */
    @Override
    protected List<AbstractBeanUIModel> getModelsToModify() {
        return Lists.newArrayList(
                getModel().getFilterListUIModel(),
                getModel().getFilterElementUIModel()
        );
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getUI().getFilterListUI().getHandler().reloadComboBox();

        super.postSuccessAction();

    }
}
