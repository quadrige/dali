package fr.ifremer.dali.ui.swing.util.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableUIModel;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Abstract AbstractDaliTableUIModel class.</p>
 *
 * @param <B> type of incoming bean to edit
 * @param <R> type of the row of the table model
 * @param <M> type of this model
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public abstract class AbstractDaliTableUIModel<B extends QuadrigeBean, R extends AbstractDaliRowUIModel, M extends AbstractDaliTableUIModel<B, R, M>>
        extends AbstractTableUIModel<B, R, M> {

    /**
     * Property pour les colonnes dynamiques.
     */
    private List<PmfmDTO> pmfms;

    private List<PmfmTableColumn> pmfmColumns;
    public static final String PROPERTY_PMFM_COLUMNS = "pmfmColumns";

    /**
     * <p>Getter for the field <code>pmfms</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PmfmDTO> getPmfms() {
        return pmfms;
    }

    /**
     * <p>Setter for the field <code>pmfms</code>.</p>
     *
     * @param pmfms a {@link java.util.List} object.
     */
    public void setPmfms(List<PmfmDTO> pmfms) {
        this.pmfms = pmfms;
    }

    /**
     * <p>Getter for the field <code>pmfmColumns</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PmfmTableColumn> getPmfmColumns() {
        if (pmfmColumns == null) {
            pmfmColumns = new ArrayList<>();
        }
        return pmfmColumns;
    }

    public void addPmfmColumn(PmfmTableColumn pmfmColumn) {
        getPmfmColumns().add(pmfmColumn);
        firePropertyChange(PROPERTY_PMFM_COLUMNS, null, getPmfmColumns());
    }

}
