package fr.ifremer.dali.ui.swing.util.map.layer;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.geotools.data.FeatureSource;
import org.geotools.map.FeatureLayer;
import org.geotools.styling.Style;

/**
 * Encapsulating class indicating a layer of data (!= map layer)
 *
 * The title is used to compute the hash code
 *
 * @author peck7 on 05/09/2017.
 */
public class DataFeatureLayer extends FeatureLayer implements DataLayer{

    /**
     * The holder of this data layer
     */
    private final DataLayerCollection dataLayerCollection;

    private final Integer id;

    public DataFeatureLayer(DataLayerCollection dataLayerCollection, Integer id, FeatureSource featureSource, Style style, String title) {
        super(featureSource, style, title);
        assert title != null;
        this.dataLayerCollection = dataLayerCollection;
        this.id = id;
    }

    public DataLayerCollection getDataLayerCollection() {
        return dataLayerCollection;
    }

    public Integer getId() {
        return id;
    }

    /**
     * the hash code is compute by the title
     * @return hash code
     */
    @Override
    public int hashCode() {
        return getTitle().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DataFeatureLayer && hashCode() == obj.hashCode();
    }

}
