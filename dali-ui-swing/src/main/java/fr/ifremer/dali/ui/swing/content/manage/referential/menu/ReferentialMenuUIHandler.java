package fr.ifremer.dali.ui.swing.content.manage.referential.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;

import java.util.List;

/**
 * Created by Ludovic on 23/03/2016.
 */
public abstract class ReferentialMenuUIHandler<M extends AbstractReferentialMenuUIModel, UI extends ReferentialMenuUI<M, ?>> extends AbstractDaliUIHandler<M, UI> {

    /** {@inheritDoc} */
    @Override
    public void afterInit(UI ui) {
        initUI(ui);

        // set filter list
        initBeanFilterableComboBox(getApplyFilterUI().getApplyFilterCombo(), getFilters(), null);
    }

    /**
     * Search activaton.
     *
     * @param enabled a boolean.
     */
    public abstract void enableSearch(boolean enabled);

    /**
     * get filters to display in ApplyFilterUI
     *
     * @return a {@link java.util.List} object.
     */
    public abstract List<FilterDTO> getFilters();

    /**
     * get the ApplyFilterUI
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUI} object.
     */
    public abstract ApplyFilterUI getApplyFilterUI();


    /**
     * <p>enableContextFilter.</p>
     *
     * @param enabled a boolean.
     */
    public void enableContextFilter(boolean enabled) {
        getApplyFilterUI().setVisible(enabled);
    }

}
