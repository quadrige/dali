package fr.ifremer.dali.ui.swing.content.home.map;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.map.MapMode;
import fr.ifremer.dali.ui.swing.util.map.MapParentUIModel;
import fr.ifremer.dali.ui.swing.util.map.MapUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

import java.util.Collection;

/**
 * UI Model for surveys map
 */
public class SurveysMapUIModel extends AbstractEmptyUIModel<SurveysMapUIModel> implements MapUIModel {

    private MapMode mapMode;

    private MapParentUIModel parentUIModel;

    private AbstractDaliTableUIModel<SurveyDTO, ? extends SurveyDTO, ?> selectionModel;

    private Collection<? extends SurveyDTO> selectedSurveys;

    private boolean buildMapOnSelectionChanged = true;

    /**
     * Constructor.
     */
    public SurveysMapUIModel() {
        super();
    }

    @Override
    public MapParentUIModel getParentUIModel() {
        return parentUIModel;
    }

    public void setParentUIModel(MapParentUIModel parentModel) {
        this.parentUIModel = parentModel;
    }

    public AbstractDaliTableUIModel<SurveyDTO, ? extends SurveyDTO, ?> getSelectionModel() {
        return selectionModel;
    }

    public void setSelectionModel(AbstractDaliTableUIModel<SurveyDTO, ? extends SurveyDTO, ?> selectionModel) {
        this.selectionModel = selectionModel;
    }

    public Collection<? extends SurveyDTO> getSelectedSurveys() {
        if (selectionModel != null) return selectionModel.getSelectedRows();
        return selectedSurveys;
    }

    public void setSelectedSurvey(SurveyDTO selectedSurvey) {
        this.selectedSurveys = ImmutableList.of(selectedSurvey);
    }

    public MapMode getMapMode() {
        return mapMode;
    }

    public void setMapMode(MapMode mapMode) {
        this.mapMode = mapMode;
    }

    public boolean isBuildMapOnSelectionChanged() {
        return buildMapOnSelectionChanged;
    }

    public void setBuildMapOnSelectionChanged(boolean buildMapOnSelectionChanged) {
        this.buildMapOnSelectionChanged = buildMapOnSelectionChanged;
    }
}
