package fr.ifremer.dali.ui.swing.util.map;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.map.layer.DataFeatureLayer;
import org.geotools.geometry.DirectPosition2D;

import java.awt.Point;
import java.util.EventObject;
import java.util.Set;

/**
 * @author peck7 on 14/09/2017.
 */
public class DataSelectionEvent extends EventObject {

    public enum Type {

        /**
         * The selectedDataLayers layer selection
         * The selectedDataLayers object is a Set of <{@link DataFeatureLayer}/>
         */
        DATA_LAYER_SELECTED,

        /**
         * Empty selection
         */
        EMPTY_SELECTION
    }

    private Type type;

    private Set<DataFeatureLayer> selectedDataLayers;

    private Point screenPoint;

    private DirectPosition2D worldPos;

    public DataSelectionEvent(DataMapPane source, Type type) {
        this(source, type, null, null, null);
    }

    /**
     * Constructs the Event.
     *
     * @param source      The object on which the Event initially occurred.
     * @param type        the type of event
     * @param dataLayers  the selected data layers
     * @param screenPoint the selection point in screen coordinates
     * @param worldPos    the selection point in world coordinates
     * @throws IllegalArgumentException if source is null.
     */
    public DataSelectionEvent(DataMapPane source, Type type, Set<DataFeatureLayer> dataLayers, Point screenPoint, DirectPosition2D worldPos) {
        super(source);
        this.type = type;
        this.selectedDataLayers = dataLayers;
        this.screenPoint = screenPoint;
        this.worldPos = worldPos;
    }

    @Override
    public DataMapPane getSource() {
        return (DataMapPane) super.getSource();
    }

    public Type getType() {
        return type;
    }

    public Set<DataFeatureLayer> getSelectedDataLayers() {
        return selectedDataLayers;
    }

    public Point getScreenPoint() {
        return screenPoint;
    }

    public DirectPosition2D getWorldPos() {
        return worldPos;
    }
}
