package fr.ifremer.dali.ui.swing.util.validator;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.ErrorAware;
import fr.ifremer.dali.dto.ErrorDTO;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;

import static org.nuiton.i18n.I18n.t;

/**
 * A customized FieldValidatorSupport that shows control messages from a AbstractDaliTableUIModel with ErrorAware row model
 * <p/>
 * Created by Ludovic on 01/07/2015.
 */
public class ControlTableExpressionValidator extends AbstractControlExpressionValidator {

    /** {@inheritDoc} */
    @Override
    public void validate(Object object) throws ValidationException {

        Object objectValue = getFieldValue(getFieldName(), object);

        if (!(objectValue instanceof AbstractDaliTableUIModel)) {
            return;
        }

        AbstractDaliTableUIModel<? extends QuadrigeBean, ? extends AbstractDaliRowUIModel, ?> model =
                (AbstractDaliTableUIModel<? extends QuadrigeBean, ? extends AbstractDaliRowUIModel, ?>) objectValue;

        for (AbstractDaliRowUIModel row : model.getRows()) {
            if (row instanceof ErrorAware) {

                if (isErrorActive()) {
                    for (ErrorDTO error : DaliBeans.getErrors((ErrorAware) row, false)) {
                        addFieldErrorMessage(ApplicationUIUtil.removeHtmlTags(t("dali.validator.error", error.getMessage())));
                    }
                }

                if (isControlErrorActive()) {
                    for (ErrorDTO error : DaliBeans.getErrors((ErrorAware) row, true)) {
                        addFieldErrorMessage(ApplicationUIUtil.removeHtmlTags(t("dali.validator.error.control", error.getMessage())));
                    }
                }

                if (isWarningActive()) {
                    for (ErrorDTO warning : DaliBeans.getWarnings((ErrorAware) row, false)) {
                        addFieldErrorMessage(ApplicationUIUtil.removeHtmlTags(t("dali.validator.warning", warning.getMessage())));
                    }
                }

                if (isControlWarningActive()) {
                    for (ErrorDTO warning : DaliBeans.getWarnings((ErrorAware) row, true)) {
                        addFieldErrorMessage(ApplicationUIUtil.removeHtmlTags(t("dali.validator.warning.control", warning.getMessage())));
                    }
                }
            }
        }
    }

}

