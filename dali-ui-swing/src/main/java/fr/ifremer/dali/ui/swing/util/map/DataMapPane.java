package fr.ifremer.dali.ui.swing.util.map;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.map.Maps;
import fr.ifremer.dali.ui.swing.util.map.layer.*;
import fr.ifremer.dali.ui.swing.util.map.layer.tile.MapTileLayer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.renderer.GTRenderer;
import org.geotools.renderer.label.LabelCacheImpl;
import org.geotools.renderer.lite.LabelCache;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.swing.AbstractMapPane;
import org.geotools.swing.RenderingExecutor;
import org.geotools.swing.event.MapPaneAdapter;
import org.geotools.swing.event.MapPaneEvent;
import org.opengis.geometry.Envelope;

import java.awt.*;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.util.List;
import java.util.*;

/**
 * A featured MapPane with better buffered image than JMapPane
 *
 * @author peck7 on 21/06/2017.
 */
public class DataMapPane extends AbstractMapPane {

    private static final Log LOG = LogFactory.getLog(DataMapPane.class);

    /**
     * The default step ratio
     */
    public static final double ZOOM_STEP_RATIO = 0.3;

    private GTRenderer renderer;
    private BufferedImage baseImage;
    private Graphics2D baseImageGraphics;

    // interstitial image painted before next render
    private BufferedImage interstitialImage;
    private Rectangle interstitialRectangle = new Rectangle();
    private boolean drawInterstitialImageOnce;
    private boolean busy;
    private boolean newContentPending;
    private boolean graticuleVisible = true;
    private boolean legendVisible = false;

    // the map builder
    private MapBuilder mapBuilder;

    // the data layer collection build by the map builder
    private final List<DataLayerCollection> dataLayerCollections;
    private final Set<DataSelectionListener> selectionListeners = new HashSet<>();

    /**
     * Creates a new map pane.
     */
    public DataMapPane() {
        this(null);
    }

    /**
     * Creates a new map pane.
     *
     * @param content the map content containing the layers to display
     *                (may be {@code null})
     */
    public DataMapPane(MapContent content) {
        this(content, null, null);
    }

    /**
     * Creates a new map pane. Any or all arguments may be {@code null}
     *
     * @param content  the map content containing the layers to display
     * @param executor the rendering executor to manage drawing
     * @param renderer the renderer to use for drawing layers
     */
    public DataMapPane(MapContent content, RenderingExecutor executor, GTRenderer renderer) {
        super(content, executor);
        doSetRenderer(renderer);
        setDoubleBuffered(true);

        dataLayerCollections = Lists.newArrayList();

        // default tool
        setCursorTool(new MultipleCursorTool(this));

        // add mapPane listener
        addMapPaneListener(new MapPaneAdapter() {
            @Override
            public void onDisplayAreaChanged(MapPaneEvent ev) {
                ReferencedEnvelope newDisplayArea = (ReferencedEnvelope) ev.getData();
                pendingDisplayArea = newDisplayArea;

                if (!newContentPending) {
                    setLayersVisible(newDisplayArea);
                }
            }

            @Override
            public void onRenderingStarted(MapPaneEvent ev) {
                busy = true;
            }

            @Override
            public void onRenderingStopped(MapPaneEvent ev) {
                busy = false;
            }
        });
    }

    public MapBuilder getMapBuilder() {
        return mapBuilder;
    }

    public void setMapBuilder(MapBuilder mapBuilder) {
        this.mapBuilder = mapBuilder;

        // Affect default background
        setBackground(mapBuilder.getBackgroundColor());
    }

    public List<DataLayerCollection> getDataLayerCollections() {
        return dataLayerCollections;
    }

    public void clearDataLayerCollections() {
        dataLayerCollections.clear();
    }

    public DataLayerCollection newDataLayerCollection(Object dataObject) throws Exception {

        DataLayerCollection dataLayerCollection = mapBuilder.buildDataLayerCollection(dataObject);
        dataLayerCollections.add(dataLayerCollection);
        return dataLayerCollection;

    }

    /**
     * Get the overall envelope (=union of all envelope in data layer collection)
     *
     * @return overall envelope
     */
    public ReferencedEnvelope getOverallEnvelope() {
        ReferencedEnvelope overallEnvelope = new ReferencedEnvelope(
            !CRS.equalsIgnoreMetadata(DefaultGeographicCRS.WGS84, getMapBuilder().getTargetCRS())
                ? DefaultGeographicCRS.WGS84
                : null);
        for (DataLayerCollection dataLayerCollection : getDataLayerCollections()) {
            overallEnvelope.expandToInclude(dataLayerCollection.getEnvelope());
        }
        return overallEnvelope;
    }

    /**
     * The overall envelope with a buffer
     */
    public ReferencedEnvelope getOverallDisplayEnvelope() {
        ReferencedEnvelope envelope = getOverallEnvelope();
        if (!envelope.isEmpty()) {
            // expand envelope by 10% of the biggest dimension or by a fixed size if envelope is a point
            if (envelope.getHeight() == 0 && envelope.getWidth() == 0)
                envelope.expandBy(0.05);
            else
                envelope.expandBy(Math.max(envelope.getHeight(), envelope.getWidth()) * 0.1);
        }
        return envelope;
    }

    public void addDataSelectionListener(DataSelectionListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener must not be null");
        }
        selectionListeners.add(listener);
    }

    public void removeDataSelectionListener(DataSelectionListener listener) {
        if (listener != null) selectionListeners.remove(listener);
    }

    void publishSelectionEvent(DataSelectionEvent event) {
        for (DataSelectionListener listener : selectionListeners) {
            switch (event.getType()) {
                case DATA_LAYER_SELECTED:
                    listener.onDataLayerSelected(event);
                    break;
                case EMPTY_SELECTION:
                    listener.onEmptySelection(event);
                    break;
            }
        }
    }

    /*
     MAP CONTENT HANDLING
     */

    public void clearMapContent() {
        if (getMapContent() != null) {
            getMapContent().dispose();
            setMapContent(null);
        }
    }

    /**
     * Override default setMapContent and doSetMapContent (private) to set the pending display area at first render
     *
     * @param content new content
     */
    @Override
    public void setMapContent(MapContent content) {
        //super.setMapContent(content); // Don't call super method

        paramsLock.writeLock().lock();
        try {
            doSetMapContent(content);

        } finally {
            paramsLock.writeLock().unlock();
        }

        if (content != null && getRenderer() != null) {
            // If the new map content had layers to draw, and this pane is visible,
            // then the map content will already have been set with the renderer
            //
            if (getRenderer().getMapContent() != content) { // just check reference equality
                getRenderer().setMapContent(mapContent);
            }
        }
    }

    /**
     * Override the private method from org.geotools.swing.AbstractMapPane
     *
     * @param newMapContent new map content
     */
    private void doSetMapContent(MapContent newMapContent) {
        if (mapContent != newMapContent) {

            newContentPending = true;

            if (mapContent != null) {
                mapContent.removeMapLayerListListener(this);
                for (Layer layer : mapContent.layers()) {
                    if (layer instanceof ComponentListener) {
                        removeComponentListener((ComponentListener) layer);
                    }
                }
            }

            mapContent = newMapContent;

            if (mapContent != null) {
                MapViewport viewport = mapContent.getViewport();
                viewport.setMatchingAspectRatio(true);
                Rectangle rect = getVisibleRect();
                if (!rect.isEmpty()) {
                    viewport.setScreenArea(rect);
                }

                mapContent.addMapLayerListListener(this);
                mapContent.addMapBoundsListener(this);

                if (!mapContent.layers().isEmpty()) {
                    // set all layers as selected by default for the info tool
                    for (Layer layer : mapContent.layers()) {
                        layer.setSelected(true);

                        if (layer instanceof ComponentListener) {
                            addComponentListener((ComponentListener) layer);
                        }

                        // affect this to osm layers
                        if (layer instanceof MapTileLayer) {
                            ((MapTileLayer) layer).setMapPane(this);
                        }
                    }

                    setFullExtent();

                    // If pending display area is set, so affect it directly at first render
                    if (pendingDisplayArea == null) {
                        doSetDisplayArea(mapContent.getViewport().getBounds());
                    } else {

                        // transform if needed
                        doSetDisplayArea(pendingDisplayArea);
                    }
                }
            }

            MapPaneEvent event = new MapPaneEvent(this, MapPaneEvent.Type.NEW_MAPCONTENT, mapContent);
            publishEvent(event);

            // Clear label cache to prevent old labels still rendered after new content
            clearLabelCache();

            drawLayers(true); // Force create a new image when content changes (Mantis #52670)
            newContentPending = false;
        }
    }

    @Override
    public void setDisplayArea(Envelope envelope) {

        InfoPanelLayer infoPanelLayer = getInfoPanelLayer();
        if (infoPanelLayer != null)
            infoPanelLayer.clear();

        imageOrigin.setLocation(0, 0);
        if (!drawInterstitialImageOnce)
            interstitialRectangle = new Rectangle();

        super.setDisplayArea(envelope);
    }

    @Override
    protected void doSetDisplayArea(Envelope envelope) {

        if (envelope instanceof ReferencedEnvelope) {
            ReferencedEnvelope referencedEnvelope = (ReferencedEnvelope) envelope;

            // transform envelope to map CRS
            referencedEnvelope = Maps.transformReferencedEnvelope(referencedEnvelope,
                getMapContent() != null ? getMapContent().getCoordinateReferenceSystem() : getMapBuilder().getTargetCRS());

            // limit envelope to restricted area
            if (fullExtent != null) {

                if (referencedEnvelope.getMinY() < fullExtent.getMinY() && referencedEnvelope.getMaxY() > fullExtent.getMaxY()
                    && referencedEnvelope.getMinX() < fullExtent.getMinX() && referencedEnvelope.getMaxX() > fullExtent.getMaxX()) {

                    // Reset to fullExtend if both axis over limits
                    referencedEnvelope = fullExtent;
                    if (LOG.isDebugEnabled()) LOG.debug("envelope reset to full extent");

                } else {

                    // limit envelope in y axis
                    if (referencedEnvelope.getMinY() < fullExtent.getMinY() && referencedEnvelope.getMaxY() < fullExtent.getMaxY()) {
                        referencedEnvelope.translate(0, Math.min(fullExtent.getMaxY() - referencedEnvelope.getMaxY(), fullExtent.getMinY() - referencedEnvelope.getMinY()));
                        if (LOG.isDebugEnabled()) LOG.debug("Y ↑");
                    }
                    if (referencedEnvelope.getMinY() > fullExtent.getMinY() && referencedEnvelope.getMaxY() > fullExtent.getMaxY()) {
                        referencedEnvelope.translate(0, Math.max(fullExtent.getMaxY() - referencedEnvelope.getMaxY(), fullExtent.getMinY() - referencedEnvelope.getMinY()));
                        if (LOG.isDebugEnabled()) LOG.debug("Y ↓");
                    }

                    // limit envelope in x axis
                    if (referencedEnvelope.getMinX() < fullExtent.getMinX() && referencedEnvelope.getMaxX() < fullExtent.getMaxX()) {
                        referencedEnvelope.translate(Math.min(fullExtent.getMaxX() - referencedEnvelope.getMaxX(), fullExtent.getMinX() - referencedEnvelope.getMinX()), 0);
                        if (LOG.isDebugEnabled()) LOG.debug("← X");
                    }
                    if (referencedEnvelope.getMinX() > fullExtent.getMinX() && referencedEnvelope.getMaxX() > fullExtent.getMaxX()) {
                        referencedEnvelope.translate(Math.max(fullExtent.getMaxX() - referencedEnvelope.getMaxX(), fullExtent.getMinX() - referencedEnvelope.getMinX()), 0);
                        if (LOG.isDebugEnabled()) LOG.debug("X →");
                    }
                }
            }

            super.doSetDisplayArea(referencedEnvelope);

        } else {

            super.doSetDisplayArea(envelope);
        }

    }

    private void clearLabelCache() {
        if (labelCache != null) {
            labelCache.stop();
            labelCache.clear();
        }
    }

    /**
     * Gets the renderer, creating a default one if required.
     *
     * @return the renderer
     */
    public GTRenderer getRenderer() {
        if (renderer == null) {
            doSetRenderer(new StreamingRenderer());
        }
        return renderer;
    }

    /**
     * Sets the renderer to be used by this map pane.
     *
     * @param renderer the renderer to use
     */
    public void setRenderer(GTRenderer renderer) {
        doSetRenderer(renderer);
    }

    private void doSetRenderer(GTRenderer newRenderer) {
        if (newRenderer != null) {
            Map<Object, Object> hints = newRenderer.getRendererHints();
            if (hints == null) {
                hints = new HashMap<>();
            }

            if (newRenderer instanceof StreamingRenderer) {
                if (hints.containsKey(StreamingRenderer.LABEL_CACHE_KEY)) {
                    labelCache = (LabelCache) hints.get(StreamingRenderer.LABEL_CACHE_KEY);
                } else {
                    labelCache = new LabelCacheImpl();
                    hints.put(StreamingRenderer.LABEL_CACHE_KEY, labelCache);
                }

                // set render modes
                hints.put(StreamingRenderer.TEXT_RENDERING_KEY, StreamingRenderer.TEXT_RENDERING_OUTLINE);
            }

            Map<RenderingHints.Key, Object> j2dHintsMap = new HashMap<>();
            j2dHintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            j2dHintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
            RenderingHints j2dHints = new RenderingHints(j2dHintsMap);

            newRenderer.setJava2DHints(j2dHints);
            newRenderer.setRendererHints(hints);

            if (mapContent != null) {
                newRenderer.setMapContent(mapContent);
            }
        }

        renderer = newRenderer;
    }

    /**
     * Retrieve the map pane's current base image.
     * <p>
     * The map pane caches the most recent rendering of map layers
     * as an image to avoid time-consuming rendering requests whenever
     * possible. The base image will be re-drawn whenever there is a
     * change to map layer data, style or visibility; and it will be
     * replaced by a new image when the pane is resized.
     * <p>
     * This method returns a <b>live</b> reference to the current
     * base image. Use with caution.
     *
     * @return a live reference to the current base image
     */
    public RenderedImage getBaseImage() {
        return this.baseImage;
    }

    public boolean isBusy() {
        return busy;
    }

    @Override
    public void reset() {

        super.reset();

        // Reset the pending display area
        pendingDisplayArea = null;
    }

    @Override
    protected void onShownOrResized() {

        // Don't schedule the 'setForNewSize' if nothing to compute.
        if (mapContent == null) return;

        super.onShownOrResized();

    }

    @Override
    public void moveImage(int dx, int dy) {
        drawingLock.lock();
        try {
            if (isShowing() && !getVisibleRect().isEmpty()) {

                if (interstitialRectangle.isEmpty()) {
                    interstitialRectangle = getMapContent().getViewport().getScreenArea().getBounds();
                }

                interstitialRectangle.translate(dx, dy);
                drawInterstitialImageOnce = false;
                repaint();
            }

        } finally {
            drawingLock.unlock();
        }
    }

    public void zoomIn() {
        applyZoom(1 + ZOOM_STEP_RATIO, null);
    }

    public void zoomOut() {
        applyZoom(1 - ZOOM_STEP_RATIO, null);
    }

    public void applyZoom(double zoomRatio, Point zoomCenter) {
//        if (busy) return; // Don't wait for render finished (Mantis #52670)

        ReferencedEnvelope displayArea = getDisplayArea();
        Rectangle bounds = getMapContent().getViewport().getScreenArea();

        // calculate the deltas for envelope
        double deltaWidth = displayArea.getWidth() * (zoomRatio - 1);
        double deltaHeight = displayArea.getHeight() * (zoomRatio - 1);
        // calculate the deltas for view (zoomRation is 10% larger here to compensate the envelope enlargement due to StreamingRender)
        int deltaViewWidth = (int) ((bounds.width * (zoomRatio * 1.1 - 1)));
        int deltaViewHeight = (int) ((bounds.height * (zoomRatio * 1.1 - 1)));

        // calculate shifting from center point if defined, or 0.5 on each axe to preserve center
        double ratioLeft = zoomCenter != null ? zoomCenter.getX() * 1d / getWidth() : 0.5;
        double ratioTop = zoomCenter != null ? 1 - (zoomCenter.getY() * 1d / getHeight()) : 0.5; // Y axis inverted between swing and crs

        // the delta for X
        double deltaLeft = deltaWidth * ratioLeft;
        double deltaRight = deltaLeft - deltaWidth;
        int deltaViewLeft = (int) (deltaViewWidth * ratioLeft);

        // the delta for Y
        double deltaTop = deltaHeight * ratioTop;
        double deltaBottom = deltaTop - deltaHeight;
        int deltaViewTop = (int) (deltaViewHeight * (1 - ratioTop));

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Map mouse zoom (zoom ratio : %s, deltaLeft : %s, deltaRight : %s, deltaTop : %s, deltaBottom : %s)",
                zoomRatio, deltaLeft, deltaRight, deltaRight, deltaBottom));
        }

        // compute new envelope
        ReferencedEnvelope newDisplayArea = new ReferencedEnvelope(
            displayArea.getMinX() + deltaLeft,
            displayArea.getMaxX() + deltaRight,
            displayArea.getMinY() + deltaTop,
            displayArea.getMaxY() + deltaBottom,
            displayArea.getCoordinateReferenceSystem()
        );

        // compute new bounds
        Rectangle newViewBounds = new Rectangle(
            bounds.x - deltaViewLeft,
            bounds.y - deltaViewTop,
            bounds.width + deltaViewWidth,
            bounds.height + deltaViewHeight
        );
        // Paint zoomed image
        resizeImage(newViewBounds);

        // Set display area (submit a render)
        setDisplayArea(newDisplayArea);

    }

    /**
     * Draw the interstitial image in the provided rectangle
     *
     * @param rectangle target rectangle
     */
    public void resizeImage(Rectangle rectangle) {
        if (isShowing() && !getVisibleRect().isEmpty()
            // Don't paint buffered image when shape map is used (aka offline), the render is too fast to have a good behavior
            && mapBuilder.getCurrentMapMode().isOnline()) {

            // Set the interstitial image bounds
            interstitialRectangle.setBounds(rectangle);
            drawInterstitialImageOnce = true;
            repaint();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (drawingLock.tryLock()) {
            try {
                Graphics2D g2 = (Graphics2D) g;

                // If interstitial image is available with a target rectangle
                if (interstitialImage != null && interstitialImage.getWidth() > 0 && !interstitialRectangle.isEmpty()) {

                    // Antialiasing ON
                    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

                    // Draw interstitial image if valid
                    g2.drawImage(interstitialImage, interstitialRectangle.x, interstitialRectangle.y, interstitialRectangle.width, interstitialRectangle.height, null);

                    // Reset the zoom rectangle to avoid another interstitial repaint
                    if (drawInterstitialImageOnce)
                        interstitialRectangle = new Rectangle();

                } else

                    // If base image has been rendered
                    if (baseImage != null) {

                        // Draw rendered image
                        g2.drawImage(baseImage, imageOrigin.x, imageOrigin.y, null);

                        // copy baseImage to interstitial image
                        interstitialImage = deepCopy(baseImage);

                    }
            } finally {
                drawingLock.unlock();
            }
        }
    }

    public void redrawLayers() {
        redrawLayers(false);
    }

    public void redrawLayers(boolean createNewImage) {
        if (mapContent != null) {
            clearLabelCache.set(true);
            if (createNewImage) clearLabelCache();
            drawLayers(createNewImage);
        }
    }

    /**
     * Default drawLayers method
     *
     * @param createNewImage true will force redraw on new image
     */
    @Override
    protected void drawLayers(boolean createNewImage) {
        drawingLock.lock();
        try {
            if (mapContent != null
                && !mapContent.getViewport().isEmpty()
                && acceptRepaintRequests.get()) {

                Rectangle r = getVisibleRect();
                if (baseImage == null || createNewImage) {
                    baseImage = GraphicsEnvironment.getLocalGraphicsEnvironment().
                        getDefaultScreenDevice().getDefaultConfiguration().
                        createCompatibleImage(r.width, r.height, Transparency.TRANSLUCENT);

                    if (baseImageGraphics != null) {
                        baseImageGraphics.dispose();
                    }

                    baseImageGraphics = baseImage.createGraphics();
                    clearLabelCache.set(true);

                }
                // Always reset background
                if (baseImageGraphics != null) {
                    baseImageGraphics.setBackground(getBackground());

                    // reset full background due to transparency
                    if (mapBuilder.getCurrentMapMode().isTransparent()
                        // or if current enveloppe is over full extend
                        || isCurrentDisplayAreaOverFullExtend()) {

                        baseImageGraphics.clearRect(0, 0, r.width, r.height);
                    }
                }

                if (mapContent != null && !mapContent.layers().isEmpty()) {

                    // Submit a render. the result will be drawn in baseImage
                    getRenderingExecutor().submit(mapContent, getRenderer(), baseImageGraphics, this);
                }
            }
        } finally {
            drawingLock.unlock();
        }
    }

    private boolean isCurrentDisplayAreaOverFullExtend() {
        if (fullExtent != null) {
            return !fullExtent.covers(getDisplayArea());
        }
        return false;
    }

    private static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    /**
     * Compute the visibility of each layer in the current map depending on their definition (see MapLayerDefinition)
     *
     * @param envelope the envelope
     */
    public void setLayersVisible(ReferencedEnvelope envelope) {
        if (mapContent != null) {
            setLayersVisible(mapContent.layers(), envelope);
        }
    }

    /**
     * Compute the visibility of each layer depending on their definition (see MapLayerDefinition)
     *
     * @param layers   the layer collection
     * @param envelope the envelope
     */
    public void setLayersVisible(List<Layer> layers, ReferencedEnvelope envelope) {

        if (CollectionUtils.isNotEmpty(layers)) {
            List<String> displayableLayerNames = mapBuilder.getDisplayableLayerNames(envelope);
            for (Layer layer : layers) {
                if (layer instanceof MapLayer) {
                    // A map layer is visible if mapBuilder allows it
                    layer.setVisible(displayableLayerNames.contains(layer.getTitle()));
                } else if (layer instanceof GraticuleLayer) {
                    // A graticule layer is visible depending graticuleVisible property
                    layer.setVisible(isGraticuleVisible());
                } else if (layer instanceof LegendLayer) {
                    // A legend layer is visible depending legendVisible property
                    layer.setVisible(isLegendVisible());
                } else {
                    // A data layer is always visible
                    layer.setVisible(true);
                }
            }
        }
    }

    /**
     * Toggle graticule visibility
     */
    public void toggleGraticuleVisibility() {
        graticuleVisible = !graticuleVisible;

        if (getMapContent() != null) {
            for (Layer layer : getMapContent().layers()) {
                if (layer instanceof GraticuleLayer) {
                    layer.setVisible(graticuleVisible);
                }
            }
        }

    }

    /**
     * Determine if graticule is visible
     */
    public boolean isGraticuleVisible() {
        return graticuleVisible;
    }

    /**
     * Toggle legend visibility
     */
    public void toggleLegendVisibility() {
        legendVisible = !legendVisible;

        if (getMapContent() != null) {
            for (Layer layer : getMapContent().layers()) {
                if (layer instanceof LegendLayer) {
                    layer.setVisible(legendVisible);
                }
            }
        }

    }

    /**
     * Determine if legend is visible
     */
    public boolean isLegendVisible() {
        return legendVisible;
    }

    public InfoPanelLayer getInfoPanelLayer() {
        if (mapContent != null) {
            for (Layer layer : mapContent.layers()) {
                if (layer instanceof InfoPanelLayer) return (InfoPanelLayer) layer;
            }
        }
        return null;
    }

    public void fixFullExtend() {

        getRenderer().stopRendering();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignored) {
        }

        zoomIn();

    }
}
