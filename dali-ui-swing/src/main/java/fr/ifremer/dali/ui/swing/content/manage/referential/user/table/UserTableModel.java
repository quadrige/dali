/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.dali.ui.swing.content.manage.referential.user.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>UserTableModel class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class UserTableModel extends AbstractDaliTableModel<UserRowModel> {

    /**
     * <p>Constructor for UserTableModel.</p>
     *
     * @param createNewRowAllowed a boolean.
     */
    public UserTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
    }

    /** {@inheritDoc} */
    @Override
    public UserRowModel createNewRow() {
        return new UserRowModel();
    }

    /** Constant <code>REG_CODE</code> */
    public static final DaliColumnIdentifier<UserRowModel> REG_CODE = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_REG_CODE,
            n("dali.property.user.regCode"),
            n("dali.property.user.regCode"),
            String.class);

    /** Constant <code>LASTNAME</code> */
    public static final DaliColumnIdentifier<UserRowModel> LASTNAME = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_NAME,
            n("dali.property.user.lastname"),
            n("dali.property.user.lastname"),
            String.class,
            true);

    /** Constant <code>FIRSTNAME</code> */
    public static final DaliColumnIdentifier<UserRowModel> FIRSTNAME = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_FIRST_NAME,
            n("dali.property.user.firstname"),
            n("dali.property.user.firstname"),
            String.class,
            true);

    /** Constant <code>DEPARTMENT</code> */
    public static final DaliColumnIdentifier<UserRowModel> DEPARTMENT = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_DEPARTMENT,
            n("dali.property.department"),
            n("dali.property.department"),
            DepartmentDTO.class,
            true);

    /** Constant <code>INTRANET_LOGIN</code> */
    public static final DaliColumnIdentifier<UserRowModel> INTRANET_LOGIN = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_INTRANET_LOGIN,
            n("dali.property.user.intranetLogin"),
            n("dali.property.user.intranetLogin"),
            String.class,
            true);

    /** Constant <code>EXTRANET_LOGIN</code> */
    public static final DaliColumnIdentifier<UserRowModel> EXTRANET_LOGIN = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_EXTRANET_LOGIN,
            n("dali.property.user.extranetLogin"),
            n("dali.property.user.extranetLogin"),
            String.class);

    /** Constant <code>HAS_PASSWORD</code> */
    public static final DaliColumnIdentifier<UserRowModel> HAS_PASSWORD = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_HAS_PASSWORD,
            n("dali.property.user.password"),
            n("dali.property.user.password"),
            Boolean.class);

    /** Constant <code>EMAIL</code> */
    public static final DaliColumnIdentifier<UserRowModel> EMAIL = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_EMAIL,
            n("dali.property.email"),
            n("dali.property.email"),
            String.class);

    /** Constant <code>PHONE</code> */
    public static final DaliColumnIdentifier<UserRowModel> PHONE = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_PHONE,
            n("dali.property.phone"),
            n("dali.property.phone"),
            String.class);

    /** Constant <code>ADDRESS</code> */
    public static final DaliColumnIdentifier<UserRowModel> ADDRESS = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_ADDRESS,
            n("dali.property.address"),
            n("dali.property.address"),
            String.class);

    /** Constant <code>STATUS</code> */
    public static final DaliColumnIdentifier<UserRowModel> STATUS = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class,
            true);

    // PRIVILEGES
    /** Constant <code>PRIVILEGES</code> */
    public static final DaliColumnIdentifier<UserRowModel> PRIVILEGES = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_PRIVILEGE_SIZE,
            n("dali.property.user.privileges"),
            n("dali.property.user.privileges"),
            Integer.class);

    /** Constant <code>ORGANISM</code> */
    public static final DaliColumnIdentifier<UserRowModel> ORGANISM = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_ORGANISM,
            n("dali.property.department"),
            n("dali.property.department"),
            String.class);

    /** Constant <code>ADMIN_CENTER</code> */
    public static final DaliColumnIdentifier<UserRowModel> ADMIN_CENTER = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_ADMIN_CENTER,
            n("dali.property.user.adminCenter"),
            n("dali.property.user.adminCenter"),
            String.class);

    /** Constant <code>SITE</code> */
    public static final DaliColumnIdentifier<UserRowModel> SITE = DaliColumnIdentifier.newId(
            UserRowModel.PROPERTY_SITE,
            n("dali.property.user.site"),
            n("dali.property.user.site"),
            String.class);

    public static final DaliColumnIdentifier<UserRowModel> COMMENT = DaliColumnIdentifier.newId(
        UserRowModel.PROPERTY_COMMENT,
        n("dali.property.comment"),
        n("dali.property.comment"),
        String.class,
        false);

    public static final DaliColumnIdentifier<UserRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        UserRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<UserRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        UserRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);


    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<UserRowModel> getFirstColumnEditing() {
        return LASTNAME;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<UserRowModel> propertyName) {

        boolean editable = super.isCellEditable(rowIndex, columnIndex, propertyName);

        if (editable) {
            if (propertyName == PRIVILEGES) {

                // local privileges can be empty BUT editable
                UserRowModel row = getEntry(rowIndex);
                editable = row.getStatus() == null
                        || !StatusFilter.ALL.toStatusCodes().contains(row.getStatus().getCode())
                        || row.sizePrivilege() > 0;

            } else if (propertyName == HAS_PASSWORD) {

                // password changes is allowed for admins and for the user itself
                UserRowModel row = getEntry(rowIndex);
                editable = (getTableUIModel().isAdmin() || getTableUIModel().isUserItself(row)) && StringUtils.isNotBlank(row.getIntranetLogin());
            }

        }

        return editable;

    }

    /** {@inheritDoc} */
    @Override
    public UserTableUIModel getTableUIModel() {
        return (UserTableUIModel) super.getTableUIModel();
    }
}
