/*
 * Copyright (c) 2005, 2006, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package fr.ifremer.dali.ui.swing.util.plaf;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javax.swing.JComponent;
import javax.swing.plaf.nimbus.AbstractRegionPainter;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

/**
 * Copy of javax.swing.plaf.nimbus.SpinnerPanelSpinnerFormattedTextFieldPainter with possibility of overriding background color
 */
public class SpinnerTextFieldPainter extends AbstractRegionPainter {
    //package private integers representing the available states that
    //this painter will paint. These are used when creating a new instance
    //of SpinnerPanelSpinnerFormattedTextFieldPainter to determine which region/state is being painted
    //by that instance.
    static final int BACKGROUND_DISABLED = 1;
    static final int BACKGROUND_ENABLED = 2;
    static final int BACKGROUND_FOCUSED = 3;
    static final int BACKGROUND_SELECTED = 4;
    static final int BACKGROUND_SELECTED_FOCUSED = 5;

    private int state; //refers to one of the static final ints above
    private PaintContext ctx;

    //the following 4 variables are reused during the painting code of the layers
    private Rectangle2D rect = new Rectangle2D.Float(0, 0, 0, 0);

    //All Colors used for painting are stored here. Ideally, only those colors being used
    //by a particular instance of SpinnerPanelSpinnerFormattedTextFieldPainter would be created. For the moment at least,
    //however, all are created for each instance.
    private Color color1 = decodeColor("nimbusBlueGrey", -0.6111111f, -0.110526316f, -0.74509805f, -237);
    private Color color2;
    private Color color3;
    private Color color4;
    private Color color5;
    private Color color6;
    private Color color7;
    private Color color8;
    private Color color9;
    private Color color10;
    private Color color11;
    private Color color12;
    private Color color13;
    private Color color14 = decodeColor("nimbusFocus", 0.0f, 0.0f, 0.0f, 0);
    private Color color15 = decodeColor("nimbusBlueGrey", 0.055555582f, -0.1048766f, -0.05098039f, 0);

    public SpinnerTextFieldPainter(Object ctx, int state, Color background) {
        super();
        this.state = state;
        this.ctx = (PaintContext) ctx;

        this.color2 = background;
        this.color3 = background;
        this.color4 = background;
        this.color5 = background;
        this.color6 = background;
        this.color7 = background;
        this.color8 = background;
        this.color9 = background;
        this.color10 = background;
        this.color11 = background;
        this.color12 = background;
        this.color13 = background;

    }

    @Override
    protected void doPaint(Graphics2D g, JComponent c, int width, int height, Object[] extendedCacheKeys) {
        //generate this entire method. Each state/bg/fg/border combo that has
        //been painted gets its own KEY and paint method.
        switch (state) {
            case BACKGROUND_DISABLED:
                paintBackgroundDisabled(g);
                break;
            case BACKGROUND_ENABLED:
                paintBackgroundEnabled(g);
                break;
            case BACKGROUND_FOCUSED:
                paintBackgroundFocused(g);
                break;
            case BACKGROUND_SELECTED:
                paintBackgroundSelected(g);
                break;
            case BACKGROUND_SELECTED_FOCUSED:
                paintBackgroundSelectedAndFocused(g);
                break;

        }
    }

    @Override
    protected final PaintContext getPaintContext() {
        return ctx;
    }

    private void paintBackgroundDisabled(Graphics2D g) {
        rect = decodeRect1();
        g.setPaint(color1);
        g.fill(rect);
        rect = decodeRect2();
        g.setPaint(decodeGradient1(rect));
        g.fill(rect);
        rect = decodeRect3();
        g.setPaint(decodeGradient2(rect));
        g.fill(rect);
        rect = decodeRect4();
        g.setPaint(color6);
        g.fill(rect);
        rect = decodeRect5();
        g.setPaint(color7);
        g.fill(rect);

    }

    private void paintBackgroundEnabled(Graphics2D g) {
        rect = decodeRect1();
        g.setPaint(color1);
        g.fill(rect);
        rect = decodeRect2();
        g.setPaint(decodeGradient3(rect));
        g.fill(rect);
        rect = decodeRect3();
        g.setPaint(decodeGradient4(rect));
        g.fill(rect);
        rect = decodeRect4();
        g.setPaint(color12);
        g.fill(rect);
        rect = decodeRect5();
        g.setPaint(color13);
        g.fill(rect);

    }

    private void paintBackgroundFocused(Graphics2D g) {
        rect = decodeRect6();
        g.setPaint(color14);
        g.fill(rect);
        rect = decodeRect2();
        g.setPaint(decodeGradient5(rect));
        g.fill(rect);
        rect = decodeRect3();
        g.setPaint(decodeGradient4(rect));
        g.fill(rect);
        rect = decodeRect4();
        g.setPaint(color12);
        g.fill(rect);
        rect = decodeRect5();
        g.setPaint(color13);
        g.fill(rect);

    }

    private void paintBackgroundSelected(Graphics2D g) {
        rect = decodeRect1();
        g.setPaint(color1);
        g.fill(rect);
        rect = decodeRect2();
        g.setPaint(decodeGradient3(rect));
        g.fill(rect);
        rect = decodeRect3();
        g.setPaint(decodeGradient4(rect));
        g.fill(rect);
        rect = decodeRect4();
        g.setPaint(color12);
        g.fill(rect);
        rect = decodeRect5();
        g.setPaint(color13);
        g.fill(rect);

    }

    private void paintBackgroundSelectedAndFocused(Graphics2D g) {
        rect = decodeRect6();
        g.setPaint(color14);
        g.fill(rect);
        rect = decodeRect2();
        g.setPaint(decodeGradient5(rect));
        g.fill(rect);
        rect = decodeRect3();
        g.setPaint(decodeGradient4(rect));
        g.fill(rect);
        rect = decodeRect4();
        g.setPaint(color12);
        g.fill(rect);
        rect = decodeRect5();
        g.setPaint(color13);
        g.fill(rect);

    }

    private Rectangle2D decodeRect1() {
        rect.setRect(decodeX(0.6666667f), //x
                decodeY(2.3333333f), //y
                decodeX(3.0f) - decodeX(0.6666667f), //width
                decodeY(2.6666667f) - decodeY(2.3333333f)); //height
        return rect;
    }

    private Rectangle2D decodeRect2() {
        rect.setRect(decodeX(0.6666667f), //x
                decodeY(0.4f), //y
                decodeX(3.0f) - decodeX(0.6666667f), //width
                decodeY(1.0f) - decodeY(0.4f)); //height
        return rect;
    }

    private Rectangle2D decodeRect3() {
        rect.setRect(decodeX(1.0f), //x
                decodeY(0.6f), //y
                decodeX(3.0f) - decodeX(1.0f), //width
                decodeY(1.0f) - decodeY(0.6f)); //height
        return rect;
    }

    private Rectangle2D decodeRect4() {
        rect.setRect(decodeX(0.6666667f), //x
                decodeY(1.0f), //y
                decodeX(3.0f) - decodeX(0.6666667f), //width
                decodeY(2.3333333f) - decodeY(1.0f)); //height
        return rect;
    }

    private Rectangle2D decodeRect5() {
        rect.setRect(decodeX(1.0f), //x
                decodeY(1.0f), //y
                decodeX(3.0f) - decodeX(1.0f), //width
                decodeY(2.0f) - decodeY(1.0f)); //height
        return rect;
    }

    private Rectangle2D decodeRect6() {
        rect.setRect(decodeX(0.22222222f), //x
                decodeY(0.13333334f), //y
                decodeX(2.916668f) - decodeX(0.22222222f), //width
                decodeY(2.75f) - decodeY(0.13333334f)); //height
        return rect;
    }

    private Paint decodeGradient1(Shape s) {
        Rectangle2D bounds = s.getBounds2D();
        float x = (float) bounds.getX();
        float y = (float) bounds.getY();
        float w = (float) bounds.getWidth();
        float h = (float) bounds.getHeight();
        return decodeGradient((0.5f * w) + x, (0.0f * h) + y, (0.5f * w) + x, (1.0f * h) + y,
                new float[]{0.0f, 0.5f, 1.0f},
                new Color[]{color2,
                        decodeColor(color2, color3, 0.5f),
                        color3});
    }

    private Paint decodeGradient2(Shape s) {
        Rectangle2D bounds = s.getBounds2D();
        float x = (float) bounds.getX();
        float y = (float) bounds.getY();
        float w = (float) bounds.getWidth();
        float h = (float) bounds.getHeight();
        return decodeGradient((0.5f * w) + x, (1.0f * h) + y, (0.5f * w) + x, (0.0f * h) + y,
                new float[]{0.0f, 0.5f, 1.0f},
                new Color[]{color4,
                        decodeColor(color4, color5, 0.5f),
                        color5});
    }

    private Paint decodeGradient3(Shape s) {
        Rectangle2D bounds = s.getBounds2D();
        float x = (float) bounds.getX();
        float y = (float) bounds.getY();
        float w = (float) bounds.getWidth();
        float h = (float) bounds.getHeight();
        return decodeGradient((0.5f * w) + x, (0.0f * h) + y, (0.5f * w) + x, (1.0f * h) + y,
                new float[]{0.0f, 0.49573863f, 0.99147725f},
                new Color[]{color8,
                        decodeColor(color8, color9, 0.5f),
                        color9});
    }

    private Paint decodeGradient4(Shape s) {
        Rectangle2D bounds = s.getBounds2D();
        float x = (float) bounds.getX();
        float y = (float) bounds.getY();
        float w = (float) bounds.getWidth();
        float h = (float) bounds.getHeight();
        return decodeGradient((0.5f * w) + x, (0.0f * h) + y, (0.5f * w) + x, (1.0f * h) + y,
                new float[]{0.0f, 0.1684492f, 1.0f},
                new Color[]{color10,
                        decodeColor(color10, color11, 0.5f),
                        color11});
    }

    private Paint decodeGradient5(Shape s) {
        Rectangle2D bounds = s.getBounds2D();
        float x = (float) bounds.getX();
        float y = (float) bounds.getY();
        float w = (float) bounds.getWidth();
        float h = (float) bounds.getHeight();
        return decodeGradient((0.5f * w) + x, (0.0f * h) + y, (0.5f * w) + x, (1.0f * h) + y,
                new float[]{0.0f, 0.49573863f, 0.99147725f},
                new Color[]{color8,
                        decodeColor(color8, color15, 0.5f),
                        color15});
    }

}
