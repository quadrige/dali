package fr.ifremer.dali.ui.swing.content.manage.referential.taxon.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;

import java.util.List;

/**
 * Controlleur du menu pour la gestion des taxons au niveau National
 */
public class TaxonMenuUIHandler extends ReferentialMenuUIHandler<TaxonMenuUIModel, TaxonMenuUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final TaxonMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final TaxonMenuUIModel model = new TaxonMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final TaxonMenuUI ui) {
        super.afterInit(ui);

        // Initialiser les combobox
        initComboBox();
    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getLevelCombo().setEnabled(enabled);
        getUI().getNameEditor().setEnabled(enabled);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllTaxonFilters();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        initBeanFilterableComboBox(
                getUI().getLevelCombo(),
                getContext().getReferentialService().getTaxonomicLevels(),
                null);

        DaliUIs.forceComponentSize(getUI().getLevelCombo());
    }
}
