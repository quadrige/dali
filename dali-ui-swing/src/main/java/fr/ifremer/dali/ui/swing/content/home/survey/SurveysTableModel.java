package fr.ifremer.dali.ui.swing.content.home.survey;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.StateDTO;
import fr.ifremer.dali.dto.SynchronizationStatusDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.dto.referential.PositioningSystemDTO;
import fr.ifremer.dali.dto.referential.QualityLevelDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.time.LocalDate;
import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>SurveysTableModel class.</p>
 *
 * @author Antoine
 */
public class SurveysTableModel extends AbstractDaliTableModel<SurveysTableRowModel> {

    /** Constant <code>SYNCHRONIZATION_STATUS</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> SYNCHRONIZATION_STATUS = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_SYNCHRONIZATION_STATUS,
            n("dali.property.synchronizationStatus"),
            n("dali.property.synchronizationStatus"),
            SynchronizationStatusDTO.class);

    /** Constant <code>STATE</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> STATE = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_STATE,
            n("dali.property.status"),
            n("dali.property.status"),
            StateDTO.class);

    /** Constant <code>LOCATION</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> LOCATION = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LOCATION,
            n("dali.property.location"),
            n("dali.property.location"),
            LocationDTO.class,
            true);

    /** Constant <code>DATE</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> DATE = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_DATE,
            n("dali.property.date"),
            n("dali.property.date"),
            LocalDate.class,
            true);

    /** Constant <code>BOTTOM_DEPTH</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> BOTTOM_DEPTH = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_BOTTOM_DEPTH,
            n("dali.property.depth.bottom"),
            n("dali.property.depth.bottom"),
            Double.class);

    /** Constant <code>PROGRAM</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> PROGRAM = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_PROGRAM,
            n("dali.property.program"),
            n("dali.property.program"),
            ProgramDTO.class,
            true);

	public static final DaliColumnIdentifier<SurveysTableRowModel> CAMPAIGN = DaliColumnIdentifier.newId(
			SurveysTableRowModel.PROPERTY_CAMPAIGN,
			n("dali.property.campaign"),
			n("dali.property.campaign"),
			CampaignDTO.class);

    /** Constant <code>COMMENT</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> COMMENT = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_COMMENT,
            n("dali.property.comment"),
            n("dali.property.comment"),
            String.class);

    /** Constant <code>LATITUDE_MIN</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> LATITUDE_MIN = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LATITUDE_MIN,
            n("dali.survey.coordinates.latitude.min"),
            n("dali.survey.coordinates.latitude.min"),
            Double.class);

    /** Constant <code>LONGITUDE_MIN</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> LONGITUDE_MIN = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LONGITUDE_MIN,
            n("dali.survey.coordinates.longitude.min"),
            n("dali.survey.coordinates.longitude.min"),
            Double.class);

    /** Constant <code>LATITUDE_MIN</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> LATITUDE_MAX = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LATITUDE_MAX,
            n("dali.survey.coordinates.latitude.max"),
            n("dali.survey.coordinates.latitude.max"),
            Double.class);

    /** Constant <code>LONGITUDE_MIN</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> LONGITUDE_MAX = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LONGITUDE_MAX,
            n("dali.survey.coordinates.longitude.max"),
            n("dali.survey.coordinates.longitude.max"),
            Double.class);

    /** Constant <code>POSITIONING</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> POSITIONING = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_POSITIONING,
            n("dali.property.positioning.name"),
            n("dali.property.positioning.name"),
            PositioningSystemDTO.class);

    /** Constant <code>POSITIONING_PRECISION</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> POSITIONING_PRECISION = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_POSITIONING_PRECISION,
            n("dali.property.positioning.precision"),
            n("dali.property.positioning.precision"),
            String.class);

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> NAME = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_NAME,
            n("dali.property.mnemonic"),
            n("dali.property.mnemonic"),
            String.class);

    /** Constant <code>RECORDER_DEPARTMENT</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> RECORDER_DEPARTMENT = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_RECORDER_DEPARTMENT,
            n("dali.property.department.recorder"),
            n("dali.property.department.recorder"),
            DepartmentDTO.class);

    /** Constant <code>TIME</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> TIME = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_TIME,
            n("dali.property.survey.time"),
            n("dali.property.survey.time"),
            Integer.class);

    /** Constant <code>UPDATE_DATE</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_UPDATE_DATE,
            n("dali.property.date.modification"),
            n("dali.property.date.modification"),
            Date.class);

    /** Constant <code>CONTROL_DATE</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> CONTROL_DATE = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_CONTROL_DATE,
            n("dali.property.date.control"),
            n("dali.property.date.control"),
            Date.class);

    /** Constant <code>VALIDATION_DATE</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> VALIDATION_DATE = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_VALIDATION_DATE,
            n("dali.property.date.validation"),
            n("dali.property.date.validation"),
            Date.class);

    /** Constant <code>VALIDATION_COMMENT</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> VALIDATION_COMMENT = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_VALIDATION_COMMENT,
            n("dali.property.comment.validation"),
            n("dali.property.comment.validation"),
            String.class);

    /** Constant <code>QUALIFICATION_DATE</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> QUALIFICATION_DATE = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_QUALIFICATION_DATE,
            n("dali.property.date.qualification"),
            n("dali.property.date.qualification"),
            Date.class);

    /** Constant <code>QUALIFICATION_COMMENT</code> */
    public static final DaliColumnIdentifier<SurveysTableRowModel> QUALIFICATION_COMMENT = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_QUALIFICATION_COMMENT,
            n("dali.property.comment.qualification"),
            n("dali.property.comment.qualification"),
            String.class);

    public static final DaliColumnIdentifier<SurveysTableRowModel> QUALITY_FLAG = DaliColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_QUALITY_LEVEL,
            n("dali.property.qualityFlag"),
            n("dali.property.qualityFlag"),
            QualityLevelDTO.class);

    /**
     * <p>Constructor for SurveysTableModel.</p>
     *
     */
    public SurveysTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, true, false);
    }

    /** {@inheritDoc} */
    @Override
    public SurveysTableRowModel createNewRow() {
        return new SurveysTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<SurveysTableRowModel> getFirstColumnEditing() {
        return PROGRAM;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<SurveysTableRowModel> propertyName) {

        // validation and qualification comments are always editable
        if (VALIDATION_COMMENT == propertyName || QUALIFICATION_COMMENT == propertyName) return true;

        boolean editable = true;
        // Positioning is allowed when coordinate is set
        if (POSITIONING == propertyName) {
            SurveysTableRowModel rowModel = getEntry(rowIndex);
            editable = rowModel.getLatitudeMin() != null || rowModel.getLongitudeMin() != null;
        }

        return editable && super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

    @Override
    public SurveysTableUIModel getTableUIModel() {
        return (SurveysTableUIModel) super.getTableUIModel();
    }

    @Override
    public String getStateContext() {

        if (getTableUIModel().getMainUIModel() != null) {
            // Use program code in filter to set state context (Mantis #52357)
            return getTableUIModel().getMainUIModel().getStateContext();
        }

        return super.getStateContext();
    }
}
