package fr.ifremer.dali.ui.swing.content.synchro.changes;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.system.synchronization.SynchroChangesDTO;
import fr.ifremer.dali.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.i18n.I18n;

import javax.swing.JScrollPane;
import javax.swing.SortOrder;

/**
 * Controleur pour la zone des programmes.
 */
public class SynchroChangesUIHandler extends AbstractDaliTableUIHandler<SynchroChangesRowModel, SynchroChangesUIModel, SynchroChangesUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SynchroChangesUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SynchroChangesUIModel model = new SynchroChangesUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(SynchroChangesUI ui) {

        // Init UI
        initUI(ui);

        // Init list
        initList();

        // Init table
        initTable();

        // load change log?
        //getModel().setBeans(programs);

        getTable().packAll();
    }

    @SuppressWarnings("unchecked")
    private void initList() {
        getUI().getEntityTypeList().setCellRenderer(newListCellRender(SynchroTableDTO.class, DecoratorService.WITH_COUNT));

        getModel().addPropertyChangeListener(SynchroChangesUIModel.PROPERTY_CHANGES, evt -> populateList());

        getUI().getEntityTypeList().addListSelectionListener(e -> {
            SynchroTableDTO selectedTable = getUI().getEntityTypeList().getSelectedValue();
            populateTable(selectedTable);
        });
    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // Get the main table
        final SwingTable table = getTable();

        // Operation type
        TableColumnExt operationTypeColumm = addColumn(
                null,
                new SynchroOperationTypeIconCellRenderer(getContext()),
                SynchroChangesTableModel.OPERATION_TYPE);
        operationTypeColumm.setSortable(true);
        operationTypeColumm.setMinWidth(30);
        operationTypeColumm.setMaxWidth(30);
        operationTypeColumm.setResizable(false);

        // Column name
        final TableColumnExt columnName = addColumn(
                SynchroChangesTableModel.NAME);
        columnName.setSortable(true);

        // Modele de la table
        final SynchroChangesTableModel tableModel = new SynchroChangesTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Les columns obligatoire sont toujours presentes
        columnName.setHideable(false);

        table.setEditable(false);

        // Initialisation de la table
        initTable(table);

        // Tri par defaut
        table.setSortOrder(SynchroChangesTableModel.NAME, SortOrder.ASCENDING);

        getUI().getTableParent().setVisible(false);
    }


    /**
     * <p>populateList.</p>
     */
    private void populateList() {
        // Set tables from model, to the JList
        SynchroChangesDTO synchroChanges = getModel().getChanges();
        if (synchroChanges != null && CollectionUtils.isNotEmpty(synchroChanges.getTables())) {
            SynchroTableDTO[] tables = synchroChanges.getTables().toArray(new SynchroTableDTO[synchroChanges.getTables().size()]);
            getUI().getEntityTypeList().setListData(tables);
        }
        else {
            getUI().getEntityTypeList().setListData(new SynchroTableDTO[0]);
        }

        getModel().setValid(false);
        //getUI().updateUI();
        getTableParent().setVisible(false);
    }

    /**
     * <p>populateTable.</p>
     *
     * @param synchroTable a {@link fr.ifremer.dali.dto.system.synchronization.SynchroTableDTO} object.
     */
    private void populateTable(SynchroTableDTO synchroTable) {

        TableColumnExt nameColumn = getTable().getColumnExt(SynchroChangesTableModel.NAME);

        if (synchroTable == null
                || CollectionUtils.isEmpty(synchroTable.getRows())) {
            getTableParent().setVisible(false);
            nameColumn.setTitle(I18n.t("dali.synchro.changes.name.short.default"));
        }
        else {

            getTableParent().setVisible(true);
            getUI().updateUI();
            nameColumn.setTitle(I18n.t("dali.synchro.changes.name.short", decorate(synchroTable)));
            getModel().setBeans(synchroTable.getRows());
        }
    }



    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<SynchroChangesRowModel> getTableModel() {
        return (SynchroChangesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getTable();
    }

    /**
     * <p>getTableParent.</p>
     *
     * @return a {@link javax.swing.JScrollPane} object.
     */
    private JScrollPane getTableParent() {
        return getUI().getTableParent();
    }

    /**
     * <p>cancel.</p>
     */
    public void cancel() {
        getModel().setChangesValidated(false);
        closeDialog();
    }

    /**
     * <p>confirm.</p>
     */
    public void confirm() {
        getModel().setChangesValidated(true);
        closeDialog();
    }

    /* -- Internal methods -- */
}
