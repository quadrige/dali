package fr.ifremer.dali.ui.swing.content.extraction.list;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Duplicate Extraction Action
 */
public class DuplicateExtractionAction extends AbstractDaliAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

    private ExtractionDTO duplicateExtraction;

    /**
     * Constructor.
     *
     * @param handler the handler
     */
    public DuplicateExtractionAction(ExtractionsTableUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction() || getModel().getSelectedRows().size() != 1) {
            return false;
        }

        return getContext().getDialogHelper().showConfirmDialog(
                getUI(),
                t("dali.action.duplicate.extraction.message"),
                t("dali.action.duplicate.extraction.title"),
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        // Selected observation
        final ExtractionsRowModel extractionsRowModel = getModel().getSelectedRows().iterator().next();
        if (extractionsRowModel != null) {

            // Duplicate observation
            duplicateExtraction = getContext().getExtractionService().duplicateExtraction(extractionsRowModel.toBean());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getModel().setModify(true);

        // Add focus on duplicate row
        getHandler().setFocusOnCell(getModel().addNewRow(duplicateExtraction));
    }
}
