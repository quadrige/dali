package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.multiedit;

import com.google.common.collect.ImmutableSet;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared.AbstractOperationMeasurementsGroupedTableUIModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OperationMeasurementsMultiEditUIModel
    extends AbstractOperationMeasurementsGroupedTableUIModel<MeasurementDTO, OperationMeasurementsGroupedRowModel, OperationMeasurementsMultiEditUIModel> {

    // rows selected by caller
    Set<OperationMeasurementsGroupedRowModel> rowsToEdit;

    // pmfm ids as read-only
    List<Integer> readOnlyPmfmIds;

    public Set<OperationMeasurementsGroupedRowModel> getRowsToEdit() {
        if (rowsToEdit == null)
            rowsToEdit = new HashSet<>();
        return rowsToEdit;
    }

    public void setRowsToEdit(Set<OperationMeasurementsGroupedRowModel> rowsToEdit) {
        this.rowsToEdit = rowsToEdit;
    }

    public List<Integer> getReadOnlyPmfmIds() {
        if (readOnlyPmfmIds == null)
            readOnlyPmfmIds = new ArrayList<>();
        return readOnlyPmfmIds;
    }

    public void setReadOnlyPmfmIds(List<Integer> readOnlyPmfmIds) {
        this.readOnlyPmfmIds = readOnlyPmfmIds;
    }

    public Set<DaliColumnIdentifier<OperationMeasurementsGroupedRowModel>> getIdentifiersToCheck() {
        return ImmutableSet.of(
            OperationMeasurementsGroupedTableModel.SAMPLING,
            OperationMeasurementsGroupedTableModel.TAXON_GROUP,
            OperationMeasurementsGroupedTableModel.TAXON,
            OperationMeasurementsGroupedTableModel.INPUT_TAXON_NAME,
            OperationMeasurementsGroupedTableModel.ANALYST,
            OperationMeasurementsGroupedTableModel.ANALYSIS_INSTRUMENT
        );
    }
}
