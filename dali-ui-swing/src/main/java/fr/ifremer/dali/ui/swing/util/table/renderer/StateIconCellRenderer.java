package fr.ifremer.dali.ui.swing.util.table.renderer;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.StateDTO;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.quadrige3.ui.swing.table.renderer.IconCellRenderer;

import javax.swing.Icon;

/**
 * Renderer pour la gestion des icons pour la partage.
 */
public class StateIconCellRenderer extends IconCellRenderer<StateDTO> {
	
	/**
	 * Le contexte.
	 */
	private DaliUIContext context;
	
	/**
	 * Constructor.
	 *
	 * @param context a {@link DaliUIContext} object.
	 */
	public StateIconCellRenderer(final DaliUIContext context) {
		setContext(context);
	}

    /** {@inheritDoc} */
    @Override
    protected Icon getIcon(StateDTO object) {
        if (object == null) {
            return null;
        }
        return getContext().getObjectStatusIcon(object.getIconName(), /*object.getIconName()*/ null);
    }
    
    /** {@inheritDoc} */
    @Override
    protected String getToolTipText(StateDTO object) {
        if (object == null) {
            return null;
        }
        return object.getName();
    }

	/** {@inheritDoc} */
	@Override
	protected String getText(StateDTO object) {
      if (object == null) {
            return null;
        }
		return object.getName();
	}

	private DaliUIContext getContext() {
		return context;
	}

	private void setContext(DaliUIContext context) {
		this.context = context;
	}
}
