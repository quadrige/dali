package fr.ifremer.dali.ui.swing.content.manage.rule.controlrule.precondition;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePreconditionTableModel extends AbstractDaliTableModel<RulePreconditionRowModel> {

    public static final DaliColumnIdentifier<RulePreconditionRowModel> NAME = DaliColumnIdentifier.newId(
            QualitativeValueDTO.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class
    );

    /**
     * <p>Constructor for AbstractDaliTableModel.</p>
     *
     * @param columnModel column model
     */
    public RulePreconditionTableModel(SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    @Override
    public DaliColumnIdentifier<RulePreconditionRowModel> getFirstColumnEditing() {
        return NAME;
    }

    @Override
    public RulePreconditionRowModel createNewRow() {
        return new RulePreconditionRowModel();
    }
}
