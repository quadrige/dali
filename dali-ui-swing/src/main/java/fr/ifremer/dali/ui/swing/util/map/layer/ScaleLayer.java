package fr.ifremer.dali.ui.swing.util.map.layer;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.DirectLayer;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;
import org.geotools.renderer.lite.RendererUtilities;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * A layer drawing the scale on map in miles ans meters
 *
 * @author peck7 on 13/06/2017.
 */
public class ScaleLayer extends DirectLayer {

    private static final Log log = LogFactory.getLog(ScaleLayer.class);

    private static final int MARGIN = 10;
    private static final int SCALE_HEIGHT = 10;
    private static final int SCALE_WIDTH_MAX = 200;

    private static final int METERS_BY_MILES = 1852;

    private int mileScaleWidth;
    private int meterScaleWidth;

    private String mileLabelScaleUp;
    private String meterLabelScaleUp;

    public ScaleLayer() {
        setTitle("scaleLayer");
    }

    @Override
    public void draw(Graphics2D graphics, MapContent map, MapViewport viewport) {
        if (viewport == null) {
            viewport = map.getViewport(); // use the map viewport if one has not been provided
        }
        if (viewport == null || viewport.getScreenArea() == null) {
            return; // renderer is not set up for use yet
        }

        int viewWidth = graphics.getClipBounds().width;
        int viewHeight = graphics.getClipBounds().height;

        ReferencedEnvelope displayArea = viewport.getBounds();
        // Round the envelope to prevent lat/long out of range (ex lat=90.00000000000001)
        ReferencedEnvelope roundDisplayArea = new ReferencedEnvelope(
                DaliBeans.roundDouble(displayArea.getMinX(), 4),
                DaliBeans.roundDouble(displayArea.getMaxX(), 4),
                DaliBeans.roundDouble(displayArea.getMinY(), 4),
                DaliBeans.roundDouble(displayArea.getMaxY(), 4),
                displayArea.getCoordinateReferenceSystem());

        double dpi = 2.54 / 100; // pour avoir l'echélle en metre/pixel

        try {
            double meterPerPixel = RendererUtilities.calculateScale(roundDisplayArea, viewWidth, viewHeight, dpi);
            double maxWidthMeter = SCALE_WIDTH_MAX * meterPerPixel;
            {
                // Mile scale
                double maxWidthMiles = maxWidthMeter / METERS_BY_MILES;
                int nbDigit = (int) Math.floor(Math.log10(maxWidthMiles));
                int firstDigit = (int) Math.floor(maxWidthMiles / Math.pow(10, nbDigit));  // le premier chiffre significatif
                int useFirstDigit;
                if (firstDigit >= 5) {
                    useFirstDigit = 5;
                } else if (firstDigit >= 2) {
                    useFirstDigit = 2;
                } else {
                    useFirstDigit = 1;
                }
                long scaleInMiles = useFirstDigit * (long) Math.pow(10, nbDigit);
                mileScaleWidth = (int) Math.round(scaleInMiles * METERS_BY_MILES / meterPerPixel);
                mileLabelScaleUp = String.format("%,d " + "milles", scaleInMiles);
            }
            {
                // Meter scale
                int nbDigit = (int) Math.floor(Math.log10(maxWidthMeter));
                int firstDigit = (int) Math.floor(maxWidthMeter / Math.pow(10, nbDigit));  // le premier chiffre significatif
                int useFirstDigit;
                if (firstDigit >= 5) {
                    useFirstDigit = 5;
                } else if (firstDigit >= 2) {
                    useFirstDigit = 2;
                } else {
                    useFirstDigit = 1;
                }
                long scaleInMeter = useFirstDigit * (long) Math.pow(10, nbDigit);
                meterScaleWidth = (int) Math.round(scaleInMeter / meterPerPixel);
                meterLabelScaleUp = String.format("%,d " + "meters", scaleInMeter);
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("error", e);
            }
        }

        graphics.setColor(Color.BLACK);
        graphics.setStroke(new BasicStroke());
        FontMetrics fm = graphics.getFontMetrics();

        {
            // Draw Mile scale
            Rectangle2D textArea = fm.getStringBounds(mileLabelScaleUp, graphics);
            int labelLeft = viewWidth - MARGIN * 2 - mileScaleWidth - (int) textArea.getWidth();
            graphics.drawString(mileLabelScaleUp, labelLeft, viewHeight - MARGIN);
            int scalesEndX = viewWidth - MARGIN;
            int scaleStartX = scalesEndX - mileScaleWidth;
            int scalesEndY = viewHeight - MARGIN;
            int scaleStartY = scalesEndY - SCALE_HEIGHT;

            graphics.drawLine(scaleStartX, scaleStartY, scaleStartX, scalesEndY);
            graphics.drawLine(scaleStartX, scalesEndY, scalesEndX, scalesEndY);
            graphics.drawLine(scalesEndX, scalesEndY, scalesEndX, scaleStartY);
        }

        {
            // Draw Meter scale
            Rectangle2D textArea = fm.getStringBounds(meterLabelScaleUp, graphics);
            int offsetHeight = (int) (textArea.getHeight());
            int labelLeft = viewWidth - MARGIN * 2 - meterScaleWidth - (int) textArea.getWidth();
            graphics.drawString(meterLabelScaleUp, labelLeft, viewHeight - MARGIN - offsetHeight);
            int scalesEndX = viewWidth - MARGIN;
            int scaleStartX = scalesEndX - meterScaleWidth;
            int scalesEndY = viewHeight - MARGIN - offsetHeight;
            int scaleStartY = scalesEndY - SCALE_HEIGHT;

            graphics.drawLine(scaleStartX, scaleStartY, scaleStartX, scalesEndY);
            graphics.drawLine(scaleStartX, scalesEndY, scalesEndX, scalesEndY);
            graphics.drawLine(scalesEndX, scalesEndY, scalesEndX, scaleStartY);
        }


    }

    @Override
    public ReferencedEnvelope getBounds() {
        return null;
    }
}
