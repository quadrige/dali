package fr.ifremer.dali.ui.swing.content.manage.referential.taxongroup.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;

import java.util.List;

/**
 * Action permettant la recherche de groupeTaxons au niveau national
 */
public class SearchAction extends AbstractDaliAction<TaxonGroupMenuUIModel, TaxonGroupMenuUI, TaxonGroupMenuUIHandler> {

    private List<TaxonGroupDTO> result;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final TaxonGroupMenuUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        result = getContext().getReferentialService().searchTaxonGroups(getModel());
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().setResults(result);

        super.postSuccessAction();
    }

}
