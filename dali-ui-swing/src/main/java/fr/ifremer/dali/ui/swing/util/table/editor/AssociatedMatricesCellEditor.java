package fr.ifremer.dali.ui.swing.util.table.editor;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.associatedMatrices.AssociatedMatricesUI;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ButtonCellEditor;

import java.awt.Dimension;

/**
 * <p>AssociatedMatricesCellEditor class.</p>
 *
 */
public class AssociatedMatricesCellEditor extends ButtonCellEditor {

    private final SwingTable table;

    private final DaliUI parentUI;

    /**
     * <p>Constructor for AssociatedMatricesCellEditor.</p>
     *
     * @param table a {@link SwingTable} object.
     * @param parentUI a {@link DaliUI} object.
     */
    public AssociatedMatricesCellEditor(
            final SwingTable table,
            final DaliUI parentUI) {
        this.table = table;
        this.parentUI = parentUI;
    }

    /** {@inheritDoc} */
    @Override
    public void onButtonCellAction(final int rowIndex, final int column) {

        final AbstractDaliTableModel<?> tableModel = (AbstractDaliTableModel<?>) table.getModel();

        final int rowModelIndex = table.convertRowIndexToModel(rowIndex);
        final FractionDTO rowModel = (FractionDTO) tableModel.getEntry(rowModelIndex);

        // open associated matrices screen
        final AssociatedMatricesUI associatedMatricesUI = new AssociatedMatricesUI((DaliUIContext) parentUI.getHandler().getContext());
        associatedMatricesUI.getModel().setFraction(rowModel);
        parentUI.getHandler().openDialog(associatedMatricesUI, new Dimension(640, 480));
    }
}
