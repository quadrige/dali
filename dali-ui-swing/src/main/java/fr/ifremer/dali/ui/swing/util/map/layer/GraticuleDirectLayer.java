package fr.ifremer.dali.ui.swing.util.map.layer;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.map.MapProjection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.DirectLayer;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Layer drawing the graticule
 *
 * @author peck7 on 09/06/2017.
 */
public class GraticuleDirectLayer extends DirectLayer implements GraticuleLayer {

    private static Log log = LogFactory.getLog(GraticuleDirectLayer.class);

    private static XWilkinson xWilkinson = XWilkinson.base10();

    private final DecimalFormat df;

    public GraticuleDirectLayer() {
        setTitle("graticuleDirectLayer");
        df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.HALF_EVEN);
    }

    /**
     * Draw layer contents onto screen
     *
     * @param map      Map being drawn; check map bounds and crs
     * @param graphics Graphics to draw into
     * @param viewport Area to draw the map into; including screen area
     */
    @Override
    public void draw(Graphics2D graphics, MapContent map, MapViewport viewport) {
        if (viewport == null) {
            viewport = map.getViewport(); // use the map viewport if one has not been provided
        }
        if (viewport == null || viewport.getScreenArea() == null) {
            return; // renderer is not set up for use yet
        }
        try {

            // transform to WGS84 if needed
            ReferencedEnvelope envelope = viewport.getBounds();
            CoordinateReferenceSystem crs = envelope.getCoordinateReferenceSystem();
            if (!CRS.equalsIgnoreMetadata(crs, DefaultGeographicCRS.WGS84)) {
                envelope = envelope.transform(DefaultGeographicCRS.WGS84, true);
            }
            // restrict to overall WGS84 envelope
            envelope = envelope.intersection(MapProjection.WGS84.getEnvelope());

            double aspectRatio = envelope.getWidth() / envelope.getHeight();

            int nbXLines = 10;
            int nbYLines = Math.max((int) ((nbXLines / aspectRatio) + 0.5), 2);
            XWilkinson.Label latitudeLabel = xWilkinson.search(envelope.getMinX(), envelope.getMaxX(), nbXLines);
            XWilkinson.Label longitudeLabel = xWilkinson.search(envelope.getMinY(), envelope.getMaxY(), nbYLines);
            if (longitudeLabel.getScore() < 0.5)
                longitudeLabel = xWilkinson.search(envelope.getMinY(), envelope.getMaxY(), nbYLines + 1);

            if (log.isDebugEnabled()) {
                log.debug("aspect ratio = " + aspectRatio + " nbXLines = " + nbXLines + " nbYLines = " + nbYLines);
                log.debug("latitudes  = " + latitudeLabel);
                log.debug("longitudes = " + longitudeLabel);
            }

            // latitude points
            List<Point2D> pointList = new ArrayList<>();
            List<String> xPointNames = new ArrayList<>();
            for (Double latitude : latitudeLabel.getList()) {
                // include only inbound lines
                if (latitude > envelope.getMinX() && latitude < envelope.getMaxX()) {
                    xPointNames.add(df.format(latitude));
                    ReferencedEnvelope pointEnv = new ReferencedEnvelope(latitude, latitude, envelope.getMinY(), envelope.getMaxY(), DefaultGeographicCRS.WGS84);
                    if (!CRS.equalsIgnoreMetadata(crs, DefaultGeographicCRS.WGS84)) {
                        pointEnv = pointEnv.transform(crs, true);
                    }

                    pointList.add(new Point2D.Double(pointEnv.getMaxX(), pointEnv.getMaxY()));
                    pointList.add(new Point2D.Double(pointEnv.getMinX(), pointEnv.getMinY()));
                }
            }

            int xPointsCount = pointList.size();
            Point2D[] latitudePoints = pointList.toArray(new Point2D[xPointsCount]);
            Point2D[] xPoints = new Point2D[xPointsCount];
            viewport.getWorldToScreen().transform(latitudePoints, 0, xPoints, 0, xPointsCount);

            // longitude points
            pointList.clear();
            List<String> yPointNames = new ArrayList<>();
            for (Double longitude : longitudeLabel.getList()) {
                // include only inbound lines
                if (longitude >= envelope.getMinY() && longitude <= envelope.getMaxY()) {
                    yPointNames.add(df.format(longitude));
                    ReferencedEnvelope pointEnv = new ReferencedEnvelope(envelope.getMinX(), envelope.getMaxX(), longitude, longitude, DefaultGeographicCRS.WGS84);
                    if (!CRS.equalsIgnoreMetadata(crs, DefaultGeographicCRS.WGS84))
                        pointEnv = pointEnv.transform(crs, true);

                    pointList.add(new Point2D.Double(pointEnv.getMinX(), pointEnv.getMinY()));
                    pointList.add(new Point2D.Double(pointEnv.getMaxX(), pointEnv.getMaxY()));
                }
            }

            int yPointsCount = pointList.size();
            Point2D[] longitudePoints = pointList.toArray(new Point2D[yPointsCount]);
            Point2D[] yPoints = new Point2D[yPointsCount];
            viewport.getWorldToScreen().transform(longitudePoints, 0, yPoints, 0, yPointsCount);

            // draw
            Stroke oldStroke = graphics.getStroke();
            graphics.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
            graphics.setColor(Color.BLUE.brighter().brighter());

            // latitude (x) lines
            for (int i = 0; i <= xPointsCount - 2; i += 2) {
                graphics.draw(new Line2D.Double(xPoints[i], xPoints[i + 1]));
            }
            // longitude (y) lines
            for (int i = 0; i <= yPointsCount - 2; i += 2) {
                graphics.draw(new Line2D.Double(yPoints[i], yPoints[i + 1]));
            }

            // latitude (x) text
            for (int i = 0; i < xPointNames.size(); i++) {
                String text = xPointNames.get(i);
                FontMetrics fm = graphics.getFontMetrics();
                Rectangle2D textRect = fm.getStringBounds(text, graphics);
                int textX = (int) ((int) xPoints[i * 2].getX() - (textRect.getWidth() / 2));
                int textY = (int) xPoints[i * 2].getY();

                graphics.setColor(Color.WHITE);
                graphics.fillRect(textX, textY, (int) textRect.getWidth() + 2, (int) textRect.getHeight() + 1);
                graphics.setColor(Color.BLACK);
                graphics.drawString(text, textX + 1, textY + 13);
            }
            // longitude (y) text
            for (int i = 0; i < yPointNames.size(); i++) {
                String text = yPointNames.get(i);
                FontMetrics fm = graphics.getFontMetrics();
                Rectangle2D textRect = fm.getStringBounds(text, graphics);
                int textX = (int) yPoints[i * 2].getX() + 1;
                int textY = (int) ((int) yPoints[i * 2].getY() - (textRect.getHeight() / 2));

                graphics.setColor(Color.WHITE);
                graphics.fillRect(textX, textY, (int) textRect.getWidth() + 2, (int) textRect.getHeight() + 1);
                graphics.setColor(Color.BLACK);
                graphics.drawString(text, textX + 1, textY + 13);
            }

            graphics.setStroke(oldStroke);

        } catch (TransformException | FactoryException e) {
            log.warn(e.getMessage(), e);
        }
    }

    /**
     * Does not contribute a bounding box to the map.
     *
     * @return null
     */
    @Override
    public ReferencedEnvelope getBounds() {
        return null;
    }

    public static class XWilkinson {

        private XWilkinson(double[] Q, double base, double[] w, double eps) {
            this.w = w;
            this.Q = Q;
            this.base = base;
            this.eps = eps;
        }

        private XWilkinson(double[] Q, double base) {
            this(Q, base, new double[]{0.25, 0.2, 0.5, 0.05}, 1e-10);
        }

        public static XWilkinson of(double[] Q, double base) {
            return new XWilkinson(Q, base);
        }

        public static XWilkinson base10() {
            return XWilkinson.of(new double[]{1, 5, 2, 2.5, 4, 3}, 10);
        }

        public static XWilkinson base2() {
            return XWilkinson.of(new double[]{1}, 2);
        }

        public static XWilkinson base16() {
            return XWilkinson.of(new double[]{1, 2, 4, 8}, 16);
        }

        //- Factory methods that may be useful for time-axis implementations
        public static XWilkinson forSeconds() {
            return XWilkinson.of(new double[]{1, 2, 3, 5, 10, 15, 20, 30}, 60);
        }

        public static XWilkinson forMinutes() {
            return XWilkinson.of(new double[]{1, 2, 3, 5, 10, 15, 20, 30}, 60);
        }

        public static XWilkinson forHours24() {
            return XWilkinson.of(new double[]{1, 2, 3, 4, 6, 8, 12}, 24);
        }

        public static XWilkinson forHours12() {
            return XWilkinson.of(new double[]{1, 2, 3, 4, 6}, 12);
        }

        public static XWilkinson forDays() {
            return XWilkinson.of(new double[]{1, 2}, 7);
        }

        public static XWilkinson forWeeks() {
            return XWilkinson.of(new double[]{1, 2, 4, 13, 26}, 52);
        }

        public static XWilkinson forMonths() {
            return XWilkinson.of(new double[]{1, 2, 3, 4, 6}, 12);
        }

        public static XWilkinson forYears() {
            return XWilkinson.of(new double[]{1, 2, 5}, 10);
        }

        // Loose flag
        public boolean loose = false;

        // scale-goodness weights for simplicity, coverage, density, legibility
        final private double w[];

        // calculation of scale-goodness
        private double w(double s, double c, double d, double l) {
            return w[0] * s + w[1] * c + w[2] * d + w[3] * l;
        }

        // Initial step sizes which we use as seed of generator
        final private double[] Q;

        // Number base used to calculate logarithms
        final private double base;

        private double logB(double a) {
            return Math.log(a) / Math.log(base);
        }

        /*
         * a mod b for float numbers (reminder of a/b)
         */
        private double flooredMod(double a, double n) {
            return a - n * Math.floor(a / n);
        }

        // can be injected via c'tor depending on your application, default is 1e-10
        final private double eps;

        private double v(double min, double max, double step) {
            return (flooredMod(min, step) < eps && min <= 0 && max >= 0) ? 1 : 0;
        }

        private double simplicity(int i, int j, double min, double max, double step) {
            if (Q.length > 1) {
                return 1 - (double) i / (Q.length - 1) - j + v(min, max, step);
            } else {
                return 1 - j + v(min, max, step);
            }
        }

        private double simplicity_max(int i, int j) {
            if (Q.length > 1) {
                return 1 - (double) i / (Q.length - 1) - j + 1.0;
            } else {
                return 1 - j + 1.0;
            }
        }

        private double coverage(double dmin, double dmax, double lmin, double lmax) {
            double a = dmax - lmax;
            double b = dmin - lmin;
            double c = 0.1 * (dmax - dmin);
            return 1 - 0.5 * ((a * a + b * b) / (c * c));
        }

        private double coverage_max(double dmin, double dmax, double span) {
            double range = dmax - dmin;
            if (span > range) {
                double half = (span - range) / 2;
                double r = 0.1 * range;
                return 1 - half * half / (r * r);
            } else {
                return 1.0;
            }
        }

        /**
         * @param k    number of labels
         * @param m    number of desired labels
         * @param dmin data range minimum
         * @param dmax data range maximum
         * @param lmin label range minimum
         * @param lmax label range maximum
         * @return density k-1 number of intervals between labels
         * m-1 number of intervals between desired number of labels
         * r   label interval length/label range
         * rt  desired label interval length/actual range
         */
        private double density(int k, int m, double dmin, double dmax, double lmin, double lmax) {
            double r = (k - 1) / (lmax - lmin);
            double rt = (m - 1) / (Math.max(lmax, dmax) - Math.min(lmin, dmin));
            return 2 - Math.max(r / rt, rt / r);   // return 1-Math.max(r/rt, rt/r); (paper is wrong)
        }

        private double density_max(int k, int m) {
            if (k >= m) {
                return 2 - (double)(k - 1) / (m - 1);        // return 2-(k-1)/(m-1); (paper is wrong)
            } else {
                return 1;
            }
        }

        private double legibility(double min, double max, double step) {
            return 1; // Maybe later more...
        }

        public class Label implements Iterable<Double> {

            private double min, max, step, score;

            @Override
            public String toString() {
                DecimalFormat df = new DecimalFormat("00.00");
                StringBuilder s = new StringBuilder("(Score: " + df.format(score) + ") ");
                for (double x = min; x <= max; x = x + step) {
                    s.append(df.format(x)).append("\t");
                }
                return s.toString();
            }

            @Override
            public Iterator<Double> iterator() {
                return getList().iterator();
            }

            public List<Double> getList() {
                List<Double> list = new ArrayList<>();
                for (double i = min; i <= max; i += step) {
                    list.add(i);
                }
                return list;
            }

            public double getMin() {
                return min;
            }

            public double getMax() {
                return max;
            }

            public double getStep() {
                return step;
            }

            public double getScore() {
                return score;
            }

        }

        /**
         * @param dmin data range min
         * @param dmax data range max
         * @param m    desired number of labels
         * @return XWilkinson.Label
         */
        public Label search(double dmin, double dmax, int m) {
            Label best = new Label();
            double bestScore = -2;
            double sm, dm, cm, delta;
            int j = 1;

            main_loop:
            while (j < Integer.MAX_VALUE) {
                for (int _i = 0; _i < Q.length; _i++) {
                    int i = _i + 1;
                    double q = Q[_i];
                    sm = simplicity_max(i, j);
                    if (w(sm, 1, 1, 1) < bestScore) {
                        break main_loop;
                    }
                    int k = 2;
                    while (k < Integer.MAX_VALUE) {
                        dm = density_max(k, m);
                        if (w(sm, 1, dm, 1) < bestScore) {
                            break;
                        }
                        delta = (dmax - dmin) / (k + 1) / (j * q);
                        int z = (int) Math.ceil(logB(delta));
                        while (z < Integer.MAX_VALUE) {
                            double step = j * q * Math.pow(base, z);
                            cm = coverage_max(dmin, dmax, step * (k - 1));
                            if (w(sm, cm, dm, 1) < bestScore) {
                                break;
                            }
                            int min_start = (int) (Math.floor(dmax / step - (k - 1)) * j);
                            int max_start = (int) (Math.ceil(dmin / step)) * j;

                            for (int start = min_start; start <= max_start; start++) {
                                double lmin = start * step / j;
                                double lmax = lmin + step * (k - 1);
                                double c = coverage(dmin, dmax, lmin, lmax);
                                double s = simplicity(i, j, lmin, lmax, step);
                                double d = density(k, m, dmin, dmax, lmin, lmax);
                                double l = legibility(lmin, lmax, step);
                                double score = w(s, c, d, l);

                                // later legibility logic can be implemented here

                                if (score > bestScore && (!loose || (lmin <= dmin && lmax >= dmax))) {
                                    best.min = lmin;
                                    best.max = lmax;
                                    best.step = step;
                                    best.score = score;
                                    bestScore = score;
                                }
                            }
                            z = z + 1;
                        }
                        k = k + 1;
                    }
                }
                j = j + 1;
            }
            return best;
        }

    }

}
