package fr.ifremer.dali.ui.swing.content.manage.referential.location.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.HarbourDTO;
import fr.ifremer.dali.dto.referential.PositioningSystemDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>LocationTableModel class.</p>
 *
 * @author Antoine
 */
public class LocationTableModel extends AbstractDaliTableModel<LocationTableRowModel> {

    private final boolean readOnly;

    /** Constant <code>ID</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> ID = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_ID,
            n("dali.property.id"),
            n("dali.property.id.tip"),
            Integer.class);

    /** Constant <code>LABEL</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> LABEL = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_LABEL,
            n("dali.property.label"),
            n("dali.property.label"),
            String.class);

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> NAME = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class,
            true);

    /** Constant <code>BATHYMETRIE</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> BATHYMETRIE = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_BATHYMETRY,
            n("dali.property.location.bathymetry"),
            n("dali.property.location.bathymetry"),
            Double.class);

    /** Constant <code>LATITUDE_MIN</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> LATITUDE_MIN = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MIN_LATITUDE,
            n("dali.property.location.latitude.min"),
            n("dali.property.location.latitude.min"),
            Double.class,
            true);

    /** Constant <code>LONGITUDE_MIN</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> LONGITUDE_MIN = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MIN_LONGITUDE,
            n("dali.property.location.longitude.min"),
            n("dali.property.location.longitude.min"),
            Double.class,
            true);

    /** Constant <code>COMMENT</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> COMMENT = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_COMMENT,
            n("dali.property.comment"),
            n("dali.property.comment"),
            String.class);

    /** Constant <code>HARBOUR</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> HARBOUR = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_HARBOUR,
            n("dali.property.location.harbour"),
            n("dali.property.location.harbour"),
            HarbourDTO.class);

    /** Constant <code>DELTA_UT_HIVER</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> DELTA_UT_HIVER = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_UT_FORMAT,
            n("dali.property.location.deltaUT"),
            n("dali.property.location.deltaUT"),
            Double.class);

    /** Constant <code>DAYLIGHT_SAVING_TIME</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> DAYLIGHT_SAVING_TIME = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_DAY_LIGHT_SAVING_TIME,
            n("dali.property.location.daylightSavingTime"),
            n("dali.property.location.daylightSavingTime"),
            Boolean.class);

    /** Constant <code>LATITUDE_MAX</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> LATITUDE_MAX = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MAX_LATITUDE,
            n("dali.property.location.latitude.max"),
            n("dali.property.location.latitude.max"),
            Double.class);

    /** Constant <code>LONGITUDE_MAX</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> LONGITUDE_MAX = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MAX_LONGITUDE,
            n("dali.property.location.longitude.max"),
            n("dali.property.location.longitude.max"),
            Double.class);

    /** Constant <code>POSITIONING_NAME</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> POSITIONING_NAME = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_POSITIONING,
            n("dali.property.location.positioning.name"),
            n("dali.property.location.positioning.name"),
            PositioningSystemDTO.class,
            true);

    /** Constant <code>POSITIONING_PRECISION</code> */
    public static final DaliColumnIdentifier<LocationTableRowModel> POSITIONING_PRECISION = DaliColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_POSITIONING_PRECISION,
            n("dali.property.location.positioning.precision"),
            n("dali.property.location.positioning.precision"),
            String.class,
            true);

//	public static final DaliColumnIdentifier<GererLieuxNationalTableUIRowModel> POSITIONING_PRECISION = DaliColumnIdentifier.newId(
//			LocationTableRowModel.PROPERTY_POSITIONING,
//			n("dali.ui.swing.content.config.gerer.lieux.national.table.positionnementPrecision.short"),
//			n("dali.ui.swing.content.config.gerer.lieux.national.table.positionnementPrecision.tip"),
//			PositioningSystemDTO.class, DecoratorService.PRECISION);

//	public static final DaliColumnIdentifier<GererLieuxNationalTableUIRowModel> FICHE_LIEU = DaliColumnIdentifier.newId(
//			LocationTableRowModel.PROPERTY_FICHE_LIEU,
//			n("dali.ui.swing.content.config.gerer.lieux.national.table.ficheLieu.short"),
//			n("dali.ui.swing.content.config.gerer.lieux.national.table.ficheLieu.tip"),
//			String.class);

    public static final DaliColumnIdentifier<LocationTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        LocationTableRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<LocationTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        LocationTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);

    public static final DaliColumnIdentifier<LocationTableRowModel> STATUS = DaliColumnIdentifier.newId(
        LocationTableRowModel.PROPERTY_STATUS,
        n("dali.property.status"),
        n("dali.property.status"),
        StatusDTO.class, true);

    /**
     * <p>Constructor for LocationTableModel.</p>
     *
     * @param createNewRowAllowed a boolean.
     */
    public LocationTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
        this.readOnly = !createNewRowAllowed;
    }

    /** {@inheritDoc} */
    @Override
    public LocationTableRowModel createNewRow() {
        return new LocationTableRowModel(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<LocationTableRowModel> getFirstColumnEditing() {
        return LABEL;
    }
}
