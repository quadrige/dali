package fr.ifremer.dali.ui.swing.util.table.renderer;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.quadrige3.ui.swing.table.renderer.IconCellRenderer;
import fr.ifremer.dali.dto.SynchronizationStatusDTO;

import javax.swing.Icon;

import static org.nuiton.i18n.I18n.t;

/**
 * Renderer pour la gestion des icons pour la partage.
 */
public class SynchronizationStatusIconCellRenderer extends IconCellRenderer<SynchronizationStatusDTO> {
	
	/**
	 * Le contexte.
	 */
	private DaliUIContext context;
	
	/**
	 * Constructor.
	 *
	 * @param context a {@link DaliUIContext} object.
	 */
	public SynchronizationStatusIconCellRenderer(final DaliUIContext context) {
		setContext(context);
	}

    /** {@inheritDoc} */
    @Override
    protected Icon getIcon(SynchronizationStatusDTO object) {
        if (object == null) {
            return null;
        }
        return getContext().getObjectStatusIcon(object.getIconName(), null);
    }
    
    /** {@inheritDoc} */
    @Override
    protected String getToolTipText(SynchronizationStatusDTO object) {
        if (object == null) {
            return null;
        }
        return t("dali.property.status." + object.getIconName());
    }

	/** {@inheritDoc} */
	@Override
	protected String getText(SynchronizationStatusDTO object) {
      if (object == null) {
            return null;
        }
		return object.getName();
	}

	private DaliUIContext getContext() {
		return context;
	}

	private void setContext(DaliUIContext context) {
		this.context = context;
	}
}
