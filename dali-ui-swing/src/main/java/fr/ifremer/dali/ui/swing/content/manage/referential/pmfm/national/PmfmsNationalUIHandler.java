package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.menu.PmfmMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.table.PmfmTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.table.PmfmTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.editor.AssociatedQualitativeValueCellEditor;
import fr.ifremer.dali.ui.swing.util.table.renderer.AssociatedQualitativeValueCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Quadruplets au niveau national
 */
public class PmfmsNationalUIHandler extends
        AbstractDaliTableUIHandler<PmfmTableRowModel, PmfmsNationalUIModel, PmfmsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(PmfmsNationalUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        PmfmsNationalUIModel model = new PmfmsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(PmfmsNationalUI ui) {
        initUI(ui);
        
        // hide 'apply filter'
        ui.getPmfmsNationalMenuUI().getHandler().enableContextFilter(false);

        // listen to search results
        ui.getPmfmsNationalMenuUI().getModel().addPropertyChangeListener(PmfmMenuUIModel.PROPERTY_RESULTS,
                evt -> getModel().setBeans((List<PmfmDTO>) evt.getNewValue()));
        
        // listen to 'apply filter' results
        ui.getPmfmsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS,
                evt -> getModel().setBeans((List<PmfmDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        TableColumnExt idCol = addColumn(PmfmTableModel.PMFM_ID);
        idCol.setSortable(true);
        idCol.setEditable(false);

        // name
        TableColumnExt nameCol = addColumn(PmfmTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // parameter

        TableColumnExt parameterCol = addColumn(
                null,
                newTableCellRender(
                        PmfmTableModel.PARAMETER.getPropertyType(),
                        PmfmTableModel.PARAMETER.getDecoratorName(),
                        DecoratorService.NAME // Transcribed name as tooltip (Mantis #47110)
                ),
                PmfmTableModel.PARAMETER);
        parameterCol.setSortable(true);
        parameterCol.setEditable(false);

        // support
        final TableColumnExt supportCol = addColumn(PmfmTableModel.SUPPORT);
        supportCol.setSortable(true);
        supportCol.setEditable(false);

        // fraction
        final TableColumnExt fractionCol = addColumn(PmfmTableModel.FRACTION);
        fractionCol.setSortable(true);
        fractionCol.setEditable(false);

        // method
        final TableColumnExt methodCol = addColumn(PmfmTableModel.METHOD);
        methodCol.setSortable(true);
        methodCol.setEditable(false);

        // unit
        final TableColumnExt unitCol = addColumn(PmfmTableModel.UNIT);
        unitCol.setSortable(true);
        unitCol.setEditable(false);

//		// threshold
//		final TableColumnExt stepCol = addColumnToModel(columnModel, PmfmTableModel.THRESHOLD);
//		stepCol.setSortable(true);
//		stepCol.setEditable(false);
//
//		// max decimal number
//		final TableColumnExt maxDecimalNumberCol = addColumnToModel(columnModel, PmfmTableModel.MAX_DECIMAL_NUMBER);
//		maxDecimalNumberCol.setSortable(true);
//		maxDecimalNumberCol.setEditable(false);
//
//		// significant digits number
//		final TableColumnExt significantDigitsNumberCol = addColumnToModel(columnModel, PmfmTableModel.SIGNIFICANT_DIGITS_NUMBER);
//		significantDigitsNumberCol.setSortable(true);
//		significantDigitsNumberCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(PmfmTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(PmfmTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(PmfmTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addColumn(PmfmTableModel.STATUS);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // Associated qualitative value
        final TableColumnExt associatedQualitativeValueCol = addColumn(
                new AssociatedQualitativeValueCellEditor(getTable(), getUI(), false),
                new AssociatedQualitativeValueCellRenderer(),
                PmfmTableModel.QUALITATIVE_VALUES);
        associatedQualitativeValueCol.setSortable(true);
        fixColumnWidth(associatedQualitativeValueCol, 120);

        PmfmTableModel tableModel = new PmfmTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.pmfm"), PmfmTableModel.QUALITATIVE_VALUES);

        // Initialisation du tableau
        initTable(table, true);

        // Les colonnes optionnelles sont invisibles
        associatedQualitativeValueCol.setVisible(false);
        idCol.setVisible(false);
//		stepCol.setVisible(false);
//		maxDecimalNumberCol.setVisible(false);
//		significantDigitsNumberCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }



    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<PmfmTableRowModel> getTableModel() {
        return (PmfmTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getPmfmsNationalTable();
    }

}
