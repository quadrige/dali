package fr.ifremer.dali.ui.swing.action;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.service.control.ControlRuleMessagesBean;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.DialogHelper;
import fr.ifremer.quadrige3.ui.swing.action.AbstractAction;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportReferentialSynchroAtOnceAction;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 04/07/2017.
 */
public abstract class AbstractDaliAction<M extends AbstractBeanUIModel, UI extends DaliUI<M, ?>, H extends AbstractUIHandler<M, UI>>
        extends AbstractAction<M, UI, H> {

    /**
     * <p>Constructor for AbstractAction.</p>
     *
     * @param handler  a H object.
     * @param hideBody a boolean.
     */
    public AbstractDaliAction(H handler, boolean hideBody) {
        super(handler, hideBody);
    }

    @Override
    protected DaliConfiguration getConfig() {
        return (DaliConfiguration) getHandler().getConfig();
    }

    @Override
    public DaliUIContext getContext() {
        return (DaliUIContext) super.getContext();
    }

    /* Utility methods */

    /**
     * <p>showControlResult.</p>
     *
     * @param messages      a {@link fr.ifremer.dali.service.control.ControlRuleMessagesBean} object.
     * @param showIfSuccess a boolean.
     */
    protected void showControlResult(ControlRuleMessagesBean messages, boolean showIfSuccess) {

        // TODO show all detailed messages ?

        if (messages == null || (!messages.isErrorMessages() && !messages.isWarningMessages())) {
            if (showIfSuccess) {
                displayInfoMessage(t("dali.rulesControl.title"), t("dali.rulesControl.noMessage.message"));
            }
        } else if (messages.isWarningMessages() && messages.isErrorMessages()) {
            displayErrorMessage(t("dali.rulesControl.title"), t("dali.rulesControl.errorAndWarningMessages.message"));
        } else if (messages.isErrorMessages()) {
            displayErrorMessage(t("dali.rulesControl.title"), t("dali.rulesControl.errorMessages.message"));
        } else if (messages.isWarningMessages()) {
            displayWarningMessage(t("dali.rulesControl.title"), t("dali.rulesControl.warningMessages.message"));
        }
    }

    /**
     * <p>showControlResultAndAskContinue.</p>
     *
     * @param messages      a {@link ControlRuleMessagesBean} object.
     */
    protected boolean showControlResultAndAskContinue(ControlRuleMessagesBean messages) {

        // TODO show all detailed messages ?
        boolean canContinue = true;
        if (messages != null) {
            if (messages.isWarningMessages() && messages.isErrorMessages()) {
                canContinue = getContext().getDialogHelper().showOptionDialog(null,
                        t("dali.rulesControl.errorAndWarningMessages.message"),
                        null,
                        t("dali.rulesControl.continue.message"),
                        t("dali.rulesControl.title"),
                        JOptionPane.ERROR_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("dali.common.continue"),
                        t("dali.common.cancel")
                ) == JOptionPane.OK_OPTION;
            } else if (messages.isErrorMessages()) {
                canContinue = getContext().getDialogHelper().showOptionDialog(null,
                        t("dali.rulesControl.errorMessages.message"),
                        null,
                        t("dali.rulesControl.continue.message"),
                        t("dali.rulesControl.title"),
                        JOptionPane.ERROR_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("dali.common.continue"),
                        t("dali.common.cancel")
                ) == JOptionPane.OK_OPTION;
            } else if (messages.isWarningMessages()) {
                canContinue = getContext().getDialogHelper().showOptionDialog(null,
                        t("dali.rulesControl.warningMessages.message"),
                        null,
                        t("dali.rulesControl.continue.message"),
                        t("dali.rulesControl.title"),
                        JOptionPane.WARNING_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("dali.common.continue"),
                        t("dali.common.cancel")
                ) == JOptionPane.OK_OPTION;
            }
        }
        return canContinue;
    }

    /**
     * Do an import of referential data
     */
    protected void doImportReferential() {

        getModel().setModify(false); // avoid modified model detection

        // Will clear the updateDt cache, then reimport referential tables
        ImportReferentialSynchroAtOnceAction importAction = getContext().getActionFactory().createLogicAction(getContext().getMainUI().getHandler(), ImportReferentialSynchroAtOnceAction.class);
        importAction.setForceClearUpdateDateCache(true);

        // force hide body
        importAction.getContext().setHideBody(false);

        // override action description
        forceActionDescription(t("dali.action.synchro.import.referential"));

        // run internally
        getContext().getActionEngine().runInternalAction(importAction);

        // restore body
        importAction.getContext().setHideBody(true);

    }

}
