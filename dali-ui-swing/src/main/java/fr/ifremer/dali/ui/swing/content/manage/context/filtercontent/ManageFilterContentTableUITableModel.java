package fr.ifremer.dali.ui.swing.content.manage.context.filtercontent;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ManageFilterContentTableUITableModel extends AbstractDaliTableModel<ManageFilterContentTableUIRowModel> {

	/** Constant <code>LABEL</code> */
	public static final DaliColumnIdentifier<ManageFilterContentTableUIRowModel> LABEL = DaliColumnIdentifier.newId(
			ManageFilterContentTableUIRowModel.PROPERTY_LABEL,
			n("dali.filter.selected.label"),
			n("dali.filter.selected.tip"),
			String.class);
 
	/**
	 * <p>Constructor for ManageFilterContentTableUITableModel.</p>
	 *
	 */
	public ManageFilterContentTableUITableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ManageFilterContentTableUIRowModel createNewRow() {
		return new ManageFilterContentTableUIRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ManageFilterContentTableUIRowModel> getFirstColumnEditing() {
		return LABEL;
	}
}
