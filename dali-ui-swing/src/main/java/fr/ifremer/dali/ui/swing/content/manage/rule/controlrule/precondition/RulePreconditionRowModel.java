package fr.ifremer.dali.ui.swing.content.manage.rule.controlrule.precondition;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePreconditionRowModel extends AbstractDaliRowUIModel<QualitativeValueDTO, RulePreconditionRowModel> implements QualitativeValueDTO {

    private static final Binder<QualitativeValueDTO, RulePreconditionRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(QualitativeValueDTO.class, RulePreconditionRowModel.class);
    private static final Binder<RulePreconditionRowModel, QualitativeValueDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(RulePreconditionRowModel.class, QualitativeValueDTO.class);

    public RulePreconditionRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }



    // Delegate methods

    @Override
    protected QualitativeValueDTO newBean() {
        return DaliBeanFactory.newQualitativeValueDTO();
    }

    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    @Override
    public String getName() {
        return delegateObject.getName();
    }

    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public Date getCreationDate() {
        return null;
    }

    @Override
    public void setCreationDate(Date date) {

    }

    @Override
    public Date getUpdateDate() {
        return null;
    }

    @Override
    public void setUpdateDate(Date date) {

    }

    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }
}
