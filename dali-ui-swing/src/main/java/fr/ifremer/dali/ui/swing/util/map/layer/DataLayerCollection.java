package fr.ifremer.dali.ui.swing.util.map.layer;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;

import java.util.List;

/**
 * A layer collection used to group layers from a data object
 *
 * @author peck7 on 12/06/2017.
 */
public interface DataLayerCollection {

    /**
     * Get all existing layers in this collection
     * @return all layers
     */
    List<Layer> getLayers();

    /**
     * Calculate the envelope of this collection
     * @return the envelope
     */
    default ReferencedEnvelope getEnvelope() {
        ReferencedEnvelope envelope = new ReferencedEnvelope();
        for (Layer layer : getLayers()) {
            envelope.expandToInclude(layer.getBounds());
        }
        return envelope;
    }

}
