package fr.ifremer.dali.ui.swing.action;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.content.DaliMainUIHandler;
import fr.ifremer.quadrige3.ui.swing.action.AbstractMainUIAction;
import fr.ifremer.quadrige3.ui.swing.action.GoToHomeAction;
import fr.ifremer.quadrige3.ui.swing.action.GoToManageDbAction;
import fr.ifremer.quadrige3.ui.swing.content.db.OpenDbAction;

import static org.nuiton.i18n.I18n.t;

/**
 * Start action.
 * <p/>
 * starts ui action (open db if exists, or go to manage db screen).
 *
 * @since 2.4
 */
public class StartAction extends AbstractDaliMainUIAction {

    private AbstractMainUIAction delegateAction;

    /**
     * <p>Constructor for StartAction.</p>
     *
     * @param handler a {@link DaliMainUIHandler} object.
     */
    public StartAction(DaliMainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("dali.main.action.startDali"));
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        super.prepareAction();

        if (getContext().isPersistenceLoaded()) {
            // db already opened (happens when reloading ui)
            // just go to home screen
            GoToHomeAction action = getContext().getActionFactory().createLogicAction(handler, GoToHomeAction.class);
            action.setSkipCheckCurrentScreen(true);
            delegateAction = action;

        } else {

            if (getContext().isDbExist()) {
                // open db (using a fake button to have simple api)
                OpenDbAction action = getContext().getActionFactory().createLogicAction(handler, OpenDbAction.class);
                action.setSkipCheckCurrentScreen(true);
                delegateAction = action;

            } else {

                // clean db context
                getContext().clearDbContext();

                // go to manage db screen (to install db)
                GoToManageDbAction action = getContext().getActionFactory().createLogicAction(handler, GoToManageDbAction.class);
                action.setSkipCheckCurrentScreen(true);
                delegateAction = action;
            }
        }

        setActionDescription(delegateAction.getActionDescription());
        return delegateAction.prepareAction();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {
        getActionEngine().runInternalAction(delegateAction);
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        delegateAction = null;
        super.releaseAction();
    }

}
