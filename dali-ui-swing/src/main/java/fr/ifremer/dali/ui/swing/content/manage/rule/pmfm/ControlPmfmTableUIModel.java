package fr.ifremer.dali.ui.swing.content.manage.rule.pmfm;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.dali.ui.swing.content.manage.rule.RulesUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;

/**
 * Model pour le tableau des controles sur les PSFM
 */
public class ControlPmfmTableUIModel extends AbstractDaliTableUIModel<RulePmfmDTO, ControlPmfmRowModel, ControlPmfmTableUIModel> {

    private RulesUIModel parentModel;

    private boolean pmfmsMandatory;
    /** Constant <code>PROPERTY_PMFMS_MANDATORY="pmfmsMandatory"</code> */
    public static final String PROPERTY_PMFMS_MANDATORY = "pmfmsMandatory";

    /**
     * Constructor.
     */
    public ControlPmfmTableUIModel() {
        super();
    }

    /**
     * <p>Getter for the field <code>parentModel</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.manage.rule.RulesUIModel} object.
     */
    public RulesUIModel getParentModel() {
        return parentModel;
    }

    /**
     * <p>Setter for the field <code>parentModel</code>.</p>
     *
     * @param parentModel a {@link fr.ifremer.dali.ui.swing.content.manage.rule.RulesUIModel} object.
     */
    public void setParentModel(RulesUIModel parentModel) {
        this.parentModel = parentModel;
    }

    /**
     * <p>isPmfmsMandatory.</p>
     *
     * @return a boolean.
     */
    public boolean isPmfmsMandatory() {
        return pmfmsMandatory;
    }

    /**
     * <p>Setter for the field <code>pmfmsMandatory</code>.</p>
     *
     * @param pmfmsMandatory a boolean.
     */
    public void setPmfmsMandatory(boolean pmfmsMandatory) {
        boolean oldValue = isPmfmsMandatory();
        this.pmfmsMandatory = pmfmsMandatory;
        firePropertyChange(PROPERTY_PMFMS_MANDATORY, oldValue, pmfmsMandatory);
    }
}
