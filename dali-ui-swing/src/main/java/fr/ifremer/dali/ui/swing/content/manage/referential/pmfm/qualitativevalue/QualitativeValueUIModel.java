package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;

/**
 * Modele pour la zone des prelevements de l'ecran d'accueil.
 */
public class QualitativeValueUIModel extends AbstractDaliTableUIModel<QualitativeValueDTO, QualitativeValueTableRowModel, QualitativeValueUIModel> {

    private boolean editable;
    public static final String PROPERTY_EDITABLE = "editable";

    private boolean doubleList;
    public static final String PROPERTY_DOUBLE_LIST = "doubleList";
    private AbstractDaliRowUIModel parentRowModel = null;
    public static final String PROPERTY_PARENT_ROW_MODEL = "parentRowModel";

    /**
     * Constructor.
     */
    public QualitativeValueUIModel() {
        super();
    }

    /**
     * <p>Getter for the field <code>parentRowModel</code>.</p>
     *
     * @return a {@link AbstractDaliRowUIModel} object.
     */
    public AbstractDaliRowUIModel getParentRowModel() {
        return parentRowModel;
    }

    /**
     * <p>Setter for the field <code>parentRowModel</code>.</p>
     *
     * @param parentRowModel a {@link AbstractDaliRowUIModel} object.
     */
    public void setParentRowModel(AbstractDaliRowUIModel parentRowModel) {
        this.parentRowModel = parentRowModel;
        firePropertyChange(PROPERTY_PARENT_ROW_MODEL, null, parentRowModel);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);

    }

    public boolean isDoubleList() {
        return doubleList;
    }

    public void setDoubleList(boolean doubleList) {
        this.doubleList = doubleList;
        firePropertyChange(PROPERTY_DOUBLE_LIST, null, doubleList);
    }
}
