package fr.ifremer.dali.ui.swing.content.manage.program.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUIModel;
import fr.ifremer.dali.ui.swing.content.manage.program.SaveAction;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

/**
 * Search action.
 */
public class SearchAction extends AbstractCheckModelAction<ProgramsMenuUIModel, ProgramsMenuUI, ProgramsMenuUIHandler> {

    private List<ProgramDTO> results;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final ProgramsMenuUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<SaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getHandler().getProgramsUI().getHandler();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        final ProgramsUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        final ProgramsUIModel model = getLocalModel();
        if (model != null) {
            model.setModify(modelModify);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        final ProgramsUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {

        return super.prepareAction();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        results = StringUtils.isBlank(getModel().getProgramCode())
                ? getContext().getProgramStrategyService().getReadablePrograms()
                : ImmutableList.of(getContext().getProgramStrategyService().getReadableProgramByCode(getModel().getProgramCode()));

        // remove errors (cause they are stored in cache)
        if (CollectionUtils.isNotEmpty(results))
            results.forEach(program -> program.getErrors().clear());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {

        getModel().setResults(results);

        super.postSuccessAction();
    }

    private ProgramsUIModel getLocalModel() {
        ProgramsUI ui = getHandler().getProgramsUI();
        return ui != null ? ui.getModel() : null;
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        ProgramsUI ui = getHandler().getProgramsUI();
        if (ui != null) {
            return ImmutableList.of(
                    ui.getProgramsTableUI().getModel(),
                    ui.getLocationsTableUI().getModel(),
                    ui.getPmfmsTableUI().getModel(),
                    ui.getStrategiesTableUI().getModel()
            );
        }
        return super.getOtherModelsToModify();
    }

}
