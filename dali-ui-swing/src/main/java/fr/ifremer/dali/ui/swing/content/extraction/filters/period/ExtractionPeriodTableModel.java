package fr.ifremer.dali.ui.swing.content.extraction.filters.period;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.time.LocalDate;

import static org.nuiton.i18n.I18n.n;

/**
 * Created by Ludovic on 18/01/2016.
 */
public class ExtractionPeriodTableModel extends AbstractDaliTableModel<ExtractionPeriodRowModel> {

    /** Constant <code>START_DATE</code> */
    public static final DaliColumnIdentifier<ExtractionPeriodRowModel> START_DATE = DaliColumnIdentifier.newId(
            ExtractionPeriodRowModel.PROPERTY_START_DATE,
            n("dali.extraction.period.startDate.label"),
            n("dali.extraction.period.startDate.label"),
            LocalDate.class,
            true);

    /** Constant <code>END_DATE</code> */
    public static final DaliColumnIdentifier<ExtractionPeriodRowModel> END_DATE = DaliColumnIdentifier.newId(
            ExtractionPeriodRowModel.PROPERTY_END_DATE,
            n("dali.extraction.period.endDate.label"),
            n("dali.extraction.period.endDate.label"),
            LocalDate.class,
            true);

    /**
     * <p>Constructor for ExtractionPeriodTableModel.</p>
     *
     */
    public ExtractionPeriodTableModel(SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<ExtractionPeriodRowModel> getFirstColumnEditing() {
        return START_DATE;
    }

    /** {@inheritDoc} */
    @Override
    public ExtractionPeriodRowModel createNewRow() {
        return new ExtractionPeriodRowModel();
    }
}
