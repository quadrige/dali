package fr.ifremer.dali.ui.swing.content.manage.program.strategiesByLocation.programs;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.time.LocalDate;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class StrategiesProgrammeTableModel extends AbstractDaliTableModel<StrategiesProgrammeTableRowModel> {

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final DaliColumnIdentifier<StrategiesProgrammeTableRowModel> NAME = DaliColumnIdentifier.newId(
    		StrategiesProgrammeTableRowModel.PROPERTY_NAME,
            n("dali.program.strategies.program.name.short"),
            n("dali.program.strategies.program.name.tip"),
            String.class);

	/**
	 * Identifiant pour la colonne date debut.
	 */
    public static final DaliColumnIdentifier<StrategiesProgrammeTableRowModel> START_DATE = DaliColumnIdentifier.newId(
    		StrategiesProgrammeTableRowModel.PROPERTY_START_DATE,
            n("dali.program.strategies.program.startDate.short"),
            n("dali.program.strategies.program.startDate.tip"),
			LocalDate.class);

	/**
	 * Identifiant pour la colonne date fin.
	 */
    public static final DaliColumnIdentifier<StrategiesProgrammeTableRowModel> END_DATE = DaliColumnIdentifier.newId(
    		StrategiesProgrammeTableRowModel.PROPERTY_END_DATE,
            n("dali.program.strategies.program.endDate.short"),
            n("dali.program.strategies.program.endDate.tip"),
			LocalDate.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public StrategiesProgrammeTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public StrategiesProgrammeTableRowModel createNewRow() {
		return new StrategiesProgrammeTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<StrategiesProgrammeTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
