package fr.ifremer.dali.ui.swing.content.manage.referential.samplingequipment.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.samplingequipment.menu.SamplingEquipmentsMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.samplingequipment.table.SamplingEquipmentsTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.samplingequipment.table.SamplingEquipmentsTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des enginsPrelevement au niveau national
 */
public class SamplingEquipmentsNationalUIHandler extends
        AbstractDaliTableUIHandler<SamplingEquipmentsTableRowModel, SamplingEquipmentsNationalUIModel, SamplingEquipmentsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(SamplingEquipmentsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        SamplingEquipmentsNationalUIModel model = new SamplingEquipmentsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(SamplingEquipmentsNationalUI ui) {
        initUI(ui);

        // init listener on found samplingEquipments
        ui.getSamplingEquipmentsNationalMenuUI().getModel().addPropertyChangeListener(SamplingEquipmentsMenuUIModel.PROPERTY_RESULTS, evt -> {
            if (evt.getNewValue() != null) {
                getModel().setBeans((List<SamplingEquipmentDTO>) evt.getNewValue());
            }
        });

        // listen to 'apply filter' results
        ui.getSamplingEquipmentsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS,
                evt -> getModel().setBeans((List<SamplingEquipmentDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // mnemonic
        TableColumnExt mnemonicCol = addColumn(SamplingEquipmentsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        TableColumnExt descriptionCol = addColumn(SamplingEquipmentsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // size
        TableColumnExt sizeCol = addColumn(SamplingEquipmentsTableModel.SIZE);
        sizeCol.setSortable(true);
        sizeCol.setEditable(false);

        // unit
        TableColumnExt unitCol = addFilterableComboDataColumnToModel(
                SamplingEquipmentsTableModel.UNIT,
                getContext().getReferentialService().getUnits(StatusFilter.ACTIVE),
                false);
        unitCol.setSortable(true);
        unitCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(SamplingEquipmentsTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(SamplingEquipmentsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(SamplingEquipmentsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                SamplingEquipmentsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ACTIVE),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        SamplingEquipmentsTableModel tableModel = new SamplingEquipmentsTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.samplingEquipment"));

        // Initialisation du tableau
        initTable(table, true);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
        getModel().setBeans(null);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<SamplingEquipmentsTableRowModel> getTableModel() {
        return (SamplingEquipmentsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getSamplingEquipmentsNationalTable();
    }
}
