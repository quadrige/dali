package fr.ifremer.dali.ui.swing.content.manage.program;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.ui.swing.action.QuitScreenAction;
import fr.ifremer.dali.ui.swing.content.manage.program.menu.ProgramsMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.program.menu.SearchAction;
import fr.ifremer.dali.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.quadrige3.synchro.vo.SynchroProgressionStatus;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportSynchroCheckAction;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingUtilities;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l'onglet prelevements mesures.
 */
public class ProgramsUIHandler extends AbstractDaliUIHandler<ProgramsUIModel, ProgramsUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ProgramsUIHandler.class);
    private ImportSynchroCheckAction importSynchroCheckAction;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ProgramsUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ProgramsUIModel model = new ProgramsUIModel();
        ui.setContextValue(model);

        ui.setContextValue(SwingUtil.createActionIcon("program"));

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ProgramsUI ui) {
        initUI(ui);

        ui.getMenuUI().getHandler().enableContextFilter(false);

        // Initialiser les parametres des ecrans Observation et prelevemnts
        getContext().clearObservationIds();

        // Save models
        getModel().setProgramsUIModel(getUI().getProgramsTableUI().getModel());
        getModel().setStrategiesUIModel(getUI().getStrategiesTableUI().getModel());
        getModel().setLocationsUIModel(getUI().getLocationsTableUI().getModel());
        getModel().setPmfmsUIModel(getUI().getPmfmsTableUI().getModel());

        initListeners();

        // Chargement des programmes si un lieu a ete selectionne
        if (getContext().getSelectProgramCode() != null) {

            // Load un programme
            SearchAction searchAction = getContext().getActionFactory().createLogicAction(getUI().getMenuUI().getHandler(), SearchAction.class);
            searchAction.getModel().setProgram(null);
            getContext().getActionEngine().runInternalAction(searchAction);
        }

        getUI().applyDataBinding(ProgramsUI.BINDING_SAVE_BUTTON_ENABLED);

        // check referential update (Mantis #46969)
        SwingUtilities.invokeLater(this::checkForReferentialUpdates);
    }

    @SuppressWarnings("unchecked")
    private void initListeners() {

        // listen to menu results
        getUI().getMenuUI().getModel().addPropertyChangeListener(ProgramsMenuUIModel.PROPERTY_RESULTS, evt -> {

            // Clear sub tables
            getModel().getStrategiesUIModel().clear();
            getModel().getLocationsUIModel().clear();
            getModel().getPmfmsUIModel().clear();
            // affect programs
            getUI().getProgramsTableUI().getHandler().loadPrograms((Collection<ProgramDTO>) evt.getNewValue());
        });

        // Listen modify property and set dirty to the selected program
        PropertyChangeListener modifyListener = evt -> {

            Boolean modify = (Boolean) evt.getNewValue();
            if (modify != null) {
                getModel().setModify(modify);

                ProgramsTableRowModel selectedProgram = getModel().getProgramsUIModel().getSingleSelectedRow();
                if (selectedProgram != null) {
                    if (modify) {
                        selectedProgram.setDirty(true);
                    }
                    getUI().getProgramsTableUI().getHandler().recomputeRowValidState(selectedProgram);
                    getUI().getProgramsTableUI().getHandler().forceRevalidateModel();
                    forceRevalidateModel();
                }

            }
        };
        getModel().getProgramsUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_MODIFY, modifyListener);
        getModel().getStrategiesUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_MODIFY, modifyListener);
        getModel().getLocationsUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_MODIFY, modifyListener);
        getModel().getPmfmsUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_MODIFY, modifyListener);

        // Listen valid state
        listenModelValid(getModel().getProgramsUIModel());

        getModel().getStrategiesUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_VALID, evt -> {

            Boolean valid = (Boolean) evt.getNewValue();
            if (valid != null) {
                getModel().setValid(valid);
                if (getModel().getProgramsUIModel().getSingleSelectedRow() != null) {
                    getModel().getProgramsUIModel().getSingleSelectedRow().setStrategiesValid(valid);
                    getUI().getProgramsTableUI().getHandler().recomputeRowValidState(getModel().getProgramsUIModel().getSingleSelectedRow());
                    forceRevalidateModel();
                }
            }
        });

        getModel().getLocationsUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_VALID, evt -> {

            Boolean valid = (Boolean) evt.getNewValue();
            if (valid != null) {
                getModel().setValid(valid);
                if (getModel().getProgramsUIModel().getSingleSelectedRow() != null) {
                    getModel().getProgramsUIModel().getSingleSelectedRow().setLocationsValid(valid);
                    getUI().getProgramsTableUI().getHandler().recomputeRowValidState(getModel().getProgramsUIModel().getSingleSelectedRow());
                    forceRevalidateModel();
                }
            }
        });

        getModel().getPmfmsUIModel().addPropertyChangeListener(AbstractDaliBeanUIModel.PROPERTY_VALID, evt -> {

            Boolean valid = (Boolean) evt.getNewValue();
            if (valid != null) {
                getModel().setValid(valid);
                if (getModel().getStrategiesUIModel().getSingleSelectedRow() != null) {
                    getUI().getStrategiesTableUI().getHandler().recomputeRowValidState(getModel().getStrategiesUIModel().getSingleSelectedRow());
                    forceRevalidateModel();
                }
            }
        });

        // Add listener on saveEnabled property to disable change (Mantis #47532)
        getModel().addPropertyChangeListener(ProgramsUIModel.PROPERTY_SAVE_ENABLED, evt -> getModel().getProgramsUIModel().setSaveEnabled(getModel().isSaveEnabled()));

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<ProgramsUIModel> getValidator() {
        return getUI().getValidator();
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private void checkForReferentialUpdates() {
        if (getContext().isNextImportSynchroCheckActionPrevented()) {
            getModel().setSaveEnabled(true);
            return;
        }
        if (getContext().getSynchroContext().isRunningStatus()) {
            return;
        }
        getImportSynchroCheckAction().execute();
        if (LOG.isDebugEnabled())
            LOG.debug("checkForReferentialUpdates executed");
    }

    private ImportSynchroCheckAction getImportSynchroCheckAction() {
        if (importSynchroCheckAction == null) {
            importSynchroCheckAction = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(), ImportSynchroCheckAction.class);
            importSynchroCheckAction.setCheckReferentialOnly(true);
            importSynchroCheckAction.setUseOptimisticCheck(true);
        }
        if (!importSynchroCheckAction.isConsumerSet()) {
            importSynchroCheckAction.setConsumer(synchroUIContext -> {

                if (LOG.isDebugEnabled())
                    LOG.debug("check result: " + synchroUIContext.isImportReferential());

                // If error occurs (eg. connection problem) set screen read-only (Mantis #47532)
                if (synchroUIContext.getStatus() == SynchroProgressionStatus.FAILED) {
                    getModel().setSaveEnabled(false);
                    getContext().getDialogHelper().showWarningDialog(t("dali.error.synchro.serverNotYetAvailable"));
                } else {
                    getModel().setSaveEnabled(true);
                    // get result
                    if (synchroUIContext.isImportReferential()) {
                        UpdateProgramsAction updateProgramsAction = getContext().getActionFactory().createLogicAction(this, UpdateProgramsAction.class);
                        getContext().getActionEngine().runAction(updateProgramsAction);
                    }
                }

                // Reset correctly the synchro context and widget (Mantis #48842)
                getContext().getSynchroHandler().report(t("quadrige3.synchro.report.idle"), false);
                getContext().getSynchroContext().resetImportContext();
                getContext().getSynchroContext().saveImportContext(true, true);

            });
        }
        return importSynchroCheckAction;
    }

}
