package fr.ifremer.dali.ui.swing.content.welcome;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import jaxx.runtime.SwingUtil;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

/**
 * Controller for Welcome scereen.
 */
public class WelcomeUIHandler extends AbstractDaliUIHandler<DaliUIContext, WelcomeUI> {

    /**
     * Button font.
     */
    private static final Font BUTTON_FONT = new Font("", Font.BOLD, 40);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final WelcomeUI ui) {
        super.beforeInit(ui);

        // Ajout du model pour la page d acceuil
        ui.setContextValue(getContext());

        ui.setContextValue(SwingUtil.createActionIcon("home"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final WelcomeUI ui) {
        initUI(ui);

        // Change font on title
        getUI().getObservationButton().setEnabled(getContext().isAuthenticated());
        getUI().getObservationButton().setFont(BUTTON_FONT);
        ImageIcon image = getResourceImage("saisie", "jpg");
        if (image != null) {

            // Add image on label
            getUI().getObservationButton().setIcon(image);
            getUI().getObservationButton().setPreferredSize(new Dimension(image.getIconWidth(), image.getIconHeight()));

            getUI().getObservationButton().setHorizontalTextPosition(SwingConstants.CENTER);
            getUI().getObservationButton().setForeground(Color.WHITE);
        }

        getUI().getExtractionButton().setEnabled(getContext().isAuthenticated());
        getUI().getExtractionButton().setFont(BUTTON_FONT);
        image = getResourceImage("extraction","jpg");
        if (image != null) {

            // Add image on label
            getUI().getExtractionButton().setIcon(image);
            getUI().getExtractionButton().setPreferredSize(new Dimension(image.getIconWidth(), image.getIconHeight()));

            getUI().getExtractionButton().setHorizontalTextPosition(SwingConstants.CENTER);
            getUI().getExtractionButton().setForeground(Color.WHITE);
        }

    }

}
