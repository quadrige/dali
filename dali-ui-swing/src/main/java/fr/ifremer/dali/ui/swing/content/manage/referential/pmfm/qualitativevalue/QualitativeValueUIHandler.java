package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handler.
 */
public class QualitativeValueUIHandler extends AbstractDaliTableUIHandler<QualitativeValueTableRowModel, QualitativeValueUIModel, QualitativeValueUI> implements Cancelable {

    public static final String DOUBLE_LIST = "doubleList";
    public static final String TABLE = "table";

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(QualitativeValueUI ui) {
        super.beforeInit(ui);

        QualitativeValueUIModel model = new QualitativeValueUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(QualitativeValueUI qualitativeValueUI) {

        initUI(qualitativeValueUI);

        // Init table columns and properties
        initTable();

        initBeanList(getUI().getAssociatedQualitativeValuesDoubleList(), null, null);

        getModel().addPropertyChangeListener(QualitativeValueUIModel.PROPERTY_PARENT_ROW_MODEL, evt -> {

            // here, detect parentRowModel type , if PmfmDTO and local, set a DoubleList view
            getModel().setDoubleList(PmfmStrategyDTO.class.isAssignableFrom(getModel().getParentRowModel().getClass()) && getModel().isEditable());

            if (getModel().isDoubleList()) {
                PmfmStrategyDTO pmfmStrategy = (PmfmStrategyDTO) getModel().getParentRowModel();
                List<QualitativeValueDTO> universeValues = null;
                if (pmfmStrategy.getPmfm() != null && pmfmStrategy.getPmfm().getParameter() != null) {
                    List<QualitativeValueDTO> qualitativeValues = pmfmStrategy.getPmfm().getQualitativeValues();
                    // sort by label
                    universeValues = qualitativeValues.stream().sorted(Comparator.comparing(BaseReferentialDTO::getName)).collect(Collectors.toList());
                }
                List<QualitativeValueDTO> selectedValues = Lists.newArrayList(pmfmStrategy.getQualitativeValues());

                getUI().getAssociatedQualitativeValuesDoubleList().getModel().setUniverse(universeValues);
                getUI().getAssociatedQualitativeValuesDoubleList().getModel().setSelected(selectedValues);

                getUI().getListPanelLayout().setSelected(DOUBLE_LIST);
            } else {
                Collection<QualitativeValueDTO> qualitativeValues = getQualitativeValuesFromRow(getModel().getParentRowModel());
                if (qualitativeValues != null) {
                    qualitativeValues = qualitativeValues.stream()
                            .sorted(Comparator.comparing(BaseReferentialDTO::getName))
                                .collect(Collectors.toList());
                }
                getModel().setBeans(qualitativeValues);
                getUI().getListPanelLayout().setSelected(TABLE);
            }

//            getModel().setBeans(getQualitativeValuesFromRow(getModel().getParentRowModel()));
        });
    }

    private void initTable() {

        // La table des prelevements
        final SwingTable table = getTable();

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(QualitativeValueTableModel.MNEMONIC);
        mnemonicCol.setSortable(true);

        // Description
        final TableColumnExt descriptionCol = addColumn(QualitativeValueTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
            QualitativeValueTableModel.STATUS,
            getContext().getReferentialService().getStatus(StatusFilter.ALL),
            false);
        statusCol.setSortable(true);

        // model for table
        final QualitativeValueTableModel tableModel = new QualitativeValueTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        initTable(table);
        table.setEditable(false);

        // 5 lines visibles
        table.setVisibleRowCount(5);
    }

    private Collection<QualitativeValueDTO> getQualitativeValuesFromRow(AbstractDaliRowUIModel rowModel) {
        if (ParameterDTO.class.isAssignableFrom(rowModel.getClass())) {
            return ((ParameterDTO) rowModel).getQualitativeValues();
        }
        if (PmfmDTO.class.isAssignableFrom(rowModel.getClass())) {
            return ((PmfmDTO) rowModel).getQualitativeValues();
        }
        if (PmfmStrategyDTO.class.isAssignableFrom(rowModel.getClass())) {
            return ((PmfmStrategyDTO) rowModel).getQualitativeValues();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<QualitativeValueTableRowModel> getTableModel() {
        return (QualitativeValueTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getAssociatedQualitativeValuesTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel() {
        closeDialog();
    }

    public void valid() {

        // Replace the old associated qualitative value liste by the new list (no save here !)
        // only for PmfmStrategy
        if (PmfmStrategyDTO.class.isAssignableFrom(getModel().getParentRowModel().getClass()) && getModel().isDoubleList() && getModel().isEditable()) {
            ((PmfmStrategyDTO) getModel().getParentRowModel()).setQualitativeValues(
                getUI().getAssociatedQualitativeValuesDoubleList().getModel().getSelected()
            );
            getModel().getParentRowModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);
        }

        // close the dialog box
        closeDialog();
    }
}
