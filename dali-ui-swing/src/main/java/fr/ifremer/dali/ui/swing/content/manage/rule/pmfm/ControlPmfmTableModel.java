package fr.ifremer.dali.ui.swing.content.manage.rule.pmfm;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.UnitDTO;
import fr.ifremer.dali.dto.referential.pmfm.*;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlPmfmTableModel extends AbstractDaliTableModel<ControlPmfmRowModel> {

	public static final DaliColumnIdentifier<ControlPmfmRowModel> PMFM_ID = DaliColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_ID,
			n("dali.property.pmfm.id"),
			n("dali.property.pmfm.id"),
			Integer.class);

	/** Constant <code>NAME</code> */
	public static final DaliColumnIdentifier<ControlPmfmRowModel> NAME = DaliColumnIdentifier.newPmfmNameId(
			ControlPmfmRowModel.PROPERTY_PMFM,
			n("dali.property.name"),
			n("dali.property.name"));
	
	/** Constant <code>MATRIX</code> */
	public static final DaliColumnIdentifier<ControlPmfmRowModel> MATRIX = DaliColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_MATRIX,
			n("dali.property.pmfm.matrix"),
			n("dali.property.pmfm.matrix"),
			MatrixDTO.class);
	
	/** Constant <code>FRACTION</code> */
	public static final DaliColumnIdentifier<ControlPmfmRowModel> FRACTION = DaliColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_FRACTION,
			n("dali.property.pmfm.fraction"),
			n("dali.property.pmfm.fraction"),
			FractionDTO.class);
	
	/** Constant <code>METHOD</code> */
	public static final DaliColumnIdentifier<ControlPmfmRowModel> METHOD = DaliColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_METHOD,
			n("dali.property.pmfm.method"),
			n("dali.property.pmfm.method"),
			MethodDTO.class);
	
	/** Constant <code>PARAMETER</code> */
	public static final DaliColumnIdentifier<ControlPmfmRowModel> PARAMETER = DaliColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_PARAMETER,
			n("dali.property.pmfm.parameter"),
			n("dali.property.pmfm.parameter"),
			ParameterDTO.class);
    
	/** Constant <code>UNIT</code> */
	public static final DaliColumnIdentifier<ControlPmfmRowModel> UNIT = DaliColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_UNIT,
			n("dali.property.pmfm.unit"),
			n("dali.property.pmfm.unit"),
			UnitDTO.class);

	/**
	 * <p>Constructor for ControlPmfmTableModel.</p>
	 *
	 */
	public ControlPmfmTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ControlPmfmRowModel createNewRow() {
		return new ControlPmfmRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ControlPmfmRowModel> getFirstColumnEditing() {
		return null;
	}
}
