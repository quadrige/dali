package fr.ifremer.dali.ui.swing.content.manage.program.pmfms;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.UnitDTO;
import fr.ifremer.dali.dto.referential.pmfm.*;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.util.Optional;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class PmfmsTableModel extends AbstractDaliTableModel<PmfmsTableRowModel> {

    public static final DaliColumnIdentifier<PmfmsTableRowModel> PMFM_ID = DaliColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_ID,
            n("dali.property.pmfm.id"),
            n("dali.property.pmfm.id"),
            Integer.class);

    /**
     * Identifiant pour la colonne libelle parametre.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> PMFM_NAME = DaliColumnIdentifier.newPmfmNameId(
            PmfmsTableRowModel.PROPERTY_PMFM,
            n("dali.property.pmfm.name"),
            n("dali.program.pmfm.name.tip"));

    /**
     * Identifiant pour la colonne code parametre.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> PARAMETER_CODE = DaliColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_PARAMETER,
            n("dali.property.pmfm.parameter.code"),
            n("dali.program.pmfm.parameter.code.tip"),
            ParameterDTO.class);

    /**
     * Identifiant pour la colonne support.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> MATRIX = DaliColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_MATRIX,
            n("dali.property.pmfm.matrix"),
            n("dali.program.pmfm.matrix"),
            MatrixDTO.class);

    /**
     * Identifiant pour la colonne fraction.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> FRACTION = DaliColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_FRACTION,
            n("dali.property.pmfm.fraction"),
            n("dali.program.pmfm.fraction.tip"),
            FractionDTO.class);

    /**
     * Identifiant pour la colonne methode.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> METHOD = DaliColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_METHOD,
            n("dali.property.pmfm.method"),
            n("dali.program.pmfm.method.tip"),
            MethodDTO.class);

    /**
     * Identifiant pour la colonne unite.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> UNIT = DaliColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_UNIT,
            n("dali.property.pmfm.unit"),
            n("dali.program.pmfm.unit.tip"),
            UnitDTO.class);

    /**
     * Identifiant pour la colonne passage.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> SURVEY = DaliColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_SURVEY,
            n("dali.program.pmfm.survey.short"),
            n("dali.program.pmfm.survey.tip"),
            Boolean.class);

    /**
     * Identifiant pour la colonne prelevement.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> SAMPLING = DaliColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_SAMPLING,
            n("dali.program.pmfm.samplingOperation.short"),
            n("dali.program.pmfm.samplingOperation.tip"),
            Boolean.class);

    /**
     * Identifiant pour la colonne regroupement.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> GROUPING = DaliColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_GROUPING,
            n("dali.program.pmfm.grouping.short"),
            n("dali.program.pmfm.grouping.tip"),
            Boolean.class);

    /**
     * Identifiant pour la colonne unicite.
     */
    public static final DaliColumnIdentifier<PmfmsTableRowModel> UNIQUE = DaliColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_UNIQUE,
            n("dali.program.pmfm.unique.short"),
            n("dali.program.pmfm.unique.tip"),
            Boolean.class);

    public static final DaliColumnIdentifier<PmfmsTableRowModel> QUALITATIVE_VALUES = DaliColumnIdentifier.newId(
        PmfmsTableRowModel.PROPERTY_QUALITATIVE_VALUES,
        n("dali.property.pmfm.parameter.associatedQualitativeValue"),
        n("dali.property.pmfm.parameter.associatedQualitativeValue"),
        QualitativeValueDTO.class,
        DecoratorService.COLLECTION_SIZE);

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public PmfmsTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmsTableRowModel createNewRow() {
        return new PmfmsTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<PmfmsTableRowModel> getFirstColumnEditing() {
        return SURVEY;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, ColumnIdentifier<PmfmsTableRowModel> propertyName) {
        if (propertyName.equals(QUALITATIVE_VALUES)) {
            PmfmsTableRowModel row = getEntry(rowIndex);
            return Optional.ofNullable(row.getPmfm()).map(PmfmDTO::getParameter).map(ParameterDTO::isQualitative).orElse(false);
        } else {
            return super.isCellEditable(rowIndex, columnIndex, propertyName);
        }
    }
}
