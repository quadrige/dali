package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.dali.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.content.observation.ObservationUIHandler;
import fr.ifremer.dali.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.OperationMeasurementsTabUIModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsFilter;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;

import java.util.Collection;
import java.util.List;

/**
 * Modele pour les mesures des prelevements (ecran prelevements/mesure).
 */
public abstract class AbstractOperationMeasurementsGroupedTableUIModel<
        B extends MeasurementDTO,
        R extends OperationMeasurementsGroupedRowModel,
        M extends AbstractOperationMeasurementsGroupedTableUIModel<B, R, M>
    >
    extends AbstractDaliTableUIModel<B, R, M> {

    public static final String PROPERTY_SURVEY = "survey";
    public static final String PROPERTY_MEASUREMENT_FILTER = "measurementFilter";
    // property identifier used to propagate row sampling operation property change to parent
    public static final String PROPERTY_SAMPLING_OPERATION = "samplingOperation";
    public static final String PROPERTY_ADJUSTING = "adjusting";
    public static final String PROPERTY_MEASUREMENTS_LOADED = "measurementsLoaded";

    private OperationMeasurementsTabUIModel parentModel;
    private ObservationUIModel survey;
    private ObservationUIHandler observationHandler;
    private OperationMeasurementsFilter measurementFilter;
    private boolean measurementsLoaded;
    // pmfms with unique constraint
    private List<PmfmDTO> uniquePmfms;
    // map holding preconditions per pmfm id
    private Multimap<Integer, PreconditionRuleDTO> preconditionRulesByPmfmIdMap;
    private boolean adjusting;

    /**
     * Constructor.
     */
    public AbstractOperationMeasurementsGroupedTableUIModel() {
        super();
    }

    public OperationMeasurementsTabUIModel getParentModel() {
        return parentModel;
    }

    public void setParentModel(OperationMeasurementsTabUIModel parentModel) {
        this.parentModel = parentModel;
    }

    /**
     * <p>Getter for the field <code>survey</code>.</p>
     *
     * @return a {@link ObservationUIModel} object.
     */
    public ObservationUIModel getSurvey() {
        return survey;
    }

    /**
     * <p>Setter for the field <code>survey</code>.</p>
     *
     * @param survey a {@link ObservationUIModel} object.
     */
    public void setSurvey(ObservationUIModel survey) {
        this.survey = survey;
        firePropertyChange(PROPERTY_SURVEY, null, survey);
    }

    public ObservationUIHandler getObservationUIHandler() {
        return observationHandler;
    }

    public void setObservationHandler(ObservationUIHandler observationHandler) {
        this.observationHandler = observationHandler;
    }

    /**
     * <p>getSamplingOperations.</p>
     *
     * @return a {@link List} object.
     */
    public List<SamplingOperationDTO> getSamplingOperations() {
        return survey == null ? null : (List<SamplingOperationDTO>) survey.getSamplingOperations();
    }

    /**
     * <p>Setter for the field <code>measurementFilter</code>.</p>
     *
     * @param measurementFilter a {@link OperationMeasurementsFilter} object.
     */
    public void setMeasurementFilter(OperationMeasurementsFilter measurementFilter) {
        this.measurementFilter = measurementFilter;
        firePropertyChange(PROPERTY_MEASUREMENT_FILTER, null, measurementFilter);
    }

    /**
     * <p>getTaxonGroupFilter.</p>
     *
     * @return a {@link TaxonGroupDTO} object.
     */
    public TaxonGroupDTO getTaxonGroupFilter() {
        return measurementFilter == null ? null : measurementFilter.getTaxonGroup();
    }

    /**
     * <p>getTaxonFilter.</p>
     *
     * @return a {@link TaxonDTO} object.
     */
    public TaxonDTO getTaxonFilter() {
        return measurementFilter == null ? null : measurementFilter.getTaxon();
    }

    /**
     * <p>getSamplingFilter.</p>
     *
     * @return a {@link SamplingOperationDTO} object.
     */
    public SamplingOperationDTO getSamplingFilter() {
        return measurementFilter == null ? null : measurementFilter.getSamplingOperation();
    }

    /**
     * <p>Getter for the field <code>uniquePmfms</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<PmfmDTO> getUniquePmfms() {
        return uniquePmfms;
    }

    /**
     * <p>Setter for the field <code>uniquePmfms</code>.</p>
     *
     * @param uniquePmfms a {@link List} object.
     */
    public void setUniquePmfms(List<PmfmDTO> uniquePmfms) {
        this.uniquePmfms = uniquePmfms;
    }

    public Multimap<Integer, PreconditionRuleDTO> getPreconditionRulesByPmfmIdMap() {
        if (preconditionRulesByPmfmIdMap == null) preconditionRulesByPmfmIdMap = ArrayListMultimap.create();
        return preconditionRulesByPmfmIdMap;
    }

    public void addPreconditionRuleByPmfmId(Integer pmfmId, PreconditionRuleDTO precondition) {
        getPreconditionRulesByPmfmIdMap().put(pmfmId, precondition);
    }

    public boolean isPmfmIdHasPreconditions(int pmfmId) {
        return getPreconditionRulesByPmfmIdMap().containsKey(pmfmId);
    }

    public Collection<PreconditionRuleDTO> getPreconditionRulesByPmfmId(int pmfmId) {
        return getPreconditionRulesByPmfmIdMap().get(pmfmId);
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    public boolean isMeasurementsLoaded() {
        return measurementsLoaded;
    }

    public void setMeasurementsLoaded(boolean measurementsLoaded) {
        this.measurementsLoaded = measurementsLoaded;
        firePropertyChange(PROPERTY_MEASUREMENTS_LOADED, null, measurementsLoaded);
    }

}
