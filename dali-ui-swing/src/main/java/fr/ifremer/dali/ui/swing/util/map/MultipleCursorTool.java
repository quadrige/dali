package fr.ifremer.dali.ui.swing.util.map;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.map.Maps;
import fr.ifremer.dali.ui.swing.util.map.layer.DataFeatureLayer;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Extends default ScrollWheelTool to add pan capabilities from PanTool
 *
 * @author peck7 on 02/06/2017.
 */
public class MultipleCursorTool extends CursorTool {

    private Cursor cursor;
    private boolean panning;

    private Point startPointInScreen;
    private Point panningPointInScreen;
    private Point2D startPointInWorld;
    private ReferencedEnvelope startDisplayArea;

    private static final Log log = LogFactory.getLog(MultipleCursorTool.class);

    public MultipleCursorTool(DataMapPane mapPane) {
        setMapPane(mapPane);
        cursor = ApplicationUIUtil.getCustomCursor("select");
        panning = false;
    }

    @Override
    public DataMapPane getMapPane() {
        return (DataMapPane) super.getMapPane();
    }

    /**
     * Respond to a mouse button click.
     * Select the underlying feature
     *
     * @param ev event
     */
    @Override
    public void onMouseClicked(MapMouseEvent ev) {

        // Create a envelope by 10x10 pixels of screen area
        ReferencedEnvelope envelope = ev.getEnvelopeByPixels(10);

        /*
         * Create a Filter to select features that intersect with
         * the bounding box
         */
        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(null);

        // Build feature list across all visible data layers
        Set<DataFeatureLayer> selectedDataLayers = new HashSet<>();

        try {
            for (Layer layer : getMapPane().getMapContent().layers()) {
                if (layer instanceof DataFeatureLayer && layer.isVisible()) {
                    DataFeatureLayer dataLayer = (DataFeatureLayer) layer;
                    String geometryAttributeName = layer.getFeatureSource().getSchema().getGeometryDescriptor().getLocalName();
                    Filter filter = ff.intersects(
                            ff.property(geometryAttributeName),
                            ff.literal(
                                    Maps.transformReferencedEnvelope(
                                            envelope,
                                            dataLayer.getBounds().getCoordinateReferenceSystem())
                            )
                    );

                    // Use the filter to identify the selected features
                    SimpleFeatureCollection selectedFeatureCollection = dataLayer.getSimpleFeatureSource().getFeatures(filter);

                    Set<FeatureId> IDs = new HashSet<>();

                    try (SimpleFeatureIterator iterator = selectedFeatureCollection.features()) {
                        while (iterator.hasNext()) {
                            SimpleFeature feature = iterator.next();
                            IDs.add(feature.getIdentifier());
                            if (log.isDebugEnabled()) {
                                log.debug(" feature selected: " + feature.getIdentifier());
                            }
                        }

                    }

                    if (!IDs.isEmpty()) {

                        // This data layer is selected
                        selectedDataLayers.add(dataLayer);
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("no feature selected");
                        }
                    }
                }
            }
        } catch (IOException ex) {
            log.error("Unable to retrieve selected features", ex);
        }

        if (!selectedDataLayers.isEmpty()) {

            // Produce an event
            DataSelectionEvent event = new DataSelectionEvent(getMapPane(), DataSelectionEvent.Type.DATA_LAYER_SELECTED, selectedDataLayers, ev.getPoint(), ev.getWorldPos());
            getMapPane().publishSelectionEvent(event);

        } else {

            // Empty selection event
            DataSelectionEvent event = new DataSelectionEvent(getMapPane(), DataSelectionEvent.Type.EMPTY_SELECTION);
            getMapPane().publishSelectionEvent(event);

        }
    }

    /**
     * Respond to a mouse button press event from the map mapPane. This may
     * signal the start of a mouse drag. Records the event's window position.
     *
     * @param ev the mouse event
     */
    @Override
    public void onMousePressed(MapMouseEvent ev) {
        startDisplayArea = getMapPane().getDisplayArea();
        startPointInWorld = ev.getWorldPos();
        startPointInScreen = panningPointInScreen = ev.getPoint();
    }

    /**
     * Respond to a mouse dragged event. Calls {@link org.geotools.swing.MapPane#moveImage(int, int)}
     *
     * @param ev the mouse event
     */
    @Override
    public void onMouseDragged(MapMouseEvent ev) {

        // Don't process translation if delta < 2 pixels
        if (getMapPane().isBusy() || ev.getPoint().distance(startPointInScreen) < 2) return;

        panning = true;

        // move image
        Point point = ev.getPoint();
        if (!point.equals(panningPointInScreen)) {
            getMapPane().moveImage(point.x - panningPointInScreen.x, point.y - panningPointInScreen.y);
            panningPointInScreen = point;
        }
    }

    /**
     * If this button release is the end of a mouse dragged event, requests the
     * map mapPane to repaint the display
     *
     * @param ev the mouse event
     */
    @Override
    public void onMouseReleased(MapMouseEvent ev) {

        if (!panning) return;

        panning = false;
        Point2D endPointInWorld = ev.getWorldPos();
        double transX = startPointInWorld.getX() - endPointInWorld.getX();
        double transY = startPointInWorld.getY() - endPointInWorld.getY();

        ReferencedEnvelope endDisplayArea = new ReferencedEnvelope(startDisplayArea);

        endDisplayArea.translate(transX, transY);

        getMapPane().setDisplayArea(endDisplayArea);

        if (log.isDebugEnabled()) {
            log.debug(String.format("Translate (x : %s, y : %s)", transX, transY));
        }
    }

    /**
     * Respond to wheel action (will zoom in or out)
     * <p>
     * If allowed by the mapPane, a buffered image (interstitial) will be drawn
     *
     * @param ev event
     */
    @Override
    public void onMouseWheelMoved(MapMouseEvent ev) {

        int notches = ev.getWheelAmount();
        double zoomRatio = 1 - (DataMapPane.ZOOM_STEP_RATIO * notches);
        Point zoomCenter = ev.getPoint();

        if (zoomRatio != 1) {

            // Apply zoom
            getMapPane().applyZoom(zoomRatio, zoomCenter);

        }
    }

    /**
     * Returns false to indicate that this tool does not draw a box
     * on the map display when the mouse is being dragged
     */
    @Override
    public boolean drawDragBox() {
        return false;
    }

    @Override
    public Cursor getCursor() {
        return cursor;
    }

}
