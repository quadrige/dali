package fr.ifremer.dali.ui.swing.util.tab;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.quadrige3.ui.swing.tab.AbstractTabContainerUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * UI containing a tab panel.
 * Use DaliTab as tab component
 *
 * @param <M>  type of the ui model
 * @param <UI> type of the screen ui
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public abstract class AbstractDaliTabContainerUIHandler<M, UI extends DaliUI<M, ?>>
        extends AbstractTabContainerUIHandler<M, UI> {

    @Override
    public DaliConfiguration getConfig() {
        return (DaliConfiguration) super.getConfig();
    }

    @Override
    public DaliUIContext getContext() {
        return (DaliUIContext) super.getContext();
    }

    /**
     * get the screen title
     *
     * @return the screen title
     */
    public String getTitle() {
        return t("dali.screen." + getUI().getClass().getSimpleName() + ".title");
    }

}
