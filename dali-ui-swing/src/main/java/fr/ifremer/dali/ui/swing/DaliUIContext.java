package fr.ifremer.dali.ui.swing;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.config.DaliConfigurationOption;
import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.context.ContextDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.data.survey.SurveyFilterDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.service.DaliDataContext;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.service.administration.campaign.CampaignService;
import fr.ifremer.dali.service.administration.context.ContextService;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.dali.service.administration.user.UserService;
import fr.ifremer.dali.service.control.ControlRuleService;
import fr.ifremer.dali.service.control.RuleListService;
import fr.ifremer.dali.service.extraction.ExtractionPerformService;
import fr.ifremer.dali.service.extraction.ExtractionService;
import fr.ifremer.dali.service.observation.ObservationService;
import fr.ifremer.dali.service.persistence.PersistenceService;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.dali.service.system.SystemService;
import fr.ifremer.dali.ui.swing.action.ImportReferentialSynchroAction;
import fr.ifremer.dali.ui.swing.content.DaliMainUI;
import fr.ifremer.dali.ui.swing.content.config.DaliConfigUI;
import fr.ifremer.dali.ui.swing.content.extraction.ExtractionUI;
import fr.ifremer.dali.ui.swing.content.home.HomeUI;
import fr.ifremer.dali.ui.swing.content.manage.campaign.CampaignsUI;
import fr.ifremer.dali.ui.swing.content.manage.context.ManageContextsUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.campaign.FilterCampaignUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.department.FilterDepartmentUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.equipment.FilterEquipmentUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.instrument.FilterInstrumentUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.location.FilterLocationUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.pmfm.FilterPmfmUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.program.FilterProgramUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.taxon.FilterTaxonUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.taxongroup.FilterTaxonGroupUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.user.FilterUserUI;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.dali.ui.swing.content.manage.program.strategiesByLocation.StrategiesLieuxUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.analysisinstruments.ReferentialAnalysisInstrumentsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.department.ManageDepartmentsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.location.ManageLocationUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.ManagePmfmsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.ManageFractionsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.ManageMatricesUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.ManageMethodsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.parameter.ManageParametersUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.samplingequipment.ManageSamplingEquipmentsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.taxon.ManageTaxonsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.taxongroup.ManageTaxonGroupUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.unit.ReferentialUnitsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.user.ManageUsersUI;
import fr.ifremer.dali.ui.swing.content.manage.rule.RulesUI;
import fr.ifremer.dali.ui.swing.content.observation.ObservationUI;
import fr.ifremer.dali.ui.swing.content.welcome.WelcomeUI;
import fr.ifremer.dali.ui.swing.util.tab.DaliTabIndexes;
import fr.ifremer.dali.ui.swing.util.table.state.DaliTableSessionState;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.QuadrigeUserDetails;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.synchro.service.client.SynchroHistoryService;
import fr.ifremer.quadrige3.ui.swing.*;
import fr.ifremer.quadrige3.ui.swing.component.OverlayIcon;
import fr.ifremer.quadrige3.ui.swing.content.db.DbManagerUI;
import fr.ifremer.quadrige3.ui.swing.model.BeanPropertyChangeListener;
import fr.ifremer.quadrige3.ui.swing.model.ProgressionUIModel;
import fr.ifremer.quadrige3.ui.swing.synchro.log.SynchroLogUI;
import fr.ifremer.quadrige3.ui.swing.table.FixedSwingTable;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.state.FixedSwingTableSessionState;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanDoubleList;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.swing.help.JAXXHelpBroker;
import jaxx.runtime.swing.help.JAXXHelpUIHandler;
import jaxx.runtime.swing.session.BeanDoubleListState;
import jaxx.runtime.swing.session.BeanFilterableComboBoxState;
import jaxx.runtime.swing.session.State;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.listener.PropagatePropertyChangeListener;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;
import org.springframework.security.core.GrantedAuthority;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>DaliUIContext class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class DaliUIContext extends ApplicationUIContext implements JAXXHelpUIHandler, BeanPropertyChangeListener {

    /**
     * Constant <code>PROPERTY_VALIDATION_CONTEXT="validationContext"</code>
     */
    public static final String PROPERTY_VALIDATION_CONTEXT = "validationContext";
    /**
     * Constant <code>PROPERTY_LAST_OBSERVATION_ID="lastObservationId"</code>
     */
    public static final String PROPERTY_LAST_OBSERVATION_ID = "lastObservationId";
    /**
     * Constant <code>PROPERTY_SELECTED_CONTEXT_ID="selectedContextId"</code>
     */
    public static final String PROPERTY_SELECTED_CONTEXT_ID = "selectedContextId";
    /**
     * Constant <code>PROPERTY_AUTHENTICATION_LABEL="authenticationLabel"</code>
     */
    public static final String PROPERTY_AUTHENTICATION_LABEL = "authenticationLabel";
    /**
     * Constant <code>PROPERTY_AUTHENTICATION_TOOLTIPTEXT="authenticationToolTipText"</code>
     */
    public static final String PROPERTY_AUTHENTICATION_TOOLTIPTEXT = "authenticationToolTipText";
    /**
     * Constant <code>PROPERTY_SELECTED_SURVEY_ID="selectedSurveyId"</code>
     */
    public static final String PROPERTY_SELECTED_SURVEY_ID = "selectedSurveyId";
    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(DaliUIContext.class);

    /**
     * Message notifiers.
     *
     * @since 1.0
     */
    protected final Set<UIMessageNotifier> messageNotifiers;
    /**
     * Shared data context.
     */
    protected final DaliDataContext dataContext;
    /**
     * Map used to cache icons
     */
    private final Map<String, Icon> objectStatusIconMap = Maps.newHashMap();
    /**
     * Dali help broker.
     *
     * @since 1.0
     */
    protected DaliHelpBroker helpBroker;
    private Properties helpMapping;
    /**
     * Validation context (used by fishingOperation screens).
     *
     * @since 1.0
     */
    private String validationContext;
    /**
     * Les parametres de recherche pour l'acceuil
     */
    private SurveyFilterDTO surveyFilter;

    /**
     * Authentication information
     */
    private String authenticationLabel;
    private String authenticationToolTipText;

    /**
     * The selected survey id in home
     */
    private Integer selectedSurveyId;
    /**
     * Identifiant du prelevement selectionne.
     */
    private Integer selectedSamplingOperationId;
    /**
     * Identifiant du programme selectionne dans la configuration des programmes et strategie.
     */
    private String selectProgramCode;
    /**
     * Identifiant du lieu selectionne dans la configuration des programmes et strategie.
     */
    private Integer selectedLocationId;
    /**
     * Szlected context.
     */
    private Integer selectedContextId;

    private boolean preventNextImportSynchroCheckAction;

    /**
     * Constructor.
     *
     * @param config Configuration
     */
    protected DaliUIContext(DaliConfiguration config) {
        super(config);

        Map<Class, State> additionalStates = Maps.newHashMap();
        additionalStates.put(BeanFilterableComboBox.class, new BeanFilterableComboBoxState());
        additionalStates.put(BeanDoubleList.class, new BeanDoubleListState());
        additionalStates.put(SwingTable.class, new DaliTableSessionState());
        // Add also state for FixedSwingTable (Mantis #51311)
        additionalStates.put(FixedSwingTable.class, new FixedSwingTableSessionState());
        setSwingSession(SwingSession.newSwingSession(config.getUIConfigFile(), false, additionalStates));
        this.dataContext = DaliServiceLocator.instance().getDataContext();
        PropagatePropertyChangeListener.listenAndPropagateAll(dataContext, this);
        UIMessageNotifier logMessageNotifier = message -> {
            if (StringUtils.isNotBlank(message) && LOG.isDebugEnabled()) {
                LOG.debug(ApplicationUIUtil.removeHtmlTags(message));
            }
        };
        this.messageNotifiers = Sets.newHashSet();
        addMessageNotifier(logMessageNotifier);

        PROPERTIES_TO_SAVE.add(PROPERTY_SELECTED_CONTEXT_ID);
        PROPERTIES_TO_SAVE.add(PROPERTY_LAST_OBSERVATION_ID);

    }

    /**
     * <p>newContext.</p>
     *
     * @param config a {@link DaliConfiguration} object.
     * @return a {@link DaliUIContext} object.
     */
    public static DaliUIContext newContext(DaliConfiguration config) {
        Assert.notNull(config);
        Assert.state(getInstance() == null, "Application context was already opened!");
        ApplicationUIContext.setInstance(new DaliUIContext(config));
        return (DaliUIContext) ApplicationUIContext.getInstance();
    }

    /**
     * <p>Getter for the field <code>validationContext</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValidationContext() {
        return validationContext;
    }

    /**
     * <p>Setter for the field <code>validationContext</code>.</p>
     *
     * @param validationContext a {@link java.lang.String} object.
     */
    public void setValidationContext(String validationContext) {
        Object oldValue = getValidationContext();
        this.validationContext = validationContext;
        firePropertyChange(PROPERTY_VALIDATION_CONTEXT, oldValue, validationContext);
    }

    // ------------------------------------------------------------------------//
    // -- Open / close methods --//
    // ------------------------------------------------------------------------//

    /**
     * <p>init.</p>
     *
     * @param i18nBundleName i18n bundle name
     */
    @Override
    public void init(String i18nBundleName) {

        super.init(i18nBundleName);

        // --------------------------------------------------------------------//
        // init help
        // --------------------------------------------------------------------//
        File helpDirectory = getConfiguration().getHelpDirectory();

        if (!getConfiguration().isFullLaunchMode() && (helpDirectory == null || !helpDirectory.exists())) {
            helpDirectory = new File(getConfiguration().getDataDirectory(), "help");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("Help directory: " + helpDirectory);
        }
        ApplicationIOUtil.forceMkdir(helpDirectory, t("dali.help.mkDir.error", helpDirectory));

        // load help mapping
        String mappingProperties = "/dali-help-" + getLocale().getLanguage() + ".properties"; // TODO what happens if locale changes
        try {

            InputStream resourceAsStream = getClass().getResourceAsStream(mappingProperties);
            helpMapping = new Properties();
            helpMapping.load(resourceAsStream);

        } catch (Exception eee) {
            LOG.error("Failed to load help mapping file at '" + mappingProperties + "'", eee);
        }
        if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Starts help with locale [%s] at [%s]", getLocale(), helpDirectory));
        }

        initAuthentication();

        // check database
        checkDbExists();

    }

    private void initAuthentication() {

        // reset authentication user
        setAuthenticated(false);

    }

    /**
     * Do some auto save actions at application close (Mantis #52386)
     */
    @Override
    public void close() {

        // Don't try to close if already closed
        if (isClosed()) {
            return;
        }

        // Save context and extractions only if persistence is loaded (Mantis #52727)
        // and if user is authenticated
        if (isPersistenceLoaded() && isAuthenticated()) {

            if (LOG.isInfoEnabled()) {
                LOG.info("Saving Contexts and Filters ...");
            }

            {
                // Load all context
                List<ContextDTO> contexts = getContextService().getAllContexts();
                // create new filename
                File exportFile = new File(getConfiguration().getContextDirectory(), String.format(ContextService.EXPORT_FILE_FORMAT, "autosave"));
                // Export context
                getContextService().exportContexts(contexts, exportFile);
                if (LOG.isInfoEnabled()) {
                    LOG.info(String.format("Auto-save all contexts in %s", exportFile.getAbsolutePath()));
                }
            }

            {
                // Load all extractions (with filters and configs)
                List<ExtractionDTO> extractions = getExtractionService().getExtractions(null, null);
                File extractionDirectory = getConfiguration().getExtractionDirectory();
                extractions.forEach(extraction -> {
                    File exportFile = new File(
                        extractionDirectory,
                        String.format("%s-%s-autosave.%s", t("dali.service.extraction.file.prefix"), extraction.getName(), getConfiguration().getExtractionFileExtension())
                    );
                    getExtractionService().exportExtraction(extraction, exportFile);
                    if (LOG.isInfoEnabled()) {
                        LOG.info(String.format("Auto-save extraction in %s", exportFile.getAbsolutePath()));
                    }
                });
            }
        }

        super.close();
    }

    // ------------------------------------------------------------------------//
    // -- Services methods --//
    // ------------------------------------------------------------------------//

    /**
     * <p>Getter for the field <code>decoratorService</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.decorator.DecoratorService} object.
     */
    public DecoratorService getDecoratorService() {
        return dataContext.getDecoratorService();
    }

    /**
     * <p>getSystemService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.system.SystemService} object.
     */
    public SystemService getSystemService() {
        return dataContext.getSystemService();
    }

    /**
     * <p>getReferentialService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.referential.ReferentialService} object.
     */
    public ReferentialService getReferentialService() {
        return dataContext.getReferentialService();
    }

    /**
     * <p>getObservationService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.observation.ObservationService} object.
     */
    public ObservationService getObservationService() {
        return dataContext.getObservationService();
    }

    /**
     * <p>getExtractionService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.extraction.ExtractionService} object.
     */
    public ExtractionService getExtractionService() {
        return dataContext.getExtractionService();
    }

    /**
     * <p>getExtractionPerformService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.extraction.ExtractionPerformService} object.
     */
    public ExtractionPerformService getExtractionPerformService() {
        return dataContext.getExtractionPerformService();
    }

    /**
     * <p>getPersistenceService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.persistence.PersistenceService} object.
     */
    public PersistenceService getPersistenceService() {
        return dataContext.getPersistenceService();
    }

    /**
     * <p>getUserService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.user.UserService} object.
     */
    public UserService getUserService() {
        return dataContext.getUserService();
    }

    /**
     * <p>getSynchroHistoryService.</p>
     *
     * @return a {@link fr.ifremer.quadrige3.synchro.service.client.SynchroHistoryService} object.
     */
    public SynchroHistoryService getSynchroHistoryService() {
        return dataContext.getSynchroHistoryService();
    }

    /**
     * <p>getProgramStrategyService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.program.ProgramStrategyService} object.
     */
    public ProgramStrategyService getProgramStrategyService() {
        return dataContext.getProgramStrategyService();
    }

    /**
     * <p>getContextService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.context.ContextService} object.
     */
    public ContextService getContextService() {
        return dataContext.getContextService();
    }

    /**
     * <p>getRulesControlService.</p>
     *
     * @return a {@link ControlRuleService} object.
     */
    public ControlRuleService getRulesControlService() {
        return dataContext.getRulesControlService();
    }

    public RuleListService getRuleListService() {
        return dataContext.getRuleListService();
    }

    public CampaignService getCampaignService() {
        return dataContext.getCampaignService();
    }

    @Override
    protected boolean doUpdates() {
        boolean needRestart = super.doUpdates();

        // check dali.persistence.db.timezone option
        if (StringUtils.isBlank(getConfiguration().getApplicationConfig().getOption(DaliConfigurationOption.DB_TIMEZONE.getKey()))) {
            getDialogHelper().showWarningDialog(t("dali.config.option.dbTimezone.missing"), t("quadrige3.error.business.warning"));
        }

        return needRestart;
    }

    // ------------------------------------------------------------------------//
    // -- Db methods --//
    // ------------------------------------------------------------------------//
    private void checkDbExists() {

        // TODO EIS remove this test on 'dali.persistence.enable' (when mock is removed)
        String enablePersistenceProperty = System.getProperty("dali.persistence.enable");
        boolean enablePersistence = !"false".equals(enablePersistenceProperty);
        if (enablePersistence) {
            setDbExist(getConfiguration().isDbExists());

            if (!isDbExist()) {
                setPersistenceLoaded(false);
            }
        } else {

            // mock is used, so simulate db is open
            setDbExist(true);
            setPersistenceLoaded(true);
        }
    }

    /**
     * <p>closePersistenceService.</p>
     */
    public void closePersistenceService() {
        closePersistenceService(false, false);
    }

    /**
     * Close services and database
     *
     * @param compact                      if true, the database is compact
     * @param keepAuthenticationProperties if true, the authentication is kept if possible
     */
    public void closePersistenceService(boolean compact, boolean keepAuthenticationProperties) {

        if (isPersistenceLoaded() && compact) {
            getPersistenceService().compactDatabaseOnClose();
        }

        // close Sprint context
        if (LOG.isDebugEnabled()) {
            LOG.debug("closing Spring context");
        }
        IOUtils.closeQuietly(DaliServiceLocator.instance());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Spring context closed");
        }

        // DB is unloaded
        setPersistenceLoaded(false);

        // clear db context
        if (keepAuthenticationProperties) {
            clearDbContext(true);
        } else {
            clearDbContext(false);
        }

    }

    /**
     * <p>openPersistenceService.</p>
     *
     * @param clearCache a boolean.
     */
    public void openPersistenceService(boolean clearCache) {

        // open and load the persistence layer
        getPersistenceService();

        // mark the persistence layer as loaded
        setPersistenceLoaded(true);

        // clear caches if forced OR if a post import action is needed
        if (clearCache || isDbJustInstalled() || isDbJustImportedFromFile()) {
            clearAllCaches();
        }

        // Try to re authenticate
        tryReAuthenticate();
    }

    /**
     * <p>checkDbContext.</p>
     *
     * @param progressionModel a {@link ProgressionUIModel} object.
     */
    public void checkDbContext(ProgressionUIModel progressionModel) {

        // Make post DB open tasks
        {
            // If just imported
            if (isDbJustImportedFromFile()) {

                // Reset flags
                setDbJustImportedFromFile(false);

                // Do tasks after imported/installed DB
                // example : denormalize some data...
            }

            // If just installed
            if (isDbJustInstalled()) {
                // Reset flags
                setDbJustInstalled(false);

                // Clear all caches
                clearAllCaches();
            }

        }

        // Load default caches
        try {
            getPersistenceService().loadDefaultCaches(progressionModel);
        } catch (Exception e) {
            // log but continue
            LOG.error("Something goes wrong with cache loading", e);
        }

        // save config
        save();

        // ------------------------------------------------------------------ //
        // --- Check referential updates                                      //
        // ------------------------------------------------------------------ //
        if (isSynchroEnabled()) {
            ImportReferentialSynchroAction importSynchroAction = getActionFactory().createLogicAction(getMainUI().getHandler(), ImportReferentialSynchroAction.class);
            importSynchroAction.setSilentIfNoUpdate(true);
            getActionEngine().runFullInternalAction(importSynchroAction);
        }
    }

    /**
     * Clear all caches
     */
    public void clearAllCaches() {

        if (isPersistenceLoaded()) {

            // clear DB cache
            getPersistenceService().clearAllCaches();

            // clear local cache
            dataContext.resetLocalCache();
        }
    }

    /**
     * <p>clearAuthenticationCache.</p>
     * <p>
     * update some cache or other things depending on authenticated user
     */
    protected void clearAuthenticationCache() {
    }

    /**
     * Will clean the context, but re-authenticate the user if already authenticated
     */
    @Override
    public void clearDbContext() {

        if (isPersistenceLoaded()) {
            tryReAuthenticate();
            clearDbContext(isAuthenticated());
        } else {
            clearDbContext(false);
        }
    }

    /**
     * Will clean the context (never attempt to authenticate, but can keep authentication properties)
     *
     * @param keepAuthentication a boolean.
     */
    protected void clearDbContext(boolean keepAuthentication) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Clear DB context (keep authentication=%1$s)", keepAuthentication));
        }
        if (!keepAuthentication || !isAuthenticated()) {
            dataContext.clearContext();
            // Force logged out, because the Person object could have changed in the PERSON table
            setAuthenticated(false);
        } else {
            dataContext.clearContextKeepRecorderPerson();
            // Force logged in, because the Person object could have changed in the PERSON table
            setAuthenticated(true);
        }
        setDbJustInstalled(false);
        setDbJustImportedFromFile(false);
    }

    /**
     * <p>getLastSurveyId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getLastObservationId() {
        return getConfiguration().getLastSurveyId();
    }

    /**
     * <p>setLastSurveyId.</p>
     *
     * @param lastObservationId a {@link java.lang.Integer} object.
     */
    public void setLastObservationId(Integer lastObservationId) {
        Integer oldValue = getLastObservationId();
        getConfiguration().setLastSurveyId(lastObservationId);
        firePropertyChange(PROPERTY_LAST_OBSERVATION_ID, oldValue, lastObservationId);
    }

    /**
     * <p>getObjectStatusIcon.</p>
     *
     * @param objectType a {@link java.lang.String} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return a {@link javax.swing.Icon} object.
     */
    public Icon getObjectStatusIcon(String objectType, String statusCode) {
        if (StringUtils.isBlank(objectType)) {
            return null;
        }

        Icon icon = objectStatusIconMap.get(statusCode);

        if (icon == null) {

            OverlayIcon newIcon = new OverlayIcon(SwingUtil.createActionIcon(objectType));
            newIcon.setOverlay((ImageIcon) getStatusOverlayIcon(statusCode));

            objectStatusIconMap.put(objectType + StringUtils.defaultString(statusCode), newIcon);
            return newIcon;
        }

        return icon;
    }

    private Icon getStatusOverlayIcon(String code) {
        String overlayName = null;
        if (getConfiguration().getEnableStatusCode().equals(code)) {
            overlayName = "overlay-enable";
        } else if (getConfiguration().getTemporaryStatusCode().equals(code)) {
            overlayName = "overlay-temporary";
        } else if (getConfiguration().getDisableStatusCode().equals(code)) {
            overlayName = "overlay-disable";
        } else if (getConfiguration().getDeletedStatusCode().equals(code)) {
            overlayName = "overlay-deleted";
        }
        if (overlayName != null) {
            return SwingUtil.createActionIcon(overlayName);
        }
        return null;
    }

    private Icon getSynchronizationStatusOverlayIcon(String code) {
        String overlayName = null;
        if (getConfiguration().getDirtySynchronizationStatusCode().equals(code)) {
            overlayName = "overlay-dirty";
        } else if (getConfiguration().getReadySynchronizationStatusCode().equals(code)) {
            overlayName = "overlay-waiting";
        } else if (getConfiguration().getSynchronizedSynchronizationStatusCode().equals(code)) {
            overlayName = "overlay-enable";
        }
        if (overlayName != null) {
            return SwingUtil.createActionIcon(overlayName);
        }
        return null;
    }

    // ------------------------------------------------------------------------//
    // -- DataContext methods --//
    // ------------------------------------------------------------------------//

    /**
     * <p>Getter for the field <code>dataContext</code>.</p>
     *
     * @return a {@link DaliDataContext} object.
     */
    public DaliDataContext getDataContext() {
        return dataContext;
    }

    // ------------------------------------------------------------------------//
    // -- Help methods --//
    // ------------------------------------------------------------------------//

    /**
     * {@inheritDoc}
     */
    @Override
    public void showHelp(final JAXXContext context, final JAXXHelpBroker broker, final String helpId) {

        String helpIdTemp = helpId;
        if (helpIdTemp == null) {
            helpIdTemp = broker.getDefaultID();
        }

        if (LOG.isInfoEnabled()) {
            LOG.info("show help " + helpIdTemp);
        }

        String value = (String) helpMapping.get(helpIdTemp);

        if (value == null) {
            throw new ApplicationBusinessException(t("dali.context.helpPage.notFound", helpIdTemp));
        }

        String helpDirectory = getConfiguration().getHelpResourceWithLocale(value);
        boolean withFragment = helpDirectory.contains("#");

        String fragment = null;
        if (withFragment) {
            helpDirectory = StringUtils.substringBefore(helpDirectory, "#");
            fragment = StringUtils.substringAfter(helpDirectory, "#");
        }

        URI resolvedUri = new File(helpDirectory).toURI();
        try {

            if (withFragment) {
                resolvedUri = new URI(resolvedUri.toString() + "#" + fragment);
            }
            if (LOG.isInfoEnabled()) {
                LOG.info("help uri = " + resolvedUri);
            }
            ApplicationUIUtil.openLink(resolvedUri);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new ApplicationBusinessException(t("dali.context.helpPage.notFound", resolvedUri));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void firePropertyChanged(String propertyName, Object oldValue, Object newValue) {
        firePropertyChange(propertyName, oldValue, newValue);
    }

    @Override
    public void fireIndexedPropertyChanged(String propertyName, int index, Object oldValue, Object newValue) {
        fireIndexedPropertyChange(propertyName, index, oldValue, newValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getI18nPrefix() {
        return "dali.property.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDateFormat() {
        return getConfiguration().getDateFormat();
    }

    //------------------------------------------------------------------------//
    //-- Authentication methods                                             --//
    //------------------------------------------------------------------------//

    /**
     * <p>Getter for the field <code>authenticationLabel</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @Override
    public final String getAuthenticationLabel() {
        return authenticationLabel;
    }

    /**
     * <p>Setter for the field <code>authenticationLabel</code>.</p>
     *
     * @param authenticationLabel a {@link java.lang.String} object.
     */
    public final void setAuthenticationLabel(String authenticationLabel) {
        this.authenticationLabel = authenticationLabel;
        firePropertyChange(PROPERTY_AUTHENTICATION_LABEL, null, authenticationLabel);
    }

    /**
     * <p>Getter for the field <code>authenticationToolTipText</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public final String getAuthenticationToolTipText() {
        return authenticationToolTipText;
    }

    /**
     * <p>Setter for the field <code>authenticationToolTipText</code>.</p>
     *
     * @param authenticationTooTipText a {@link java.lang.String} object.
     */
    public final void setAuthenticationToolTipText(String authenticationTooTipText) {
        this.authenticationToolTipText = authenticationTooTipText;
        firePropertyChange(PROPERTY_AUTHENTICATION_TOOLTIPTEXT, null, authenticationTooTipText);
    }

    /**
     * <p>Setter for the field <code>authenticated</code>.</p>
     *
     * @param authenticated a boolean.
     */
    @Override
    public final void setAuthenticated(boolean authenticated) {
        super.setAuthenticated(authenticated);
        if (authenticated) {
            QuadrigeUserDetails user = SecurityContextHelper.getQuadrigeUser();
            if (LOG.isInfoEnabled()) {
                LOG.info("the authenticated user is " + user);
            }
            dataContext.setRecorderPersonId(user.getUserId());
            setAuthenticationLabel(t("dali.status.authentication.label.user", user.getUsername()));

            // authorities
            String userToolTip = null;
            if (CollectionUtils.isNotEmpty(user.getAuthorities())) {
                List<String> authorities = Lists.newArrayList();
                for (GrantedAuthority authority : user.getAuthorities()) {
                    if (QuadrigeUserAuthority.LOCAL_ADMIN.equals(authority)) continue; // Exclude reefdb administrator
                    authorities.add(String.valueOf(authority));
                }
                userToolTip = ApplicationUIUtil.getHtmlString(t("dali.status.authentication.label.authorities"), authorities);
            }
            setAuthenticationToolTipText(userToolTip);

            // update some cache or other things depending on authenticated user
            clearAuthenticationCache();

        } else {
            SecurityContextHelper.clear();
            dataContext.setRecorderPersonId(null);
            setAuthenticationLabel(t("dali.status.authentication.label.none"));
            setAuthenticationToolTipText(null);
        }
    }

    /**
     * Say if the user is a local user
     *
     * @return a boolean.
     */
    public boolean isAuthenticatedAsLocalUser() {
        return isAuthenticated() && SecurityContextHelper.getQuadrigeUser().isLocal();
    }

    /**
     * Say if the current user has admin privilege
     *
     * @return a boolean.
     */
    public boolean isAuthenticatedAsAdmin() {
        return isAuthenticated() && SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.ADMIN);
    }

    // ------------------------------------------------------------------------//
    // -- UI methods --//
    // ------------------------------------------------------------------------//

    /**
     * Get the UI instance corresponding to the requested screen
     *
     * @param screen the requested screen
     * @return the UI instance
     */
    @Override
    public ApplicationUI<?, ?> getApplicationUI(Screen screen) {

        if (DaliScreen.HOME.equals(screen)) {
            return new WelcomeUI(getMainUI());

        } else if (DaliScreen.OBSERVATION.equals(screen)) {
            return new HomeUI(getMainUI());

        } else if (DaliScreen.MANAGE_DB.equals(screen)) {
            DbManagerUI dbManagerUI = new DbManagerUI(getMainUI());
            // Hide export all referential (Mantis #40201)
            dbManagerUI.getExportAllReferentialToFileButton().setVisible(false);
            return dbManagerUI;

        } else if (DaliScreen.OBSERVATION_GENERAL.equals(screen)) {
            return new ObservationUI(getMainUI(), DaliTabIndexes.OBSERVATION_GENERAL);

        } else if (DaliScreen.OPERATION_MEASUREMENTS.equals(screen)) {
            return new ObservationUI(getMainUI(), DaliTabIndexes.OPERATION_MEASUREMENTS);

        } else if (DaliScreen.PHOTOS.equals(screen)) {
            return new ObservationUI(getMainUI(), DaliTabIndexes.PHOTOS);

        } else if (DaliScreen.CONFIGURATION.equals(screen)) {
            return new DaliConfigUI(getMainUI());

        } else if (DaliScreen.CONTEXT.equals(screen)) {
            return new ManageContextsUI(getMainUI());

        } else if (DaliScreen.FILTER_LOCATION.equals(screen)) {
            return new FilterLocationUI(getMainUI());

        } else if (DaliScreen.FILTER_PROGRAM.equals(screen)) {
            return new FilterProgramUI(getMainUI());

        } else if (DaliScreen.FILTER_CAMPAIGN.equals(screen)) {
            return new FilterCampaignUI(getMainUI());

        } else if (DaliScreen.FILTER_DEPARTMENT.equals(screen)) {
            return new FilterDepartmentUI(getMainUI());

        } else if (DaliScreen.FILTER_ANALYSIS_INSTRUMENT.equals(screen)) {
            return new FilterInstrumentUI(getMainUI());

        } else if (DaliScreen.FILTER_SAMPLING_EQUIPMENT.equals(screen)) {
            return new FilterEquipmentUI(getMainUI());

        } else if (DaliScreen.FILTER_PMFM.equals(screen)) {
            return new FilterPmfmUI(getMainUI());

        } else if (DaliScreen.FILTER_TAXON.equals(screen)) {
            return new FilterTaxonUI(getMainUI());

        } else if (DaliScreen.FILTER_TAXON_GROUP.equals(screen)) {
            return new FilterTaxonGroupUI(getMainUI());

        } else if (DaliScreen.FILTER_USER.equals(screen)) {
            return new FilterUserUI(getMainUI());

        } else if (DaliScreen.LOCATION.equals(screen)) {
            return new ManageLocationUI(getMainUI());

        } else if (DaliScreen.STRATEGY_LOCATION.equals(screen)) {
            return new StrategiesLieuxUI(getMainUI());

        } else if (DaliScreen.PROGRAM.equals(screen)) {
            return new ProgramsUI(getMainUI());

        } else if (DaliScreen.CAMPAIGNS.equals(screen)) {
            return new CampaignsUI(getMainUI());

        } else if (DaliScreen.RULE_LIST.equals(screen)) {
            return new RulesUI(getMainUI());

        } else if (DaliScreen.TAXON_GROUP.equals(screen)) {
            return new ManageTaxonGroupUI(getMainUI());

        } else if (DaliScreen.TAXON.equals(screen)) {
            return new ManageTaxonsUI(getMainUI());

        } else if (DaliScreen.USER.equals(screen)) {
            return new ManageUsersUI(getMainUI());

        } else if (DaliScreen.DEPARTMENT.equals(screen)) {
            return new ManageDepartmentsUI(getMainUI());

        } else if (DaliScreen.PARAMETER.equals(screen)) {
            return new ManageParametersUI(getMainUI());

        } else if (DaliScreen.METHOD.equals(screen)) {
            return new ManageMethodsUI(getMainUI());

        } else if (DaliScreen.FRACTION.equals(screen)) {
            return new ManageFractionsUI(getMainUI());

        } else if (DaliScreen.MATRIX.equals(screen)) {
            return new ManageMatricesUI(getMainUI());

        } else if (DaliScreen.PMFM.equals(screen)) {
            return new ManagePmfmsUI(getMainUI());

        } else if (DaliScreen.UNIT.equals(screen)) {
            return new ReferentialUnitsUI(getMainUI());

        } else if (DaliScreen.SAMPLING_EQUIPMENT.equals(screen)) {
            return new ManageSamplingEquipmentsUI(getMainUI());

        } else if (DaliScreen.ANALYSIS_INSTRUMENT.equals(screen)) {
            return new ReferentialAnalysisInstrumentsUI(getMainUI());

        } else if (DaliScreen.SYNCHRO_LOG.equals(screen)) {
            return new SynchroLogUI(getMainUI());

        } else if (DaliScreen.EXTRACTION.equals(screen)) {
            return new ExtractionUI(getMainUI());

        }

        return null;
    }

    @Override
    public String getSelectedScreenTitle() {
        String title;

        if (isPersistenceLoaded()) {
            if (getSelectedContext() == null) {
                title = t("dali.main.title.noContext");
            } else {
                title = getSelectedContext().getName();
            }
        } else {

            // no db loaded
            title = t("dali.main.title.noDb");
        }
        return title;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DaliMainUI getMainUI() {
        return (DaliMainUI) super.getMainUI();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ApplicationActionUI getExistingActionUI() {
        while (getActionUI() == null) {

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                // ignore this one
            }
        }
        return getActionUI();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getBodyUI() {
        return getMainUI() == null ? null : getMainUI().getBody();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getStatusUI() {
        return getMainUI() == null ? null : getMainUI().getStatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Color getColorBlockingLayer() {
        return getConfiguration().getColorBlockingLayer();
    }

    // ------------------------------------------------------------------------//
    // -- UIMessageNotifier methods --//
    // ------------------------------------------------------------------------//

    /**
     * <p>addMessageNotifier.</p>
     *
     * @param messageNotifier a {@link UIMessageNotifier} object.
     */
    public final void addMessageNotifier(UIMessageNotifier messageNotifier) {
        this.messageNotifiers.add(messageNotifier);
    }

    /**
     * <p>removeMessageNotifier.</p>
     *
     * @param messageNotifier a {@link UIMessageNotifier} object.
     */
    public final void removeMessageNotifier(UIMessageNotifier messageNotifier) {
        this.messageNotifiers.remove(messageNotifier);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showInformationMessage(String message) {
        for (UIMessageNotifier messageNotifier : messageNotifiers) {
            messageNotifier.showInformationMessage(message);
        }
    }

    // ------------------------------------------------------------------------//
    // -- Config methods --//
    // ------------------------------------------------------------------------//

    /**
     * <p>Getter for the field <code>config</code>.</p>
     *
     * @return a {@link DaliConfiguration} object.
     */
    @Override
    public DaliConfiguration getConfiguration() {
        return (DaliConfiguration) super.getConfiguration();
    }

    /**
     * <p>Getter for the field <code>selectedSurveyId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSelectedSurveyId() {
        return selectedSurveyId;
    }

    /**
     * <p>Setter for the field <code>selectedSurveyId</code>.</p>
     *
     * @param selectedSurveyId a {@link java.lang.Integer} object.
     */
    public void setSelectedSurveyId(Integer selectedSurveyId) {
        this.selectedSurveyId = selectedSurveyId;
        firePropertyChange(PROPERTY_SELECTED_SURVEY_ID, null, selectedSurveyId);
    }

    /**
     * <p>Getter for the field <code>selectedSamplingOperationId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSelectedSamplingOperationId() {
        return selectedSamplingOperationId;
    }

    /**
     * <p>Setter for the field <code>selectedSamplingOperationId</code>.</p>
     *
     * @param selectedSamplingOperationId a {@link java.lang.Integer} object.
     */
    public void setSelectedSamplingOperationId(Integer selectedSamplingOperationId) {
        this.selectedSamplingOperationId = selectedSamplingOperationId;
    }

    /**
     * <p>Getter for the field <code>surveyFilter</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.data.survey.SurveyFilterDTO} object.
     */
    public SurveyFilterDTO getSurveyFilter() {
        return surveyFilter;
    }

    /**
     * <p>Setter for the field <code>surveyFilter</code>.</p>
     *
     * @param surveyFilter a {@link fr.ifremer.dali.dto.data.survey.SurveyFilterDTO} object.
     */
    public void setSurveyFilter(SurveyFilterDTO surveyFilter) {
        this.surveyFilter = surveyFilter;
    }

    /**
     * Clear observations & prelevements IDs.
     * <p/>
     * TODO vraiment utile ?
     */
    public void clearObservationIds() {
        setSelectedSurveyId(null);
        setSelectedSamplingOperationId(null);
        setSurveyFilter(null);
    }

    /**
     * <p>Getter for the field <code>selectProgramCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectProgramCode() {
        return selectProgramCode;
    }

    /**
     * <p>setSelectedProgramCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     */
    public void setSelectedProgramCode(String code) {
        this.selectProgramCode = code;
    }

    /**
     * <p>Getter for the field <code>selectedLocationId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSelectedLocationId() {
        return selectedLocationId;
    }

    /**
     * <p>Setter for the field <code>selectedLocationId</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setSelectedLocationId(Integer id) {
        this.selectedLocationId = id;
    }

    /**
     * Clear programme & strategie IDs.
     */
    public void clearProgrammeStrategieIds() {
        setSelectedProgramCode(null);
        setSelectedLocationId(null);
    }

    /**
     * <p>Getter for the field <code>selectedContextId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSelectedContextId() {
        return selectedContextId;
    }

    /**
     * <p>getSelectedContext.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    public ContextDTO getSelectedContext() {
        if (selectedContextId == null) {

            // load last context from configuration
            if (getConfiguration().getLastContextId() != null) {
                setSelectedContext(getContextService().getContext(getConfiguration().getLastContextId()));
            }
        }
        return dataContext.getContext();
    }

    /**
     * <p>setSelectedContext.</p>
     *
     * @param selectedContext a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    public void setSelectedContext(ContextDTO selectedContext) {
        Integer contextId = selectedContext == null ? null : selectedContext.getId();
        this.selectedContextId = contextId;

        // set to configuration to hold last value
        getConfiguration().setLastContextId(contextId);

        // set to data context (for service use)
        dataContext.setContext(selectedContext);
        firePropertyChange(PROPERTY_SELECTED_CONTEXT_ID, null, contextId);

        // Change title to reflect context change (Mantis #49551)
        getMainUI().getHandler().changeTitle();
    }

    /**
     * delete the content of synchro directory for all user
     */
    public void deleteAllSynchroContext() {

        File synchroDir = getConfiguration().getSynchronizationDirectory();
        if (synchroDir != null && synchroDir.exists()) {
            FileUtils.deleteQuietly(synchroDir);
        }
    }

    public void preventNextImportSynchroCheckAction() {
        this.preventNextImportSynchroCheckAction = true;
    }

    public boolean isNextImportSynchroCheckActionPrevented() {
        try {
            return preventNextImportSynchroCheckAction;
        } finally {
            preventNextImportSynchroCheckAction = false;
        }
    }

    public Permissions getPermissions(Collection<? extends SurveyDTO> surveys) {
        Permissions permissions = new Permissions();
        surveys = CollectionUtils.emptyIfNull(surveys);
        Set<ProgramDTO> selectedPrograms = surveys.stream()
            .map(SurveyDTO::getProgram)
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
        int userId = getDataContext().getRecorderPersonId();
        int departmentId = getDataContext().getRecorderDepartmentId();

        permissions.isManager = isAuthenticatedAsAdmin() || selectedPrograms.stream().allMatch(program -> DaliBeans.isProgramManager(program, userId, departmentId));
        permissions.isValidator = permissions.isManager || selectedPrograms.stream().allMatch(program -> DaliBeans.isProgramValidator(program, userId, departmentId));
        permissions.isProgramRecorder =  permissions.isManager || permissions.isValidator || selectedPrograms.stream().allMatch(program -> DaliBeans.isProgramRecorder(program, userId, departmentId));
        permissions.isSurveyRecorder = permissions.isManager || permissions.isValidator || surveys.stream().allMatch(survey -> getDataContext().isSurveyEditable(survey));
        permissions.isQualifier = permissions.isManager || (permissions.isSurveyRecorder && getDataContext().isRecorderQualifier());

        return permissions;
    }

    static public class Permissions {
        public boolean isManager;
        public boolean isValidator;
        public boolean isProgramRecorder;
        public boolean isSurveyRecorder;
        public boolean isQualifier;
    }
}
