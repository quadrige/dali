package fr.ifremer.dali.ui.swing.content.manage.program.strategies;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.dali.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.dali.ui.swing.action.AbstractDaliSaveAction;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUIModel;
import fr.ifremer.dali.ui.swing.content.manage.program.SaveAction;
import fr.ifremer.dali.ui.swing.content.manage.program.menu.SearchAction;
import fr.ifremer.dali.ui.swing.content.manage.program.strategies.duplicate.SelectProgramUI;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeans;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de dupliquer une stratégie.
 */
public class DuplicateStrategyAction extends AbstractCheckModelAction<StrategiesTableUIModel, StrategiesTableUI, StrategiesTableUIHandler> {

    private static final Log LOG = LogFactory.getLog(DuplicateStrategyAction.class);

    private StrategyDTO strategyToDuplicate;
    private StrategyDTO duplicateStrategy;
    private ProgramDTO sourceProgram;
    private ProgramDTO targetProgram;
    private boolean localToNational;

    /**
     * Constructor.
     *
     * @param handler Controleur
     */
    public DuplicateStrategyAction(final StrategiesTableUIHandler handler) {
        super(handler, false);
    }

    private ProgramsUI getProgramsUI() {
        return getUI().getParentContainer(ProgramsUI.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<? extends AbstractDaliSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        final ProgramsUIModel model = getModel().getParentModel();
        return model != null && model.isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        final ProgramsUIModel model = getModel().getParentModel();
        if (model != null) {
            model.setModify(modelModify);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        final ProgramsUIModel model = getModel().getParentModel();
        return model == null || model.isValid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getProgramsUI().getHandler();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getParentModel().getProgramsUIModel().getSingleSelectedRow() == null || getModel().getSelectedRows().size() != 1) {
            return false;
        }

        duplicateStrategy = null;
        strategyToDuplicate = getModel().getSelectedRows().iterator().next().toBean();
        sourceProgram = getModel().getParentModel().getProgramsUIModel().getSingleSelectedRow().toBean();

        // Open select program UI
        SelectProgramUI selectProgramUI = new SelectProgramUI(getContext());
        selectProgramUI.getModel().setSourceProgram(sourceProgram);
        selectProgramUI.getModel().setSourceStrategy(strategyToDuplicate);
        getHandler().openDialog(selectProgramUI);
        targetProgram = selectProgramUI.getModel().getTargetProgram();
        if (targetProgram == null) {
            // mantis 27906
            return false;
        }

        // Test the status
        localToNational = QuadrigeBeans.isLocalStatus(sourceProgram.getStatus()) && !QuadrigeBeans.isLocalStatus(targetProgram.getStatus());
        if (localToNational) {
            getContext().getDialogHelper().showWarningDialog(t("dali.action.duplicate.strategy.warning.localReferential", decorate(strategyToDuplicate)));
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        // Do not load target program if same as source
        if (!sourceProgram.equals(targetProgram)) {

            // force loading state to be able to wait strategies loading
            getProgramsUI().getMenuUI().getModel().setLoading(true);
            getModel().setLoading(true);
            // Select the target program
            getProgramsUI().getMenuUI().getProgramCodeCombo().setSelectedItem(targetProgram);
            getProgramsUI().getMenuUI().getModel().setLoading(false);
            // Do search
            getActionEngine().runFullInternalAction(
                    getActionFactory().createLogicAction(getProgramsUI().getMenuUI().getHandler(), SearchAction.class)
            );

            // wait for strategies loading
            while (getModel().isLoading()) {
                // do nothing
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
            }
        }

        // duplicate
        duplicateStrategy = getContext().getProgramStrategyService().duplicateStrategy(strategyToDuplicate, targetProgram, localToNational);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (duplicateStrategy == null) {
            LOG.error("the strategy to duplicate is null, duplication aborted !");
            return;
        }

        // Add duplicate observation to table
        StrategiesTableRowModel row = getModel().addNewRow(duplicateStrategy);

        getModel().setModify(true);

        // Add focus on duplicate row
        getHandler().setFocusOnCell(row);
    }
}
