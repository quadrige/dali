package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.associatedMatrices;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.List;

/**
 * Controlleur pour la gestion des lieux au niveau national
 */
public class AssociatedMatricesUIHandler extends AbstractDaliTableUIHandler<MatricesTableRowModel, AssociatedMatricesUIModel, AssociatedMatricesUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(AssociatedMatricesUI ui) {
        super.beforeInit(ui);

        AssociatedMatricesUIModel matricesUIModel = new AssociatedMatricesUIModel();
        ui.setContextValue(matricesUIModel);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(AssociatedMatricesUI associatedMatricesUI) {
        initUI(associatedMatricesUI);

        // Initialisation du tableau
        initTable();


        getModel().addPropertyChangeListener(evt -> {

            if (AssociatedMatricesUIModel.PROPERTY_FRACTION.equals(evt.getPropertyName())) {

                List<MatrixDTO> matrices = getModel().getFraction() != null ? Lists.newArrayList(getModel().getFraction().getMatrixes()) : null;
                getModel().setBeans(matrices);

            }
        });
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(MatricesTableModel.NAME);
        mnemonicCol.setSortable(true);

        // description
        final TableColumnExt descriptionCol = addColumn(MatricesTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                MatricesTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);
        fixDefaultColumnWidth(statusCol);

        MatricesTableModel tableModel = new MatricesTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        // Initialisation du tableau
        initTable(table, true);

        table.setEditable(false);

    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<MatricesTableRowModel> getTableModel() {
        return (MatricesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getAssociatedMatricesTable();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
