<!--
  #%L
  Dali :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<Table id="tablePanel" decorator='help' implements='fr.ifremer.dali.ui.swing.util.DaliUI&lt;LocationsTableUIModel, LocationsTableUIHandler&gt;'>
  <import>
    fr.ifremer.dali.ui.swing.DaliHelpBroker
    fr.ifremer.dali.ui.swing.DaliUIContext
    fr.ifremer.dali.ui.swing.util.DaliUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
    fr.ifremer.quadrige3.ui.swing.plaf.WaitBlockingLayerUI
    fr.ifremer.quadrige3.ui.swing.table.SwingTable
    fr.ifremer.quadrige3.ui.swing.component.ToggleButton

    javax.swing.Box
    javax.swing.BoxLayout
    java.awt.BorderLayout

    static org.nuiton.i18n.I18n.*
  </import>

  <!--getContextValue est une méthode interne JAXX-->
  <LocationsTableUIModel id='model' initializer='getContextValue(LocationsTableUIModel.class)'/>
  <DaliHelpBroker id='broker' constructorParams='"dali.home.help"'/>
  <WaitBlockingLayerUI id='tableBlockLayer'/>

  <script><![CDATA[
        //Le parent est très utile pour intervenir sur les frères
        public LocationsTableUI(ApplicationUI parentUI) {
            ApplicationUIUtil.setParentUI(this, parentUI);
        }
	]]></script>

  <row>
    <cell weightx='1' weighty='1' fill='both'>
      <JScrollPane id="tableScrollPane" decorator='boxed'>
        <SwingTable id="locationsTable"/>
      </JScrollPane>
    </cell>
  </row>
  <row>
    <cell weightx='1' weighty='0' fill='both'>
      <JPanel layout='{new BorderLayout()}'>
        <JPanel constraints='BorderLayout.LINE_START'>
          <JButton id='addLocationsButton' alignmentX='{Component.CENTER_ALIGNMENT}' onActionPerformed="handler.addLocations()"/>
          <JComboBox id='editCombobox' alignmentX='{Component.CENTER_ALIGNMENT}'/>
          <JButton id='updatePeriodButton' alignmentX='{Component.CENTER_ALIGNMENT}' onActionPerformed="handler.updatePeriod()"/>
          <JButton id='showStrategiesButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
          <JButton id='removeLocationsButton' alignmentX='{Component.CENTER_ALIGNMENT}' onActionPerformed="handler.removeLocations()"/>
          <JCheckBox id="strategyFilterCheckbox" alignmentX='{Component.CENTER_ALIGNMENT}'/>
        </JPanel>
        <JPanel constraints='BorderLayout.LINE_END'>
          <ToggleButton id="fullScreenToggleButton" onActionPerformed="handler.toggleFullScreen(tablePanel, fullScreenToggleButton)"/>
        </JPanel>
      </JPanel>
    </cell>
  </row>
</Table>