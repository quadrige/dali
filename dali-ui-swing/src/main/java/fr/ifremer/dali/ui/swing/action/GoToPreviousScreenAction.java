package fr.ifremer.dali.ui.swing.action;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.action.AbstractChangeScreenAction;
import fr.ifremer.dali.ui.swing.content.DaliMainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * To return on previous screen according to screen breadcrumb in UI context.
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class GoToPreviousScreenAction extends AbstractChangeScreenAction {

    /**
     * <p>Constructor for GoToPreviousScreenAction.</p>
     *
     * @param handler a {@link DaliMainUIHandler} object.
     */
    public GoToPreviousScreenAction(DaliMainUIHandler handler) {
        super(handler, true, handler.getContext().getScreenBreadcrumb().peekLast());
        setActionDescription(t("dali.main.action.goto.previousScreen.tip"));
    }

    /** {@inheritDoc} */
    @Override
    protected boolean doNotTrackThisScreen() {
        return true;
    }

}
