<!--
  #%L
  Dali :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JPanel decorator='help' implements='fr.ifremer.dali.ui.swing.content.manage.referential.menu.ReferentialMenuUI&lt;SamplingEquipmentsMenuUIModel, SamplingEquipmentsMenuUIHandler&gt;'>
  <import>
    fr.ifremer.dali.ui.swing.DaliHelpBroker
    fr.ifremer.dali.ui.swing.DaliUIContext
    fr.ifremer.dali.ui.swing.util.DaliUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil

    fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO
    fr.ifremer.dali.dto.referential.UnitDTO
    fr.ifremer.dali.dto.referential.SamplingEquipmentDTO
    fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUI

    javax.swing.Box
    javax.swing.BoxLayout
    java.awt.BorderLayout
    javax.swing.SwingConstants

    fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox

    static org.nuiton.i18n.I18n.*
  </import>

  <!--getContextValue est une méthode interne JAXX-->
  <SamplingEquipmentsMenuUIModel id='model' initializer='getContextValue(SamplingEquipmentsMenuUIModel.class)'/>

  <DaliHelpBroker id='broker' constructorParams='"dali.home.help"'/>

  <script><![CDATA[
		//Le parent est très utile pour intervenir sur les frères
		public SamplingEquipmentsMenuUI(ApplicationUI parentUI) {
		    ApplicationUIUtil.setParentUI(this, parentUI);
		}
	]]></script>

  <JPanel id="menuPanel" layout='{new BoxLayout(menuPanel, BoxLayout.PAGE_AXIS)}'>

    <ApplyFilterUI id='applyFilterUI' constructorParams='this'/>

    <Table id="selectionPanel" fill="both">
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='nameLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='nameCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='SamplingEquipmentDTO'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='statusLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='statusCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='StatusDTO'/>
          </JPanel>
        </cell>
      </row>
    </Table>

    <JPanel id='selectionButtonsPanel' layout='{new GridLayout(1,0)}'>
      <JButton id='clearButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
      <JButton id='searchButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
    </JPanel>

  </JPanel>
</JPanel>