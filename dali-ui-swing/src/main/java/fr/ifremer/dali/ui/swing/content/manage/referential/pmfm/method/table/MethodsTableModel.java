package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class MethodsTableModel extends AbstractDaliTableModel<MethodsTableRowModel> {

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> NAME = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class, true);
    /** Constant <code>REFERENCE</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> REFERENCE = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_REFERENCE,
            n("dali.property.pmfm.method.reference"),
            n("dali.property.pmfm.method.reference"),
            String.class);
    /** Constant <code>NUMBER</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> NUMBER = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_NUMBER,
            n("dali.property.pmfm.method.number"),
            n("dali.property.pmfm.method.number"),
            String.class);
    /** Constant <code>STATUS</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> STATUS = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class, true);
    /** Constant <code>DESCRIPTION</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION,
            n("dali.property.description"),
            n("dali.property.description"),
            String.class, true);
    /** Constant <code>DESCRIPTIONPACKAGING</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> DESCRIPTIONPACKAGING = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION_PACKAGING,
            n("dali.property.pmfm.method.descriptionPackaging"),
            n("dali.property.pmfm.method.descriptionPackaging"),
            String.class);
    /** Constant <code>DESCRIPTIONPREPARATION</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> DESCRIPTIONPREPARATION = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION_PREPARATION,
            n("dali.property.pmfm.method.descriptionPreparation"),
            n("dali.property.pmfm.method.descriptionPreparation"),
            String.class);
    /** Constant <code>DESCRIPTIONPRESERVATION</code> */
    public static final DaliColumnIdentifier<MethodsTableRowModel> DESCRIPTIONPRESERVATION = DaliColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION_PRESERVATION,
            n("dali.property.pmfm.method.descriptionPreservation"),
            n("dali.property.pmfm.method.descriptionPreservation"),
            String.class);

    public static final DaliColumnIdentifier<MethodsTableRowModel> COMMENT = DaliColumnIdentifier.newId(
        MethodsTableRowModel.PROPERTY_COMMENT,
        n("dali.property.comment"),
        n("dali.property.comment"),
        String.class,
        false);

    public static final DaliColumnIdentifier<MethodsTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        MethodsTableRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<MethodsTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        MethodsTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);


    /**
     * <p>Constructor for MethodsTableModel.</p>
     *
     * @param createNewRowAllowed a boolean.
     */
    public MethodsTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
    }

    /** {@inheritDoc} */
    @Override
    public MethodsTableRowModel createNewRow() {
        return new MethodsTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<MethodsTableRowModel> getFirstColumnEditing() {
        return NAME;
    }
}
