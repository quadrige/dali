/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.dali.ui.swing.content.manage.referential.department.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>DepartmentTableModel class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class DepartmentTableModel extends AbstractDaliTableModel<DepartmentRowModel> {

    private final boolean readOnly;

    /**
     * <p>Constructor for DepartmentTableModel.</p>
     *
     * @param createNewRowAllowed a boolean.
     */
    public DepartmentTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
        readOnly = !createNewRowAllowed;
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentRowModel createNewRow() {
        return new DepartmentRowModel(readOnly);
    }

    /** Constant <code>CODE</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> CODE = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_CODE,
            n("dali.property.code"),
            n("dali.property.code"),
            String.class,
            true);

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> NAME = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class,
            true);

    /** Constant <code>DESCRIPTION</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_DESCRIPTION,
            n("dali.property.description"),
            n("dali.property.description"),
            String.class);

    /** Constant <code>PARENT_DEPARTMENT</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> PARENT_DEPARTMENT = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_PARENT_DEPARTMENT,
            n("dali.property.department.parent"),
            n("dali.property.department.parent"),
            DepartmentDTO.class);

    /** Constant <code>EMAIL</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> EMAIL = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_EMAIL,
            n("dali.property.email"),
            n("dali.property.email"),
            String.class);

    /** Constant <code>PHONE</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> PHONE = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_PHONE,
            n("dali.property.phone"),
            n("dali.property.phone"),
            String.class);

    /** Constant <code>ADDRESS</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> ADDRESS = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_ADDRESS,
            n("dali.property.address"),
            n("dali.property.address"),
            String.class);

    public static final DaliColumnIdentifier<DepartmentRowModel> SIRET = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_SIRET,
            n("dali.property.siret"),
            n("dali.property.siret"),
            String.class);

    public static final DaliColumnIdentifier<DepartmentRowModel> URL = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_URL,
            n("dali.property.url"),
            n("dali.property.url"),
            String.class);

    /** Constant <code>STATUS</code> */
    public static final DaliColumnIdentifier<DepartmentRowModel> STATUS = DaliColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class,
            true);

    public static final DaliColumnIdentifier<DepartmentRowModel> COMMENT = DaliColumnIdentifier.newId(
        DepartmentRowModel.PROPERTY_COMMENT,
        n("dali.property.comment"),
        n("dali.property.comment"),
        String.class,
        false);

    public static final DaliColumnIdentifier<DepartmentRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        DepartmentRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<DepartmentRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        DepartmentRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);



    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<DepartmentRowModel> getFirstColumnEditing() {
        return CODE;
    }
}
