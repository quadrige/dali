package fr.ifremer.dali.ui.swing.content.extraction.list;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.enums.ExtractionFilterTypeValues;
import fr.ifremer.dali.dto.enums.ExtractionOutputType;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.dali.ui.swing.action.AbstractDaliSaveAction;
import fr.ifremer.dali.ui.swing.content.extraction.ExtractionUI;
import fr.ifremer.dali.ui.swing.content.extraction.SaveAction;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.synchro.vo.SynchroImportContextVO;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportDataSynchroAtOnceAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.JOptionPane;
import java.io.File;
import java.util.LinkedHashSet;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 08/02/2018.
 */
public abstract class AbstractExtractAction extends AbstractCheckModelAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

    private static final Log LOG = LogFactory.getLog(AbstractExtractAction.class);

    private File outputFile;

    /**
     * Constructor.
     *
     * @param handler Handler
     */
    protected AbstractExtractAction(ExtractionsTableUIHandler handler) {
        super(handler, true);
    }

    /**
     * <p>getOutputType.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.enums.ExtractionOutputType} object.
     */
    protected abstract ExtractionOutputType getOutputType();

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<? extends AbstractDaliSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        return getModel().getExtractionUIModel().isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().getExtractionUIModel().setModify(modelModify);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        return getModel().getExtractionUIModel().isValid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        final ExtractionUI extractionUI = getUI().getParentContainer(ExtractionUI.class);
        return extractionUI.getHandler();
    }

    protected ExtractionDTO getSelectedExtraction() {
        return getModel().getSelectedRows().size() != 1 ? null : getModel().getSelectedRows().iterator().next();
    }

    protected File getOutputFile() {
        return outputFile;
    }

    protected void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getSelectedExtraction() == null) {
            LOG.warn("no selected extraction");
            return false;
        }

        if (getOutputType() == null) {
            LOG.error("no ExtractionOutputType");
            return false;
        }

        // Check potential data under moratorium
        if (getContext().getProgramStrategyService().hasPotentialMoratoriums(
            DaliBeans.getFilterElementsIds(getSelectedExtraction(), ExtractionFilterTypeValues.PROGRAM),
            DaliBeans.getExtractionPeriods(getSelectedExtraction())
        )) {
            getContext().getDialogHelper().showWarningDialog(t("dali.action.extract.potentialMoratorium"), t("dali.action.extract.title", getOutputType().getLabel()));
        }

        return true;
    }

    @Override
    public void doAction() throws Exception {

        // Check data updates if server available
        try {
            checkDataUpdate();
        } catch (Exception e) {
            LOG.warn("Connection to synchronization server failed, data updates ignored");
        }

    }

    private void checkDataUpdate() {
        List<String> programs = DaliBeans.getFilterElementsIds(getSelectedExtraction(), ExtractionFilterTypeValues.PROGRAM);
        Assert.notEmpty(programs);
        getContext().getSynchroContext().setImportDataProgramCodes(new LinkedHashSet<>(programs));

        SynchroImportContextVO contextResult = DaliServiceLocator.instance().getSynchroRestClientService().checkImportDataContext(
                getContext().getAuthenticationInfo(),
                getContext().getSynchroContext().getSynchroImportContext()
        );

        if (contextResult.isWithData()) {

            // ask use to update to start import data
            if (getContext().getDialogHelper().showConfirmDialog(
                    t("dali.action.extract.updateAvailable"),
                    t("dali.action.extract.title", getOutputType().getLabel()), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

                doImportData();
            }

        }

    }

    private void doImportData() {

        // Keep old action description
        String oldDescription = getActionDescription();

        // Create import data action
        ImportDataSynchroAtOnceAction importAction = getContext().getActionFactory().createLogicAction(getContext().getMainUI().getHandler(), ImportDataSynchroAtOnceAction.class);

        // override action description
        forceActionDescription(t("dali.action.synchro.import.data"));

        // run internally
        getContext().getActionEngine().runInternalAction(importAction);

        // Restore description
        forceActionDescription(oldDescription);

    }

    protected void performExtraction() {
        getContext().getExtractionPerformService().performExtraction(getSelectedExtraction(), getOutputType(), getOutputFile(), createProgressionUIModel());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void releaseAction() {
        outputFile = null;
        super.releaseAction();
    }
}
