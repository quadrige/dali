package fr.ifremer.dali.ui.swing.content.manage.context.filterslist;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ManageFiltersListTableUITableModel extends AbstractDaliTableModel<ManageFiltersListTableUIRowModel> {

	/** Constant <code>TYPE</code> */
	public static final DaliColumnIdentifier<ManageFiltersListTableUIRowModel> TYPE = DaliColumnIdentifier.newId(
			ManageFiltersListTableUIRowModel.PROPERTY_TYPE,
			n("dali.context.filtersList.label"),
			n("dali.context.filtersList.label.tip"),
			String.class);

	/** Constant <code>FILTER</code> */
	public static final DaliColumnIdentifier<ManageFiltersListTableUIRowModel> FILTER = DaliColumnIdentifier.newId(
			ManageFiltersListTableUIRowModel.PROPERTY_FILTER,
			n("dali.context.filtersList.name"),
			n("dali.context.filtersList.name.tip"),
			FilterDTO.class);
	
	/**
	 * <p>Constructor for ManageFiltersListTableUITableModel.</p>
	 *
	 */
	public ManageFiltersListTableUITableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ManageFiltersListTableUIRowModel createNewRow() {
		return new ManageFiltersListTableUIRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ManageFiltersListTableUIRowModel> getFirstColumnEditing() {
		return FILTER;
	}
}
