package fr.ifremer.dali.ui.swing.content.synchro.changes;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model for change log table.
 */
public class SynchroChangesTableModel extends AbstractDaliTableModel<SynchroChangesRowModel> {

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final DaliColumnIdentifier<SynchroChangesRowModel> NAME = DaliColumnIdentifier.newId(
    		SynchroChangesRowModel.PROPERTY_NAME,
            n("dali.synchro.changes.name.short"),
            n("dali.synchro.changes.name.tip"),
            String.class);

	/**
	 * Identifier for column operation type
	 */
	public static final DaliColumnIdentifier<SynchroChangesRowModel> OPERATION_TYPE = DaliColumnIdentifier.newId(
			SynchroChangesRowModel.PROPERTY_OPERATION_TYPE,
			null,
			n("dali.synchro.changes.operationType.tip"),
			String.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public SynchroChangesTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, true, false);
	}

	/** {@inheritDoc} */
	@Override
	public SynchroChangesRowModel createNewRow() {
		return new SynchroChangesRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<SynchroChangesRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
