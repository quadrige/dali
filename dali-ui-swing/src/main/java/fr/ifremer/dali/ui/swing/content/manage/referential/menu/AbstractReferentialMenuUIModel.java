package fr.ifremer.dali.ui.swing.content.manage.referential.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.dali.dto.configuration.filter.FilterCriteriaDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import org.nuiton.util.beans.Binder;

import java.util.*;

/**
 * Created by Ludovic on 24/03/2016.
 */
public abstract class AbstractReferentialMenuUIModel<F extends FilterCriteriaDTO, M extends AbstractReferentialMenuUIModel<F, M>>
        extends AbstractDaliBeanUIModel<F, M>
        implements FilterCriteriaDTO {

    /**
     * <p>Constructor for AbstractReferentialMenuUIModel.</p>
     *
     * @param fromBeanBinder a {@link org.nuiton.util.beans.Binder} object.
     * @param toBeanBinder a {@link org.nuiton.util.beans.Binder} object.
     */
    protected AbstractReferentialMenuUIModel(Binder<F, M> fromBeanBinder, Binder<M, F> toBeanBinder) {
        super(fromBeanBinder, toBeanBinder);
    }

    /**
     * <p>clear.</p>
     */
    public void clear() {
        setName(null);
        setStatus(null);
        setResults(null);
    }

    /** {@inheritDoc} */
    @Override
    public List<? extends QuadrigeBean> getResults() {
        return delegateObject.getResults();
    }

    /** {@inheritDoc} */
    @Override
    public void setResults(List<? extends QuadrigeBean> results) {
        // set the results in a new list to let pcs fire the property change event
        // but it's not enough, if old and new collections are really equals, it don't fire
        boolean mustFire = Objects.equals(results, getResults());
        delegateObject.setResults(results != null ? new ArrayList<>(results) : null);
        if (mustFire)
            firePropertyChange(PROPERTY_RESULTS, null, results);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /**
     * <p>getStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStatusCode(){
        return getStatus() != null ? getStatus().getCode() : null;
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isStrictName() {
        return delegateObject.isStrictName();
    }

    /** {@inheritDoc} */
    @Override
    public void setStrictName(boolean strictName) {
        delegateObject.setStrictName(strictName);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }
}
