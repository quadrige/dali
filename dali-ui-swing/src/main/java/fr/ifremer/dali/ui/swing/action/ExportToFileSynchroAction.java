package fr.ifremer.dali.ui.swing.action;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.SearchDateDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.DaliMainUIHandler;
import fr.ifremer.dali.ui.swing.content.synchro.program.ProgramSelectUI;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.synchro.service.client.SynchroClientService;
import fr.ifremer.quadrige3.synchro.service.client.vo.SynchroClientExportToFileResult;
import fr.ifremer.quadrige3.ui.swing.ApplicationUI;
import fr.ifremer.quadrige3.ui.swing.action.AbstractReloadCurrentScreenAction;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroDirection;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIContext;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.awt.Dimension;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ExportToFileSynchroAction class.</p>
 *
 * @author benoit.lavenier@e-is.pro
 * @since 1.0
 */
public class ExportToFileSynchroAction extends AbstractReloadCurrentScreenAction {

    private static final Log log = LogFactory.getLog(ExportToFileSynchroAction.class);
    protected File file;
    private Set<String> programCodes;
    private boolean dirtyOnly;
    private SearchDateDTO searchDate;
    private Date startDate;
    private Date endDate;
    private boolean hasData = false;

    /**
     * <p>Constructor for ExportToFileSynchroAction.</p>
     *
     * @param handler a {@link DaliMainUIHandler} object.
     */
    public ExportToFileSynchroAction(DaliMainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("dali.action.synchro.exportToFile.title"));
    }

    /**
     * <p>Setter for the field <code>file</code>.</p>
     *
     * @param file a {@link java.io.File} object.
     */
    public void setFile(File file) {
        this.file = file;
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        file = null;
        boolean doAction = super.prepareAction();
        if (!doAction) {
            return false;
        }

        getSynchroUIContext().setDirection(SynchroDirection.EXPORT);
        getSynchroUIContext().loadExportContext();

        // Ask user to select program to import
        programCodes = getSynchroUIContext().getExportDataProgramCodes();
        {
            ProgramSelectUI programSelectUI = new ProgramSelectUI((ApplicationUI) getUI(), StatusFilter.ALL, programCodes, true, false);
            handler.openDialog(programSelectUI, t("dali.action.synchro.export.dataProgramCodes.title"), new Dimension(800, 400));

            List<ProgramDTO> programs = programSelectUI.getModel().getSelectedPrograms();

            // If no programs selected (or user cancelled): exit
            if (CollectionUtils.isEmpty(programs)) {
                return false;
            }

            // Get selected programs as code list
            programCodes = Sets.newHashSet(DaliBeans.collectProperties(programs, ProgramDTO.PROPERTY_CODE));

            // Get export options
            dirtyOnly = programSelectUI.getModel().isDirtyOnly();
            searchDate = programSelectUI.getModel().getSearchDate();
            startDate = programSelectUI.getModel().getStartDate();
            endDate = programSelectUI.getModel().getEndDate();
        }

        // ask user file where to export db
        String date = DateUtil.formatDate(new Date(), "yyy-MM-dd");
        file = saveFile(
                getConfig().getSynchroZipFilePrefix() + date,
                "zip",
                t("dali.synchro.export.choose.exportDataToFile.title"),
                t("dali.synchro.export.choose.exportDataToFile.buttonLabel"),
                "^.*\\.zip", t("dali.common.file.zip")
        );
        doAction = file != null;

        return doAction;
    }

    private SynchroUIContext getSynchroUIContext() {
        return getContext().getSynchroContext();
    }

    private SynchroUIHandler getSynchroHandler() {
        return getContext().getSynchroHandler();
    }

    /** {@inheritDoc} */
    @Override
    public void doActionBeforeReload() {
        hasData = false;

        SynchroClientService synchroService = DaliServiceLocator.instance().getSynchroClientService();
        int userId = SecurityContextHelper.getQuadrigeUserId();

        createProgressionUIModel(100);

        // Save the UI context
        getSynchroUIContext().setExportDataProgramCodes(programCodes);
        getSynchroUIContext().saveExportContext();

        // build temp database and export local to temp
        getProgressionUIModel().setMessage(t("quadrige3.synchro.progress.export"));

        // export directory is set by the synchronization service
        SynchroClientExportToFileResult exportResult = synchroService.exportToFile(userId,
                file,
                programCodes,
                dirtyOnly,
                false,
                DaliBeans.getDateOperator(searchDate),
                startDate,
                endDate,
                getProgressionUIModel(), 100);

        // If no data to export, stop here
        hasData = exportResult.getDataResult().getTotalTreated() > 0;
        if (!hasData) {
            showNoDataMessage();
            return;
        }

        // delegate progression model from SynchroUIContext
        getProgressionUIModel().setTotal(100);

        // restore action progression model
        setProgressionUIModel(getProgressionUIModel());

        // do NOT reload screen if no data
        setSkipScreenReload(!hasData);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        super.postSuccessAction();
        if (log.isInfoEnabled()) {
            log.info(t("dali.action.synchro.export.success"));
        }

        // do not display if no data found
        if (!hasData) {
            getContext().getSynchroHandler().report(t("dali.action.synchro.export.noData"), false);
        } else {
            getContext().getSynchroHandler().report(t("dali.action.synchro.export.success"));
        }
    }

    /** {@inheritDoc} */
    @Override
    public void postFailedAction(Throwable error) {
        super.postFailedAction(error);

        if (error != null) {
            log.error(t("dali.action.synchro.export.failed.log", error.getMessage()));
        }

        else if (getSynchroUIContext().getProgressionModel() != null
                && getSynchroUIContext().getProgressionModel().getMessage() != null) {

            String serverMessage = getSynchroUIContext().getProgressionModel().getMessage();
            log.error(t("dali.action.synchro.export.failed.server.log", serverMessage));

        }

        else {
            log.error(t("dali.action.synchro.export.failed"));
        }

        getContext().getSynchroHandler().report(t("dali.action.synchro.export.failed"));
    }

    /* -- Internal methods -- */

    private void showNoDataMessage() {
        getContext().getDialogHelper().showMessageDialog(t("dali.action.synchro.export.noData"), t("dali.action.synchro.export.result.title"));
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        super.releaseAction();
        file = null;
        hasData = false;
    }
}
