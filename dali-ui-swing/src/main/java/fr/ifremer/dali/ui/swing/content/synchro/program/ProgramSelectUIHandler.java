package fr.ifremer.dali.ui.swing.content.synchro.program;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.enums.SearchDateValues;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroDirection;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Program selector UI handler.
 */
public class ProgramSelectUIHandler extends AbstractDaliUIHandler<ProgramSelectUIModel, ProgramSelectUI> implements Cancelable {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ProgramSelectUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ProgramSelectUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ProgramSelectUIModel model = new ProgramSelectUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ProgramSelectUI ui) {

        initUI(ui);

        initOptions(ui);

        boolean writableProgramsOnly = getContext().getSynchroContext().getDirection() == SynchroDirection.EXPORT;

        // load programs
        StatusFilter programStatusFilter = ui.getProgramStatusFilter();
        // Get program from synchronisation server
        List<ProgramDTO> programs = null;
        boolean useServer = Boolean.FALSE.equals(ui.fileSynchro) && getContext().isSynchroEnabled()
                && (programStatusFilter == StatusFilter.ALL || programStatusFilter == StatusFilter.ACTIVE);

        if (useServer) {
            try {
                programs = writableProgramsOnly
                    ? getContext().getProgramStrategyService().getRemoteWritableProgramsByUser(getContext().getAuthenticationInfo())
                    : getContext().getProgramStrategyService().getRemoteReadableProgramsByUser(getContext().getAuthenticationInfo());
            } catch (QuadrigeTechnicalException ex) {
                if (Exceptions.hasCause(ex, IOException.class)) {
                    LOG.warn("Connection to synchronization server failed, will use local programs");
                    useServer = false;
                } else throw ex;
            }
        }

        if (!useServer) {
            programs = writableProgramsOnly
                ? getContext().getProgramStrategyService().getWritableProgramsByStatus(programStatusFilter)
                : getContext().getProgramStrategyService().getReadableProgramsByStatus(programStatusFilter);
        }

        List<ProgramDTO> selectedPrograms = Lists.newArrayList();

        if (CollectionUtils.isNotEmpty(programs) && CollectionUtils.isNotEmpty(ui.getInitialProgramCodes())) {
            selectedPrograms = programs.stream().filter(program -> program != null && ui.getInitialProgramCodes().contains(program.getCode())).collect(Collectors.toList());
        }

        // Apply default previously selected programs
        getModel().setSelectedPrograms(selectedPrograms);
        initBeanList(ui.getProgramDoubleList(), programs, selectedPrograms);

    }

    private void initOptions(ProgramSelectUI ui) {

        if (Boolean.TRUE.equals(ui.getFileSynchro())) {

            // show file options
            ui.getOptionsPanel().setVisible(true);
            ui.getOnlyDirtyCheckBox().setVisible(true);
            ui.getDatesPanel().setVisible(true);

            // set 'dirty only' flag to true by default
            getModel().setDirtyOnly(true);

            // init fields
            ui.getStartDateEditor().setEnabled(false);
            ui.getEndDateEditor().setEnabled(false);
            ui.getAndLabel().setVisible(false);
            ui.getEndDateEditor().setVisible(false);

            DaliUIs.forceComponentSize(ui.getSearchDateCombo(), 82);
            DaliUIs.forceComponentSize(ui.getStartDateEditor(), 120);
            DaliUIs.forceComponentSize(ui.getEndDateEditor(), 120);

            // init search date combo
            initBeanFilterableComboBox(
                    ui.getSearchDateCombo(),
                    getContext().getSystemService().getSearchDates(),
                    null);

            // add a listener on it
            getModel().addPropertyChangeListener(ProgramSelectUIModel.PROPERTY_SEARCH_DATE, evt -> {

                if (getModel().getSearchDateId() != null) {

                    // enable fields
                    ui.getStartDateEditor().setEnabled(true);
                    ui.getEndDateEditor().setEnabled(true);

                    final SearchDateValues searchDateValue = SearchDateValues.values()[getModel().getSearchDateId()];
                    if (searchDateValue == SearchDateValues.BETWEEN) {
                        // show all fields
                        ui.getAndLabel().setVisible(true);
                        ui.getEndDateEditor().setVisible(true);
                    } else {
                        // show start date only
                        ui.getEndDateEditor().setVisible(false);
                        ui.getAndLabel().setVisible(false);
                        getModel().setEndDate(null);
                    }
                } else {

                    // clear
                    ui.getStartDateEditor().setEnabled(false);
                    ui.getEndDateEditor().setEnabled(false);
                    ui.getEndDateEditor().setVisible(false);
                    ui.getAndLabel().setVisible(false);
                    getModel().setStartDate(null);
                    getModel().setEndDate(null);
                }
            });

        } else {

            // hide file options
            ui.getOnlyDirtyCheckBox().setVisible(false);
            ui.getDatesPanel().setVisible(false);
            // reset values
            getModel().setDirtyOnly(false);
            getModel().setSearchDate(null);
            getModel().setStartDate(null);
            getModel().setEndDate(null);

        }

        if (Boolean.TRUE.equals(ui.getPhotoSynchro())) {

            // show options
            ui.getOptionsPanel().setVisible(true);
            ui.getEnablePhotoCheckBox().setVisible(true);
            // init the photo option
            getModel().setEnablePhoto(getConfig().isSynchroPhotoEnabledByDefault());

        } else {

            // hide options
            ui.getEnablePhotoCheckBox().setVisible(false);
        }

        // Hide options panel if no option selected
        if (Boolean.FALSE.equals(ui.getFileSynchro()) && Boolean.FALSE.equals(ui.getPhotoSynchro())) {
            ui.getOptionsPanel().setVisible(false);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel() {

        // clear selected programs
        getModel().setSelectedPrograms(null);

        closeDialog();
    }

    /**
     * <p>select.</p>
     */
    public void select() {
        closeDialog();
    }
}
