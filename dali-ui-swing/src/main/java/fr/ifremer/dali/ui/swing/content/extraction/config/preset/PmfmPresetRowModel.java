package fr.ifremer.dali.ui.swing.content.extraction.config.preset;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.List;

/**
 * @author peck7 on 19/07/2017.
 */
public class PmfmPresetRowModel extends AbstractDaliRowUIModel<PmfmPresetDTO, PmfmPresetRowModel> implements PmfmPresetDTO {

    private static final Binder<PmfmPresetDTO, PmfmPresetRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(PmfmPresetDTO.class, PmfmPresetRowModel.class);
    private static final Binder<PmfmPresetRowModel, PmfmPresetDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(PmfmPresetRowModel.class, PmfmPresetDTO.class);

    /**
     * <p>Constructor for PmfmPresetRowModel.</p>
     */
    public PmfmPresetRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    @Override
    protected PmfmPresetDTO newBean() {
        return DaliBeanFactory.newPmfmPresetDTO();
    }

    @Override
    public PmfmDTO getPmfm() {
        return delegateObject.getPmfm();
    }

    @Override
    public void setPmfm(PmfmDTO pmfm) {
        delegateObject.setPmfm(pmfm);
    }

    @Override
    public QualitativeValueDTO getQualitativeValues(int index) {
        return delegateObject.getQualitativeValues(index);
    }

    @Override
    public boolean isQualitativeValuesEmpty() {
        return delegateObject.isQualitativeValuesEmpty();
    }

    @Override
    public int sizeQualitativeValues() {
        return delegateObject.sizeQualitativeValues();
    }

    @Override
    public void addQualitativeValues(QualitativeValueDTO qualitativeValues) {
        delegateObject.addQualitativeValues(qualitativeValues);
    }

    @Override
    public void addAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        delegateObject.addAllQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean removeQualitativeValues(QualitativeValueDTO qualitativeValues) {
        return delegateObject.removeQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean removeAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        return delegateObject.removeAllQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean containsQualitativeValues(QualitativeValueDTO qualitativeValues) {
        return delegateObject.containsQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean containsAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        return delegateObject.containsAllQualitativeValues(qualitativeValues);
    }

    @Override
    public List<QualitativeValueDTO> getQualitativeValues() {
        return delegateObject.getQualitativeValues();
    }

    @Override
    public void setQualitativeValues(List<QualitativeValueDTO> qualitativeValues) {
        delegateObject.setQualitativeValues(qualitativeValues);
    }
}
