package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.multiedit;

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableUI;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared.AbstractOperationMeasurementsGroupedTableModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared.AbstractOperationMeasurementsGroupedTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.dali.ui.swing.util.table.renderer.MultipleValueCellRenderer;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.state.FixedSwingTableSessionState;
import fr.ifremer.quadrige3.ui.swing.table.state.SwingTableSessionState;
import jaxx.runtime.swing.session.State;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Controleur pour les mesures de l'observation.
 */
public class OperationMeasurementsMultiEditUIHandler
    extends AbstractOperationMeasurementsGroupedTableUIHandler<OperationMeasurementsGroupedRowModel, OperationMeasurementsMultiEditUIModel, OperationMeasurementsMultiEditUI>
    implements Cancelable {

    private static final Log LOG = LogFactory.getLog(OperationMeasurementsMultiEditUIHandler.class);

    /**
     * <p>Constructor for OperationMeasurementsMultiEditUIHandler.</p>
     */
    public OperationMeasurementsMultiEditUIHandler() {
        super(
            OperationMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP,
            OperationMeasurementsGroupedRowModel.PROPERTY_TAXON,
            OperationMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS,
            OperationMeasurementsGroupedRowModel.PROPERTY_COMMENT
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractOperationMeasurementsGroupedTableModel<OperationMeasurementsGroupedRowModel> getTableModel() {
        return (OperationMeasurementsGroupedTableModel) getTable().getModel();
    }

    @Override
    protected OperationMeasurementsMultiEditUIModel createNewModel() {
        return new OperationMeasurementsMultiEditUIModel();
    }

    @Override
    protected OperationMeasurementsGroupedRowModel createNewRow(boolean readOnly, SamplingOperationDTO parentBean) {
        return new OperationMeasurementsGroupedRowModel(readOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getOperationGroupedMeasurementMultiEditTable();
    }

    /**
     * Initialisation du tableau.
     */
    @Override
    protected void initTable() {

        final TableColumnExt samplingCol = addFixedColumn(OperationMeasurementsGroupedTableModel.SAMPLING);
        samplingCol.setEditable(false);

        // Colonne groupe taxon
        addColumn(
            taxonGroupCellEditor,
            newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON_GROUP),
            OperationMeasurementsGroupedTableModel.TAXON_GROUP);

        // Colonne taxon
        addColumn(
            taxonCellEditor,
            newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON),
            OperationMeasurementsGroupedTableModel.TAXON);

        // Colonne taxon saisi
        final TableColumnExt colInputTaxon = addColumn(OperationMeasurementsGroupedTableModel.INPUT_TAXON_NAME);
        colInputTaxon.setEditable(false);

        // Colonne analyste
        addColumn(
            departmentCellEditor,
            newTableCellRender(AbstractOperationMeasurementsGroupedTableModel.ANALYST),
            AbstractOperationMeasurementsGroupedTableModel.ANALYST);

        addColumn(
            analysisInstrumentCellEditor,
            newTableCellRender(AbstractOperationMeasurementsGroupedTableModel.ANALYSIS_INSTRUMENT),
            AbstractOperationMeasurementsGroupedTableModel.ANALYSIS_INSTRUMENT
        );

        // No comment column because comment editor is also a JDialog
//        final TableColumnExt colComment = addCommentColumn(OperationMeasurementsGroupedTableModel.COMMENT);

        // Modele de la table
        final OperationMeasurementsGroupedTableModel tableModel = new OperationMeasurementsGroupedTableModel(getTable().getColumnModel(), false);
        tableModel.setNoneEditableCols();
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable());

        // border
        addEditionPanelBorder();
    }

    /**
     * Don't really build rows but create a multi edit row
     *
     * @param readOnly readOnly
     * @return rows from model
     */
    @Override
    protected List<OperationMeasurementsGroupedRowModel> buildRows(boolean readOnly) {

        if (getModel().getRowsToEdit().isEmpty())
            return null;

        // Create a ghost row without parent
        OperationMeasurementsGroupedRowModel multiEditRow = createNewRow(readOnly, null);
        multiEditRow.setIndividualPmfms(new ArrayList<>(getModel().getPmfms()));
        DaliBeans.createEmptyMeasurements(multiEditRow);

        boolean firstRow = true;
        for (OperationMeasurementsGroupedRowModel row : getModel().getRowsToEdit()) {

            // find multiple value on each known columns
            findMultipleValueOnIdentifier(row, multiEditRow, firstRow);

            // find multiple value on each measurement
            findMultipleValueOnPmfmIdentifier(row, multiEditRow, firstRow);

            firstRow = false;
        }

        overrideColumnRenderersAndEditors(multiEditRow);

        overrideColumnWidthAndPosition();

        return ImmutableList.of(multiEditRow);
    }

    // detect multiple values on each column
    private void findMultipleValueOnIdentifier(OperationMeasurementsGroupedRowModel row, OperationMeasurementsGroupedRowModel multiEditRow, boolean firstRow) {

        for (DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> identifier : getModel().getIdentifiersToCheck()) {
            if (!multiEditRow.getMultipleValuesOnIdentifier().contains(identifier)) {
                if (!Objects.equals(identifier.getValue(row), identifier.getValue(multiEditRow))) {
                    if (firstRow) {
                        // on first row, consider a unique value
                        identifier.setValue(multiEditRow, identifier.getValue(row));
                    } else {
                        // on other rows, if the value differs, set it as multiple
                        identifier.setValue(multiEditRow, null);
                        multiEditRow.addMultipleValuesOnIdentifier(identifier);
                    }
                }
            }
        }
    }

    // detect multiple values on each pmfm column
    private void findMultipleValueOnPmfmIdentifier(OperationMeasurementsGroupedRowModel row, OperationMeasurementsGroupedRowModel multiEditRow, boolean firstRow) {

        for (PmfmDTO pmfm : getModel().getPmfms()) {
            if (!multiEditRow.getMultipleValuesOnPmfmIds().contains(pmfm.getId())) {
                MeasurementDTO multiMeasurement = DaliBeans.getIndividualMeasurementByPmfmId(multiEditRow, pmfm.getId());
                Assert.notNull(multiMeasurement, "should be already present");
                MeasurementDTO measurement = DaliBeans.getIndividualMeasurementByPmfmId(row, pmfm.getId());
                if (measurement == null) {
                    // create ghost measurement
                    measurement = DaliBeanFactory.newMeasurementDTO();
                    measurement.setPmfm(pmfm);
                }

                if (!DaliBeans.measurementValuesEquals(multiMeasurement, measurement)) {
                    if (firstRow) {
                        // on first row, consider a unique value
                        multiMeasurement.setNumericalValue(measurement.getNumericalValue());
                        multiMeasurement.setQualitativeValue(measurement.getQualitativeValue());
                    } else {
                        // on other rows, if the value differs, set it as multiple
                        multiMeasurement.setNumericalValue(null);
                        multiMeasurement.setQualitativeValue(null);
                        multiEditRow.addMultipleValuesOnPmfmId(pmfm.getId());
                    }
                }
            }
        }
    }

    /**
     * Override all column renderers with specific behavior if multiple values is found
     *
     * @param multiEditRow
     */
    private void overrideColumnRenderersAndEditors(OperationMeasurementsGroupedRowModel multiEditRow) {

        multiEditRow.getMultipleValuesOnIdentifier().forEach(identifier -> {
            TableColumn col = getTable().getColumnExt(identifier);
            // override the renderer
            col.setCellRenderer(new MultipleValueCellRenderer(col.getCellRenderer()));
        });
        multiEditRow.getMultipleValuesOnPmfmIds().forEach(pmfmId ->
            getModel().getPmfmColumns().stream().filter(pmfmTableColumn -> pmfmTableColumn.getPmfmId() == pmfmId).findFirst()
                .ifPresent(pmfmTableColumn -> {
                        // override the renderer
                        pmfmTableColumn.setCellRenderer(new MultipleValueCellRenderer(pmfmTableColumn.getCellRenderer()));
                        // and set not editable if the pmfm is set as read-only
                        if (getModel().getReadOnlyPmfmIds().contains(pmfmId))
                            pmfmTableColumn.setEditable(false);
                    }
                ));
    }

    private void overrideColumnWidthAndPosition() {

        OperationMeasurementsGroupedTableUI parentUI = ((OperationMeasurementsGroupedTableUI) ApplicationUIUtil.getParentUI(getUI()));
        if (parentUI == null) {
            LOG.warn("The parent of class 'OperationMeasurementsGroupedTableUI' should be found");
            return;
        }

        {
            // get the state of the referent table
            State referentState = getContext().getSwingSession().findStateByComponentName(".*" + parentUI.getHandler().getTable().getName());

            // apply this state to the current table
            if (referentState instanceof SwingTableSessionState) {
                referentState.setState(getTable(), referentState);
            }
        }

        // get the state of the referent fixed table
        if (getFixedTable() != null && parentUI.getHandler().getFixedTable() != null) {
            State referentState = getContext().getSwingSession().findStateByComponentName(".*" + parentUI.getHandler().getFixedTable().getName());

            // apply this state to the current table
            if (referentState instanceof FixedSwingTableSessionState) {
                referentState.setState(getFixedTable(), referentState);
            }
        }
    }

    @Override
    protected void onRowsAdded(List<OperationMeasurementsGroupedRowModel> addedRows) {
        // Don't call super method because the row is already initialized in buildRows()
//        super.onRowsAdded(addedRows);
    }

    @Override
    protected void filterMeasurements() {
    }

    public void valid() {
        if (getModel().isValid()) {
            closeDialog();
        }
    }

    @Override
    public void cancel() {
        stopListenValidatorValid(getValidator());
        getModel().setValid(false);
        closeDialog();
    }
}
