package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared.AbstractOperationMeasurementsGroupedTableModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared.AbstractOperationMeasurementsGroupedTableUIModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

/**
 * Le modele pour le tableau des mesures des prelevements (ecran prelevements/mesure).
 */
public class OperationMeasurementsGroupedTableModel extends AbstractOperationMeasurementsGroupedTableModel<OperationMeasurementsGroupedRowModel> {

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public OperationMeasurementsGroupedTableModel(SwingTableColumnModel columnModel, boolean createNewRow) {
        super(columnModel, createNewRow);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationMeasurementsGroupedRowModel createNewRow() {
        return new OperationMeasurementsGroupedRowModel(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> getFirstColumnEditing() {
        return SAMPLING;
    }

    @Override
    public DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> getPmfmInsertPosition() {
        return SAMPLING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractOperationMeasurementsGroupedTableUIModel getTableUIModel() {
        return (AbstractOperationMeasurementsGroupedTableUIModel) super.getTableUIModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStateContext() {
        if (getTableUIModel().getSurvey() != null && getTableUIModel().getSurvey().getProgram() != null) {

            return SurveyDTO.PROPERTY_SAMPLING_OPERATIONS + '_'
                + SamplingOperationDTO.PROPERTY_INDIVIDUAL_PMFMS + '_'
                + getTableUIModel().getSurvey().getProgram().getCode();
        }

        return super.getStateContext();
    }

}
