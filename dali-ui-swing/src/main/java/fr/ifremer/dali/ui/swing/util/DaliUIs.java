package fr.ifremer.dali.ui.swing.util;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;

import java.awt.Component;
import java.awt.Dimension;

/**
 * @author peck7 on 04/07/2017.
 */
public class DaliUIs extends ApplicationUIUtil {

    /**
     * La largeur des combobox.
     */
    public static final int DALI_COMPONENT_WIDTH = 200;

    /**
     * La hauteur des combobox.
     */
    public static final int DALI_COMPONENT_HEIGHT = 30;

    /**
     * Size of a Dali double list
     */
    public static final int DALI_DOUBLE_LIST_SIZE = 260;

    /**
     * Size of a Dali checkbox column
     */
    public static final int DALI_CHECKBOX_WIDTH = 80;

    /**
     * Size of a Dali image thumbnail
     */
    public static final int DALI_IMAGE_WIDTH = 200;

    /**
     * Affecte une largeur et une hauteur a un composant.
     *
     * @param component La combobox
     */
    public static void forceComponentSize(Component component) {
        forceComponentSize(component, DALI_COMPONENT_WIDTH);
    }

    /**
     * Affecte une largeur et une hauteur a une combobox.
     *
     * @param component La combobox
     * @param width     La largeur
     */
    public static void forceComponentSize(Component component, int width) {
        forceComponentSize(component, width, DALI_COMPONENT_HEIGHT);
    }

    public static void forceComponentSize(Component component, int width, int height) {
        forceComponentSize(component, new Dimension(width, height));
    }

    public static void forceComponentSize(Component component, Dimension dimension) {
        component.setPreferredSize(dimension);
        component.setSize(component.getPreferredSize());
        component.setMaximumSize(component.getPreferredSize());
        component.setMinimumSize(component.getPreferredSize());
    }
}