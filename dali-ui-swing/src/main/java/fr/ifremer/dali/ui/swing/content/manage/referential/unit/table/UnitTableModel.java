package fr.ifremer.dali.ui.swing.content.manage.referential.unit.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class UnitTableModel extends AbstractDaliTableModel<UnitTableRowModel> {


	/**
	 * <p>Constructor for UnitTableModel.</p>
	 *
	 * @param createNewRowAllowed a boolean.
	 */
	public UnitTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
		super(columnModel, createNewRowAllowed, false);
	}

	/** Constant <code>NAME</code> */
	public static final DaliColumnIdentifier<UnitTableRowModel> NAME = DaliColumnIdentifier.newId(
			UnitTableRowModel.PROPERTY_NAME,
			n("dali.property.name"),
			n("dali.property.name"),
			String.class,
			true);

	/** Constant <code>STATUS</code> */
	public static final DaliColumnIdentifier<UnitTableRowModel> STATUS = DaliColumnIdentifier.newId(
			UnitTableRowModel.PROPERTY_STATUS,
			n("dali.property.status"),
			n("dali.property.status"),
			StatusDTO.class,
			true);

	/** Constant <code>SYMBOL</code> */
	public static final DaliColumnIdentifier<UnitTableRowModel> SYMBOL = DaliColumnIdentifier.newId(
			UnitTableRowModel.PROPERTY_SYMBOL,
			n("dali.property.pmfm.unit.symbol"),
			n("dali.property.pmfm.unit.symbol.tip"),
			String.class,
			true);

	public static final DaliColumnIdentifier<UnitTableRowModel> COMMENT = DaliColumnIdentifier.newId(
		UnitTableRowModel.PROPERTY_COMMENT,
		n("dali.property.comment"),
		n("dali.property.comment"),
		String.class,
		false);

	public static final DaliColumnIdentifier<UnitTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
		UnitTableRowModel.PROPERTY_CREATION_DATE,
		n("dali.property.date.creation"),
		n("dali.property.date.creation"),
		Date.class);

	public static final DaliColumnIdentifier<UnitTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
		UnitTableRowModel.PROPERTY_UPDATE_DATE,
		n("dali.property.date.modification"),
		n("dali.property.date.modification"),
		Date.class);


	/** {@inheritDoc} */
	@Override
	public UnitTableRowModel createNewRow() {
		return new UnitTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<UnitTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
