package fr.ifremer.dali.ui.swing.content.manage.program.programs;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.service.DaliBusinessException;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;

import java.util.List;

/**
 * Modele pour la zone des programmes.
 */
public class ProgramsTableUIModel extends AbstractDaliTableUIModel<ProgramDTO, ProgramsTableRowModel, ProgramsTableUIModel> {

    private boolean saveEnabled;
    public static final String PROPERTY_SAVE_ENABLED = "saveEnabled";

    private ProgramsUIModel parentModel;
    public static final String EVENT_SAVE_STRATEGIES = "saveStrategies";
    public static final String EVENT_REMOVE_LOCATIONS = "removeLocations";
    public static final String EVENT_RESELECT = "reselect";

    @Override
    public ProgramsTableRowModel addNewRow(ProgramDTO bean) {
        throw new DaliBusinessException("New Program is forbidden");
    }

    public boolean isSaveEnabled() {
        return saveEnabled;
    }

    public void setSaveEnabled(boolean saveEnabled) {
        this.saveEnabled = saveEnabled;
        firePropertyChange(PROPERTY_SAVE_ENABLED, null, saveEnabled);
    }

    public ProgramsUIModel getParentModel() {
        return parentModel;
    }

    public void setParentModel(ProgramsUIModel parentModel) {
        this.parentModel = parentModel;
    }

    public void fireSaveStrategies() {
        firePropertyChange(EVENT_SAVE_STRATEGIES, null, null);
    }

    public void fireRemoveLocations(List<Integer> locationIds) {
        firePropertyChange(EVENT_REMOVE_LOCATIONS, null, locationIds);
    }

    public void fireReselect() {
        firePropertyChange(EVENT_RESELECT, null, null);
    }

    public void clear() {
        setBeans(null);
    }
}
