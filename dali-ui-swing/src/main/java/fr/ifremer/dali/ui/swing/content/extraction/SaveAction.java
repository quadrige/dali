package fr.ifremer.dali.ui.swing.content.extraction;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliSaveAction;
import fr.ifremer.dali.ui.swing.content.extraction.list.ExtractionsRowModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Save action.
 */
public class SaveAction extends AbstractDaliSaveAction<ExtractionUIModel, ExtractionUI, ExtractionUIHandler> {

    private Collection<? extends ExtractionDTO> extractionsToSave;

    /**
     * <p>Constructor for SaveAction.</p>
     *
     * @param handler a {@link fr.ifremer.dali.ui.swing.content.extraction.ExtractionUIHandler} object.
     */
    public SaveAction(final ExtractionUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }
        // Model is valid
        if (!getModel().isValid()) {

            // Remove error
            displayErrorMessage(t("dali.action.save.errors.title"), t("dali.action.save.errors.remove"));

            // Stop saving
            return false;
        }

        // Selected extractions
        extractionsToSave = getUI().getExtractionsTable().getModel().getRows().stream().filter(ExtractionsRowModel::isDirty).collect(Collectors.toList());

        return !CollectionUtils.isEmpty(extractionsToSave);

    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        // Save observations
        getContext().getExtractionService().saveExtractions(extractionsToSave);
    }

    /** {@inheritDoc} */
    @Override
    protected List<AbstractBeanUIModel> getModelsToModify() {
        List<AbstractBeanUIModel> models = Lists.newArrayList();
        models.add(getModel().getExtractionsTableUIModel());
        models.add(getModel().getExtractionFiltersUIModel());
        models.add(getModel().getExtractionConfigUIModel());
        return models;
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getHandler().reloadComboBox();

        sendMessage(t("dali.action.save.extractions.success", extractionsToSave.size()));

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        super.releaseAction();

        extractionsToSave = null;
    }
}
