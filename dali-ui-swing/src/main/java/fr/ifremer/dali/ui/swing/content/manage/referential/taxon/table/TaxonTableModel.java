package fr.ifremer.dali.ui.swing.content.manage.referential.taxon.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.CitationDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonomicLevelDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class TaxonTableModel extends AbstractDaliTableModel<TaxonTableRowModel> {

    private final boolean readOnly;

    /**
     * <p>Constructor for TaxonTableModel.</p>
     *
     * @param createNewRowAllowed a boolean.
     */
    public TaxonTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
        readOnly = !createNewRowAllowed;
    }

    // name
    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> NAME = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class,
            true);

    // citation
    /** Constant <code>CITATION</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> CITATION = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_CITATION,
            n("dali.property.taxon.citation"),
            n("dali.property.taxon.citation"),
            CitationDTO.class);

    // reference taxon
    /** Constant <code>REFERENCE_TAXON</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> REFERENCE_TAXON = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_REFERENCE_TAXON,
            n("dali.property.taxon.reference"),
            n("dali.property.taxon.reference"),
            TaxonDTO.class,
            DecoratorService.WITH_CITATION);

    // parent taxon
    /** Constant <code>PARENT</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> PARENT = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_PARENT_TAXON,
            n("dali.property.taxon.parent"),
            n("dali.property.taxon.parent"),
            TaxonDTO.class,
            DecoratorService.WITH_CITATION,
            true);

    // level
    /** Constant <code>LEVEL</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> LEVEL = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_LEVEL,
            n("dali.property.taxon.level"),
            n("dali.property.taxon.level.tip"),
            TaxonomicLevelDTO.class,
            true);

    // comment
    /** Constant <code>COMMENT</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> COMMENT = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_COMMENT,
            n("dali.property.comment"),
            n("dali.property.comment"),
            String.class);

    // temporary
    /** Constant <code>TEMPORARY</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> TEMPORARY = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_TEMPORARY,
            n("dali.property.temporary"),
            n("dali.property.temporary"),
            Boolean.class);

    // obsolete
    /** Constant <code>OBSOLETE</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> OBSOLETE = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_OBSOLETE,
            n("dali.property.obsolete"),
            n("dali.property.obsolete"),
            Boolean.class/*,
            true*/);

    // virtual
    /** Constant <code>VIRTUAL</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> VIRTUAL = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_VIRTUAL,
            n("dali.property.virtual"),
            n("dali.property.virtual"),
            Boolean.class);

    // composites
    /** Constant <code>COMPOSITES</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> COMPOSITES = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_COMPOSITE_TAXONS,
            n("dali.property.taxon.composites"),
            n("dali.property.taxon.composites"),
            TaxonDTO.class,
            DecoratorService.COLLECTION_SIZE);

    // taxref
    /** Constant <code>TAXREF</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> TAXREF = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_TAX_REF,
            n("dali.property.taxon.taxRef"),
            n("dali.property.taxon.taxRef"),
            String.class);

    // worms
    /** Constant <code>WORMS</code> */
    public static final DaliColumnIdentifier<TaxonTableRowModel> WORMS = DaliColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_WORMS_REF,
            n("dali.property.taxon.worms"),
            n("dali.property.taxon.worms"),
            String.class);

    public static final DaliColumnIdentifier<TaxonTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        TaxonTableRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<TaxonTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        TaxonTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);



    /** {@inheritDoc} */
    @Override
    public TaxonTableRowModel createNewRow() {
        return new TaxonTableRowModel(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<TaxonTableRowModel> getFirstColumnEditing() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<TaxonTableRowModel> propertyName) {

        if (propertyName == COMPOSITES) {
            TaxonTableRowModel row = getEntry(rowIndex);
            return row.sizeCompositeTaxons() > 0;
        }

        return super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

}
