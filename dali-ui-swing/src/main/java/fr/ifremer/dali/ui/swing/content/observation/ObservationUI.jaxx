<!--
  #%L
  Dali :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
-->
<JPanel layout="{new BorderLayout()}" decorator='help' implements='fr.ifremer.dali.ui.swing.util.DaliUI&lt;ObservationUIModel, ObservationUIHandler&gt;'>
  <import>
    fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO

    fr.ifremer.dali.ui.swing.DaliHelpBroker
    fr.ifremer.dali.ui.swing.DaliUIContext
    fr.ifremer.dali.ui.swing.util.DaliUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
    fr.ifremer.quadrige3.ui.swing.plaf.ComponentBlockingLayerUI

    fr.ifremer.dali.ui.swing.content.observation.survey.SurveyDetailsTabUI
    fr.ifremer.dali.ui.swing.content.observation.operation.measurement.OperationMeasurementsTabUI
    fr.ifremer.dali.ui.swing.content.observation.photo.PhotosTabUI

    java.awt.BorderLayout
    java.awt.Font
    java.awt.Color

    javax.swing.Box
    javax.swing.BoxLayout
    javax.swing.SwingConstants
    javax.swing.border.EmptyBorder

    static org.nuiton.i18n.I18n.*
  </import>

  <!--getContextValue est une méthode interne JAXX-->
  <ObservationUIModel id='model' initializer='getContextValue(ObservationUIModel.class)'/>

  <DaliHelpBroker id='broker' constructorParams='"dali.home.help"'/>
  <ComponentBlockingLayerUI id='surveyBlockLayer'/>

  <script><![CDATA[

    private int tabIndex;

		//Le parent est très utile pour intervenir sur les frères
		public ObservationUI(ApplicationUI parentUI, int tabIndex) {
		    ApplicationUIUtil.setParentUI(this, parentUI);
		    setTabIndex(tabIndex);
		}

		public int getTabIndex() {
		  return this.tabIndex;
		}

    public void setTabIndex(Integer tabIndex) {
        this.tabIndex = tabIndex;
    }

	]]></script>

  <JPanel id="labelPanel" constraints="BorderLayout.PAGE_START" layout='{new BoxLayout(labelPanel, BoxLayout.LINE_AXIS)}' border="{new EmptyBorder(10,0,10,0)}">
    <JLabel id="observationLabel" border="{new EmptyBorder(0,5,0,5)}"/>
    <JLabel id="observationDescriptionLabel"/>
  </JPanel>

  <JTabbedPane id='observationTabbedPane' constraints='BorderLayout.CENTER' decorator='boxed'>
    <tab>
      <SurveyDetailsTabUI id='surveyDetailsTabUI' constructorParams='this'/>
    </tab>
    <!--<tab>-->
      <!--<SurveyMeasurementsTabUI id='surveyMeasurementsTabUI' constructorParams='this'/>-->
    <!--</tab>-->
    <tab>
      <OperationMeasurementsTabUI id='operationMeasurementsTabUI' constructorParams='this'/>
    </tab>
    <tab>
      <PhotosTabUI id='photosTabUI' constructorParams='this'/>
    </tab>
  </JTabbedPane>

  <JPanel layout='{new BorderLayout()}' constraints='BorderLayout.PAGE_END'>
    <JPanel layout='{new GridLayout(1,0)}' constraints='BorderLayout.CENTER'>
      <JButton id='closeButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
      <JButton id='saveButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
      <JButton id='nextButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
    </JPanel>
  </JPanel>

</JPanel>
