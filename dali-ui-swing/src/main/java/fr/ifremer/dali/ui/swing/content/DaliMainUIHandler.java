package fr.ifremer.dali.ui.swing.content;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.content.AbstractMainUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.URL;

/**
 * <p>DaliMainUIHandler class.</p>
 *
 * @since 0.1
 */
public class DaliMainUIHandler extends AbstractMainUIHandler<DaliUIContext, DaliMainUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(DaliMainUIHandler.class);

    @Override
    public DaliUIContext getContext() {
        return (DaliUIContext) super.getContext();
    }

    @Override
    public DaliConfiguration getConfig() {
        return getContext().getConfiguration();
    }

    @Override
    public void beforeInit(DaliMainUI ui) {
        super.beforeInit(ui);

        // override context value
        ui.setContextValue(getContext());
        ui.createModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(DaliMainUI ui) {

        super.afterInit(ui);

        ui.getMenuFile().add(ui.getMenuFileExit());

        ui.getMenuHelp().add(ui.getMenuChangeLocale());

    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    /**
     * <p>reloadUI.</p>
     */
//    public void reloadUI() {
//
//        //close ui
//        onCloseUI();
//
//        // restart ui
//        DaliApplication.reloadUI(getContext());
//    }

    /**
     * <p>gotoSite.</p>
     */
    public void gotoSite() {

        URL siteURL = getConfig().getSiteUrl();

        if (LOG.isDebugEnabled()) {
            LOG.debug("goto " + siteURL);
        }
        ApplicationUIUtil.openLink(siteURL);
    }

    /**
     * <p>showHelp.</p>
     */
    public void showHelp() {
        getContext().showHelp(ui, ui.getBroker(), null);
    }

}
