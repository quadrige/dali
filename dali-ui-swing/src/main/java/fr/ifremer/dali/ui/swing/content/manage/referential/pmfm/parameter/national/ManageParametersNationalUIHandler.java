package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.parameter.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.parameter.menu.ManageParametersMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.parameter.table.ParameterTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.parameter.table.ParameterTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.editor.AssociatedQualitativeValueCellEditor;
import fr.ifremer.dali.ui.swing.util.table.renderer.AssociatedQualitativeValueCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Parameters au niveau national
 */
public class ManageParametersNationalUIHandler extends
        AbstractDaliTableUIHandler<ParameterTableRowModel, ManageParametersNationalUIModel, ManageParametersNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageParametersNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageParametersNationalUIModel model = new ManageParametersNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(ManageParametersNationalUI ui) {
        initUI(ui);

        ManageParametersMenuUIModel menuUIModel = getUI().getMenuUI().getModel();

        //listen to results
        menuUIModel.addPropertyChangeListener(ManageParametersMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((Collection<ParameterDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // code
        final TableColumnExt codeCol = addColumn(ParameterTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(ParameterTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        final TableColumnExt descriptionCol = addColumn(ParameterTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // qualitative
        final TableColumnExt qualitativeCol = addBooleanColumnToModel(ParameterTableModel.IS_QUALITATIVE, table);
        qualitativeCol.setSortable(true);
        qualitativeCol.setEditable(false);

        // calculated
        final TableColumnExt calculatedCol = addBooleanColumnToModel(ParameterTableModel.IS_CALCULATED, table);
        calculatedCol.setSortable(true);
        calculatedCol.setEditable(false);

        // taxonomic
        final TableColumnExt taxonomicCol = addBooleanColumnToModel(ParameterTableModel.IS_TAXONOMIC, table);
        taxonomicCol.setSortable(true);
        taxonomicCol.setEditable(false);

        // Associated qualitative value
        final TableColumnExt associatedQualitativeValueCol = addColumn(
                new AssociatedQualitativeValueCellEditor(getTable(), getUI(), false),
                new AssociatedQualitativeValueCellRenderer(),
                ParameterTableModel.ASSOCIATED_QUALITATIVE_VALUE);
        associatedQualitativeValueCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(ParameterTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(ParameterTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(ParameterTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

// status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                ParameterTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // parameter group
        final TableColumnExt parameterGroupCol = addFilterableComboDataColumnToModel(
                ParameterTableModel.PARAMETER_GROUP,
                getContext().getReferentialService().getParameterGroup(StatusFilter.ACTIVE),
                false);
        parameterGroupCol.setSortable(true);
        parameterGroupCol.setEditable(false);

        ParameterTableModel tableModel = new ParameterTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.pmfm.parameter"), ParameterTableModel.ASSOCIATED_QUALITATIVE_VALUE);

        // Initialisation du tableau
        initTable(table, true);

        // hidden columns
        parameterGroupCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<ParameterTableRowModel> getTableModel() {
        return (ParameterTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getTable();
    }

}
