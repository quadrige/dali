package fr.ifremer.dali.ui.swing;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.ui.swing.action.StartAction;
import fr.ifremer.dali.ui.swing.content.DaliMainUI;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfiguration;
import fr.ifremer.quadrige3.ui.swing.Application;
import fr.ifremer.quadrige3.ui.swing.ApplicationSplashScreen;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIContext;
import fr.ifremer.quadrige3.ui.swing.plaf.FileChooserUI;
import fr.ifremer.quadrige3.ui.swing.plaf.WiderSynthComboBoxUI;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.factory.GeoTools;
import org.jdesktop.swingx.util.PaintUtils;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.UIResource;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Arrays;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>DaliApplication class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class DaliApplication extends Application {

    /* Logger */
    private static final Log LOG = LogFactory.getLog(DaliApplication.class);

    private static DaliUIContext context;
    private static DaliConfiguration config;
    private static DaliMainUI mainUI;

    public static void main(String... args) {

        DaliApplication application = new DaliApplication();
        application.start(args);
    }

    @Override
    protected boolean init(String... args) {

        if (LOG.isInfoEnabled()) {
            LOG.info("Starting DALI with arguments: " + Arrays.toString(args));
        }

        // Could override config file path (useful for dev)
        String configFile = "dali.config";
        if (System.getProperty(configFile) != null) {
            configFile = System.getProperty(configFile);
            configFile = configFile.replaceAll("\\\\", "/");
        }

        // Create configuration
        config = new DaliConfiguration(configFile, args);
        DaliConfiguration.setInstance(config);

        // Init and customize splash screen
        ApplicationSplashScreen.init(config);

        // Initialize GEOTOOLS library
        GeoTools.init();

        // Create application context
        context = DaliUIContext.newContext(config);
        ApplicationSplashScreen.progress(20);

        initLookAndFeel(context);
        ApplicationSplashScreen.progress(40);

        // prepare context (mainly init configs, i18n)
        context.init("dali");
        ApplicationSplashScreen.progress(60);

        return true;
    }

    @Override
    protected void show() {

        // Start this context
        startUI(context);

    }

    @Override
    public void restartUI() {

        initLookAndFeel(context);
        startUI(context);
    }

    @Override
    protected QuadrigeCoreConfiguration getConfig() {
        return config;
    }

    @Override
    protected ApplicationUIContext getContext() {
        return context;
    }

    private static void initLookAndFeel(DaliUIContext context) {
        /*
         * Override default color : http://docs.oracle.com/javase/tutorial/uiswing/lookandfeel/_nimbusDefaults.html
         */

        // background
        UIManager.put("control", Color.WHITE);
        UIManager.put("background", Color.WHITE);
        UIManager.put("nimbusSelection", context.getConfiguration().getColorSelectedRow());
        UIManager.put("nimbusDisabledText", Color.BLACK);

        // table
        UIManager.put("Table[Disabled+Selected].textBackground", context.getConfiguration().getColorSelectedRow());
        UIManager.put("Table[Enabled+Selected].textBackground", context.getConfiguration().getColorSelectedRow());
        UIResource cellBorder = new BorderUIResource.CompoundBorderUIResource(
            new BorderUIResource.LineBorderUIResource(context.getConfiguration().getColorSelectedCell()),
            new BorderUIResource.EmptyBorderUIResource(1, 4, 1, 4));
        UIManager.put("Table.focusCellHighlightBorder", cellBorder);

        // list
        UIManager.put("List.focusCellHighlightBorder", cellBorder);

        // default selection
        UIManager.put("nimbusSelectionBackground", context.getConfiguration().getColorSelectedRow());
        UIManager.put("nimbusSelectedText", PaintUtils.computeForeground(context.getConfiguration().getColorSelectedRow()));

        // default label
        UIManager.put("TitledBorder.titleColor", context.getConfiguration().getColorThematicLabel());
        UIManager.put("thematicLabelColor", context.getConfiguration().getColorThematicLabel());

        // inactive components
//        UIManager.put("TextArea.disabledText", new ColorUIResource(Color.BLACK));
//        UIManager.put("TextArea[Disabled].textForeground", new ColorUIResource(Color.BLACK));
//        UIManager.put("TextArea[Disabled+NotInScrollPane].textForeground", new ColorUIResource(Color.BLACK));
//        UIManager.put("ComboBox:\"ComboBox.textField\"[Disabled].textForeground", Color.BLACK);
//        UIManager.put("TextField.inactiveForeground", Color.BLACK);
//        UIManager.put("FormattedTextField.inactiveForeground", Color.BLACK);

        // Prepare ui look&feel and load ui properties
        try {
            SwingUtil.initNimbusLoookAndFeel();

        } catch (Exception e) {
            // could not find nimbus look-and-feel
            if (LOG.isWarnEnabled()) {
                LOG.warn("Failed to init nimbus look and feel", e);
            }
        }

        // Add specific UI for ComboBox
        UIManager.put("ComboBoxUI", WiderSynthComboBoxUI.class.getCanonicalName());

        // Override other L&F defaults
        UIManager.getLookAndFeelDefaults().put("control", Color.WHITE);
        UIManager.getLookAndFeelDefaults().put("background", Color.WHITE);

        // Set default button order (Mantis #43121)
        UIManager.getDefaults().put("OptionPane.isYesLast", true);
        UIManager.put("FileChooserUI", FileChooserUI.class.getCanonicalName());
    }

    private static void startUI(DaliUIContext context) {

        // control screen resolution
        int recommendedWidth = context.getConfiguration().getUIRecommendedWidth();
        int recommendedHeight = context.getConfiguration().getUIRecommendedHeight();
        if (recommendedWidth > 0 && recommendedHeight > 0) {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if (screenSize.getWidth() < recommendedWidth || screenSize.getHeight() < recommendedHeight) {
                context.getErrorHelper().showWarningDialog(t("dali.screen.resolution.warning", recommendedWidth, recommendedHeight));
            }
        }

        ApplicationSplashScreen.progress(80);
        mainUI = new DaliMainUI(context);
        context.addMessageNotifier(mainUI.getHandler());
        context.getSwingSession().add(mainUI, true);
        ApplicationSplashScreen.progress(100);

        ApplicationSplashScreen.waitFor();
        SwingUtilities.invokeLater(() -> {
            mainUI.setVisible(true);
            ApplicationSplashScreen.hide();
        });

        StartAction uiAction = context.getActionFactory().createLogicAction(mainUI.getHandler(), StartAction.class);
        context.getActionEngine().runAction(uiAction);
    }

}
