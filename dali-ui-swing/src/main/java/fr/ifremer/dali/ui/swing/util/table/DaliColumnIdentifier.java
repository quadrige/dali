package fr.ifremer.dali.ui.swing.util.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.dao.technical.decorator.Decorator;

/**
 * override version of DaliColumnIdentifier with property type
 *
 * @param <R>
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class DaliColumnIdentifier<R extends AbstractDaliRowUIModel> extends fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier<R> {

    /**
     * <p>Constructor for DaliColumnIdentifier.</p>
     *
     * @param propertyName     a {@link String} object.
     * @param headerI18nKey    a {@link String} object.
     * @param headerTipI18nKey a {@link String} object.
     * @param propertyType     a {@link Class} object.
     * @param decoratorName    a {@link String} object.
     * @param mandatory        a boolean.
     */
    protected DaliColumnIdentifier(String propertyName, String headerI18nKey, String headerTipI18nKey, Class propertyType, String decoratorName, boolean mandatory) {
        super(propertyName, headerI18nKey, headerTipI18nKey, propertyType, decoratorName, mandatory);
    }

    /**
     * <p>newId.</p>
     *
     * @param propertyName     a {@link java.lang.String} object.
     * @param headerI18nKey    a {@link java.lang.String} object.
     * @param headerTipI18nKey a {@link java.lang.String} object.
     * @param propertyType     a {@link java.lang.Class} object.
     * @param <R>              a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newId(String propertyName, String headerI18nKey, String headerTipI18nKey, Class propertyType) {
        return new DaliColumnIdentifier<>(propertyName, headerI18nKey, headerTipI18nKey, propertyType, null, false);
    }

    /**
     * <p>newId.</p>
     *
     * @param propertyName     a {@link java.lang.String} object.
     * @param headerI18nKey    a {@link java.lang.String} object.
     * @param headerTipI18nKey a {@link java.lang.String} object.
     * @param propertyType     a {@link java.lang.Class} object.
     * @param decoratorName    a {@link java.lang.String} object.
     * @param <R>              a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newId(String propertyName, String headerI18nKey, String headerTipI18nKey, Class propertyType,
                                                                                   String decoratorName) {
        return new DaliColumnIdentifier<>(propertyName, headerI18nKey, headerTipI18nKey, propertyType, decoratorName, false);
    }

    /**
     * <p>newId.</p>
     *
     * @param propertyName     a {@link java.lang.String} object.
     * @param headerI18nKey    a {@link java.lang.String} object.
     * @param headerTipI18nKey a {@link java.lang.String} object.
     * @param propertyType     a {@link java.lang.Class} object.
     * @param mandatory        a boolean.
     * @param <R>              a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newId(String propertyName, String headerI18nKey, String headerTipI18nKey, Class propertyType,
                                                                                   boolean mandatory) {
        return new DaliColumnIdentifier<>(propertyName, headerI18nKey, headerTipI18nKey, propertyType, null, mandatory);
    }

    /**
     * <p>newId.</p>
     *
     * @param propertyName     a {@link java.lang.String} object.
     * @param headerI18nKey    a {@link java.lang.String} object.
     * @param headerTipI18nKey a {@link java.lang.String} object.
     * @param propertyType     a {@link java.lang.Class} object.
     * @param decoratorName    a {@link java.lang.String} object.
     * @param mandatory        a boolean.
     * @param <R>              a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newId(String propertyName, String headerI18nKey, String headerTipI18nKey, Class propertyType,
                                                                                   String decoratorName, boolean mandatory) {
        return new DaliColumnIdentifier<>(propertyName, headerI18nKey, headerTipI18nKey, propertyType, decoratorName, mandatory);
    }

    /**
     * <p>newReadOnlyId.</p>
     *
     * @param propertyName     a {@link java.lang.String} object.
     * @param headerI18nKey    a {@link java.lang.String} object.
     * @param headerTipI18nKey a {@link java.lang.String} object.
     * @param propertyType     a {@link java.lang.Class} object.
     * @param <R>              a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newReadOnlyId(String propertyName, String headerI18nKey, String headerTipI18nKey, Class propertyType) {

        return newReadOnlyId(propertyName, headerI18nKey, headerTipI18nKey, propertyType, null);
    }

    /**
     * <p>newReadOnlyId.</p>
     *
     * @param propertyName     a {@link java.lang.String} object.
     * @param headerI18nKey    a {@link java.lang.String} object.
     * @param headerTipI18nKey a {@link java.lang.String} object.
     * @param propertyType     a {@link java.lang.Class} object.
     * @param decoratorName    a {@link java.lang.String} object.
     * @param <R>              a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newReadOnlyId(String propertyName, String headerI18nKey, String headerTipI18nKey, Class propertyType,
                                                                                           String decoratorName) {
        return new DaliColumnIdentifier<R>(propertyName, headerI18nKey, headerTipI18nKey, propertyType, decoratorName, false) {

            private static final long serialVersionUID = 1L;

            @Override
            public void setValue(R entry, Object value) {
                // no set
            }
        };
    }

    /**
     * <p>newPmfmNameId.</p>
     *
     * @param headerI18nKey    a {@link java.lang.String} object.
     * @param headerTipI18nKey a {@link java.lang.String} object.
     * @param <R>              a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newPmfmNameId(String headerI18nKey, String headerTipI18nKey) {

        return newPmfmNameId(null, headerI18nKey, headerTipI18nKey);
    }

    /**
     * <p>newPmfmNameId.</p>
     *
     * @param parentPropertyName a {@link java.lang.String} object.
     * @param headerI18nKey      a {@link java.lang.String} object.
     * @param headerTipI18nKey   a {@link java.lang.String} object.
     * @param <R>                a R object.
     * @return a {@link DaliColumnIdentifier} object.
     */
    public static <R extends AbstractDaliRowUIModel> DaliColumnIdentifier<R> newPmfmNameId(final String parentPropertyName, String headerI18nKey, String headerTipI18nKey) {

        final DecoratorService decoratorService = DaliServiceLocator.instance().getDecoratorService();

        return new DaliColumnIdentifier<R>(PmfmDTO.PROPERTY_NAME, headerI18nKey, headerTipI18nKey, String.class, null, false) {

            @Override
            public Object getValue(R entry) {

                PmfmDTO pmfm = null;
                if (parentPropertyName != null) {
                    pmfm = DaliBeans.getProperty(entry, parentPropertyName);
                } else if (entry instanceof PmfmDTO) {
                    pmfm = (PmfmDTO) entry;
                }

                if (pmfm != null) {

                    Decorator decorator = decoratorService.getDecoratorByType(PmfmDTO.class, DecoratorService.NAME);
                    if (decorator != null) {
                        return decorator.toString(pmfm);
                    }
                }

                return null;
            }

            @Override
            public void setValue(R entry, Object value) {
                // do nothing
            }

        };
    }

}
