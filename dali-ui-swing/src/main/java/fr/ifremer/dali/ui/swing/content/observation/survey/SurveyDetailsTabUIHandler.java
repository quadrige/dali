package fr.ifremer.dali.ui.swing.content.observation.survey;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.content.home.map.SurveysMapUI;
import fr.ifremer.dali.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.dali.ui.swing.content.observation.survey.history.QualificationHistoryUI;
import fr.ifremer.dali.ui.swing.content.observation.survey.history.ValidationHistoryUI;
import fr.ifremer.dali.ui.swing.content.observation.survey.measurement.ungrouped.SurveyMeasurementsUngroupedTableUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.dali.ui.swing.util.map.MapParentUIModel;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l'onglet observation general.
 */
public class SurveyDetailsTabUIHandler extends AbstractDaliUIHandler<SurveyDetailsTabUIModel, SurveyDetailsTabUI> implements TabHandler {

    /**
     * Constant <code>OBSERVERS_DOUBLE_LIST="observersDoubleList"</code>
     */
    public static final String OBSERVERS_DOUBLE_LIST = "observersDoubleList";
    /**
     * Constant <code>OBSERVERS_LIST="observersList"</code>
     */
    public static final String OBSERVERS_LIST = "observersList";

    private AbstractAction unfilterUsersAction;

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final SurveyDetailsTabUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SurveyDetailsTabUIModel model = new SurveyDetailsTabUIModel();
        model.setDateTimePattern(getConfig().getDateTimeFormat());
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final SurveyDetailsTabUI ui) {
        initUI(ui);

        getModel().setUngroupedTableUIModel(ui.getUngroupedTable().getModel());

        // Initialisation des combobox
        initComboBox();

        // Init double list
        initObserversLists();

        initListeners();

        registerValidators(getValidator());

    }

    private void initListeners() {

        // main listener on model
        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {
                case SurveyDetailsTabUIModel.PROPERTY_OBSERVATION_MODEL:
                    // Listener on parent model
                    load(getModel().getObservationModel());
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_OBSERVERS:

                    if (getModel().isAdjusting()) return;

                    // Reload selected observers
                    List<PersonDTO> observers = new ArrayList<>(getModel().getObservers());
                    getUI().getObserversDoubleList().getHandler().setSelected(observers);
                    getUI().getObserversList().setListData(observers.toArray(new PersonDTO[observers.size()]));
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_PROGRAM:
                    // reload locations combo
                    updateLocations(false);
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_LOCATION:
                    // update coordinate
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MIN_LATITUDE_EDITOR_NUMBER_VALUE);
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MAX_LATITUDE_EDITOR_NUMBER_VALUE);
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MIN_LONGITUDE_EDITOR_NUMBER_VALUE);
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MAX_LONGITUDE_EDITOR_NUMBER_VALUE);
                    // reload program combo
                    updatePrograms(false);
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_DATE:
                    // reload program combo
                    updatePrograms(false);
                    updateCampaigns(false);
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_COORDINATE:
                    // update coordinate
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_SURVEY_LATITUDE_MIN_EDITOR_NUMBER_VALUE);
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_SURVEY_LATITUDE_MAX_EDITOR_NUMBER_VALUE);
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_SURVEY_LONGITUDE_MIN_EDITOR_NUMBER_VALUE);
                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_SURVEY_LONGITUDE_MAX_EDITOR_NUMBER_VALUE);
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_LATITUDE_MIN:
                case SurveyDetailsTabUIModel.PROPERTY_LATITUDE_MAX:
                case SurveyDetailsTabUIModel.PROPERTY_LONGITUDE_MIN:
                case SurveyDetailsTabUIModel.PROPERTY_LONGITUDE_MAX:
                    // update positioning when all latitude and longitude are null
                    if (getModel().getLatitudeMin() == null && getModel().getLatitudeMax() == null
                            && getModel().getLongitudeMin() == null && getModel().getLongitudeMax() == null) {
                        getModel().setPositioning(null);
                    }
                    break;

//                case SurveyDetailsTabUIModel.PROPERTY_TIME:
//                    // update time editor
//                    getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_TIME_EDITOR_DATE);
//                    break;

                case SurveyDetailsTabUIModel.PROPERTY_POSITIONING:
                    // update positioning precision label
                    getUI().getPositioningPrecisionText().setText(getModel().getPositioningPrecision());
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_RECORDER_DEPARTMENT:
                    // update department
                    getUI().getRecorderDepartmentEditor().setText(decorate(getModel().getRecorderDepartment()));
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_QUALITY_LEVEL:
                    // update quality flag
                    getUI().getQualityFlagEditor().setText(decorate(getModel().getQualityLevel()));
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_EDITABLE:
                    // select list only if model is not editable
                    if (getModel().isEditable()) {
                        getUI().getObserversPanelLayout().setSelected(OBSERVERS_DOUBLE_LIST);
                    } else {
                        getUI().getObserversPanelLayout().setSelected(OBSERVERS_LIST);
                    }
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_LOADING:
                    // Propagate loading event
                    getModel().getUngroupedTableUIModel().setLoading((Boolean) evt.getNewValue());
                    break;

                case SurveyDetailsTabUIModel.PROPERTY_PMFM_STRATEGIES:
                    ObservationUIModel surveyModel = getModel().getObservationModel();
                    List<PmfmStrategyDTO> pmfmStrategies = DaliBeans.filterCollection(surveyModel.getPmfmStrategies(), input -> input != null && input.isSurvey());

                    // load pmfms for ungrouped measurements
                    List<PmfmDTO> pmfms = pmfmStrategies.stream()
                            .filter(pmfmStrategy -> !pmfmStrategy.isGrouping())
                            .map(PmfmStrategyDTO::getPmfm)
                            .collect(Collectors.toList());

                    // Load other Pmfms in survey
                    DaliBeans.fillListsEachOther(surveyModel.getPmfms(), pmfms);

                    // filter out pmfms under moratorium
                    filterPmfmsUnderMoratorium(pmfms, surveyModel);

                    // Set model properties
                    getModel().getUngroupedTableUIModel().setPmfms(pmfms);

                    break;
            }
        });

        // Add listeners on PROPERTY_ROWS_IN_ERROR to catch modifications and revalidate
        getModel().getUngroupedTableUIModel().addPropertyChangeListener(SurveyMeasurementsUngroupedTableUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            // re validate
            getValidator().doValidate();
        });

        listenModelModify(getModel().getUngroupedTableUIModel());

    }

    private void filterPmfmsUnderMoratorium(List<PmfmDTO> pmfms, ObservationUIModel survey) {
        if (CollectionUtils.isEmpty(survey.getPmfmsUnderMoratorium()))
            return;

        pmfms.removeIf(pmfm -> survey.getPmfmsUnderMoratorium().stream().anyMatch(moratoriumPmfm -> DaliBeans.isPmfmEquals(pmfm, moratoriumPmfm)));
    }

    /**
     * Load observation.
     *
     * @param surveyModel to load
     */
    private void load(final ObservationUIModel surveyModel) {

        Assert.notNull(surveyModel);

        getModel().setLoading(true);

        // Selected observation
        getModel().setBean(surveyModel);

        // Model not modified
        getModel().setModify(false);
        getModel().setLoading(false);

        // Listen validator
        listenValidatorValid(getValidator(), getModel());

    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        // Intialisation des campagnes
        initBeanFilterableComboBox(
                getUI().getCampaignCombo(),
                null,
                null);
        getUI().getCampaignCombo().setActionListener(e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            updateCampaigns(true);
        });
        updateCampaigns(false);

        // Initailisation des programmes
        initBeanFilterableComboBox(
                getUI().getProgramCombo(),
                null,
                null);
        getUI().getProgramCombo().setActionListener(e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            updatePrograms(true);
        });
        updatePrograms(false);

        // Initialisation des lieux
        initBeanFilterableComboBox(
                getUI().getLocationCombo(),
                null,
                null);
        getUI().getLocationCombo().setActionListener(e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            updateLocations(true);
        });
        updateLocations(false);

        // Initialisation des Positionnements
        initBeanFilterableComboBox(
                getUI().getPositioningCombo(),
                getContext().getReferentialService().getPositioningSystems(),
                null);

    }

    @SuppressWarnings("unchecked")
    private void initObserversLists() {

        // init list
        getUI().getObserversList().setCellRenderer(newListCellRender(PersonDTO.class));

        // init double list
        initBeanList(
                getUI().getObserversDoubleList(),
                new ArrayList<>(),
                new ArrayList<>(),
                300);

        // add unfilter action
        unfilterUsersAction = new AbstractAction("", SwingUtil.createActionIcon("unfilter")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateUsers(true);
            }
        };
        JButton unfilterUsersButton = new JButton();
        unfilterUsersButton.setAction(unfilterUsersAction);
        unfilterUsersButton.setToolTipText(t("dali.common.unfilter"));
        getUI().getObserversDoubleList().getFilterFieldLabel().setVisible(false);
        getUI().getObserversDoubleList().getToolbarLeft().add(unfilterUsersButton, 0);
        updateUsers(false);
    }

    private void updatePrograms(boolean forceNoFilter) {

        if (getModel().isAdjusting() || !getModel().isEditable()) {
            return;
        }
        getModel().setAdjusting(true);

        getUI().getProgramCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.PROGRAM));

        List<ProgramDTO> programs = getContext().getObservationService().getAvailablePrograms(
                getModel().getLocationId(),
                getModel().getDate(),
                forceNoFilter,
                true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        // Mantis #0029591 : Prevent set null when model is loading
        if (!getModel().isLoading() && getModel().getProgram() != null && !programs.contains(getModel().getProgram())) {
            getModel().setProgram(null);
        }
        getUI().getProgramCombo().setData(programs);

        getModel().setAdjusting(false);
    }

    private void updateCampaigns(boolean forceNoFilter) {

        if (getModel().isAdjusting() || !getModel().isEditable()) {
            return;
        }
        getModel().setAdjusting(true);

        getUI().getCampaignCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.CAMPAIGN));

        List<CampaignDTO> campaigns = getContext().getObservationService().getAvailableCampaigns(getModel().getDate(), forceNoFilter);

        // Mantis #0029591 : Prevent set null when model is loading
        if (!getModel().isLoading() && getModel().getCampaign() != null && !campaigns.contains(getModel().getCampaign())) {
            getModel().setCampaign(null);
        }
        getUI().getCampaignCombo().setData(campaigns);

        getModel().setAdjusting(false);
    }

    private void updateLocations(boolean forceNoFilter) {

        if (getModel().isAdjusting() || !getModel().isEditable()) {
            return;
        }
        getModel().setAdjusting(true);

        getUI().getLocationCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.LOCATION));

        List<LocationDTO> locations = getContext().getObservationService().getAvailableLocations(
                null, //                getModel().getCampaignId(),
                getModel().getProgramCode(),
                forceNoFilter, true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        // Mantis #0029591 : Prevent set null when model is loading
        if (!getModel().isLoading() && getModel().getLocation() != null && !locations.contains(getModel().getLocation())) {
            getModel().setLocation(null);
        }
        getUI().getLocationCombo().setData(locations);

        getModel().setAdjusting(false);
    }

    private void updateUsers(boolean forceNoFilter) {

        if (getModel().isAdjusting()) {
            return;
        }
        getModel().setAdjusting(true);

        unfilterUsersAction.setEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.USER));

        List<PersonDTO> users = getContext().getObservationService().getAvailableUsers(forceNoFilter);

        getUI().getObserversDoubleList().getHandler().setUniverse(users);

        getModel().setAdjusting(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingValidator<SurveyDetailsTabUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onShowTab(int currentIndex, int newIndex) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onRemoveTab() {
        return false;
    }

    /**
     * <p>saveActualModel.</p>
     */
    public void saveActualModel() {

        getModel().setAdjusting(true);

        // save observers to parent model
        getModel().setObservers(getModel().getSelectedObservers());

        getModel().setAdjusting(false);
    }

    public void showValidationHistory() {

        ValidationHistoryUI ui = new ValidationHistoryUI(getContext());
        ui.getModel().setBeans(getContext().getObservationService().getValidationHistory(getModel().getId()));
        openDialog(ui, new Dimension(800, 600));
    }

    public void showQualificationHistory() {

        QualificationHistoryUI ui = new QualificationHistoryUI(getContext());
        ui.getModel().setBeans(getContext().getObservationService().getQualificationHistory(getModel().getId()));
        openDialog(ui, new Dimension(800, 600));
    }

    private JFrame mapFrame;
    private SurveysMapUI mapUI;

    public void showMap() {

        // if needed
        closeMap();

        mapUI = new SurveysMapUI(getUI());
        getModel().setMapUIModel(mapUI.getModel());
        mapUI.getModel().setSelectedSurvey(getModel());
        getModel().addPropertyChangeListener(MapParentUIModel.EVENT_CLOSE_FULLSCREEN, evt -> closeMap());
        mapUI.getHandler().fullScreen();

        mapFrame = createFrame(mapUI, t("dali.home.map.title"), null, getUI().getBounds());

        SwingUtilities.invokeLater(() -> {
            mapFrame.setExtendedState(mapFrame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
            mapFrame.setVisible(true);
            mapUI.getHandler().openMap();
        });
    }

    private void closeMap() {
        if (mapUI != null) {
            mapUI.getHandler().onCloseUI();
            mapUI = null;
        }
        if (mapFrame != null) {
            mapFrame.dispose();
        }
    }

    @Override
    public void onCloseUI() {
        closeMap();
        super.onCloseUI();
    }
}
