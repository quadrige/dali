package fr.ifremer.dali.ui.swing.util.plaf;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.service.DaliTechnicalException;

import javax.swing.*;
import javax.swing.plaf.nimbus.AbstractRegionPainter;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.Color;
import java.lang.reflect.Field;

/**
 * platform look&feel util class
 *
 * @author peck7 on 20/10/2017.
 */
public class PlafUtils {

    public static void changeBackground(JComponent component, Color color) {

        if (component instanceof JSpinner) {

            // get painter
            if (isNimbusLAF()) {
                UIDefaults defaults = new UIDefaults();
                defaults.putAll(UIManager.getLookAndFeelDefaults());
                Object painter = defaults.get("Spinner:Panel:\"Spinner.formattedTextField\"[Enabled].backgroundPainter");

                if (painter instanceof AbstractRegionPainter) {
                    AbstractRegionPainter abstractRegionPainter = (AbstractRegionPainter) painter;
                    // get paint context by reflexion
                    try {
                        Field contextField = abstractRegionPainter.getClass().getDeclaredField("ctx");
                        contextField.setAccessible(true);

                        SpinnerTextFieldPainter newEnabledPainter = new SpinnerTextFieldPainter(
                                contextField.get(abstractRegionPainter),
                                SpinnerTextFieldPainter.BACKGROUND_ENABLED,
                                color);

                        SpinnerTextFieldPainter newFocusedPainter = new SpinnerTextFieldPainter(
                                contextField.get(abstractRegionPainter),
                                SpinnerTextFieldPainter.BACKGROUND_FOCUSED,
                                color);

                        defaults.put("Spinner:Panel:\"Spinner.formattedTextField\"[Enabled].backgroundPainter", newEnabledPainter);
                        defaults.put("Spinner:Panel:\"Spinner.formattedTextField\"[Focused].backgroundPainter", newFocusedPainter);

                        JSpinner spinner = (JSpinner) component;
                        JTextField textField = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
                        textField.putClientProperty("Nimbus.Overrides", defaults);
                        textField.putClientProperty("Nimbus.Overrides.InheritDefaults", false);
                        textField.updateUI();

                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        throw new DaliTechnicalException(e);
                    }
                }
            }

        } else if (component instanceof JComboBox) {

            // get painter
            if (isNimbusLAF()) {
                UIDefaults defaults = new UIDefaults();
                defaults.putAll(UIManager.getLookAndFeelDefaults());
                Object painter = defaults.get("ComboBox:\"ComboBox.textField\"[Enabled].backgroundPainter");

                if (painter instanceof AbstractRegionPainter) {
                    AbstractRegionPainter abstractRegionPainter = (AbstractRegionPainter) painter;
                    // get paint context by reflexion
                    try {
                        Field contextField = abstractRegionPainter.getClass().getDeclaredField("ctx");
                        contextField.setAccessible(true);

                        ComboBoxTextFieldPainter newEnabledPainter = new ComboBoxTextFieldPainter(
                                contextField.get(abstractRegionPainter),
                                ComboBoxTextFieldPainter.BACKGROUND_ENABLED,
                                color);

                        ComboBoxTextFieldPainter newSelectedPainter = new ComboBoxTextFieldPainter(
                                contextField.get(abstractRegionPainter),
                                ComboBoxTextFieldPainter.BACKGROUND_SELECTED,
                                color);

                        defaults.put("ComboBox:\"ComboBox.textField\"[Enabled].backgroundPainter", newEnabledPainter);
                        defaults.put("ComboBox:\"ComboBox.textField\"[Selected].backgroundPainter", newSelectedPainter);

                        JTextField textField = (JTextField) ((JComboBox) component).getEditor().getEditorComponent();
                        textField.putClientProperty("Nimbus.Overrides", defaults);
                        textField.putClientProperty("Nimbus.Overrides.InheritDefaults", false);
                        textField.updateUI();

                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        throw new DaliTechnicalException(e);
                    }
                }
            }

        }

    }

    public static boolean isNimbusLAF() {
        return UIManager.getLookAndFeel() instanceof NimbusLookAndFeel;
    }
}
