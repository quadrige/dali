package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped;

import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.quadrige3.core.dao.technical.AlphanumericComparator;

import java.util.Comparator;

/**
 * @author peck7 on 03/07/2020.
 */
public class SamplingOperationComparator implements Comparator<SamplingOperationDTO> {

    public static final SamplingOperationComparator instance = new SamplingOperationComparator();

    @Override
    public int compare(SamplingOperationDTO o1, SamplingOperationDTO o2) {
        if (o1 == o2) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return AlphanumericComparator.instance().compare(o1.getName(), o2.getName());
    }
}
