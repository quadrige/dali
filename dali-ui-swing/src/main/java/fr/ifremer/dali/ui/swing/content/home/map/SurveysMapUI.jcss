/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
#surveysMapUI {
	border: {BorderFactory.createTitledBorder(t("dali.home.map.title"))};
	_panelType: {DaliUI.EDITION_PANEL_TYPE};
}

#mapPane {
    border: {BorderFactory.createLineBorder(Color.BLACK)};
}

#mapBlockLayer {
    blockingColor: {handler.getConfig().getColorBlockingLayer()};
    block:{model.isLoading()};
}

#defaultToolButton {
    actionIcon: select;
	toolTipText: "dali.action.map.tool.default.tip";
}

#zoomInToolButton {
    actionIcon: zoomIn;
	toolTipText: "dali.action.map.tool.zoomIn.tip";
}

#zoomOutToolButton {
    actionIcon: zoomOut;
	toolTipText: "dali.action.map.tool.zoomOut.tip";
}

#resetButton {
    actionIcon: map-full;
	toolTipText: "dali.action.map.reset.tip";
}

#zoomFitButton {
    actionIcon: map-center;
	toolTipText: "dali.action.map.zoomFit.tip";
}

#offlineButton {
    actionIcon: map-offline;
    toolTipText: "dali.action.map.toggleOffline.tip";
}

#graticuleButton {
    actionIcon: graticule;
    toolTipText: "dali.action.map.toggleGraticule.tip";
}

#legendButton {
    actionIcon: about;
    toolTipText: "dali.action.map.toggleLegend.tip";
}

#fullScreenButton {
    actionIcon: fullScreen;
    toolTipText: "dali.action.map.fullScreen.tip";
}

#mapPositionLabel {
    border: {BorderFactory.createEmptyBorder(0,5,0,0)};
}

#spacer1 {
    border: {BorderFactory.createEmptyBorder(10,0,0,0)};
}

#spacer2 {
    border: {BorderFactory.createEmptyBorder(10,0,0,0)};
}

#loadingIndicator {
    border: {BorderFactory.createEmptyBorder(0,5,0,5)};
    icon: {SwingUtil.createImageIcon("loading.gif")};
    visible: false;
}
