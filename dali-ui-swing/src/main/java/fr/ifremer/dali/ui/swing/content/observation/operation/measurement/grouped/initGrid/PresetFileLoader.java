package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.gson.Gsons;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.vo.PresetVO;

import java.io.File;

/**
 * @author peck7 on 20/07/2017.
 */
public class PresetFileLoader {

    private static final String PRESET_FILE_PATTERN = "Preset_%s.json";

    public static PresetVO load(String programCode) {

        return Gsons.deserializeFile(getFile(programCode), PresetVO.class);

    }

    public static void save(PresetVO pmfmInit) {

        Gsons.serializeToFile(pmfmInit, getFile(pmfmInit.getProgramCode()));

    }

    public static File getFile(String programCode) {

        File dir = DaliConfiguration.getInstance().getMeasurementGridPresetsDirectory();
        if (!dir.isDirectory()) dir.mkdir();

        return new File(dir, String.format(PRESET_FILE_PATTERN, programCode));
    }

}
