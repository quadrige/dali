package fr.ifremer.dali.ui.swing.content.manage.program.locations;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.time.LocalDate;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class LocationsTableModel extends AbstractDaliTableModel<LocationsTableRowModel> {

	/**
	 * Identifiant pour la colonne code = location id
	 */
    public static final DaliColumnIdentifier<LocationsTableRowModel> CODE = DaliColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_ID,
            n("dali.property.code"),
            n("dali.program.location.code.tip"),
            Integer.class);

	/**
	 * Identifiant pour la colonne label.
	 */
    public static final DaliColumnIdentifier<LocationsTableRowModel> LABEL = DaliColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_LABEL,
            n("dali.property.label"),
            n("dali.program.location.label.tip"),
            String.class);
    
    /**
     * Identifiant pour la colonne name.
     */
    public static final DaliColumnIdentifier<LocationsTableRowModel> NAME = DaliColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_NAME,
            n("dali.program.location.name.short"),
            n("dali.program.location.name.tip"),
            String.class);

    /**
     * Identifiant pour la colonne date debut.
     */
    public static final DaliColumnIdentifier<LocationsTableRowModel> START_DATE = DaliColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_START_DATE,
            n("dali.program.location.startDate.short"),
            n("dali.program.location.startDate.tip"),
            LocalDate.class);

    /**
     * Identifiant pour la colonne date fin.
     */
    public static final DaliColumnIdentifier<LocationsTableRowModel> END_DATE = DaliColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_END_DATE,
            n("dali.program.location.endDate.short"),
            n("dali.program.location.endDate.tip"),
            LocalDate.class);

    /**
     * Identifiant pour la colonne preleveur.
     */
    public static final DaliColumnIdentifier<LocationsTableRowModel> SAMPLING_DEPARTMENT = DaliColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_SAMPLING_DEPARTMENT,
            n("dali.program.location.samplingDepartment"),
            n("dali.program.location.samplingDepartment.tip"),
            DepartmentDTO.class);

    /**
     * Identifiant pour la colonne analyste.
     */
    public static final DaliColumnIdentifier<LocationsTableRowModel> ANALYSIS_DEPARTMENT = DaliColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_ANALYSIS_DEPARTMENT,
            n("dali.program.location.analysisDepartment"),
            n("dali.program.location.analysisDepartment.tip"),
            DepartmentDTO.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	LocationsTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public LocationsTableRowModel createNewRow() {
		return new LocationsTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<LocationsTableRowModel> getFirstColumnEditing() {
		return CODE;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<LocationsTableRowModel> propertyName) {

		if (propertyName == SAMPLING_DEPARTMENT || propertyName == ANALYSIS_DEPARTMENT) {
			LocationsTableRowModel rowModel = getEntry(rowIndex);
			return rowModel.getStartDate() != null && rowModel.getEndDate() != null;
		}

		return super.isCellEditable(rowIndex, columnIndex, propertyName);
	}
}
