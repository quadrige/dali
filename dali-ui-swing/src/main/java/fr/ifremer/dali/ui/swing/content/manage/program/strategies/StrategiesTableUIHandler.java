package fr.ifremer.dali.ui.swing.content.manage.program.strategies;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.dali.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import jaxx.runtime.SwingUtil;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SortOrder;
import javax.swing.SwingWorker;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class StrategiesTableUIHandler extends AbstractDaliTableUIHandler<StrategiesTableRowModel, StrategiesTableUIModel, StrategiesTableUI> {

    private PmfmAndAppliedStrategyLoader pmfmAndAppliedStrategyLoader;

    /**
     * <p>Constructor for StrategiesTableUIHandler.</p>
     */
    public StrategiesTableUIHandler() {
        super(StrategiesTableRowModel.PROPERTY_NAME,
                StrategiesTableRowModel.PROPERTY_COMMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<StrategiesTableRowModel> getTableModel() {
        return (StrategiesTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getStrategiesTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final StrategiesTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final StrategiesTableUIModel model = new StrategiesTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(StrategiesTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initTable();
        SwingUtil.setLayerUI(ui.getTableScrollPane(), ui.getTableBlockLayer());

        // Ajout des listeners
        initListeners();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(final StrategiesTableRowModel row) {
        row.getErrors().clear();
        return (!row.isEditable() || super.isRowValid(row)) && isStrategyValid(row);
    }

    private boolean isStrategyValid(StrategiesTableRowModel row) {

        // no need to check duplicates if number of rows < 2
        if (getModel().getRowCount() >= 2) {

            for (StrategiesTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equals(otherRow.getName())) {
                    // duplicate found
                    DaliBeans.addError(row, t("dali.program.strategy.name.duplicates"), StrategiesTableRowModel.PROPERTY_NAME);
                }
            }
        }

        if (row.isAppliedStrategiesLoaded()) {

            Map<Integer, AppliedStrategyDTO> validAppliedStrategies = Maps.newHashMap();

            // check at least 1 applied strategy has date range
            boolean isStrategyApplied = false;
            if (!row.isAppliedStrategiesEmpty()) {
                for (AppliedStrategyDTO appliedStrategy : row.getAppliedStrategies()) {
                    if (appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null) {
                        isStrategyApplied = true;
                        validAppliedStrategies.put(appliedStrategy.getId(), appliedStrategy);
                    }
                }
            }
            if (!isStrategyApplied) {
                DaliBeans.addError(row, t("dali.programs.validation.error.noAppliedPeriod"), StrategiesTableRowModel.PROPERTY_NAME);
            } else {

                // check there is no crossing strategies
                Multimap<Integer, AppliedStrategyDTO> otherAppliedStrategiesByLocationId = ArrayListMultimap.create();
                for (StrategiesTableRowModel otherRow : getModel().getRows()) {

                    if (otherRow == row) continue;

                    // build map of other applied strategies not in this row
                    for (AppliedStrategyDTO appliedStrategy : otherRow.getAppliedStrategies()) {
                        if (validAppliedStrategies.containsKey(appliedStrategy.getId()) && appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null) {
                            otherAppliedStrategiesByLocationId.put(appliedStrategy.getId(), appliedStrategy);
                        }
                    }
                }

                // exploit map to found overlapping
                for (Integer appliedStrategyId : validAppliedStrategies.keySet()) {
                    boolean overlappingFound = false;
                    AppliedStrategyDTO appliedStrategy = validAppliedStrategies.get(appliedStrategyId);
                    Collection<AppliedStrategyDTO> otherAppliedStrategies = otherAppliedStrategiesByLocationId.get(appliedStrategyId);
                    for (AppliedStrategyDTO otherAppliedStrategy : otherAppliedStrategies) {

                        if (appliedStrategy.getStartDate().compareTo(otherAppliedStrategy.getEndDate()) <= 0
                                && otherAppliedStrategy.getStartDate().compareTo(appliedStrategy.getEndDate()) <= 0) {

                            // overlapping found
                            overlappingFound = true;
                            break;
                        }

                    }

                    if (overlappingFound) {
                        DaliBeans.addError(row, t("dali.programs.validation.error.overlappingPeriods"), StrategiesTableRowModel.PROPERTY_NAME);
                        break;
                    }
                }

                // Check other applied period errors
                for (AppliedStrategyDTO appliedStrategy : row.getAppliedStrategies()) {
                    if (!appliedStrategy.isErrorsEmpty()) {
                        DaliBeans.addError(row, t("dali.programs.validation.error.appliedStrategies"), StrategiesTableRowModel.PROPERTY_NAME);
                    }
                }
            }
        }

        // check pmfm integrity
        if (row.isPmfmStrategiesLoaded()) {

            for (PmfmStrategyDTO pmfmStrategy : row.getPmfmStrategies()) {
                if (!pmfmStrategy.isSurvey() && !pmfmStrategy.isSampling()) {
                    DaliBeans.addError(row, t("dali.program.tables.error"), StrategiesTableRowModel.PROPERTY_NAME);
                }

            }
        }

        return row.isErrorsEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, StrategiesTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        saveToProgram();
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{StrategiesTableRowModel.PROPERTY_APPLIED_STRATEGIES,
                StrategiesTableRowModel.PROPERTY_APPLIED_STRATEGIES_LOADED,
                StrategiesTableRowModel.PROPERTY_PMFM_STRATEGIES,
                StrategiesTableRowModel.PROPERTY_PMFM_STRATEGIES_LOADED,
                StrategiesTableRowModel.PROPERTY_ERRORS};
    }

    /**
     * Load strategies from program
     *
     * @param selectedProgram a {@link fr.ifremer.dali.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public void load(ProgramsTableRowModel selectedProgram) {

        getModel().setProgramStatus(selectedProgram.getStatus());
        getModel().setBeans(selectedProgram.getStrategies());
        getModel().setEditable(selectedProgram.isEditable());
        getModel().setLoading(false);
        getModel().setLoaded(true);
        recomputeRowsValidState();
    }

    private ProgramsUI getProgramsUI() {
        return getUI().getParentContainer(ProgramsUI.class);
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(StrategiesTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            // Get selected program and strategies
            final ProgramsTableRowModel program = getModel().getParentModel().getProgramsUIModel().getSingleSelectedRow();
            final StrategiesTableRowModel strategy = getModel().getSingleSelectedRow();

            // If only once selected
            if (program != null && strategy != null) {

                // load applied strategies
                if (pmfmAndAppliedStrategyLoader != null && !pmfmAndAppliedStrategyLoader.isDone()) {
                    pmfmAndAppliedStrategyLoader.cancel(true);
                }
                getModel().getParentModel().getLocationsUIModel().setLoading(true);
                getModel().getParentModel().getPmfmsUIModel().setLoading(true);
                pmfmAndAppliedStrategyLoader = new PmfmAndAppliedStrategyLoader(program, strategy);
                pmfmAndAppliedStrategyLoader.execute();

            } else {

                // Suppression des PSFMs
                getModel().getParentModel().getPmfmsUIModel().clear();
            }
        });

        getModel().addPropertyChangeListener(StrategiesTableUIModel.EVENT_SAVE_APPLIED_STRATEGIES, evt -> {
            if (getModel().getSingleSelectedRow() != null) {
                getModel().getSingleSelectedRow().setAppliedStrategies(getModel().getParentModel().getLocationsUIModel().getBeans());
                getModel().getSingleSelectedRow().setAppliedStrategiesLoaded(true);
            }
        });
        getModel().addPropertyChangeListener(StrategiesTableUIModel.EVENT_SAVE_PMFM_STRATEGIES, evt -> {
            if (getModel().getSingleSelectedRow() != null) {
                getModel().getSingleSelectedRow().setPmfmStrategies(getModel().getParentModel().getPmfmsUIModel().getBeans());
                getModel().getSingleSelectedRow().setPmfmStrategiesLoaded(true);

                // tell also applied strategies tables to refresh rows validity
                getModel().getParentModel().getLocationsUIModel().fireValidateRows();
            }
        });

        getModel().addPropertyChangeListener(StrategiesTableUIModel.EVENT_VALIDATE_ROWS, evt -> recomputeRowsValidState());
        //noinspection unchecked
        getModel().addPropertyChangeListener(StrategiesTableUIModel.EVENT_REMOVE_LOCATIONS, evt -> removeLocations((List<Integer>) evt.getNewValue()));

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        final SwingTable table = getTable();

        // Colonne libelle
        final TableColumnExt colonneLibelle = addColumn(
                StrategiesTableModel.NAME);
        colonneLibelle.setSortable(true);

        // Colonne description
        TableColumnExt colonneDescription = addCommentColumn(StrategiesTableModel.COMMENT);

        final StrategiesTableModel tableModel = new StrategiesTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Les colonnes obligatoire sont toujours presentes
        colonneLibelle.setHideable(false);
        colonneDescription.setHideable(false);

        // Initialisation de la table
        initTable(table);

        // Number rows visible
        table.setVisibleRowCount(4);

        // Tri par defaut
        table.setSortOrder(StrategiesTableModel.NAME, SortOrder.ASCENDING);
    }

    /**
     * <p>saveToProgram.</p>
     */
    private void saveToProgram() {

        if (getModel().isLoading()) return;

        // programs contains strategies, strategies contains pmfm
        // save modification on master object
        getModel().getParentModel().getProgramsUIModel().fireSaveStrategies();

        // force model modify
        recomputeRowsValidState();
        getModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<StrategiesTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            StrategiesTableRowModel row = addedRows.get(0);

            getModel().setModify(true);

            setFocusOnCell(row);

            saveToProgram();
        }
    }

    /**
     * <p>removeStrategies.</p>
     */
    void removeStrategies() {

        if (getModel().getSelectedRows().isEmpty())
            return;

        if (!checkExistingSurveysInsideRemovedStrategy()) {

            getContext().getDialogHelper().showErrorDialog(
                    t("dali.program.location.period.surveyInsidePeriod"),
                    t("dali.program.strategy.delete.titre")
            );

            return;
        }

        // Demande de confirmation avant la suppression
        if (askBeforeDelete(t("dali.program.strategy.delete.titre"), t("dali.program.strategy.delete.message"))) {

            // Suppression des lignes
            getModel().deleteSelectedRows();

            // Suppression des psfms du tableau
            getModel().getParentModel().getPmfmsUIModel().clear();

            saveToProgram();

            // Reselect the program to refresh location/applied strategies table
            getModel().getParentModel().getProgramsUIModel().fireReselect();

        }

    }

    void removeLocations(List<Integer> locationIds) {
        getModel().getRows().stream().filter(strategy -> !strategy.isAppliedStrategiesEmpty())
            .forEach(strategy -> strategy.getAppliedStrategies().removeIf(appliedStrategy -> locationIds.contains(appliedStrategy.getId())));
    }


    /**
     * check if strategies to delete have data inside their applied periods (Mantis #48011)
     *
     * @return true if no data inside strategies, delete is allowed
     */
    private boolean checkExistingSurveysInsideRemovedStrategy() {

        getModel().setLoading(true);
        try {

            List<AppliedStrategyDTO> appliedStrategies = getModel().getSelectedBeans().stream()
                    .filter(strategyDTO -> strategyDTO.getId() != null)
                    .flatMap(strategyDTO -> strategyDTO.getAppliedStrategies().stream())
                    .filter(appliedStrategyDTO -> appliedStrategyDTO.getStartDate() != null && appliedStrategyDTO.getEndDate() != null)
                    .collect(Collectors.toList());

            if (appliedStrategies.isEmpty()) return true;

            String programCode = getModel().getParentModel().getProgramsUIModel().getSingleSelectedRow().getCode();

            return appliedStrategies.stream().noneMatch(appliedStrategy ->
                    getContext().getObservationService().countSurveysWithProgramLocationAndInsideDates(
                            programCode,
                            appliedStrategy.getAppliedStrategyId(),
                            appliedStrategy.getId(),
                            appliedStrategy.getStartDate(),
                            appliedStrategy.getEndDate()
                    ) > 0
            );

        } finally {
            getModel().setLoading(false);
        }
    }

    private class PmfmAndAppliedStrategyLoader extends SwingWorker<Object, Object> {

        private final ProgramsUI adminProgrammeUI = getProgramsUI();
        private final ProgramsTableRowModel selectedProgram;
        private final StrategiesTableRowModel selectedStrategy;

        PmfmAndAppliedStrategyLoader(ProgramsTableRowModel selectedProgram, StrategiesTableRowModel selectedStrategy) {
            this.selectedProgram = selectedProgram;
            this.selectedStrategy = selectedStrategy;
        }

        @Override
        protected Object doInBackground() {

            // Make sure applied strategies and pmfm strategies are loaded into the selected strategy
            getContext().getProgramStrategyService().loadAppliedPeriodsAndPmfmStrategies(selectedProgram, selectedStrategy);

            return null;
        }

        @Override
        protected void done() {
            if (isCancelled()) {
                return;
            }
            try {
                get();
            } catch (InterruptedException | ExecutionException e) {
                throw new DaliTechnicalException(e.getMessage(), e);
            }

            // loading locations
            adminProgrammeUI.getLocationsTableUI().getHandler().loadAppliedStrategiesFromStrategy(selectedProgram, selectedStrategy);

            // loading pmfms
            adminProgrammeUI.getPmfmsTableUI().getHandler().load(selectedProgram, selectedStrategy);

        }
    }

}
