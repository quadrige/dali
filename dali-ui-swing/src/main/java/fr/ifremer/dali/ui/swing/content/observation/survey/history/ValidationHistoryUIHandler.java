package fr.ifremer.dali.ui.swing.content.observation.survey.history;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.renderer.MultiLineStringCellRenderer;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableRowHeightController;

import javax.swing.SortOrder;

/**
 * @author peck7 on 26/09/2017.
 */
public class ValidationHistoryUIHandler extends AbstractDaliTableUIHandler<ValidationHistoryRowModel, ValidationHistoryUIModel, ValidationHistoryUI> {

    @Override
    public void beforeInit(ValidationHistoryUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new ValidationHistoryUIModel());
    }

    @Override
    public void afterInit(ValidationHistoryUI validationHistoryUI) {

        initUI(validationHistoryUI);
        initTable();
    }

    private void initTable() {

        SwingTable table = getTable();

        TableColumnExt dateCol = addColumn(
                null,
                newDateCellRenderer(getConfig().getDateTimeFormat()),
                ValidationHistoryTableModel.DATE);
        fixColumnWidth(dateCol, 110);

        addColumn(
                null,
                new MultiLineStringCellRenderer(),
                ValidationHistoryTableModel.COMMENT);

        addColumn(ValidationHistoryTableModel.RECORDER_PERSON);

        ValidationHistoryTableModel tableModel = new ValidationHistoryTableModel(getTable().getColumnModel());
        table.setModel(tableModel);
        initTable(table, true);

        table.setEditable(false);

        table.setSortOrder(ValidationHistoryTableModel.DATE, SortOrder.ASCENDING);

        // Auto adjust rows height
        new TableRowHeightController(table);
    }

    @Override
    public AbstractTableModel<ValidationHistoryRowModel> getTableModel() {
        return (ValidationHistoryTableModel) getTable().getModel();
    }

    @Override
    public SwingTable getTable() {
        return getUI().getValidationHistoryTable();
    }

}
