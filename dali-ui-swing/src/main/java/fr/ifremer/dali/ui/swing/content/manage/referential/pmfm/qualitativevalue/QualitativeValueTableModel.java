package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model for qualitative value dialog table
 */
public class QualitativeValueTableModel extends AbstractDaliTableModel<QualitativeValueTableRowModel> {

    /**
     * Column Mnemonic
     */
    public static final DaliColumnIdentifier<QualitativeValueTableRowModel> MNEMONIC = DaliColumnIdentifier.newId(
            QualitativeValueTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class, true);
    /**
     * Column Description
     */
    public static final DaliColumnIdentifier<QualitativeValueTableRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
            QualitativeValueTableRowModel.PROPERTY_DESCRIPTION,
            n("dali.property.description"),
            n("dali.property.description"),
            String.class);
    /**
     * Column Status
     */
    public static final DaliColumnIdentifier<QualitativeValueTableRowModel> STATUS = DaliColumnIdentifier.newId(
            QualitativeValueTableRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class, true);

    /**
     * Constructor.
     *
     * @param columnModel model for columns
     */
    public QualitativeValueTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public QualitativeValueTableRowModel createNewRow() {
        return new QualitativeValueTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<QualitativeValueTableRowModel> getFirstColumnEditing() {
        return MNEMONIC;
    }
}
