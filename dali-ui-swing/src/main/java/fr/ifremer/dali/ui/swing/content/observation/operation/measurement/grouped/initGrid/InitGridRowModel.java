package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author peck7 on 19/07/2017.
 */
public class InitGridRowModel extends AbstractDaliRowUIModel<QuadrigeBean, InitGridRowModel> {

    private PmfmDTO pmfm;
    public static final String PROPERTY_PMFM = "pmfm";
    private List<QualitativeValueDTO> qualitativeValues;
    public static final String PROPERTY_QUALITATIVE_VALUES = "qualitativeValues";

    /**
     * <p>Constructor for AbstractDaliRowUIModel.</p>
     */
    public InitGridRowModel() {
        super(null, null);
    }

    @Override
    protected QuadrigeBean newBean() {
        return null;
    }

    public PmfmDTO getPmfm() {
        return pmfm;
    }

    public void setPmfm(PmfmDTO pmfm) {
        PmfmDTO oldValue = getPmfm();
        this.pmfm = pmfm;
        firePropertyChange(PROPERTY_PMFM, oldValue, pmfm);
    }

    public List<QualitativeValueDTO> getQualitativeValues() {
        if (qualitativeValues == null) qualitativeValues = new ArrayList<>();
        return qualitativeValues;
    }

    public void setQualitativeValues(List<QualitativeValueDTO> qualitativeValues) {
        this.qualitativeValues = qualitativeValues;
        firePropertyChange(PROPERTY_QUALITATIVE_VALUES, null, qualitativeValues);
    }
}
