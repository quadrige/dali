package fr.ifremer.dali.ui.swing.content.extraction.list;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.GroupingTypeDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * extractions table model
 */
public class ExtractionsTableModel extends AbstractDaliTableModel<ExtractionsRowModel> {

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<ExtractionsRowModel> NAME = DaliColumnIdentifier.newId(
            ExtractionsRowModel.PROPERTY_NAME,
            n("dali.extraction.list.name.short"),
            n("dali.extraction.list.name.tip"),
            String.class,
            true);

    /** Constant <code>GROUPING_TYPE</code> */
    public static final DaliColumnIdentifier<ExtractionsRowModel> GROUPING_TYPE = DaliColumnIdentifier.newId(
            ExtractionsRowModel.PROPERTY_GROUPING_TYPE,
            n("dali.extraction.list.groupingType.short"),
            n("dali.extraction.list.groupingType.tip"),
            GroupingTypeDTO.class,
            true);

    /**
     * <p>Constructor for ExtractionsTableModel.</p>
     *
     */
    public ExtractionsTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, true, false);
    }

    /** {@inheritDoc} */
    @Override
    public ExtractionsRowModel createNewRow() {
        return new ExtractionsRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<ExtractionsRowModel> getFirstColumnEditing() {
        return NAME;
    }

}
