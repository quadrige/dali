package fr.ifremer.dali.ui.swing.util.plaf;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jaxx.runtime.validator.swing.ui.AbstractBeanValidatorUI;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;

/**
 * @author peck7 on 17/01/2018.
 */
public class BorderValidationUI extends AbstractBeanValidatorUI {

//    private static final String VALIDATION_BREAK = "<br/><br/>-----<br/>";
//    private Set<String> messages = new HashSet<>();

    public BorderValidationUI(String field) {
        super(field);
    }

    public BorderValidationUI(Collection<String> fields) {
        super(fields);
    }

    @Override
    protected void paintLayer(Graphics2D g2, JXLayer<? extends JComponent> l) {
        super.paintLayer(g2, l);
        NuitonValidatorScope scope = getScope();
        if (scope == null) return;

        JComponent view = l.getView();
        Color color = null;
        // Override scope color from nuiton
        switch (scope) {
            case FATAL:
                color = Color.MAGENTA;
                break;
            case ERROR:
                color = Color.RED;
                break;
            case WARNING:
                color = Color.ORANGE;
                break;
            case INFO:
                color = Color.GREEN;
                break;
        }

        // Paint dashed border
        Border border = BorderFactory.createDashedBorder(color, 2f, 4, 4, false);
        border.paintBorder(view, g2, 0, 0, view.getWidth(), view.getHeight());

        // Add tooltip text on view
        // FIXME LP 17/01/18 : tooltip retiré car
        // 1 - produit des JavaHeapSpace erreur ç cause des nombreux appels de cette methode
        // 2 - le tooltip apparait uniquement sur la bordure des composant car le composant editeur a déjà sont propre tooltip
//        String toolTipText = view.getToolTipText();
//        if (!messages.isEmpty()) {
//            if (toolTipText != null) {
//                if (toolTipText.contains(VALIDATION_BREAK)) {
//                    toolTipText = removeValidationBreak(toolTipText);
//                }
//                toolTipText += toolTipText + VALIDATION_BREAK + getMessages();
//            } else {
//                toolTipText = VALIDATION_BREAK + getMessages();
//            }
//
//        } else {
//            if (toolTipText != null && toolTipText.contains(VALIDATION_BREAK)) {
//                toolTipText = removeValidationBreak(toolTipText);
//            }
//        }
//        view.setToolTipText(ApplicationUIUtil.getHtmlString(toolTipText));
    }

//    private String removeValidationBreak(String text) {
//        return text.substring(0, text.indexOf(VALIDATION_BREAK));
//    }
//
//    private String getMessages() {
//        StringJoiner joiner = new StringJoiner("<br/>");
//        messages.forEach(message -> joiner.add(t(message)));
//        return joiner.toString();
//    }
//
//    @Override
//    protected NuitonValidatorScope getHighestScope(SimpleBeanValidatorEvent event) {
//
//        // intercept messages
//        if (event.getMessagesToAdd() != null) {
//            messages.addAll(Arrays.asList(event.getMessagesToAdd()));
//        }
//        if (event.getMessagesToDelete() != null) {
//            messages.removeAll(Arrays.asList(event.getMessagesToDelete()));
//        }
//
//        return super.getHighestScope(event);
//    }
}
