package fr.ifremer.dali.ui.swing.action;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.content.DaliMainUIHandler;
import fr.ifremer.quadrige3.ui.swing.action.AbstractMainUIAction;
import fr.ifremer.quadrige3.ui.swing.content.AbstractMainUIHandler;
import fr.ifremer.dali.ui.swing.content.DaliMainUI;

/**
 * @author peck7 on 04/07/2017.
 */
public abstract class AbstractDaliMainUIAction extends AbstractMainUIAction<DaliUIContext, DaliMainUI> {

    /**
     * <p>Constructor for AbstractMainUIAction.</p>
     *
     * @param handler  a {@link AbstractMainUIHandler} object.
     * @param hideBody a boolean.
     */
    public AbstractDaliMainUIAction(AbstractMainUIHandler handler, boolean hideBody) {
        super(handler, hideBody);
    }

    @Override
    public DaliMainUIHandler getHandler() {
        return (DaliMainUIHandler) super.getHandler();
    }

    @Override
    protected DaliConfiguration getConfig() {
        return (DaliConfiguration) super.getConfig();
    }

    @Override
    public DaliUIContext getContext() {
        return (DaliUIContext) super.getContext();
    }
}
