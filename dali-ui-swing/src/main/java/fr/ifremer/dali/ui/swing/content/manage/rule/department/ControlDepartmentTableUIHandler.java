package fr.ifremer.dali.ui.swing.content.manage.rule.department;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des services.
 */
public class ControlDepartmentTableUIHandler extends
        AbstractDaliTableUIHandler<ControlDepartmentTableRowModel, ControlDepartmentTableUIModel, ControlDepartmentTableUI> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ControlDepartmentTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlDepartmentTableUIModel model = new ControlDepartmentTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ControlDepartmentTableUI ui) {

        initUI(ui);

        // Disable buttons
        getUI().getAddDepartmentButton().setEnabled(false);
        getUI().getRemoveDepartmentButton().setEnabled(false);

        // Initialisation du tableau
        initTable();

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // Courant
        TableColumnExt codeCol = addColumn(ControlDepartmentTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // Courant
        TableColumnExt nameCol = addColumn(ControlDepartmentTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        ControlDepartmentTableModel tableModel = new ControlDepartmentTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Initialisation du tableau
        initTable(table);

        // Number rows visible
        table.setVisibleRowCount(3);
    }

    /**
     * <p>loadServicesControle.</p>
     *
     * @param departments a {@link Collection} object.
     * @param readOnly    read only ?
     */
    public void loadDepartments(final Collection<DepartmentDTO> departments, boolean readOnly) {

        // Load table
        getModel().setBeans(departments);
        getTable().setEditable(!readOnly);

        // activate add button
        getUI().getAddDepartmentButton().setEnabled(!readOnly);

        recomputeRowsValidState(false);
    }

    /**
     * <p>supprimerServicesControle.</p>
     */
    public void clearTable() {

        getModel().setBeans(null);

        getUI().getAddDepartmentButton().setEnabled(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<ControlDepartmentTableRowModel> getTableModel() {
        return (ControlDepartmentTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getControlDepartmentTable();
    }

    /**
     * <p>openAddDialog.</p>
     */
    public void openAddDialog() {
        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.DEPARTMENT.getFilterTypeId());
        dialog.setTitle(t("dali.filter.department.addDialog.title"));
        List<DepartmentDTO> departments = getModel().getBeans().stream()
                .sorted(getDecorator(DepartmentDTO.class, null).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(department -> department.setReadOnly(true))
                .collect(Collectors.toList());
        dialog.getModel().setSelectedElements(departments);

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            List<DepartmentDTO> newDepartments = dialog.getModel().getSelectedElements().stream()
                    .map(element -> ((DepartmentDTO) element))
                    .filter(newDepartment -> !getModel().getBeans().contains(newDepartment))
                    .collect(Collectors.toList());

            if (!newDepartments.isEmpty()) {
                getModel().addBeans(newDepartments);
                getModel().setModify(true);
                saveToParentModel();
            }
        }
    }

    /**
     * <p>removeDepartments.</p>
     */
    public void removeDepartments() {
        getModel().deleteSelectedRows();
        saveToParentModel();
    }

    private void saveToParentModel() {
        getModel().getParentModel().getRuleListUIModel().getSingleSelectedRow().setDepartments(getModel().getBeans());
        recomputeRowsValidState(false);

        getModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);

    }

}
