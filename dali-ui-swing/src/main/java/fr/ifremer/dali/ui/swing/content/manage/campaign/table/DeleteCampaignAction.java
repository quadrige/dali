package fr.ifremer.dali.ui.swing.content.manage.campaign.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;
import fr.ifremer.dali.ui.swing.util.DaliUIs;
import fr.ifremer.quadrige3.core.exception.DeleteForbiddenException;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de supprimer une campagne.
 */
public class DeleteCampaignAction extends AbstractDaliAction<CampaignsTableUIModel, CampaignsTableUI, CampaignsTableUIHandler> {

    private List<CampaignDTO> campaignsToDelete;
    private boolean deleteAborted = false;

    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteCampaignAction(final CampaignsTableUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            return false;
        }

        campaignsToDelete = new ArrayList<>();
        List<CampaignDTO> deniedCampaigns = new ArrayList<>();

        // Check already saved campaign
        for (CampaignsTableRowModel campaign : getModel().getSelectedRows()) {
            if (!campaign.isEditable())
                deniedCampaigns.add(campaign);

            if (campaign.getId() != null)
                campaignsToDelete.add(campaign);
        }

        // Stop if a denied campaign is about to be delete
        if (!deniedCampaigns.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("dali.action.delete.campaign.denied.message"),
                    DaliUIs.getHtmlString(deniedCampaigns.stream().map(CampaignDTO::getName).collect(Collectors.toList())),
                    null,
                    t("dali.action.delete.campaign.title")
            );
            return false;
        }

        return askBeforeDelete(t("dali.action.delete.campaign.title"), t("dali.action.delete.campaign.message"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        // check program usage in data
        for (CampaignDTO campaign : campaignsToDelete) {

            long surveyCount = getContext().getObservationService().countSurveysWithCampaign(campaign.getId());
            if (surveyCount > 0) {
                getContext().getDialogHelper().showErrorDialog(
                        surveyCount == 1
                                ? t("dali.action.delete.campaign.used.data.message", campaign.getName())
                                : t("dali.action.delete.campaign.used.data.many.message", surveyCount, campaign.getName()),
                        t("dali.action.delete.campaign.title"));
                deleteAborted = true;
                return;
            }

            if (campaign.getId() != null) {
                if (getContext().getCampaignService().isCampaignUsedByFilter(campaign.getId())) {
                    getContext().getDialogHelper().showErrorDialog(
                            t("dali.action.delete.campaign.used.filter.message", campaign.getName()),
                            t("dali.action.delete.campaign.title")
                    );
                    deleteAborted = true;
                    return;
                }
            }
        }

        try {

            // Suppression des campagnes
            getContext().getCampaignService().deleteCampaign(
                    getContext().getAuthenticationInfo(),
                    DaliBeans.collectIds(campaignsToDelete));

        } catch (DeleteForbiddenException e) {

            // Delete is forbidden (remote error), retrieve ids of campaigns
            if (CollectionUtils.isNotEmpty(e.getObjectIds())) {
                List<CampaignDTO> campaigns = e.getObjectIds().stream()
                        .map(objectId -> DaliBeans.findById(campaignsToDelete, Integer.parseInt(objectId)))
                        .collect(Collectors.toList());
                getContext().getDialogHelper().showErrorDialog(
                        t("dali.action.delete.campaign.used.data.remote.topMessage"),
                        ApplicationUIUtil.getHtmlString(campaigns.stream().map(this::decorate).collect(Collectors.toList())),
                        t("dali.action.delete.campaign.used.data.remote.bottomMessage"),
                        t("dali.action.delete.campaign.title")
                );
            } else {
                getContext().getDialogHelper().showErrorDialog(
                        t("dali.action.delete.campaign.used.data.remote.message"),
                        t("dali.action.delete.campaign.title")
                );
            }
            deleteAborted = true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // Suppression des lignes
        if (!deleteAborted)
            getModel().deleteSelectedRows();

    }

}
