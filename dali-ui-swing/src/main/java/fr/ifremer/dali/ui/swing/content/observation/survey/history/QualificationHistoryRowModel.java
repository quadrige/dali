package fr.ifremer.dali.ui.swing.content.observation.survey.history;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.QualityLevelDTO;
import fr.ifremer.dali.dto.system.QualificationHistoryDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;

/**
 * @author peck7 on 26/09/2017.
 */
public class QualificationHistoryRowModel extends AbstractDaliRowUIModel<QualificationHistoryDTO, QualificationHistoryRowModel> implements QualificationHistoryDTO {

    private static final Binder<QualificationHistoryDTO, QualificationHistoryRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(QualificationHistoryDTO.class, QualificationHistoryRowModel.class);
    private static final Binder<QualificationHistoryRowModel, QualificationHistoryDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(QualificationHistoryRowModel.class, QualificationHistoryDTO.class);

    public QualificationHistoryRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    @Override
    protected QualificationHistoryDTO newBean() {
        return DaliBeanFactory.newQualificationHistoryDTO();
    }

    @Override
    public Date getDate() {
        return delegateObject.getDate();
    }

    @Override
    public void setDate(Date date) {
        delegateObject.setDate(date);
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    @Override
    public PersonDTO getRecorderPerson() {
        return delegateObject.getRecorderPerson();
    }

    @Override
    public void setRecorderPerson(PersonDTO recorderPerson) {
        delegateObject.setRecorderPerson(recorderPerson);
    }

    @Override
    public QualityLevelDTO getQualityLevel() {
        return delegateObject.getQualityLevel();
    }

    @Override
    public void setQualityLevel(QualityLevelDTO qualityLevel) {
        delegateObject.setQualityLevel(qualityLevel);
    }
}
