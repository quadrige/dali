package fr.ifremer.dali.ui.swing.content.manage.referential.location.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.configuration.filter.location.LocationCriteriaDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.referential.GroupingDTO;
import fr.ifremer.dali.dto.referential.GroupingTypeDTO;
import fr.ifremer.dali.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Modele du menu pour la gestion des lieux
 */
public class LocationMenuUIModel extends AbstractReferentialMenuUIModel<LocationCriteriaDTO, LocationMenuUIModel> implements LocationCriteriaDTO {

    private static final Binder<LocationMenuUIModel, LocationCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(LocationMenuUIModel.class, LocationCriteriaDTO.class);

    private static final Binder<LocationCriteriaDTO, LocationMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(LocationCriteriaDTO.class, LocationMenuUIModel.class);

    /**
     * Constructor.
     */
    public LocationMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected LocationCriteriaDTO newBean() {
        return DaliBeanFactory.newLocationCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getLabel() {
        return delegateObject.getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLabel(String label) {
        delegateObject.setLabel(label);
    }

    /** {@inheritDoc} */
    @Override
    public GroupingTypeDTO getGroupingType() {
        return delegateObject.getGroupingType();
    }

    /** {@inheritDoc} */
    @Override
    public void setGroupingType(GroupingTypeDTO groupingType) {
        delegateObject.setGroupingType(groupingType);
    }

    /** {@inheritDoc} */
    @Override
    public GroupingDTO getGrouping() {
        return delegateObject.getGrouping();
    }

    /** {@inheritDoc} */
    @Override
    public void setGrouping(GroupingDTO grouping) {
        delegateObject.setGrouping(grouping);
    }

    /** {@inheritDoc} */
    @Override
    public ProgramDTO getProgram() {
        return delegateObject.getProgram();
    }

    /** {@inheritDoc} */
    @Override
    public void setProgram(ProgramDTO program) {
        delegateObject.setProgram(program);
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setLabel(null);
        setGroupingType(null);
        setGrouping(null);
        setProgram(null);
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void setDirty(boolean dirty) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }
}
