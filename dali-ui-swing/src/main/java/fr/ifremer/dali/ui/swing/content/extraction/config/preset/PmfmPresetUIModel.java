package fr.ifremer.dali.ui.swing.content.extraction.config.preset;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Created by Ludovic on 26/05/2015.
 */
public class PmfmPresetUIModel extends AbstractDaliTableUIModel<PmfmPresetDTO, PmfmPresetRowModel, PmfmPresetUIModel> {

    private List<PmfmDTO> availablePmfms;
    public static final String PROPERTY_AVAILABLE_PMFMS = "availablePmfms";

    private List<PmfmPresetDTO> initialPmfmPresets;
    public static final String PROPERTY_INITIAL_PMFM_PRESETS = "initialPmfmPresets";

    private List<QualitativeValueDTO> selectedValues;
    public static final String PROPERTY_SELECTED_VALUES = "selectedValues";

    private List<PmfmDTO> availableResultPmfms;
    public static final String PROPERTY_AVAILABLE_RESULT_PMFMS = "availableResultPmfms";

    private List<PmfmDTO> selectedResultPmfms;
    public static final String PROPERTY_SELECTED_RESULT_PMFMS = "selectedResultPmfms";

    private List<PmfmDTO> initialResultPmfms;
    public static final String PROPERTY_INITIAL_RESULT_PMFMS = "initialResultPmfms";

    public List<PmfmDTO> getAvailablePmfms() {
        return availablePmfms;
    }

    public void setAvailablePmfms(List<PmfmDTO> availablePmfms) {
        this.availablePmfms = availablePmfms;
        firePropertyChange(PROPERTY_AVAILABLE_PMFMS, null, availablePmfms);
    }

    public List<PmfmPresetDTO> getInitialPmfmPresets() {
        return initialPmfmPresets;
    }

    public void setInitialPmfmPresets(List<PmfmPresetDTO> initialPmfmPresets) {
        this.initialPmfmPresets = initialPmfmPresets;
        firePropertyChange(PROPERTY_INITIAL_PMFM_PRESETS, null, initialPmfmPresets);
    }

    public List<QualitativeValueDTO> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<QualitativeValueDTO> selectedValues) {
        this.selectedValues = selectedValues;
        firePropertyChange(PROPERTY_SELECTED_VALUES, null, selectedValues);
    }

    public List<PmfmDTO> getAvailableResultPmfms() {
        return availableResultPmfms;
    }

    public void setAvailableResultPmfms(List<PmfmDTO> availableResultPmfms) {
        this.availableResultPmfms = availableResultPmfms;
        firePropertyChange(PROPERTY_AVAILABLE_RESULT_PMFMS, null, availableResultPmfms);
    }

    public List<PmfmDTO> getSelectedResultPmfms() {
        return selectedResultPmfms;
    }

    public void setSelectedResultPmfms(List<PmfmDTO> selectedResultPmfms) {
        this.selectedResultPmfms = selectedResultPmfms;
        firePropertyChange(PROPERTY_SELECTED_RESULT_PMFMS, null, selectedResultPmfms);
    }

    public List<PmfmDTO> getInitialResultPmfms() {
        return initialResultPmfms;
    }

    public void setInitialResultPmfms(List<PmfmDTO> initialResultPmfms) {
        this.initialResultPmfms = initialResultPmfms;
        firePropertyChange(PROPERTY_INITIAL_RESULT_PMFMS, null, initialResultPmfms);
    }

    public boolean isPresetValid() {
        for (PmfmPresetRowModel rowModel : getRows()) {
            if (CollectionUtils.isNotEmpty(rowModel.getQualitativeValues())) return true;
        }
        return false;
    }

    public void setPresetValid(boolean presetValid) {
        // dummy setter
    }

    public boolean isResultValid() {
        return CollectionUtils.isNotEmpty(getSelectedResultPmfms());
    }

    public void setResultValid(boolean resultValid) {
        // dummy setter
    }

}
