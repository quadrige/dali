package fr.ifremer.dali.ui.swing.content.home;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.service.control.ControlRuleMessagesBean;
import fr.ifremer.dali.ui.swing.action.AbstractDaliSaveAction;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Save action.
 */
public class SaveAction extends AbstractDaliSaveAction<HomeUIModel, HomeUI, HomeUIHandler> {

    private Collection<? extends SurveyDTO> surveysToSave;

    private ControlRuleMessagesBean controlMessages;

    private boolean controlSilently = false;

    private boolean showControlIfSuccess = false;

    private boolean updateControlDateWhenControlSucceed = false;

    /**
     * <p>Constructor for SaveAction.</p>
     *
     * @param handler a {@link fr.ifremer.dali.ui.swing.content.home.HomeUIHandler} object.
     */
    public SaveAction(final HomeUIHandler handler) {
        super(handler, false);
    }

    /**
     * <p>Setter for the field <code>surveysToSave</code>.</p>
     *
     * @param surveysToSave a {@link java.util.Collection} object.
     */
    public void setSurveysToSave(Collection<? extends SurveyDTO> surveysToSave) {
        this.surveysToSave = surveysToSave;
    }

    /**
     * <p>Setter for the field <code>controlSilently</code>.</p>
     *
     * @param controlSilently a boolean.
     */
    public void setControlSilently(boolean controlSilently) {
        this.controlSilently = controlSilently;
    }

    /**
     * <p>Setter for the field <code>showControlIfSuccess</code>.</p>
     *
     * @param showControlIfSuccess a boolean.
     */
    public void setShowControlIfSuccess(boolean showControlIfSuccess) {
        this.showControlIfSuccess = showControlIfSuccess;
    }

    /**
     * <p>Setter for the field <code>updateControlDateWhenControlSucceed</code>.</p>
     *
     * @param updateControlDateWhenControlSucceed a boolean.
     */
    public void setUpdateControlDateWhenControlSucceed(boolean updateControlDateWhenControlSucceed) {
        this.updateControlDateWhenControlSucceed = updateControlDateWhenControlSucceed;
    }

    /**
     * <p>Getter for the field <code>controlMessages</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.service.control.ControlRuleMessagesBean} object.
     */
    public ControlRuleMessagesBean getControlMessages() {
        return controlMessages;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }
        // Model is valid
        // the model can be valid if control is valid (Mantis #61701)
        if (!getModel().isValid() && !getModel().isControlValid()) {

            // Remove error
            displayErrorMessage(t("dali.action.save.errors.title"), t("dali.action.save.errors.remove"));

            // Stop saving
            return false;
        }

        // Selected surveys
        if (surveysToSave == null) {
            surveysToSave = getUI().getSurveysTableUI().getModel().getRows();
        }

        if (CollectionUtils.isEmpty(surveysToSave)) {
            return false;
        }

        // Additional control before save
        for (SurveyDTO survey : surveysToSave) {
            if (survey.isDirty()) {

                // Check existing surveys on same date, same location in table
                // Test removed (Mantis #42609)
//                boolean skipSearchInDb = false;
//                for (SurveyDTO otherSurvey : surveysToSave) {
//                    if (survey == otherSurvey) continue;
//                    if (survey.getDate().equals(otherSurvey.getDate())
//                            && survey.getLocation().equals(otherSurvey.getLocation())) {
//                        if (getContext().getDialogHelper().showConfirmDialog(
//                                t("dali.action.save.observation.alreadyExistsOnLocationAndDate", DaliBeans.toString(otherSurvey)),
//                                t("dali.action.save.observations"),
//                                JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
//                            return false;
//                        } else {
//                            skipSearchInDb = true;
//                        }
//                    }
//                }

                // Check existing surveys on same date, same location in database
                // Test removed (Mantis #42609)
//                if (!skipSearchInDb) {
//                    SurveyFilterDTO surveyFilter = DaliBeanFactory.newSurveyFilterDTO();
//                    surveyFilter.setLocationId(survey.getLocation().getId());
//                    surveyFilter.setDate1(survey.getDate());
//                    surveyFilter.setSearchDateId(SearchDateValues.EQUALS.ordinal());
//                    List<SurveyDTO> existingSurveys = getContext().getObservationService().getSurveys(surveyFilter);
//                    if (CollectionUtils.isNotEmpty(existingSurveys)) {
//                        for (SurveyDTO existingSurvey : existingSurveys) {
//                            if (existingSurvey.getId().equals(survey.getId())) continue;
//                            if (getContext().getDialogHelper().showConfirmDialog(
//                                    t("dali.action.save.observation.alreadyExistsOnLocationAndDate", DaliBeans.toString(existingSurvey)),
//                                    t("dali.action.save.observations"),
//                                    JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
//                                return false;
//                            }
//                        }
//                    }
//                }

                // Check survey date within campaign dates
                if (survey.getCampaign() != null) {
                    if (survey.getDate().isBefore(survey.getCampaign().getStartDate())
                        || (survey.getCampaign().getEndDate() != null && survey.getDate().isAfter(survey.getCampaign().getEndDate()))) {
                        // The save is invalid if the survey is outside campaign dates
                        getContext().getDialogHelper().showErrorDialog(
                            t("dali.action.save.observation.notInCampaign", DaliBeans.toString(survey), DaliBeans.toString(survey.getCampaign())),
                            t("dali.action.save.observations"));
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        createProgressionUIModel();

        // Save observations
        getContext().getObservationService().saveSurveys(surveysToSave, getProgressionUIModel());

        // control them
        controlMessages = getContext().getRulesControlService().controlSurveys(surveysToSave,
            updateControlDateWhenControlSucceed,
            true, /*Reset control date, when failed */
            getProgressionUIModel());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        sendMessage(t("dali.action.save.observations.success", surveysToSave.size()));

        // reset model modify state only if all surveys in table have been saved
        if (surveysToSave.size() == getModel().getSurveysTableUIModel().getRowCount()) {
            getModel().getSurveysTableUIModel().setModify(false);
            getModel().getOperationsTableUIModel().setModify(false);
            getModel().setModify(false);
        }

        // update selected ids (Mantis #52824) - rollback from c1920adcbaf623511b4e44399b0b5cffc2e23868
        if (getModel().getSelectedSurvey() != null) {
            getContext().setSelectedSurveyId(getModel().getSelectedSurvey().getId());
        }

        // Don't refresh nested UI if save action is called from a change screen action (Mantis #52661)
        if (!isFromChangeScreenAction()) {

            getUI().getOperationsTableUI().getHandler().loadOperations(getModel().getSelectedSurvey());

            if (HomeUIHandler.SURVEYS_MAP_CARD.equals(getUI().getBottomPanelLayout().getSelected())) {
                getUI().getSurveysMapUI().getHandler().buildMapPositions();
            }
        }

        // If error messages
        if (!controlSilently) {
            getUI().getSurveysTableUI().getHandler().ensureColumnsWithErrorAreVisible(surveysToSave);
            if (isFromChangeScreenAction()) {
                setAllowChangeScreen(showControlResultAndAskContinue(controlMessages));
            } else {
                showControlResult(controlMessages, showControlIfSuccess);
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void releaseAction() {
        super.releaseAction();

        surveysToSave = null;
        controlSilently = false;
        showControlIfSuccess = false;
        updateControlDateWhenControlSucceed = false;
    }
}
