package fr.ifremer.dali.ui.swing.content.home;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.data.survey.SurveyFilterDTO;
import fr.ifremer.dali.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant la recherche des obervations.
 */
public class SearchAction extends AbstractCheckModelAction<HomeUIModel, HomeUI, HomeUIHandler> {

    private final static Log log = LogFactory.getLog(SearchAction.class);
    private List<SurveyDTO> surveys;
    private boolean controlAfterSelect = false;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final HomeUIHandler handler) {
        super(handler, false);
        setActionDescription(t("dali.action.search.tip"));
    }

    public void setControlAfterSelect(boolean controlAfterSelect) {
        this.controlAfterSelect = controlAfterSelect;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<SaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getHandler();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        return getModel().isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().setModify(modelModify);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        return getModel().isValid();
    }

    @Override
    public boolean prepareAction() throws Exception {
        if (getContext().isActionInProgress(this.getClass())) {
            log.warn("Action already running, skip.");
            return false;
        }
        boolean prepared = super.prepareAction();
        if (prepared) {
            getContext().setActionInProgress(this.getClass(), true);
        }
        return prepared;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        // Tell surveys table handler a load has begun
        getUI().getSurveysTableUI().getHandler().beforeLoad();

        // Create new search parameters
        final SurveyFilterDTO surveyFilter = DaliBeanFactory.newSurveyFilterDTO();

        // Gather model properties
        surveyFilter.setCampaignId(getModel().getCampaignId());
        surveyFilter.setProgramCode(getModel().getProgramCode());
        surveyFilter.setLocationId(getModel().getLocationId());
        surveyFilter.setDate1(getModel().getStartDate());
        surveyFilter.setDate2(getModel().getEndDate());
        surveyFilter.setSearchDateId(getModel().getSearchDateId());
        surveyFilter.setName(getModel().getName());
        surveyFilter.setComment(getModel().getComment());
        surveyFilter.setStateId(getModel().getStateId());
        surveyFilter.setShareId(getModel().getSynchronizationStatusId());

        // Save into context
        getContext().setSurveyFilter(surveyFilter);

        // Get observations
        surveys = getContext().getObservationService().getSurveys(surveyFilter);
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        return ImmutableList.of(
            getModel().getSurveysTableUIModel(),
            getModel().getOperationsTableUIModel()
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getUI().getSurveysTableUI().getSurveysTable().clearSelection();

        getModel().getSurveysTableUIModel().setBeans(surveys);

        // The load is done
        getUI().getSurveysTableUI().getHandler().afterLoad();

        if (getContext().getSelectedSurveyId() != null) {

            // Select
            getUI().getSurveysTableUI().getHandler().selectRowById(getContext().getSelectedSurveyId());

            if (controlAfterSelect) {
                getContext().getRulesControlService().controlSurvey(getModel().getSelectedSurvey(), false, false);
                getUI().getSurveysTableUI().getHandler().ensureColumnsWithErrorAreVisible(getModel().getSelectedSurvey());
            }
        }

    }

    @Override
    protected void releaseAction() {
        super.releaseAction();
        surveys = null;
        controlAfterSelect = false;
        getContext().setActionInProgress(this.getClass(), false);
    }
}
