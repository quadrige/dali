package fr.ifremer.dali.ui.swing.content.manage.referential.unit.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.UnitDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.unit.menu.ReferentialUnitsMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.unit.table.UnitTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.unit.table.UnitTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Units au niveau national
 */
public class ReferentialUnitsNationalUIHandler extends
        AbstractDaliTableUIHandler<UnitTableRowModel, ReferentialUnitsNationalUIModel, ReferentialUnitsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ReferentialUnitsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ReferentialUnitsNationalUIModel model = new ReferentialUnitsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(ReferentialUnitsNationalUI ui) {
        initUI(ui);

        ReferentialUnitsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();

        menuUIModel.addPropertyChangeListener(ReferentialUnitsMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((Collection<UnitDTO>) evt.getNewValue()));

        initTable();
    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(
                UnitTableModel.NAME);
        mnemonicCol.setSortable(true);

        // Symbol
        final TableColumnExt symbolCol = addColumn(
                UnitTableModel.SYMBOL);
        symbolCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(UnitTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(UnitTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(UnitTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                UnitTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ACTIVE),
                false);
        statusCol.setSortable(true);

        final UnitTableModel tableModel = new UnitTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        // Colonnes non editable
        tableModel.setNoneEditableCols(
                UnitTableModel.NAME,
                UnitTableModel.SYMBOL,
                UnitTableModel.STATUS);

        addExportToCSVAction(t("dali.property.unit"));

        // Initialisation du tableau
        initTable(table, true);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<UnitTableRowModel> getTableModel() {
        return (UnitTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getReferentialUnitsNationalTable();
    }

}
