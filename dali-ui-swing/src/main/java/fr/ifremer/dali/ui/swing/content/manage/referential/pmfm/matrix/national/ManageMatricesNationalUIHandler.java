package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.menu.ManageMatricesMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.editor.AssociatedFractionsCellEditor;
import fr.ifremer.dali.ui.swing.util.table.renderer.AssociatedFractionCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Matrices au niveau national
 */
public class ManageMatricesNationalUIHandler extends
        AbstractDaliTableUIHandler<MatricesTableRowModel, ManageMatricesNationalUIModel, ManageMatricesNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageMatricesNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageMatricesNationalUIModel model = new ManageMatricesNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(ManageMatricesNationalUI ui) {
        initUI(ui);

        ManageMatricesMenuUIModel menuUIModel = getUI().getMenuUI().getModel();

        menuUIModel.addPropertyChangeListener(ManageMatricesMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<MatrixDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(MatricesTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        final TableColumnExt descriptionCol = addColumn(MatricesTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(MatricesTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(MatricesTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(MatricesTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                MatricesTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // associated fractions
        final TableColumnExt associatedFractionsCol = addColumn(
                new AssociatedFractionsCellEditor(getTable(), getUI()),
                new AssociatedFractionCellRenderer(),
                MatricesTableModel.ASSOCIATED_FRACTIONS);
        associatedFractionsCol.setSortable(true);

        MatricesTableModel tableModel = new MatricesTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.pmfm.matrix"), MatricesTableModel.ASSOCIATED_FRACTIONS);

        // Initialisation du tableau
        initTable(table, true);

        // Les colonnes optionnelles sont invisibles
        associatedFractionsCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<MatricesTableRowModel> getTableModel() {
        return (MatricesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageMatricesNationalTable();
    }
}
