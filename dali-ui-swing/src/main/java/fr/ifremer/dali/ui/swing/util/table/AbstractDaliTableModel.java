package fr.ifremer.dali.ui.swing.util.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.AbstractBaseReferentialDTOBean;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

/**
 * <p>Abstract AbstractDaliTableModel class.</p>
 *
 * @param <R> type of AbstractDaliRowUIModel
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 *         <p>
 */
public abstract class AbstractDaliTableModel<R extends AbstractDaliRowUIModel<?, ?>> extends AbstractTableModel<R> {

    /**
     * <p>Constructor for AbstractDaliTableModel.</p>
     *
     * @param createNewRow          a boolean.
     * @param createEmptyRowIsEmpty a boolean.
     */
    public AbstractDaliTableModel(SwingTableColumnModel columnModel, boolean createNewRow, boolean createEmptyRowIsEmpty) {
        super(columnModel, createNewRow, createEmptyRowIsEmpty);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, ColumnIdentifier<R> propertyName) {

        if (AbstractBaseReferentialDTOBean.PROPERTY_CREATION_DATE.equals(propertyName.getPropertyName())
         || AbstractBaseReferentialDTOBean.PROPERTY_UPDATE_DATE.equals(propertyName.getPropertyName())) {
            return false;
        }

        return super.isCellEditable(rowIndex, columnIndex, propertyName);
    }
}
