package fr.ifremer.dali.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.FilterUI;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.AbstractFilterElementUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller.
 */
public class FilterListUIHandler extends AbstractDaliTableUIHandler<FilterListRowModel, FilterListUIModel, FilterListUI> {

    public FilterListUIHandler() {
        super(FilterListRowModel.PROPERTY_NAME);
    }

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{FilterListRowModel.PROPERTY_ELEMENTS, FilterListRowModel.PROPERTY_FILTER_LOADED, FilterListRowModel.PROPERTY_DIRTY};
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(FilterListUI ui) {
        super.beforeInit(ui);

        // Create model and register to the JAXX context
        final FilterListUIModel model = new FilterListUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(FilterListUI ui) {
        initUI(ui);
        initComboBox();
        initTable();

        // Buttons not enabled
        getUI().getDuplicateButton().setEnabled(false);
        getUI().getDeleteButton().setEnabled(false);
        getUI().getExportButton().setEnabled(false);

    }

    /**
     * Init combobox
     */
    private void initComboBox() {

        // Init filters combo
        initBeanFilterableComboBox(
                getUI().getFiltersCombo(),
                getContext().getContextService().getFiltersByType(getFilterTypeId()),
                null);

        // Combobox size
        DaliUIs.forceComponentSize(getUI().getFiltersCombo());

        getUI().getFiltersCombo().getComboBoxModel().addWillChangeSelectedItemListener(event -> {
            if (getModel().isLoading()) return;
            if (event.getNextSelectedItem() != null) SwingUtilities.invokeLater(() -> getUI().getSearchButton().getAction().actionPerformed(null));
        });
    }

    /**
     * <p>reloadComboBox.</p>
     */
    @SuppressWarnings("unchecked")
    public void reloadComboBox() {
        getModel().setLoading(true);
        BeanFilterableComboBox cb = getUI().getFiltersCombo();
        cb.setData(null);
        List<FilterDTO> filterList = getContext().getContextService().getFiltersByType(getFilterTypeId());
        cb.setData(filterList);

        // reach 'apply Filter' combobox
        BeanFilterableComboBox afCombo = ((AbstractFilterElementUIHandler) getParentUI().getFilterElementUI().getHandler())
                .getReferentialMenuUI().getHandler().getApplyFilterUI().getApplyFilterCombo();
        afCombo.setData(filterList);

        getModel().setLoading(false);
    }

    /**
     * Init table.
     */
    private void initTable() {

        // Tableau
        final SwingTable table = getTable();

        // Name column
        final TableColumnExt nameColumn = addColumn(FilterListTableModel.NAME);
        nameColumn.setSortable(true);

        // Table model
        final FilterListTableModel tableModel = new FilterListTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Init table
        initTable(table);

        // Default sort
        table.setSortOrder(FilterListTableModel.NAME, SortOrder.ASCENDING);

        // Number rows visible
        table.setVisibleRowCount(5);
    }

    /**
     * Load filters.
     *
     * @param filters Filters list
     */
    public void loadFilters(final List<FilterDTO> filters) {

        // Add filters
        getModel().setBeans(filters);

        // Auto select if single row
        if (getModel().getRowCount() == 1) {
            FilterListRowModel rowModel = getModel().getRows().get(0);
            SwingUtilities.invokeLater(() -> {
                selectRow(rowModel);
                getModel().setSingleSelectedRow(rowModel);
            });
        } else {
            // Delete filters
            getParentUI().getHandler().clearFilterElements();
        }
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public AbstractDaliTableModel<FilterListRowModel> getTableModel() {
        return (AbstractDaliTableModel<FilterListRowModel>) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getTable();
    }

    /**
     * <p>getParentUI.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.manage.filter.FilterUI} object.
     */
    public FilterUI getParentUI() {
        return (FilterUI) ApplicationUIUtil.getParentUI(getUI());
    }

    /**
     * <p>getFilterTypeId.</p>
     *
     * @return a int.
     */
    public int getFilterTypeId() {
        Integer filterTypeId = getParentUI().getFilterTypeId();
        Assert.notNull(filterTypeId);
        return filterTypeId;
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<FilterListRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        // should be only one row
        if (addedRows.size() == 1) {
            FilterListRowModel row = addedRows.get(0);

            // affect filterTypeId
            row.setFilterTypeId(getFilterTypeId());
            getModel().setModify(true);
            setFocusOnCell(row);
        }

    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, FilterListRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        recomputeRowsValidState();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(FilterListRowModel row) {

        row.getErrors().clear();
        return super.isRowValid(row) && isElementsValid(row);
    }

    private boolean isElementsValid(FilterListRowModel row) {

        if (row.isFilterLoaded() && CollectionUtils.isEmpty(row.getElements())) {
            DaliBeans.addError(row, t("dali.filter.filterList.noElement"), FilterListRowModel.PROPERTY_NAME);
        }

        // check name uniqueness in ui
        boolean duplicateFound = false;
        for (FilterListRowModel otherRow : getModel().getRows()) {
            if (row == otherRow) continue;
            if (otherRow.getName() != null && otherRow.getName().equals(row.getName())) {
                DaliBeans.addError(row, t("dali.error.alreadyExists.label.ui", row.getName()), FilterListRowModel.PROPERTY_NAME);
                duplicateFound = true;
                break;
            }
        }

        // check name uniqueness in db
        if (!duplicateFound) {
            List<FilterDTO> allFilters = getContext().getContextService().getFiltersByType(row.getFilterTypeId());
            Map<String, Integer> filterIdsByNames = Maps.newHashMap();
            for (FilterDTO filter : allFilters) {
                filterIdsByNames.put(filter.getName(), filter.getId());
            }
            if (row.isDirty()) {
                Integer existingId = filterIdsByNames.get(row.getName());
                if (existingId != null && !existingId.equals(row.getId())) {
                    // duplicate found
                    DaliBeans.addError(row, t("dali.error.alreadyExists.label.db", row.getName()), FilterListRowModel.PROPERTY_NAME);
                }
            }
        }

        return row.getErrors().isEmpty();
    }
}
