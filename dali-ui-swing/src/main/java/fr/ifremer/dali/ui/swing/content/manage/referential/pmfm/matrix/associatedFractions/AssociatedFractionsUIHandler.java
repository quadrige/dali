package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.matrix.associatedFractions;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.List;

/**
 * Handler.
 */
public class AssociatedFractionsUIHandler extends AbstractDaliTableUIHandler<FractionsTableRowModel, AssociatedFractionsUIModel, AssociatedFractionsUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(AssociatedFractionsUI ui) {
        super.beforeInit(ui);

        AssociatedFractionsUIModel model = new AssociatedFractionsUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(AssociatedFractionsUI associatedFractionsUI) {
        initUI(associatedFractionsUI);

        // Init table columns and properties
        initTable();

        getModel().addPropertyChangeListener(evt -> {

            if (AssociatedFractionsUIModel.PROPERTY_MATRIX.equals(evt.getPropertyName())) {

                List<FractionDTO> fractions = getModel().getMatrix() != null ? Lists.newArrayList(getModel().getMatrix().getFractions()) : null;
                getModel().setBeans(fractions);

            }
        });
    }

    /**
     * Init the table
     */
    private void initTable() {

        // La table des prelevements
        final SwingTable table = getTable();

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(FractionsTableModel.NAME);
        mnemonicCol.setSortable(true);

        // Description
        final TableColumnExt descriptionCol = addColumn(FractionsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                FractionsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);

        // model for table
        final FractionsTableModel tableModel = new FractionsTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        initTable(table);

        table.setEditable(false);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<FractionsTableRowModel> getTableModel() {
        return (FractionsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getAssociatedFractionsTable();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
