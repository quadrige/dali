package fr.ifremer.dali.ui.swing.content.home.map;

/*-
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.map.layer.DataFeatureLayer;
import fr.ifremer.dali.ui.swing.util.map.layer.DataLayerCollection;
import org.geotools.map.Layer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A layer collection used to group layers from a data (location, survey and its operations)
 *
 * @author peck7 on 12/06/2017.
 */
public class SurveyLayerCollection implements DataLayerCollection {

    private DataFeatureLayer locationLayer;
    private DataCompositeLayer surveyLayer;
    private List<DataCompositeLayer> operationLayers;

    public SurveyLayerCollection() {
        operationLayers = new ArrayList<>();
    }

    public void setLocationLayer(DataFeatureLayer locationLayer) {
        this.locationLayer = locationLayer;
    }

    public void setSurveyLayer(DataFeatureLayer surveyLineLayer, DataFeatureLayer surveyPointLayer) {
        this.surveyLayer = new DataCompositeLayer(surveyLineLayer, surveyPointLayer);
    }

    public void addOperationLayer(DataFeatureLayer operationLineLayer, DataFeatureLayer operationPointLayer) {
        this.operationLayers.add(new DataCompositeLayer(operationLineLayer, operationPointLayer));
    }

    public Integer getSurveyId() {
        return surveyLayer != null ? surveyLayer.getId() : null;
    }

    public Integer getLocationId() {
        return locationLayer != null ? locationLayer.getId() : null;
    }

    public Set<Integer> getOperationIds() {
        return operationLayers.stream().map(DataCompositeLayer::getId).collect(Collectors.toSet());
    }

    /**
     * Get all existing layers
     * @return all existing layers
     */
    @Override
    public List<Layer> getLayers() {
        List<Layer> layers = new ArrayList<>();
        if (locationLayer != null) layers.add(locationLayer);
        if (surveyLayer != null) layers.addAll(surveyLayer.getLayers());
        for (DataCompositeLayer operationLayer : operationLayers) {
            if (operationLayer != null) layers.addAll(operationLayer.getLayers());
        }
        return layers;
    }

    /**
     * Inner class describing a layer of line or point
     */
    private class DataCompositeLayer {

        private DataFeatureLayer lineLayer;
        private DataFeatureLayer pointLayer;

        DataCompositeLayer(DataFeatureLayer lineLayer, DataFeatureLayer pointLayer) {

            // Affect line data layer
            this.lineLayer = lineLayer;

            // Affect point data layer
            this.pointLayer = pointLayer;
        }

        List<Layer> getLayers() {
            List<Layer> layers = new ArrayList<>();
            if (lineLayer != null) layers.add(lineLayer);
            if (pointLayer != null) layers.add(pointLayer);
            return layers;
        }

        public Integer getId() {
            return lineLayer != null && lineLayer.getId() != null ? lineLayer.getId() : (pointLayer != null && pointLayer.getId() != null ? pointLayer.getId() : null);
        }
    }
}
