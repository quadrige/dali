package fr.ifremer.dali.ui.swing.content.home.operation;

import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.ui.swing.content.home.survey.SurveysTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;

/**
 * @author peck7 on 28/06/2018.
 */
public abstract class AbstractOperationsTableUIModel<M extends AbstractOperationsTableUIModel<M>>
        extends AbstractDaliTableUIModel<SamplingOperationDTO, OperationsTableRowModel, M> {

    private SurveysTableRowModel survey;
    public static final String PROPERTY_SURVEY = "survey";
    public static final String PROPERTY_SURVEY_EDITABLE = "surveyEditable";

    /**
     * <p>Getter for the field <code>survey</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.home.survey.SurveysTableRowModel} object.
     */
    public SurveysTableRowModel getSurvey() {
        return survey;
    }

    /**
     * <p>Setter for the field <code>survey</code>.</p>
     *
     * @param survey a {@link fr.ifremer.dali.ui.swing.content.home.survey.SurveysTableRowModel} object.
     */
    public void setSurvey(SurveysTableRowModel survey) {
        this.survey = survey;
        firePropertyChange(PROPERTY_SURVEY, null, survey);
        firePropertyChange(PROPERTY_SURVEY_EDITABLE, null, isSurveyEditable());
    }

    /**
     * <p>isSurveyEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isSurveyEditable() {
        return survey != null && survey.isEditable();
    }
}
