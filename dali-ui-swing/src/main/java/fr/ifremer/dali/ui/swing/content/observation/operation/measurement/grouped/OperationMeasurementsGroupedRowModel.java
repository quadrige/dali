package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.ErrorAware;
import fr.ifremer.dali.dto.ErrorDTO;
import fr.ifremer.dali.dto.data.measurement.MeasurementAware;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationAware;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliRowUIModel;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.AllowedQualitativeValuesMap;
import fr.ifremer.quadrige3.ui.core.dto.CommentAware;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>OperationMeasurementsGroupedRowModel class.</p>
 */
public class OperationMeasurementsGroupedRowModel extends AbstractDaliRowUIModel<QuadrigeBean, OperationMeasurementsGroupedRowModel>
        implements MeasurementAware, CommentAware, ErrorAware, SamplingOperationAware {

    public static final String PROPERTY_INDIVIDUAL_ID = "individualId";
    public static final String PROPERTY_TAXON = "taxon";
    public static final String PROPERTY_INPUT_TAXON_ID = "inputTaxonId";
    public static final String PROPERTY_INPUT_TAXON_NAME = "inputTaxonName";
    public static final String PROPERTY_TAXON_GROUP = "taxonGroup";
    public static final String PROPERTY_SAMPLING_OPERATION = "samplingOperation";
    public static final String PROPERTY_COMMENT = "comment";
    public static final String PROPERTY_ANALYST = "analyst";
    public static final String PROPERTY_ANALYSIS_INSTRUMENT = "analysisInstrument";
    public static final String PROPERTY_INDIVIDUAL_PMFMS = "individualPmfms";
    public static final String PROPERTY_INDIVIDUAL_MEASUREMENTS = "individualMeasurements";

    private Integer individualId;
    private TaxonDTO taxon;
    private Integer inputTaxonId;
    private String inputTaxonName;
    private TaxonGroupDTO taxonGroup;
    private SamplingOperationDTO samplingOperation;
    private String comment;
    private DepartmentDTO analyst;
    private AnalysisInstrumentDTO analysisInstrument;
    private List<PmfmDTO> individualPmfms;
    private List<MeasurementDTO> individualMeasurements;
    private final List<ErrorDTO> errors;
    private final boolean readOnly;

    // flag indicating if the row is really dirty or not. ie: an initialized row is not dirty
    private boolean initialized;

    // map used to filter qualitative value combos
    private final AllowedQualitativeValuesMap allowedQualitativeValuesMap;

    // used with multi edit model
    private final List<ColumnIdentifier<OperationMeasurementsGroupedRowModel>> multipleValuesOnIdentifier;
    private final List<Integer> multipleValuesOnPmfmIds;

    /**
     * Constructor.
     *
     * @param readOnly a boolean.
     */
    public OperationMeasurementsGroupedRowModel(boolean readOnly) {
        super(null, null);
        this.individualMeasurements = Lists.newArrayList();
        this.errors = Lists.newArrayList();
        this.allowedQualitativeValuesMap = new AllowedQualitativeValuesMap();
        this.multipleValuesOnIdentifier = new ArrayList<>();
        this.multipleValuesOnPmfmIds = new ArrayList<>();
        this.readOnly = readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEditable() {
        return !readOnly && super.isEditable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected QuadrigeBean newBean() {
        return null;
    }

    /**
     * <p>Getter for the field <code>individualId</code>.</p>
     *
     * @return the individualId
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * <p>Setter for the field <code>individualId</code>.</p>
     *
     * @param individualId the individualId to set
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * <p>Getter for the field <code>taxonGroup</code>.</p>
     *
     * @return the groupTaxon
     */
    public TaxonGroupDTO getTaxonGroup() {
        return taxonGroup;
    }

    /**
     * <p>Setter for the field <code>taxonGroup</code>.</p>
     *
     * @param taxonGroup the taxonGroup to set
     */
    public void setTaxonGroup(TaxonGroupDTO taxonGroup) {
        TaxonGroupDTO oldValue = this.taxonGroup;
        this.taxonGroup = taxonGroup;
        firePropertyChange(PROPERTY_TAXON_GROUP, oldValue, taxonGroup);
    }

    /**
     * <p>Getter for the field <code>taxon</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.referential.TaxonDTO} object.
     */
    public TaxonDTO getTaxon() {
        return taxon;
    }

    /**
     * <p>Setter for the field <code>taxon</code>.</p>
     *
     * @param taxon a {@link fr.ifremer.dali.dto.referential.TaxonDTO} object.
     */
    public void setTaxon(TaxonDTO taxon) {
        TaxonDTO oldValue = this.taxon;
        this.taxon = taxon;
        firePropertyChange(PROPERTY_TAXON, oldValue, taxon);
    }

    public Integer getInputTaxonId() {
        return inputTaxonId;
    }

    public void setInputTaxonId(Integer inputTaxonId) {
        Integer oldValue = this.inputTaxonId;
        this.inputTaxonId = inputTaxonId;
        firePropertyChange(PROPERTY_INPUT_TAXON_ID, oldValue, inputTaxonId);
    }

    public String getInputTaxonName() {
        return inputTaxonName;
    }

    public void setInputTaxonName(String inputTaxonName) {
        String oldValue = this.inputTaxonName;
        this.inputTaxonName = inputTaxonName;
        firePropertyChange(PROPERTY_INPUT_TAXON_NAME, oldValue, inputTaxonName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SamplingOperationDTO getSamplingOperation() {
        return samplingOperation;
    }

    /**
     * <p>Setter for the field <code>samplingOperation</code>.</p>
     *
     * @param samplingOperation a {@link fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO} object.
     */
    public void setSamplingOperation(SamplingOperationDTO samplingOperation) {
        SamplingOperationDTO oldValue = this.samplingOperation;
        this.samplingOperation = samplingOperation;
        firePropertyChange(PROPERTY_SAMPLING_OPERATION, oldValue, samplingOperation);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getComment() {
        return comment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setComment(String comment) {
        String oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    /**
     * <p>Getter for the field <code>analyst</code>.</p>
     *
     * @return the analyst
     */
    public DepartmentDTO getAnalyst() {
        return analyst;
    }

    /**
     * <p>Setter for the field <code>analyst</code>.</p>
     *
     * @param analyst the analyst to set
     */
    public void setAnalyst(DepartmentDTO analyst) {
        DepartmentDTO oldValue = this.analyst;
        this.analyst = analyst;
        firePropertyChange(PROPERTY_ANALYST, oldValue, analyst);
    }

    public AnalysisInstrumentDTO getAnalysisInstrument() {
        return analysisInstrument;
    }

    public void setAnalysisInstrument(AnalysisInstrumentDTO analysisInstrument) {
        AnalysisInstrumentDTO oldValue = this.analysisInstrument;
        this.analysisInstrument = analysisInstrument;
        firePropertyChange(PROPERTY_ANALYSIS_INSTRUMENT, oldValue, analysisInstrument);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfms() {
        return getIndividualPmfms();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MeasurementDTO> getMeasurements() {
        return getIndividualMeasurements();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getIndividualPmfms() {
        return individualPmfms;
    }

    /**
     * <p>Setter for the field <code>individualPmfms</code>.</p>
     *
     * @param individualPmfms a {@link java.util.List} object.
     */
    public void setIndividualPmfms(List<PmfmDTO> individualPmfms) {
        List<PmfmDTO> oldValue = this.individualPmfms;
        this.individualPmfms = individualPmfms;
        firePropertyChange(PROPERTY_INDIVIDUAL_PMFMS, oldValue, individualPmfms);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MeasurementDTO> getIndividualMeasurements() {
        return individualMeasurements;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    /**
     * <p>Setter for the field <code>individualMeasurements</code>.</p>
     *
     * @param individualMeasurements a {@link java.util.List} object.
     */
    public void setIndividualMeasurements(List<MeasurementDTO> individualMeasurements) {
        List<MeasurementDTO> oldValue = this.individualMeasurements;
        this.individualMeasurements = individualMeasurements;
        firePropertyChange(PROPERTY_INDIVIDUAL_MEASUREMENTS, oldValue, individualMeasurements);
    }

    public AllowedQualitativeValuesMap getAllowedQualitativeValuesMap() {
        return allowedQualitativeValuesMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    public boolean hasTaxonInformation() {
        return getTaxonGroup() != null || getTaxon() != null;
    }

    public boolean hasNonEmptyMeasurements() {
        return getIndividualMeasurements().stream().anyMatch(measurement -> !DaliBeans.isMeasurementEmpty(measurement));
    }


    public List<ColumnIdentifier<OperationMeasurementsGroupedRowModel>> getMultipleValuesOnIdentifier() {
        return multipleValuesOnIdentifier;
    }

    public void addMultipleValuesOnIdentifier(ColumnIdentifier<OperationMeasurementsGroupedRowModel> identifier) {
        multipleValuesOnIdentifier.add(identifier);
    }

    public List<Integer> getMultipleValuesOnPmfmIds() {
        return multipleValuesOnPmfmIds;
    }

    public void addMultipleValuesOnPmfmId(Integer pmfmId) {
        multipleValuesOnPmfmIds.add(pmfmId);
    }


}
