package fr.ifremer.dali.ui.swing.util.map.layer.wms;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.map.layer.MapLayer;
import org.geotools.data.ows.Layer;
import org.geotools.data.wms.WebMapServer;
import org.geotools.map.WMSLayer;

/**
 * A WMSLayer for map (indeed) for used to identify a layer of map
 *
 * @author peck7 on 05/09/2017.
 */
public class WMSMapLayer extends WMSLayer implements MapLayer {

    /**
     * Builds a new WMS layer
     *
     * @param wms the WMS server instance
     * @param layer the Layer to display
     */
    public WMSMapLayer(WebMapServer wms, Layer layer) {
        super(wms, layer);
    }
}
