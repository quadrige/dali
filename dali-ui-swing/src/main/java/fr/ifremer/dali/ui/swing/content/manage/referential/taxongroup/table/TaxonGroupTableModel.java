package fr.ifremer.dali.ui.swing.content.manage.referential.taxongroup.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class TaxonGroupTableModel extends AbstractDaliTableModel<TaxonGroupTableRowModel> {

    /** Constant <code>PARENT</code> */
    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> PARENT = DaliColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_PARENT_TAXON_GROUP,
            n("dali.property.taxonGroup.parent"),
            n("dali.property.taxonGroup.parent"),
            TaxonGroupDTO.class);

    /** Constant <code>LABEL</code> */
    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> LABEL = DaliColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_LABEL,
            n("dali.property.label"),
            n("dali.property.label"),
            String.class);

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> NAME = DaliColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class,
            true);

    /** Constant <code>STATUS</code> */
    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> STATUS = DaliColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class,
            true);

    /** Constant <code>COMMENT</code> */
    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> COMMENT = DaliColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_COMMENT,
            n("dali.property.comment"),
            n("dali.property.comment"),
            String.class);

    /** Constant <code>TYPE</code> */
    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> TYPE = DaliColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_TYPE,
            n("dali.property.taxonGroup.type"),
            n("dali.property.taxonGroup.type.tip"),
            String.class);

    /** Constant <code>TAXONS</code> */
    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> TAXONS = DaliColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_TAXONS,
            n("dali.property.taxonGroup.taxons"),
            n("dali.property.taxonGroup.taxons"),
            TaxonDTO.class,
            DecoratorService.COLLECTION_SIZE);

    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        TaxonGroupTableRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<TaxonGroupTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        TaxonGroupTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);


    final boolean readOnly;

    /**
     * <p>Constructor for TaxonGroupTableModel.</p>
     *
     * @param readOnly a boolean.
     */
    public TaxonGroupTableModel(final SwingTableColumnModel columnModel, boolean readOnly) {
        super(columnModel, !readOnly, false);
        this.readOnly = readOnly;
    }

    /** {@inheritDoc} */
    @Override
    public TaxonGroupTableRowModel createNewRow() {
        return new TaxonGroupTableRowModel(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<TaxonGroupTableRowModel> propertyName) {

        if (propertyName == TAXONS && readOnly) {
            TaxonGroupTableRowModel row = getEntry(rowIndex);
            return row.sizeTaxons() > 0;
        }

        return super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<TaxonGroupTableRowModel> getFirstColumnEditing() {
        return NAME;
    }
}
