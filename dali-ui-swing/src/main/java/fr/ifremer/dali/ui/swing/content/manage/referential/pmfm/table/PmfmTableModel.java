package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.UnitDTO;
import fr.ifremer.dali.dto.referential.pmfm.*;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class PmfmTableModel extends AbstractDaliTableModel<PmfmTableRowModel> {

    public static final DaliColumnIdentifier<PmfmTableRowModel> PMFM_ID = DaliColumnIdentifier.newReadOnlyId(
            PmfmTableRowModel.PROPERTY_ID,
            n("dali.property.pmfm.id"),
            n("dali.property.pmfm.id"),
            Integer.class);

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> NAME = DaliColumnIdentifier.newPmfmNameId(
            n("dali.property.name"),
            n("dali.property.name"));

    /** Constant <code>PARAMETER</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> PARAMETER = DaliColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_PARAMETER,
            n("dali.property.pmfm.parameter"),
            n("dali.property.pmfm.parameter"),
            ParameterDTO.class,
            true);

    /** Constant <code>SUPPORT</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> SUPPORT = DaliColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_MATRIX,
            n("dali.property.pmfm.matrix"),
            n("dali.property.pmfm.matrix"),
            MatrixDTO.class,
            true);

    /** Constant <code>FRACTION</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> FRACTION = DaliColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_FRACTION,
            n("dali.property.pmfm.fraction"),
            n("dali.property.pmfm.fraction"),
            FractionDTO.class,
            true);

    /** Constant <code>METHOD</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> METHOD = DaliColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_METHOD,
            n("dali.property.pmfm.method"),
            n("dali.property.pmfm.method"),
            MethodDTO.class,
            true);

    /** Constant <code>UNIT</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> UNIT = DaliColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_UNIT,
            n("dali.property.pmfm.unit"),
            n("dali.property.pmfm.unit"),
            UnitDTO.class,
            true);

    // qualitative values
    /** Constant <code>QUALITATIVE_VALUES</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> QUALITATIVE_VALUES = DaliColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_QUALITATIVE_VALUES,
            n("dali.property.pmfm.parameter.associatedQualitativeValue"),
            n("dali.property.pmfm.parameter.associatedQualitativeValue"),
            QualitativeValueDTO.class,
            DecoratorService.COLLECTION_SIZE);

    /** Constant <code>STATUS</code> */
    public static final DaliColumnIdentifier<PmfmTableRowModel> STATUS = DaliColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class,
            true);

    // TODO : threshold, maxDecimal and SignificativeNb are not used in this table... remove ?

//	public static final DaliColumnIdentifier<PmfmTableRowModel> THRESHOLD = DaliColumnIdentifier.newId(
//			PmfmTableRowModel.PROPERTY_THRESHOLD,
//			n("dali.property.pmfm.threshold"),
//			n("dali.property.pmfm.threshold"),
//			Double.class);
//	public static final DaliColumnIdentifier<PmfmTableRowModel> MAX_DECIMAL_NUMBER = DaliColumnIdentifier.newId(
//			PmfmTableRowModel.PROPERTY_MAX_DECIMAL_NB,
//			n("dali.property.pmfm.maxDecimalNumber"),
//			n("dali.property.pmfm.maxDecimalNumber"),
//			Integer.class);
//	public static final DaliColumnIdentifier<PmfmTableRowModel> SIGNIFICANT_DIGITS_NUMBER = DaliColumnIdentifier.newId(
//			PmfmTableRowModel.PROPERTY_SIGNIFICATIVE_FIGURES_NB,
//			n("dali.property.pmfm.significantDigitsNumber"),
//			n("dali.property.pmfm.significantDigitsNumber"),
//			Integer.class);

    public static final DaliColumnIdentifier<PmfmTableRowModel> COMMENT = DaliColumnIdentifier.newId(
        PmfmTableRowModel.PROPERTY_COMMENT,
        n("dali.property.comment"),
        n("dali.property.comment"),
        String.class,
        false);

    public static final DaliColumnIdentifier<PmfmTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        PmfmTableRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<PmfmTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        PmfmTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);


    /**
     * <p>Constructor for PmfmTableModel.</p>
     *
     */
    public PmfmTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, true, false);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmTableRowModel createNewRow() {
        return new PmfmTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<PmfmTableRowModel> getFirstColumnEditing() {
        return PARAMETER;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<PmfmTableRowModel> propertyName) {
        if (propertyName.equals(QUALITATIVE_VALUES)) {
            PmfmTableRowModel row = getEntry(rowIndex);
            return super.isCellEditable(rowIndex, columnIndex, propertyName) && row.getParameter() != null && row.getParameter().isQualitative();
        } else {
            return super.isCellEditable(rowIndex, columnIndex, propertyName);
        }
    }

}
