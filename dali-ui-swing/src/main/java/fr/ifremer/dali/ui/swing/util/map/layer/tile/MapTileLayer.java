package fr.ifremer.dali.ui.swing.util.map.layer.tile;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.map.DataMapPane;
import fr.ifremer.dali.ui.swing.util.map.layer.MapLayer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.DirectLayer;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;
import org.geotools.renderer.lite.RendererUtilities;
import org.geotools.tile.Tile;
import org.geotools.tile.TileService;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.concurrent.*;

/**
 * Map Layer for Tile service which can partially draw to the target graphic progressively
 *
 * @author peck7 on 02/10/2017.
 */
public class MapTileLayer extends DirectLayer implements MapLayer {

    private static final Log LOG = LogFactory.getLog(MapTileLayer.class);
    private static boolean trace = LOG.isTraceEnabled();

    private TileService service;
    private DataMapPane mapPane;

    public MapTileLayer(TileService service, String title) {
        super();
        this.service = service;
        setTitle(title);
    }

    public void setMapPane(DataMapPane mapPane) {
        this.mapPane = mapPane;
    }

    @Override
    public ReferencedEnvelope getBounds() {
        return service.getBounds();
    }

    @Override
    public void draw(Graphics2D graphics, MapContent map, MapViewport theViewport) {

        final MapViewport viewport = new MapViewport(theViewport);

        final ReferencedEnvelope viewportExtent = viewport.getBounds();
        int scale = calculateScale(viewportExtent, viewport.getScreenArea());

        Collection<Tile> tiles = service.findTilesInExtent(viewportExtent, scale, false, 256);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("rendering %s tiles for envelope %s to screen %s (scale = %,d)",
                    tiles.size(), viewportExtent, viewport.getScreenArea(), scale));
        }

        try {
            renderTiles(tiles, viewport.getScreenArea(), viewportExtent, viewport.getWorldToScreen(), graphics);
        } catch (InterruptedException ignored) {
        }

    }

    private void renderTiles(Collection<Tile> tiles, Rectangle rectangle,
                             ReferencedEnvelope viewportExtent, AffineTransform worldToImageTransform, Graphics2D targetGraphics) throws InterruptedException {

        BufferedImage mosaicImage = new BufferedImage(rectangle.width, rectangle.height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = mosaicImage.createGraphics();

        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        ExecutorService pool = Executors.newFixedThreadPool(8);

        for (Tile tile : tiles) {
            ReferencedEnvelope nativeTileEnvelope = tile.getExtent();

            ReferencedEnvelope tileEnvViewport;
            try {
                tileEnvViewport = nativeTileEnvelope.transform(viewportExtent.getCoordinateReferenceSystem(), true);
            } catch (TransformException | FactoryException e) {
                throw new RuntimeException(e);
            }
            double[] points = new double[4];
            points[0] = tileEnvViewport.getMinX();
            points[3] = tileEnvViewport.getMinY();
            points[2] = tileEnvViewport.getMaxX();
            points[1] = tileEnvViewport.getMaxY();

            worldToImageTransform.transform(points, 0, points, 0, 2);
            Rectangle tileRect = new Rectangle((int) points[0], (int) points[1], (int) Math.ceil(points[2] - points[0]), (int) Math.ceil(points[3] - points[1]));

            // log the tile info
            if (trace) {
                LOG.trace(String.format("ask for tile '%s' , bounds:%s", tile.getId(), tileRect));
            }

            Runnable callback = () -> {
                if (mapPane != null && mapPane.getMapBuilder().getCurrentMapMode().isProgressiveRender()) {

                    // partial draw
                    targetGraphics.drawImage(mosaicImage, 0, 0, null);
                    // call mapPane to repaint on each iteration
                    mapPane.repaint();

                }
            };

            pool.execute(new ImageLoader(tile, tileRect, g2d, callback));

        }

        pool.shutdown();
        pool.awaitTermination(10, TimeUnit.SECONDS);

        // final draw
        targetGraphics.drawImage(mosaicImage, 0, 0, null);

    }

    private class ImageLoader implements Runnable {

        private final FutureTask<BufferedImage> task;

        ImageLoader(Tile tile, Rectangle rectangle, Graphics2D g2d, Runnable callback) {

            Callable<BufferedImage> callable = () -> {
                if (trace) LOG.trace("load tile " + tile.getId());
                return tile.getBufferedImage();
            };

            task = new FutureTask<BufferedImage>(callable) {
                @Override
                protected void done() {

                    if (isCancelled()) {
                        if (trace) LOG.trace("cancel tile " + tile.getId());
                        return;
                    }
                    if (trace) LOG.trace("draw tile " + tile.getId());
                    try {

                        g2d.drawImage(get(), rectangle.x, rectangle.y, rectangle.width, rectangle.height, null);

                        // debug rectangle
//                        g2d.setColor(Color.RED);
//                        g2d.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

                        callback.run();

                    } catch (InterruptedException | ExecutionException e) {
                        LOG.error("can get the image", e);
                    }

                }
            };
        }

        @Override
        public void run() {
            task.run();
        }
    }

    private int calculateScale(ReferencedEnvelope extent, Rectangle screenArea) {

        int scale;
        try {
            scale = (int) Math.round(RendererUtilities.calculateScale(extent, screenArea.width, screenArea.height, 90));
        } catch (FactoryException | TransformException ex) {
            throw new RuntimeException("Failed to calculate scale", ex);
        }
        return scale;
    }
}
