package fr.ifremer.dali.ui.swing.content.manage.rule.program;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des programmes
 */
public class ControlProgramTableUIHandler extends
        AbstractDaliTableUIHandler<ControlProgramTableRowModel, ControlProgramTableUIModel, ControlProgramTableUI> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ControlProgramTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlProgramTableUIModel model = new ControlProgramTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ControlProgramTableUI ui) {

        initUI(ui);

        // Disable buttons
        getUI().getAddProgramButton().setEnabled(false);
        getUI().getRemoveProgramButton().setEnabled(false);

        // Initialisation du tableau
        initTable();

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // Code
        TableColumnExt codeCol = addColumn(ControlProgramTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // Libelle
        TableColumnExt nameCol = addColumn(ControlProgramTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // Description
        TableColumnExt descriptionCol = addColumn(ControlProgramTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        ControlProgramTableModel tableModel = new ControlProgramTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Initialisation du tableau
        initTable(table);

        // Number rows visible
        table.setVisibleRowCount(3);
    }

    /**
     * <p>loadProgrammesControle.</p>
     *
     * @param programs a {@link Collection} object.
     * @param readOnly read only ?
     */
    public void loadPrograms(final Collection<ProgramDTO> programs, boolean readOnly) {

        // Load table
        getModel().setBeans(programs);
        getTable().setEditable(!readOnly);

        // Activate add button
        getUI().getAddProgramButton().setEnabled(!readOnly);

        recomputeRowsValidState(false);
    }

    /**
     * <p>supprimerProgrammesControle.</p>
     */
    public void clearTable() {

        getModel().setBeans(null);

        getUI().getAddProgramButton().setEnabled(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<ControlProgramTableRowModel> getTableModel() {
        return (ControlProgramTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getControlProgramTable();
    }

    /**
     * <p>openAddDialog.</p>
     */
    public void openAddDialog() {

        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.PROGRAM.getFilterTypeId());
        dialog.setTitle(t("dali.filter.program.addDialog.title"));
        List<ProgramDTO> programs = getModel().getBeans().stream()
                .sorted(getDecorator(ProgramDTO.class, null).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(programDTO -> programDTO.setReadOnly(true))
                .collect(Collectors.toList());
        dialog.getModel().setSelectedElements(programs);

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            List<ProgramDTO> programsToAdd = dialog.getModel().getSelectedElements().stream()
                    .map(element -> ((ProgramDTO) element))
                    .filter(program -> !getModel().getBeans().contains(program))
                    .collect(Collectors.toList());

            if (!programsToAdd.isEmpty()) {

                Set<String> managedProgramCodes = getContext().getProgramStrategyService().getManagedProgramCodesByQuserId(getContext().getDataContext().getPrincipalUserId());
                List<ProgramDTO> rejectedPrograms = programsToAdd.stream()
                        .filter(programToAdd -> !managedProgramCodes.contains(programToAdd.getCode()))
                        .collect(Collectors.toList());
                if (!rejectedPrograms.isEmpty()) {

                    // message
                    getContext().getDialogHelper().showWarningDialog(
                            t("dali.rule.program.add.invalid.message"),
                            ApplicationUIUtil.getHtmlString(DaliBeans.transformCollection(rejectedPrograms, this::decorate)),
                            null,
                            t("dali.rule.program.add.invalid.title")
                    );

                    programsToAdd.removeAll(rejectedPrograms);
                }

                getModel().addBeans(programsToAdd);
                getModel().setModify(!programsToAdd.isEmpty());

                saveToParentModel();
            }
        }
    }

    /**
     * <p>removePrograms.</p>
     */
    public void removePrograms() {
        getModel().deleteSelectedRows();
        saveToParentModel();
    }

    private void saveToParentModel() {
        getModel().getParentModel().getRuleListUIModel().getSingleSelectedRow().setPrograms(getModel().getBeans());
        recomputeRowsValidState(false);

        getModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);
    }

}
