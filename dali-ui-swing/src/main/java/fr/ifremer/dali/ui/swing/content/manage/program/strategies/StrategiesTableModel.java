package fr.ifremer.dali.ui.swing.content.manage.program.strategies;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class StrategiesTableModel extends AbstractDaliTableModel<StrategiesTableRowModel> {

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final DaliColumnIdentifier<StrategiesTableRowModel> NAME = DaliColumnIdentifier.newId(
    		StrategiesTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.program.strategy.name.tip"),
            String.class,
			true);
    
    /**
     * Identifiant pour la colonne description.
     */
    public static final DaliColumnIdentifier<StrategiesTableRowModel> COMMENT = DaliColumnIdentifier.newId(
    		StrategiesTableRowModel.PROPERTY_COMMENT,
            n("dali.property.description"),
            n("dali.program.strategy.description.tip"),
            String.class,
			true);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public StrategiesTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, true, false);
	}

	/** {@inheritDoc} */
	@Override
	public StrategiesTableRowModel createNewRow() {
		return new StrategiesTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<StrategiesTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
