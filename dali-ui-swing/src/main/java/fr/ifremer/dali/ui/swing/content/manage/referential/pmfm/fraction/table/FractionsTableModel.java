package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>FractionsTableModel class.</p>
 *
 * @author Antoine
 */
public class FractionsTableModel extends AbstractDaliTableModel<FractionsTableRowModel> {

    /** Constant <code>NAME</code> */
    public static final DaliColumnIdentifier<FractionsTableRowModel> NAME = DaliColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.property.name"),
            String.class,
            true);
    /** Constant <code>DESCRIPTION</code> */
    public static final DaliColumnIdentifier<FractionsTableRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_DESCRIPTION,
            n("dali.property.description"),
            n("dali.property.description"),
            String.class);
    /** Constant <code>STATUS</code> */
    public static final DaliColumnIdentifier<FractionsTableRowModel> STATUS = DaliColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_STATUS,
            n("dali.property.status"),
            n("dali.property.status"),
            StatusDTO.class,
            true);
    /** Constant <code>ASSOCIATED_SUPPORTS</code> */
    public static final DaliColumnIdentifier<FractionsTableRowModel> ASSOCIATED_SUPPORTS = DaliColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_MATRIXES,
            n("dali.property.pmfm.fraction.associatedMatrices"),
            n("dali.property.pmfm.fraction.associatedMatrices"),
            MatrixDTO.class,
            DecoratorService.COLLECTION_SIZE,
            true);

    public static final DaliColumnIdentifier<FractionsTableRowModel> COMMENT = DaliColumnIdentifier.newId(
        FractionsTableRowModel.PROPERTY_COMMENT,
        n("dali.property.comment"),
        n("dali.property.comment"),
        String.class,
        false);

    public static final DaliColumnIdentifier<FractionsTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
        FractionsTableRowModel.PROPERTY_CREATION_DATE,
        n("dali.property.date.creation"),
        n("dali.property.date.creation"),
        Date.class);

    public static final DaliColumnIdentifier<FractionsTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        FractionsTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);


    /**
     * <p>Constructor for FractionsTableModel.</p>
     *
     */
    public FractionsTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public FractionsTableRowModel createNewRow() {
        return new FractionsTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public DaliColumnIdentifier<FractionsTableRowModel> getFirstColumnEditing() {
        return null;
    }
}
