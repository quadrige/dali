package fr.ifremer.dali.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ManageContextsListTableUITableModel extends AbstractDaliTableModel<ManageContextsListTableUIRowModel> {

	/** Constant <code>NAME</code> */
	public static final DaliColumnIdentifier<ManageContextsListTableUIRowModel> LIBELLE = DaliColumnIdentifier.newId(
			ManageContextsListTableUIRowModel.PROPERTY_NAME,
			n("dali.property.name"),
			n("dali.context.contextsList.name.tip"),
			String.class,
            true);
	
	/** Constant <code>DESCRIPTION</code> */
	public static final DaliColumnIdentifier<ManageContextsListTableUIRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
			ManageContextsListTableUIRowModel.PROPERTY_DESCRIPTION,
			n("dali.property.description"),
			n("dali.context.contextsList.description.tip"),
			String.class);
 
	/**
	 * <p>Constructor for ManageContextsListTableUITableModel.</p>
	 *
	 */
	public ManageContextsListTableUITableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, true, false);
	}

	/** {@inheritDoc} */
	@Override
	public ManageContextsListTableUIRowModel createNewRow() {
		return new ManageContextsListTableUIRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ManageContextsListTableUIRowModel> getFirstColumnEditing() {
		return LIBELLE;
	}
}
