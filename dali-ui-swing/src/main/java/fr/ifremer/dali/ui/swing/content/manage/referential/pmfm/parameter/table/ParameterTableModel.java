package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.parameter.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.ParameterGroupDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>ParameterTableModel class.</p>
 *
 * @author Antoine
 */
public class ParameterTableModel extends AbstractDaliTableModel<ParameterTableRowModel> {

	
	/**
	 * <p>Constructor for ParameterTableModel.</p>
	 *
	 * @param createNewRowAllowed a boolean.
	 */
	public ParameterTableModel(final SwingTableColumnModel columnModel, boolean createNewRowAllowed) {
		super(columnModel, createNewRowAllowed, false);
	}
	
	/** Constant <code>CODE</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> CODE = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_TRANSCRIBED_CODE, // use transcribed code (Mantis #47330)
			n("dali.property.code"),
			n("dali.property.code"),
			String.class,
			true);
	
	/** Constant <code>NAME</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> NAME = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_NAME,
			n("dali.property.name"),
			n("dali.property.name"),
			String.class,
			true);    
    
	/** Constant <code>DESCRIPTION</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_DESCRIPTION,
			n("dali.property.description"),
			n("dali.property.description"),
			String.class);
	
	/** Constant <code>STATUS</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> STATUS = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_STATUS,
			n("dali.property.status"),
			n("dali.property.status"),
			StatusDTO.class,
			true);
	
	/** Constant <code>ASSOCIATED_QUALITATIVE_VALUE</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> ASSOCIATED_QUALITATIVE_VALUE = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_QUALITATIVE_VALUES,
			n("dali.property.pmfm.parameter.associatedQualitativeValue"),
			n("dali.property.pmfm.parameter.associatedQualitativeValue"),
			QualitativeValueDTO.class,
			DecoratorService.COLLECTION_SIZE);
	
	/** Constant <code>PARAMETER_GROUP</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> PARAMETER_GROUP = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_PARAMETER_GROUP,
			n("dali.property.pmfm.parameter.parameterGroup"),
			n("dali.property.pmfm.parameter.parameterGroup"),
			ParameterGroupDTO.class,
			true);
	
	/** Constant <code>IS_QUALITATIVE</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> IS_QUALITATIVE = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_QUALITATIVE,
			n("dali.property.pmfm.parameter.qualitative"),
			n("dali.property.pmfm.parameter.qualitative"),
			Boolean.class);
	
	/** Constant <code>IS_CALCULATED</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> IS_CALCULATED = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_CALCULATED,
			n("dali.property.pmfm.parameter.calculated"),
			n("dali.property.pmfm.parameter.calculated"),
			Boolean.class);
	
	/** Constant <code>IS_TAXONOMIC</code> */
	public static final DaliColumnIdentifier<ParameterTableRowModel> IS_TAXONOMIC = DaliColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_TAXONOMIC,
			n("dali.property.pmfm.parameter.taxonomic"),
			n("dali.property.pmfm.parameter.taxonomic"),
			Boolean.class);

	public static final DaliColumnIdentifier<ParameterTableRowModel> COMMENT = DaliColumnIdentifier.newId(
		ParameterTableRowModel.PROPERTY_COMMENT,
		n("dali.property.comment"),
		n("dali.property.comment"),
		String.class,
		false);

	public static final DaliColumnIdentifier<ParameterTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
		ParameterTableRowModel.PROPERTY_CREATION_DATE,
		n("dali.property.date.creation"),
		n("dali.property.date.creation"),
		Date.class);

	public static final DaliColumnIdentifier<ParameterTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
		ParameterTableRowModel.PROPERTY_UPDATE_DATE,
		n("dali.property.date.modification"),
		n("dali.property.date.modification"),
		Date.class);


	/** {@inheritDoc} */
	@Override
	public ParameterTableRowModel createNewRow() {
		return new ParameterTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ParameterTableRowModel> getFirstColumnEditing() {
		return CODE;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<ParameterTableRowModel> propertyName) {
		boolean editable = super.isCellEditable(rowIndex, columnIndex, propertyName);

		if (editable && ASSOCIATED_QUALITATIVE_VALUE.equals(propertyName)) {

			ParameterTableRowModel rowModel = getEntry(rowIndex);
			editable = rowModel.isQualitative();
		}

		return editable;
	}
}
