package fr.ifremer.dali.ui.swing.content.extraction.config;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.ui.swing.content.extraction.ExtractionUI;
import fr.ifremer.dali.ui.swing.content.extraction.config.preset.PmfmPresetUI;
import fr.ifremer.dali.ui.swing.content.extraction.config.preset.PmfmPresetUIModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import org.apache.commons.collections4.CollectionUtils;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 21/11/2017.
 */
public class ExtractionConfigUIHandler extends AbstractDaliUIHandler<ExtractionConfigUIModel, ExtractionConfigUI> {

    @Override
    public void beforeInit(ExtractionConfigUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(new ExtractionConfigUIModel());
    }

    @Override
    public void afterInit(ExtractionConfigUI ui) {

        initUI(ui);
        initListeners();
        loadConfig();
        getModel().setEnabled(false);

    }

    private void initListeners() {

        PropertyChangeListener configListener = evt -> {

            // build text
            buildPmfmPresetText();

            if (!getModel().isLoading()) getModel().setModify(true);

        };
        getModel().addPropertyChangeListener(ExtractionConfigUIModel.PROPERTY_PMFM_PRESETS, configListener);
        getModel().addPropertyChangeListener(ExtractionConfigUIModel.PROPERTY_PMFM_RESULTS, configListener);

        getUI().getPmfmPresetTextField().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() >= 2) {
                    openConfig();
                }
            }
        });

    }

    private void buildPmfmPresetText() {

        String text = null;
        if (!getModel().isPmfmPresetsEmpty() && !getModel().isPmfmResultsEmpty()) {

            StringJoiner resultPmfmJoiner = new StringJoiner(", ");
            for (PmfmDTO resultPmfm : getModel().getPmfmResults()) {
                resultPmfmJoiner.add(decorate(resultPmfm, DecoratorService.NAME));
            }

            StringJoiner pmfmPresetJoiner = new StringJoiner(" " + t("dali.extraction.config.pmfmPreset.text.and") + " ");
            for (PmfmPresetDTO pmfmPreset : getModel().getPmfmPresets()) {
                if (pmfmPreset.isQualitativeValuesEmpty()) continue;
                pmfmPresetJoiner.add(String.format("%s%s",
                        decorate(pmfmPreset.getPmfm(), DecoratorService.NAME),
                        pmfmPreset.sizeQualitativeValues() == pmfmPreset.getPmfm().sizeQualitativeValues() ? "" :
                                String.format(" (%s,...)", decorate(pmfmPreset.getQualitativeValues(0)))));
            }

            text = t("dali.extraction.config.pmfmPreset.text", resultPmfmJoiner.toString(), pmfmPresetJoiner.toString());

        }

        getUI().getPmfmPresetTextField().setText(text);

    }

    public void loadConfig() {

        getModel().setLoading(true);
        if (getModel().getSelectedExtraction() == null) {

            // empty and disable
            getModel().fromBean(null);

        } else {
            // load parameter
            getModel().fromBean(getModel().getSelectedExtraction().getParameter());

        }
        getModel().setModify(false);
        getModel().setLoading(false);
    }

    public void openConfig() {

        PmfmPresetUI pmfmPresetUI = new PmfmPresetUI(getContext());
        PmfmPresetUIModel presetUIModel = pmfmPresetUI.getModel();

        // get all pmfm strategies for the actual filters
        Set<PmfmDTO> allPmfms = getParentContainer(ExtractionUI.class).getHandler().getPmfmsForSelectedExtraction(null, null);

        // parse
        List<PmfmDTO> qualitativePmfms = DaliBeans.filterQualitativePmfms(allPmfms);
        List<PmfmDTO> numericalPmfms = DaliBeans.filterNumericalPmfms(allPmfms);

        // if no pmfm found
        if (CollectionUtils.isEmpty(qualitativePmfms)) {

            getContext().getDialogHelper().showWarningDialog(
                    t("dali.extraction.config.missing.qualitativePmfms"),
                    t("dali.extraction.config.title"));
            return;
        }
        if (CollectionUtils.isEmpty(numericalPmfms)) {

            getContext().getDialogHelper().showWarningDialog(
                    t("dali.extraction.config.missing.numericalPmfms"),
                    t("dali.extraction.config.title"));
            return;
        }

        // set available pmfms
        presetUIModel.setAvailablePmfms(qualitativePmfms);
        presetUIModel.setAvailableResultPmfms(numericalPmfms);

        // Set presets if exists, AFTER available pmfms
        presetUIModel.setInitialPmfmPresets(new ArrayList<>(getModel().getPmfmPresets()));

        // Set result pmfms if exists
        presetUIModel.setInitialResultPmfms(new ArrayList<>(getModel().getPmfmResults()));

        openDialog(pmfmPresetUI, new Dimension(1000, (int) (getContext().getMainUI().getHeight() * 0.95)));

        if (presetUIModel.isValid()) {

            getModel().setPmfmResults(presetUIModel.getSelectedResultPmfms());
            getModel().setPmfmPresets(presetUIModel.getBeans());

            // Force modify state
            getModel().firePropertyChanged(AbstractBeanUIModel.PROPERTY_MODIFY, null, true);
        }

    }

}
