package fr.ifremer.dali.ui.swing.content.synchro.changes.duplicate;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.common.synchro.service.RejectedRow;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.system.synchronization.SynchroRowDTO;
import fr.ifremer.dali.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.dali.ui.swing.content.synchro.changes.SynchroChangesRowModel;
import fr.ifremer.dali.ui.swing.content.synchro.changes.SynchroChangesTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.synchro.service.client.vo.SynchroOperationType;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.SortOrder;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class SynchroDuplicatesUIHandler extends AbstractDaliTableUIHandler<SynchroChangesRowModel, SynchroDuplicatesUIModel, SynchroDuplicatesUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SynchroDuplicatesUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SynchroDuplicatesUIModel model = new SynchroDuplicatesUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(SynchroDuplicatesUI ui) {

        // Init UI
        initUI(ui);

        // Init table
        initTable();
        ui.getTablePanel().setVisible(false);
        ui.pack();

        getModel().addPropertyChangeListener(SynchroDuplicatesUIModel.PROPERTY_CHANGES, evt -> populateUI());

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // Get the main table
        final SwingTable table = getTable();

        // Column name
        final TableColumnExt columnName = addColumn(
                SynchroChangesTableModel.NAME);
        columnName.setSortable(true);
        columnName.setEditable(false);
        columnName.setPreferredWidth(500);
        columnName.setWidth(500);

        // Modele de la table
        final SynchroChangesTableModel tableModel = new SynchroChangesTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Les columns obligatoire sont toujours presentes
        columnName.setHideable(false);

        table.setEditable(true);

        // Initialisation de la table
        initTable(table, false, true);

        // Tri par defaut
        table.setSortOrder(SynchroChangesTableModel.NAME, SortOrder.ASCENDING);

        table.setVisibleRowCount(5);
    }

    private void populateUI() {

        Assert.notNull(getModel().getChanges());

        // This filter is mandatory because this UI handles only one table type
        Assert.notNull(getModel().getTableNameFilter());

        // get the change
        SynchroTableDTO synchroTable = DaliBeans.findByProperty(getModel().getChanges().getTables(), SynchroTableDTO.PROPERTY_NAME, getModel().getTableNameFilter());
        Assert.notNull(synchroTable);
        getModel().setTableChange(synchroTable);

        int nbInsert = getInsertRows(synchroTable).size();
        int nbDuplicate = getDuplicateRows(synchroTable).size();
        boolean hasChanges = (nbInsert > 0 || nbDuplicate > 0);

        List<String> strings = new ArrayList<>();

        if (nbInsert > 0) {
            strings.add(t("dali.synchro.duplicates.nbInsert", nbInsert, decorate(synchroTable)));
        }
        if (nbDuplicate > 0) {
            strings.add(t("dali.synchro.duplicates.nbDuplicate", nbDuplicate, decorate(synchroTable)));
            getUI().getShowDuplicatesButton().setVisible(true);
            // populate table
            populateTable(synchroTable);
        } else {
            getUI().getShowDuplicatesButton().setVisible(false);
        }
        getUI().getChangesLabel().setText(ApplicationUIUtil.getHtmlString(strings));

        // Display a message on survey ignored because of missing programs (mantis #37518)
        List<SynchroRowDTO> ignoredRows = getRowsByType(synchroTable, SynchroOperationType.IGNORE);
        if (ignoredRows.size() > 0) {
            List<String> progCds = DaliBeans.collectProperties(ignoredRows, SynchroRowDTO.PROPERTY_NAME);
            String message = t("dali.synchro.duplicates.nbIgnore",
                    ignoredRows.size(),
                    decorate(synchroTable),
                    ApplicationUIUtil.formatHtmlList(Sets.newHashSet(progCds))); // Use a Set, to remove duplicated prog_cd
            getUI().getIgnoreLabel().setText(ApplicationUIUtil.getHtmlString(Lists.newArrayList(message)));

            // No other changes: user can only cancel
            if (!hasChanges) {
                getUI().getIgnoreHelpLabel().setText(ApplicationUIUtil.getHtmlString(t("dali.synchro.duplicates.nbIgnore.help.cancelOnly")));
                getUI().getConfirmBouton().setEnabled(false);
                getUI().getChangesLabel().setVisible(false);
            }
            else {
                getUI().getIgnoreHelpLabel().setText(ApplicationUIUtil.getHtmlString(t("dali.synchro.duplicates.nbIgnore.help")));
            }
        }
        else {
            getUI().getIgnorePanel().setVisible(false);
        }
    }

    /**
     * <p>populateTable.</p>
     *
     * @param synchroTable a {@link SynchroTableDTO} object.
     */
    private void populateTable(SynchroTableDTO synchroTable) {

        TableColumnExt nameColumn = getTable().getColumnExt(SynchroChangesTableModel.NAME);

        if (synchroTable == null || CollectionUtils.isEmpty(synchroTable.getRows())) {

            nameColumn.setTitle(t("dali.synchro.duplicates.name.short.default"));

        } else {

            nameColumn.setTitle(t("dali.synchro.duplicates.name.short", decorate(synchroTable)));
            getModel().setBeans(getDuplicateRows(synchroTable));
        }

    }

    /**
     * <p>showDuplicates.</p>
     */
    public void showDuplicates() {

        getUI().getTablePanel().setVisible(true);
        getUI().getShowDuplicatesButton().setVisible(false);
        getUI().pack();
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<SynchroChangesRowModel> getTableModel() {
        return (SynchroChangesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getTable();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        getModel().setChangesValidated(false);
        closeDialog();
    }

    /**
     * <p>confirm.</p>
     */
    public void confirm() {

        // remove old table from changes
        getModel().getChanges().removeTables(getModel().getTableChange());

        // build another SynchroTableDTO
        SynchroTableDTO synchroTable = DaliBeanFactory.newSynchroTableDTO();
        synchroTable.setName(getModel().getTableChange().getName());
        synchroTable.setRows(new ArrayList<>());

        // add insertRows by adding strategy
        synchroTable.addAllRows(DaliBeans.transformCollection(getInsertRows(getModel().getTableChange()), synchroRow -> {
                if (synchroRow != null) {
                    synchroRow.setStrategy(RejectedRow.ResolveStrategy.UPDATE.toString());
                }
                return synchroRow;
        }));

        // add duplicates rows
        synchroTable.addAllRows(DaliBeans.transformCollection(getModel().getSelectedBeans(), synchroRow -> {
                if (synchroRow != null) {
                    synchroRow.setStrategy(RejectedRow.ResolveStrategy.DUPLICATE.toString());
                }
                return synchroRow;
        }));

        getModel().getChanges().addTables(synchroTable);

        getModel().setChangesValidated(true);
        closeDialog();
    }

    /* -- Internal methods -- */

    private List<SynchroRowDTO> getDuplicateRows(SynchroTableDTO synchroTable) {
        return getRowsByType(synchroTable, SynchroOperationType.DUPLICATE);
    }

    private List<SynchroRowDTO> getInsertRows(SynchroTableDTO synchroTable) {
        return getRowsByType(synchroTable, SynchroOperationType.INSERT);
    }

    private List<SynchroRowDTO> getRowsByType(final SynchroTableDTO synchroTable, final SynchroOperationType operationType) {
        final String typeStr = operationType.name();

        return DaliBeans.filterCollection(synchroTable.getRows(), (Predicate<SynchroRowDTO>) input -> input != null && typeStr.equalsIgnoreCase(input.getOperationType()));
    }

}
