<!--
  #%L
  Dali :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JDialog id='editPmfmDialogUI' layout='{new BorderLayout()}'
         implements="fr.ifremer.dali.ui.swing.util.DaliUI&lt;EditPmfmDialogUIModel, EditPmfmDialogUIHandler&gt;">

  <import>
    fr.ifremer.dali.ui.swing.util.DaliUI
    fr.ifremer.dali.ui.swing.DaliUIContext
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil

    fr.ifremer.dali.dto.referential.DepartmentDTO
    fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO

    fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox
  </import>

  <EditPmfmDialogUIModel id='model' initializer='getContextValue(EditPmfmDialogUIModel.class)'/>

  <script><![CDATA[
		public EditPmfmDialogUI(DaliUIContext context) {
			super(context.getMainUI());
			ApplicationUIUtil.setApplicationContext(this, context);
		}
	]]></script>

  <Table fill="both">
    <row>
      <cell weightx='0' fill='both'>
        <JLabel id="passageLabel"/>
      </cell>
      <cell weightx='1' fill='both'>
        <JCheckBox id='passageCheckbox' onItemStateChanged='handler.setBoolean(event, PmfmStrategyDTO.PROPERTY_SURVEY)'/>
      </cell>
    </row>
    <row>
      <cell weightx='0' fill='both'>
        <JLabel id="prelevementLabel"/>
      </cell>
      <cell weightx='1' fill='both'>
        <JCheckBox id='prelevementCheckbox' onItemStateChanged='handler.setBoolean(event, PmfmStrategyDTO.PROPERTY_SAMPLING)'/>
      </cell>
    </row>
    <row>
      <cell weightx='0' fill='both'>
        <JLabel id="regroupementLabel"/>
      </cell>
      <cell weightx='1' fill='both'>
        <JCheckBox id='regroupementCheckbox' onItemStateChanged='handler.setBoolean(event, PmfmStrategyDTO.PROPERTY_GROUPING)'/>
      </cell>
    </row>
    <row>
      <cell weightx='0' fill='both'>
        <JLabel id="uniciteLabel"/>
      </cell>
      <cell weightx='1' fill='both'>
        <JCheckBox id='uniciteCheckbox' onItemStateChanged='handler.setBoolean(event, PmfmStrategyDTO.PROPERTY_UNIQUE)'/>
      </cell>
    </row>
  </Table>

  <JPanel layout='{new GridLayout(1,0)}' constraints='BorderLayout.PAGE_END'>
    <JButton id="cancelButton" onActionPerformed='handler.cancel()'/>
    <JButton id="validButton" onActionPerformed='handler.valid()'/>
  </JPanel>

</JDialog>