package fr.ifremer.dali.ui.swing.content.extraction.list;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 22/01/2018.
 */
public class ImportExtractionAction extends AbstractDaliAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

    private File importFile;
    private ExtractionDTO extraction;

    /**
     * <p>Constructor for ExportExtractionAction.</p>
     *
     * @param handler a H object.
     */
    public ImportExtractionAction(ExtractionsTableUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) return false;

        String fileExtension = getConfig().getExtractionFileExtension();
        importFile = chooseFile(
                t("dali.extraction.list.import.tip"),
                t("dali.common.import"),
                "^.*\\." + fileExtension,
                t("dali.common.file." + fileExtension)
        );

        return importFile != null;
    }

    @Override
    public void doAction() throws Exception {

        extraction = getContext().getExtractionService().importExtraction(importFile);

    }

    @Override
    public void postSuccessAction() {

        if (extraction != null) {
            getModel().addBean(extraction);
            getModel().setModify(true);
        }

        super.postSuccessAction();
    }

    @Override
    protected void releaseAction() {
        super.releaseAction();

        importFile = null;
        extraction = null;
    }
}
