package fr.ifremer.dali.ui.swing.content.observation.photo;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.PhotoTypeDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des photos.
 */
public class PhotosTableModel extends AbstractDaliTableModel<PhotosTableRowModel> {

    /**
     * Identifiant pour la colonne mnemonique.
     */
    public static final DaliColumnIdentifier<PhotosTableRowModel> NAME = DaliColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.photo.name.tip"),
            String.class,
            true);
    
    /**
     * Identifiant pour la colonne type.
     */
    public static final DaliColumnIdentifier<PhotosTableRowModel> TYPE = DaliColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_PHOTO_TYPE,
            n("dali.photo.type"),
            n("dali.photo.type.tip"),
            PhotoTypeDTO.class);
    
    /**
     * Identifiant pour la colonne legende.
     */
    public static final DaliColumnIdentifier<PhotosTableRowModel> CAPTION = DaliColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_CAPTION,
            n("dali.photo.caption"),
            n("dali.photo.caption.tip"),
            String.class);
    
    /**
     * Identifiant pour la colonne date.
     */
    public static final DaliColumnIdentifier<PhotosTableRowModel> DATE = DaliColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_DATE,
            n("dali.property.date"),
            n("dali.photo.date.tip"),
            Date.class,
            true);
    
    /**
     * Identifiant pour la colonne prelevement.
     */
    public static final DaliColumnIdentifier<PhotosTableRowModel> SAMPLING_OPERATION = DaliColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_SAMPLING_OPERATION,
            n("dali.property.samplingOperation"),
            n("dali.photo.samplingOperation.tip"),
            SamplingOperationDTO.class,
            DecoratorService.CONCAT);
    
    /**
     * Identifiant pour la colonne direction.
     */
    public static final DaliColumnIdentifier<PhotosTableRowModel> DIRECTION = DaliColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_DIRECTION,
            n("dali.photo.direction"),
            n("dali.photo.direction.tip"),
            String.class);
    
    /**
     * Identifiant pour la colonne chemin physique.
     */
    public static final DaliColumnIdentifier<PhotosTableRowModel> PATH = DaliColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_PATH,
            n("dali.photo.path"),
            n("dali.photo.path.tip"),
            String.class);

    private boolean readOnly;

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public PhotosTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
        this.readOnly = false;
    }

    /**
     * <p>Setter for the field <code>readOnly</code>.</p>
     *
     * @param readOnly a boolean.
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    /** {@inheritDoc} */
    @Override
	public PhotosTableRowModel createNewRow() {
		return new PhotosTableRowModel(readOnly);
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<PhotosTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
