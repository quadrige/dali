package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped;

import com.google.common.collect.Multimap;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.service.DaliBusinessException;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.vo.PresetVO;
import fr.ifremer.quadrige3.core.dao.technical.factorization.Combination;
import fr.ifremer.quadrige3.core.dao.technical.factorization.CombinationList;
import fr.ifremer.quadrige3.core.dao.technical.factorization.MaxCombinationExceededException;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

import javax.swing.SwingWorker;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 03/07/2020.
 */
class GridInitializer extends SwingWorker<Object, Void> {

    private final Log LOG = LogFactory.getLog(GridInitializer.class);
    private final OperationMeasurementsGroupedTableUIHandler handler;
    private final PresetVO preset;
    private final List<OperationMeasurementsGroupedRowModel> rows;

    GridInitializer(OperationMeasurementsGroupedTableUIHandler handler, PresetVO preset) {
        this.handler = handler;
        this.preset = preset;
        rows = new ArrayList<>();
    }

    @Override
    protected Object doInBackground() {

        if (LOG.isDebugEnabled()) {
            // count all possible combinations
            long count = 1;
            for (Integer pmfmId : preset.getPmfmIds()) {
                count *= preset.getQualitativeValueIds(pmfmId).size();
            }
            LOG.debug("possible combinations = " + count);
        }

        CombinationList combinations = handler.getContext().getRuleListService().buildAndFactorizeAllowedValues(
            preset,
            handler.getModel().getPreconditionRulesByPmfmIdMap(),
            handler.getConfig().getGridInitializationMaxCombinationCount());

        // init the grid and values
        List<SamplingOperationDTO> samplingOperations =
            handler.getModel().getSamplingFilter() != null
                ? Collections.singletonList(handler.getModel().getSamplingFilter())
                : handler.getModel().getSamplingOperations();

        DepartmentDTO analyst = handler.getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(handler.getModel().getSurvey());

        Multimap<SamplingOperationDTO, OperationMeasurementsGroupedRowModel> existingRowsBySamplingOperation = DaliBeans.populateByProperty(
            handler.getModel().getRows(),
            OperationMeasurementsGroupedRowModel.PROPERTY_SAMPLING_OPERATION
        );

        for (SamplingOperationDTO samplingOperation : samplingOperations) {
            for (Combination combination : combinations) {

                // Check presence of a row with this combination and skip it (Mantis #52403)
                if (isPresent(existingRowsBySamplingOperation.get(samplingOperation), combination))
                    continue;

                OperationMeasurementsGroupedRowModel row = handler.getTableModel().createNewRow();
                // Initialize the row
                row.setSamplingOperation(samplingOperation);
                row.setAnalyst(analyst);
                // Affect individual pmfms from parent model
                row.setIndividualPmfms(new ArrayList<>(handler.getModel().getPmfms()));
                // Create empty measurements
                DaliBeans.createEmptyMeasurements(row);
                // Get the measurements
                for (Map.Entry<Integer, Integer> entry : combination) {
                    MeasurementDTO measurement = DaliBeans.getIndividualMeasurementByPmfmId(row, entry.getKey());
                    if (measurement == null) {
                        throw new DaliTechnicalException("No measurement found with id=" + entry.getKey());
                    }
                    // Set its qualitative value
                    measurement.setQualitativeValue(
                        DaliBeans.findById(measurement.getPmfm().getQualitativeValues(), entry.getValue())
                    );
                }

                // Add the line
                row.setValid(true);
                row.setInitialized(true); // Set this flag to prevent saving if not modified (Mantis #52658)
                rows.add(row);

            }
            samplingOperation.setDirty(true);
        }

        return null;
    }

    /**
     * Try to find in rows if the combination is already present
     *
     * @param rows        the rows to check
     * @param combination the combination to find
     * @return true if the combination is found
     */
    private boolean isPresent(Collection<OperationMeasurementsGroupedRowModel> rows, Combination combination) {
        return rows.stream().anyMatch(row -> findCombination(row, combination));
    }

    /**
     * Check if the row already have this combination in its measurements
     *
     * @param row         the row the check
     * @param combination the combination to find
     * @return true if the combination is found
     */
    private boolean findCombination(OperationMeasurementsGroupedRowModel row, Combination combination) {
        for (Map.Entry<Integer, Integer> entry : combination) {
            MeasurementDTO measurement = DaliBeans.getIndividualMeasurementByPmfmId(row, entry.getKey());
            if (measurement == null || measurement.getQualitativeValue() == null || !measurement.getQualitativeValue().getId().equals(entry.getValue()))
                // if this entry (pmfmId/qualitativeValuId) is not present in row's measurements, stop here
                return false;
        }
        return true;
    }

    @Override
    protected void done() {

        try {
            Throwable resultException = null;
            try {
                // get the result object
                Object result = get();
                // it can be an exception
                if (result instanceof Throwable) {
                    resultException = (Throwable) result;
                }
            } catch (InterruptedException | ExecutionException e) {
                resultException = e;
            }

            // handle the exception
            if (resultException != null) {
                if (Exceptions.hasCause(resultException, MaxCombinationExceededException.class)) {
                    // show the MaxCombinationExceededException message
                    handler.getContext().getDialogHelper().showErrorDialog(
                        I18n.t("dali.samplingOperation.measurement.grouped.init.maxCombinationsExceeded", handler.getConfig().getGridInitializationMaxCombinationCount()),
                        t("dali.samplingOperation.measurement.grouped.init.title"));
                } else if (Exceptions.hasCause(resultException, DaliBusinessException.class)) {
                    // show the business message
                    handler.getContext().getDialogHelper().showErrorDialog(
                        Exceptions.getCause(resultException, DaliBusinessException.class).getMessage(),
                        t("dali.samplingOperation.measurement.grouped.init.title"));
                } else {
                    // show complete exception
                    LOG.error(resultException.getMessage(), resultException);
                    handler.getContext().getDialogHelper().showErrorDialog(
                        resultException.getMessage(),
                        t("dali.samplingOperation.measurement.grouped.init.title"));
                }
                return;
            }

            if (LOG.isDebugEnabled()) LOG.debug("nb rows to add : " + rows.size());

            // show message if no tuple generated
            if (CollectionUtils.isEmpty(rows)) {
                handler.getContext().getDialogHelper().showWarningDialog(
                    t("dali.samplingOperation.measurement.grouped.init.empty"),
                    t("dali.samplingOperation.measurement.grouped.init.title"));
            } else {

                // add rows
                handler.getModel().addRows(rows);
                handler.getModel().setModify(true);
            }

        } finally {
            handler.resetCellEditors();
            handler.getModel().setLoading(false);
        }
    }
}
