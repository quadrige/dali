package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.menu.ManageFractionsMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.editor.AssociatedMatricesCellEditor;
import fr.ifremer.dali.ui.swing.util.table.renderer.AssociatedSupportsCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Fractions au niveau national
 */
public class ManageFractionsNationalUIHandler extends
        AbstractDaliTableUIHandler<FractionsTableRowModel, ManageFractionsNationalUIModel, ManageFractionsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageFractionsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageFractionsNationalUIModel model = new ManageFractionsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(ManageFractionsNationalUI ui) {
        initUI(ui);

        ManageFractionsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();

        menuUIModel.addPropertyChangeListener(ManageFractionsMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((Collection<FractionDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(FractionsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        final TableColumnExt descriptionCol = addColumn(FractionsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(FractionsTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(FractionsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(FractionsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                FractionsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // associated matrices
        final TableColumnExt associatedSupportsCol = addColumn(
                new AssociatedMatricesCellEditor(getTable(), getUI()),
                new AssociatedSupportsCellRenderer(),
                FractionsTableModel.ASSOCIATED_SUPPORTS);
        associatedSupportsCol.setSortable(true);

        FractionsTableModel tableModel = new FractionsTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        associatedSupportsCol.setVisible(false);

        addExportToCSVAction(t("dali.property.pmfm.fraction"), FractionsTableModel.ASSOCIATED_SUPPORTS);

        // Initialisation du tableau
        initTable(table, true);

        // Les colonnes optionnelles sont invisibles
        descriptionCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<FractionsTableRowModel> getTableModel() {
        return (FractionsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageFractionsNationalTable();
    }
}
