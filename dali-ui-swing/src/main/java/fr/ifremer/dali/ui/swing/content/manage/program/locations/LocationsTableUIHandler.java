package fr.ifremer.dali.ui.swing.content.manage.program.locations;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.dali.ui.swing.content.manage.program.locations.updatePeriod.UpdatePeriodUI;
import fr.ifremer.dali.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.dali.ui.swing.content.manage.program.strategies.StrategiesTableRowModel;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.JOptionPane;
import javax.swing.SortOrder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Strategy locations .
 */
public class LocationsTableUIHandler extends AbstractDaliTableUIHandler<LocationsTableRowModel, LocationsTableUIModel, LocationsTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(LocationsTableUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<LocationsTableRowModel> getTableModel() {
        return (LocationsTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getLocationsTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final LocationsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final LocationsTableUIModel model = new LocationsTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(LocationsTableUI ui) {

        initUI(ui);

        // Initialize table without dates and departments columns
        getModel().setPeriodsEnabled(false);
        initTable();
        SwingUtil.setLayerUI(ui.getTableScrollPane(), ui.getTableBlockLayer());

        initListeners();

        initActionComboBox(getUI().getEditCombobox());

    }

    /**
     * Load all program's monitoring location
     *
     * @param selectedProgram a {@link fr.ifremer.dali.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public void loadMonitoringLocationsFromProgram(ProgramsTableRowModel selectedProgram) {

        // Initialize table without dates and departments columns
        if (getModel().isPeriodsEnabled()) {
            getModel().setPeriodsEnabled(false);
            initTable();
        }

        getModel().setSelectedProgram(selectedProgram);
        getModel().setSelectedStrategy(null);

        // Load locations
        load();
    }

    /**
     * Load all applied periods of the selected strategy
     *
     * @param selectedProgram  a {@link fr.ifremer.dali.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     * @param selectedStrategy a {@link fr.ifremer.dali.ui.swing.content.manage.program.strategies.StrategiesTableRowModel} object.
     */
    public void loadAppliedStrategiesFromStrategy(ProgramsTableRowModel selectedProgram, StrategiesTableRowModel selectedStrategy) {

        // Initialize table with dates and departments columns
        if (!getModel().isPeriodsEnabled()) {
            getModel().setPeriodsEnabled(true);
            initTable();
        }

        getModel().setSelectedProgram(selectedProgram);
        getModel().setSelectedStrategy(selectedStrategy);

        // Load appliedPeriods
        load();
    }

    private void load() {
        try {
            if (getModel().getSelectedProgram() == null) {
                return;
            }

            getModel().setEditable(getModel().getSelectedProgram().isEditable());

            if (getModel().getSelectedStrategy() == null) {

                // add all locations
                getModel().setBeans(DaliBeans.locationsToAppliedStrategyDTOs(getModel().getSelectedProgram().getLocations()));

            } else {

                if (getUI().getStrategyFilterCheckbox().isSelected()) {

                    // add filtered locations
                    getModel().setBeans(DaliBeans.filterNotEmptyAppliedPeriod(getModel().getSelectedStrategy().getAppliedStrategies()));

                } else {

                    // add all locations
                    getModel().setBeans(getModel().getSelectedStrategy().getAppliedStrategies());
                }
            }

            if (getContext().getSelectedLocationId() != null) {
                selectRowById(getContext().getSelectedLocationId());
            }

            recomputeRowsValidState();
            getModel().setLoaded(true);

        } finally {
            getModel().setLoading(false);
        }
    }

    /**
     * <p>addLocations.</p>
     */
    void addLocations() {

        ProgramsTableRowModel program = getModel().getSelectedProgram(); // getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow();
        StrategiesTableRowModel strategy = getModel().getSelectedStrategy(); // getProgramsUI().getStrategiesTableUI().getModel().getSingleSelectedRow();

        Assert.notNull(program);

        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.LOCATION.getFilterTypeId());
        dialog.setTitle(t("dali.program.location.new.dialog.title"));
        List<LocationDTO> locations = program.getLocations().stream()
                .sorted(getDecorator(LocationDTO.class, null).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(location -> location.setReadOnly(true))
                .collect(Collectors.toList());
        dialog.getModel().setSelectedElements(locations);

        // filter active location (for better performance on the first load without filter)
//        if (dialog.getFilterElementPanel().getComponentCount() == 1 && dialog.getFilterElementPanel().getComponent(0) instanceof FilterElementLocationUI) {
//            StatusDTO enableStatus = QuadrigeBeanFactory.newStatusDTO();
//            enableStatus.setCode(StatusCode.ENABLE.getValue());
//            ((FilterElementLocationUI) dialog.getFilterElementPanel().getComponent(0)).getHandler().getReferentialMenuUI().getModel().setStatus(enableStatus);
//        }

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            // get new location only
            List<LocationDTO> newLocations = dialog.getModel().getSelectedElements().stream()
                    .map(newLocation -> ((LocationDTO) newLocation))
                    .filter(newLocation -> program.getLocations().stream().noneMatch(location -> location.equals(newLocation)))
                    .collect(Collectors.toList());

            if (!newLocations.isEmpty()) {

                // affect to program
                newLocations.forEach(program::addLocations);

                // affect to strategy if selected
                if (strategy != null) {
                    newLocations.stream()
                            // filter by precaution but it should be absent too
                            .filter(newLocation -> strategy.getAppliedStrategies().stream().noneMatch(appliedStrategy -> appliedStrategy.getId().equals(newLocation.getId())))
                            .forEach(newLocation -> strategy.addAppliedStrategies(DaliBeans.locationToAppliedStrategyDTO(newLocation)));
                }

                // re-affect lines
                List<AppliedStrategyDTO> beans = strategy != null ? strategy.getAppliedStrategies() : DaliBeans.locationsToAppliedStrategyDTOs(program.getLocations());
                getModel().setBeans(beans);

                saveToStrategy();

                // select new lines
                unselectAllRows();
                List<Integer> newLocIds = DaliBeans.collectIds(newLocations);
                selectRows(getModel().getRows().stream().filter(row -> newLocIds.contains(row.getId())).collect(Collectors.toList()));

            }
        }
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(LocationsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            // save selected location id
            getContext().setSelectedLocationId(getModel().getSingleSelectedRow() == null ? null : getModel().getSingleSelectedRow().getId());

        });

        // Listener sur la checkbox
        getUI().getStrategyFilterCheckbox().addItemListener(e -> {

            // Filter locations on strategy
            load();
        });

        getModel().addPropertyChangeListener(LocationsTableUIModel.EVENT_VALIDATE_ROWS, evt -> recomputeRowsValidState());

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // empty previous rows
        getModel().setBeans(null);

        // Force a new column to avoid column duplication (Mantis #47426)
        getTable().setColumnModel(new SwingTableColumnModel());

        // Disabled highlighter
        Highlighter disabledHighlighter = new AbstractHighlighter() {

            @Override
            protected boolean canHighlight(Component component, ComponentAdapter adapter) {
                int modelRow = adapter.convertRowIndexToModel(adapter.row);
                LocationsTableRowModel row = getTableModel().getEntry(modelRow);
                return DaliBeans.isDisabledReferential(row);
            }

            @Override
            protected Component doHighlight(Component component, ComponentAdapter adapter) {
                component.setForeground(Color.ORANGE.darker());
                component.setFont(component.getFont().deriveFont(Font.ITALIC));
                return component;
            }
        };

        // Column code
        final TableColumnExt colCode = addColumn(LocationsTableModel.CODE);
        colCode.setSortable(true);
        colCode.setCellRenderer(newNumberCellRenderer(0));
        colCode.setHighlighters(disabledHighlighter);

        // Column name
        final TableColumnExt colName = addColumn(LocationsTableModel.LABEL);
        colName.setSortable(true);
        colName.setHighlighters(disabledHighlighter);

        // Column comment
        final TableColumnExt colComment = addColumn(LocationsTableModel.NAME);
        colComment.setSortable(true);
        colComment.setHighlighters(disabledHighlighter);

        // Show following columns if a strategy is selected
        if (getModel().isPeriodsEnabled()) {

            // Column date debut
            final TableColumnExt colStartDate = addLocalDatePickerColumnToModel(
                    LocationsTableModel.START_DATE, getConfig().getDateFormat());
            colStartDate.setSortable(true);
            colStartDate.setHideable(false);

            // Column date fin
            final TableColumnExt colEndDate = addLocalDatePickerColumnToModel(
                    LocationsTableModel.END_DATE, getConfig().getDateFormat());
            colEndDate.setSortable(true);
            colEndDate.setHideable(false);

            // Column sampling department
            final TableColumnExt colSamplingDepartment = addFilterableComboDataColumnToModel(
                    LocationsTableModel.SAMPLING_DEPARTMENT,
                    getContext().getReferentialService().getDepartments(StatusFilter.ACTIVE),
                    false);
            colSamplingDepartment.setSortable(true);
            colSamplingDepartment.setHideable(false);

            // Column analysis department
            final TableColumnExt colAnalysisDepartment = addFilterableComboDataColumnToModel(
                    LocationsTableModel.ANALYSIS_DEPARTMENT,
                    getContext().getReferentialService().getDepartments(StatusFilter.ACTIVE),
                    false);
            colAnalysisDepartment.setSortable(true);
            colAnalysisDepartment.setHideable(false);
        }

        final LocationsTableModel tableModel = new LocationsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        tableModel.setNoneEditableCols();

        colCode.setHideable(false);
        colName.setHideable(false);
        colComment.setHideable(false);

        colCode.setEditable(false);
        colName.setEditable(false);
        colComment.setEditable(false);

        initTable(getTable());

        getTable().setVisibleRowCount(4);

        getTable().setSortOrder(LocationsTableModel.CODE, SortOrder.ASCENDING);

        if (getModel().isPeriodsEnabled()) {
            overrideTabKeyActions(getTable(), true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{LocationsTableRowModel.PROPERTY_ERRORS};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(LocationsTableRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && isDatesValid(row) && isAnalystValid(row);
    }

    private boolean isAnalystValid(LocationsTableRowModel row) {

        // error if an analyst is set without pmfm strategies (Mantis #44055)
        if (getModel().getSelectedStrategy() != null && getModel().getSelectedStrategy().isPmfmStrategiesEmpty() && row.getAnalysisDepartment() != null) {
            DaliBeans.addError(row, t("dali.program.location.analysisDepartment.noPmfmStrategy"), LocationsTableRowModel.PROPERTY_ANALYSIS_DEPARTMENT);
            return false;
        }

        return true;
    }

    private boolean isDatesValid(LocationsTableRowModel row) {

        // check first if start or end date has been cleared (Mantis #43965)
        if (row.getStartDate() == null && row.getPreviousStartDate() != null
                && row.getEndDate() == null && row.getPreviousEndDate() != null) {
            checkExistingSurveysInsideStrategy(row);
        }

        // both start and end dates must be null or not null
        if (row.getStartDate() == null && row.getEndDate() != null) {
            DaliBeans.addError(row, t("dali.program.location.startDate.null"), LocationsTableRowModel.PROPERTY_START_DATE);

        } else if (row.getStartDate() != null && row.getEndDate() == null) {
            DaliBeans.addError(row, t("dali.program.location.endDate.null"), LocationsTableRowModel.PROPERTY_END_DATE);

        } else if (row.getStartDate() != null && row.getEndDate() != null) {
            // end date must be after start date
            if (row.getStartDate().isAfter(row.getEndDate())) {
                DaliBeans.addError(row, t("dali.program.location.endDate.before"), LocationsTableRowModel.PROPERTY_END_DATE);
            } else {

                // if dates valid, check surveys outside dates
                checkExistingSurveysOutsideStrategy(row);
            }
        }

        return row.getErrors().isEmpty();
    }

    private void checkExistingSurveysInsideStrategy(LocationsTableRowModel row) {
        if (row.getAppliedStrategyId() != null && row.getId() != null) {
            long surveysCount = getContext().getObservationService().countSurveysWithProgramLocationAndInsideDates(
                    getModel().getSelectedProgram().getCode(),
                    row.getAppliedStrategyId(),
                    row.getId(), // = location id
                    row.getPreviousStartDate(),
                    row.getPreviousEndDate()
            );
            if (surveysCount > 0) {
                DaliBeans.addError(row, t("dali.program.location.period.surveyInsidePeriod"),
                        LocationsTableRowModel.PROPERTY_START_DATE, LocationsTableRowModel.PROPERTY_END_DATE);
            }
        }
    }

    private void checkExistingSurveysOutsideStrategy(LocationsTableRowModel row) {
        if (row.getAppliedStrategyId() != null && row.getId() != null) {
            long surveysCount = getContext().getObservationService().countSurveysWithProgramLocationAndOutsideDates(
                    getModel().getSelectedProgram().getCode(),
                    row.getAppliedStrategyId(),
                    row.getId(), // = location id
                    row.getStartDate(),
                    row.getEndDate()
            );
            if (surveysCount > 0) {
                DaliBeans.addError(row, t("dali.program.location.period.surveyOutsidePeriod"),
                        LocationsTableRowModel.PROPERTY_START_DATE, LocationsTableRowModel.PROPERTY_END_DATE);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, LocationsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // Remove departments if no dates (Mantis #43233)
        if (getModel().isPeriodsEnabled()) {
            if (LocationsTableRowModel.PROPERTY_START_DATE.equals(propertyName) || LocationsTableRowModel.PROPERTY_END_DATE.equals(propertyName)) {
                if (row.getStartDate() == null && row.getEndDate() == null) {
                    if (row.getSamplingDepartment() != null) row.setSamplingDepartment(null);
                    if (row.getAnalysisDepartment() != null) row.setAnalysisDepartment(null);
                }
            }
        }

        saveToStrategy();
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
    }

    /**
     * <p>saveToStrategy.</p>
     */
    private void saveToStrategy() {

        if (getModel().isLoading()) return;

        // programs contains strategies, strategies contains pmfm
        getModel().getParentModel().getStrategiesUIModel().fireSaveAppliedStrategies();
        getModel().getParentModel().getProgramsUIModel().fireSaveStrategies();

        // force model modify and valid
        recomputeRowsValidState();
        getModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_MODIFY, null, true);
        getModel().firePropertyChanged(AbstractDaliBeanUIModel.PROPERTY_VALID, null, getModel().getRowCount() > 0);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void recomputeRowsValidState() {
        super.recomputeRowsValidState();

        // recompute strategy valid state
        getModel().getParentModel().getStrategiesUIModel().fireValidateRows();
    }

    /**
     * <p>removeLocations.</p>
     */
    void removeLocations() {

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("No location selected");
            return;
        }

        // the selected program
        ProgramsTableRowModel program = getModel().getSelectedProgram();
        List<Integer> locationIds = Lists.newArrayList();
        for (final AppliedStrategyDTO row : getModel().getSelectedRows()) {
            // list of location ids to delete
            locationIds.add(row.getId());
        }

        // check delete is for program locations or applied strategies
        if (getModel().isPeriodsEnabled()) {

            // selected rows are applied strategies (with applied period)
            if (!askBeforeDelete(t("dali.program.location.delete.titre"), t("dali.program.location.delete.appliedStrategy.message"))) {
                return;
            }

        } else {

            // check usage of location and program in surveys
            long surveysCount = getContext().getObservationService().countSurveysWithProgramAndLocations(program.getCode(), locationIds);
            if (surveysCount > 0) {
                getContext().getDialogHelper().showErrorDialog(
                        surveysCount == 1
                                ? t("dali.program.location.delete.usedInData")
                                : t("dali.program.location.delete.usedInData.many", surveysCount),
                        t("dali.program.location.delete.titre"));
                return;
            }

            // selected rows are program location
            if (!askBeforeDelete(t("dali.program.location.delete.titre"), t("dali.program.location.delete.location.message"))) {
                return;
            }

            // check usage of locations in strategies
            Set<ProgStratDTO> usedStrategies = Sets.newHashSet();
            for (Integer locationId : locationIds) {
                List<ProgStratDTO> appliedStrategies = getContext().getProgramStrategyService().getStrategyUsageByProgramAndLocationId(program.getCode(), locationId);
                usedStrategies.addAll(appliedStrategies);
            }
            if (!usedStrategies.isEmpty()) {
                List<String> locationNames = Lists.newArrayList();
                for (AppliedStrategyDTO row : getModel().getSelectedRows()) {
                    locationNames.add(decorate(row));
                }
                List<String> strategyNames = DaliBeans.collectProperties(usedStrategies, ProgStratDTO.PROPERTY_NAME);

                String topPart = t("dali.program.location.delete.location.formStrategy");
                String bottomMessage = t("dali.program.location.delete.location.fromStrategy.list", ApplicationUIUtil.formatHtmlList(strategyNames));

                if (locationNames.size() > 8) {

                    if (getContext().getDialogHelper().showConfirmDialog(topPart, ApplicationUIUtil.getHtmlString(locationNames), bottomMessage, JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        return;
                    }

                } else {

                    String message = String.format("%s\n%s\n%s", topPart, ApplicationUIUtil.formatHtmlList(locationNames), bottomMessage);
                    if (getContext().getDialogHelper().showConfirmDialog(message, JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        return;
                    }
                }

            }
        }

        // remove rows
        getModel().deleteSelectedRows();

        if (!getModel().isPeriodsEnabled()) {
            // remove from program model
            removeFromModels(locationIds);
        }

        saveToStrategy();

    }

    /**
     * <p>removeFromModels.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     */
    private void removeFromModels(List<Integer> locationIds) {

        // fix list of locations in program model
        getModel().getParentModel().getProgramsUIModel().fireRemoveLocations(locationIds);

        // fix applied strategies lists in existing strategy models
        getModel().getParentModel().getStrategiesUIModel().fireRemoveLocations(locationIds);
    }

    /**
     * <p>updatePeriod.</p>
     */
    void updatePeriod() {

        final UpdatePeriodUI dialogue = new UpdatePeriodUI(getContext());
        dialogue.getModel().setTableModel(getModel());

        openDialog(dialogue, new Dimension(400, 220));

        getModel().setModify(true);
        saveToStrategy();
        getTable().repaint();
    }

}
