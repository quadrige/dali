package fr.ifremer.dali.ui.swing.util.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.ErrorAware;
import fr.ifremer.dali.dto.ErrorDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.DaliUIContext;
import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.dali.ui.swing.util.table.export.ExportToCSVAction;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.HiddenColumn;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.action.AdditionalTableActions;
import fr.ifremer.quadrige3.ui.swing.table.renderer.ColorCheckBoxRenderer;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.util.PaintUtils;
import org.nuiton.jaxx.application.swing.util.ApplicationColorHighlighter;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>Abstract AbstractDaliTableUIHandler class.</p>
 *
 * @param <R>  type of a row
 * @param <M>  type of the ui model
 * @param <UI> type of the ui
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public abstract class AbstractDaliTableUIHandler<R extends AbstractDaliRowUIModel<?, ?>, M extends AbstractDaliTableUIModel<?, R, M>, UI extends DaliUI<M, ?>>
        extends AbstractTableUIHandler<R, M, UI> {

    // initial border of full screen toggle button
    private Border initialToggleButtonBorder;

    public AbstractDaliTableUIHandler(String... properties) {
        super(properties);
    }

    @Override
    protected void initTable(SwingTable table, boolean singleSelection, boolean checkBoxSelection) {

        checkBoxSelection |= getConfig().isShowTableCheckbox();
        // Assume a single selection mode is over a checkbox selection
        if (singleSelection && checkBoxSelection) {
            checkBoxSelection = false;
        }

        // Main Init
        super.initTable(table, singleSelection, checkBoxSelection);

        addErrorHighlighters(table);
        if (getFixedTable() != null) {
            addErrorHighlighters(getFixedTable());
        }

        // Add default renderer if not already exists
        table.setDefaultRenderer(QualitativeValueDTO.class, newTableCellRender(QualitativeValueDTO.class));

    }

    @Override
    public DaliUIContext getContext() {
        return (DaliUIContext) super.getContext();
    }

    @Override
    public DaliConfiguration getConfig() {
        return (DaliConfiguration) super.getConfig();
    }

    @Override
    protected void addHighlighters(JXTable table) {
        /* PREDICATES */
        HighlightPredicate notSelectedPredicate = new HighlightPredicate.NotHighlightPredicate(HighlightPredicate.IS_SELECTED);
        HighlightPredicate invalidPredicate = (renderer, adapter) -> {
            boolean result = false;
            if (adapter.row >= 0 && adapter.row < table.getRowCount()) {
                int modelRow = adapter.convertRowIndexToModel(adapter.row);
                R row = getTableModel().getEntry(modelRow);
                if (row != null) {
                    result = !row.isValid();
                }
            }
            return result;
        };
        HighlightPredicate selectedPredicate = HighlightPredicate.IS_SELECTED;
        HighlightPredicate editablePredicate = HighlightPredicate.EDITABLE;
        HighlightPredicate readOnlyPredicate = HighlightPredicate.READ_ONLY;
        HighlightPredicate validPredicate = new HighlightPredicate.NotHighlightPredicate(invalidPredicate);
        HighlightPredicate readOnlySelectedPredicate = new HighlightPredicate.AndHighlightPredicate(selectedPredicate, readOnlyPredicate);
        HighlightPredicate invalidSelectedPredicate = new HighlightPredicate.AndHighlightPredicate(selectedPredicate, invalidPredicate);
        HighlightPredicate notColorizedColumnPredicate = (renderer, adapter) -> {

            if (adapter.getComponent() instanceof JXTable) {
                JXTable table1 = (JXTable) adapter.getComponent();
                TableColumnExt column = table1.getColumnExt(adapter.column);
                return !(column.getCellRenderer() instanceof ColorCheckBoxRenderer);
            }

            return true;
        };

        // predicate for calculated row
        HighlightPredicate calculatedPredicate = (renderer, adapter) -> {
            int modelRow = adapter.convertRowIndexToModel(adapter.row);
            R row = getTableModel().getEntry(modelRow);
            return row.isCalculated();
        };

        /* COLORS */
        Color selectedColor = getConfig().getColorSelectedRow();
        Color oddColor = getConfig().getColorAlternateRow();
        Color readOnlyColor = getConfig().getColorRowReadOnly();
        Color readOnlySelectedColor = PaintUtils.interpolate(readOnlyColor, selectedColor, 0.25f);
        Color readOnlyOddColor = PaintUtils.interpolate(readOnlyColor, oddColor, 0.75f);
        Color invalidColor = getConfig().getColorRowInvalid();
        Color invalidSelectedColor = PaintUtils.interpolate(invalidColor, selectedColor, 0.25f);
        Color invalidOddColor = PaintUtils.interpolate(oddColor, invalidColor, 0.25f);

        /* NORMAL ODD ROW */
        table.addHighlighter(new ApplicationColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.ODD,
                        notSelectedPredicate,
                        validPredicate,
                        notColorizedColumnPredicate,
                        editablePredicate),
                oddColor, false));

        /* READ ONLY ROW */
        table.addHighlighter(new ApplicationColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        readOnlyPredicate,
                        notSelectedPredicate,
                        notColorizedColumnPredicate,
                        validPredicate),
                readOnlyColor, false));

        table.addHighlighter(new ApplicationColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.ODD,
                        readOnlyPredicate,
                        notSelectedPredicate,
                        notColorizedColumnPredicate,
                        validPredicate),
                readOnlyOddColor, false));

        /* INVALID ROW */
        table.addHighlighter(new ApplicationColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        notSelectedPredicate,
                        notColorizedColumnPredicate,
                        invalidPredicate),
                invalidColor, false));

        table.addHighlighter(new ApplicationColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.ODD,
                        notSelectedPredicate,
                        notColorizedColumnPredicate,
                        invalidPredicate),
                invalidOddColor, false));

        /* SELECTED ROW */
        table.addHighlighter(new ApplicationColorHighlighter(
                selectedPredicate,
                selectedColor, false));
        table.addHighlighter(new ApplicationColorHighlighter(
                readOnlySelectedPredicate,
                readOnlySelectedColor, false));
        table.addHighlighter(new ApplicationColorHighlighter(
                invalidSelectedPredicate,
                invalidSelectedColor, false));

        // paint in a special font selected rows
        table.addHighlighter(new BoldFontHighlighter(HighlightPredicate.IS_SELECTED));

        /* CALCULATED ROW */
        Highlighter calculatedHighlighter = ApplicationUIUtil.newForegroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(notSelectedPredicate, calculatedPredicate),
                getConfig().getColorComputedRow());
        table.addHighlighter(calculatedHighlighter);

    }

    private void addErrorHighlighters(SwingTable table) {

        // add error highlighter
        table.addHighlighter(new ErrorHighlighter(table, BorderFactory.createDashedBorder(Color.RED, 1.5f, 4, 4, false), t("dali.table.cell.error")) {
            @Override
            public List<String> getMessages(ErrorAware bean, String propertyName, Integer pmfmId) {
                return DaliBeans.getErrorMessages(bean, propertyName, pmfmId).stream().distinct().collect(Collectors.toList());
            }
        });

        // add warning highlighter
        table.addHighlighter(new ErrorHighlighter(table, BorderFactory.createDashedBorder(Color.ORANGE, 1.5f, 4, 4, false), t("dali.table.cell.warning")) {
            @Override
            public List<String> getMessages(ErrorAware bean, String propertyName, Integer pmfmId) {
                return DaliBeans.getWarningMessages(bean, propertyName, pmfmId).stream().distinct().collect(Collectors.toList());
            }
        });

    }

    @Override
    protected boolean isRowValid(R row) {

        boolean valid = super.isRowValid(row);
        if (row instanceof ErrorAware) {
            ErrorAware errorAwareRow = (ErrorAware) row;
            DaliBeans.removeBlockingErrors(errorAwareRow);

            if (!valid) {
                row.getInvalidMandatoryIdentifiers().forEach(identifier -> {
                    if (identifier instanceof DaliPmfmColumnIdentifier) {
                        DaliPmfmColumnIdentifier pmfmIdentifier = (DaliPmfmColumnIdentifier) identifier;
                        DaliBeans.addError(errorAwareRow, t("dali.validator.error.field.empty"), pmfmIdentifier.getPmfmId(), pmfmIdentifier.getPropertyName());
                    } else {
                        DaliBeans.addError(errorAwareRow, t("dali.validator.error.field.empty"), identifier.getPropertyName());
                    }
                });
            }
        }
        return valid;
    }

    public void ensureColumnsWithErrorAreVisible(Collection<? extends ErrorAware> beans) {
        if (CollectionUtils.isNotEmpty(beans)) {
            // Get all columns
            List<TableColumnExt> columns = getTable().getColumns(true).stream()
                .filter(Objects::nonNull).filter(tableColumn -> !(tableColumn instanceof HiddenColumn))
                .map(TableColumnExt.class::cast).collect(Collectors.toList());
            // Build map of columns indexed by property name
            Map<String, TableColumnExt> map = Maps.uniqueIndex(columns, column -> {
                Assert.notNull(column);
                if (column.getIdentifier() instanceof DaliPmfmColumnIdentifier) {
                    DaliPmfmColumnIdentifier pmfmColumnIdentifier = (DaliPmfmColumnIdentifier) column.getIdentifier();
                    return String.format("%s_%s", pmfmColumnIdentifier.getPropertyName(), pmfmColumnIdentifier.getPmfmId());
                } else {
                    return ((ColumnIdentifier) column.getIdentifier()).getPropertyName();
                }
            });
            for (ErrorAware bean : beans) {
                Collection<ErrorDTO> allErrors = CollectionUtils.union(DaliBeans.getErrors(bean, null), DaliBeans.getWarnings(bean, null));
                for (ErrorDTO error : allErrors) {
                    for (String propertyName : error.getPropertyName()) {
                        if (map.containsKey(propertyName))
                            map.get(propertyName).setVisible(true);
                    }
                }
            }
        }
    }

    public void ensureColumnsWithErrorAreVisible(ErrorAware bean) {
        if (bean != null) {
            ensureColumnsWithErrorAreVisible(ImmutableList.of(bean));
        }
    }

    public void addEditionPanelBorder() {
        addEditionPanelBorder(JScrollPane.class);
    }

    public void addEditionPanelBorder(Class<?> ancestorClass) {
        Container container = SwingUtilities.getAncestorOfClass(ancestorClass, getTable());
        if (container instanceof JComponent) {
            ((JComponent) container).setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorEditionPanelBorder()));
        }
    }

    /**
     * <p>addAttachementHighlighter.</p>
     *
     * @param table      a {@link SwingTable} object.
     * @param identifier a {@link DaliColumnIdentifier} object.
     */
    protected void addAttachementHighlighter(SwingTable table, DaliColumnIdentifier<?> identifier) {
        Color cellWithValueColor = getConfig().getColorCellWithValue();

        Highlighter attachmentHighlighter = ApplicationUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.IdentifierHighlightPredicate(identifier),
                        // for not null value
                        (renderer, adapter) -> {
                            Collection attachments = (Collection) adapter.getValue();
                            return CollectionUtils.isNotEmpty(attachments);
                        }
                ), cellWithValueColor);
        table.addHighlighter(attachmentHighlighter);
    }

    /**
     * Select a row by its ID
     *
     * @param rowId the row id to select
     */
    public void selectRowById(int rowId) {

        // Selection de la ligne du tableau
        for (final R row : getModel().getRows()) {
            if (row.getId() == rowId) {
                selectRow(row);
                break;
            }
        }
    }

    /**
     * Ajouter des colonnes dynamiques en utilisant le decorateur de PMFM par défaut.
     *
     * @param pmfms        La liste des psfm
     * @param propertyName Le nom de la propriete des Psfms
     */
    protected void addPmfmColumns(Collection<PmfmDTO> pmfms, String propertyName) {

        addPmfmColumns(pmfms, propertyName, null, null);

    }

    /**
     * <p>addPmfmColumns.</p>
     *
     * @param pmfms         a {@link java.util.Collection} object.
     * @param propertyName  a {@link java.lang.String} object.
     * @param decoratorName a {@link java.lang.String} object.
     */
    protected void addPmfmColumns(Collection<PmfmDTO> pmfms, String propertyName, String decoratorName) {

        addPmfmColumns(pmfms, propertyName, decoratorName, null);

    }

    /**
     * Ajouter des colonnes dynamiques en utilisant le decorateur de PMFM par défaut.
     *
     * @param pmfms          La liste des psfm
     * @param propertyName   Le nom de la propriete des Psfms
     * @param insertPosition a {@link DaliColumnIdentifier} object.
     */
    protected void addPmfmColumns(Collection<PmfmDTO> pmfms, String propertyName, DaliColumnIdentifier<R> insertPosition) {

        addPmfmColumns(pmfms, propertyName, null, insertPosition);

    }

    /**
     * Ajouter des colonnes dynamiques.
     *
     * @param pmfms                La liste des psfm
     * @param propertyName         Le nom de la propriete des Psfms
     * @param decoratorName        Le contexte du decorateur de PMFM
     * @param insertColumnPosition identifiant de la colonne après laquelle les colonnes dynamiques seront placées
     */
    @SuppressWarnings("unchecked")
    protected void addPmfmColumns(Collection<PmfmDTO> pmfms, String propertyName, String decoratorName, DaliColumnIdentifier<R> insertColumnPosition) {

        // First, remove old dynamic columns
        removePmfmColumns();

        if (CollectionUtils.isNotEmpty(pmfms)) {

            // before add the column, reset row sorter to prevent exceptions
            uninstallSortController();

            // compute insert position
            int insertPosition = -1;
            if (insertColumnPosition != null) {

                // find view index of the specified identifier
                TableColumnExt insertColumn = getTable().getColumnExt(insertColumnPosition);
                int modelIndex = insertColumn.getModelIndex();
                insertPosition = getTable().convertColumnIndexToView(modelIndex);
                // while the column is not visible, try to find a visible column in left direction
                while (insertPosition == -1 && modelIndex > 0) {
                    insertPosition = getTable().convertColumnIndexToView(--modelIndex);
                }

            }

            // pmfm index
            int index = 0;

            // create a column for each pmfm
            for (final PmfmDTO pmfm : pmfms) {

                // Create corresponding editor and renderer
                TableCellEditor editor;
                TableCellRenderer renderer; // default table render should be used

                // Define value type
                Class<?> typeClass;
                if (pmfm.getParameter().isQualitative()) {
                    typeClass = QualitativeValueDTO.class;
                    editor = newExtendedComboBoxCellEditor(new ArrayList<>(pmfm.getQualitativeValues()), QualitativeValueDTO.class, false);
                    renderer = newTableCellRender(QualitativeValueDTO.class);

                } else {
                    typeClass = BigDecimal.class;
                    // an arbitrary pattern for BigDecimal input
                    editor = newNumberCellEditor(BigDecimal.class, false, "\\d{0,10}(\\.\\d{0,10})?");
                    renderer = newBigDecimalRenderer();
                }

                // Column header
                String headerLabel = decorate(pmfm, decoratorName);
                String headerTip = decorate(pmfm);

                DaliPmfmColumnIdentifier identifier = DaliPmfmColumnIdentifier.newId(
                        propertyName,
                        pmfm,
                        headerLabel,
                        headerTip,
                        typeClass,
                        false);
                // mantis #25139 : all PMFM columns must not be mandatory

                // Create dynamic column
                final PmfmTableColumn column = addPmfmColumn(
                        editor,
                        renderer,
                        identifier);

                // Add new identifier
                getTableModel().getIdentifiers().add(identifier);

                // Move move if insertPosition is set
                if (insertPosition > -1) {
                    getTable().moveColumn(
                            getTable().convertColumnIndexToView(column.getModelIndex()),
                            insertPosition + 1 + index);
                }

                index++;
            }

            // recreate row sorter
            installSortController();

        }
    }

    protected void removePmfmColumns() {
        removeColumns(getModel().getPmfmColumns());
        // Reset pmfm columns in model to avoid duplication (Mantis #51850)
        getModel().getPmfmColumns().clear();
    }

    /**
     * <p>addPmfmColumn.</p>
     *
     * @param editor     a {@link javax.swing.table.TableCellEditor} object.
     * @param renderer   a {@link javax.swing.table.TableCellRenderer} object.
     * @param identifier a {@link DaliPmfmColumnIdentifier} object.
     * @return a {@link fr.ifremer.dali.ui.swing.util.table.PmfmTableColumn} object.
     */
    protected PmfmTableColumn addPmfmColumn(
            final TableCellEditor editor,
            final TableCellRenderer renderer,
            final DaliPmfmColumnIdentifier<R> identifier) {

        final PmfmTableColumn column = new PmfmTableColumn(getTable().getColumnModel().getColumnCount(true));

        column.setCellEditor(editor);
        column.setCellRenderer(renderer);

        // if the column is mandatory, override headerValue and toolTip
        if (identifier.isMandatory()) {
            column.setHeaderValue(t("dali.table.mandatoryColumn.header", identifier.getHeaderLabel()));
            column.setToolTipText(t("dali.table.mandatoryColumn.tip", identifier.getHeaderLabelTip()));
        } else {
            column.setHeaderValue(ApplicationUIUtil.getHtmlString(identifier.getHeaderLabel()));
            column.setToolTipText(ApplicationUIUtil.getHtmlString(identifier.getHeaderLabelTip()));
        }
        column.setIdentifier(identifier);
        column.setHideable(false);
        column.setSortable(true);
        // set default pmfm column min width (Mantis #52362)
        setDefaultColumnMinWidth(column);
        // Add the new column in UI model before add it to Table model
        getModel().addPmfmColumn(column);
        getTable().getColumnModel().addColumn(column);
        return column;
    }

    private class BoldFontHighlighter extends AbstractHighlighter {

        BoldFontHighlighter(HighlightPredicate predicate) {
            super(predicate);
        }

        @Override
        protected Component doHighlight(Component component, ComponentAdapter adapter) {
            if (component.getFont().isItalic()) {
                component.setFont(component.getFont().deriveFont(Font.BOLD + Font.ITALIC));
            } else {
                component.setFont(component.getFont().deriveFont(Font.BOLD));
            }
            return component;
        }
    }

    @SafeVarargs
    protected final void addExportToCSVAction(String tableName, DaliColumnIdentifier<R>... columnIdentifiersToIgnore) {
        ExportToCSVAction<M, UI, ?> exportAction = new ExportToCSVAction<M, UI, AbstractDaliTableUIHandler<?, M, UI>>(
                this,
                tableName,
                columnIdentifiersToIgnore
        );
        Action tableAction = getContext().getActionFactory().createUIAction(null, exportAction);
        tableAction.putValue(Action.NAME, exportAction.getActionDescription());
        Icon actionIcon = SwingUtil.createActionIcon("export");
        tableAction.putValue(Action.SMALL_ICON, actionIcon);
        tableAction.putValue(Action.LARGE_ICON_KEY, actionIcon);
        tableAction.putValue(AdditionalTableActions.ACTION_TARGET_GROUP, 1);
        getAdditionalTableActions().addAction(tableAction);
    }

    @Override
    public void toggleFullScreen(JPanel panel, JToggleButton toggleButton) {

        if (toggleButton.isSelected()) {
            // keep its initial border
            initialToggleButtonBorder = toggleButton.getBorder();
        }

        super.toggleFullScreen(panel, toggleButton);

        // Set highlight border when selected (Mantis #48586)
        toggleButton.setBorder(
            toggleButton.isSelected()
                ? BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), initialToggleButtonBorder)
                : initialToggleButtonBorder
        );
    }
}
