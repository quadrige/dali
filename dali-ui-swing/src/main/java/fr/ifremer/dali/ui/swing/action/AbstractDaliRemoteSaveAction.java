package fr.ifremer.dali.ui.swing.action;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.DaliUI;
import fr.ifremer.quadrige3.core.exception.BadUpdateDtException;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.exception.SaveForbiddenException;
import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.DialogHelper;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 04/07/2017.
 */
public abstract class AbstractDaliRemoteSaveAction<M extends AbstractBeanUIModel, UI extends DaliUI<M, ?>, H extends AbstractUIHandler<M, UI>>
        extends AbstractDaliSaveAction<M, UI, H> {

    private static final Log LOG = LogFactory.getLog(AbstractDaliRemoteSaveAction.class);

    private boolean saveAborted = false;

    protected abstract void doSave();

    /**
     * <p>Constructor for AbstractAction.</p>
     *
     * @param handler  a H object.
     * @param hideBody a boolean.
     */
    public AbstractDaliRemoteSaveAction(H handler, boolean hideBody) {
        super(handler, hideBody);
    }

    public boolean isSaveAborted() {
        return saveAborted;
    }

    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        // if the screen can be saved and the screen is valid
        if (!getModel().isModify()) {
            return false;
        }
        if (!getModel().isValid()) {
            displayErrorMessage(t("dali.action.save.errors.title"), t("dali.action.save.errors.remove"));
            return false;
        }
        return true;

    }

    @Override
    public void doAction() throws Exception {

        // Check server connection (Mantis #47532)
        try {
            getContext().getProgramStrategyService().getRemoteWritableProgramsByUser(getContext().getAuthenticationInfo());
        } catch (QuadrigeTechnicalException ex) {
            LOG.warn("Connection to synchronization server failed", ex);
            displayWarningMessage(t("dali.action.save.errors.title"), t("dali.error.synchro.serverUnavailable"));

            saveAborted = true;
            return;
        }

        // Perform save action and reload
        try {

            doSave();

            reload();

        } catch (Throwable throwable) {

            // If a concurrent save occurs, ask user to import referential
            if (Exceptions.hasCause(throwable, BadUpdateDtException.class)) {
                LOG.warn(throwable.getMessage(), throwable);
                boolean confirmImportReferential = getContext().getDialogHelper().showOptionDialog(null,
                        ApplicationUIUtil.getHtmlString(t("dali.action.common.import.referential.confirm")),
                        t("dali.action.save.errors.title"),
                        JOptionPane.ERROR_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("dali.common.import"),
                        t("dali.common.cancel")
                ) == JOptionPane.OK_OPTION;

                if (confirmImportReferential) {

                    // Import referential
                    doImportReferential();

                    // then reload
                    reload();

                } else {

                    // Abort save, don't reload
                    saveAborted = true;
                }

            } else if (Exceptions.hasCause(throwable, SaveForbiddenException.class)) {
                SaveForbiddenException exception = (SaveForbiddenException) Exceptions.getCause(throwable, SaveForbiddenException.class);

                onSaveForbiddenException(exception);

                saveAborted = true;

            } else
                // throw other exceptions
                throw throwable;

        }

    }

    protected void reload() {
        // nothing by default
    }

    protected void onSaveForbiddenException(SaveForbiddenException exception) {
        // nothing by default
    }

    @Override
    public void postSuccessAction() {
        if (isSaveAborted()) {
            // no reload if aborted
            return;
        }

        super.postSuccessAction();
    }

    @Override
    protected void releaseAction() {

        saveAborted = false;

        super.releaseAction();
    }
}
