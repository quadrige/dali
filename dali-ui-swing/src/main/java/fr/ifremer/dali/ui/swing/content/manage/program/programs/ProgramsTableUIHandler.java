package fr.ifremer.dali.ui.swing.content.manage.program.programs;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.dali.ui.swing.content.manage.program.programs.departments.DepartmentsDialogUI;
import fr.ifremer.dali.ui.swing.content.manage.program.programs.users.UsersDialogUI;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramPrivilegeIds;
import fr.ifremer.quadrige3.ui.swing.action.ActionFactory;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class ProgramsTableUIHandler extends AbstractDaliTableUIHandler<ProgramsTableRowModel, ProgramsTableUIModel, ProgramsTableUI> {

    private ThreadPoolExecutor executor;
    private boolean newRowSelected;

    /**
     * <p>Constructor for ProgramsTableUIHandler.</p>
     */
    public ProgramsTableUIHandler() {
        super(ProgramsTableRowModel.PROPERTY_NAME,
            ProgramsTableRowModel.PROPERTY_DESCRIPTION,
            ProgramsTableRowModel.PROPERTY_COMMENT,
            ProgramsTableRowModel.PROPERTY_CODE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{ProgramsTableRowModel.PROPERTY_STRATEGIES, ProgramsTableRowModel.PROPERTY_STRATEGIES_LOADED,
            ProgramsTableRowModel.PROPERTY_LOCATIONS, ProgramsTableRowModel.PROPERTY_LOCATIONS_LOADED,
            ProgramsTableRowModel.PROPERTY_ERRORS};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractDaliTableModel<ProgramsTableRowModel> getTableModel() {
        return (ProgramsTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getProgramsTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ProgramsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ProgramsTableUIModel model = new ProgramsTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(ProgramsTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initTable();

        // Ajout des listeners
        initListeners();

        // Initilisation du bouton editer
        getUI().getEditCombobox().setMaximumRowCount(10);
        initActionComboBox(getUI().getEditCombobox());

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

        executor = ActionFactory.createSingleThreadExecutor(ActionFactory.ExecutionMode.LATEST);

    }

    @Override
    public void onCloseUI() {
        executor.shutdownNow();
        super.onCloseUI();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRowValid(final ProgramsTableRowModel row) {
        row.getErrors().clear();

        return !row.isEditable() || (super.isRowValid(row) && isProgramValid(row));
    }

    private boolean isProgramValid(ProgramsTableRowModel row) {

        // check if a national program has at least one manager
        if (row.sizeManagerPersons() == 0) {
            DaliBeans.addError(row, t("dali.programs.validation.error.noManager"), ProgramsTableRowModel.PROPERTY_CODE);
        }

        // check if at least one location is set
        if (row.isLocationsLoaded() && row.isLocationsEmpty()) {
            DaliBeans.addError(row, t("dali.programs.validation.error.noLocationOnProg"), ProgramsTableRowModel.PROPERTY_CODE);
        }

        // check sub-models
        if (!row.isStrategiesValid() || !row.isLocationsValid()) {
            DaliBeans.addError(row, t("dali.program.tables.error"), ProgramsTableRowModel.PROPERTY_CODE);
        }

        return row.isErrorsEmpty();
    }

    /**
     * load programs in table
     *
     * @param programs programs to load
     */
    public void loadPrograms(Collection<ProgramDTO> programs) {

        getModel().setSingleSelectedRow(null);
        getModel().setBeans(programs);

        boolean isAdmin = getContext().isAuthenticatedAsAdmin();
        getModel().getRows().forEach(programsTableRowModel ->
        {
            // a program is editable if save is enabled
            programsTableRowModel.setEditable(getModel().isSaveEnabled());

            // determinate if current user is manager of the program or if he is a referential admin (Mantis #58673)
            programsTableRowModel.setCurrentUserIsManager(
                isAdmin ||
                    DaliBeans.isProgramManager(
                        programsTableRowModel,
                        getContext().getDataContext().getRecorderPersonId(),
                        getContext().getDataContext().getRecorderDepartmentId()
                    )
            );
        });

        recomputeRowsValidState();
        autoSelectRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, ProgramsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(ProgramsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            final ProgramsTableRowModel selectedProgram = getModel().getSingleSelectedRow();

            // Si un seul element a ete selectionne
            if (selectedProgram != null) {

                newRowSelected = true;

                // Suppression des informations du contexte
                getContext().setSelectedProgramCode(selectedProgram.getCode());

                getModel().getParentModel().getStrategiesUIModel().setLoading(true);
                getModel().getParentModel().getLocationsUIModel().setLoading(true);
                executor.execute(new StrategyAndLocationLoader(selectedProgram));

            }
        });

        // add a click listener on table to handle unselect strategy table
        getTable().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!newRowSelected) {
                    getModel().firePropertyChanged(ProgramsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, null, null);
                }
                newRowSelected = false;
            }

        });

        // Add listener on saveEnabled property to disable change (Mantis #47532)
        getModel().addPropertyChangeListener(ProgramsTableUIModel.PROPERTY_SAVE_ENABLED, evt -> {
            getModel().getRows().forEach(programsTableRowModel -> programsTableRowModel.setEditable(getModel().isSaveEnabled()));
            getUI().getProgramsTable().invalidate();
            getUI().getProgramsTable().repaint();
            autoSelectRow();
        });

        getModel().addPropertyChangeListener(ProgramsTableUIModel.EVENT_SAVE_STRATEGIES, evt -> {
            if (getModel().getSingleSelectedRow() != null) {
                getModel().getSingleSelectedRow().setStrategies(getModel().getParentModel().getStrategiesUIModel().getBeans());
                getModel().getSingleSelectedRow().setStrategiesLoaded(true);
            }
        });

        //noinspection unchecked
        getModel().addPropertyChangeListener(ProgramsTableUIModel.EVENT_REMOVE_LOCATIONS, evt -> removeLocations((List<Integer>) evt.getNewValue()));
        getModel().addPropertyChangeListener(ProgramsTableUIModel.EVENT_RESELECT, evt -> autoSelectRow());
    }

    /**
     * <p>removeLocations.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     */
    public void removeLocations(List<Integer> locationIds) {
        ProgramsTableRowModel programModel = getModel().getSingleSelectedRow();
        if (programModel != null) {
            programModel.getLocations().removeIf(location -> locationIds.contains(location.getId()));
            recomputeRowValidState(programModel);
        }
    }

    public void showManagers() {
        showPersonDialog(ProgramPrivilegeIds.MANAGER);
    }

    public void showRecorderPersons() {
        showPersonDialog(ProgramPrivilegeIds.RECORDER);
    }

    public void showFullViewerPersons() {
        showPersonDialog(ProgramPrivilegeIds.FULL_VIEWER);
    }

    public void showViewerPersons() {
        showPersonDialog(ProgramPrivilegeIds.VIEWER);
    }

    public void showValidatorPersons() {
        showPersonDialog(ProgramPrivilegeIds.VALIDATOR);
    }

    protected void showPersonDialog(ProgramPrivilegeIds privilege) {

        if (getModel().getSingleSelectedRow() == null) {
            return;
        }

        UsersDialogUI usersDialogUI = new UsersDialogUI(getContext());
        usersDialogUI.getModel().setPrivilege(privilege);
        usersDialogUI.getModel().setProgram(getModel().getSingleSelectedRow());

        openDialog(usersDialogUI);

    }

    public void showRecorderDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.RECORDER);
    }

    public void showFullViewerDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.FULL_VIEWER);
    }

    public void showViewerDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.VIEWER);
    }

    public void showValidatorDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.VALIDATOR);
    }

    protected void showDepartmentDialog(ProgramPrivilegeIds privilege) {

        if (getModel().getSingleSelectedRow() == null) {
            return;
        }

        DepartmentsDialogUI departmentsDialogUI = new DepartmentsDialogUI(getContext());
        departmentsDialogUI.getModel().setPrivilege(privilege);
        departmentsDialogUI.getModel().setProgram(getModel().getSingleSelectedRow());

        openDialog(departmentsDialogUI);

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // La table des prelevements
        final SwingTable table = getTable();

        // Column code
        final TableColumnExt columnCode = addColumn(
            ProgramsTableModel.CODE);
        columnCode.setSortable(true);
        columnCode.setEditable(false);

        // Column libelle
        final TableColumnExt columnLibelle = addColumn(
            ProgramsTableModel.NAME);
        columnLibelle.setSortable(true);

        // Column description
        TableColumnExt columnDescription = addCommentColumn(ProgramsTableModel.DESCRIPTION, ProgramDTO.PROPERTY_DESCRIPTION, true);

        // Comment, creation and update dates
        addCommentColumn(ProgramsTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(ProgramsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(ProgramsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // Modele de la table
        final ProgramsTableModel tableModel = new ProgramsTableModel(getTable().getColumnModel());
        table.setModel(tableModel);

        // Les columns obligatoire sont toujours presentes
        columnCode.setHideable(false);
        columnLibelle.setHideable(false);
        columnDescription.setHideable(false);

        // Initialisation de la table
        initTable(table);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        table.setVisibleRowCount(4);

        // Tri par defaut
        table.setSortOrder(ProgramsTableModel.CODE, SortOrder.ASCENDING);
    }

    /**
     * Selection d une ligne dans le tableau suivant l identifiant de la ligne.
     */
    public void autoSelectRow() {

        String previousSelectedProgramCode = getContext().getSelectProgramCode();

        if (StringUtils.isBlank(previousSelectedProgramCode) && getModel().getRowCount() > 1) {
            return;
        }

        // force deselection first
        getModel().setSingleSelectedRow(null);

        ProgramsTableRowModel rowToSelect = null;
        if (getModel().getRowCount() == 1) {

            // unique row
            rowToSelect = getModel().getRows().get(0);

        } else {

            // find row with the previous selected program code
            for (final ProgramsTableRowModel row : getModel().getRows()) {
                if (row.getCode().equals(previousSelectedProgramCode)) {
                    rowToSelect = row;
                    break;
                }
            }
        }

        // Selected row
        if (rowToSelect != null) {
            ProgramsTableRowModel finalRowToSelect = rowToSelect;
            SwingUtilities.invokeLater(() -> {
                selectRow(finalRowToSelect);
                getModel().setSingleSelectedRow(finalRowToSelect);
            });
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingValidator<ProgramsTableUIModel> getValidator() {
        return getUI().getValidator();
    }

    private class StrategyAndLocationLoader extends SwingWorker<Object, Object> {

        private final ProgramsUI adminProgrammeUI = getUI().getParentContainer(ProgramsUI.class);
        private final ProgramsTableRowModel selectedProgram;

        StrategyAndLocationLoader(ProgramsTableRowModel selectedProgram) {
            this.selectedProgram = selectedProgram;
        }

        @Override
        protected Object doInBackground() {

            getContext().getProgramStrategyService().loadStrategiesAndLocations(selectedProgram);

            return null;
        }

        @Override
        protected void done() {
            if (isCancelled()) {
                return;
            }
            try {
                get();
            } catch (InterruptedException | ExecutionException e) {
                throw new DaliTechnicalException(e.getMessage(), e);
            }

            // Update strategies table
            adminProgrammeUI.getStrategiesTableUI().getHandler().load(selectedProgram);

            // Update applied strategies table
            adminProgrammeUI.getLocationsTableUI().getHandler().loadMonitoringLocationsFromProgram(selectedProgram);

            // Suppression des PSFMs
            adminProgrammeUI.getModel().getPmfmsUIModel().clear();

            // revalidate program
            recomputeRowValidState(selectedProgram);
            adminProgrammeUI.getHandler().getValidator().doValidate();

        }
    }
}
