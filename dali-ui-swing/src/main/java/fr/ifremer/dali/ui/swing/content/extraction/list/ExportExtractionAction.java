package fr.ifremer.dali.ui.swing.content.extraction.list;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 22/01/2018.
 */
public class ExportExtractionAction extends AbstractDaliAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

    private File exportFile;

    /**
     * <p>Constructor for ExportExtractionAction.</p>
     *
     * @param handler a H object.
     */
    public ExportExtractionAction(ExtractionsTableUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) return false;

        if (getModel().getSingleSelectedRow() == null) {
            return false;
        }

        if (getModel().getSingleSelectedRow().isDirty()) {
            getContext().getDialogHelper().showWarningDialog(
                    t("dali.extraction.list.export.dirty.message"),
                    t("dali.extraction.list.export.title")
            );
            return false;
        }

        String fileName = String.format("%s-%s", t("dali.service.extraction.file.prefix"), getModel().getSingleSelectedRow().getName());
        String fileExtension = getConfig().getExtractionFileExtension();
        exportFile = saveFile(
                fileName,
                fileExtension,
                t("dali.extraction.list.export.title"),
                t("dali.common.export"),
                "^.*\\." + fileExtension,
                t("dali.common.file." + fileExtension)
        );

        return exportFile != null;
    }

    @Override
    public void doAction() throws Exception {

        getContext().getExtractionService().exportExtraction(getModel().getSingleSelectedRow(), exportFile);
    }

    @Override
    protected void releaseAction() {
        super.releaseAction();

        exportFile = null;
    }
}
