package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des mesures des prelevements (ecran prelevements/mesure).
 */
public abstract class AbstractOperationMeasurementsGroupedTableModel<R extends OperationMeasurementsGroupedRowModel>
    extends AbstractDaliTableModel<R> {

    private boolean readOnly;

    /**
     * Identifiant pour la colonne mnemonique.
     */
    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> SAMPLING = DaliColumnIdentifier.newId(
        OperationMeasurementsGroupedRowModel.PROPERTY_SAMPLING_OPERATION,
        n("dali.property.mnemonic"),
        n("dali.samplingOperation.measurement.mnemonic.tip"),
        SamplingOperationDTO.class, true);

    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> INDIVIDUAL_ID = DaliColumnIdentifier.newReadOnlyId(
        OperationMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_ID,
        n("dali.property.individualId"),
        n("dali.property.individualId"),
        Integer.class
    );

    /**
     * Identifiant pour la colonne groupe taxon.
     */
    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> TAXON_GROUP = DaliColumnIdentifier.newId(
        OperationMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP,
        n("dali.property.taxonGroup.short"),
        n("dali.samplingOperation.measurement.taxonGroup.tip"),
        TaxonGroupDTO.class, DecoratorService.NAME);

    /**
     * Identifiant pour la colonne taxon.
     */
    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> TAXON = DaliColumnIdentifier.newId(
        OperationMeasurementsGroupedRowModel.PROPERTY_TAXON,
        n("dali.property.taxon"),
        n("dali.samplingOperation.measurement.taxon.tip"),
        TaxonDTO.class,
        DecoratorService.WITH_CITATION);

    /**
     * Identifiant pour la colonne taxon saisi.
     */
    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> INPUT_TAXON_NAME = DaliColumnIdentifier.newReadOnlyId(
        OperationMeasurementsGroupedRowModel.PROPERTY_INPUT_TAXON_NAME,
        n("dali.property.inputTaxon"),
        n("dali.samplingOperation.measurement.inputTaxon.tip"),
        String.class);

    /**
     * Identifiant pour la colonne valeur.
     */
    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> COMMENT = DaliColumnIdentifier.newId(
        OperationMeasurementsGroupedRowModel.PROPERTY_COMMENT,
        n("dali.property.comment"),
        n("dali.samplingOperation.measurement.comment.tip"),
        String.class);

    /**
     * Identifiant pour la colonne analyst.
     */
    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> ANALYST = DaliColumnIdentifier.newId(
        OperationMeasurementsGroupedRowModel.PROPERTY_ANALYST,
        n("dali.property.analyst"),
        n("dali.samplingOperation.measurement.analyst.tip"),
        DepartmentDTO.class);

    /**
     * Identifiant pour la colonne analysisInstrument.
     */
    public static final DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> ANALYSIS_INSTRUMENT = DaliColumnIdentifier.newId(
        OperationMeasurementsGroupedRowModel.PROPERTY_ANALYSIS_INSTRUMENT,
        n("dali.property.analysisInstrument"),
        n("dali.samplingOperation.measurement.analysisInstrument.tip"),
        AnalysisInstrumentDTO.class);

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public AbstractOperationMeasurementsGroupedTableModel(final SwingTableColumnModel columnModel, boolean createNewRow) {
        super(columnModel, createNewRow, false);
        readOnly = false;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public boolean isCreateNewRow() {
        return !readOnly && super.isCreateNewRow();
    }


    public abstract DaliColumnIdentifier<R> getPmfmInsertPosition();

}
