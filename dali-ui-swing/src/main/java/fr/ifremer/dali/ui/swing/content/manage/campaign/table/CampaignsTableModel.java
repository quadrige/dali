package fr.ifremer.dali.ui.swing.content.manage.campaign.table;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.time.LocalDate;
import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class CampaignsTableModel extends AbstractDaliTableModel<CampaignsTableRowModel> {

    /**
     * Identifiant pour la colonne libelle.
     */
    public static final DaliColumnIdentifier<CampaignsTableRowModel> NAME = DaliColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.campaign.name.tip"),
            String.class,
            true);

    public static final DaliColumnIdentifier<CampaignsTableRowModel> START_DATE = DaliColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_START_DATE,
            n("dali.property.date.start"),
            n("dali.campaign.date.start.tip"),
            LocalDate.class,
            true);

    public static final DaliColumnIdentifier<CampaignsTableRowModel> END_DATE = DaliColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_END_DATE,
            n("dali.property.date.end"),
            n("dali.campaign.date.end.tip"),
            LocalDate.class);

    public static final DaliColumnIdentifier<CampaignsTableRowModel> SISMER_LINK = DaliColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_SISMER_LINK,
            n("dali.property.sismer.link"),
            n("dali.campaign.sismer.link.tip"),
            String.class);

    public static final DaliColumnIdentifier<CampaignsTableRowModel> MANAGER = DaliColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_MANAGER,
            n("dali.property.manager"),
            n("dali.campaign.manager.tip"),
            PersonDTO.class,
            true);

    public static final DaliColumnIdentifier<CampaignsTableRowModel> COMMENT = DaliColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_COMMENT,
            n("dali.property.comment"),
            n("dali.campaign.comment.tip"),
            String.class);

    public static final DaliColumnIdentifier<CampaignsTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
        CampaignsTableRowModel.PROPERTY_UPDATE_DATE,
        n("dali.property.date.modification"),
        n("dali.property.date.modification"),
        Date.class);


    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public CampaignsTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, true, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CampaignsTableRowModel createNewRow() {
        return new CampaignsTableRowModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DaliColumnIdentifier<CampaignsTableRowModel> getFirstColumnEditing() {
        return NAME;
    }

}
