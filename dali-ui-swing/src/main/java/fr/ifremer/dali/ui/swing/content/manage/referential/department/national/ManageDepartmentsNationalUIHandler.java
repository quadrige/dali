package fr.ifremer.dali.ui.swing.content.manage.referential.department.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.referential.department.menu.DepartmentMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.department.table.DepartmentRowModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.department.table.DepartmentTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Departments au niveau national
 */
public class ManageDepartmentsNationalUIHandler extends AbstractDaliTableUIHandler<DepartmentRowModel, ManageDepartmentsNationalUIModel, ManageDepartmentsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageDepartmentsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageDepartmentsNationalUIModel model = new ManageDepartmentsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(ManageDepartmentsNationalUI ui) {
        initUI(ui);

        // hide 'apply filter'
        ui.getDepartmentMenuUI().getHandler().enableContextFilter(false);

        // listen to search results
        ui.getDepartmentMenuUI().getModel().addPropertyChangeListener(DepartmentMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<DepartmentDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // code
        TableColumnExt codeCol = addColumn(DepartmentTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // name
        TableColumnExt nameCol = addColumn(DepartmentTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // description
        TableColumnExt descCol = addColumn(DepartmentTableModel.DESCRIPTION);
        descCol.setSortable(true);
        descCol.setEditable(false);

        // parent
        TableColumnExt parentCol = addFilterableComboDataColumnToModel(
                DepartmentTableModel.PARENT_DEPARTMENT,
                getContext().getReferentialService().getDepartments(StatusFilter.ALL),
                true);
        parentCol.setSortable(true);
        parentCol.setEditable(false);

        // email
        TableColumnExt emailCol = addColumn(DepartmentTableModel.EMAIL);
        emailCol.setSortable(true);
        emailCol.setEditable(false);

        // phone
        TableColumnExt phoneCol = addColumn(DepartmentTableModel.PHONE);
        phoneCol.setSortable(true);
        phoneCol.setEditable(false);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                DepartmentTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        // address
        TableColumnExt addressCol = addColumn(DepartmentTableModel.ADDRESS);
        addressCol.setSortable(true);
        addressCol.setEditable(false);

        // siret & url
        TableColumnExt siretCol = addColumn(DepartmentTableModel.SIRET);
        siretCol.setSortable(true);
        siretCol.setEditable(false);
        TableColumnExt urlCol = addColumn(DepartmentTableModel.URL);
        urlCol.setSortable(true);
        urlCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(DepartmentTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(DepartmentTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(DepartmentTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);


        DepartmentTableModel tableModel = new DepartmentTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.department"));

        // Initialisation du tableau
        initTable(table, true);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // optional columns are hidden
        addressCol.setVisible(false);
        siretCol.setVisible(false);
        urlCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<DepartmentRowModel> getTableModel() {
        return (DepartmentTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getDepartmentsNationalTable();
    }
}
