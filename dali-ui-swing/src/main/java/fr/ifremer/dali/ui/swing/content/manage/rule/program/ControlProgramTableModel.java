package fr.ifremer.dali.ui.swing.content.manage.rule.program;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlProgramTableModel extends AbstractDaliTableModel<ControlProgramTableRowModel> {

	/** Constant <code>CODE</code> */
	public static final DaliColumnIdentifier<ControlProgramTableRowModel> CODE = DaliColumnIdentifier.newId(
			ControlProgramTableRowModel.PROPERTY_CODE,
			n("dali.property.code"),
			n("dali.rule.program.code.tip"),
			String.class);
	
	/** Constant <code>NAME</code> */
	public static final DaliColumnIdentifier<ControlProgramTableRowModel> NAME = DaliColumnIdentifier.newId(
			ControlProgramTableRowModel.PROPERTY_NAME,
			n("dali.property.name"),
			n("dali.rule.program.name.tip"),
			String.class);
	
	/** Constant <code>COMMENT</code> */
	public static final DaliColumnIdentifier<ControlProgramTableRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
			ControlProgramTableRowModel.PROPERTY_DESCRIPTION,
			n("dali.property.description"),
			n("dali.rule.program.description.tip"),
			String.class);
	
	
	/**
	 * <p>Constructor for ControlProgramTableModel.</p>
	 *
	 */
	public ControlProgramTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ControlProgramTableRowModel createNewRow() {
		return new ControlProgramTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ControlProgramTableRowModel> getFirstColumnEditing() {
		return CODE;
	}
}
