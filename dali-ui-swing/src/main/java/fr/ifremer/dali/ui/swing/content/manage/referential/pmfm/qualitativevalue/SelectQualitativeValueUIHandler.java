package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.ArrayList;

/**
 * @author peck7 on 24/10/2017.
 */
public class SelectQualitativeValueUIHandler extends AbstractDaliUIHandler<SelectQualitativeValueUIModel, SelectQualitativeValueUI> implements Cancelable {

    public static final String QUALITATIVE_VALUES_LIST = "qualitativeValuesList";
    public static final String QUALITATIVE_VALUES_DOUBLE_LIST = "qualitativeValuesDoubleList";

    @Override
    public void beforeInit(SelectQualitativeValueUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(new SelectQualitativeValueUIModel());
    }

    @Override
    public void afterInit(SelectQualitativeValueUI ui) {

        initUI(ui);

        ui.getAssociatedQualitativeValuesList().setCellRenderer(newListCellRender(QualitativeValueDTO.class));
        initBeanList(ui.getAssociatedQualitativeValuesDoubleList(), null,null);

        getModel().addPropertyChangeListener(SelectQualitativeValueUIModel.PROPERTY_AVAILABLE_LIST,
                evt -> getUI().getAssociatedQualitativeValuesDoubleList().getModel().setUniverse(getModel().getAvailableList()));

        getModel().addPropertyChangeListener(SelectQualitativeValueUIModel.PROPERTY_SELECTED_LIST,
                evt -> {
            if (getModel().getSelectedList() != null) {
                getUI().getAssociatedQualitativeValuesDoubleList().getModel().setSelected(getModel().getSelectedList());
                getUI().getAssociatedQualitativeValuesList().setListData(getModel().getSelectedList().toArray(new QualitativeValueDTO[0]));
            }
                });

    }

    public void setEnabled(boolean enabled) {
        if (enabled)
            getUI().getAssociatedQualitativeValuesPanelLayout().setSelected(QUALITATIVE_VALUES_DOUBLE_LIST);
        else
            getUI().getAssociatedQualitativeValuesPanelLayout().setSelected(QUALITATIVE_VALUES_LIST);

        getUI().getActionValidate().setEnabled(enabled);
    }

    public void valid() {
        getModel().setSelectedList(new ArrayList<>(getUI().getAssociatedQualitativeValuesDoubleList().getModel().getSelected()));
        getModel().setValid(true);
        closeDialog();
    }

    @Override
    public void cancel() {
        getModel().setSelectedList(null);
        getModel().setValid(false);
        closeDialog();
    }
}
