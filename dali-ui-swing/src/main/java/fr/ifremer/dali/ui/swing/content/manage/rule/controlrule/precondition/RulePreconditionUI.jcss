/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

#rulePreconditionUI {
	modal: true;
	resizable: true;
	title: "dali.rule.rulePrecondition.title";
}

#baseRulePanel {
    border: {BorderFactory.createTitledBorder("")};
}

#usedRulePanel {
    border: {BorderFactory.createTitledBorder("")};
}

#availableLabel {
	text: "dali.filter.available.label";
	horizontalAlignment: "{SwingConstants.CENTER}";
}

#selectedLabel {
	text: "dali.filter.selected.label";
	horizontalAlignment: "{SwingConstants.CENTER}";
}

#usedRuleQVDoubleList {
    bean: {model};
    property: "selectedValues";
	filterable: true;
	showDecorator: true;
    alwaysSortSelected: true;
}

#usedRuleQVList {
    enabled: false;
}

#cancelButton {
	actionIcon: cancel;
	text: "dali.common.cancel";
	toolTipText: "dali.common.cancel";
	i18nMnemonic: "dali.common.cancel.mnemonic";
}

#validButton {
	actionIcon: accept;
	text: "dali.common.validate";
	toolTipText: "dali.common.validate";
	i18nMnemonic: "dali.common.validate.mnemonic";
	enabled: {model.isValid()};
}