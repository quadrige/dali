package fr.ifremer.dali.ui.swing.content.manage.rule;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.ui.swing.action.AbstractDaliRemoteSaveAction;
import fr.ifremer.dali.ui.swing.content.manage.rule.menu.SearchAction;
import fr.ifremer.quadrige3.core.exception.SaveForbiddenException;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Save rule list action
 */
public class SaveAction extends AbstractDaliRemoteSaveAction<RulesUIModel, RulesUI, RulesUIHandler> {

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final RulesUIHandler handler) {
        super(handler, false);
    }

    @Override
    protected void doSave() {

        // service call
        getContext().getRuleListService().saveRuleLists(getContext().getAuthenticationInfo(), getModel().getRuleListUIModel().getRows());
    }

    @Override
    protected void reload() {

        // If control rules in table, force newCode state to false (Mantis #46454)
        if (getModel().getControlRuleUIModel().getRowCount() > 0) {
            getModel().getControlRuleUIModel().getRows().forEach(controlRuleRowModel -> controlRuleRowModel.setNewCode(false));
        }

        // reload ComboBox
        getUI().getRulesMenuUI().getHandler().reloadComboBox();

        // reload rules
        getActionEngine().runInternalAction(getUI().getRulesMenuUI().getHandler(), SearchAction.class);
    }

    @Override
    protected void onSaveForbiddenException(SaveForbiddenException exception) {

        if (CollectionUtils.isNotEmpty(exception.getObjectIds())) {
            getContext().getDialogHelper().showErrorDialog(
                    t("dali.action.save.rules.forbidden.topMessage"),
                    ApplicationUIUtil.getHtmlString(exception.getObjectIds()),
                    t("dali.action.save.rules.forbidden.bottomMessage"),
                    t("dali.action.save.errors.title"));
        } else {
            getContext().getDialogHelper().showErrorDialog(
                    t("dali.action.save.rules.forbidden.message"),
                    t("dali.action.save.errors.title"));
        }

    }

    /** {@inheritDoc} */
    @Override
    protected List<AbstractBeanUIModel> getModelsToModify() {
        List<AbstractBeanUIModel> models = Lists.newArrayList();
        models.add(getModel().getRuleListUIModel());
        models.add(getModel().getProgramsUIModel());
        models.add(getModel().getDepartmentsUIModel());
        models.add(getModel().getControlRuleUIModel());
        models.add(getModel().getPmfmUIModel());
        return models;
    }

}
