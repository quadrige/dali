package fr.ifremer.dali.ui.swing.content.home.survey;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.referential.QualityLevelDTO;
import fr.ifremer.dali.ui.swing.action.AbstractDaliAction;
import fr.ifremer.dali.ui.swing.content.home.HomeUIModel;
import fr.ifremer.dali.ui.swing.content.home.survey.qualify.QualifySurveyUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de changer l'état d'une observation dans le tableau des
 * observations de l ecran d accueil.
 */
public class QualifySurveyAction extends AbstractDaliAction<SurveysTableUIModel, SurveysTableUI, SurveysTableUIHandler> {

    private Collection<? extends SurveyDTO> selectedSurveys;

    private String qualificationComment;
    private QualityLevelDTO qualityLevel;

    /**
     * Loggeur.
     */
    private static final Log LOG = LogFactory.getLog(QualifySurveyAction.class);

    /**
     * Constructor.
     *
     * @param handler Controleur
     */
    public QualifySurveyAction(final SurveysTableUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucune Observation de selectionne");
            return false;
        }

        selectedSurveys = getModel().getSelectedRows();

        if (!getContext().getPermissions(selectedSurveys).isQualifier) {
            displayErrorMessage(t("dali.action.qualify.survey.title"), t("dali.action.qualify.survey.error.forbidden"));
            return false;
        }

        // check if selected surveys can be qualified
        for (SurveyDTO survey : selectedSurveys) {

            // On ne peut pas valider une observation modifiée (normallement cela n'est pas possible)
            if (survey.isDirty()) {
                getContext().getDialogHelper().showWarningDialog(t("dali.action.qualify.survey.error.dirty"));
                return false;
            }

            // On ne peut pas qualifier une observation qui n'est pas validée
            if (!DaliBeans.isSurveyValidated(survey)) {
                displayWarningMessage(t("dali.action.qualify.survey.title"), t("dali.action.qualify.survey.error.notValid"));
                return false;
            }

        }

        // Demande de confirmation avant la qualification
        QualifySurveyUI qualifySurveyUI = new QualifySurveyUI(getContext());
        getHandler().openDialog(qualifySurveyUI);

        if (qualifySurveyUI.getModel().isValid()) {
            qualificationComment = qualifySurveyUI.getModel().getComment();
            qualityLevel = qualifySurveyUI.getModel().getQualityLevel();
            return true;
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {
        boolean isModifyModelState = getModel().isModify();

        // Qualify surveys
        getContext().getObservationService().qualifySurveys(selectedSurveys, qualificationComment, qualityLevel, createProgressionUIModel());

        getModel().setModify(isModifyModelState);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        // force reloading operations
        getModel().getMainUIModel().firePropertyChanged(HomeUIModel.PROPERTY_SELECTED_SURVEY, null, null);

        super.postSuccessAction();
    }
}
