package fr.ifremer.dali.ui.swing.content.observation.photo;

import fr.ifremer.quadrige3.core.dao.technical.Images;
import fr.ifremer.quadrige3.core.dao.technical.enumeration.EnumValue;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 18/06/2019.
 */
public enum PhotoViewType implements EnumValue {

    VIEW_DIAPO("DIAPO", n("dali.photo.type.diaporama"), Images.ImageType.DIAPO),
    VIEW_THUMBNAIL("THUMBNAIL", n("dali.photo.type.thumbnail"), Images.ImageType.THUMBNAIL);

    private final String code;
    private final String i18nLabel;
    private final Images.ImageType imageType;


    PhotoViewType(String code, String i18nLabel, Images.ImageType imageType) {
        this.code = code;
        this.i18nLabel = i18nLabel;
        this.imageType = imageType;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return t(i18nLabel);
    }

    public Images.ImageType getImageType() {
        return imageType;
    }
}
