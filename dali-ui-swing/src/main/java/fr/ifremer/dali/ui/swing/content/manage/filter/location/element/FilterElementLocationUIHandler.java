package fr.ifremer.dali.ui.swing.content.manage.filter.location.element;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.AbstractFilterElementUIHandler;
import fr.ifremer.dali.ui.swing.content.manage.referential.location.menu.LocationMenuUI;

/**
 * Controler.
 */
public class FilterElementLocationUIHandler extends AbstractFilterElementUIHandler<LocationDTO, FilterElementLocationUI, LocationMenuUI> {

    /** {@inheritDoc} */
    @Override
    protected LocationMenuUI createNewReferentialMenuUI() {
        return new LocationMenuUI(getUI());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(FilterElementLocationUI ui) {

        // Force bean type on double list
        getUI().getFilterDoubleList().setBeanType(LocationDTO.class);

        super.afterInit(ui);

        // hide code / name editors
        getReferentialMenuUI().getLabelEditor().getParent().setVisible(false);
        getReferentialMenuUI().getNameEditor().getParent().setVisible(false);

    }

}
