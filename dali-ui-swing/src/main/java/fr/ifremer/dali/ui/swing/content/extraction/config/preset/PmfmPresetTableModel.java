package fr.ifremer.dali.ui.swing.content.extraction.config.preset;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * @author peck7 on 19/07/2017.
 */
public class PmfmPresetTableModel extends AbstractDaliTableModel<PmfmPresetRowModel> {

    public static final DaliColumnIdentifier<PmfmPresetRowModel> PMFM = DaliColumnIdentifier.newId(
            PmfmPresetRowModel.PROPERTY_PMFM,
            n("dali.property.pmfm"),
            n("dali.property.pmfm"),
            PmfmDTO.class
    );

    public static final DaliColumnIdentifier<PmfmPresetRowModel> QUALITATIVE_VALUES = DaliColumnIdentifier.newId(
            PmfmPresetRowModel.PROPERTY_QUALITATIVE_VALUES,
            n("dali.property.qualitativeValues.count"),
            n("dali.property.qualitativeValues.count"),
            QualitativeValueDTO.class,
            DecoratorService.COLLECTION_SIZE
    );

    public PmfmPresetTableModel(SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    @Override
    public DaliColumnIdentifier<PmfmPresetRowModel> getFirstColumnEditing() {
        return PMFM;
    }

    @Override
    public PmfmPresetRowModel createNewRow() {
        return new PmfmPresetRowModel();
    }
}
