package fr.ifremer.dali.ui.swing.content.manage.referential.location.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.location.menu.LocationMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.location.table.LocationTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.location.table.LocationTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.component.coordinate.CoordinateEditor;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des lieux au niveau national
 */
public class LocationNationalUIHandler extends AbstractDaliTableUIHandler<LocationTableRowModel, LocationNationalUIModel, LocationNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(LocationNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        LocationNationalUIModel model = new LocationNationalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(LocationNationalUI ui) {
        initUI(ui);

        // listen to search results
        ui.getLocationNationalMenuUI().getModel().addPropertyChangeListener(LocationMenuUIModel.PROPERTY_RESULTS,
                evt -> getModel().setBeans((List<LocationDTO>) evt.getNewValue()));

        // listen to 'apply filter' results
        ui.getLocationNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS,
                evt -> getModel().setBeans((List<LocationDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // id
        TableColumnExt idCol = addColumn(LocationTableModel.ID);
        idCol.setSortable(true);
        idCol.setEditable(false);
        fixColumnWidth(idCol, 80);

        // label
        TableColumnExt identifiantCol = addColumn(LocationTableModel.LABEL);
        identifiantCol.setSortable(true);
        identifiantCol.setEditable(false);
        fixColumnWidth(identifiantCol, 100);

        // Libelle
        TableColumnExt libelleCol = addColumn(LocationTableModel.NAME);
        libelleCol.setSortable(true);
        libelleCol.setEditable(false);

        // Bathymetrie
        TableColumnExt bathymetrieCol = addColumn(LocationTableModel.BATHYMETRIE);
        bathymetrieCol.setSortable(true);
        bathymetrieCol.setEditable(false);
        fixColumnWidth(bathymetrieCol, 100);

        // Latitude Min
        TableColumnExt latitudeMinCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MIN,
                LocationTableModel.LATITUDE_MIN);
        latitudeMinCol.setSortable(true);
        latitudeMinCol.setEditable(false);
        latitudeMinCol.setPreferredWidth(100);

        // Longitude Min
        TableColumnExt longitudeMinCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MIN,
                LocationTableModel.LONGITUDE_MIN);
        longitudeMinCol.setSortable(true);
        longitudeMinCol.setEditable(false);
        longitudeMinCol.setPreferredWidth(100);

        // Commentaire
        addCommentColumn(LocationTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(LocationTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(LocationTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // Port de Rattachement
        TableColumnExt portRattachementCol = addColumn(LocationTableModel.HARBOUR);
        portRattachementCol.setSortable(true);
        portRattachementCol.setEditable(false);

        // Delta UT Hivers
        TableColumnExt deltaUTHiverCol = addColumn(LocationTableModel.DELTA_UT_HIVER);
        deltaUTHiverCol.setSortable(true);
        deltaUTHiverCol.setEditable(false);

        // Fiche du lieu
//		TableColumnExt ficheLieuCol = addColumnToModel(columnModel, LocationTableModel.FICHE_LIEU);
//		ficheLieuCol.setSortable(true);

        // Changement d heure
        final TableColumnExt changementHeureCol = addBooleanColumnToModel(LocationTableModel.DAYLIGHT_SAVING_TIME, table);
        changementHeureCol.setSortable(true);
        changementHeureCol.setEditable(false);

        // Latitude Max
        TableColumnExt latitudeMaxCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MAX,
                LocationTableModel.LATITUDE_MAX);
        latitudeMaxCol.setSortable(true);
        latitudeMaxCol.setEditable(false);
        latitudeMaxCol.setPreferredWidth(100);

        // Longitude Max
        TableColumnExt longitudeMaxCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MAX,
                LocationTableModel.LONGITUDE_MAX);
        longitudeMaxCol.setSortable(true);
        longitudeMaxCol.setEditable(false);
        longitudeMaxCol.setPreferredWidth(100);

        // Positionnement
        TableColumnExt positioningCol = addFilterableComboDataColumnToModel(
                LocationTableModel.POSITIONING_NAME,
                getContext().getReferentialService().getPositioningSystems(), false);
        positioningCol.setSortable(true);
        positioningCol.setEditable(false);
        positioningCol.setPreferredWidth(200);

        TableColumnExt positioningPrecisionCol = addColumn(
                LocationTableModel.POSITIONING_PRECISION);
        positioningPrecisionCol.setEditable(false);
        positioningPrecisionCol.setPreferredWidth(100);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
            LocationTableModel.STATUS,
            getContext().getReferentialService().getStatus(StatusFilter.ACTIVE),
            false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        LocationTableModel tableModel = new LocationTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("dali.property.location"));

        // Initialisation du tableau
        initTable(table, true);

        // Les colonnes optionnelles sont invisibles
        portRattachementCol.setVisible(false);
        deltaUTHiverCol.setVisible(false);
        changementHeureCol.setVisible(false);
        latitudeMaxCol.setVisible(false);
        longitudeMaxCol.setVisible(false);
        positioningCol.setVisible(false);
        positioningPrecisionCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<LocationTableRowModel> getTableModel() {
        return (LocationTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getLocationNationalTable();
    }

}
