package fr.ifremer.dali.ui.swing.content.observation;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dto.CoordinateDTO;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.ui.swing.action.GoToObservationAction;
import fr.ifremer.dali.ui.swing.action.QuitScreenAction;
import fr.ifremer.dali.ui.swing.content.home.survey.SurveysTableRowModel;
import fr.ifremer.dali.ui.swing.content.observation.survey.SurveyDetailsTabUI;
import fr.ifremer.dali.ui.swing.content.observation.survey.SurveyDetailsTabUIModel;
import fr.ifremer.dali.ui.swing.util.plaf.PlafUtils;
import fr.ifremer.dali.ui.swing.util.tab.AbstractDaliTabContainerUIHandler;
import fr.ifremer.dali.ui.swing.util.tab.DaliTabIndexes;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.state.SwingTableSessionState;
import fr.ifremer.quadrige3.ui.swing.table.state.SwingTableState;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.session.State;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker;
import java.awt.Color;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour observation.
 */
public class ObservationUIHandler extends AbstractDaliTabContainerUIHandler<ObservationUIModel, ObservationUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ObservationUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ObservationUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new ObservationUIModel());
        ui.setContextValue(SwingUtil.createActionIcon("observation"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ObservationUI ui) {

        initUI(ui);
        SwingUtil.setLayerUI(getTabPanel(), ui.getSurveyBlockLayer());

        // Affect models
        getModel().setSurveyDetailsTabUIModel(getUI().getSurveyDetailsTabUI().getModel());
        getModel().setOperationMeasurementsTabUIModel(getUI().getOperationMeasurementsTabUI().getModel());
        getModel().setPhotosTabUIModel(getUI().getPhotosTabUI().getModel());

        // Ajout des onglets
        setCustomTab(DaliTabIndexes.OBSERVATION_GENERAL, getModel().getSurveyDetailsTabUIModel());
        setCustomTab(DaliTabIndexes.OPERATION_MEASUREMENTS, getModel().getOperationMeasurementsTabUIModel());
        setCustomTab(DaliTabIndexes.PHOTOS, getModel().getPhotosTabUIModel());

        // set label colors
        ApplicationUIUtil.setChildrenLabelForeground(ui, getConfig().getColorThematicLabel());
        ui.getObservationDescriptionLabel().setForeground(Color.BLACK);

        // Selection de l onglet actif
        getTabPanel().setSelectedIndex(getUI().getTabIndex());

        // Binding save button
        getUI().applyDataBinding(ObservationUI.BINDING_SAVE_BUTTON_ENABLED);

        // button border
        getUI().getNextButton().setBorder(
                BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), ui.getNextButton().getBorder())
        );

        // Listen modify
        listenModelModify(getModel().getSurveyDetailsTabUIModel());
        listenModelModify(getModel().getOperationMeasurementsTabUIModel());
        listenModelModify(getModel().getPhotosTabUIModel());

        // Listen valid
        listenModelValid(getModel().getSurveyDetailsTabUIModel());
        listenModelValid(getModel().getOperationMeasurementsTabUIModel());
        listenModelValid(getModel().getPhotosTabUIModel());

        // Register validators
        registerValidators(
                getUI().getSurveyDetailsTabUI().getValidator(),
                getUI().getOperationMeasurementsTabUI().getValidator(),
                getUI().getPhotosTabUI().getValidator());

        // listener on properties that updates the title
        getModel().addPropertyChangeListener(evt -> {
            if (ObservationUIModel.PROPERTY_LOCATION.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_DATE.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_TIME.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_PROGRAM.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_NAME.equals(evt.getPropertyName())
            ) {
                getUI().applyDataBinding(ObservationUI.BINDING_OBSERVATION_DESCRIPTION_LABEL_TEXT);
            }

            if (!getModel().isLoading() && !ImmutableList.of(SurveyDetailsTabUIModel.PROPERTY_VALID, SurveyDetailsTabUIModel.PROPERTY_LOADING).contains(evt.getPropertyName())) {
                forceRevalidateModel();
                getUI().applyDataBinding(ObservationUI.BINDING_SAVE_BUTTON_ENABLED);
                getUI().applyDataBinding(ObservationUI.BINDING_NEXT_BUTTON_ENABLED);
            }
        });

        // load model
        loadSurvey();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTitle() {
        String contextName;
        if (getContext().getSelectedContext() == null) {
            contextName = t("dali.main.title.noContext");
        } else {
            contextName = getContext().getSelectedContext().getName();
        }

        return String.format("%s [%s]", super.getTitle(), contextName);
    }

    private void loadSurvey() {

        getModel().setLoading(true);
        SurveyLoader loader = new SurveyLoader();
        loader.execute();

    }

    /**
     * Select next available tab
     */
    public void selectNextTab() {

        int index = getTabPanel().getSelectedIndex() + 1;

        if (index >= getTabPanel().getTabCount()) {
            // if last tab is reached, return to ObservationUI
            changeScreenAction(GoToObservationAction.class);
        }

        while (index < getTabPanel().getTabCount()) {
            if (getTabPanel().isEnabledAt(index)) {
                getTabPanel().setSelectedIndex(index);
                return;
            }
            index++;
        }
    }

    /**
     * <p>refreshModels.</p>
     */
    public void refreshModels() {

        // force components to update enabled state if survey is editable or not
        getModel().getSurveyDetailsTabUIModel().getUngroupedTableUIModel().setSurvey(getModel());
        getModel().getSurveyDetailsTabUIModel().setEditable(getModel().isEditable());
        getModel().getOperationMeasurementsTabUIModel().setObservationModel(getModel());
        getModel().getPhotosTabUIModel().setObservationModel(getModel());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JTabbedPane getTabPanel() {
        return getUI().getObservationTabbedPane();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeTab(int i) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private class SurveyLoader extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() {

            // Observation
            final SurveyDTO survey = getContext().getObservationService().getSurvey(getContext().getSelectedSurveyId());

            // run a control
            getContext().getRulesControlService().controlSurvey(survey, false, false);

            // Determine if current user is allowed to edit survey
            boolean userIsRecorder = getContext().getDataContext().isSurveyEditable(survey);

            // Prepare editable state for general tab to speed up some behaviours
            getModel().setEditable(userIsRecorder && !DaliBeans.isSurveyValidated(survey));
            getModel().getSurveyDetailsTabUIModel().setEditable(getModel().isEditable());

            // load preconditioned rules
            getModel().setPreconditionedRules(getContext().getRuleListService().getPreconditionedControlRulesForSurvey(survey));

            // load pmfms under moratorium
            getModel().setPmfmsUnderMoratorium(getContext().getObservationService().getPmfmsUnderMoratorium(survey));

            // load observation into parent model (this)
            getModel().fromBean(survey);

            return null;
        }

        @Override
        protected void done() {
            try {
                try {
                    get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new DaliTechnicalException(e.getMessage(), e);
                }

                getUI().applyDataBinding(ObservationUI.BINDING_OBSERVATION_DESCRIPTION_LABEL_TEXT);

                // affect other models
                refreshModels();

                if (getModel().isEditable()) {
                    updateUnusedEditor();
                }

                // Reset modify flag
                getModel().setModify(false);

            } finally {

                getModel().setLoading(false);
            }
        }
    }

    /**
     * Update editors background when unselected in home screen
     * Set unused background color to each component (not mandatory) without value
     * <p>
     * see Mantis #38351
     */
    private void updateUnusedEditor() {

        // get surveys table state
        SwingTableState surveysTableState = getSurveysTableState();
        if (surveysTableState == null) return;
        Color unusedColor = getConfig().getColorUnusedEditorBackground();
        SurveyDetailsTabUI surveyUI = getUI().getSurveyDetailsTabUI();
        Map<String, Integer> visibility = surveysTableState.getVisibility();

        // time
        if (getModel().getTime() == null && visibility.get(SurveysTableRowModel.PROPERTY_TIME) == -1) {
            surveyUI.getTimeEditor().setBackground(unusedColor);
        }

        // name
        if (StringUtils.isEmpty(getModel().getName()) && visibility.get(SurveysTableRowModel.PROPERTY_NAME) == -1) {
            surveyUI.getNameEditor().setBackground(unusedColor);
        }

        // bottom depth
        if (getModel().getBottomDepth() == null && visibility.get(SurveysTableRowModel.PROPERTY_BOTTOM_DEPTH) == -1) {
            surveyUI.getBottomDepthEditor().getTextField().setBackground(unusedColor);
        }

        // campaign
        if (getModel().getCampaign() == null && visibility.get(SurveysTableRowModel.PROPERTY_CAMPAIGN) == -1) {
            PlafUtils.changeBackground(surveyUI.getCampaignCombo().getCombobox(), unusedColor);
        }

        CoordinateDTO coordinate = getModel().getCoordinate();
        // latitude start
        if ((coordinate == null || coordinate.getMinLatitude() == null) && visibility.get(SurveysTableRowModel.PROPERTY_LATITUDE_MIN) == -1) {
            surveyUI.getSurveyLatitudeMinEditor().getTextField().setBackground(unusedColor);
        }
        // latitude end
        if ((coordinate == null || coordinate.getMaxLatitude() == null) && visibility.get(SurveysTableRowModel.PROPERTY_LATITUDE_MAX) == -1) {
            surveyUI.getSurveyLatitudeMaxEditor().getTextField().setBackground(unusedColor);
        }
        // longitude start
        if ((coordinate == null || coordinate.getMinLongitude() == null) && visibility.get(SurveysTableRowModel.PROPERTY_LONGITUDE_MIN) == -1) {
            surveyUI.getSurveyLongitudeMinEditor().getTextField().setBackground(unusedColor);
        }
        // longitude end
        if ((coordinate == null || coordinate.getMaxLongitude() == null) && visibility.get(SurveysTableRowModel.PROPERTY_LONGITUDE_MAX) == -1) {
            surveyUI.getSurveyLongitudeMaxEditor().getTextField().setBackground(unusedColor);
        }

        // positioning
        if (getModel().getPositioning() == null && visibility.get(SurveysTableRowModel.PROPERTY_POSITIONING) == -1) {
            PlafUtils.changeBackground(surveyUI.getPositioningCombo().getCombobox(), unusedColor);
            surveyUI.getPositioningCommentEditor().setBackground(unusedColor);
        }

    }

    private SwingTableState getSurveysTableState() {

        State state = getContext().getSwingSession().findStateByComponentName("/homeUI.*/surveysTableUI.*/surveysTable");

        if (state instanceof SwingTableSessionState) {
            SwingTableSessionState tableSessionState = (SwingTableSessionState) state;
            if (tableSessionState.getSessionStates() != null) {
                return tableSessionState.getSessionStates().get(AbstractTableModel.DEFAULT_STATE_CONTEXT);
            }
        }

        return null;
    }

}
