package fr.ifremer.dali.ui.swing.content.observation;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.service.control.ControlRuleMessagesBean;
import fr.ifremer.dali.ui.swing.action.AbstractDaliSaveAction;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;

import static org.nuiton.i18n.I18n.t;

/**
 * Save action.
 */
public class SaveAction extends AbstractDaliSaveAction<ObservationUIModel, ObservationUI, ObservationUIHandler> {

    private ControlRuleMessagesBean controlMessages;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final ObservationUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        // check if survey is not dirty
        if (DaliBeans.isSurveyValidated(getModel())) {
            getContext().getDialogHelper().showErrorDialog(t("dali.action.save.observation.alreadySynchronized"), t("dali.action.save.observation"));
            return false;
        }

        // Check survey date within campaign dates
        if (getModel().getCampaign() != null) {
            if (getModel().getDate().isBefore(getModel().getCampaign().getStartDate())
                    || (getModel().getCampaign().getEndDate() != null && getModel().getDate().isAfter(getModel().getCampaign().getEndDate()))) {
                // The save is invalid if the survey is outside campaign dates
                getContext().getDialogHelper().showErrorDialog(
                        t("dali.action.save.observation.notInCampaign", DaliBeans.toString(getModel()), DaliBeans.toString(getModel().getCampaign())),
                        t("dali.action.save.observations"));
                return false;
            }
        }

        // check for individual measurements with taxon (or taxon group) and without measurement value
        if (isSurveyHasEmptyTaxonMeasurements()) {
            getContext().getDialogHelper().showErrorDialog(t("dali.action.save.observation.emptyTaxonMeasurement"), t("dali.action.save.observation"));
            return false;
        }

//        // Save partial models from tabs
//        getUI().getSurveyDetailsTabUI().getHandler().saveActualModel();
//        getUI().getPhotosTabUI().getHandler().saveActualModel();
//
//        // Save and control measurements tabs (Mantis #52401)
//        getUI().getOperationMeasurementsTabUI().getHandler().save();

        return true;
    }

    private boolean isSurveyHasEmptyTaxonMeasurements() {

        // check operations individual measurement rows
        for (OperationMeasurementsGroupedRowModel row : getModel().getOperationMeasurementsTabUIModel().getGroupedTableUIModel().getRows()) {
            if (row.getTaxonGroup() != null || row.getTaxon() != null) {
                boolean rowWithoutMeasurements = true;
                for (MeasurementDTO measurement : row.getMeasurements()) {
                    if (!DaliBeans.isMeasurementEmpty(measurement)) {
                        rowWithoutMeasurements = false;
                        break;
                    }
                }
                if (rowWithoutMeasurements) return true;
            }
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        // Save partial models from tabs
        getUI().getSurveyDetailsTabUI().getHandler().saveActualModel();
        getUI().getPhotosTabUI().getHandler().saveActualModel();

        // Save and control measurements tabs (Mantis #52401)
        getUI().getOperationMeasurementsTabUI().getHandler().save();

        // Save observation
        getContext().getObservationService().saveSurvey(getModel());

        // Clear some errors
        getModel().getSurveyDetailsTabUIModel().getUngroupedTableUIModel().getRows()
                .forEach(surveyMeasurementsUngroupedRowModel -> surveyMeasurementsUngroupedRowModel.setErrors(null));

        // Control observation
        controlMessages = getContext().getRulesControlService().controlSurvey(getModel(),
                false /*Do not set control date, if succeed */,
                true /*But reset control date if KO */);

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getModel().setModify(false);

        // show error messages
        showControlResult(controlMessages, false);

        getHandler().refreshModels();

    }

}
