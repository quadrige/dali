package fr.ifremer.dali.ui.swing.content.manage.program.strategies;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUIModel;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;

import java.util.List;

/**
 * Modele pour la zone des programmes.
 */
public class StrategiesTableUIModel extends AbstractDaliTableUIModel<StrategyDTO, StrategiesTableRowModel, StrategiesTableUIModel> {

    /** Constant <code>PROPERTY_LOADED="loaded"</code> */
    public static final String PROPERTY_LOADED = "loaded";
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";
    private static final long serialVersionUID = 3992314434806850926L;
    private boolean loaded;
    private boolean editable;
    private StatusDTO programStatus;
    private ProgramsUIModel parentModel;
    public static final String EVENT_SAVE_APPLIED_STRATEGIES = "saveAppliedStrategies";
    public static final String EVENT_SAVE_PMFM_STRATEGIES = "savePMFMStrategies";
    public static final String EVENT_VALIDATE_ROWS = "validateRows";
    public static final String EVENT_REMOVE_LOCATIONS = "removeLocations";

    /**
     * Constructor.
     */
    public StrategiesTableUIModel() {
        super();
    }

    /**
     * <p>isLoaded.</p>
     *
     * @return a boolean.
     */
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * <p>Setter for the field <code>loaded</code>.</p>
     *
     * @param loaded a boolean.
     */
    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
        firePropertyChange(PROPERTY_LOADED, null, loaded);
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }

    /**
     * <p>Getter for the field <code>programStatus</code>.</p>
     *
     * @return a {@link fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO} object.
     */
    public StatusDTO getProgramStatus() {
        return programStatus;
    }

    /**
     * <p>Setter for the field <code>programStatus</code>.</p>
     *
     * @param programStatus a {@link fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO} object.
     */
    public void setProgramStatus(StatusDTO programStatus) {
        this.programStatus = programStatus;
    }

    public ProgramsUIModel getParentModel() {
        return parentModel;
    }

    public void setParentModel(ProgramsUIModel parentModel) {
        this.parentModel = parentModel;
    }

    public void fireSaveAppliedStrategies() {
        firePropertyChange(EVENT_SAVE_APPLIED_STRATEGIES, null, null);
    }

    public void fireSavePmfmStrategies() {
        firePropertyChange(EVENT_SAVE_PMFM_STRATEGIES, null, null);
    }

    public void fireValidateRows() {
        firePropertyChange(EVENT_VALIDATE_ROWS, null, null);
    }

    public void fireRemoveLocations(List<Integer> locationIds) {
        firePropertyChange(EVENT_REMOVE_LOCATIONS, null, locationIds);
    }

    public void clear() {
        setBeans(null);
        setLoaded(false);
    }

}
