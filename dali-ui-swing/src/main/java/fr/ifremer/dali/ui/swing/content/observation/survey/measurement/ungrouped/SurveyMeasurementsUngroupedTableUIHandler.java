package fr.ifremer.dali.ui.swing.content.observation.survey.measurement.ungrouped;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.PmfmTableColumn;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SwingUtilities;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour le tableau du haut (Psfm) pour l onglet des mesures de l'observation.
 */
public class SurveyMeasurementsUngroupedTableUIHandler
        extends AbstractDaliTableUIHandler<SurveyMeasurementsUngroupedRowModel, SurveyMeasurementsUngroupedTableUIModel, SurveyMeasurementsUngroupedTableUI> {

    private static final int ROW_COUNT = 1;

    // editor for analyst column
    private ExtendedComboBoxCellEditor<DepartmentDTO> departmentCellEditor;


    /**
     * <p>Constructor for SurveyMeasurementsUngroupedTableUIHandler.</p>
     */
    public SurveyMeasurementsUngroupedTableUIHandler() {
        super(SurveyMeasurementsUngroupedRowModel.PROPERTY_PMFMS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyMeasurementsUngroupedTableModel getTableModel() {
        return (SurveyMeasurementsUngroupedTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final SurveyMeasurementsUngroupedTableUI ui) {
        super.beforeInit(ui);
        // create model and register to the JAXX context
        ui.setContextValue(new SurveyMeasurementsUngroupedTableUIModel());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final SurveyMeasurementsUngroupedTableUI ui) {

        // Initialiser l UI
        initUI(ui);
        createDepartmentCellEditor();
        updateDepartmentCellEditor(false);

        initTable();
        getTable().setVisibleRowCount(ROW_COUNT);

        SwingUtil.setLayerUI(ui.getTableScrollPane(), ui.getTableBlockLayer());
        // TODO mettre cette ligne si la scrollbar doit etre visible (et retirer setVisibleRowCount)
//        DaliUIs.forceComponentSize(ui.getTableauHautScrollPane(), 500, 70);

        // Initialiser listeners
        initListeners();
    }

    private void createDepartmentCellEditor() {

        departmentCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        departmentCellEditor.setAction("unfilter", "dali.common.unfilter", e -> {
            if (!askBefore(t("dali.common.unfilter"), t("dali.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateDepartmentCellEditor(true);
        });

    }

    private void updateDepartmentCellEditor(boolean forceNoFilter) {

        departmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        departmentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    /**
     * Initialiser les listeners
     */
    private void initListeners() {

        getModel().addPropertyChangeListener(SurveyMeasurementsUngroupedTableUIModel.PROPERTY_SURVEY, evt -> loadSurvey());

        getModel().addPropertyChangeListener(SurveyMeasurementsUngroupedTableUIModel.EVENT_MEASUREMENTS_LOADED, evt -> {
            // Detect read only measurement
            detectRealOnlyMeasurements();
        });

    }

    private void detectRealOnlyMeasurements() {

        Set<Integer> readOnlyPmfmIds = new HashSet<>();

        // Add Calculated length
        readOnlyPmfmIds.add(getConfig().getSurveyCalculatedLengthPmfmId());

        // Add calculated area (first pmfm id of each triplets)
        readOnlyPmfmIds.addAll(getConfig().getCalculatedTrawlAreaPmfmTriplets().stream().map(integers -> integers[0]).collect(Collectors.toList()));

        if (CollectionUtils.isNotEmpty(readOnlyPmfmIds)) {
            for (PmfmTableColumn column : getModel().getPmfmColumns()) {
                if (readOnlyPmfmIds.contains(column.getPmfmId())) {
                    column.setEditable(false);
                    column.getPmfmIdentifier().setNotMandatory(); // by precaution
                }
            }
        }

    }

    /**
     * Load survey.
     */
    private void loadSurvey() {

        SwingUtilities.invokeLater(() -> {

            // Uninstall save state listener
            uninstallSaveTableStateListener();

            addPmfmColumns(
                    getModel().getPmfms(),
                    SurveyDTO.PROPERTY_PMFMS,
                    DecoratorService.NAME_WITH_UNIT); // insert at the end

            boolean notEmpty = CollectionUtils.isNotEmpty(getModel().getPmfms());

            // tell the table model is editable or not
            getTableModel().setReadOnly(!getModel().getSurvey().isEditable());

            // Enable the table if pmfms non empty (Mantis #40984)
            getUI().getTableScrollPane().getParent().setVisible(notEmpty);
            getModel().setRows(null);

            if (notEmpty) {

                // affect row
                SurveyMeasurementsUngroupedRowModel row = getModel().addNewRow(getModel().getSurvey());

                // set analyst from first non null measurement
                Optional<MeasurementDTO> measurementFound = getModel().getSurvey().getMeasurements().stream().filter(measurement -> measurement.getAnalyst() != null).findFirst();
                if (measurementFound.isPresent()) {
                    row.setAnalyst(measurementFound.get().getAnalyst());
                } else {
                    // todo attention si on charge des mesures sans analyst (en base) on affiche qd meme l'analyste de la stratégie
                    row.setAnalyst(getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(getModel().getSurvey()));
                }

            }

            recomputeRowsValidState();

            // restore table from swing session
            restoreTableState();

            // hide analyst if no pmfm
//            forceColumnVisibleAtLastPosition(SurveyMeasurementsUngroupedTableModel.ANALYST, notEmpty);
            // Don't force position (Mantis #49939)
            forceColumnVisible(SurveyMeasurementsUngroupedTableModel.ANALYST, notEmpty);

            getModel().fireMeasurementsLoaded();

            // Install save state listener
            SwingUtilities.invokeLater(this::installSaveTableStateListener);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, SurveyMeasurementsUngroupedRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // no need to tell the table is modified if no pmfms (measurement) changes
        if (SurveyMeasurementsUngroupedRowModel.PROPERTY_PMFMS.equals(propertyName) && oldValue == newValue) {
            return;
        }

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        // update all measurements
        row.getMeasurements().forEach(measurement -> measurement.setAnalyst(row.getAnalyst()));

        // save modifications to parent model
        getModel().getSurvey().setMeasurements(row.getMeasurements());
        getModel().getSurvey().setDirty(true);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(SurveyMeasurementsUngroupedRowModel row) {
        return super.isRowValid(row) && isMeasurementRowValid(row);
    }

    private boolean isMeasurementRowValid(SurveyMeasurementsUngroupedRowModel row) {

        if (row.getAnalyst() == null &&
            row.getMeasurements().stream()
                .filter(measurement -> getModel().getSurvey().getPmfmsUnderMoratorium().stream().noneMatch(moratoriumPmfm -> DaliBeans.isPmfmEquals(measurement.getPmfm(), moratoriumPmfm)))
                .anyMatch(measurement -> !DaliBeans.isMeasurementEmpty(measurement))
        ) {
            DaliBeans.addError(row,
                    t("dali.validator.error.analyst.required"),
                    SurveyMeasurementsUngroupedRowModel.PROPERTY_ANALYST);
            return false;
        }
        return true;
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // La table des mesures
        final SwingTable table = getTable();

        // Add analyst column (Mantis #42617)
        TableColumnExt analystColumn = addColumn(
                departmentCellEditor,
                newTableCellRender(SurveyMeasurementsUngroupedTableModel.ANALYST),
                SurveyMeasurementsUngroupedTableModel.ANALYST
        );
        analystColumn.setMinWidth(100);

        // Modele de la table
        SurveyMeasurementsUngroupedTableModel tableModel = new SurveyMeasurementsUngroupedTableModel(getTable().getColumnModel());
        tableModel.setNoneEditableCols();
        table.setModel(tableModel);

        // Initialisation de la table
        initTable(table, true);

        // border
        addEditionPanelBorder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getSurveyUngroupedMeasurementTable();
    }
}
