package fr.ifremer.dali.ui.swing.content.manage.program.locations.updatePeriod;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.JComponent;
import java.time.LocalDate;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour l'écran principal.
 */
public class UpdatePeriodUIHandler extends AbstractDaliUIHandler<UpdatePeriodUIModel, UpdatePeriodUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(UpdatePeriodUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new UpdatePeriodUIModel());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(UpdatePeriodUI ui) {
        initUI(ui);

        // Chargement de la combo
        initBeanFilterableComboBox(
                getUI().getSamplerCombo(),
                getContext().getReferentialService().getDepartments(StatusFilter.ACTIVE),
                null);

        getUI().getSamplerCombo().setShowReset(true);
        getUI().getSamplerCombo().setShowDecorator(false);

        initBeanFilterableComboBox(
                getUI().getAnalystCombo(),
                getContext().getReferentialService().getDepartments(StatusFilter.ACTIVE),
                null);

        getUI().getAnalystCombo().setShowReset(true);
        getUI().getAnalystCombo().setShowDecorator(false);

    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // Recuperation des saisies
        final LocalDate startDate = getUI().getStartDateEditor().getLocalDate();
        final LocalDate endDate = getUI().getEndDateEditor().getLocalDate();
        final DepartmentDTO samplingDepartment = (DepartmentDTO) getUI().getSamplerCombo().getSelectedItem();
        final DepartmentDTO analystDepartment = (DepartmentDTO) getUI().getAnalystCombo().getSelectedItem();

        if (startDate == null ^ endDate == null) {

            // La date de debut doit être renseignee
            getContext().getDialogHelper().showErrorDialog(getUI(),
                    t("dali.program.location.periods.error.date.message"),
                    t("dali.program.location.periods.error.titre"));

        } else if (startDate != null && endDate != null && endDate.isBefore(startDate)) {

            // la date de fin ne pas etre antérieur au début
            getContext().getDialogHelper().showErrorDialog(getUI(),
                    t("dali.program.location.periods.error.date.before.message"),
                    t("dali.program.location.periods.error.titre"));

        } else {

            // Ajout des elements dans les lieux selectionnes
            for (final AppliedStrategyDTO lieu : getModel().getTableModel().getSelectedRows()) {
                lieu.setStartDate(startDate);
                lieu.setEndDate(endDate);
                lieu.setSamplingDepartment(samplingDepartment);
                lieu.setAnalysisDepartment(analystDepartment);
            }

            // Quitter la dialogue
            closeDialog();
        }
    }

    /** {@inheritDoc} */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getStartDateEditor();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
