package fr.ifremer.dali.ui.swing.content.observation.survey.history;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.QualityLevelDTO;
import fr.ifremer.dali.dto.system.QualificationHistoryDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * @author peck7 on 26/09/2017.
 */
public class QualificationHistoryTableModel extends AbstractDaliTableModel<QualificationHistoryRowModel> {

    public static final DaliColumnIdentifier<QualificationHistoryRowModel> DATE = DaliColumnIdentifier.newId(
            QualificationHistoryDTO.PROPERTY_DATE,
            n("dali.property.date.qualification"),
            n("dali.property.date.qualification"),
            Date.class
    );

    public static final DaliColumnIdentifier<QualificationHistoryRowModel> COMMENT = DaliColumnIdentifier.newId(
            QualificationHistoryDTO.PROPERTY_COMMENT,
            n("dali.property.comment.qualification"),
            n("dali.property.comment.qualification"),
            String.class
    );

    public static final DaliColumnIdentifier<QualificationHistoryRowModel> QUALITY_LEVEL = DaliColumnIdentifier.newId(
            QualificationHistoryDTO.PROPERTY_QUALITY_LEVEL,
            n("dali.property.qualityFlag"),
            n("dali.property.qualityFlag"),
            QualityLevelDTO.class
    );

    public static final DaliColumnIdentifier<QualificationHistoryRowModel> RECORDER_PERSON = DaliColumnIdentifier.newId(
            QualificationHistoryDTO.PROPERTY_RECORDER_PERSON,
            n("dali.property.user.recorder"),
            n("dali.property.user.recorder"),
            PersonDTO.class
    );

    /**
     * <p>Constructor for AbstractDaliTableModel.</p>
     *
     */
    public QualificationHistoryTableModel(SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    @Override
    public DaliColumnIdentifier<QualificationHistoryRowModel> getFirstColumnEditing() {
        return null;
    }

    @Override
    public QualificationHistoryRowModel createNewRow() {
        return new QualificationHistoryRowModel();
    }
}
