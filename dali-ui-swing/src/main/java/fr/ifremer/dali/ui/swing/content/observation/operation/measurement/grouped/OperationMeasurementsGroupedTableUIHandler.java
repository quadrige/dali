package fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid.InitGridUI;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid.InitGridUIModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.initGrid.PresetFileLoader;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.multiedit.OperationMeasurementsMultiEditUI;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.multiedit.OperationMeasurementsMultiEditUIModel;
import fr.ifremer.dali.ui.swing.content.observation.operation.measurement.grouped.shared.AbstractOperationMeasurementsGroupedTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.dali.vo.PresetVO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.action.AdditionalTableActions;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.RowFilter;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Controleur pour les mesures des prelevements (ecran prelevements/mesure).
 */
public class OperationMeasurementsGroupedTableUIHandler
        extends AbstractOperationMeasurementsGroupedTableUIHandler<OperationMeasurementsGroupedRowModel, OperationMeasurementsGroupedTableUIModel, OperationMeasurementsGroupedTableUI> {

    private static final Log LOG = LogFactory.getLog(OperationMeasurementsGroupedTableUIHandler.class);

    /**
     * <p>Constructor for OperationMeasurementsGroupedTableUIHandler.</p>
     */
    public OperationMeasurementsGroupedTableUIHandler() {
        super(OperationMeasurementsGroupedRowModel.PROPERTY_SAMPLING_OPERATION,
                OperationMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP,
                OperationMeasurementsGroupedRowModel.PROPERTY_TAXON,
                OperationMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS,
                OperationMeasurementsGroupedRowModel.PROPERTY_COMMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationMeasurementsGroupedTableModel getTableModel() {
        return (OperationMeasurementsGroupedTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getOperationGroupedMeasurementTable();
    }

    @Override
    protected OperationMeasurementsGroupedTableUIModel createNewModel() {
        return new OperationMeasurementsGroupedTableUIModel();
    }

    @Override
    protected OperationMeasurementsGroupedRowModel createNewRow(boolean readOnly, SamplingOperationDTO parentBean) {
        OperationMeasurementsGroupedRowModel row = new OperationMeasurementsGroupedRowModel(readOnly);
        row.setSamplingOperation(parentBean);
        return row;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final OperationMeasurementsGroupedTableUI ui) {

        super.afterInit(ui);

        SwingUtil.setLayerUI(ui.getTableauBasScrollPane(), ui.getTableBlockLayer());

        initActionComboBox(ui.getInitDataGridComboBox());

        // Les boutons sont desactives
        getUI().getDeleteButton().setEnabled(false);
        getUI().getDuplicateButton().setEnabled(false);

    }

    /**
     * Initialiser les listeners
     */
    protected void initListeners() {
        super.initListeners();

        getModel().addPropertyChangeListener(evt -> {

            if (AbstractDaliTableUIModel.PROPERTY_ROWS.equals(evt.getPropertyName())) {// update some controls
                getUI().processDataBinding(OperationMeasurementsGroupedTableUI.BINDING_INIT_DATA_GRID_COMBO_BOX_ENABLED);
            }
        });

    }

    protected void filterMeasurements() {

        // sampling operation column should not be editable if only one sampling operation
        getTable().getColumnExt(OperationMeasurementsGroupedTableModel.SAMPLING).setEditable(getModel().getSamplingFilter() == null);
//        getTable().getColumnExt(OperationMeasurementsGroupedTableModel.TAXON_GROUP).setEditable(getModel().getTaxonGroupFilter() == null);
//        getTable().getColumnExt(OperationMeasurementsGroupedTableModel.TAXON).setEditable(getModel().getTaxonFilter() == null);

        if (getModel().getSamplingFilter() == null /*&& getModel().getTaxonGroupFilter() == null && getModel().getTaxonFilter() == null*/) {
            // remove filter
            getTable().setRowFilter(null);
        } else {
            // add filter on sampling operation (for the moment)
            getTable().setRowFilter(new OperationRowFilter(getTable().getColumnExt(OperationMeasurementsGroupedTableModel.SAMPLING).getModelIndex()));
        }

        getUI().applyDataBinding(OperationMeasurementsGroupedTableUI.BINDING_INIT_DATA_GRID_COMBO_BOX_ENABLED);
    }

    class OperationRowFilter extends RowFilter<OperationMeasurementsGroupedTableModel, Integer> {

        private final int samplingModelIndex;

        OperationRowFilter(int samplingModelIndex) {
            this.samplingModelIndex = samplingModelIndex;
        }

        @Override
        public boolean include(Entry<? extends OperationMeasurementsGroupedTableModel, ? extends Integer> entry) {
            return (getModel().getSamplingFilter() == null
                || getModel().getSamplingFilter().equals(entry.getValue(samplingModelIndex)))
                            /*&& (getModel().getTaxonGroupFilter() == null
                            || getModel().getTaxonGroupFilter().equals(taxonGroupModelIndex)))
                            && (getModel().getTaxonFilter() == null
                            || getModel().getTaxonFilter().equals(entry.getValue(taxonModelIndex)))*/;
        }
    }

    /**
     * Initialisation du tableau.
     */
    protected void initTable() {

        // La table des prelevements
        final SwingTable table = getTable();

        // Colonne mnemonique
        samplingOperationCellEditor = newExtendedComboBoxCellEditor(new ArrayList<>(), SamplingOperationDTO.class, false);
        final TableColumnExt colName = addFixedColumn(
                samplingOperationCellEditor,
                newTableCellRender(OperationMeasurementsGroupedTableModel.SAMPLING),
                OperationMeasurementsGroupedTableModel.SAMPLING);
        colName.setCellEditor(samplingOperationCellEditor);
        colName.setSortable(true);
        colName.setMinWidth(100);

        if (getConfig().isDebugMode()) {
            TableColumnExt indivIdCol = addColumn(OperationMeasurementsGroupedTableModel.INDIVIDUAL_ID);
            indivIdCol.setSortable(true);
            setDefaultColumnMinWidth(indivIdCol);
        }

        // Colonne groupe taxon
        final TableColumnExt colTaxonGroup = addColumn(
                taxonGroupCellEditor,
                newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON_GROUP),
                OperationMeasurementsGroupedTableModel.TAXON_GROUP);
        colTaxonGroup.setSortable(true);
        setDefaultColumnMinWidth(colTaxonGroup);

        // Colonne taxon
        final TableColumnExt colTaxon = addColumn(
                taxonCellEditor,
                newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON),
                OperationMeasurementsGroupedTableModel.TAXON);
        colTaxon.setSortable(true);
        setDefaultColumnMinWidth(colTaxon);

        // Colonne taxon saisi
        final TableColumnExt colInputTaxon = addColumn(OperationMeasurementsGroupedTableModel.INPUT_TAXON_NAME);
        colInputTaxon.setSortable(true);
        colInputTaxon.setEditable(false);
        setDefaultColumnMinWidth(colInputTaxon);

        // Colonne analyste
        final TableColumnExt colAnalyst = addColumn(
                departmentCellEditor,
                newTableCellRender(OperationMeasurementsGroupedTableModel.ANALYST),
                OperationMeasurementsGroupedTableModel.ANALYST);
        colAnalyst.setSortable(true);
        setDefaultColumnMinWidth(colAnalyst);

        final TableColumnExt colAnalysisInstrument = addColumn(
                analysisInstrumentCellEditor,
                newTableCellRender(OperationMeasurementsGroupedTableModel.ANALYSIS_INSTRUMENT),
                OperationMeasurementsGroupedTableModel.ANALYSIS_INSTRUMENT);
        colAnalysisInstrument.setSortable(true);
        setDefaultColumnMinWidth(colAnalysisInstrument);

        // Colonne commentaire
        TableColumnExt colComment = addCommentColumn(OperationMeasurementsGroupedTableModel.COMMENT);
        setDefaultColumnMinWidth(colComment);

        // Modele de la table
        final OperationMeasurementsGroupedTableModel tableModel = new OperationMeasurementsGroupedTableModel(getTable().getColumnModel(), true);
        table.setModel(tableModel);

        // Add numeric total
        addColumnTotalAction();

        // Initialisation de la table
        initTable(table);

        colTaxonGroup.setVisible(false);
        colTaxon.setVisible(false);
        colInputTaxon.setVisible(false);
        colComment.setVisible(false);
        colAnalyst.setVisible(false);
        colAnalysisInstrument.setVisible(false);

        // border
        addEditionPanelBorder(JPanel.class);
    }

    private void addColumnTotalAction() {

        OperationMeasurementsTotalFooterAction action = new OperationMeasurementsTotalFooterAction(this);
        action.putValue(AdditionalTableActions.ACTION_TARGET_GROUP, 0);
        getAdditionalTableActions().addAction(action);

    }

    /**
     * Initialize the data grid
     */
    void initializeDataGrid(boolean configure) {

        boolean initOK = true;
        String programCode = getModel().getSurvey().getProgram().getCode();
        PresetVO preset = PresetFileLoader.load(programCode);

        List<PmfmDTO> availablePmfms = DaliBeans.filterCollection(getModel().getPmfms(), input -> input.getParameter().isQualitative());

        if (preset != null) {

            // validate integrity
            Map<Integer, PmfmDTO> pmfmByIds = DaliBeans.mapById(availablePmfms);

            for (Integer pmfmPresetId : preset.getPmfmIds()) {
                PmfmDTO pmfm = pmfmByIds.get(pmfmPresetId);
                if (pmfm == null) {
                    initOK = false;
                    break;
                } else {
                    List<Integer> qualitativeValueIds = DaliBeans.collectIds(pmfm.getQualitativeValues());
                    if (!qualitativeValueIds.containsAll(preset.getQualitativeValueIds(pmfmPresetId))) {
                        initOK = false;
                        break;
                    }
                }
            }

        } else {

            // create new preset
            preset = new PresetVO(programCode);
            initOK = false;
        }

        if (configure || !initOK) {

            InitGridUI initGridUI = new InitGridUI(getContext());
            InitGridUIModel initModel = initGridUI.getModel();

            // Set available pmfms
            initModel.setAvailablePmfms(availablePmfms);
            // Set presets if exists, AFTER available pmfms
            initModel.setPreset(preset);

            openDialog(initGridUI, new Dimension(1000, (int) (getContext().getMainUI().getHeight() * 0.95)));

            preset = initModel.getPreset();
            initOK = initModel.isValid();

        }

        if (initOK) {

            getModel().setLoading(true);
            GridInitializer gridInitializer = new GridInitializer(this, preset);
            gridInitializer.execute();

        }
    }

    public void editSelectedMeasurements() {

        OperationMeasurementsMultiEditUI multiEditUI = new OperationMeasurementsMultiEditUI(getUI());

        // add the calculated transition pmfm as read-only
//        editUI.getModel().setReadOnlyPmfmIds(
//            Stream.of(
//                getModel().getPitTransitionLengthPmfmId(),
//                getModel().getPitTransectLengthPmfmId(),
//                getModel().getPitTransectOriginPmfmId(),
//                getModel().getCalculatedLengthEndPositionPmfmId(),
//                getModel().getCalculatedLengthTransitionPmfmId()
//            ).filter(Objects::nonNull).collect(Collectors.toList())
//        );

        // save current table state to be able to restore it in dialog's table
        saveTableState();

//        Assert.isInstanceOf(DaliUI.class, dialog);
//        DaliUI<?, ?> multiEditUI = (DaliUI<?, ?>) dialog;
//        Assert.isInstanceOf(OperationMeasurementsMultiEditUIModel.class, multiEditUI.getModel());
        OperationMeasurementsMultiEditUIModel multiEditModel = multiEditUI.getModel();
        multiEditModel.setObservationHandler(getModel().getObservationUIHandler());
        multiEditModel.setRowsToEdit(getModel().getSelectedRows());
        multiEditModel.setPmfms(getModel().getPmfms());
        multiEditModel.setSurvey(getModel().getSurvey());

        // get parent component for dialog centering
        JComponent parent = getUI().getParentContainer(getTable(), JScrollPane.class);
        Insets borderInsets = parent.getBorder() != null ? parent.getBorder().getBorderInsets(parent) : new Insets(0,5,0,5);
        openDialog(multiEditUI, new Dimension(parent.getWidth() + borderInsets.left + borderInsets.right, 160), true, parent);

        // if dialog is valid
        if (multiEditModel.isValid()) {

            // affect all non multiple column values to selected rows
            OperationMeasurementsGroupedRowModel multiEditRow = multiEditModel.getRows().get(0);

            for (OperationMeasurementsGroupedRowModel row : getModel().getSelectedRows()) {

                // affect column values
                for (DaliColumnIdentifier<OperationMeasurementsGroupedRowModel> identifier : multiEditModel.getIdentifiersToCheck()) {
                    // be sure that sampling operation value can not be changed
                    if (identifier.getPropertyName().equals(OperationMeasurementsGroupedRowModel.PROPERTY_SAMPLING_OPERATION))
                        continue;

                    // get values
                    Object multiValue = identifier.getValue(multiEditRow);
                    Object currentValue = identifier.getValue(row);
                    boolean isMultiple = multiEditRow.getMultipleValuesOnIdentifier().contains(identifier);
                    // affect value if a multiple value has been replaced, or modification has been made
                    if ((!isMultiple || multiValue != null) && !Objects.equals(multiValue, currentValue)) {
                        identifier.setValue(row, multiValue);
                        // special case for INPUT_TAXON_NAME
                        if (identifier.getPropertyName().equals(OperationMeasurementsGroupedRowModel.PROPERTY_INPUT_TAXON_NAME)) {
                            // affect also INPUT_TAXON_ID
                            row.setInputTaxonId(multiEditRow.getInputTaxonId());
                        }
                    }
                }

                // for measurement columns
                for (PmfmDTO pmfm : getModel().getPmfms()) {
                    MeasurementDTO multiMeasurement = DaliBeans.getIndividualMeasurementByPmfmId(multiEditRow, pmfm.getId());
                    if (multiMeasurement == null) {
                        // can happened if user pass on on a cel without setting a value
                        multiMeasurement = DaliBeanFactory.newMeasurementDTO();
                    }
                    boolean isMultiple = multiEditRow.getMultipleValuesOnPmfmIds().contains(pmfm.getId());
                    // affect value if a multiple value has been replaced, or modification has been made
                    if (!isMultiple || !DaliBeans.isMeasurementEmpty(multiMeasurement)) {
                        MeasurementDTO measurement = DaliBeans.getIndividualMeasurementByPmfmId(row, pmfm.getId());
                        if (measurement == null) {
                            // create and add the measurement if not exists
                            measurement = DaliBeanFactory.newMeasurementDTO();
                            measurement.setPmfm(pmfm);
                            measurement.setIndividualId(row.getIndividualId());
                            row.getIndividualMeasurements().add(measurement);
                        }
                        // affect value (either numeric or qualitative)
                        if (!DaliBeans.measurementValuesEquals(multiMeasurement, measurement)) {
                            measurement.setNumericalValue(multiMeasurement.getNumericalValue());
                            measurement.setQualitativeValue(multiMeasurement.getQualitativeValue());
                        }
                    }
                }

            }

            // done
            getModel().setModify(true);
            getTable().repaint();
        }

    }

}
