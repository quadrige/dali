package fr.ifremer.dali.ui.swing.content.home.survey.qualify;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.ui.swing.util.AbstractDaliUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.nuiton.jaxx.application.swing.util.Cancelable;

/**
 * @author peck7 on 26/09/2017.
 */
public class QualifySurveyUIHandler extends AbstractDaliUIHandler<QualifySurveyUIModel, QualifySurveyUI> implements Cancelable {

    @Override
    public void beforeInit(QualifySurveyUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new QualifySurveyUIModel());
    }

    @Override
    public void afterInit(QualifySurveyUI qualifySurveyUI) {

        initUI(qualifySurveyUI);

        initBeanFilterableComboBox(getUI().getQualityLevelCombo(),
                getContext().getObservationService().getQualityLevels(),
                null);

        listenValidatorValid(getValidator(), getModel());

    }

    @Override
    public SwingValidator<QualifySurveyUIModel> getValidator() {
        return getUI().getValidator();
    }

    public void qualify() {
        closeDialog();
    }

    @Override
    public void cancel() {
        stopListenValidatorValid(getValidator());
        getModel().setValid(false);
        closeDialog();
    }
}
