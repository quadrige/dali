package fr.ifremer.dali.ui.swing.content.manage.referential.taxongroup.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Modele du menu pour la gestion des groupeTaxons au niveau National
 */
public class TaxonGroupMenuUIModel extends AbstractReferentialMenuUIModel<TaxonGroupCriteriaDTO, TaxonGroupMenuUIModel> implements TaxonGroupCriteriaDTO {

    private static final Binder<TaxonGroupMenuUIModel, TaxonGroupCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(TaxonGroupMenuUIModel.class, TaxonGroupCriteriaDTO.class);

    private static final Binder<TaxonGroupCriteriaDTO, TaxonGroupMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(TaxonGroupCriteriaDTO.class, TaxonGroupMenuUIModel.class);

    /**
     * Constructor.
     */
    public TaxonGroupMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected TaxonGroupCriteriaDTO newBean() {
        return DaliBeanFactory.newTaxonGroupCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getLabel() {
        return delegateObject.getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLabel(String label) {
        delegateObject.setLabel(label);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonGroupDTO getParentTaxonGroup() {
        return delegateObject.getParentTaxonGroup();
    }

    /** {@inheritDoc} */
    @Override
    public void setParentTaxonGroup(TaxonGroupDTO parentTaxonGroup) {
        delegateObject.setParentTaxonGroup(parentTaxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setLabel(null);
        setParentTaxonGroup(null);
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void setDirty(boolean dirty) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }
}
