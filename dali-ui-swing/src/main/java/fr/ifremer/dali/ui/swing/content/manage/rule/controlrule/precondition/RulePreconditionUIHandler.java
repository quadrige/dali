package fr.ifremer.dali.ui.swing.content.manage.rule.controlrule.precondition;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePreconditionUIHandler
        extends AbstractDaliTableUIHandler<RulePreconditionRowModel, RulePreconditionUIModel, RulePreconditionUI>
        implements Cancelable {

    private static final Log LOG = LogFactory.getLog(RulePreconditionUIHandler.class);
    public static final String LIST = "list";
    public static final String DOUBLE_LIST = "doubleList";

    @Override
    public void beforeInit(RulePreconditionUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new RulePreconditionUIModel());
    }

    @Override
    public void afterInit(RulePreconditionUI rulePreconditionUI) {
        initUI(rulePreconditionUI);

        initTable();

        // adjust double list size (Mantis #50643)
        initBeanList(rulePreconditionUI.getUsedRuleQVDoubleList(), null, new ArrayList<>(), 100);
        rulePreconditionUI.getUsedRuleQVDoubleList().getHandler().addAdditionalControls();

        rulePreconditionUI.getUsedRuleQVList().setCellRenderer(newListCellRender(QualitativeValueDTO.class));

        initListeners();

        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());
    }

    private void initTable() {

        TableColumnExt nameCol = addColumn(RulePreconditionTableModel.NAME);
        nameCol.setSortable(true);

        RulePreconditionTableModel tableModel = new RulePreconditionTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        initTable(getTable(), true);

        getTable().setSortOrder(RulePreconditionTableModel.NAME, SortOrder.ASCENDING);
        getTable().setHorizontalScrollEnabled(false);
        getTable().setEditable(false);
    }

    private void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {

                case RulePreconditionUIModel.PROPERTY_BASE_PMFM:
                    initBasePmfm();
                    break;

                case RulePreconditionUIModel.PROPERTY_USED_PMFM:
                    initUsedPmfm();
                    break;

                case RulePreconditionUIModel.PROPERTY_SINGLE_ROW_SELECTED:
                    updateLists();
                    break;
            }
        });
    }

    private void initBasePmfm() {
        PmfmDTO basePmfm = getModel().getBasePmfm();
        if (basePmfm == null) {
            LOG.error("base PMFM should be set");
            return;
        }
        ((TitledBorder) getUI().getBaseRulePanel().getBorder()).setTitle(decorate(basePmfm, DecoratorService.NAME));
        getUI().getBaseRulePanel().setToolTipText(decorate(basePmfm));

        // load real qualitative values
        getModel().setBeans(getContext().getReferentialService().getUniquePmfmFromPmfm(basePmfm).getQualitativeValues());

        // select first line
        if (getModel().getRowCount() > 0)
            SwingUtilities.invokeLater(() -> selectCell(0, null));
    }

    private void initUsedPmfm() {
        PmfmDTO usedPmfm = getModel().getUsedPmfm();
        if (usedPmfm == null) {
            LOG.error("used PMFM should be set");
            return;
        }
        ((TitledBorder) getUI().getUsedRulePanel().getBorder()).setTitle(decorate(usedPmfm, DecoratorService.NAME));
        getUI().getUsedRulePanel().setToolTipText(decorate(usedPmfm));

        // load real qualitative values
        getUI().getUsedRuleQVDoubleList().getHandler().setUniverse(
                getContext().getReferentialService().getUniquePmfmFromPmfm(usedPmfm).getQualitativeValues()
        );
    }

    private void updateLists() {

        if (getModel().getSingleSelectedRow() != null) {
            getModel().setAdjusting(true);
            Collection<QualitativeValueDTO> qualitativeValues = getModel().getQvMap().get(getModel().getSingleSelectedRow().toBean());
            getUI().getUsedRuleQVDoubleList().getHandler().setSelected(
                    new ArrayList<>(qualitativeValues)
            );
            getUI().getUsedRuleQVList().setListData(qualitativeValues.toArray(new QualitativeValueDTO[0]));
            getModel().setAdjusting(false);
        }
    }

    @Override
    public SwingValidator<RulePreconditionUIModel> getValidator() {
        return getUI().getValidator();
    }

    public void setEnabled(boolean enabled) {
        if (enabled)
            getUI().getSwitchPanelLayout().setSelected(DOUBLE_LIST);
        else
            getUI().getSwitchPanelLayout().setSelected(LIST);

        getUI().getValidButton().setEnabled(enabled);
    }

    public void valid() {
        if (getModel().isValid()) {
            stopListenValidatorValid(getValidator());
            closeDialog();
        }
    }

    @Override
    public void cancel() {
        getModel().setModify(false);
//        getModel().setValid(false);
        stopListenValidatorValid(getValidator());
        closeDialog();
    }

    @Override
    public AbstractTableModel<RulePreconditionRowModel> getTableModel() {
        return (RulePreconditionTableModel) getTable().getModel();
    }

    @Override
    public SwingTable getTable() {
        return getUI().getBaseRuleQVTable();
    }
}
