package fr.ifremer.dali.ui.swing.content.manage.program.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.dali.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.dali.ui.swing.util.DaliUIs;
import jaxx.runtime.swing.model.WillChangeSelectedItemListener;

import javax.swing.SwingUtilities;
import java.util.List;

/**
 * Controlleur du menu de l'onglet prelevements mesures.
 */
public class ProgramsMenuUIHandler extends ReferentialMenuUIHandler<ProgramsMenuUIModel, ProgramsMenuUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ProgramsMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ProgramsMenuUIModel model = new ProgramsMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ProgramsMenuUI ui) {
        super.afterInit(ui);

        // Init the combobox
        initComboBox();

    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getProgramCodeCombo().setEnabled(enabled);
        getUI().getProgramMnemonicCombo().setEnabled(enabled);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllProgramFilter();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        List<ProgramDTO> programs = getContext().getProgramStrategyService().getReadablePrograms();

        // Init programs
        initBeanFilterableComboBox(
                getUI().getProgramMnemonicCombo(),
                programs,
                null,
                DecoratorService.NAME);

        initBeanFilterableComboBox(
                getUI().getProgramCodeCombo(),
                programs,
                null,
                DecoratorService.CODE);

        // Minimal size for comboBox
        DaliUIs.forceComponentSize(getUI().getProgramMnemonicCombo());
        DaliUIs.forceComponentSize(getUI().getProgramCodeCombo());

        WillChangeSelectedItemListener listener = event -> {
            if (getModel().isLoading()) return;
            if (event.getNextSelectedItem() != null) {
                SwingUtilities.invokeLater(() -> getUI().getSearchButton().getAction().actionPerformed(null));
            }
        };
        getUI().getProgramMnemonicCombo().getComboBoxModel().addWillChangeSelectedItemListener(listener);
        getUI().getProgramCodeCombo().getComboBoxModel().addWillChangeSelectedItemListener(listener);
    }

    /**
     * <p>getProgramsUI.</p>
     *
     * @return a {@link fr.ifremer.dali.ui.swing.content.manage.program.ProgramsUI} object.
     */
    public ProgramsUI getProgramsUI() {
        return getUI().getParentContainer(ProgramsUI.class);
    }

}
