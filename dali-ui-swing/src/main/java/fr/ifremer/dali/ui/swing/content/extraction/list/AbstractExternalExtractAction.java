package fr.ifremer.dali.ui.swing.content.extraction.list;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.ui.swing.content.extraction.list.external.ExternalChooseUI;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.ZipUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.awt.Dimension;
import java.io.File;
import java.util.Date;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Abstract extract action
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public abstract class AbstractExternalExtractAction extends AbstractExtractAction {

    private static final Log LOG = LogFactory.getLog(AbstractExternalExtractAction.class);
    private static final int MAX_FILE_NAME_LENGTH = 255;

    private String fileName;
    private String email;

    /**
     * Constructor.
     *
     * @param handler Handler
     */
    protected AbstractExternalExtractAction(ExtractionsTableUIHandler handler) {
        super(handler);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if(!getOutputType().isExternal()){
            LOG.error("wrong ExtractionOutputType (should be external");
            return false;
        }

        String tempFileName = String.format("%s-%s-%s-%s",
                t("dali.service.extraction.file.prefix"),
                getOutputType().getLabel(),
                getSelectedExtraction().getName(),
                DateUtil.formatDate(new Date(), "yyyy-MM-dd-HHmmss"));

        // ask for email& file name confirmation
        ExternalChooseUI externalChooseUI = new ExternalChooseUI(getContext());
        externalChooseUI.getModel().setEmail(StringUtils.isEmpty(getSelectedExtraction().getUser().getEmail()) ? "" : getSelectedExtraction().getUser().getEmail().toLowerCase());
        externalChooseUI.getModel().setFileName(tempFileName);
        getHandler().openDialog(externalChooseUI, new Dimension(500, 150));

        if (!externalChooseUI.getModel().isValid()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("extraction aborted");
            }
            return false;
        }

        email = externalChooseUI.getModel().getEmail().toLowerCase().trim();
        String securedName = DaliBeans.toSecuredString(externalChooseUI.getModel().getFileName()).trim();
        fileName = String.format("%s#%s", email, securedName);

        // trim to fit 255 characters maximum
        if (fileName.length() > MAX_FILE_NAME_LENGTH) {
            fileName = String.format("%s#%s", email, securedName.substring(0, securedName.length() - MAX_FILE_NAME_LENGTH - fileName.length()));
        }
        Assert.isTrue(fileName.length() <= 255);

        createProgressionUIModel();

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        // Will check data updates
        super.doAction();

        // create the extraction file in temp dir
        File tempDir = getConfig().getNewTmpDirectory("extraction");
        File extractionDir = new File(tempDir, fileName);
        extractionDir.mkdirs();
        setOutputFile(new File(extractionDir, fileName + "." + getConfig().getExtractionResultFileExtension()));

        // Perform
        performExtraction();

        // zip files
        File zipFile = new File(tempDir, fileName + ".zip");
        ZipUtils.compressFilesInPath(extractionDir.toPath(), zipFile.toPath(), true);

        // export zip file
        getProgressionUIModel().setMessage(t("dali.extraction.external.upload.message"));

        DaliServiceLocator.instance().getExtractionRestClientService().uploadExtractionFile(
                getContext().getAuthenticationInfo(),
                zipFile, getOutputType().toString(),
                getProgressionUIModel());

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        String text = t("dali.action.external.extract.done",
                decorate(Objects.requireNonNull(getSelectedExtraction())),
                getOutputType().getLabel(),
                email
                );

        getContext().getDialogHelper().showMessageDialog(text, t("dali.action.extract.title", getOutputType().getLabel()));

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        fileName = null;
        email = null;
        super.releaseAction();
    }
}
