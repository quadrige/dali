package fr.ifremer.dali.ui.swing.content.manage.referential.pmfm.method.menu;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.configuration.filter.FilterCriteriaDTO;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.dali.dto.referential.pmfm.MethodDTO;
import fr.ifremer.dali.ui.swing.util.AbstractDaliBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;
import java.util.List;

/**
 * Modele du menu pour la gestion des Methods au niveau local
 */
public class ManageMethodsMenuUIModel extends AbstractDaliBeanUIModel<FilterCriteriaDTO, ManageMethodsMenuUIModel> implements FilterCriteriaDTO {

    /** Constant <code>PROPERTY_METHOD="method"</code> */
    public static final String PROPERTY_METHOD = "method";
    private static final Binder<ManageMethodsMenuUIModel, FilterCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ManageMethodsMenuUIModel.class, FilterCriteriaDTO.class);
    private static final Binder<FilterCriteriaDTO, ManageMethodsMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(FilterCriteriaDTO.class, ManageMethodsMenuUIModel.class);
    private MethodDTO method;

    /**
     * <p>Constructor for ManageMethodsMenuUIModel.</p>
     */
    public ManageMethodsMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /**
     * <p>getStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStatusCode() {
        return getStatus() == null ? null : getStatus().getCode();
    }

    /**
     * <p>getMethodId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getMethodId() {
        return getMethod() == null ? null : getMethod().getId();
    }

    /**
     * <p>Getter for the field <code>method</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.MethodDTO} object.
     */
    public MethodDTO getMethod() {
        return method;
    }

    /**
     * <p>Setter for the field <code>method</code>.</p>
     *
     * @param method a {@link fr.ifremer.dali.dto.referential.pmfm.MethodDTO} object.
     */
    public void setMethod(MethodDTO method) {
        this.method = method;
        firePropertyChange(PROPERTY_METHOD, null, method);
    }

    /** {@inheritDoc} */
    @Override
    public List<? extends QuadrigeBean> getResults() {
        return delegateObject.getResults();
    }

    /** {@inheritDoc} */
    @Override
    public void setResults(List<? extends QuadrigeBean> results) {
        delegateObject.setResults(results);
    }

    /** {@inheritDoc} */
    @Override
    protected FilterCriteriaDTO newBean() {
        return DaliBeanFactory.newFilterCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public void setReadOnly(boolean b) {

    }

    @Override
    public Date getCreationDate() {
        return null;
    }

    @Override
    public void setCreationDate(Date date) {

    }

    @Override
    public Date getUpdateDate() {
        return null;
    }

    @Override
    public void setUpdateDate(Date date) {

    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isStrictName() {
        return delegateObject.isStrictName();
    }

    /** {@inheritDoc} */
    @Override
    public void setStrictName(boolean strictName) {
        delegateObject.setStrictName(strictName);
    }

}
