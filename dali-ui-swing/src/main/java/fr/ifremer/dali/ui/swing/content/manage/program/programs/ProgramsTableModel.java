package fr.ifremer.dali.ui.swing.content.manage.program.programs;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class ProgramsTableModel extends AbstractDaliTableModel<ProgramsTableRowModel> {

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final DaliColumnIdentifier<ProgramsTableRowModel> NAME = DaliColumnIdentifier.newId(
    		ProgramsTableRowModel.PROPERTY_NAME,
            n("dali.property.name"),
            n("dali.program.program.name.tip"),
            String.class,
            true);

    /**
     * Identifiant pour la colonne description.
     */
    public static final DaliColumnIdentifier<ProgramsTableRowModel> DESCRIPTION = DaliColumnIdentifier.newId(
    		ProgramsTableRowModel.PROPERTY_DESCRIPTION,
            n("dali.property.description"),
            n("dali.program.program.description.tip"),
            String.class,
            true);

    /**
     * Identifiant pour la colonne code.
     */
    public static final DaliColumnIdentifier<ProgramsTableRowModel> CODE = DaliColumnIdentifier.newReadOnlyId(
    		ProgramsTableRowModel.PROPERTY_CODE,
            n("dali.property.code"),
            n("dali.program.program.code.tip"),
            String.class);

	public static final DaliColumnIdentifier<ProgramsTableRowModel> COMMENT = DaliColumnIdentifier.newId(
			ProgramsTableRowModel.PROPERTY_COMMENT,
			n("dali.property.comment"),
			n("dali.property.comment"),
			String.class,
			false);

	public static final DaliColumnIdentifier<ProgramsTableRowModel> CREATION_DATE = DaliColumnIdentifier.newReadOnlyId(
			ProgramsTableRowModel.PROPERTY_CREATION_DATE,
			n("dali.property.date.creation"),
			n("dali.property.date.creation"),
			Date.class);

    public static final DaliColumnIdentifier<ProgramsTableRowModel> UPDATE_DATE = DaliColumnIdentifier.newReadOnlyId(
			ProgramsTableRowModel.PROPERTY_UPDATE_DATE,
			n("dali.property.date.modification"),
			n("dali.property.date.modification"),
			Date.class);



	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public ProgramsTableModel(final SwingTableColumnModel columnModel) {
		super(columnModel, false, false);
    }

	/** {@inheritDoc} */
	@Override
	public ProgramsTableRowModel createNewRow() {
		return new ProgramsTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public DaliColumnIdentifier<ProgramsTableRowModel> getFirstColumnEditing() {
		return NAME;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex, ColumnIdentifier<ProgramsTableRowModel> propertyName) {

		if (ProgramsTableRowModel.PROPERTY_DESCRIPTION.equals(propertyName.getPropertyName())) {
			return true;
		}

		return super.isCellEditable(rowIndex, columnIndex, propertyName);
	}
}
