package fr.ifremer.dali.ui.swing.content.home.operation;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.referential.*;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.DaliColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau de prelevements pour l'ecran d accueil.
 */
public class OperationsTableModel extends AbstractDaliTableModel<OperationsTableRowModel> {

    /**
     * Identifiant pour la colonne mnemonique.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> NAME = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_NAME,
            n("dali.property.mnemonic"),
            n("dali.property.mnemonic"),
            String.class,
            true);
    /**
     * Identifiant pour la colonne engin.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> SAMPLING_EQUIPMENT = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SAMPLING_EQUIPMENT,
            n("dali.property.samplingEquipment"),
            n("dali.property.samplingEquipment"),
            SamplingEquipmentDTO.class,
            true);
    /**
     * Identifiant pour la colonne heure.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> TIME = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_TIME,
            n("dali.property.time"),
            n("dali.property.time"),
            Integer.class);
    /**
     * Identifiant pour la colonne taille.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> SIZE = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SIZE,
            n("dali.property.size"),
            n("dali.property.size"),
            Double.class);
    /**
     * Identifiant pour la colonne unite taille.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> SIZE_UNIT = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SIZE_UNIT,
            n("dali.property.unit"),
            n("dali.property.unit"),
            UnitDTO.class);
    /**
     * Identifiant pour la colonne commentaire.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> COMMENT = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_COMMENT,
            n("dali.property.comment"),
            n("dali.property.comment"),
            String.class);
    /**
     * Identifiant pour la colonne service preleveur.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> SAMPLING_DEPARTMENT = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SAMPLING_DEPARTMENT,
            n("dali.property.department.sampler"),
            n("dali.home.samplingOperation.department.sampler.tip"),
            DepartmentDTO.class, true);
    /**
     * Identifiant pour la colonne analyst.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> ANALYST = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_ANALYST,
            n("dali.property.analyst"),
            n("dali.home.samplingOperation.department.analyst.tip"),
            DepartmentDTO.class);

    /**
     * Identifiant pour la colonne immersion.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> DEPTH = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_DEPTH,
            n("dali.property.depth.precise"),
            n("dali.home.samplingOperation.depth.precise.tip"),
            Double.class);

    /**
     * Identifiant pour la colonne niveau.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> DEPTH_LEVEL = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_DEPTH_LEVEL,
            n("dali.property.level"),
            n("dali.property.level"),
            LevelDTO.class);

    /**
     * Identifiant pour la colonne immersion min.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> MIN_DEPTH = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_MIN_DEPTH,
            n("dali.property.depth.precise.min"),
            n("dali.home.samplingOperation.depth.precise.min.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne immersion.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> MAX_DEPTH = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_MAX_DEPTH,
            n("dali.property.depth.precise.max"),
            n("dali.home.samplingOperation.depth.precise.max.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne coordonnees (latitude).
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> LATITUDE = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_LATITUDE,
            n("dali.property.latitude.real"),
            n("dali.home.samplingOperation.latitude.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne coordonnees (longitude).
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> LONGITUDE = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_LONGITUDE,
            n("dali.property.longitude.real"),
            n("dali.home.samplingOperation.longitude.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne positionnement.
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> POSITIONING = DaliColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_POSITIONING,
            n("dali.property.positioning.name"),
            n("dali.home.samplingOperation.positioning.name.tip"),
            PositioningSystemDTO.class);

    /**
     * Constant <code>POSITIONING_PRECISION</code>
     */
    public static final DaliColumnIdentifier<OperationsTableRowModel> POSITIONING_PRECISION = DaliColumnIdentifier.newReadOnlyId(
            OperationsTableRowModel.PROPERTY_POSITIONING_PRECISION,
            n("dali.property.positioning.precision"),
            n("dali.home.samplingOperation.positioning.precision.tip"),
            String.class);

    private boolean readOnly;
    private boolean nameInModel;

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public OperationsTableModel(final SwingTableColumnModel columnModel, boolean createNewRow) {
        super(columnModel, createNewRow, false);
        readOnly = false;
        try {
            columnModel.getColumnIndex(NAME);
            nameInModel = true;
        } catch (IllegalArgumentException e) {
            nameInModel = false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationsTableRowModel createNewRow() {
        return new OperationsTableRowModel(readOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DaliColumnIdentifier<OperationsTableRowModel> getFirstColumnEditing() {
        return nameInModel ? NAME : SAMPLING_EQUIPMENT;
    }

    /**
     * <p>Setter for the field <code>readOnly</code>.</p>
     *
     * @param readOnly a boolean.
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractOperationsTableUIModel getTableUIModel() {
        return (AbstractOperationsTableUIModel) super.getTableUIModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStateContext() {
        if (getTableUIModel().getSurvey() != null && getTableUIModel().getSurvey().getProgram() != null) {

            return SurveyDTO.PROPERTY_SAMPLING_OPERATIONS + '_'
                    + SamplingOperationDTO.PROPERTY_PMFMS + '_'
                    + getTableUIModel().getSurvey().getProgram().getCode();

        }

        return super.getStateContext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<OperationsTableRowModel> propertyName) {

        boolean editable = true;
        // Positioning is allowed when coordinate is set
        if (POSITIONING == propertyName) {
            OperationsTableRowModel rowModel = getEntry(rowIndex);
            editable = rowModel.getLatitude() != null || rowModel.getLongitude() != null;
        }

        return editable && super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

    @Override
    public boolean isCreateNewRow() {
        return super.isCreateNewRow() && !readOnly;
    }
}
