package fr.ifremer.dali.ui.swing.content.extraction.config.preset;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>PmfmPresetUIHandler class.</p>
 */
public class PmfmPresetUIHandler extends AbstractDaliTableUIHandler<PmfmPresetRowModel, PmfmPresetUIModel, PmfmPresetUI> implements Cancelable {

    public static final Log LOG = LogFactory.getLog(PmfmPresetUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(PmfmPresetUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new PmfmPresetUIModel());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(PmfmPresetUI ui) {
        initUI(ui);

        initTable();

        initBeanList(ui.getValueDoubleList(), null, null);
        ui.getValueDoubleList().getHandler().addAdditionalControls();

        initBeanList(ui.getResultPmfmDoubleList(), null, new ArrayList<>());

        // fix preferred size (Mantis #40756)
        Dimension size = ui.getResultPmfmDoubleList().getPreferredSize();
        ui.getResultPmfmDoubleList().setPreferredSize(new Dimension(size.width, 130));

        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

    }

    @Override
    public SwingValidator<PmfmPresetUIModel> getValidator() {
        return getUI().getValidator();
    }

    private void initTable() {

        addColumn(PmfmPresetTableModel.PMFM);

        TableColumnExt valuesCol = addColumn(PmfmPresetTableModel.QUALITATIVE_VALUES);
        fixColumnWidth(valuesCol, 50);

        PmfmPresetTableModel tableModel = new PmfmPresetTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        initTable(getTable(), true);

        getTable().setHorizontalScrollEnabled(false);
        getTable().setVisibleRowCount(6);
        getTable().setEditable(false);
    }

    private void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {
                case PmfmPresetUIModel.PROPERTY_INITIAL_PMFM_PRESETS:

                    loadTable();
                    break;

                case PmfmPresetUIModel.PROPERTY_SINGLE_ROW_SELECTED:

                    updateDoubleList();
                    break;

                case PmfmPresetUIModel.PROPERTY_SELECTED_VALUES:

                    if (getModel().getSingleSelectedRow() != null) {
                        // update row values
                        getModel().getSingleSelectedRow().setQualitativeValues(
                                new ArrayList<>(getModel().getSelectedValues()));
                    }
                    break;

                case PmfmPresetUIModel.PROPERTY_AVAILABLE_RESULT_PMFMS:

                    getUI().getResultPmfmDoubleList().getHandler().setUniverse(getModel().getAvailableResultPmfms());
                    break;

                case PmfmPresetUIModel.PROPERTY_INITIAL_RESULT_PMFMS:

                    getUI().getResultPmfmDoubleList().getHandler().setSelected(getModel().getInitialResultPmfms());
                    break;
            }
        });
    }

    private void loadTable() {

        getModel().setLoading(true);
        List<PmfmPresetRowModel> rows = new ArrayList<>();

        Map<PmfmDTO, List<QualitativeValueDTO>> initialPmfmPresetMap = getModel().getInitialPmfmPresets().stream().collect(
                HashMap::new, (map, pmfmPreset) -> map.put(pmfmPreset.getPmfm(), pmfmPreset.getQualitativeValues()),
                HashMap::putAll
        );

        // for each ordered list of available pmfms
        for (PmfmDTO availablePmfm: getModel().getAvailablePmfms()) {
            PmfmPresetRowModel row = getTableModel().createNewRow();
            row.setPmfm(availablePmfm);
            // add, if available, the initial list of qualitative values
            row.setQualitativeValues(initialPmfmPresetMap.get(availablePmfm));
            rows.add(row);
        }

        getModel().setRows(rows);
        recomputeRowsValidState();
        getModel().setLoading(false);
    }

    private void updateDoubleList() {

        List<QualitativeValueDTO> universeList = null;
        List<QualitativeValueDTO> selectedList = null;

        if (getModel().getSingleSelectedRow() != null) {

            universeList = getModel().getSingleSelectedRow().getPmfm() == null
                    ? null
                    : getModel().getSingleSelectedRow().getPmfm().getQualitativeValues();
            selectedList = new ArrayList<>(getModel().getSingleSelectedRow().getQualitativeValues());

        }

        getUI().getValueDoubleList().getHandler().setUniverse(universeList);
        getUI().getValueDoubleList().getHandler().setSelected(selectedList);
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        if (getModel().isValid()) {
            stopListenValidatorValid(getValidator());
            closeDialog();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel() {
        stopListenValidatorValid(getValidator());
        getModel().setValid(false);
        closeDialog();
    }

    @Override
    public AbstractTableModel<PmfmPresetRowModel> getTableModel() {
        return (PmfmPresetTableModel) getTable().getModel();
    }

    @Override
    public SwingTable getTable() {
        return getUI().getPmfmTable();
    }

}
