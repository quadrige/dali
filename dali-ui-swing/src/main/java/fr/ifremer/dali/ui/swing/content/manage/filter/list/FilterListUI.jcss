/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

ExtendedComboBox {
	showReset: true;
	filterable: true;
	showDecorator: false;
}

#menuPanel {
	border: {BorderFactory.createTitledBorder(t("dali.config.menu.title"))};
}

#filterLabel {
	text: "dali.filter.filterList.filter.label";
	labelFor: {filtersCombo};
}

#searchButton {
	actionIcon: find;
	text: "dali.action.search.label";
	toolTipText: "dali.action.search.tip";
	_applicationAction: {SearchAction.class};
}

#clearButton{
	actionIcon: reset;
	text: "dali.action.search.clear.label";
	toolTipText: "dali.action.search.clear.tip";
	_applicationAction: {ClearAction.class};
}

#tablePanel {
	border: {BorderFactory.createTitledBorder(t("dali.filter.filterList.title"))};
}

#newButton {
	actionIcon: add;
	text: "dali.common.new";
	toolTipText: "dali.filter.filterList.new.tip";
}

#duplicateButton {
	actionIcon: copy;
	text: "dali.action.duplicate.label";
	toolTipText: "dali.action.duplicate.filter.tip";
	enabled: {!model.getSelectedRows().isEmpty() && model.getSelectedRows().size() == 1};
	_applicationAction: {DuplicateFilterAction.class};
}

#deleteButton {
	actionIcon: delete;
	text: "dali.common.delete";
	toolTipText: "dali.filter.filterList.delete.tip";
	enabled: {!model.getSelectedRows().isEmpty()};
	_applicationAction: {DeleteFilterAction.class};
}

#importButton {
	actionIcon: import;
	text: "dali.common.import";
	toolTipText: "dali.filter.filterList.import.tip";
	_applicationAction: {ImportFilterAction.class};
}

#exportButton {
	actionIcon: export;
	text: "dali.common.export";
	toolTipText: "dali.filter.filterList.export.tip";
	enabled: {!model.getSelectedRows().isEmpty()};
	_applicationAction: {ExportFilterAction.class};
}