package fr.ifremer.dali.ui.swing.util.map.layer;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.DirectLayer;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;

/**
 * @author peck7 on 14/09/2017.
 */
public class InfoPanelLayer extends DirectLayer implements DataLayer {

    private Point point;
    private List<String> texts;

    private static final int LINE_LENGTH = 30;
    private static final int TEXT_BORDER = 10;

    public InfoPanelLayer() {
        setTitle("infoPanelLayer");
    }

    public void setInfo(Point point, List<String> texts) {
        this.point = point;
        this.texts = texts;
        setVisible(true);
    }

    public void clear() {
        point = null;
        texts = null;
    }

    private boolean isValid() {
        return point != null && CollectionUtils.isNotEmpty(texts);
    }

    @Override
    public void draw(Graphics2D graphics, MapContent map, MapViewport viewport) {

        if (!isValid()) return;

        Stroke oldStroke = graphics.getStroke();
        Paint oldPaint = graphics.getPaint();
        Color oldBackground = graphics.getBackground();

//        graphics.setStroke(new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
        graphics.setStroke(new BasicStroke(2));
        graphics.setPaint(Color.BLACK);

        // calculate text height and width
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int maxTextWidth = 0;
        int maxTextHeight = 0;
        for (String text : texts) {
            Rectangle2D textRect = fontMetrics.getStringBounds(text, graphics);
            maxTextWidth = Math.max(maxTextWidth, (int) (textRect.getWidth() + 0.5));
            maxTextHeight = Math.max(maxTextHeight, (int) (textRect.getHeight() + 0.5));
        }

        int boxWidth = maxTextWidth + TEXT_BORDER * 2;
        int boxHeight = maxTextHeight * texts.size() + TEXT_BORDER * 2;

        graphics.setPaint(Color.WHITE);
        graphics.fillRoundRect(point.x - boxWidth / 2, point.y - LINE_LENGTH - boxHeight, boxWidth, boxHeight, 10, 10);
        graphics.setPaint(Color.BLACK);
        graphics.drawRoundRect(point.x - boxWidth / 2, point.y - LINE_LENGTH - boxHeight, boxWidth, boxHeight, 10, 10);

        for (int i = 0; i < texts.size(); i++) {
            graphics.drawString(texts.get(i),
                    point.x - maxTextWidth / 2,
                    point.y - LINE_LENGTH - maxTextHeight * texts.size() + (i * maxTextHeight));
        }

        graphics.drawLine(point.x, point.y, point.x, point.y - LINE_LENGTH);

        graphics.setStroke(oldStroke);
        graphics.setPaint(oldPaint);
        graphics.setBackground(oldBackground);
    }

    @Override
    public ReferencedEnvelope getBounds() {
        return null;
    }
}
