package fr.ifremer.dali.ui.swing.util.map;

/**
 * @author peck7 on 28/07/2020.
 */
public interface MapUIModel {

    MapParentUIModel getParentUIModel();

}
