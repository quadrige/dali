package fr.ifremer.dali.ui.swing.content.manage.referential.analysisinstruments.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.analysisinstruments.menu.AnalysisInstrumentsMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.analysisinstruments.table.AnalysisInstrumentsTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.analysisinstruments.table.AnalysisInstrumentsTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des analysisInstruments au niveau national
 */
public class AnalysisInstrumentsNationalUIHandler extends AbstractDaliTableUIHandler<AnalysisInstrumentsTableRowModel, AnalysisInstrumentsNationalUIModel, AnalysisInstrumentsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(AnalysisInstrumentsNationalUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        AnalysisInstrumentsNationalUIModel model = new AnalysisInstrumentsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(AnalysisInstrumentsNationalUI ui) {
        initUI(ui);

        // init listener on found analysis instruments
        ui.getAnalysisInstrumentsNationalMenuUI().getModel().addPropertyChangeListener(AnalysisInstrumentsMenuUIModel.PROPERTY_RESULTS, evt -> {

            if (evt.getNewValue() != null) {
                getModel().setBeans((List<AnalysisInstrumentDTO>) evt.getNewValue());
            }
        });
        
        // listen to 'apply filter' results
        ui.getAnalysisInstrumentsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS,
                evt -> getModel().setBeans((List<AnalysisInstrumentDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // mnemonic
        TableColumnExt mnemonicCol = addColumn(AnalysisInstrumentsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        TableColumnExt descriptionCol = addColumn(AnalysisInstrumentsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(AnalysisInstrumentsTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(AnalysisInstrumentsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(AnalysisInstrumentsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                AnalysisInstrumentsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ACTIVE),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        AnalysisInstrumentsTableModel tableModel = new AnalysisInstrumentsTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.analysisInstrument"));

        // Initialisation du tableau
        initTable(table, true);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        table.setVisibleRowCount(5);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
    	getModel().setBeans(null);
    }
    
    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<AnalysisInstrumentsTableRowModel> getTableModel() {
        return (AnalysisInstrumentsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getAnalysisInstrumentsNationalTable();
    }
}
