package fr.ifremer.dali.ui.swing.content.manage.referential.taxon.national;

/*
 * #%L
 * Dali :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.taxon.menu.TaxonMenuUIModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.taxon.table.TaxonTableModel;
import fr.ifremer.dali.ui.swing.content.manage.referential.taxon.table.TaxonTableRowModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableModel;
import fr.ifremer.dali.ui.swing.util.table.AbstractDaliTableUIHandler;
import fr.ifremer.dali.ui.swing.util.table.editor.AssociatedTaxonCellEditor;
import fr.ifremer.dali.ui.swing.util.table.renderer.AssociatedTaxonCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des taxons au niveau national
 */
public class TaxonsNationalUIHandler extends AbstractDaliTableUIHandler<TaxonTableRowModel, TaxonsNationalUIModel, TaxonsNationalUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(TaxonsNationalUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        TaxonsNationalUIModel model = new TaxonsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(TaxonsNationalUI ui) {
        initUI(ui);

        TaxonMenuUIModel nationalMenuModel = ui.getTaxonsNationalMenuUI().getModel();

        // set full search mode
        nationalMenuModel.setFullProperties(true);
        
        // listen to search results
        nationalMenuModel.addPropertyChangeListener(TaxonMenuUIModel.PROPERTY_RESULTS,
                evt -> getModel().setBeans((List<TaxonDTO>) evt.getNewValue()));
        
        
        // listen to 'apply filter' results
        ui.getTaxonsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS, evt -> {

            List<TaxonDTO> taxons = (List<TaxonDTO>) evt.getNewValue();

            // taxonsFromFilter list miss some properties, must query them
            getContext().getReferentialService().fillTaxonsProperties(taxons);

            // then affect list to table
            getModel().setBeans(taxons);
        });

        initTable();

    }

    private void initTable() {

        // Le tableau
        final SwingTable table = getTable();

        // name
        TableColumnExt nameCol = addColumn(TaxonTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // citation
        TableColumnExt citationCol = addFilterableComboDataColumnToModel(  // todo enlever les editeurs
                TaxonTableModel.CITATION,
                getContext().getReferentialService().getCitations(),
                false);
        citationCol.setSortable(true);
        citationCol.setEditable(false);

        // reference taxon
        TableColumnExt refTaxonCol = addFilterableComboDataColumnToModel(
                TaxonTableModel.REFERENCE_TAXON,
                getContext().getReferentialService().getTaxons(null),
                false);
        refTaxonCol.setSortable(true);
        refTaxonCol.setEditable(false);

        // parent taxon
        TableColumnExt parentTaxonCol = addFilterableComboDataColumnToModel(
                TaxonTableModel.PARENT,
                getContext().getReferentialService().getTaxons(null),
                false);
        parentTaxonCol.setSortable(true);
        parentTaxonCol.setEditable(false);

        // level
        TableColumnExt levelCol = addFilterableComboDataColumnToModel(
                TaxonTableModel.LEVEL,
                getContext().getReferentialService().getTaxonomicLevels(),
                false);
        levelCol.setSortable(true);
        levelCol.setEditable(false);

        // comment
        addCommentColumn(TaxonTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(TaxonTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(TaxonTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // temporary
        TableColumnExt temporaryCol = addBooleanColumnToModel(TaxonTableModel.TEMPORARY, table);
        temporaryCol.setSortable(true);
        temporaryCol.setEditable(false);

        // obsolete
        TableColumnExt obsoleteCol = addBooleanColumnToModel(TaxonTableModel.OBSOLETE, table);
        obsoleteCol.setSortable(true);
        obsoleteCol.setEditable(false);

        // virtual
        TableColumnExt virtualCol = addBooleanColumnToModel(TaxonTableModel.VIRTUAL, table);
        virtualCol.setSortable(true);
        virtualCol.setEditable(false);

        // composites
        TableColumnExt compositesCol = addColumn(
                new AssociatedTaxonCellEditor(getTable(), getUI()),
                new AssociatedTaxonCellRenderer(),
                TaxonTableModel.COMPOSITES);
        compositesCol.setSortable(true);

        // taxref
        TableColumnExt taxRefCol = addColumn(TaxonTableModel.TAXREF);
        taxRefCol.setSortable(true);
        taxRefCol.setEditable(false);

        // worms
        TableColumnExt wormsCol = addColumn(TaxonTableModel.WORMS);
        wormsCol.setSortable(true);
        wormsCol.setEditable(false);

        TaxonTableModel tableModel = new TaxonTableModel(getTable().getColumnModel(), false);
        table.setModel(tableModel);

        addExportToCSVAction(t("dali.property.taxon"), TaxonTableModel.COMPOSITES);

        // Initialisation du tableau
        initTable(table, true);

        // optional columns are hidden
        temporaryCol.setVisible(false);
        obsoleteCol.setVisible(false);
        virtualCol.setVisible(false);
        compositesCol.setVisible(false);
        taxRefCol.setVisible(false);
        wormsCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        table.setVisibleRowCount(5);

    }


    /** {@inheritDoc} */
    @Override
    public AbstractDaliTableModel<TaxonTableRowModel> getTableModel() {
        return (TaxonTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getTaxonsNationalTable();
    }


}
