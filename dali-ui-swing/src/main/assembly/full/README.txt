Pour démarrer Dali
--------------------

# Sous Linux

./Dali.sh

# Sous windows

Dali.exe

Consulter l'aide
----------------

En attendant que l'aide soit finalisée, vous pouvez consulter le site : 

http://www.ifremer.fr/maven/reports/quadrige2/dali/
