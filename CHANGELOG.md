## Sprint 89 - v6.6.1

- Le server de synchronisation à utiliser est la version **3.8.14**


## Sprint 88 - v6.6.0

- Le server de synchronisation à utiliser est la version **3.8.13**


## Sprint 87 - v6.5.1

- Le server de synchronisation à utiliser est la version **3.8.12**


## Sprint 86 - v6.5.0

- Le server de synchronisation à utiliser est la version **3.8.11**


## Sprint 85 - v6.4.1

- Le server de synchronisation à utiliser est la version **3.8.10**


## Sprint 84 - v6.3.7

- Le server de synchronisation à utiliser est la version **3.8.7**


## Sprint 83 - v6.3.6

- Le server de synchronisation à utiliser est la version **3.8.6**


## Sprint 82 - v6.3.5

- Le server de synchronisation à utiliser est la version **3.8.5**


## Sprint 81 - v6.3.4

- Le server de synchronisation à utiliser est la version **3.8.4**


## Sprint 80 - v6.3.3

- Le server de synchronisation à utiliser est la version **3.8.3**


## Sprint 79 - v6.3.2

- Le server de synchronisation à utiliser est la version **3.8.2**


## Sprint 77 - v6.3.1

- Le server de synchronisation à utiliser est la version **3.8.0**


## Sprint 76 - v6.3.0

- Le server de synchronisation à utiliser est la version **3.8.0**


## Sprint 75 - v6.2.9

- Le server de synchronisation à utiliser est la version **3.7.17**


## Sprint 74 - v6.2.8

- Le server de synchronisation à utiliser est la version **3.7.16**


## Sprint 73 - v6.2.7

- Le server de synchronisation à utiliser est la version **3.7.15**


## Sprint 72 - v6.2.6

- Le server de synchronisation à utiliser est la version **3.7.14**


## Sprint 71 - v6.2.5

- Le server de synchronisation à utiliser est la version **3.7.13**
- Mise à jour du modèle de base de données en version **3.4.0** à effectuer par Quadmire


## Sprint 70 - v6.2.4

- Le server de synchronisation à utiliser est la version **3.7.12**


## Sprint 69 - v6.2.3

- Le server de synchronisation à utiliser est la version **3.7.11**


## Sprint 68 - v6.2.2

- Le server de synchronisation à utiliser est la version **3.7.10**
- Mise à jour du modèle de base de données en version **3.3.7**
- Si l'importation des référentiels ne récupère pas tous les moratoires :
  > supprimer le fichier `version.appup` se trouvant dans le répertoire `data/db` de votre installation, puis importer les référentiels depuis le serveur.


## Sprint 67 - v6.2.1

- Le server de synchronisation à utiliser est la version **3.7.9**
- Mise à jour du modèle de base de données en version **3.3.6**


## Sprint 66 - v6.2.0

- Le server de synchronisation à utiliser est la version **3.7.8**


## Sprint 65 - v6.1.3

- Le server de synchronisation à utiliser est la version **3.7.7**


## Sprint 64 - v6.1.2

- Le server de synchronisation à utiliser est la version **3.7.7**


## Sprint 63 - v6.1.1

- Le server de synchronisation à utiliser est la version **3.7.7**


## Sprint 62 - v6.1.0

- Le server de synchronisation à utiliser est la version **3.7.6**


## Sprint 61 - v6.0.3

- Le server de synchronisation à utiliser est la version **3.7.4**


## Sprint 60 - v6.0.2

- Le server de synchronisation à utiliser est la version **3.7.3**


## Sprint 59 - v6.0.1

- Pas de mise à jour de modèle


## Sprint 58 - v6.0.0

- La base de données quadrige3 (Oracle & Postgres) doit être patchée afin de répondre aux modifications de modèle :
  * Evolutions du modèle Quadrige pour l'outil d'administration (Mantis #55336)
  * Version du modèle : 3.3.0

  Pour cela, télécharger le module quadrige3-core-server version **4.0.0-beta16**,
  puis exécuter la commande :

        'launch.(bat|sh) --schema-update' 
  ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>, ainsi que les options liées au type de base de données dans le fichier application.properties


- Le server de synchronisation à utiliser est la version **3.7.1**


## Sprint 57 - v5.2.2

- Pas de mise à jour de modèle


## Sprint 56 - v5.2.1

- Pas de mise à jour de modèle


## Sprint 55 - v5.2.0

- Pas de mise à jour de modèle


## Sprint 54 - v5.1.4

- Pas de mise à jour de modèle


## Sprint 53 - v5.1.2 & v5.1.3

- Pas de mise à jour de modèle


## Sprint 52 - v5.1.1

- Pas de mise à jour de modèle

- Paramétrage de l'image d'accueil (splash screen) : 2 nouvelles options de configuration sont ajoutées pour personaliser la liste des logos partenaires

        quadrige3.splashScreen.right.logos (liste des logos affichés en bas à droite de l'image d'accueil)
        quadrige3.splashScreen.left.logos (liste des logos affichés en bas à gauche de l'image d'accueil)

    Les valeurs par défaut de ces options sont :
        
        quadrige3.splashScreen.right.logos=logo-Ifremer.png
        quadrige3.splashScreen.left.logos=
        
    Dans cette version, le logo de CleanAtlantic est disponible. Pour l'afficher à gauche, positionner `quadrige3.splashScreen.left.logos=logo-CleanAtlantic.png`; pour l'afficher à droite, à côté du logo Ifremer, positioner `quadrige3.splashScreen.right.logos=logo-Ifremer.png,logo-CleanAtlantic.png`
    
    Les logos séparés par une virgule seront ajoutés de gauche à droite dans la partie gauche et de droite à gauche dans la partie droite.
    
    NOTE: L'ajout de logo partenaire nécessite une compilation du projet, les fichiers doivent être fournis à EIS pour redimensionnement et validation technique.

- Le lanceur d'application `dali-ui-swing-launcher` doit être mis à jour à la version **2.1** pour retirer l'ancienne image d'accueil.

    Veuillez modifier le fichier `dali-application.properties`:
    
        launcher.version=2.1
        launcher.url=zip:[...]/dali-ui-swing-launcher/2.1/dali-ui-swing-launcher-2.1-bin.zip


## Sprint 51 - v5.1

- Pas de mise à jour de modèle


## Sprint 50 - v5.0.2

- Pas de mise à jour de modèle

- Rappel: La configuration du server de synchronisation doit **impérativement** avoir les deux options suivantes :
          
                  quadrige3.persistence.db.script=classpath:fr/ifremer/quadrige3/core/db/changelog/hsqldb/quadrige3.script
                  quadrige3.persistence.liquibase.changelog.path=classpath:fr/ifremer/quadrige3/core/db/changelog/hsqldb/db-changelog-master.xml


## Sprint 49 - v5.0.1

- La base de données quadrige3 (Postgres) doit être patchée afin de répondre aux modifications de modèle :
    * Remplacement des triggers de suppression (Mantis #50370)
    * Version du modèle : 3.2.0

    Pour cela, télécharger le module quadrige3-batches-server version 3.6.3, 
    puis exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- La configuration du server de synchronisation doit **impérativement** avoir les deux options suivantes :

        quadrige3.persistence.db.script=classpath:fr/ifremer/quadrige3/core/db/changelog/hsqldb/quadrige3.script
        quadrige3.persistence.liquibase.changelog.path=classpath:fr/ifremer/quadrige3/core/db/changelog/hsqldb/db-changelog-master.xml

    
## Sprint 48 - v5.0

- La base de données quadrige3 (Postgres) doit être patchée afin de répondre aux modifications de modèle :
    * Adapter DALI pour permettre le remplacement de la base Oracle par Postgresql (Mantis #49006)
    * Version du modèle : 3.2.0

    Pour cela, télécharger le module quadrige3-batches-server version 3.6.1, 
    vérifier la présence de la ligne `quadrige3.persistence.dbms=pgsql` dans le fichier de configuration
    
    Configuration type pour Postgres :
    
        quadrige3.persistence.dbms=pgsql
        quadrige3.persistence.jdbc.url=jdbc:postgresql://<SERVER_IP>:5432/quadrige
        quadrige3.persistence.jdbc.username=<USER>
        quadrige3.persistence.jdbc.password=<PASS>
        quadrige3.persistence.jdbc.catalog=
        quadrige3.persistence.jdbc.schema=${quadrige3.persistence.jdbc.username}
        quadrige3.persistence.jdbc.driver=org.postgresql.Driver
        quadrige3.persistence.hibernate.dialect=org.hibernate.spatial.dialect.postgis.PostgisDialect
        
    puis exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- La base de données quadrige3 (HSQLDB) sera patchée au lancement de l'application
    * Version du modèle : 3.2.0

- Le serveur de synchronisation 3.6.1 possède de nouvelles options de configuration pour la synchronisation :

    * `quadrige3.synchro.tempQueryParameter.generated` : Indique si la table TEMP_QUERY_PARAMETER est créée par la synchro
        - `false` par défaut, sous Postgres elle peut être passée à `true` pour de meilleures performances

    ATTENTION: Pour une base de données Postgres issue d'une migration depuis Oracle, cette option est obligatoire
    `quadrige3.synchro.tempQueryParameter.generated=true` car la table TEMP_QUERY_PARAMETER est ignorée lors de la migration
    et n'est donc pas disponible.

    Configuration type pour Postgres :

        quadrige3.synchro.server.datasource.type=pgsql
        quadrige3.synchro.server.jdbc.url=jdbc:postgresql://<SERVER_IP>:5432/quadrige
        quadrige3.synchro.server.jdbc.username=<USER>
        quadrige3.synchro.server.jdbc.password=<PASS>
        quadrige3.synchro.server.jdbc.catalog=
        quadrige3.synchro.server.jdbc.schema=${quadrige3.synchro.server.jdbc.username}
        quadrige3.synchro.server.jdbc.driver=org.postgresql.Driver
        quadrige3.synchro.server.hibernate.dialect=org.hibernate.spatial.dialect.postgis.PostgisDialect
        quadrige3.synchro.tempQueryParameter.generated=true


## Sprint 47 - v4.7

- Pas de mise à jour de modèle

- Ajout d'un module séparé `dali-converter` pour convertir les fichiers de contextes et filtres provenant d'une version ancienne de Dali (ex: 2.6) pour les rendre compatibles avec la version 4.7


## Sprint 46 - v4.6

- Pas de note particulière pour cette version


## Sprint 45 - v4.5

- Pas de note particulière pour cette version


## Sprint 44 - v4.4

- Pas de mise à jour de modèle

- Le répertoire OLD contenant les anciennes versions de l'application est supprimé.
    Si des fichiers autres que ceux de l'historique sont présents dans ce répertoire, ils seront supprimés également !


## Sprint 43 - v4.3

- Pas de note particulière pour cette version


## Sprint 42 - v4.2

- Pas de note particulière pour cette version


## Sprint 41 - v4.1

- Pas de note particulière pour cette version


## Sprint 40 - v4.0

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Intégrer les pièces jointes à la synchronisation (Mantis #47677)
    * Version du modèle : 3.1.6

    Pour cela, télécharger le module quadrige3-batches-server version 3.4.0 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- La base de données quadrige3 (HSQLDB) sera patchée au lancement de l'application
    * Version du modèle : 3.1.2

- Le serveur de synchronisation possède de nouvelles options de configuration pour la synchronisation des photos :

    * `quadrige3.persistence.db.photo.directory` : Répertoire où sont stockées les photos
        - **Cette option est obligatoire et doit pointer sur le répertoire (ou le montage réseau) contenant les fichiers photos**  
    * `quadrige3.synchro.export.data.file.maxUploadSize` : Taille maximum autorisé (en octets) pour l'envoi unique d'un fichier de synchronisation, au delà de cette limite, le fichier est scindé en plusieurs parties avant l'envoi (`268435456` par défaut, soit 256 Mo)
        - Cette option est liée à la configuration du serveur frontal (Apache par exemple) dans laquelle cette limite est fixées. Si cette limite est modifiée, il faut reporter la nouvelle valeur dans cette option côté serveur **et** côté client 
    * `synchro.persistence.file.lock.enable` : Active le verrou sur les fichiers à synchroniser
        - `true` par défaut, elle active le système de verrou sur tous les fichiers en cours de synchronisation pour éviter les écrasements ou les suppressions pouvant intervenir par d'autres synchronisations au même moment. Cette option peut être désactivée si le risque est faible.

- L'application client possède de nouvelles options de configuration pour la synchronisation des photos :

    * `quadrige3.synchro.export.data.file.maxUploadSize` : (voir plus haut)
    * `quadrige3.synchro.photo.enable.default` : Active la synchronisation des photos par défaut
        - Option qui coche par défaut (ou non) la case "Synchroniser les photos" dans les écrans de sélection de programmes à l'import et à l'export (`false` par défaut)
    * `quadrige3.synchro.photo.maxSize` : Taille maximum d'une photo pouvant être importée dans le système
        - Option limitant l'ajout de photos dont la taille ne dépasse pas cette limite. La valeur par défaut est issue de Quadrige² soit `10 000 000` octets.
    * `quadrige3.synchro.photo.maxNumber.threshold` : Seuil du nombre de photo pour lequel l'avertissement sur le temps de synchronisation sera affiché
        - Option qui permet d'avertir l'utilisateur que la synchronisation pourra être longue. Le mécanisme de synchro va compter le nombre de photo à importer ou à exporter, seulement les photos pleine résolution, sans compter le poids en octets. La valeur par défaut est arbitrairement fixée à `100`.

- Le module de cartographie possède maintenant des options de configuration. Les options configurables sont :
        
    * `dali.survey.map.baseLayer.default` : Fond de carte utilisé par défaut par l'application (liste déroulante avec les fonds de carte disponibles, `Sextant WMTS` par défaut)
        - La valeur réellement stockée dans le fichier de configuration est un code technique. Pour une configuration manuelle, les codes autorisés sont `EMB_SHAPE`, `WMTS_SEXTANT`, `OSM`, `OTM`, `VOYAGER` et `SATELLITE`   
    * `dali.survey.map.projection.code` : Code EPSG de la projection de carte (liste déroulante avec les projections disponibles, `WGS 84 / Pseudo-Mercator` par défaut)
    * `dali.survey.map.sextantWMS.url` : Url du serveur Ifremer Sextant WMS 
    * `dali.survey.map.openStreetMap.url` : Url du serveur OpenStreetMap
    * `dali.survey.map.openTopoMap.url` : Url du serveur OpenTopoMap (version améliorée d'OpenStreetMap avec couche topographique)
    * `dali.survey.map.cartoBase.url` : Url du serveur CartoBase Voyager (version légère et rapide d'OpenStreetMap)
    * `dali.survey.map.satellite.url` : Url du serveur MapTiler (carte satellite de MapTiler)
    * `dali.survey.map.maxSelection` : Nombre maximum de passages pouvant être afficher simultanément sur la carte (`50` par défaut) 


## Sprint 39 - v3.3

- De nouvelles options sont ajoutées pour gérer les transcodages des référentiels suivant :
        
        dali.transcribingItemType.label.taxLevelNm (libellé des niveaux taxonomiques)
        dali.transcribingItemType.label.statusNm (libellé des statuts)
        
    valeurs par défaut: 
    
        dali.transcribingItemType.label.taxLevelNm=fr_FR:DALI-TAXONOMIC_LEVEL.TAX_LEVEL_NM;en_GB:DALI-TAXONOMIC_LEVEL.TAX_LEVEL_NM_GB;es_ES:DALI-TAXONOMIC_LEVEL.TAX_LEVEL_NM_ES
        dali.transcribingItemType.label.statusNm=fr_FR:DALI-STATUS.STATUS_NM;en_GB:DALI-STATUS.STATUS_NM_GB;es_ES:DALI-STATUS.STATUS_NM_ES

- Nouvelles chaines i18n à traduire :

        dali.error.synchro.serverUnavailable
        dali.error.synchro.serverNotYetAvailable
        

## Sprint 38 - v3.2

- De nouvelles options sont ajoutées pour gérer les transcodages des référentiels suivant :
        
        dali.transcribingItemType.label.parameterCd (code de paramètre)
        dali.transcribingItemType.label.qualFlagNm (libellé de niveau de qualification)
        
    valeurs par défaut: 
    
        dali.transcribingItemType.label.parameterCd=fr_FR:DALI-PARAMETER.PAR_CD;en_GB:DALI-PARAMETER.PAR_CD_GB;es_ES:DALI-PARAMETER.PAR_CD_ES
        dali.transcribingItemType.label.qualFlagNm=fr_FR:DALI-QUALITY_FLAG.QUAL_FLAG_NM;en_GB:DALI-QUALITY_FLAG.QUAL_FLAG_NM_GB;es_ES:DALI-QUALITY_FLAG.QUAL_FLAG_NM_ES


## Sprint 37 - v3.1

- De nouvelles options sont ajoutées pour gérer les transcodages des référentiels suivant :
        
        dali.transcribingItemType.label.monLocNm
        
    valeur par défaut: 
    
        fr_FR:DALI-MONITORING_LOCATION.MON_LOC_NM;en_GB:DALI-MONITORING_LOCATION.MON_LOC_NM_GB;es_ES:DALI-MONITORING_LOCATION.MON_LOC_NM_ES

- Pour simplifier la configuration du serveur de synchronisation, l'option `quadrige3.synchro.import.referential.transcribingItemType.label.includes` 
    qui doit contenir tous les libellés des types de transcodage à importer, accepte désormais le caractère '%'.
        
        quadrige3.synchro.import.referential.transcribingItemType.label.includes=DALI% (tous les types de transcodage commençant par DALI seront importés)

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Internationalisation : type de transcription dupliqué (Mantis #47052)

    Pour cela, télécharger le module quadrige3-batches-server version 3.3.2 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 36 - v3.0

- De nouvelles options sont ajoutées pour gérer les transcodages des référentiels suivant :

        dali.transcribingItemType.label.pmfmNm
        dali.transcribingItemType.label.qualitativeValueNm
        dali.transcribingItemType.label.parameterNm
        dali.transcribingItemType.label.matrixNm
        dali.transcribingItemType.label.fractionNm
        dali.transcribingItemType.label.methodNm
        dali.transcribingItemType.label.unitNm
        dali.transcribingItemType.label.samplingEquipmentNm
    
    Idem pour les extractions de données :
    
        dali.transcribingItemType.label.pmfmExtraction
        dali.transcribingItemType.label.qualitativeValueExtraction
        dali.transcribingItemType.label.samplingEquipmentExtraction

    Elles sont toutes interprétées de la manière suivante:
        
        <locale1>:<transcTypeLb1>;<locale2>:<transcTypeLb2>;<locale3>:<transcTypeLb3>[; etc...]
        
    Par exemple :
        
        dali.transcribingItemType.label.pmfmNm=fr_FR:DALI-PMFM.PMFM_NM;en_GB:DALI-PMFM.PMFM_NM_GB;es_ES:DALI-PMFM.PMFM_NM_ES

    Si un libellé de transcodage est absent pour une langue choisie dans l'application, aucun transcodage ne sera effectué sur le référentiel.

    Chaque changement de langue dans l'application, commence par une reconstruction des caches de référentiels dépendant de la langue; ensuite l'écran est fermé puis réouvert.
    Note: le changement manuel de la langue en modifiant l'option `quadrige3.i18n.locale` ne réinitialisera pas ces caches, il faut l'éviter. (ou supprimer le dossier dbcache avant de redémarrer)


## Sprint 35 - v2.6

- Le lanceur se mettra à jour en version 2.0 (la mise à jour est automatique)

- Configuration par fichier de resource:
    * Le fichier de version `version.appup` est nécessaire dans l'archive pointée par le fichier de resource
      pour que la mise à jour de la configuration se déroule correctement


## Sprint 34 - v2.5

- Pas de note particulière pour cette version


## Sprint 33 - v2.4

- Pas de note particulière pour cette version


## Sprint 32 - v2.3

- Pas de note particulière pour cette version


## Sprint 31 - v2.2

- Pas de note particulière pour cette version


## Sprint 30 - v2.1

- Pas de note particulière pour cette version


## Sprint 29 - v2.0.1

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Exception à l'exportation des données vers le système central (Mantis #46165)

    Pour cela télécharger le module quadrige3-batches-server version 3.2.3 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 28 - v2.0

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Adaptations nécessaires aux évolutions du modèle pour les PSFMU (Mantis #45004)

    Pour cela télécharger le module quadrige3-batches-server version 3.2.2 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 27 - v1.9.2

- Pas de note particulière pour cette version


## Sprint 26 - v1.9.1

- Pas de note particulière pour cette version


## Sprint 25 - v1.9

- Pas de note particulière pour cette version


## Sprint 24 - v1.8

- Pas de note particulière pour cette version


## Sprint 23 - v1.7

- Pas de note particulière pour cette version


## Sprint 22 - v1.6

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Ajout de la colonne STATUS_CD dans RULE_LIST (report ReefDb)
    * Correction du commentaire sur les mesures calculées (Mantis #43150)

    Pour cela télécharger le module quadrige3-batches-server version 3.1.12 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 21 - v1.5

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Modification de 2 types de colonne dans RULE et RULE_PARAMETER
    * Ajout de la vue RULE_PRECONDITIONS

    Pour cela télécharger le module quadrige3-batches-server version 3.1.12 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 20 - v1.4

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Ajout d'un index de performance sur les tables VALIDATION_HISTORY et QUALIFICATION_HISTORY
    * Modification de 2 index de performance sur TEMP_QUERY_PARAMETER (changement d'ordre de colonne)

    Pour cela télécharger le module quadrige3-batches-server version 3.1.11 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 19 - v1.3

- Pas de note particulière pour cette version


## Sprint 18 - v1.2

- Pas de note particulière pour cette version


## Sprint 17 - v1.1

- Pas de note particulière pour cette version


## Sprint 16 - v1.0

- Pas de note particulière pour cette version


## Sprint 15 - v0.15

- Pas de note particulière pour cette version


## Sprint 14 - v0.14

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #40033)
    
    Pour cela télécharger le module quadrige3-core-server version 3.0.3 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 13 - v0.13

- Pas de note particulière pour cette version


## Sprint 12 - v0.12

- Changement de version du core Quadrige -> `quadrige3-core`

- Renommage des options de configuration:
    * quadrige2.* -> quadrige3.*

- Attention: la base de données provenant d'une version antérieure ne sera pas ouverte.
    * Veuillez réinstaller une base vierge depuis le serveur
    * Si vous souhaitez toutefois ouvrir votre ancienne base locale, avant l'ouverture de l'application,
      renommez les fichiers dans le dossier `data/db`, tous les fichiers `quadrige2.*` en `quadrige3.*`


## Sprint 11 - v2.6.2-CARTO-RC11

- Pas de note particulière pour cette version


## Sprint 10 BIS - v2.6.2-CARTO-RC10 (v2)

- Cette version corrige essentiellement un problème de chargement des requête XML dans l'environnement final

- Certaines corrections du Sprint 11 ont été livrées notamment l'amélioration des temps d'exportation des données vers le système central, pour cela :
    * La base de données locale sera patchée (cf Mantis #39066)

- La base de données Quadrige2 (ORACLE) doit être patchée afin de répondre aux modifications de modèle.

    Pour cela télécharger le module quadrige2-core-server version 3.0.0-RC14 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 10 - v2.6.2-CARTO-RC10

- Ajout de l'extraction des données en format standard

    LIMITATIONS DE CETTE VERSION :
    - L'ordre des colonnes de mesures sur le passage n'est pas encore traité
    - Les colonnes 'Heure' sont en format brute
    - Le format numérique des résultat de mesures est à améliorer (enlever les décimales 0 inutiles)
    - L'ordre des groupements de valeurs qualitative peut être revu avec une option supplémentaire à ajouter à l'écran de configuration du format de sortie
      (par exemple 1=CATEGORIE 2=TYPOLOGIE dans cet ordre sinon l'ordre naturel (PMFM_ID) est utilisé)

- Ajout de 2 Options de configuration pour le transcodage des PMFM et QUALITATIVE_VALUE pour les extraction;
    
        dali.transcribingItemType.label.pmfmExtraction
        dali.transcribingItemType.label.qualitativeValueExtraction
        (valeurs par défaut à définir)


## Sprint 9 - v2.6.2-CARTO-RC9

- Pas de note particulière pour cette version


## Sprint 8 - v2.6.2-CARTO-RC8

- La base de données Quadrige2 (ORACLE) doit être patchée afin de répondre aux modifications de modèle.

    Pour cela télécharger le module quadrige2-core-server version 3.0.0-RC10 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- Définition du fuseau horaire à utiliser pour les dates dans la base de données (report mantis #36465)
  Nouvelle option de configuration (côté client) :

    - `dali.persistence.db.timezone`: Fuseau horaire de la base de données (par défaut: ${user.timezone} = en fonction de l'OS)
        * Exemple: si la base de données est en France, il faut positionner la valeur à 'Europe/Paris'

    - Plus besoin d'utiliser l'option de configuration `dali.timezone` pour contourner le bug

    - Les dates stockée en base locales sont :
        * Conservées de bout en bout dans le fuseau horaire de la base de données (cf option précédente) s'il s'agit d'une date système (ex: UPDATE_DT)
        * ou bien converties vers le fuseau de la base de données, depuis l'heure locale du poste. Pour leur affichage elles sont alors reconverties dans le fuseau horaire du poste. (ex: VALIDATION_DT, CONTROL_DT).
        * ou bien non converties (ex: l'heure du passage SURVEY_TIME)

- Nouvelles options de configuration pour gérer les tentatives de reconnexion réseau (pour sécurisation de la synchro - report mantis #38089):
     - `quadrige2.synchronization.retry.count` : Nom bre de tentative de reocnnexion (par défaut: 10)
     - `quadrige2.synchronization.retry.timeout` : Délai (en Ms) entre chaque nouvelle tentative (par défaut: 5000ms)
     - `quadrige2.admin.email` : Email de l'assistance (par défaut: q2support@ifremer.fr)

     /!\ ancienne propriété `quadrige2.synchro.server.export.supportEmail.recipients` (email utilisée en cas de plantage des pocédures stockées)
         remplacée par `quadrige2.admin.email`

- Gestion du mode de carte par défaut:
    * Option de configuration `dali.survey.map.preferred.mapMode.code` pour démarrer l'affichage de la carte
        Option possibles : WMTS_SEXTANT (par défaut), WMS_SEXTANT
    * Le mode WMTS de Sextant reste encore à améliorer, si vous rencontrer un problème, veuillez repasser au mode WMS.


## Sprint 7 - v2.6.2-CARTO-RC7

- Pour effectuer le calcul du la longueur/distance du passage, spécifier l'option de configuration suivante:

        dali.pmfm.id.surveyCalculatedLength


## Sprint 6 - v2.6.2-CARTO-RC6

- La base de données Quadrige2 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    - Add synonym on VALIDATION_HISTORY sequence
    - Add procedure UPDATE_CAMPAIGN_PROG

    Pour cela télécharger le module quadrige2-core-server version 3.0.0-RC7 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- Le serveur de synchronisation 3.0.0-RC7 possède les nouvelles options à paramétrer :

        # Procédures stockées
        quadrige2.synchro.server.export.finalize.procedures=UPDATE_CAMPAIGN_PROG(@progCd@); <- procédure de mise à jour de CAMPAIGN_PROG
        quadrige2.synchro.server.export.supportEmail.recipients= <email du support>
        # SMTP settings
        quadrige2.smtp.host=
        quadrige2.smtp.port=
        quadrige2.smtp.username=
        quadrige2.smtp.password=
        quadrige2.smtp.sender=
        quadrige2.smtp.starttls.enable=
        quadrige2.smtp.ssl.enable=


## Sprint 5 - v2.6.2-CARTO-RC5

- La base de données Quadrige2 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    - Add SURVEY.SURVEY_VALID_CM
    - Add VALIDATION_HISTORY table
    - Add QUALIFICATION_HISTORY.QUAL_HIST_OPERATION_DT
    - Add trigger on QUALIFICATION_HISTORY

    Pour cela télécharger le module quadrige2-core-server version 3.0.0-RC6 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 4 - v2.6.2-CARTO-RC4

- La base de données Quadrige2 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    * Ajout séquences pour CAMPAIGN et OCCASION
    * Ajout d'OBJECT_TYPE : RULE_LIST et RULE

    Pour cela télécharger le module quadrige2-core-server version 3.0.0-RC5 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 3 - v2.6.2-CARTO-RC3

- La base de données Quadrige2 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #36834)
    
    Pour cela télécharger le module quadrige2-core-server version 3.0.0-RC2 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


