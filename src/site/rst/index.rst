.. -
.. * #%L
.. * DaLi
.. * %%
.. * Copyright (C) 2014 - 2017 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

====
Home
====

.. image:: ./images/splash.png

Presentation
~~~~~~~~~~~~

Welcome to the technical web site of the Dali application.

**Dali** (Data Litter) aims to collect data on marine lither. It is used in particular in by
french organizations (`Ifremer`_).

.. _Ifremer: http://www.ifremer.fr


Installation
~~~~~~~~~~~~

- Download the `latest release`_ or `another release`_;

- Unzip the downloaded archive in a folder.

.. _latest release: https://www.ifremer.fr/quadrige3_resources/dali/download/dali.zip
.. _another release: https://forge.ifremer.fr/frs/?group_id=252

Use
~~~

Launch
------

- Under Window, click on file **dali.exe**.

- Under linux, launch the file **dali.sh**.


First use
---------

When first used, the application starts and displays the screen of
management of the bases of work. Just install a database via the action
**Install**. The last available database will then be downloaded and installed.

This operation can be long (download time): be patient.

Note: It is also possible to restore a database from a zip file via the action **Restore**.


Need help ?
-----------

To be in the use of the screens of the application, refer to `the user manual`_ (french).

.. _the user manual: http://wwz.ifremer.fr/quadrige2_support


In case of a technical problem, `contact the the support`_ of the Quadrige² administration team.

.. _contact the the support: http://wwz.ifremer.fr/quadrige2_support

Screenshot
~~~~~~~~~~

- Main entry screen:

.. image:: ./images/screenshot_home.png
