.. -
.. * #%L
.. * Dali
.. * %%
.. * Copyright (C) 2014 - 2017 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -


===============
Fonctionnalités
===============

Introduction
------------

Ce document décrit les fonctionnalités de **Dali**, à savoir:

- `Gestionnaire de base`_

Gestionnaire de base
--------------------

Dali utilise une base de travail compatible **quadrige3**, l'application permet
de gérer ces bases, à savoir :

- Installer une base à partir d'une url distante via le mécanisme de mise à jour intégré.
- Mise à jour automatique via le mécanisme de mise à jour intégré (mis à jour des référentiels).
- Archiver les données de Dali (base de travail / pièces-jointes) (sous forme d'archive zip).
- Archiver les données de Dali puis les supprimer , permet alors de pouvoir installer une nouvelle base ou importer des données de Dali.
- Restaurer les données de Dali : permet d'importer les données d'une autre instance de Dali précedemment exportées.

Le format de l'archive d'un export est le suivant :

::

  dali-1.0/
  |-- data
  |   |-- db
  |   |   |-- quadrige3.data
  |   |   |-- quadrige3.log
  |   |   |-- quadrige3.properties
  |   |   |-- quadrige3.script
  |   |   `-- version.appup
  |    `-- meas_files
  |        `-- SAMPLE
  |            |-- OBJ100000
  |            |   `-- SAMPLE-OBJ100000-100002.asc
  |            |-- OBJ100015
  |            |   `-- SAMPLE-OBJ100015-100001.dat
  |            |-- OBJ100018
  |            |   `-- SAMPLE-OBJ100018-100002.dat
  |            |-- OBJ100022
  |            |   `-- SAMPLE-OBJ100022-100003.dat
  |            `-- OBJ100040
  |                `-- SAMPLE-OBJ100040-100004.dat
  `-- config
      |-- dali.config
      `-- version.appup

Pour le moment si vous voulez restaurer une base sans les autres données
(pièces-jointes), il vous suffit alors simplement de créer une
archive zip qui respecte ce format.

Pour utiliser ces fonctionnalités, rendez-vous sur l'écran **Gestionnaire de base**
(Menu fichier -> Gestionnaire de base).

