.. -
.. * #%L
.. * DaLi
.. * %%
.. * Copyright (C) 2017 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -


=======
Accueil
=======

.. image:: ../images/splash.png

Présentation
~~~~~~~~~~~~

Bienvenue sur le site de l'application Dali.

L'objectif du projet **DALI** (DAta LItter) est la mise en place d'un outil de bancarisation
de la thématique des déchets marins dans ses composantes microplastiques et déchets sur les plages
et pourra évoluer ensuite vers de nouvelles sous-thématiques.
L'outil s'intègre dans le système d'information Quadrige.

Les déchets peuvent se situer dans plusieurs compartiments qui font l’objet de suivis différents,
distinguant ainsi plusieurs sous-thématiques :
. Déchets sur les plages
. Déchets en mer en surface
. Déchets en mer sur le fond
. Microplastiques (en mer et sur les plages)
. Déchets ingérés par la faune
. Etranglement de la faune par des déchets.


Installation
~~~~~~~~~~~~

- Télécharger la `dernière version stable`_ de l'application ou `une autre version`_.

- Après téléchargement, décompresser l'archive dans un dossier.

Note : Aucun pré-requis logiciel n'est nécessaire au lancement de Dali. Nous
préconisons cependant d'avoir au moins 1Go de mémoire.

.. _dernière version stable: https://www.ifremer.fr/quadrige3_resources/dali/download/dali.zip
.. _une autre version: https://forge.ifremer.fr/frs/?group_id=322


Utilisation
~~~~~~~~~~~

Lancement
---------

- Sous windows, double-cliquez sur le fichier **dali.exe**.

- Sous linux, exécuter le fichier **dali.sh**.


Première utilisation
--------------------

Lors d'une première utilisation, l'application démarre et affiche l'écran de
gestion des bases de travail. Il suffit alors d'installer une base via l'action
**Installer**. La dernière base disponible sera alors téléchargée puis installer.

A noter que cette opération peut-être longue (temps du téléchargement), soyez
patient.

Une fois la base téléchargée puis installée, l'application est pleinement
fonctionnel.

Note: Il est aussi possible de restaurer une base de données depuis un fichier zip via l'action **Restaurer**.


Besoin d'aide ?
---------------

En cas de problème technique, `contactez le support`_ de la cellule d'administration de Quadrige².

.. _contactez le support: http://wwz.ifremer.fr/quadrige2_support

Captures d'écran
~~~~~~~~~~~~~~~~

- Ecran de saisie principale :

.. image:: ../images/screenshot_home.png