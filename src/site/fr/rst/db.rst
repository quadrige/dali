.. -
.. * #%L
.. * Dali
.. * %%
.. * Copyright (C) 2014 - 2017 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -


==========================================================
Relation entre les versions de l'applicative et de la base
==========================================================


Présentation
------------

L'application permet de faire des migrations de schémas, i.e
que lors de l'ouverture (ou import) d'une base, on vérifie la version du schéma
requit par l'applicatif.

Si la version du schéma de la base est inférieure à celle de l'applicatif, on
propose alors une migration de schéma.

Historique des versions de base
-------------------------------

Le tableau suivant résume les différents liens entre les versions :

+---------------------+-----------------+---------------------------+
| Version Dali        | Version BD Test | Version schema BD         |
+=====================+=================+===========================+
+ X.Y.Z               + 2017.12.13      + 3.0.0.12                  +
+---------------------+-----------------+---------------------------+


Légende :

- *Version Dali* : Version de l'applicatif
- *Version BD Test* : Version de la dernière base utilisée pour les tests unitaires.
- *Version schema BD* : Version du schéma (de la base embarquée HSQLDB) compatible avec l'applicatif.

Documentation
-------------

Voici la documentation disponible, concernant la base de données embarquée :

- `Tables de la base de données`_ embarquée (HSQLDB);
- `Entitées Hibernate`_, utilisées dans le code et les requêtes Hibernate.

.. _Tables de la base de données: ../../quadrige3-core/quadrige3-core-client/hibernate/tables/index.html
.. _Entitées Hibernate: ../../quadrige3-core/quadrige3-core-client/hibernate/entities/index.html
