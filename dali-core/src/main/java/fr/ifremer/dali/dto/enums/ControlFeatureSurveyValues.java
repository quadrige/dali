package fr.ifremer.dali.dto.enums;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.RuleControlAttribute;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.configuration.control.ControlFeatureDTO;
import org.apache.commons.lang3.StringUtils;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * ControlFeatureSurveyValues enum.
 */
public enum ControlFeatureSurveyValues {

    CAMPAIGN(RuleControlAttribute.CAMPAIGN, n("dali.core.enums.featureControlValues.campaign")),
    COMMENT(RuleControlAttribute.COMMENT, n("dali.core.enums.featureControlValues.comment")),
    DATE(RuleControlAttribute.DATE, n("dali.core.enums.featureControlValues.date")),
    CONTROL_DATE(RuleControlAttribute.CONTROL_DATE, n("dali.core.enums.featureControlValues.controlDate")),
    UPDATE_DATE(RuleControlAttribute.UPDATE_DATE, n("dali.core.enums.featureControlValues.updateDate")),
    VALIDATION_DATE(RuleControlAttribute.VALIDATION_DATE, n("dali.core.enums.featureControlValues.validationDate")),
    VALIDATION_COMMENT(RuleControlAttribute.VALIDATION_COMMENT, n("dali.core.enums.featureControlValues.validationComment")),
    QUALIFICATION_DATE(RuleControlAttribute.QUALIFICATION_DATE, n("dali.core.enums.featureControlValues.qualificationDate")),
    QUALIFICATION_COMMENT(RuleControlAttribute.QUALIFICATION_COMMENT, n("dali.core.enums.featureControlValues.qualificationComment")),
    TIME(RuleControlAttribute.TIME, n("dali.core.enums.featureControlValues.time")),
    LATITUDE_MAX_LOCATION(RuleControlAttribute.LATITUDE_MAX_LOCATION, n("dali.core.enums.featureControlValues.latitudeMaxLocation")),
    LATITUDE_MIN_LOCATION(RuleControlAttribute.LATITUDE_MIN_LOCATION, n("dali.core.enums.featureControlValues.latitudeMinLocation")),
    LATITUDE_REAL_SURVEY(RuleControlAttribute.SURVEY_LATITUDE_REAL, n("dali.core.enums.featureControlValues.latitudeReal")),
    LONGITUDE_MAX_LOCATION(RuleControlAttribute.LONGITUDE_MAX_LOCATION, n("dali.core.enums.featureControlValues.longitudeMaxLocation")),
    LONGITUDE_MIN_LOCATION(RuleControlAttribute.LONGITUDE_MIN_LOCATION, n("dali.core.enums.featureControlValues.longitudeMinLocation")),
    LONGITUDE_REAL_SURVEY(RuleControlAttribute.SURVEY_LONGITUDE_REAL, n("dali.core.enums.featureControlValues.longitudeReal")),
    NAME(RuleControlAttribute.NAME, n("dali.core.enums.featureControlValues.name")),
    DEPARTMENT(RuleControlAttribute.RECORDER_DEPARTMENT, n("dali.core.enums.featureControlValues.department")),
    POSITIONING(RuleControlAttribute.POSITIONING, n("dali.core.enums.featureControlValues.positioning")),
    POSITIONING_PRECISION(RuleControlAttribute.POSITIONING_PRECISION, n("dali.core.enums.featureControlValues.positioningPrecision")),
    BOTTOM_DEPTH(RuleControlAttribute.BOTTOM_DEPTH, n("dali.core.enums.featureControlValues.bottomDepth")),
    PROGRAM(RuleControlAttribute.PROGRAM, n("dali.core.enums.featureControlValues.program")),
    LOCATION(RuleControlAttribute.LOCATION, n("dali.core.enums.featureControlValues.location")),
    OBSERVERS(RuleControlAttribute.OBSERVERS, n("dali.core.enums.featureControlValues.observers"));

    private final RuleControlAttribute ruleControlAttribute;
    private final String keyLabel;

    ControlFeatureSurveyValues(RuleControlAttribute ruleControlAttribute, String key) {
        this.ruleControlAttribute = ruleControlAttribute;
        this.keyLabel = key;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return this.ruleControlAttribute.getValue();
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.configuration.control.ControlFeatureDTO} object.
     */
    public ControlFeatureDTO toControlFeatureDTO() {
        ControlFeatureDTO dto = DaliBeanFactory.newControlFeatureDTO();
        dto.setId(ordinal());
        dto.setCode(getCode());
        dto.setName(getLabel());
        return dto;
    }

    /**
     * <p>equals.</p>
     *
     * @param controlFeature a {@link fr.ifremer.dali.dto.configuration.control.ControlFeatureDTO} object.
     * @return a boolean.
     */
    public boolean equals(ControlFeatureDTO controlFeature) {
        return controlFeature != null && getCode().equals(controlFeature.getCode());
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.dali.dto.configuration.control.ControlFeatureDTO} object.
     */
    public static ControlFeatureDTO toControlFeatureDTO(String code) {
        ControlFeatureSurveyValues value = getByCode(code);
        if (value != null) {
            return value.toControlFeatureDTO();
        }
        return null;
    }

    /**
     * <p>getByCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.dali.dto.enums.ControlFeatureSurveyValues} object.
     */
    public static ControlFeatureSurveyValues getByCode(String code) {
        for (final ControlFeatureSurveyValues enumValue : ControlFeatureSurveyValues.values()) {
            if (StringUtils.isNotBlank(enumValue.getCode()) && enumValue.getCode().equals(code)) {
                return enumValue;
            }
        }
        return null;
    }
}
