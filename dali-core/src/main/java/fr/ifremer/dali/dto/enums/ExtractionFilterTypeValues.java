package fr.ifremer.dali.dto.enums;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.filter.FilterTypeId;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Extraction filter enum.
 */
public enum ExtractionFilterTypeValues {

    PERIOD(null, n("dali.core.enums.extractionFilter.period"), false),
    PROGRAM(FilterTypeId.PROGRAM, n("dali.core.enums.extractionFilter.program"), false),
    LOCATION(FilterTypeId.MONITORING_LOCATION, n("dali.core.enums.extractionFilter.location"), false),
    CAMPAIGN(FilterTypeId.CAMPAIGN, n("dali.core.enums.extractionFilter.campaign"), false),
    TAXON(FilterTypeId.TAXON_NAME, n("dali.core.enums.extractionFilter.taxon"), false),
    TAXON_GROUP(FilterTypeId.TAXON_GROUP, n("dali.core.enums.extractionFilter.taxonGroup"), false),
    SAMPLING_EQUIPMENT(FilterTypeId.SAMPLING_EQUIPMENT, n("dali.core.enums.extractionFilter.samplingEquipment"), false),
    DEPARTMENT(FilterTypeId.DEPARTMENT, n("dali.core.enums.extractionFilter.department"), false),
    PMFM(FilterTypeId.PMFM, n("dali.core.enums.extractionFilter.pmfm"), false),
    ORDER_ITEM_TYPE(FilterTypeId.ORDER_ITEM_TYPE, n("dali.core.enums.extractionFilter.orderItemType"), true);

    private final FilterTypeId filterTypeId;
    private final String keyLabel;
    private final boolean hidden;

    ExtractionFilterTypeValues(FilterTypeId filterType, String keyLabel, boolean hidden) {
        this.filterTypeId = filterType;
        this.keyLabel = keyLabel;
        this.hidden = hidden;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>filterTypeId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFilterTypeId() {
        return this.filterTypeId != null ? this.filterTypeId.getValue() : -1;
    }

    /**
     * <p>isHidden.</p>
     *
     * @return a boolean.
     */
    public boolean isHidden() {
        return this.hidden;
    }

    /**
     * <p>getExtractionFilter.</p>
     *
     * @param filterTypeId a {@link java.lang.Integer} object.
     * @return a {@link ExtractionFilterTypeValues} object.
     */
    public static ExtractionFilterTypeValues getExtractionFilterType(final Integer filterTypeId) {
        for (final ExtractionFilterTypeValues value : values()) {
            if (value.getFilterTypeId().equals(filterTypeId)) {
                return value;
            }
        }
        return null;
    }
}
