package fr.ifremer.dali.dto.data;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Interface to aware bean to get the bean coordinate in 2 dimensions
 * <p/>
 * Created by Ludovic on 14/06/2017.
 */
public interface Coordinate2DAware {

    /** Constant <code>PROPERTY_LATITUDE_MIN="latitudeMin"</code> */
    String PROPERTY_LATITUDE_MIN = "latitudeMin";
    /** Constant <code>PROPERTY_LATITUDE_MAX="latitudeMax"</code> */
    String PROPERTY_LATITUDE_MAX = "latitudeMax";
    /** Constant <code>PROPERTY_LONGITUDE_MIN="longitudeMin"</code> */
    String PROPERTY_LONGITUDE_MIN = "longitudeMin";
    /** Constant <code>PROPERTY_LONGITUDE_MAX="longitudeMax"</code> */
    String PROPERTY_LONGITUDE_MAX = "longitudeMax";

    /**
     * <p>getLatitudeMin.</p>
     *
     * @return a {@link Double} object.
     */
    Double getLatitudeMin();

    /**
     * <p>setLatitudeMin.</p>
     *
     * @param latitudeMin a {@link Double} object.
     */
    void setLatitudeMin(Double latitudeMin);

    /**
     * <p>getLatitudeMax.</p>
     *
     * @return a {@link Double} object.
     */
    Double getLatitudeMax();

    /**
     * <p>setLatitudeMax.</p>
     *
     * @param latitudeMax a {@link Double} object.
     */
    void setLatitudeMax(Double latitudeMax);

    /**
     * <p>getLongitudeMin.</p>
     *
     * @return a {@link Double} object.
     */
    Double getLongitudeMin();

    /**
     * <p>setLongitudeMin.</p>
     *
     * @param longitudeMin a {@link Double} object.
     */
    void setLongitudeMin(Double longitudeMin);

    /**
     * <p>getLongitudeMax.</p>
     *
     * @return a {@link Double} object.
     */
    Double getLongitudeMax();

    /**
     * <p>setLongitudeMax.</p>
     *
     * @param longitudeMax a {@link Double} object.
     */
    void setLongitudeMax(Double longitudeMax);

}
