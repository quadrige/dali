package fr.ifremer.dali.dto;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

//import com.google.common.collect.*;

import com.google.common.collect.Multimap;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.configuration.moratorium.MoratoriumPmfmDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.data.measurement.MeasurementAware;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.enums.*;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.synchro.vo.SynchroDateOperatorVO;
import fr.ifremer.quadrige3.ui.core.dto.CodeOnly;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeans;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.sql.Array;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * useful methods around Dali bean.
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 * <p>
 * TODO move common methods to fr.ifremer.quadrige3.core.dao.technical.Beans
 */
public class DaliBeans extends QuadrigeBeans {

    /**
     * <p>Constructor for DaliBeans.</p>
     */
    protected DaliBeans() {
        // helper class does not instantiate
    }

    /**
     * <p>toString.</p>
     *
     * @param beans a {@link Collection} object.
     * @return a {@link String} object.
     */
    public static String toString(Collection<? extends QuadrigeBean> beans) {
        return beans != null
                ? transformCollection(beans, DaliBeans::toString).stream().collect(Collectors.joining(",", "[", "]"))
                : "[]";
    }

    /**
     * Utility method to represent a bean as String where a decorator is not declared (for dates)
     *
     * @param bean a {@link QuadrigeBean} object.
     * @return a {@link String} object.
     */
    public static String toString(QuadrigeBean bean) {

        // try to represent a bean to a string
        if (bean instanceof SurveyDTO) {
            SurveyDTO survey = (SurveyDTO) bean;
            final StringBuilder result = new StringBuilder(50);
            if (survey.getLocation() != null) {
                result.append(survey.getLocation().getName());
            }
            if (survey.getDate() != null) {
                result.append(DEFAULT_STRING_SEPARATOR);
                result.append(survey.getDate().format(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)));
            }
            if (survey.getTime() != null) {
                result.append(DEFAULT_STRING_SEPARATOR);
                result.append(Times.secondsToString(survey.getTime()));
            }
            if (survey.getProgram() != null) {
                result.append(DEFAULT_STRING_SEPARATOR);
                result.append(survey.getProgram().getCode());
            }
            if (survey.getName() != null) {
                result.append(DEFAULT_STRING_SEPARATOR);
                result.append(survey.getName());
            }
            return result.toString();

        } else if (bean instanceof CampaignDTO) {
            CampaignDTO campaign = (CampaignDTO) bean;
            final StringBuilder result = new StringBuilder(50);
            result.append(campaign.getName()).append(DEFAULT_STRING_SEPARATOR).append(campaign.getStartDate().format(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)));
            if (campaign.getEndDate() != null) {
                result.append(" => ").append(campaign.getEndDate().format(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)));
            }
            return result.toString();

        } else if (bean instanceof ExtractionPeriodDTO) {
            ExtractionPeriodDTO period = (ExtractionPeriodDTO) bean;
            return Dates.formatDate(period.getStartDate(), DEFAULT_DATE_FORMAT)
                    + " => "
                    + Dates.formatDate(period.getEndDate(), DEFAULT_DATE_FORMAT);
        }

        return bean != null ? bean.toString() : "null";
    }

    public static List<MeasurementDTO> duplicate(List<MeasurementDTO> measurements) {
        if (measurements == null)
            return new ArrayList<>();
        return measurements.stream().map(DaliBeans::duplicate).collect(Collectors.toList());
    }

    public static MeasurementDTO duplicate(MeasurementDTO measurement) {
        // Clone object
        MeasurementDTO clone = clone(measurement);

        // Remove Id & individualId
        clone.setId(null);
        clone.setIndividualId(null);

        // remove errors
        clone.setErrors(null);

        return clone;
    }

    public static SamplingOperationDTO duplicate(SamplingOperationDTO samplingOperation) {

        // Duplicate sampling operation
        final SamplingOperationDTO clone = DaliBeans.clone(samplingOperation);

        // Remove ID
        clone.setId(null);

        // duplicate coordinate
        clone.setCoordinate(DaliBeans.clone(samplingOperation.getCoordinate()));

        // remove errors
        clone.setErrors(null);

        // duplicate measurements
        clone.setMeasurements(DaliBeans.duplicate(samplingOperation.getMeasurements()));
        clone.setIndividualMeasurements(DaliBeans.duplicate(samplingOperation.getIndividualMeasurements()));

        return clone;

    }

    /**
     * <p>filterNotEmptyAppliedPeriod.</p>
     *
     * @param appliedPeriods a {@link Collection} object.
     * @return a {@link Collection} object.
     */
    public static Collection<AppliedStrategyDTO> filterNotEmptyAppliedPeriod(Collection<AppliedStrategyDTO> appliedPeriods) {
        if (appliedPeriods == null) {
            return null;
        }
        return filterCollection(appliedPeriods, input -> input != null && (input.getStartDate() != null || input.getEndDate() != null || input.getSamplingDepartment() != null));
//                Collections2.filter(appliedPeriods, input -> input != null && (input.getStartDate() != null || input.getEndDate() != null || input.getSamplingDepartment() != null));
    }

    /**
     * <p>getDateOperator.</p>
     *
     * @param searchDate a {@link SearchDateDTO} object.
     * @return a {@link SynchroDateOperatorVO} object.
     */
    public static SynchroDateOperatorVO getDateOperator(SearchDateDTO searchDate) {
        if (searchDate != null) {
            SearchDateValues searchDateValue = SearchDateValues.values()[searchDate.getId()];
            switch (searchDateValue) {

                case EQUALS:
                    return SynchroDateOperatorVO.EQUALS;
                case BETWEEN:
                    return SynchroDateOperatorVO.BETWEEN;
                case BEFORE:
                    return SynchroDateOperatorVO.BEFORE;
                case BEFORE_OR_EQUALS:
                    return SynchroDateOperatorVO.BEFORE_OR_EQUALS;
                case AFTER:
                    return SynchroDateOperatorVO.AFTER;
                case AFTER_OR_EQUALS:
                    return SynchroDateOperatorVO.AFTER_OR_EQUALS;
            }
        }
        return null;
    }

    /**
     * <p>locationToAppliedStrategyDTO.</p>
     *
     * @param location a {@link LocationDTO} object.
     * @return a {@link AppliedStrategyDTO} object.
     */
    public static AppliedStrategyDTO locationToAppliedStrategyDTO(LocationDTO location) {
        if (location == null) {
            return null;
        }
        AppliedStrategyDTO result = DaliBeanFactory.newAppliedStrategyDTO();
        result.setId(location.getId());
        result.setLabel(location.getLabel());
        result.setName(location.getName());
        result.setComment(location.getComment());
        result.setGrouping(location.getGrouping());
        result.setStatus(location.getStatus());
        return result;
    }

    /**
     * <p>locationsToAppliedStrategyDTOs.</p>
     *
     * @param locations a {@link List} object.
     * @return a {@link List} object.
     */
    public static List<AppliedStrategyDTO> locationsToAppliedStrategyDTOs(List<LocationDTO> locations) {

        List<AppliedStrategyDTO> result = null;
        if (locations != null) {
            result = locations.stream().map(DaliBeans::locationToAppliedStrategyDTO).collect(Collectors.toList());
        }
        return result;
    }

    public static PmfmStrategyDTO pmfmToPmfmStrategy(PmfmDTO pmfm) {
        if (pmfm == null)
            return null;
        PmfmStrategyDTO result = DaliBeanFactory.newPmfmStrategyDTO();
        result.setPmfm(pmfm);
        result.addAllQualitativeValues(pmfm.getQualitativeValues());
        return result;
    }

    public static RulePmfmDTO pmfmToRulePmfm(PmfmDTO pmfm) {
        if (pmfm == null)
            return null;
        RulePmfmDTO rulePmfm = DaliBeanFactory.newRulePmfmDTO();
        rulePmfm.setPmfm(pmfm);
        return rulePmfm;
    }

    /**
     * <p>filterPmfm.</p>
     *
     * @param pmfms           a {@link Collection} object.
     * @param pmfmIdsToIgnore a {@link Collection} object.
     * @return a {@link List} object.
     */
    public static List<PmfmDTO> filterPmfm(Collection<PmfmDTO> pmfms, final Collection<Integer> pmfmIdsToIgnore) {
        return filterCollection(pmfms, input -> input != null && !pmfmIdsToIgnore.contains(input.getId()));
    }

    public static List<PmfmDTO> filterQualitativePmfms(Collection<PmfmDTO> pmfms) {
        return pmfms.stream().filter(pmfm -> pmfm.getParameter().isQualitative() && !pmfm.isQualitativeValuesEmpty()).collect(Collectors.toList());
    }

    public static List<PmfmDTO> filterNumericalPmfms(Collection<PmfmDTO> pmfms) {
        return pmfms.stream().filter(pmfm -> !pmfm.getParameter().isQualitative()).collect(Collectors.toList());
    }

    /**
     * Populates PMFMs ans individual PMFMs from existing measurements and individual measurements
     *
     * @param bean a {@link MeasurementAware} object.
     */
    public static void populatePmfmsFromMeasurements(MeasurementAware bean) {

        // populate pmfms from ungrouped measurements
        if (CollectionUtils.isNotEmpty(bean.getMeasurements()) && bean.getPmfms() != null) {
            for (MeasurementDTO measurement : bean.getMeasurements()) {
                // ignore individual measurement
                if (measurement.getIndividualId() != null) continue;
                if (measurement.getPmfm() != null && !bean.getPmfms().contains(measurement.getPmfm())) {
                    bean.getPmfms().add(measurement.getPmfm());
                }
            }
        }

        // populate individual pmfms from grouped (individual) measurements
        if (CollectionUtils.isNotEmpty(bean.getIndividualMeasurements()) && bean.getIndividualPmfms() != null) {
            for (MeasurementDTO measurement : bean.getIndividualMeasurements()) {
                // ignore non-individual measurement
                if (measurement.getIndividualId() == null) continue;
                if (measurement.getPmfm() != null && !bean.getIndividualPmfms().contains(measurement.getPmfm())) {
                    bean.getIndividualPmfms().add(measurement.getPmfm());
                }
            }
        }

    }

    /**
     * Populates measurements and individualMeasurements from existing measurements of bean AND create empty measurements from PMFMs of bean<br>
     * But don't add them to bean
     *
     * @param bean                   the source
     * @param measurements           the output collection of measurements
     * @param individualMeasurements the output collection of individual measurements
     */
    public static void populateMeasurementsFromPmfms(MeasurementAware bean, Collection<MeasurementDTO> measurements, Collection<MeasurementDTO> individualMeasurements) {

        Multimap<PmfmDTO, MeasurementDTO> existingMeasurementsByPmfm = populateByProperty(bean.getMeasurements(), MeasurementDTO.PROPERTY_PMFM);
        Multimap<PmfmDTO, MeasurementDTO> existingIndividualMeasurementsByPmfm = populateByProperty(bean.getIndividualMeasurements(), MeasurementDTO.PROPERTY_PMFM);
        List<Integer> existingIndividualIds = collectProperties(bean.getIndividualMeasurements(), MeasurementDTO.PROPERTY_INDIVIDUAL_ID);

        for (PmfmDTO pmfm : bean.getPmfms()) {
            Collection<MeasurementDTO> existingMeasurements = existingMeasurementsByPmfm.get(pmfm);
            if (CollectionUtils.isEmpty(existingMeasurements)) {
                MeasurementDTO measurement = DaliBeanFactory.newMeasurementDTO();
                measurement.setPmfm(pmfm);
                measurements.add(measurement);
            } else {
                measurements.addAll(existingMeasurements);
            }
        }

        for (PmfmDTO individualPmfm : bean.getIndividualPmfms()) {

            // get all existing individual measurements
            List<MeasurementDTO> existingIndividualMeasurements = new ArrayList<>(existingIndividualMeasurementsByPmfm.get(individualPmfm));

            // build all necessary measurement by individual id (with empty measurements if needed)
            existingIndividualIds.stream().distinct().forEach(individualId -> {

                MeasurementDTO existingIndividualMeasurement = findByProperty(existingIndividualMeasurements, MeasurementDTO.PROPERTY_INDIVIDUAL_ID, individualId);
                if (existingIndividualMeasurement == null) {
                    // create empty measurement
                    existingIndividualMeasurement = DaliBeanFactory.newMeasurementDTO();
                    existingIndividualMeasurement.setPmfm(individualPmfm);
                    existingIndividualMeasurement.setIndividualId(individualId);

                    // try to get other properties from another measurement in same individualId
                    MeasurementDTO anotherIndividualMeasurement = findByProperty(bean.getIndividualMeasurements(), MeasurementDTO.PROPERTY_INDIVIDUAL_ID, individualId);
                    if (anotherIndividualMeasurement != null) {
                        existingIndividualMeasurement.setTaxonGroup(anotherIndividualMeasurement.getTaxonGroup());
                        existingIndividualMeasurement.setTaxon(anotherIndividualMeasurement.getTaxon());
                        existingIndividualMeasurement.setInputTaxonId(anotherIndividualMeasurement.getInputTaxonId());
                        existingIndividualMeasurement.setInputTaxonName(anotherIndividualMeasurement.getInputTaxonName());
                        existingIndividualMeasurement.setAnalyst(anotherIndividualMeasurement.getAnalyst());
                    }

                    existingIndividualMeasurements.add(existingIndividualMeasurement);
                }
            });

            // add all measurements (with empty)
            individualMeasurements.addAll(existingIndividualMeasurements);
        }

    }

    /**
     * Create empty measurements on bean, based on pmfms ans idividualPmfms of bean.
     * This will clear existing measurements first
     *
     * @param bean a {@link MeasurementAware} object.
     */
    public static void createEmptyMeasurements(MeasurementAware bean) {

        bean.getMeasurements().clear();
        for (PmfmDTO pmfm : bean.getPmfms()) {
            MeasurementDTO measurement = DaliBeanFactory.newMeasurementDTO();
            measurement.setPmfm(pmfm);
            bean.getMeasurements().add(measurement);
        }

        bean.getIndividualMeasurements().clear();
        for (PmfmDTO individualPmfm : bean.getIndividualPmfms()) {
            MeasurementDTO individualMeasurement = DaliBeanFactory.newMeasurementDTO();
            individualMeasurement.setPmfm(individualPmfm);
            bean.getIndividualMeasurements().add(individualMeasurement);
        }
    }

    /**
     * <p>isTaxonMeasurement.</p>
     *
     * @param measurement a {@link MeasurementDTO} object.
     * @return a boolean.
     */
    public static boolean isTaxonMeasurement(MeasurementDTO measurement) {
        return measurement.getTaxonGroup() != null || measurement.getTaxon() != null || measurement.getInputTaxonId() != null;
    }

    /**
     * <p>isMeasurementEmpty.</p>
     *
     * @param measurement a {@link MeasurementDTO} object.
     * @return a boolean.
     */
    public static boolean isMeasurementEmpty(MeasurementDTO measurement) {
        return measurement == null || (measurement.getNumericalValue() == null && measurement.getQualitativeValue() == null);
    }

    public static boolean isNumericalMeasurementEmpty(MeasurementDTO measurement) {
        return measurement == null || measurement.getNumericalValue() == null;
    }

    public static final String PROPERTY_PMFM_ID = MeasurementDTO.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_ID;

    public static MeasurementDTO getMeasurementByPmfmId(MeasurementAware bean, int pmfmId) {
        if (CollectionUtils.isNotEmpty(bean.getMeasurements())) {
            return findByProperty(bean.getMeasurements(), PROPERTY_PMFM_ID, pmfmId);
        }
        return null;
    }

    public static MeasurementDTO getIndividualMeasurementByPmfmId(MeasurementAware bean, int pmfmId) {
        if (CollectionUtils.isNotEmpty(bean.getIndividualMeasurements())) {
            return findByProperty(bean.getIndividualMeasurements(), PROPERTY_PMFM_ID, pmfmId);
        }
        return null;
    }

    /**
     * Compare measurement values only (not pmfm or other propoerty)
     *
     * @param measurement1 first measurement
     * @param measurement2 second measurement
     * @return true if their values are equals or null
     */
    public static boolean measurementValuesEquals(@Nonnull MeasurementDTO measurement1, @Nonnull MeasurementDTO measurement2) {
        return (measurement1.getNumericalValue() == null && measurement2.getNumericalValue() == null
            && measurement1.getQualitativeValue() == null && measurement2.getQualitativeValue() == null)
            || (measurement1.getNumericalValue() != null && measurement1.getNumericalValue().equals(measurement2.getNumericalValue()))
            || (measurement1.getQualitativeValue() != null && measurement1.getQualitativeValue().equals(measurement2.getQualitativeValue()));
    }

    /**
     * Get the unified comments from all individual measurements
     *
     * @param bean the MeasurementAware bean to proceed
     * @return the unified comment
     */
    public static String getUnifiedCommentFromIndividualMeasurements(MeasurementAware bean) {

        // collect comments
        List<String> comments = collectProperties(bean.getIndividualMeasurements(), MeasurementDTO.PROPERTY_COMMENT);

        return getUnifiedString(comments, DEFAULT_STRING_SEPARATOR);
    }

    /**
     * Method used as external function for HSQLDB
     * see fr.ifremer.dali.service.extraction.ExtractionServiceImpl#createConcatDistinctFunction(java.lang.String, java.lang.String)
     *
     * @param array     the input array (any type)
     * @param separator the separator to use
     * @return the unified string
     */
    @SuppressWarnings(value = "unused")
    public static String getUnifiedSQLString(Array array, String separator) {

        try {

            return getUnifiedString(
                    Arrays.stream((Object[]) array.getArray())
                            .filter(Objects::nonNull)
                            .map(Object::toString)
                            .collect(Collectors.toList()),
                    separator);

        } catch (SQLException e) {

            return "";
        }
    }

    /**
     * Get the unified string from a list of string
     * <p>
     * It concat all unique occurrence of string in the list.
     * If an item already contains a joined string with the separator, the inner items are splitted before.
     *
     * @param strings   the list to proceed
     * @param separator the separator to use
     * @return the unified string
     */
    public static String getUnifiedString(List<String> strings, String separator) {

        Set<String> stringSet = new LinkedHashSet<>();

        for (String string : strings) {
            if (StringUtils.isNotBlank(string)) {

                // split already joined comment
                if (string.contains(separator)) {
                    List<String> subComments = split(string, separator);
                    stringSet.addAll(subComments);
                } else {
                    stringSet.add(string);
                }
            }
        }

        return stringSet.stream().filter(Objects::nonNull).collect(Collectors.joining(separator));
    }

    /**
     * return true only if bean has blocking errors (control errors and warnings are not blocking)
     *
     * @param bean a {@link ErrorAware} object.
     * @return a boolean.
     */
    public static boolean hasNoBlockingError(ErrorAware bean) {
        if (bean != null && CollectionUtils.isNotEmpty(bean.getErrors())) {
            for (ErrorDTO error : bean.getErrors()) {
                if (error.isError() && !error.isControl()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <p>removeBlockingErrors.</p>
     *
     * @param bean a {@link ErrorAware} object.
     */
    public static void removeBlockingErrors(ErrorAware bean) {
        bean.getErrors().removeIf(error -> !error.isControl());
    }

    /**
     * <p>getErrors.</p>
     *
     * @param bean        a {@link ErrorAware} object.
     * @param controlOnly a {@link Boolean} object.
     * @return a {@link List} object.
     */
    public static List<ErrorDTO> getErrors(ErrorAware bean, final Boolean controlOnly) {
        if (bean == null || CollectionUtils.isEmpty(bean.getErrors())) {
            return new ArrayList<>();
        }
        return filterCollection(bean.getErrors(), error -> error.isError() && (controlOnly == null || (controlOnly == error.isControl())));
    }

    /**
     * <p>getErrorMessages.</p>
     *
     * @param bean a {@link ErrorAware} object.
     * @return a {@link List} object.
     */
    public static List<String> getErrorMessages(ErrorAware bean) {
        return collectProperties(getErrors(bean, null), ErrorDTO.PROPERTY_MESSAGE);
    }

    /**
     * <p>getErrors.</p>
     *
     * @param bean         a {@link ErrorAware} object.
     * @param propertyName a {@link String} object.
     * @param pmfmId       a {@link Integer} object.
     * @param controlOnly  a {@link Boolean} object.
     * @return a {@link List} object.
     */
    public static List<ErrorDTO> getErrors(ErrorAware bean, final String propertyName, final Integer pmfmId, final Boolean controlOnly) {
        if (bean == null || CollectionUtils.isEmpty(bean.getErrors())) {
            return new ArrayList<>();
        }
        return filterCollection(bean.getErrors(), error -> error.isError()
                && (controlOnly == null || (controlOnly == error.isControl()))
                && error.containsPropertyName(propertyName)
                && (pmfmId == null || pmfmId.equals(error.getPmfmId()))
        );
    }

    /**
     * <p>getErrorMessages.</p>
     *
     * @param bean         a {@link ErrorAware} object.
     * @param propertyName a {@link String} object.
     * @param pmfmId       a {@link Integer} object.
     * @return a {@link List} object.
     */
    public static List<String> getErrorMessages(ErrorAware bean, String propertyName, Integer pmfmId) {
        return collectProperties(getErrors(bean, propertyName, pmfmId, null), ErrorDTO.PROPERTY_MESSAGE);
    }

    /**
     * <p>getWarnings.</p>
     *
     * @param bean        a {@link ErrorAware} object.
     * @param controlOnly a {@link Boolean} object.
     * @return a {@link List} object.
     */
    public static List<ErrorDTO> getWarnings(ErrorAware bean, final Boolean controlOnly) {
        if (bean == null || CollectionUtils.isEmpty(bean.getErrors())) {
            return new ArrayList<>();
        }
        return filterCollection(bean.getErrors(), error -> error.isWarning() && (controlOnly == null || (controlOnly == error.isControl())));
    }

    /**
     * <p>getWarningMessages.</p>
     *
     * @param bean a {@link ErrorAware} object.
     * @return a {@link List} object.
     */
    public static List<String> getWarningMessages(ErrorAware bean) {
        return collectProperties(getWarnings(bean, null), ErrorDTO.PROPERTY_MESSAGE);
    }

    /**
     * <p>getWarnings.</p>
     *
     * @param bean         a {@link ErrorAware} object.
     * @param propertyName a {@link String} object.
     * @param pmfmId       a {@link Integer} object.
     * @param controlOnly  a {@link Boolean} object.
     * @return a {@link List} object.
     */
    public static List<ErrorDTO> getWarnings(ErrorAware bean, final String propertyName, final Integer pmfmId, final Boolean controlOnly) {
        if (bean == null || CollectionUtils.isEmpty(bean.getErrors())
                // if an error is found, return empty list because error is priority
                || !getErrors(bean, propertyName, pmfmId, controlOnly).isEmpty()) {
            return new ArrayList<>();
        }
        return filterCollection(bean.getErrors(), error -> error.isWarning()
                && (controlOnly == null || (controlOnly == error.isControl()))
                && error.containsPropertyName(propertyName)
                && (pmfmId == null || pmfmId.equals(error.getPmfmId())));
    }

    /**
     * <p>getWarningMessages.</p>
     *
     * @param bean         a {@link ErrorAware} object.
     * @param propertyName a {@link String} object.
     * @param pmfmId       a {@link Integer} object.
     * @return a {@link List} object.
     */
    public static List<String> getWarningMessages(ErrorAware bean, String propertyName, Integer pmfmId) {
        return collectProperties(getWarnings(bean, propertyName, pmfmId, null), ErrorDTO.PROPERTY_MESSAGE);
    }

    /**
     * <p>addError.</p>
     *
     * @param bean       a {@link ErrorAware} object.
     * @param message    a {@link String} object.
     * @param properties a {@link String} object.
     */
    public static void addError(ErrorAware bean, String message, String... properties) {
        addError(bean, message, null, properties);
    }

    /**
     * <p>addError.</p>
     *
     * @param bean       a {@link ErrorAware} object.
     * @param message    a {@link String} object.
     * @param pmfmId     a {@link Integer} object.
     * @param properties a {@link String} object.
     */
    public static void addError(ErrorAware bean, String message, Integer pmfmId, String... properties) {
        addErrorDTOToBean(bean, false, message, pmfmId, properties);
    }

    /**
     * <p>addWarning.</p>
     *
     * @param bean       a {@link ErrorAware} object.
     * @param message    a {@link String} object.
     * @param properties a {@link String} object.
     */
    public static void addWarning(ErrorAware bean, String message, String... properties) {
        addWarning(bean, message, null, properties);
    }

    /**
     * <p>addWarning.</p>
     *
     * @param bean       a {@link ErrorAware} object.
     * @param message    a {@link String} object.
     * @param pmfmId     a {@link Integer} object.
     * @param properties a {@link String} object.
     */
    public static void addWarning(ErrorAware bean, String message, Integer pmfmId, String... properties) {
        addErrorDTOToBean(bean, true, message, pmfmId, properties);
    }

    private static void addErrorDTOToBean(ErrorAware bean, boolean warning, String message, Integer pmfmId, String... properties) {
        ErrorDTO error = DaliBeanFactory.newErrorDTO();
        error.setError(!warning);
        error.setWarning(warning);
        error.setMessage(message);
        error.setPropertyName(properties != null ? Arrays.asList(properties) : null);
        error.setPmfmId(pmfmId);
        bean.getErrors().add(error);
    }

    /**
     * <p>addUniqueErrors.</p>
     *
     * @param bean   a {@link ErrorAware} object.
     * @param errors a {@link Collection} object.
     */
    public static void addUniqueErrors(ErrorAware bean, Collection<ErrorDTO> errors) {

        if (CollectionUtils.isNotEmpty(errors)) {
            for (ErrorDTO errorToAdd : errors) {
                boolean canAdd = true;
                for (ErrorDTO existingError : bean.getErrors()) {
                    if (isErrorEquals(errorToAdd, existingError)) {
                        canAdd = false;
                        break;
                    }
                }
                if (canAdd) {
                    bean.getErrors().add(errorToAdd);
                }
            }
        }

    }

    private static boolean isErrorEquals(ErrorDTO error1, ErrorDTO error2) {
        return error1.isError() == error2.isError()
                && error1.isWarning() == error2.isWarning()
                && error1.isControl() == error2.isControl()
                && Objects.equals(error1.getControlElementCode(), error2.getControlElementCode())
                && Objects.deepEquals(error1.getPropertyName().toArray(), error2.getPropertyName().toArray())
                && Objects.equals(error1.getPmfmId(), error2.getPmfmId())
                && Objects.equals(error1.getIndividualId(), error2.getIndividualId())
                && Objects.equals(error1.getMessage(), error2.getMessage());
    }

    /**
     * <p>isPmfmMandatory.</p>
     *
     * @param controlRule a {@link ControlRuleDTO} object.
     * @return a boolean.
     */
    public static boolean isPmfmMandatory(ControlRuleDTO controlRule) {
        return (ControlElementValues.MEASUREMENT.equals(controlRule.getControlElement())
                || ControlElementValues.TAXON_MEASUREMENT.equals(controlRule.getControlElement())) &&
                (ControlFeatureMeasurementValues.PMFM.equals(controlRule.getControlFeature())
                        || ControlFeatureMeasurementValues.NUMERICAL_VALUE.equals(controlRule.getControlFeature())
                        || ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(controlRule.getControlFeature()));
    }

    public static boolean isPreconditionRule(ControlRuleDTO controlRule) {
        return ControlFunctionValues.PRECONDITION.equals(controlRule.getFunction());
    }

    public static boolean isQualitativeControlRule(ControlRuleDTO controlRule) {
        return ControlFunctionValues.IS_AMONG.equals(controlRule.getFunction())
                && (
                (ControlElementValues.MEASUREMENT.equals(controlRule.getControlElement()) && ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(controlRule.getControlFeature()))
                        ||
                        (ControlElementValues.TAXON_MEASUREMENT.equals(controlRule.getControlElement()) && ControlFeatureTaxonMeasurementValues.QUALITATIVE_VALUE.equals(controlRule.getControlFeature()))
        );
    }

    /**
     * <p>isSurveyValidated.</p>
     *
     * @param survey a {@link SurveyDTO} object.
     * @return a boolean.
     */
    public static boolean isSurveyValidated(SurveyDTO survey) {
        return survey.getValidationDate() != null
                || SynchronizationStatusValues.SYNCHRONIZED.equals(survey.getSynchronizationStatus())
                || SynchronizationStatusValues.READY_TO_SYNCHRONIZE.equals(survey.getSynchronizationStatus());
    }

    /**
     * <p>getFilterOfType.</p>
     *
     * @param extraction       a {@link ExtractionDTO} object.
     * @param extractionFilter a {@link ExtractionFilterTypeValues} object.
     * @return a {@link FilterDTO} object.
     */
    public static FilterDTO getFilterOfType(ExtractionDTO extraction, ExtractionFilterTypeValues extractionFilter) {

        return CollectionUtils.isNotEmpty(extraction.getFilters())
                ? findByProperty(extraction.getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID, extractionFilter.getFilterTypeId())
                : null;
    }

    /**
     * <p>getFilterElementsIds.</p>
     *
     * @param extraction       a {@link ExtractionDTO} object.
     * @param extractionFilter a {@link ExtractionFilterTypeValues} object.
     * @param <T>              a T object.
     * @return a {@link List} object.
     */
    public static <T> List<T> getFilterElementsIds(ExtractionDTO extraction, ExtractionFilterTypeValues extractionFilter) {

        FilterDTO filter = getFilterOfType(extraction, extractionFilter);
        if (filter != null && CollectionUtils.isNotEmpty(filter.getElements())) {
            QuadrigeBean firstElement = filter.getElements().get(0);
            String property = firstElement instanceof CodeOnly ? ProgramDTO.PROPERTY_CODE : QuadrigeBean.PROPERTY_ID;
            return collectProperties(filter.getElements(), property);
        }
        return null;
    }

    public static List<String> getIdsAsString(Collection<? extends QuadrigeBean> beans) {
        if (beans == null)
            return null;

        return beans.stream().map(bean -> bean instanceof CodeOnly ? ((CodeOnly)bean).getCode() : bean.getId().toString()).collect(Collectors.toList());
    }

    public static List<ExtractionPeriodDTO> getExtractionPeriods(ExtractionDTO extraction) {

        FilterDTO periodFilter = getFilterOfType(extraction, ExtractionFilterTypeValues.PERIOD);
        if (periodFilter == null || periodFilter.getElements() == null) return new ArrayList<>();
        return periodFilter.getElements().stream().map((Function<QuadrigeBean, ExtractionPeriodDTO>) ExtractionPeriodDTO.class::cast).collect(Collectors.toList());
    }

    public static boolean isPmfmEquals(PmfmDTO pmfmDTO, MoratoriumPmfmDTO moratoriumPmfm) {
        return Objects.equals(moratoriumPmfm.getParameterCode(), pmfmDTO.getParameter().getCode())
            && (moratoriumPmfm.getMatrixId() == null || Objects.equals(moratoriumPmfm.getMatrixId(), pmfmDTO.getMatrix().getId()))
            && (moratoriumPmfm.getFractionId() == null || Objects.equals(moratoriumPmfm.getFractionId(), pmfmDTO.getFraction().getId()))
            && (moratoriumPmfm.getMethodId() == null || Objects.equals(moratoriumPmfm.getMethodId(), pmfmDTO.getMethod().getId()))
            && (moratoriumPmfm.getUnitId() == null || Objects.equals(moratoriumPmfm.getUnitId(), pmfmDTO.getUnit().getId()));
    }

    public static boolean isProgramFullyReadable(ProgramDTO program, int userId, int departmentId) {
        // Program is fully readable if user (or its department) is manager, recorder or full viewer
        return isProgramManager(program, userId, departmentId)
            || isProgramRecorder(program, userId, departmentId)
            || isProgramValidator(program, userId, departmentId)
            || isProgramFullViewer(program, userId, departmentId);
    }

    public static boolean isProgramManager(ProgramDTO program, int userId, int departmentId) {
        return transformCollection(program.getManagerPersons(), PersonDTO::getId).contains(userId)
            || transformCollection(program.getManagerDepartments(), DepartmentDTO::getId).contains(departmentId);
    }

    public static boolean isProgramRecorder(ProgramDTO program, int userId, int departmentId) {
        return transformCollection(program.getRecorderPersons(), PersonDTO::getId).contains(userId)
            || transformCollection(program.getRecorderDepartments(), DepartmentDTO::getId).contains(departmentId);
    }

    public static boolean isProgramFullViewer(ProgramDTO program, int userId, int departmentId) {
        return transformCollection(program.getFullViewerPersons(), PersonDTO::getId).contains(userId)
            || transformCollection(program.getFullViewerDepartments(), DepartmentDTO::getId).contains(departmentId);
    }

    public static boolean isProgramViewer(ProgramDTO program, int userId, int departmentId) {
        return transformCollection(program.getViewerPersons(), PersonDTO::getId).contains(userId)
            || transformCollection(program.getViewerDepartments(), DepartmentDTO::getId).contains(departmentId);
    }

    public static boolean isProgramRecorderOrViewerOnly(ProgramDTO program, int userId, int departmentId) {
        return !isProgramManager(program, userId, departmentId) && !isProgramFullViewer(program, userId, departmentId)
            && (isProgramValidator(program, userId, departmentId) || isProgramRecorder(program, userId, departmentId) || isProgramViewer(program, userId, departmentId));
    }

    public static boolean isProgramValidator(ProgramDTO program, int userId, int departmentId) {
        return transformCollection(program.getValidatorPersons(), PersonDTO::getId).contains(userId)
            || transformCollection(program.getValidatorDepartments(), DepartmentDTO::getId).contains(departmentId);
    }

}
