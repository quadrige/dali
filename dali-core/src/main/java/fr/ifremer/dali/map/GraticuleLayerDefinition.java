package fr.ifremer.dali.map;

import org.geotools.geometry.jts.ReferencedEnvelope;

/**
 * @author peck7 on 13/06/2019.
 */
public class GraticuleLayerDefinition extends MapLayerDefinition {

    public GraticuleLayerDefinition(String name) {
        super(name, ReferencedEnvelope.EVERYTHING);
    }
}
