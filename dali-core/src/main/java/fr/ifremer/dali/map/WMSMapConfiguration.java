package fr.ifremer.dali.map;

import org.geotools.data.ows.WMSCapabilities;
import org.geotools.data.wms.WebMapServer;
import org.geotools.ows.ServiceException;

import java.io.IOException;

/**
 * @author peck7 on 13/06/2019.
 */
public interface WMSMapConfiguration extends OnlineMapConfiguration {

    WMSCapabilities getCapabilities() throws IOException, ServiceException;

    WebMapServer getWebService() throws IOException, ServiceException;

    @Override
    default boolean isTransparent() {
        return false; // pas testé
    }
}
