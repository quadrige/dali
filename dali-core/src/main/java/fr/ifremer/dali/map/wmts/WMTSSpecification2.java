package fr.ifremer.dali.map.wmts;

import org.geotools.data.ows.GetCapabilitiesRequest;
import org.geotools.data.ows.HTTPResponse;
import org.geotools.data.wmts.WMTSSpecification;
import org.geotools.data.wmts.response.WMTSGetCapabilitiesResponse;
import org.geotools.ows.ServiceException;

import java.io.IOException;
import java.net.URL;

/**
 * Allow the apiKey in request, but sometimes need to still in lower case
 *
 * @author peck7 on 15/06/2019.
 */
public class WMTSSpecification2 extends WMTSSpecification {

    private static final String APIKEY = "key";

    @Override
    public GetCapabilitiesRequest createGetCapabilitiesRequest(URL server) {
        return new GetCapsRequest2(server);
    }

    static public class GetCapsRequest2 extends WMTSSpecification.GetCapsRequest {
        /**
         * Construct a Request compatible with a 1.0.1 WMTS.
         *
         * @param urlGetCapabilities
         *            URL of GetCapabilities document.
         */
        public GetCapsRequest2(URL urlGetCapabilities) {
            super(urlGetCapabilities);
        }

        @Override
        protected void initService() {
            setProperty(SERVICE, "WMTS");
        }

        @Override
        protected void initVersion() {
            setProperty(VERSION, WMTS_VERSION); // $NON-NLS-1$ //$NON-NLS-2$
        }

        @Override
        protected String processKey(String key) {

            if (key.equalsIgnoreCase(APIKEY)) return APIKEY;

            return WMTSSpecification.processKey(key);
        }

        @Override
        public WMTSGetCapabilitiesResponse createResponse(HTTPResponse httpResponse)
                throws ServiceException, IOException {
            return new WMTSGetCapabilitiesResponse(httpResponse, hints);
        }
    }
}
