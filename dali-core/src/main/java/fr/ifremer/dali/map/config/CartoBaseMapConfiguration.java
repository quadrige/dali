package fr.ifremer.dali.map.config;

import fr.ifremer.dali.config.DaliConfiguration;

/**
 * @author peck7 on 13/06/2019.
 */
public class CartoBaseMapConfiguration extends OSMMapConfiguration {

    @Override
    public String getUrl() {
        return DaliConfiguration.getInstance().getMapCartoBaseUrl();
    }

    @Override
    public String getBaseLayerName() {
        return "cartoBase";
    }

}
