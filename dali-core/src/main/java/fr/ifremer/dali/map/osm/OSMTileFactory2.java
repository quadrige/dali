package fr.ifremer.dali.map.osm;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.geotools.tile.Tile;
import org.geotools.tile.TileFactory;
import org.geotools.tile.TileService;
import org.geotools.tile.impl.WebMercatorTileService;
import org.geotools.tile.impl.ZoomLevel;
import org.geotools.tile.impl.osm.OSMTileFactory;

/**
 * Fixed Tile service
 *
 * @author peck7 on 15/12/2017.
 */
public class OSMTileFactory2 extends OSMTileFactory {

    /**
     * Overridden method that return a correct Tile
     */
    @Override
    public Tile findTileAtCoordinate(double lon, double lat, ZoomLevel zoomLevel, TileService service) {
        lat = TileFactory.normalizeDegreeValue(lat, 90);
        lon = TileFactory.normalizeDegreeValue(lon, 180);

        lat = OSMTileFactory.moveInRange(lat, WebMercatorTileService.MIN_LATITUDE, WebMercatorTileService.MAX_LATITUDE);

        int xTile = (int) Math.floor((lon + 180) / 360 * (1 << zoomLevel.getZoomLevel()));
        int yTile = (int) Math.floor(
                (1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180))
                        / Math.PI) / 2 * (1 << zoomLevel.getZoomLevel()));

        // FIX LP : Normalize x & y to not return x or y < 0
        xTile = Math.max(xTile, 0);
        yTile = Math.max(yTile, 0);

        return new OSMTile2(xTile, yTile, zoomLevel, (OSMService2) service);
    }

    @Override
    public Tile findLowerNeighbour(Tile tile, TileService service) {
        return new OSMTile2(tile.getTileIdentifier().getLowerNeighbour(), (OSMService2) service);
    }

    @Override
    public Tile findRightNeighbour(Tile tile, TileService service) {
        return new OSMTile2(tile.getTileIdentifier().getRightNeighbour(), (OSMService2) service);
    }
}
