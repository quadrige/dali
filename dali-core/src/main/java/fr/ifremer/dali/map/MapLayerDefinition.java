package fr.ifremer.dali.map;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.geotools.geometry.jts.ReferencedEnvelope;

/**
 * A layer metadata used to indicate how and when this layer should be rendered
 *
 * @author peck7 on 05/09/2017.
 */
public class MapLayerDefinition {

    /**
     * The layer name (WMS layer or shape file name)
     */
    private String name;

    /**
     * The envelope in witch this layer should be rendered.
     * If null, this layer will be considered as the default layer
     * If ReferencedEnvelope.EVERYTHING, this layer be always rendered (graticule for example)
     */
    private ReferencedEnvelope viewEnvelope;

    /**
     * If this layer allows overlap, another layer with overlapping envelope will be rendered.
     * If not, only this layer will be render (depending on layer order)
     */
    private boolean allowOverlap = true;

    public MapLayerDefinition(String name) {
        this.name = name;
    }

    public MapLayerDefinition(String name, ReferencedEnvelope viewEnvelope) {
        this.name = name;
        this.viewEnvelope = viewEnvelope;
    }

    public MapLayerDefinition(String name, boolean allowOverlap) {
        this.name = name;
        this.allowOverlap = allowOverlap;
    }

    public MapLayerDefinition(String name, ReferencedEnvelope viewEnvelope, boolean allowOverlap) {
        this.name = name;
        this.viewEnvelope = viewEnvelope;
        this.allowOverlap = allowOverlap;
    }

    public String getName() {
        return name;
    }

    public ReferencedEnvelope getViewEnvelope() {
        return viewEnvelope;
    }

    public boolean isDefaultLayer() {
        return viewEnvelope == null;
    }

    public boolean isGlobalLayer() {
        return ReferencedEnvelope.EVERYTHING.equals(viewEnvelope);
    }

    public boolean isAllowOverlap() {
        return allowOverlap;
    }
}
