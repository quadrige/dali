package fr.ifremer.dali.map;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.enumeration.EnumValue;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.crs.DefaultGeographicCRS;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Map mode enumeration
 *
 * @author peck7 on 27/10/2017.
 */
public enum MapProjection implements EnumValue {

    WGS84("EPSG:4326", n("dali.core.enums.mapProjection.wgs84"), new ReferencedEnvelope(-180, 180, -90, 90, DefaultGeographicCRS.WGS84)),
    WGS84_PSEUDO_MERCATOR("EPSG:3857", n("dali.core.enums.mapProjection.pseudoMercator"), new ReferencedEnvelope(-180, 180, -85.05112878, 85.05112878, DefaultGeographicCRS.WGS84));

    private final String code;
    private final String i18nKey;
    private final ReferencedEnvelope envelope;

    MapProjection(String code, String i18nKey, ReferencedEnvelope envelope) {
        this.code = code;
        this.i18nKey = i18nKey;
        this.envelope = envelope;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return t(i18nKey);
    }

    public ReferencedEnvelope getEnvelope() {
        return envelope;
    }
}
