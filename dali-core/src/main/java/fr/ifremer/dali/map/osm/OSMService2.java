package fr.ifremer.dali.map.osm;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.geotools.tile.TileFactory;
import org.geotools.tile.impl.osm.OSMService;

/**
 * Fixed OSM Service with OSMTileFactory2 as TileService
 *
 * @author peck7 on 15/12/2017.
 */
public class OSMService2 extends OSMService {

    private static final TileFactory tileFactory = new OSMTileFactory2();

    private final String apiKey;
    private final String imageExtension;
    private final CloseableHttpClient httpClient;

    public OSMService2(String name, String baseUrl) {
        this(name, baseUrl, null, ".png");
    }

    public OSMService2(String name, String baseUrl, String apiKey, String imageExtension) {
        super(name, baseUrl);
        this.apiKey = apiKey;
        this.imageExtension = imageExtension;
        this.httpClient = HttpClientBuilder.create().setUserAgent("Geotools Http client").build();
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getImageExtension() {
        return imageExtension;
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    @Override
    public TileFactory getTileFactory() {
        return tileFactory;
    }
}
