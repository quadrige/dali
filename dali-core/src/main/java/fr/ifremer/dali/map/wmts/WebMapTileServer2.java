package fr.ifremer.dali.map.wmts;

import org.geotools.data.wmts.WebMapTileServer;
import org.geotools.ows.ServiceException;

import java.io.IOException;
import java.net.URL;

/**
 * Test with a new UserAgent but some request still not working (cf. SatelliteWMTSMapConfiguration)
 * @author peck7 on 15/06/2019.
 */
public class WebMapTileServer2 extends WebMapTileServer {
    public WebMapTileServer2(URL serverURL) throws IOException, ServiceException {
        super(serverURL);
        getHeaders().put("http.agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.90 Safari/537.36");
    }

    @Override
    protected void setupSpecifications() {
        specs = new WMTSSpecification2[1];
        specs[0] = new WMTSSpecification2();
    }
}
