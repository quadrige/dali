package fr.ifremer.dali.map;

import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Map constants
 *
 * @author peck7 on 13/06/2019.
 */
public class Maps {

    public static boolean isMapServerReachable(MapMode mapMode) {
        Assert.notNull(mapMode);

        if (!mapMode.isOnline()) return false;
        try {
            URL url = new URL(mapMode.getUrl());
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(1000);
            connection.connect();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static ReferencedEnvelope transformReferencedEnvelope(ReferencedEnvelope envelope, CoordinateReferenceSystem crs) {

        if (envelope != null && envelope.getCoordinateReferenceSystem() != null && crs != null) {
            if (!CRS.equalsIgnoreMetadata(crs, envelope.getCoordinateReferenceSystem())) {
                try {
                    return envelope.transform(crs, true);
                } catch (TransformException | FactoryException e) {
                    throw new DaliTechnicalException(e);
                }
            }
        }

        return envelope;
    }

    public static DirectPosition2D transformDirectPosition(DirectPosition2D position, CoordinateReferenceSystem crs) {

        if (position != null && crs != null) {
            if (!CRS.equalsIgnoreMetadata(crs, position.getCoordinateReferenceSystem())) {
                try {
                    ReferencedEnvelope envelope = new ReferencedEnvelope(position.x, position.x, position.y, position.y, position.getCoordinateReferenceSystem())
                            .transform(crs, true);
                    return new DirectPosition2D(envelope.getUpperCorner());
                } catch (TransformException | FactoryException e) {
                    throw new DaliTechnicalException(e);
                }
            }
        }
        return position;
    }

}
