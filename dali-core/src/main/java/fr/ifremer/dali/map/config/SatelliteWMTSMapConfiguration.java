package fr.ifremer.dali.map.config;

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.map.MapLayerDefinition;
import fr.ifremer.dali.map.WMTSMapConfiguration;
import fr.ifremer.dali.map.wmts.WebMapTileServer2;
import org.geotools.data.wmts.WebMapTileServer;
import org.geotools.data.wmts.model.WMTSCapabilities;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.ows.ServiceException;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Not used because of missing UserAgent for getCapabilities request
 *
 * @author peck7 on 13/06/2019.
 */
public class SatelliteWMTSMapConfiguration implements WMTSMapConfiguration {

    // current WMTS instance
    private WebMapTileServer wmtsServer;
    private WMTSCapabilities wmtsCapabilities;
    private List<MapLayerDefinition> mapLayerDefinitions;

    @Override
    public boolean isProgressiveRender() {
        return true;
    }

    @Override
    public String getUrl() {
        return DaliConfiguration.getInstance().getMapSatelliteUrl();
    }

    @Override
    public WMTSCapabilities getCapabilities() throws IOException, ServiceException {
        if (wmtsCapabilities == null) {
            wmtsCapabilities = getWebService().getCapabilities();
        }
        return wmtsCapabilities;
    }

    @Override
    public WebMapTileServer getWebService() throws IOException, ServiceException {
        if (wmtsServer == null) {
            wmtsServer = new WebMapTileServer2(new URL(getUrl()));
        }
        return wmtsServer;
    }

    @Override
    public String getBaseLayerName() {
        return "satellite";
    }

    @Override
    public List<MapLayerDefinition> getMapLayerDefinitions() {
        if (mapLayerDefinitions == null) {
            mapLayerDefinitions = ImmutableList.of(new MapLayerDefinition(getBaseLayerName(), ReferencedEnvelope.EVERYTHING));
        }
        return mapLayerDefinitions;
    }
}
