package fr.ifremer.dali.map.config;

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.map.MapLayerDefinition;
import fr.ifremer.dali.map.OSMBasedMapConfiguration;
import fr.ifremer.dali.map.osm.OSMService2;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.tile.TileService;

import java.util.List;

/**
 * @author peck7 on 13/06/2019.
 */
public class OSMMapConfiguration implements OSMBasedMapConfiguration {

    private List<MapLayerDefinition> mapLayerDefinitions;

    @Override
    public boolean isProgressiveRender() {
        return true;
    }

    @Override
    public String getUrl() {
        return DaliConfiguration.getInstance().getMapOSMUrl();
    }

    @Override
    public TileService getTileService() {
        return new OSMService2(getBaseLayerName(), getUrl());
    }

    @Override
    public String getBaseLayerName() {
        return "mapnik";
    }

    @Override
    public List<MapLayerDefinition> getMapLayerDefinitions() {
        if (mapLayerDefinitions == null) {
            mapLayerDefinitions = ImmutableList.of(new MapLayerDefinition(getBaseLayerName(), ReferencedEnvelope.EVERYTHING));
        }
        return mapLayerDefinitions;
    }
}
