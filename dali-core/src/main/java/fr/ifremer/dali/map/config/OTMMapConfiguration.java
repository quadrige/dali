package fr.ifremer.dali.map.config;

import fr.ifremer.dali.config.DaliConfiguration;

/**
 * @author peck7 on 13/06/2019.
 */
public class OTMMapConfiguration extends OSMMapConfiguration {

    @Override
    public String getBaseLayerName() {
        return "topo";
    }

    @Override
    public String getUrl() {
        return DaliConfiguration.getInstance().getMapOTMUrl();
    }
}
