package fr.ifremer.dali.map;

/**
 * @author peck7 on 13/06/2019.
 */
public interface OfflineMapConfiguration extends MapConfiguration {

    @Override
    default boolean isOnline() {
        return false;
    }

    @Override
    default String getUrl() {
        return null;
    }

    @Override
    default boolean isTransparent() {
        return true;
    }
}
