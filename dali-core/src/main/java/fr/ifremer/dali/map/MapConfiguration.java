package fr.ifremer.dali.map;

import java.util.List;

/**
 * @author peck7 on 13/06/2019.
 */
public interface MapConfiguration {

    boolean isOnline();

    String getUrl();

    String getBaseLayerName();

    List<MapLayerDefinition> getMapLayerDefinitions();

    boolean isProgressiveRender();

    boolean isTransparent();
}
