package fr.ifremer.dali.map;

/**
 * @author peck7 on 13/06/2019.
 */
public interface OnlineMapConfiguration extends MapConfiguration {

    @Override
    default boolean isOnline() {
        return true;
    }

}
