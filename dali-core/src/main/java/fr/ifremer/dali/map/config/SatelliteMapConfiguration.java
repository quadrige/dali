package fr.ifremer.dali.map.config;

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.map.osm.OSMService2;
import org.geotools.tile.TileService;

/**
 * @author peck7 on 13/06/2019.
 */
public class SatelliteMapConfiguration extends OSMMapConfiguration {

    @Override
    public String getUrl() {
        return DaliConfiguration.getInstance().getMapSatelliteUrl();
    }

    @Override
    public TileService getTileService() {
        // the specified free api key is owned by E-IS
        return new OSMService2(getBaseLayerName(), getUrl(), "ArAUP36vxq7LOYOZNQcM", ".jpg");
    }

    @Override
    public String getBaseLayerName() {
        return "satellite";
    }

}
