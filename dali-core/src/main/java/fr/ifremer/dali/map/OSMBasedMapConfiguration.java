package fr.ifremer.dali.map;

import org.geotools.tile.TileService;

/**
 * @author peck7 on 13/06/2019.
 */
public interface OSMBasedMapConfiguration extends OnlineMapConfiguration {

    TileService getTileService();

    @Override
    default boolean isTransparent() {
        return true;
    }
}
