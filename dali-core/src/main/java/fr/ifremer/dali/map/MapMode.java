package fr.ifremer.dali.map;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.map.config.*;
import fr.ifremer.quadrige3.core.dao.technical.enumeration.EnumValue;

import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Map mode enumeration
 *
 * @author peck7 on 27/10/2017.
 */
public enum MapMode implements EnumValue {

    EMBEDDED_SHAPE_MAP_MODE("EMB_SHAPE", n("dali.core.enums.mapMode.embeddedShape"), new EmbeddedShapeMapConfiguration()),
//    SEXTANT_WMS_MAP_MODE("WMS_SEXTANT", n("dali.core.enums.mapMode.sextantWMS"), new SextantWMSMapConfiguration()),
    SEXTANT_WMTS_MAP_MODE("WMTS_SEXTANT", n("dali.core.enums.mapMode.sextantWMTS"), new SextantWMTSMapConfiguration()),
    OSM_MAP_MODE("OSM", n("dali.core.enums.mapMode.openStreetMap"), new OSMMapConfiguration()),
    OTM_MAP_MODE("OTM", n("dali.core.enums.mapMode.openTopoMap"), new OTMMapConfiguration()),
    CARTO_BASE_MAP_MODE("VOYAGER", n("dali.core.enums.mapMode.cartoBase"), new CartoBaseMapConfiguration()),
    SATELLITE_MAP_MODE("SATELLITE", n("dali.core.enums.mapMode.satellite"), new SatelliteMapConfiguration());

    private final String code;
    private final String i18nKey;
    private final MapConfiguration mapConfiguration;

    MapMode(String code, String i18nKey, MapConfiguration mapConfiguration) {
        this.code = code;
        this.i18nKey = i18nKey;
        this.mapConfiguration = mapConfiguration;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return t(i18nKey);
    }

    public MapConfiguration getMapConfiguration() {
        return mapConfiguration;
    }

    public boolean isOnline() {
        return mapConfiguration.isOnline();
    }

    public String getUrl() {
        return mapConfiguration.getUrl();
    }

    public String getBaseLayerName() {
        return mapConfiguration.getBaseLayerName();
    }

    public List<MapLayerDefinition> getMapLayerDefinitions() {
        return mapConfiguration.getMapLayerDefinitions();
    }

    public boolean isProgressiveRender() {
        return mapConfiguration.isProgressiveRender();
    }

    public boolean isTransparent() {
        return mapConfiguration.isTransparent();
    }

}
