package fr.ifremer.dali.map.osm;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.geotools.tile.Tile;
import org.geotools.tile.TileIdentifier;
import org.geotools.tile.impl.ZoomLevel;
import org.geotools.tile.impl.osm.OSMTile;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Overridden OMSTile class allowing apiKey from OSMService2 and tweaking UserAgent property to fix HTTP 403 error when downloading the tile.
 *
 * @author peck7 on 15/06/2019.
 */
public class OSMTile2 extends OSMTile {

    private final OSMService2 service;

    public OSMTile2(int x, int y, ZoomLevel zoomLevel, OSMService2 service) {
        super(x, y, zoomLevel, service);
        this.service = service;
    }

    public OSMTile2(TileIdentifier tileName, OSMService2 service) {
        super(tileName, service);
        this.service = service;
    }

    @Override
    public URL getUrl() {
        String url = this.service.getBaseUrl() + getTileIdentifier().getCode() + service.getImageExtension();

        // Add apiKey
        if (StringUtils.isNotBlank(service.getApiKey()))
            url += "?key=" + service.getApiKey();

        try {
            return new URL(url);
        } catch (Exception e) {
            final String mesg = "Cannot create URL from " + url;
            throw new RuntimeException(mesg, e);
        }
    }

    @Override
    public BufferedImage loadImageTileImage(Tile tile) throws IOException {

        HttpGet request = new HttpGet(getUrl().toExternalForm());
        HttpResponse response = service.getHttpClient().execute(request);
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();

        ImageInputStream imageInputStream = ImageIO.createImageInputStream(inputStream);
        BufferedImage bi;
        try {
            bi = ImageIO.read(imageInputStream);
            if (bi == null) {
                imageInputStream.close();
            }
        } finally {
            inputStream.close();
        }
        return bi;

    }

}
