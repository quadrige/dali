package fr.ifremer.dali.map;

import org.geotools.data.wmts.WebMapTileServer;
import org.geotools.data.wmts.model.WMTSCapabilities;
import org.geotools.ows.ServiceException;

import java.io.IOException;

/**
 * @author peck7 on 13/06/2019.
 */
public interface WMTSMapConfiguration extends OnlineMapConfiguration {

    WMTSCapabilities getCapabilities() throws IOException, ServiceException;

    WebMapTileServer getWebService() throws IOException, ServiceException;

    @Override
    default boolean isTransparent() {
        return true;
    }
}
