package fr.ifremer.dali.map.config;

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.map.GraticuleLayerDefinition;
import fr.ifremer.dali.map.MapLayerDefinition;
import fr.ifremer.dali.map.WMSMapConfiguration;
import org.geotools.data.ows.WMSCapabilities;
import org.geotools.data.wms.WebMapServer;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.ows.ServiceException;
import org.geotools.referencing.crs.DefaultGeographicCRS;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * @author peck7 on 13/06/2019.
 */
public class SextantWMSMapConfiguration implements WMSMapConfiguration {

    // referenced envelopes for provided layers (comes from Sextant configuration)
    static final ReferencedEnvelope EUROPE_VIEW_ENVELOPE = new ReferencedEnvelope(-10, 13.3, 35, 55, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope FRANCE_VIEW_ENVELOPE = new ReferencedEnvelope(-8, 10, 41, 51, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope MARTINIQUE_VIEW_ENVELOPE = new ReferencedEnvelope(-62, -60, 14, 15.2, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope GUADELOUPE_VIEW_ENVELOPE = new ReferencedEnvelope(-62.5, -60.5, 15, 17, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope STMARTIN_VIEW_ENVELOPE = new ReferencedEnvelope(-63.5, -62, 17, 18.5, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope STPIERRE_VIEW_ENVELOPE = new ReferencedEnvelope(-56.8, -55.75, 46, 47.5, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope REUNION_VIEW_ENVELOPE = new ReferencedEnvelope(53.8, 57.2, -22.2, -19.9, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope MAYOTTE_VIEW_ENVELOPE = new ReferencedEnvelope(44.5, 46, -13.5, -12, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope GUYANE_VIEW_ENVELOPE = new ReferencedEnvelope(-54.7, -51.26, 3.8, 7, DefaultGeographicCRS.WGS84);
    static final ReferencedEnvelope CALEDONIE_VIEW_ENVELOPE = new ReferencedEnvelope(155, 178, -31, -12, DefaultGeographicCRS.WGS84);

    // ordered list of WMS layers
    private static final List<MapLayerDefinition> WMS_SEXTANT_ORDERED_LAYER_LIST = ImmutableList.of(
            new MapLayerDefinition("ETOPO1_BATHY_R", ReferencedEnvelope.EVERYTHING),
            new MapLayerDefinition("continent_wgs84"), // "continent" has incorrect bounding box   (continent_wgs84)
            new MapLayerDefinition("Europe", EUROPE_VIEW_ENVELOPE),
            new MapLayerDefinition("france_metropolitaine", FRANCE_VIEW_ENVELOPE, false),
            new MapLayerDefinition("SHOM_TCH_V1_MARTINIQUE_P", MARTINIQUE_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_GUADELOUPE_P_v1", GUADELOUPE_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_GUADELOUPE_stbarth_stmartin_P", STMARTIN_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_STPIERRE_P", STPIERRE_VIEW_ENVELOPE),
            new MapLayerDefinition("IFR_DOI_RUN_LIMITE_TERRE_MER_P", REUNION_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_MAYOTTE_P", MAYOTTE_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_GUYANE_P", GUYANE_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TC_DITTT_NEW_CAL_P", CALEDONIE_VIEW_ENVELOPE),
            new GraticuleLayerDefinition("graticule_4326"));

    // current WMS instance
    private WebMapServer wmsServer;
    private WMSCapabilities wmsCapabilities;

    @Override
    public boolean isProgressiveRender() {
        return false;
    }

    @Override
    public String getUrl() {
        return DaliConfiguration.getInstance().getMapSextantWMSUrl();
    }

    @Override
    public WMSCapabilities getCapabilities() throws IOException, ServiceException {
        if (wmsCapabilities == null) {
            wmsCapabilities = getWebService().getCapabilities();
        }

        return wmsCapabilities;
    }

    @Override
    public WebMapServer getWebService() throws IOException, ServiceException {
        if (wmsServer == null) {
            wmsServer = new WebMapServer(new URL(getUrl()));
        }
        return wmsServer;
    }

    @Override
    public String getBaseLayerName() {
        return null;
    }

    @Override
    public List<MapLayerDefinition> getMapLayerDefinitions() {
        return WMS_SEXTANT_ORDERED_LAYER_LIST;
    }
}
