package fr.ifremer.dali.map.config;

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.map.MapLayerDefinition;
import fr.ifremer.dali.map.OfflineMapConfiguration;

import java.util.List;

/**
 * @author peck7 on 13/06/2019.
 */
public class EmbeddedShapeMapConfiguration implements OfflineMapConfiguration {

    // ordered list of shape files with referenced envelopes (comes from Sextant configuration)
    private static final List<MapLayerDefinition> SHAPE_ORDERED_LAYER_LIST = ImmutableList.of(
            new MapLayerDefinition("continent_wgs84"),
            new MapLayerDefinition("europe", SextantWMSMapConfiguration.EUROPE_VIEW_ENVELOPE),
            new MapLayerDefinition("france_metropolitaine", SextantWMSMapConfiguration.FRANCE_VIEW_ENVELOPE, false),
            new MapLayerDefinition("SHOM_TCH_V1_MARTINIQUE_P", SextantWMSMapConfiguration.MARTINIQUE_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_GUADELOUPE_P_v1", SextantWMSMapConfiguration.GUADELOUPE_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_GUADELOUPE_stbarth_stmartin_P", SextantWMSMapConfiguration.STMARTIN_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_STPIERRE_P", SextantWMSMapConfiguration.STPIERRE_VIEW_ENVELOPE),
            new MapLayerDefinition("IFR_DOI_RUN_LIMITE_TERRE_MER_P", SextantWMSMapConfiguration.REUNION_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_MAYOTTE_P", SextantWMSMapConfiguration.MAYOTTE_VIEW_ENVELOPE),
            new MapLayerDefinition("SHOM_TCH_V1_GUYANE_P", SextantWMSMapConfiguration.GUYANE_VIEW_ENVELOPE),
            new MapLayerDefinition("cotes_compilation_ZEE_NC", SextantWMSMapConfiguration.CALEDONIE_VIEW_ENVELOPE)
    );

    @Override
    public boolean isProgressiveRender() {
        return false;
    }

    @Override
    public String getBaseLayerName() {
        return null;
    }

    @Override
    public List<MapLayerDefinition> getMapLayerDefinitions() {
        return SHAPE_ORDERED_LAYER_LIST;
    }
}
