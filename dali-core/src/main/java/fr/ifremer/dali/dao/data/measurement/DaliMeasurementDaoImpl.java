package fr.ifremer.dali.dao.data.measurement;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.administration.user.DaliQuserDao;
import fr.ifremer.dali.dao.referential.DaliAnalysisInstrumentDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliPmfmDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliQualitativeValueDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonGroupDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonNameDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.service.DaliDataContext;
import fr.ifremer.quadrige3.core.dao.administration.program.Program;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.dao.data.measurement.MeasurementDaoImpl;
import fr.ifremer.quadrige3.core.dao.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.dao.data.measurement.TaxonMeasurementDao;
import fr.ifremer.quadrige3.core.dao.data.samplingoperation.SamplingOperation;
import fr.ifremer.quadrige3.core.dao.data.samplingoperation.SamplingOperationImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.Survey;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyImpl;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.PmfmImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.QualitativeValueImpl;
import fr.ifremer.quadrige3.core.dao.referential.taxon.ReferenceTaxonImpl;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonGroupImpl;
import fr.ifremer.quadrige3.core.dao.system.QualificationHistory;
import fr.ifremer.quadrige3.core.dao.system.QualificationHistoryDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>DaliMeasurementDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("daliMeasurementDao")
public class DaliMeasurementDaoImpl extends MeasurementDaoImpl implements DaliMeasurementDao {

    private final static Log LOG = LogFactory.getLog(DaliMeasurementDaoImpl.class);

    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;

    @Resource(name = "daliAnalysisInstrumentDao")
    private DaliAnalysisInstrumentDao analysisInstrumentDao;

    @Resource(name = "daliQualitativeValueDao")
    private DaliQualitativeValueDao qualitativeValueDao;

    @Resource(name = "daliTaxonGroupDao")
    private DaliTaxonGroupDao taxonGroupDao;

    @Resource(name = "daliTaxonNameDao")
    private DaliTaxonNameDao taxonNameDao;

    @Resource(name = "taxonMeasurementDao")
    private TaxonMeasurementDao taxonMeasurementDao;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Resource(name = "daliDataContext")
    private DaliDataContext dataContext;

    @Resource(name = "objectTypeDao")
    protected ObjectTypeDao objectTypeDao;

    @Resource(name = "daliQuserDao")
    protected DaliQuserDao quserDao;

    @Resource(name = "qualificationHistoryDao")
    protected QualificationHistoryDao qualificationHistoryDao;

    /**
     * <p>Constructor for DaliMeasurementDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliMeasurementDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Measurement getMeasurementEntityBySurveyId(int surveyId, int pmfmId, boolean createIfNotExists) {

        Measurement measurement = queryUniqueTyped("measurementEntityBySurveyId",
                "surveyId", IntegerType.INSTANCE, surveyId,
                "pmfmId", IntegerType.INSTANCE, pmfmId);

        if (measurement == null && createIfNotExists) {
            measurement = Measurement.Factory.newInstance(surveyId,
                    load(ObjectTypeImpl.class, Daos.SURVEY_OBJECT_TYPE),
                    getDefaultQualityFlag(),
                    load(PmfmImpl.class, pmfmId));

            // Survey
            measurement.setSurvey(load(SurveyImpl.class, surveyId));
        }

        return measurement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeMeasurementBySurveyId(int surveyId, int pmfmId) {

        queryUpdate("deleteMeasurementsBySurveyId",
                "surveyId", IntegerType.INSTANCE, surveyId,
                "pmfmId", IntegerType.INSTANCE, pmfmId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MeasurementDTO> getMeasurementsBySurveyId(int surveyId, Integer... excludePmfmId) {

        // return measurements of a survey from MEASUREMENT & TAXON_MEASUREMENT tables
        List<MeasurementDTO> result = getMeasurementsByParentId("measurementsBySurveyId", "surveyId", surveyId, excludePmfmId);
        result.addAll(getMeasurementsByParentId("taxonMeasurementsBySurveyId", "surveyId", surveyId, excludePmfmId));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MeasurementDTO> getMeasurementsBySamplingOperationId(int samplingOperationId, boolean withTaxonMeasurements, Integer... excludePmfmId) {

        // return measurements of a survey from MEASUREMENT & TAXON_MEASUREMENT tables
        List<MeasurementDTO> result = getMeasurementsByParentId("measurementsBySamplingOperationId", "samplingOperationId", samplingOperationId, excludePmfmId);
        if (withTaxonMeasurements) {
            result.addAll(getMeasurementsByParentId("taxonMeasurementsBySamplingOperationId", "samplingOperationId", samplingOperationId, excludePmfmId));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveMeasurementsBySurveyId(int surveyId, Collection<MeasurementDTO> measurements, Integer... excludePmfmId) {

        // get already saved measurements and taxon measurements
        List<Integer> existingMeasurementIds = DaliBeans.collectIds(getMeasurementsByParentId("measurementsBySurveyId", "surveyId", surveyId, excludePmfmId));
        List<Integer> existingTaxonMeasurementIds = DaliBeans.collectIds(getMeasurementsByParentId("taxonMeasurementsBySurveyId", "surveyId", surveyId, excludePmfmId));

        Survey survey = get(SurveyImpl.class, surveyId);

        if (CollectionUtils.isNotEmpty(measurements)) {
            for (MeasurementDTO measurement : measurements) {

                Assert.notNull(measurement.getPmfm(), "measurement must have a pmfm");

                // Don't save excluded pmfm id
                if (excludePmfmId != null && Arrays.asList(excludePmfmId).contains(measurement.getPmfm().getId())) {
                    continue;
                }

                if (DaliBeans.isTaxonMeasurement(measurement)) {
                    // save the taxon measurement
                    if (saveSurveyTaxonMeasurement(survey, measurement)) {
                        // remove saved measurement
                        existingTaxonMeasurementIds.remove(measurement.getId());
                    }
                } else {
                    // save the measurement
                    if (saveSurveyMeasurement(survey, measurement)) {
                        // remove saved measurement
                        existingMeasurementIds.remove(measurement.getId());
                    }
                }

            }
        }

        // delete remaining measurements
        removeMeasurementsByIds(existingMeasurementIds);
        removeTaxonMeasurementsByIds(existingTaxonMeasurementIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveMeasurementsBySamplingOperationId(int samplingOperationId, Collection<MeasurementDTO> measurements, boolean withIndividual, Integer... excludePmfmId) {
        List<Integer> existingMeasurementIds = DaliBeans.collectIds(getMeasurementsByParentId("measurementsBySamplingOperationId", "samplingOperationId", samplingOperationId, excludePmfmId));
        List<Integer> existingTaxonMeasurementIds = DaliBeans.collectIds(getMeasurementsByParentId("taxonMeasurementsBySamplingOperationId", "samplingOperationId", samplingOperationId, excludePmfmId));

        SamplingOperation samplingOperation = get(SamplingOperationImpl.class, samplingOperationId);

        if (CollectionUtils.isNotEmpty(measurements)) {
            for (MeasurementDTO measurement : measurements) {
                // TODO-EIS taxonomic = must have taxon ?
                Assert.notNull(measurement.getPmfm(), "measurement must have a pmfm");

                // Don't save excluded pmfm id
                if (excludePmfmId != null && Arrays.asList(excludePmfmId).contains(measurement.getPmfm().getId())) {
                    continue;
                }

                if (DaliBeans.isTaxonMeasurement(measurement)) {
                    // save the taxon measurement
                    if (saveSamplingOperationTaxonMeasurement(samplingOperation, measurement)) {
                        // remove saved measurement
                        existingTaxonMeasurementIds.remove(measurement.getId());
                    }
                } else {
                    // save the measurement
                    if (saveSamplingOperationMeasurement(samplingOperation, measurement)) {
                        // remove saved measurement
                        existingMeasurementIds.remove(measurement.getId());
                    }
                }
            }
        }

        // delete remaining measurements
        if (withIndividual) {

            // can delete all remaining
            removeMeasurementsByIds(existingMeasurementIds);
            removeTaxonMeasurementsByIds(existingTaxonMeasurementIds);

        } else {

            // individual measurement must NOT be delete
            existingMeasurementIds.stream().distinct().forEach(measurementId -> {
                Measurement measurement = get(measurementId);
                if (measurement != null && measurement.getMeasIndivId() == null) {
                    remove(measurement);
                }
            });
            existingTaxonMeasurementIds.stream().distinct().forEach(taxonMeasurementId -> {
                TaxonMeasurement taxonMeasurement = taxonMeasurementDao.get(taxonMeasurementId);
                if (taxonMeasurement != null && taxonMeasurement.getTaxonMeasIndivId() == null) {
                    taxonMeasurementDao.remove(taxonMeasurement);
                }
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeMeasurementsByIds(Collection<Integer> measurementIds) {
        if (measurementIds == null) return;
        // remove unitarily by using a Set to avoid multiple deletation
        measurementIds.stream().distinct().forEach(this::remove);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeTaxonMeasurementsByIds(Collection<Integer> taxonMeasurementIds) {
        if (taxonMeasurementIds == null) return;
        // remove unitarily by using a Set to avoid multiple deletation
        taxonMeasurementIds.stream().distinct().forEach(taxonMeasurementId -> taxonMeasurementDao.remove(taxonMeasurementId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAllMeasurementsBySurveyId(int surveyId) {

        // Delete common measurements
        queryUpdate("deleteMeasurementsBySurveyId",
                "surveyId", IntegerType.INSTANCE, surveyId,
                "pmfmId", IntegerType.INSTANCE, null);

        // Delete taxon measurements
        queryUpdate("deleteTaxonMeasurementsBySurveyId", "surveyId", IntegerType.INSTANCE, surveyId,
                "pmfmId", IntegerType.INSTANCE, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAllMeasurementsBySamplingOperationId(int samplingOperationId) {

        // Delete common measurements
        queryUpdate("deleteMeasurementsBySamplingOperationId", "samplingOperationId", IntegerType.INSTANCE, samplingOperationId,
                "pmfmId", IntegerType.INSTANCE, null);

        // Delete taxon measurements
        queryUpdate("deleteTaxonMeasurementsBySamplingOperationId", "samplingOperationId", IntegerType.INSTANCE, samplingOperationId,
                "pmfmId", IntegerType.INSTANCE, null);
    }

    @Override
    public int validateAllMeasurementsBySurveyIds(Collection<Integer> surveyIds, Date validationDate) {
        int nbUpdates = createQuery("validateSurveyMeasurementsBySurveyIds",
                "validationDate", DateType.INSTANCE, validationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        nbUpdates += createQuery("validateSamplingOperationMeasurementsBySurveyIds",
                "validationDate", DateType.INSTANCE, validationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        nbUpdates += createQuery("validateSurveyTaxonMeasurementsBySurveyIds",
                "validationDate", DateType.INSTANCE, validationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        nbUpdates += createQuery("validateSamplingOperationTaxonMeasurementsBySurveyIds",
                "validationDate", DateType.INSTANCE, validationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        return nbUpdates;
    }

    @Override
    public int unvalidateAllMeasurementsBySurveyIds(Collection<Integer> surveyIds, Date unvalidationDate, int validatorId) {

        // Get qualified measurements for historisation
        surveyIds.forEach(surveyId -> {
            List<Measurement> qualifiedMeasurements = getQualifiedMeasurementBySurveyId(surveyId);
            if (CollectionUtils.isNotEmpty(qualifiedMeasurements)) {
                qualifiedMeasurements.forEach(measurement -> {
                    QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(measurement.getMeasId()),
                        objectTypeDao.get(ObjectTypeCode.MEASUREMENT.getValue()),
                        quserDao.get(validatorId));
                    qualificationHistory.setQualHistOperationCm(t("dali.service.observation.qualificationHistory.unqualify.message"));
                    qualificationHistory.setQualityFlag(measurement.getQualityFlag());
                    qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
                    qualificationHistoryDao.create(qualificationHistory);
                });
            }
            List<TaxonMeasurement> qualifiedTaxonMeasurements = getQualifiedTaxonMeasurementBySurveyId(surveyId);
            if (CollectionUtils.isNotEmpty(qualifiedTaxonMeasurements)) {
                qualifiedTaxonMeasurements.forEach(measurement -> {
                    QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(measurement.getTaxonMeasId()),
                        objectTypeDao.get(ObjectTypeCode.TAXON_MEASUREMENT.getValue()),
                        quserDao.get(validatorId));
                    qualificationHistory.setQualHistOperationCm(t("dali.service.observation.qualificationHistory.unqualify.message"));
                    qualificationHistory.setQualityFlag(measurement.getQualityFlag());
                    qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
                    qualificationHistoryDao.create(qualificationHistory);
                });
            }
        });

        int nbUpdates = createQuery("unvalidateMeasurementsBySurveyIds", "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue())
            .setParameterList("surveyIds", surveyIds).executeUpdate();
        nbUpdates += createQuery("unvalidateTaxonMeasurementsBySurveyIds", "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue())
            .setParameterList("surveyIds", surveyIds).executeUpdate();
        return nbUpdates;
    }

    @Override
    public int unvalidateAllMeasurementsBySamplingOperationIds(List<Integer> samplingOperationIds, Date unvalidationDate, int validatorId) {

        // Get qualified measurements for historisation
        samplingOperationIds.forEach(samplingOperationId -> {
            List<Measurement> qualifiedMeasurements = getQualifiedMeasurementBySamplingOperationId(samplingOperationId);
            if (CollectionUtils.isNotEmpty(qualifiedMeasurements)) {
                qualifiedMeasurements.forEach(measurement -> {
                    QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(measurement.getMeasId()),
                        objectTypeDao.get(ObjectTypeCode.MEASUREMENT.getValue()),
                        quserDao.get(validatorId));
                    qualificationHistory.setQualHistOperationCm(t("dali.service.observation.qualificationHistory.unqualify.message"));
                    qualificationHistory.setQualityFlag(measurement.getQualityFlag());
                    qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
                    qualificationHistoryDao.create(qualificationHistory);
                });
            }
            List<TaxonMeasurement> qualifiedTaxonMeasurements = getQualifiedTaxonMeasurementBySamplingOperationId(samplingOperationId);
            if (CollectionUtils.isNotEmpty(qualifiedTaxonMeasurements)) {
                qualifiedTaxonMeasurements.forEach(measurement -> {
                    QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(measurement.getTaxonMeasId()),
                        objectTypeDao.get(ObjectTypeCode.TAXON_MEASUREMENT.getValue()),
                        quserDao.get(validatorId));
                    qualificationHistory.setQualHistOperationCm(t("dali.service.observation.qualificationHistory.unqualify.message"));
                    qualificationHistory.setQualityFlag(measurement.getQualityFlag());
                    qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
                    qualificationHistoryDao.create(qualificationHistory);
                });
            }
        });

        int nbUpdates = createQuery("unvalidateMeasurementsBySamplingOperationIds", "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue())
            .setParameterList("samplingOperIds", samplingOperationIds).executeUpdate();
        nbUpdates += createQuery("unvalidateTaxonMeasurementsBySamplingOperationIds", "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue())
            .setParameterList("samplingOperIds", samplingOperationIds).executeUpdate();
        return nbUpdates;
    }

    @Override
    public List<Measurement> getQualifiedMeasurementBySurveyId(int surveyId) {
        return queryListTyped("qualifiedSurveyMeasurements",
            "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue(),
            "surveyId", IntegerType.INSTANCE, surveyId
        );
    }

    @Override
    public List<TaxonMeasurement> getQualifiedTaxonMeasurementBySurveyId(int surveyId) {
        return queryListTyped("qualifiedSurveyTaxonMeasurements",
            "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue(),
            "surveyId", IntegerType.INSTANCE, surveyId
        );
    }

    @Override
    public List<Measurement> getQualifiedMeasurementBySamplingOperationId(int samplingOperationId) {
        return queryListTyped("qualifiedSamplingOperationMeasurements",
            "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue(),
            "samplingOperId", IntegerType.INSTANCE, samplingOperationId
        );
    }

    @Override
    public List<TaxonMeasurement> getQualifiedTaxonMeasurementBySamplingOperationId(int samplingOperationId) {
        return queryListTyped("qualifiedSamplingOperationTaxonMeasurements",
            "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue(),
            "samplingOperId", IntegerType.INSTANCE, samplingOperationId
        );
    }

    @Override
    public int qualifyAllMeasurementsBySurveyIds(List<Integer> surveyIds, Date qualificationDate) {
        int nbUpdates = createQuery("qualifySurveyMeasurementsBySurveyIds",
                "qualificationDate", DateType.INSTANCE, qualificationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        nbUpdates += createQuery("qualifySamplingOperationMeasurementsBySurveyIds",
                "qualificationDate", DateType.INSTANCE, qualificationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        nbUpdates += createQuery("qualifySurveyTaxonMeasurementsBySurveyIds",
                "qualificationDate", DateType.INSTANCE, qualificationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        nbUpdates += createQuery("qualifySamplingOperationTaxonMeasurementsBySurveyIds",
                "qualificationDate", DateType.INSTANCE, qualificationDate)
                .setParameterList("surveyIds", surveyIds).executeUpdate();
        return nbUpdates;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateMeasurementsControlDate(Collection<Integer> measurementIds, Date controlDate) {
        createQuery("updateMeasurementControlDate")
                .setParameterList("measurementIds", measurementIds)
                .setParameter("controlDate", controlDate).executeUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateTaxonMeasurementsControlDate(Collection<Integer> taxonMeasurementIds, Date controlDate) {
        createQuery("updateTaxonMeasurementControlDate")
                .setParameterList("taxonMeasurementIds", taxonMeasurementIds)
                .setParameter("controlDate", controlDate).executeUpdate();
    }

    // PRIVATE METHODS
    @SuppressWarnings("unchecked")
    private List<MeasurementDTO> getMeasurementsByParentId(String queryName, String parentParameter, int parentId, Integer... excludePmfmIds) {

        Query query = createQuery(queryName, parentParameter, IntegerType.INSTANCE, parentId);

        List<Integer> pmfmIdsToExclude = ArrayUtils.isEmpty(excludePmfmIds) ? Lists.newArrayList(0) : Lists.newArrayList(excludePmfmIds);
        query.setParameterList("pmfmIds", pmfmIdsToExclude);

        Iterator<Object[]> it = query.iterate();

        List<MeasurementDTO> result = Lists.newArrayList();

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toMeasurement(Arrays.asList(row).iterator()));
        }

        return result;
    }

    private boolean saveSurveyMeasurement(Survey survey, MeasurementDTO measurement) {

        return saveMeasurement(measurement, survey, null);
    }

    private boolean saveSurveyTaxonMeasurement(Survey survey, MeasurementDTO measurement) {

        return saveTaxonMeasurement(measurement, survey, null);
    }

    private boolean saveSamplingOperationMeasurement(SamplingOperation samplingOperation, MeasurementDTO measurement) {

        return saveMeasurement(measurement, null, samplingOperation);
    }

    private boolean saveSamplingOperationTaxonMeasurement(SamplingOperation samplingOperation, MeasurementDTO measurement) {

        return saveTaxonMeasurement(measurement, null, samplingOperation);

    }

    /**
     * Save a measurement on a survey OR a samplingOperation
     *
     * @param measurement measurement to save
     * @param survey the parent survey
     * @param samplingOperation the parent operation
     * @return true if success
     */
    private boolean saveMeasurement(MeasurementDTO measurement, Survey survey, SamplingOperation samplingOperation) {

        if (DaliBeans.isMeasurementEmpty(measurement)) {
            return false;
        }

        Integer objectId;
        String objectType;
        Collection<Program> programs;

        if (survey != null) {
            Assert.isNull(samplingOperation, "if a survey is provided, samplingOperation must be null");
            objectId = survey.getSurveyId();
            objectType = Daos.SURVEY_OBJECT_TYPE;
            programs = survey.getPrograms();
        } else {
            Assert.notNull(samplingOperation, "if a samplingOperation is provided, survey must be null");
            objectId = samplingOperation.getSamplingOperId();
            objectType = Daos.SAMPLING_OPERATION_OBJECT_TYPE;
            programs = samplingOperation.getPrograms();
        }

        Measurement target;
        if (measurement.getId() == null || measurement.getId() < 0) {
            target = Measurement.Factory.newInstance();
        } else {
            target = get(measurement.getId());
        }

        target.setObjectType(load(ObjectTypeImpl.class, objectType));
        target.setObjectId(objectId);

        // Survey
        target.setSurvey(survey);
        // SamplingOperation
        target.setSamplingOperation(samplingOperation);

        if (CollectionUtils.isNotEmpty(programs)) {
            for (Program program : programs) {
                target.addPrograms(program);
            }
        }

        // Recorder Department (Mantis #42614 Only if REC_DEP_ID is null)
        if (target.getRecorderDepartment() == null) {
            target.setRecorderDepartment(load(DepartmentImpl.class, dataContext.getRecorderDepartmentId()));
        }

        target.setQualityFlag(getDefaultQualityFlag());
        target.setPmfm(load(PmfmImpl.class, measurement.getPmfm().getId()));
        target.setMeasIndivId(measurement.getIndividualId());
        target.setMeasCm(measurement.getComment());

        if (measurement.getAnalyst() != null) {
            target.setDepartment(load(DepartmentImpl.class, measurement.getAnalyst().getId()));
        } else {
            target.setDepartment(null);
        }

        if (measurement.getPmfm().getParameter().isQualitative() && measurement.getQualitativeValue() != null) {
            // save as qualitative value
            target.setQualitativeValue(load(QualitativeValueImpl.class, measurement.getQualitativeValue().getId()));
            target.setMeasNumerValue(null);
            target.setMeasDigitNumber(null);
            target.setMeasPrecisionValue(null);
        } else {
            // save as numeric value
            target.setMeasNumerValue(measurement.getNumericalValue() == null ? null : measurement.getNumericalValue().floatValue());
            target.setMeasPrecisionValue(measurement.getPrecision() == null ? null : measurement.getPrecision().floatValue());
            target.setMeasDigitNumber(measurement.getDigitNb());
            target.setQualitativeValue(null);
        }

        if (measurement.getAnalysisInstrument() != null) {
            target.setAnalysisInstrument(load(AnalysisInstrumentImpl.class, measurement.getAnalysisInstrument().getId()));
        } else {
            target.setAnalysisInstrument(null);
        }

        getSession().save(target);
        measurement.setId(target.getMeasId());
        return true;
    }

    /**
     * Save a taxonMeasurement on a survey OR a samplingOperation
     *
     * @param measurement the taxon measurement to save
     * @param survey the parent survey
     * @param samplingOperation the parent operation
     * @return true if success
     */
    private boolean saveTaxonMeasurement(MeasurementDTO measurement, Survey survey, SamplingOperation samplingOperation) {

        if (DaliBeans.isMeasurementEmpty(measurement)) {
            return false;
        }

        Integer objectId;
        String objectType;
        Collection<Program> programs;

        if (survey != null) {
            Assert.isNull(samplingOperation, "if a survey is provided, samplingOperation must be null");
            objectId = survey.getSurveyId();
            objectType = Daos.SURVEY_OBJECT_TYPE;
            programs = survey.getPrograms();
        } else {
            Assert.notNull(samplingOperation, "if a samplingOperation is provided, survey must be null");
            objectId = samplingOperation.getSamplingOperId();
            objectType = Daos.SAMPLING_OPERATION_OBJECT_TYPE;
            programs = samplingOperation.getPrograms();
        }

        TaxonMeasurement target;
        boolean isNew;
        if (measurement.getId() == null || measurement.getId() < 0) {
            target = TaxonMeasurement.Factory.newInstance();
            isNew = true;
        } else {
            target = taxonMeasurementDao.get(measurement.getId());
            isNew = false;
        }

        target.setObjectType(load(ObjectTypeImpl.class, objectType));
        target.setObjectId(objectId);

        // Survey
        target.setSurvey(survey);
        // SamplingOperation
        target.setSamplingOperation(samplingOperation);

        if (CollectionUtils.isNotEmpty(programs)) {
            for (Program program : programs) {
                target.addPrograms(program);
            }
        }

        // Recorder Department (Mantis #42614 Only if REC_DEP_ID is null)
        if (target.getRecorderDepartment() == null) {
            target.setRecorderDepartment(load(DepartmentImpl.class, dataContext.getRecorderDepartmentId()));
        }

        target.setQualityFlag(getDefaultQualityFlag());
        target.setPmfm(load(PmfmImpl.class, measurement.getPmfm().getId()));
        target.setTaxonMeasIndivId(measurement.getIndividualId());
        target.setTaxonMeasCm(measurement.getComment());

        if (measurement.getAnalyst() != null) {
            target.setDepartment(load(DepartmentImpl.class, measurement.getAnalyst().getId()));
        } else {
            target.setDepartment(null);
        }

        if (measurement.getTaxonGroup() == null) {
            target.setTaxonGroup(null);
        } else {
            target.setTaxonGroup(load(TaxonGroupImpl.class, measurement.getTaxonGroup().getId()));
        }

        if (measurement.getTaxon() == null) {
            target.setReferenceTaxon(null);
        } else {
            target.setReferenceTaxon(load(ReferenceTaxonImpl.class, measurement.getTaxon().getReferenceTaxonId()));
        }

        // set input taxon information if null
        if (measurement.getTaxon() != null) {
            if (measurement.getInputTaxonId() == null) {
                measurement.setInputTaxonId(measurement.getTaxon().getId());
            }
            if (StringUtils.isBlank(measurement.getInputTaxonName())) {
                measurement.setInputTaxonName(measurement.getTaxon().getName());
            }
        } else {
            measurement.setInputTaxonId(null);
            measurement.setInputTaxonName(null);
        }

        // add the input taxon name and ID (Mantis #34705 + #36232)
        target.setTaxonNameId(measurement.getInputTaxonId());
        target.setTaxonNameNm(measurement.getInputTaxonName());

        if (measurement.getPmfm().getParameter().isQualitative() && measurement.getQualitativeValue() != null) {
            // save as qualitative value
            target.setQualitativeValue(load(QualitativeValueImpl.class, measurement.getQualitativeValue().getId()));
            target.setTaxonMeasNumerValue(null);
            target.setTaxonMeasDigitNumber(null);
            target.setTaxonMeasPrecisionValue(null);
        } else {
            // save as numeric value
            target.setTaxonMeasNumerValue(measurement.getNumericalValue() == null ? null : measurement.getNumericalValue().floatValue());
            target.setTaxonMeasPrecisionValue(measurement.getPrecision() == null ? null : measurement.getPrecision().floatValue());
            target.setTaxonMeasDigitNumber(measurement.getDigitNb());
            target.setQualitativeValue(null);
        }

        if (measurement.getAnalysisInstrument() != null) {
            target.setAnalysisInstrument(load(AnalysisInstrumentImpl.class, measurement.getAnalysisInstrument().getId()));
        } else {
            target.setAnalysisInstrument(null);
        }

        // Save or update
        if (isNew) {
            getSession().save(target);
            // Update identifier
            measurement.setId(target.getTaxonMeasId());
        } else {
            getSession().update(target);
        }

        return true;
    }

    /**
     * return the default quality flag
     *
     * @return the default quality flag
     */
    private QualityFlag getDefaultQualityFlag() {
        return load(QualityFlagImpl.class, QualityFlagCode.NOT_QUALIFIED.getValue()); // = non qualifié
    }

    private MeasurementDTO toMeasurement(Iterator<Object> it) {
        MeasurementDTO result = DaliBeanFactory.newMeasurementDTO();
        Integer pmfmId = (Integer) it.next();
        Assert.notNull(pmfmId);
        result.setPmfm(pmfmDao.getPmfmById(pmfmId));
        result.setId((Integer) it.next());
        Double value = Daos.convertToDouble((Float) it.next());
        result.setPrecision(Daos.convertToInteger((Number) it.next()));
        result.setDigitNb(Daos.convertToInteger((Number) it.next()));

        // convert to BigDecimal
        result.setNumericalValue(Daos.convertToBigDecimal(value, result.getDigitNb()));

        result.setIndividualId((Integer) it.next());
        result.setComment((String) it.next());
        result.setControlDate(Daos.convertToDate(it.next()));
        result.setValidationDate(Daos.convertToDate(it.next()));
        result.setQualificationDate(Daos.convertToDate(it.next()));
        result.setQualificationComment((String) it.next());
        Integer departmentId = (Integer) it.next();
        if (departmentId != null) {
            result.setAnalyst(departmentDao.getDepartmentById(departmentId));
        }
        Integer qualitativeValueId = (Integer) it.next();
        if (qualitativeValueId != null) {
            result.setQualitativeValue(qualitativeValueDao.getQualitativeValueById(qualitativeValueId));
        }
        Integer analysisInstrumentId = (Integer) it.next();
        if (analysisInstrumentId != null) {
            result.setAnalysisInstrument(analysisInstrumentDao.getAnalysisInstrumentById(analysisInstrumentId));
        }
        if (it.hasNext()) {
            // if has more element, it is taxonGroupId and referenceTaxonId (comes from TaxonMeasurements)
            // and input taxon (Mantis #34705)
            Integer taxonGroupId = (Integer) it.next();
            if (taxonGroupId != null) {
                result.setTaxonGroup(taxonGroupDao.getTaxonGroupById(taxonGroupId));
            }
            Integer refTaxonId = (Integer) it.next();
            Integer inputTaxonId = (Integer) it.next();
            String inputTaxonName = (String) it.next();

            // Input taxon id and name (Mantis #36232)
            result.setInputTaxonId(inputTaxonId);
            result.setInputTaxonName(inputTaxonName);

            // Reference taxon (Mantis #52993)
            if (inputTaxonId != null) {
                // Get reference taxon from input taxon (if known)
                TaxonDTO inputTaxon = taxonNameDao.getTaxonNameById(inputTaxonId);
                result.setTaxon(Optional.ofNullable(inputTaxon.getReferenceTaxon()).orElse(taxonNameDao.getTaxonNameByReferenceId(inputTaxon.getReferenceTaxonId())));
            } else if (refTaxonId != null) {
                // Fallback to previous behavior
                try {
                    result.setTaxon(taxonNameDao.getTaxonNameByReferenceId(refTaxonId));
                } catch (DataRetrievalFailureException e) {
                    // should not happened now...
                    LOG.warn(String.format("Reference taxon [id=%s] not found (should not happened)", refTaxonId));
                }
            }
        }
        return result;
    }

}
