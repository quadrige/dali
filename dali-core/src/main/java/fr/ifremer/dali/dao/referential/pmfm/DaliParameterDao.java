package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.dali.dto.referential.pmfm.ParameterGroupDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 29/07/2015.
 */
public interface DaliParameterDao {

    /** Constant <code>PARAMETER_BY_CODE_CACHE="parameterByCode"</code> */
    String PARAMETER_BY_CODE_CACHE = "parameter_by_code";
    /** Constant <code>ALL_PARAMETERS_CACHE="allParameters"</code> */
    String ALL_PARAMETERS_CACHE = "all_parameters";

    /** Constant <code>ALL_PARAMETER_GROUPS_CACHE="allParameterGroups"</code> */
    String ALL_PARAMETER_GROUPS_CACHE = "all_parameter_groups";
    /** Constant <code>PARAMETER_GROUP_BY_ID_CACHE="parameterGroupById"</code> */
    String PARAMETER_GROUP_BY_ID_CACHE = "parameter_group_by_id";

    /**
     * <p>getAllParameters.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_PARAMETERS_CACHE)
    List<ParameterDTO> getAllParameters(List<String> statusCodes);

    /**
     * <p>getParameterByCode.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.ParameterDTO} object.
     */
    @Cacheable(value = PARAMETER_BY_CODE_CACHE)
    ParameterDTO getParameterByCode(String parameterCode);

    /**
     * <p>findParameters.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @param parameterGroupId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<ParameterDTO> findParameters(String parameterCode, Integer parameterGroupId, List<String> statusCodes);

    /**
     * <p>getAllParameterGroups.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_PARAMETER_GROUPS_CACHE)
    List<ParameterGroupDTO> getAllParameterGroups(List<String> statusCodes);

    /**
     * <p>getParameterGroupById.</p>
     *
     * @param parameterGroupId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.ParameterGroupDTO} object.
     */
    @Cacheable(value = PARAMETER_GROUP_BY_ID_CACHE)
    ParameterGroupDTO getParameterGroupById(int parameterGroupId);

}
