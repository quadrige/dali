package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.dali.dto.referential.pmfm.ParameterGroupDTO;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.ParameterDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Parameter DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("daliParameterDao")
public class DaliParameterDaoImpl extends ParameterDaoImpl implements DaliParameterDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;

    @Resource(name = "daliQualitativeValueDao")
    private DaliQualitativeValueDao qualitativeValueDao;

    @Resource(name = "daliParameterDao")
    private DaliParameterDao loopbackDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliParameterDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<ParameterGroupDTO> getAllParameterGroups(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(PARAMETER_GROUP_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allParameterGroups"), statusCodes);

        List<ParameterGroupDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            ParameterGroupDTO parameter = toParameterGroupDTO(Arrays.asList(source).iterator());
            result.add(parameter);
            cacheById.put(parameter.getId(), parameter);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public ParameterGroupDTO getParameterGroupById(int parameterGroupId) {
        Object[] source = queryUnique("parameterGroupById", "parameterGroupId", IntegerType.INSTANCE, parameterGroupId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load parameter group with id = " + parameterGroupId);
        }

        return toParameterGroupDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<ParameterDTO> getAllParameters(List<String> statusCodes) {

        Cache cacheByCode = cacheService.getCache(PARAMETER_BY_CODE_CACHE);
        List<ParameterDTO> result = Lists.newArrayList();
        Map<String, String> transcribingNamesByCode = getTranscribingNames();
        Map<String, String> transcribingCodesByCode = getTranscribingCodes();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allParameters"), statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            ParameterDTO parameter = toParameterDTO(Arrays.asList(source).iterator(), transcribingNamesByCode, transcribingCodesByCode);
            // without qualitative values
            result.add(parameter);
            cacheByCode.put(parameter.getCode(), parameter);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public ParameterDTO getParameterByCode(String parameterCode) {
        Assert.notBlank(parameterCode);

        Object[] source = queryUnique("parameterByCode", "parameterCode", StringType.INSTANCE, parameterCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load parameter with code = " + parameterCode);
        }

        ParameterDTO result = toParameterDTO(Arrays.asList(source).iterator(), getTranscribingNames(), getTranscribingCodes());
        // add all associated Qualitative values
        result.setQualitativeValues(qualitativeValueDao.getQualitativeValuesByParameterCode(result.getCode()));
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<ParameterDTO> findParameters(String parameterCode, Integer parameterGroupId, List<String> statusCodes) {
        List<ParameterDTO> result = Lists.newArrayList();
        Map<String, String> transcribingNamesByCode = getTranscribingNames();
        Map<String, String> transcribingCodesByCode = getTranscribingCodes();

        Query query = createQuery("parametersByCriteria",
                "parameterCode", StringType.INSTANCE, parameterCode,
                "parameterGroupId", IntegerType.INSTANCE, parameterGroupId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        // other solution: use criteria API and findByExample
        while (it.hasNext()) {
            Object[] source = it.next();
            ParameterDTO parameter = toParameterDTO(Arrays.asList(source).iterator(), transcribingNamesByCode, transcribingCodesByCode);
            // add all associated Qualitative values
            parameter.setQualitativeValues(qualitativeValueDao.getQualitativeValuesByParameterCode(parameter.getCode()));
            result.add(parameter);
        }

        return ImmutableList.copyOf(result);
    }

    private ParameterGroupDTO toParameterGroupDTO(Iterator<Object> source) {
        ParameterGroupDTO parameterGroup = DaliBeanFactory.newParameterGroupDTO();
        parameterGroup.setId((Integer) source.next());
        parameterGroup.setName((String) source.next());
        parameterGroup.setDescription((String) source.next());
        parameterGroup.setStatus(referentialDao.getStatusByCode((String) source.next()));
        Integer parentId = (Integer) source.next();
        if (parentId != null) {
            parameterGroup.setParentParameterGroup(loopbackDao.getParameterGroupById(parentId));
        }
        return parameterGroup;
    }

    private Map<String, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsByCode(config.getTranscribingItemTypeLbForParameterNm());
    }

    private Map<String, String> getTranscribingCodes() {
        return transcribingItemDao.getAllTranscribingItemsByCode(config.getTranscribingItemTypeLbForParameterCd());
    }

    private ParameterDTO toParameterDTO(Iterator<Object> source, Map<String, String> transcribingNamesByCode, Map<String, String> transcribingCodesByCode) {
        ParameterDTO result = DaliBeanFactory.newParameterDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setQualitative(Daos.safeConvertToBoolean(source.next()));
        result.setCalculated(Daos.safeConvertToBoolean(source.next()));
        result.setTaxonomic(Daos.safeConvertToBoolean(source.next()));
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        Integer groupId = (Integer) source.next();
        if (groupId != null) {
            result.setParameterGroup(loopbackDao.getParameterGroupById(groupId));
        }
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // transcribed name
        result.setName(transcribingNamesByCode.getOrDefault(result.getCode(), result.getName()));
        // transcribed code (Mantis #47330)
        result.setTranscribedCode(transcribingCodesByCode.getOrDefault(result.getCode(), result.getCode()));

        return result;
    }

}
