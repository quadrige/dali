package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 29/07/2015.
 */
public interface DaliMatrixDao {

    /** Constant <code>ALL_MATRICES_CACHE="allMatrices"</code> */
    String ALL_MATRICES_CACHE = "all_matrices";
    /** Constant <code>MATRIX_BY_ID_CACHE="matrixById"</code> */
    String MATRIX_BY_ID_CACHE = "matrix_by_id";

    /**
     * <p>getAllMatrices.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_MATRICES_CACHE)
    List<MatrixDTO> getAllMatrices(List<String> statusCodes);

    /**
     * <p>getMatrixById.</p>
     *
     * @param matrixId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.MatrixDTO} object.
     */
    @Cacheable(value = MATRIX_BY_ID_CACHE)
    MatrixDTO getMatrixById(int matrixId);

    /**
     * <p>findMatrices.</p>
     *
     * @param matrixId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<MatrixDTO> findMatrices(Integer matrixId, List<String> statusCodes);

}
