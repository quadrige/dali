package fr.ifremer.dali.dao.technical;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import fr.ifremer.dali.dto.CoordinateDTO;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.referencing.operation.TransformException;

import java.util.Objects;

/**
 * Geometry utility class
 * <p/>
 * Created by Ludovic on 16/04/2015.
 */
public class Geometries extends fr.ifremer.quadrige3.core.dao.technical.Geometries {

    private static final Log LOG = LogFactory.getLog(Geometries.class);

    /**
     * <p>getCoordinate.</p>
     *
     * @param position a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.dali.dto.CoordinateDTO} object.
     */
    public static CoordinateDTO getCoordinate(String position) {

        CoordinateDTO coordinate = DaliBeanFactory.newCoordinateDTO();

        if (StringUtils.isNotBlank(position)) {

            // Keep WTK (raw data)
            coordinate.setWkt(position);

            // Convert to Geometry
            Geometry geometry = getGeometry(position);

            switch (geometry.getNumPoints()) {
                case 1:
                    coordinate.setMinLongitude(geometry.getCoordinate().x);
                    coordinate.setMinLatitude(geometry.getCoordinate().y);
                    break;
                case 2:
                    coordinate.setMinLongitude(geometry.getCoordinates()[0].x);
                    coordinate.setMinLatitude(geometry.getCoordinates()[0].y);
                    coordinate.setMaxLongitude(geometry.getCoordinates()[1].x);
                    coordinate.setMaxLatitude(geometry.getCoordinates()[1].y);
                    break;
                default:
                    Envelope envelope = geometry.getEnvelopeInternal();
                    coordinate.setMinLongitude(envelope.getMinX());
                    coordinate.setMinLatitude(envelope.getMinY());
                    if (envelope.getWidth() > 0 || envelope.getHeight() > 0) {
                        coordinate.setMaxLongitude(envelope.getMaxX());
                        coordinate.setMaxLatitude(envelope.getMaxY());
                    }
                    break;
            }
        }

        return coordinate;
    }

    public static Geometry getGeometry(CoordinateDTO coordinate) {
        Assert.isTrue(isValid(coordinate), "the CoordinateDTO object is invalid : " + toString(coordinate));
        return getGeometry(getPosition(coordinate));
    }

    public static String getPosition(CoordinateDTO coordinate) {
        if (!isValid(coordinate)) return null;

        if (isPoint(coordinate)) {
            Geometry geometry = createPoint(coordinate.getMinLongitude(), coordinate.getMinLatitude());
            return getWKTString(geometry);
        } else {
            Geometry geometry = createLine(coordinate.getMinLongitude(), coordinate.getMinLatitude(), coordinate.getMaxLongitude(), coordinate.getMaxLatitude());
            return getWKTString(geometry);
        }
    }

    /**
     * <p>isValid.</p>
     *
     * @param coordinate a {@link fr.ifremer.dali.dto.CoordinateDTO} object.
     * @return a boolean.
     */
    public static boolean isValid(CoordinateDTO coordinate) {
        return coordinate != null && (
                (coordinate.getMinLongitude() != null && coordinate.getMaxLongitude() == null && coordinate.getMinLatitude() != null && coordinate.getMaxLatitude() == null)
                        || (coordinate.getMinLongitude() != null && coordinate.getMaxLongitude() != null && coordinate.getMinLatitude() != null && coordinate.getMaxLatitude() != null)
        );

    }

    /**
     * <p>isPoint.</p>
     *
     * @param coordinate a {@link fr.ifremer.dali.dto.CoordinateDTO} object.
     * @return a boolean.
     */
    public static boolean isPoint(CoordinateDTO coordinate) {

        if (coordinate.getMinLongitude() != null && coordinate.getMinLatitude() != null
                && coordinate.getMaxLongitude() == null && coordinate.getMaxLatitude() == null) {

            // coordinate has only min long/lat
            return true;
        }

        // true if min long/lat equals max log/lat
        return coordinate.getMinLongitude() != null && coordinate.getMinLatitude() != null && coordinate.getMaxLongitude() != null && coordinate.getMaxLatitude() != null
                && Objects.equals(coordinate.getMinLongitude(), coordinate.getMaxLongitude()) && Objects.equals(coordinate.getMinLatitude(), coordinate.getMaxLatitude());

    }

    /**
     * <p>equals.</p>
     *
     * @param source a {@link fr.ifremer.dali.dto.CoordinateDTO} object.
     * @param target a {@link fr.ifremer.dali.dto.CoordinateDTO} object.
     * @return a boolean.
     */
    public static boolean equals(CoordinateDTO source, CoordinateDTO target) {

        if (source == null && target == null) {
            // assume both null are equals
            return true;
        } else if (source == null ^ target == null) {
            // but if one is null, return false
            return false;
        }

        if (isPoint(source) && isPoint(target)) {
            // if both are point, check only min long/lat
            return Objects.equals(source.getMinLongitude(), target.getMinLongitude()) && Objects.equals(source.getMinLatitude(), target.getMinLatitude());
        } else if (!isPoint(source) && !isPoint(target)) {
            // check all values
            return Objects.equals(source.getMinLongitude(), target.getMinLongitude()) && Objects.equals(source.getMinLatitude(), target.getMinLatitude())
                    && Objects.equals(source.getMaxLongitude(), target.getMaxLongitude()) && Objects.equals(source.getMaxLatitude(), target.getMaxLatitude());
        }

        // otherwise not equals
        return false;

    }

    /**
     * <p>equals.</p>
     *
     * @param source         a {@link fr.ifremer.dali.dto.CoordinateDTO} object.
     * @param targetPosition a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean equals(CoordinateDTO source, String targetPosition) {

        CoordinateDTO target = getCoordinate(targetPosition);

        return equals(source, target);
    }

    public static String toString(CoordinateDTO coordinate) {
        return coordinate == null
                ? "null"
                : String.format(
                "minLatitude: %s ; minLongitude: %s ; maxLatitude: %s ; maxLongitude: %s",
                coordinate.getMinLatitude(),
                coordinate.getMinLongitude(),
                coordinate.getMaxLatitude(),
                coordinate.getMaxLongitude()
        );
    }

    /**
     * Calculate distance between the min and max coordinate in meters (in a WGS84 system)
     * Only if the coordinate object is a line
     *
     * @param coordinate the coordinate object
     * @return the distance in meter or null if error in transformation occurs
     */
    public static Double getDistanceInMeter(CoordinateDTO coordinate) {

        if (isValid(coordinate) && !isPoint(coordinate)) {

            Coordinate point1 = new Coordinate(coordinate.getMinLongitude(), coordinate.getMinLatitude());
            Coordinate point2 = new Coordinate(coordinate.getMaxLongitude(), coordinate.getMaxLatitude());
            return getDistanceInMeter(point1, point2);
        }

        return null;
    }

    /**
     * Calculate distance between 2 coordinates in meters (in a WGS84 system)
     *
     * @param coordinate1 the first coordinate
     * @param coordinate2 the second coordinate
     * @return the distance in meter or null if error in transformation occurs
     */
    public static Double getDistanceInMeter(Coordinate coordinate1, Coordinate coordinate2) {

        Double distance = null;
        try {
            distance = JTS.orthodromicDistance(coordinate1, coordinate2, DefaultGeographicCRS.WGS84);
        } catch (TransformException e) {
            LOG.error(String.format("Can't calculate distance between %s and %s", coordinate1, coordinate2), e);
        }
        return distance;
    }

}
