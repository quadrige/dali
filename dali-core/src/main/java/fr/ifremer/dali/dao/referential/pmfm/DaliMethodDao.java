package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.dto.referential.pmfm.MethodDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 29/07/2015.
 */
public interface DaliMethodDao {

    /** Constant <code>ALL_METHODS_CACHE="allMethods"</code> */
    String ALL_METHODS_CACHE = "all_methods";
    /** Constant <code>METHOD_BY_ID_CACHE="methodById"</code> */
    String METHOD_BY_ID_CACHE = "method_by_id";

    /**
     * <p>getAllMethods.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_METHODS_CACHE)
    List<MethodDTO> getAllMethods(List<String> statusCodes);

    /**
     * <p>getMethodById.</p>
     *
     * @param methodId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.MethodDTO} object.
     */
    @Cacheable(value = METHOD_BY_ID_CACHE)
    MethodDTO getMethodById(int methodId);

    /**
     * <p>findMethods.</p>
     *
     * @param methodId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<MethodDTO> findMethods(Integer methodId, List<String> statusCodes);

}
