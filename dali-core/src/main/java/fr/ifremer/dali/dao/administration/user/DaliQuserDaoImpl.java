package fr.ifremer.dali.dao.administration.user;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.PrivilegeDTO;
import fr.ifremer.quadrige3.core.dao.administration.user.PrivilegeCode;
import fr.ifremer.quadrige3.core.dao.administration.user.Quser;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>DaliQuserDaoImpl class.</p>
 *
 */
@Repository("daliQuserDao")
@Lazy
public class DaliQuserDaoImpl extends QuserDaoImpl implements DaliQuserDao {

    private static final Multimap<String, String> columnNamesByProgramTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_LIST_RESP_QUSER", "QUSER_ID")
            .put("PROG_QUSER_PROG_PRIV", "QUSER_ID").build();

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_LIST_RESP_QUSER", "QUSER_ID").build();

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("CAMPAIGN", "QUSER_ID")
            .put("OCCAS_QUSER", "QUSER_ID")
            .put("CONTEXT", "QUSER_ID")
            .put("FILTER", "QUSER_ID")
            .put("DELETED_ITEM_HISTORY", "REC_QUSER_ID")
            .put("QUALIFICATION_HISTORY", "QUSER_ID").build();

    private static final Multimap<String, String> columnNamesBySurveyTableNames = ImmutableListMultimap.<String, String>builder()
            .put("SURVEY_QUSER", "QUSER_ID").build();

    @Resource
    protected DaliConfiguration config;

    @Resource
    protected CacheService cacheService;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * <p>Constructor for DaliQuserDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliQuserDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public PersonDTO getUserById(int quserId) {
        Object[] row = queryUnique("userById",
                "quserId", IntegerType.INSTANCE, quserId);

        if (row == null) {
            throw new DataRetrievalFailureException(String.format("can't load quser with id = %s", quserId));
        }
        return toPersonDTO(Arrays.asList(row).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> getAllUsers(List<String> statusCodes) {
        Cache personByIdCache = cacheService.getCache(USER_BY_ID_CACHE);

        Iterator<Object[]> list = Daos.queryIteratorWithStatus(createQuery("allUsers"), statusCodes);

        List<PersonDTO> result = new ArrayList<>();
        while (list.hasNext()) {
            Object[] source = list.next();
            PersonDTO target = toPersonDTO(Arrays.asList(source).iterator());
            result.add(target);

            // Add to cache by id
            personByIdCache.put(target.getId(), target);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> findUsersByCriteria(String lastName, String firstName, boolean strictName, String login, Integer departmentId, String privilegeCode, List<String> statusCodes) {
        Query q = createQuery("usersByCriteria",
                "lastName", StringType.INSTANCE, strictName ? null : lastName,
                "firstName", StringType.INSTANCE, strictName ? null : firstName,
                "strictLastName", StringType.INSTANCE, strictName ? lastName : null,
                "strictFirstName", StringType.INSTANCE, strictName ? firstName : null,
                "departmentId", IntegerType.INSTANCE, departmentId,
                "privilegeCd", StringType.INSTANCE, privilegeCode,
                "login", StringType.INSTANCE, login);

        Iterator<Object[]> list = Daos.queryIteratorWithStatus(q, statusCodes);
        List<PersonDTO> result = new ArrayList<>();
        while (list.hasNext()) {
            Object[] source = list.next();
            PersonDTO target = toPersonDTO(Arrays.asList(source).iterator());

            // append privileges
            target.addAllPrivilege(getPrivilegesByUserId(target.getId()));

            result.add(target);
        }
        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public PersonDTO getUserByLogin(List<String> statusCodes, String login) {
        Iterator<Object[]> list = Daos.queryIteratorWithStatus(createQuery("usersByLogin", "login", StringType.INSTANCE, login), statusCodes);

        // take the first user to eliminate duplicates (should be a local user if exists first)
        if (list.hasNext()) {
            Object[] row = list.next();
            return toPersonDTO(Arrays.asList(row).iterator());
        }
        return null;
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<PersonDTO> getUsersByIds(List<Integer> userIds) {
        List<PersonDTO> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(userIds)) {
            Query q = createQuery("usersByIds").setParameterList("quserIds", userIds);
            Iterator<Object[]> it = q.iterate();
            while (it.hasNext()) {
                result.add(toPersonDTO(Arrays.asList(it.next()).iterator()));
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<PrivilegeDTO> getAllPrivileges() {
        Iterator<Object[]> list = queryIterator("allPrivileges");

        List<PrivilegeDTO> result = new ArrayList<>();
        while (list.hasNext()) {
            Object[] source = list.next();
            PrivilegeDTO target = toPrivilegeDTO(Arrays.asList(source).iterator());
            result.add(target);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PrivilegeDTO> getPrivilegesByUserId(Integer userId) {
        Iterator<Object[]> list = queryIterator("privilegesByUserId",
                "userId", IntegerType.INSTANCE, userId);

        List<PrivilegeDTO> result = new ArrayList<>();
        while (list.hasNext()) {
            Object[] source = list.next();
            PrivilegeDTO target = toPrivilegeDTO(Arrays.asList(source).iterator());
            result.add(target);
        }
        return result;

    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInProgram(int id) {

        return executeMultipleCount(columnNamesByProgramTableNames, id);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInRules(int id) {
        return executeMultipleCount(columnNamesByRulesTableNames, id);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInData(int id) {

        return executeMultipleCount(columnNamesByDataTableNames, id)
                || executeMultipleCount(columnNamesBySurveyTableNames, id);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInValidatedData(int id) {

        // query SURVEY_QUSER first to fasten this method
        if (!executeMultipleCount(columnNamesBySurveyTableNames, id)) {
            return false;
        }

        // now query with join on survey
        long count = queryCount("countValidatedSurveyByQuserId", "quserId", IntegerType.INSTANCE, id);

        return count > 0;
    }

    /** {@inheritDoc} */
    @Override
    public Object transformEntity(final int transform, final Quser entity) {
        if (entity != null) {
            if (transform == DaliQuserDao.TRANSFORM_PERSON_DTO) { // fall-through
                return toPersonDTO(entity);
            }
            return super.transformEntity(transform, entity);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     *
     * Transforms a collection of entities using the {@link #transformEntities(int, Collection)} method. This method
     * does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     * @see #transformEntities(int, Collection)
     */
    @Override
    public void transformEntities(final int transform, final java.util.Collection<?> entities) {
        if (transform == TRANSFORM_PERSON_DTO) {
            toPersonDTOCollection(entities);
        } else {
            super.transformEntities(transform, entities);
            // do nothing;
        }
    }

    /* -- Internal Methods -- */
    /**
     * <p>toPersonDTO.</p>
     *
     * @param source a {@link java.util.Iterator} object.
     * @return a {@link fr.ifremer.dali.dto.referential.PersonDTO} object.
     */
    private PersonDTO toPersonDTO(Iterator<Object> source) {
        PersonDTO target = DaliBeanFactory.newPersonDTO();

        // Id
        target.setId((Integer) source.next());

        // Registration Code
        target.setRegCode((String) source.next());

        // LastName:
        String lastname = (String) source.next();
        if (StringUtils.isBlank(lastname) || "-".equals(lastname)) {
            lastname = "";
        }
        target.setName(lastname);

        // FirstName:
        String firstname = (String) source.next();
        if (StringUtils.isBlank(firstname) || "-".equals(firstname)) {
            firstname = "";
        }
        target.setFirstName(firstname);

        // Department (id)
        target.setDepartment(departmentDao.getDepartmentById((Integer) source.next()));

        // logins
        target.setIntranetLogin((String) source.next());
        target.setExtranetLogin((String) source.next());
        target.setHasPassword(StringUtils.isNotBlank((CharSequence) source.next()));

        // email
        target.setEmail((String) source.next());

        // phone
        target.setPhone((String) source.next());

        // address
        target.setAddress((String) source.next());

        // organism
        target.setOrganism((String) source.next());

        // admin center
        target.setAdminCenter((String) source.next());

        // site
        target.setSite((String) source.next());

        // Status (code)
        target.setStatus(referentialDao.getStatusByCode((String) source.next()));

        target.setComment((String) source.next());
        target.setCreationDate(Daos.convertToDate(source.next()));
        target.setUpdateDate(Daos.convertToDate(source.next()));

        return target;
    }

    /**
     * <p>toPersonDTO.</p>
     *
     * @param source a {@link fr.ifremer.quadrige3.core.dao.administration.user.Quser} object.
     * @return a {@link fr.ifremer.dali.dto.referential.PersonDTO} object.
     */
    private PersonDTO toPersonDTO(Quser source) {
        PersonDTO target = DaliBeanFactory.newPersonDTO();
        target.setId(source.getQuserId());
        target.setRegCode(source.getQuserCd());
        target.setName(source.getQuserLastNm());
        target.setFirstName(source.getQuserFirstNm());
        target.setDepartment(departmentDao.getDepartmentById(source.getDepartment().getDepId()));
        target.setIntranetLogin(source.getQuserIntranetLg());
        target.setExtranetLogin(source.getQuserExtranetLg());
        target.setEmail(source.getQuserEMail());
        target.setPhone(source.getQuserPhone());
        target.setAddress(source.getQuserAddress());
        target.setOrganism(source.getQuserOrgan());
        target.setAdminCenter(source.getQuserAdminCenter());
        target.setSite(source.getQuserSite());
        referentialDao.getStatusByCode(source.getStatus().getStatusCd());
        target.setComment(source.getQuserCm());
        target.setCreationDate(source.getQuserCreationDt());
        target.setUpdateDate(source.getUpdateDt());
        return target;
    }

    /**
     * <p>toPersonDTOCollection.</p>
     *
     * @param entities a {@link java.util.Collection} object.
     */
    @SuppressWarnings("unchecked")
    private void toPersonDTOCollection(java.util.Collection<?> entities) {
        if (entities != null) {
            org.apache.commons.collections4.CollectionUtils.transform(entities, PERSON_DTO_TRANSFORMER);
        }
    }

    private final org.apache.commons.collections4.Transformer PERSON_DTO_TRANSFORMER
            = input -> {
                Object result = null;
                if (input instanceof Quser) {
                    result = toPersonDTO((Quser) input);
                }
                return result;
            };

    private PrivilegeDTO toPrivilegeDTO(Iterator<Object> source) {
        PrivilegeDTO target = DaliBeanFactory.newPrivilegeDTO();

        target.setCode((String) source.next());
        target.setName((String) source.next());
        target.setDescription((String) source.next());

        // Override names
        switch (PrivilegeCode.fromValue(target.getCode())) {
            case REFERENTIAL_ADMINISTRATOR:
                target.setName(t("quadrige3.security.authority.ROLE_ADMIN"));
                break;
            case LOCAL_ADMINISTRATOR:
                target.setName(t("quadrige3.security.authority.ROLE_LOCAL_ADMIN"));
                break;
            case VALIDATOR:
                target.setName(t("quadrige3.security.authority.ROLE_VALIDATOR"));
                break;
            case QUALIFIER:
                target.setName(t("quadrige3.security.authority.ROLE_QUALIFIER"));
                break;
        }

        return target;
    }

}
