package fr.ifremer.dali.dao.system.extraction;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.quadrige3.core.dao.referential.transcribing.TranscribingItemExtendDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.csv.CSVDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * Dao for extraction with JDBC access on spring data source
 * <p/>
 * Created by Ludovic on 11/12/2015.
 */
@Repository("daliExtractionResultDao")
@Lazy
public class DaliExtractionResultDaoImpl extends CSVDaoImpl implements DaliExtractionResultDao {

    private final Properties connectionProperties;

    @Resource(name = "transcribingItemDao")
    private TranscribingItemExtendDao transcribingItemDao;

    @Resource
    protected DaliConfiguration config;

    /**
     * <p>Constructor for DaliExtractionResultDaoImpl.</p>
     *
     * @param dataSource a {@link javax.sql.DataSource} object.
     */
    @Autowired
    public DaliExtractionResultDaoImpl(DataSource dataSource) {
        super(dataSource);
        this.connectionProperties = null;
    }

    /**
     * <p>Constructor for DaliExtractionResultDaoImpl.</p>
     */
    public DaliExtractionResultDaoImpl() {
        this((Properties) null);
    }

    /**
     * <p>Constructor for DaliExtractionResultDaoImpl.</p>
     *
     * @param connectionProperties a {@link java.util.Properties} object.
     */
    public DaliExtractionResultDaoImpl(Properties connectionProperties) {
        super();
        this.connectionProperties = connectionProperties;
    }

    @Override
    public long queryCount(String query) {
        return queryCount(query, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long queryCount(String query, Map<String, Object> queryBindings) {
        Long count = queryCount(connectionProperties, query, queryBindings);
        if (count == null) throw new DataRetrievalFailureException(String.format("query count result is null: %s ; bindings=%s", query, queryBindings));
        return count;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> List<T> query(String sql, Map<String, Object> queryBindings, RowMapper<T> rowMapper) {
        return query(connectionProperties, sql, queryBindings, rowMapper);
    }

    @Override
    public int queryUpdate(String query) {
        return queryUpdate(query, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int queryUpdate(String query, Map<String, Object> paramMap) {
        return queryUpdate(connectionProperties, query, paramMap);
    }

    @Override
    public String getPmfmNameForExtraction(PmfmDTO pmfm) {
        Assert.notNull(pmfm);
        return Optional
                .ofNullable(transcribingItemDao.getTranscribingItemById(config.getTranscribingItemTypeLbForPmfmExtraction(), pmfm.getId()))
                .orElse(
                        Optional.ofNullable(transcribingItemDao.getTranscribingItemById(config.getTranscribingItemTypeLbForPmfmNm(), pmfm.getId()))
                                .orElse(
                                        Optional.ofNullable(pmfm.getName())
                                                .orElse(pmfm.getParameter().getName())
                                )
                );
    }

    @Override
    public String getPmfmUnitNameForExtraction(PmfmDTO pmfm) {
        Assert.notNull(pmfm);
        return pmfm.getUnit() == null || config.getExtractionUnitIdsToIgnore().contains(pmfm.getUnit().getId()) ? "" : pmfm.getUnit().getName();
    }

    @Override
    public String getQualitativeValueNameForExtraction(QualitativeValueDTO qualitativeValue) {
        Assert.notNull(qualitativeValue);
        return Optional
                .ofNullable(transcribingItemDao.getTranscribingItemById(config.getTranscribingItemTypeLbForQualitativeValueExtraction(), qualitativeValue.getId()))
                .orElse(
                        Optional.ofNullable(transcribingItemDao.getTranscribingItemById(config.getTranscribingItemTypeLbForQualitativeValueNm(), qualitativeValue.getId()))
                                .orElse(qualitativeValue.getName())
                );
    }

}
