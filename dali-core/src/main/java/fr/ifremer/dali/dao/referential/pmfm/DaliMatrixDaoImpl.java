package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.MatrixDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Matrix DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("daliMatrixDao")
public class DaliMatrixDaoImpl extends MatrixDaoImpl implements DaliMatrixDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliFractionDao")
    private DaliFractionDao fractionDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliMatrixDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<MatrixDTO> getAllMatrices(List<String> statusCodes) {

        List<MatrixDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allMatrices"), statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            MatrixDTO matrix = toMatrixDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            // add fractions
            matrix.addAllFractions(fractionDao.getFractionsByMatrixId(matrix.getId()));
            result.add(matrix);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public MatrixDTO getMatrixById(int matrixId) {

        Object[] source = queryUnique("matrixById", "matrixId", IntegerType.INSTANCE, matrixId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load matrix with id = " + matrixId);
        }

        MatrixDTO result = toMatrixDTO(Arrays.asList(source).iterator(), getTranscribingNames());

        // add fractions
        result.addAllFractions(fractionDao.getFractionsByMatrixId(result.getId()));

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<MatrixDTO> findMatrices(Integer matrixId, List<String> statusCodes) {
        List<MatrixDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Query query = createQuery("matrixByCriteria", "matrixId", IntegerType.INSTANCE, matrixId);
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            MatrixDTO matrix = toMatrixDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            // add fractions
            matrix.addAllFractions(fractionDao.getFractionsByMatrixId(matrix.getId()));

            result.add(matrix);
        }

        return ImmutableList.copyOf(result);

    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForMatrixNm());
    }

    private MatrixDTO toMatrixDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        MatrixDTO result = DaliBeanFactory.newMatrixDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // transcribed name
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        return result;
    }

}
