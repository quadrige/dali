package fr.ifremer.dali.dao.administration.user;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * <p>DaliDepartmentDaoImpl class.</p>
 *
 */
@Repository("daliDepartmentDao")
public class DaliDepartmentDaoImpl extends DepartmentDaoImpl implements DaliDepartmentDao {

    @Resource
    protected CacheService cacheService;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * <p>Constructor for DaliDepartmentDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliDepartmentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<DepartmentDTO> getAllDepartments(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(DEPARTMENT_BY_ID_CACHE);

        Iterator<Object[]> rows = Daos.queryIteratorWithStatus(createQuery("allDepartments"), statusCodes);

        List<DepartmentDTO> result = new ArrayList<>();
        if (rows != null) {
            while (rows.hasNext()) {
                Object[] row = rows.next();
                DepartmentDTO department = toDepartmentDTO(Arrays.asList(row).iterator(), true);
                result.add(department);

                cacheById.put(department.getId(), department);
            }
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getDepartmentById(int departmentId) {
        return getDepartmentById(departmentId, false);
    }

    private DepartmentDTO getDepartmentById(int departmentId, boolean fetchParent) {

        Object[] row = queryUnique("departmentById",
                "departmentId", IntegerType.INSTANCE, departmentId);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load department with id = " + departmentId);
        }

        return toDepartmentDTO(Arrays.asList(row).iterator(), fetchParent);
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<DepartmentDTO> getDepartmentsByIds(List<Integer> departmentIds) {

        List<DepartmentDTO> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(departmentIds))
            return result;

        Iterator<Object[]> it = createQuery("departmentsByIds")
                .setParameterList("departmentIds", departmentIds)
                .iterate();

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toDepartmentDTO(Arrays.asList(row).iterator(), false));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<DepartmentDTO> findDepartmentsByCodeAndName(String code, String name, boolean strictName, Integer parentId, List<String> statusCodes) {
        Query query = createQuery("departmentsByCodeAndName",
                "departmentCode", StringType.INSTANCE, strictName ? null : code,
                "departmentName", StringType.INSTANCE, strictName ? null : name,
                "strictDepartmentCode", StringType.INSTANCE, strictName ? code : null,
                "strictDepartmentName", StringType.INSTANCE, strictName ? name : null,
                "parentId", IntegerType.INSTANCE, parentId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<DepartmentDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toDepartmentDTO(Arrays.asList(source).iterator(), true));
        }

        return ImmutableList.copyOf(result);
    }

    // INTERNAL METHODS
    private DepartmentDTO toDepartmentDTO(Iterator<Object> source, boolean fetchParent) {
        DepartmentDTO result = DaliBeanFactory.newDepartmentDTO();
        result.setId((Integer) source.next());
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setEmail((String) source.next());
        result.setAddress((String) source.next());
        result.setPhone((String) source.next());
        result.setSiret((String) source.next());
        result.setUrl((String) source.next());
        Integer parentId = (Integer) source.next();
        if (parentId != null && fetchParent) {
            result.setParentDepartment(getDepartmentById(parentId));
        }
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
