package fr.ifremer.dali.dao.referential.monitoringLocation;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dao.technical.Geometries;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.HarbourDTO;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <p>DaliMonitoringLocationDaoImpl class.</p>
 */
@Repository("daliMonitoringLocationDao")
public class DaliMonitoringLocationDaoImpl extends MonitoringLocationDaoImpl implements DaliMonitoringLocationDao {

    @Resource
    protected DaliConfiguration config;

    @Resource
    protected CacheService cacheService;

    @Resource(name = "daliReferentialDao")
    private DaliReferentialDao referentialDao;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliMonitoringLocationDao")
    private DaliMonitoringLocationDao loopbackDao;

    /**
     * <p>Constructor for DaliMonitoringLocationDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliMonitoringLocationDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getLocationsByCampaignAndProgram(Integer campaignId, String programCode, List<String> statusCodes) {

        Iterator<Object[]> it;
        if (campaignId == null) {
            it = Daos.queryIteratorWithStatus(
                createQuery("monitoringLocationsByProgramCode",
                    "programCode", StringType.INSTANCE, programCode),
                statusCodes
            );
        } else {
            it = Daos.queryIteratorWithStatus(
                createQuery("monitoringLocationsByCampaignIdAndProgramCode",
                    "programCode", StringType.INSTANCE, programCode,
                    "campaignId", IntegerType.INSTANCE, campaignId),
                statusCodes
            );
        }

        List<LocationDTO> result = new ArrayList<>();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toLocationDTO(Arrays.asList(row).iterator(), transcribingNamesById));
        }

        return ImmutableList.copyOf(result);
    }

    /**
     * {@inheritDoc}
     *
     * @param locationIds
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<LocationDTO> getLocationsByIds(List<Integer> locationIds) {

        List<LocationDTO> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(locationIds))
            return result;

        Iterator<Object[]> it = createQuery("monitoringLocationsByIds")
            .setParameterList("locationIds", locationIds)
            .iterate();

        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toLocationDTO(Arrays.asList(row).iterator(), transcribingNamesById));
        }

        return result;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getAllLocations(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(LOCATION_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allMonitoringLocations"), statusCodes);

        List<LocationDTO> result = new ArrayList<>();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        while (it.hasNext()) {
            Object[] row = it.next();
            LocationDTO location = toLocationDTO(Arrays.asList(row).iterator(), transcribingNamesById);
            result.add(location);
            cacheById.put(location.getId(), location);
        }

        return ImmutableList.copyOf(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocationDTO getLocationById(int locationId) {

        Object[] row = queryUnique("monitoringLocationById", "monitoringLocationId", IntegerType.INSTANCE, locationId);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load monitoring location with id = " + locationId);
        }

        return toLocationDTO(Arrays.asList(row).iterator(), getTranscribingNames());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> findLocations(List<String> statusCodes, String orderItemTypeCode, Integer orderItemId, String programCode, String label, String name, boolean isStrictName) {

        List<LocationDTO> result = loopbackDao.getAllLocations(statusCodes);
        Predicate<LocationDTO> filter = location -> true;

        // Predicate on grouping type
        if (orderItemTypeCode != null || orderItemId != null) {
            List<Integer> locationIds = getLocationIdsByOrderItem(orderItemTypeCode, orderItemId);
            filter = filter.and(location -> locationIds.contains(location.getId()));
        }

        // Predicate on program
        if (programCode != null) {
            List<Integer> locationIds = loopbackDao.getLocationsByCampaignAndProgram(null, programCode, statusCodes).stream().map(LocationDTO::getId).collect(Collectors.toList());
            filter = filter.and(location -> locationIds.contains(location.getId()));
        }

        // Predicate on label
        if (label != null)
            filter = filter.and(location -> location.getLabel().toLowerCase().contains(label.toLowerCase()));

        // Predicate on name after transcribing (Mantis #49923)
        if (name != null)
            filter = filter.and(location -> isStrictName ? location.getName().equalsIgnoreCase(name) : location.getName().toLowerCase().contains(name.toLowerCase()));

        // Filter
        result = result.stream().filter(filter).collect(Collectors.toList());

        return ImmutableList.copyOf(result);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<HarbourDTO> getAllHarbours(List<String> statusCodes) {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allHarbours"), statusCodes);

        List<HarbourDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toHarbourDTO(Arrays.asList(row).iterator()));
        }

        return result;
    }

    // INTERNAL METHODS

    private List<Integer> getLocationIdsByOrderItem(String orderItemTypeCode, Integer orderItemId) {
        return queryListTyped("monitoringLocationIdsByOrderItem",
            "orderItemTypeCode", StringType.INSTANCE, orderItemTypeCode,
            "orderItemId", IntegerType.INSTANCE, orderItemId
        );
    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForMonLocNm());
    }

    private LocationDTO toLocationDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        LocationDTO result = DaliBeanFactory.newLocationDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setBathymetry(Daos.convertToDouble((Float) source.next()));
        result.setComment((String) source.next());
        result.setLabel((String) source.next());
        result.setUtFormat((Double) source.next());
        result.setDayLightSavingTime(Daos.convertToBoolean(source.next()));
        HarbourDTO harbour = toHarbourDTO(source);
        result.setHarbour(StringUtils.isNotBlank(harbour.getCode()) ? harbour : null);
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setPositioning(referentialDao.getPositioningSystemById((Integer) source.next()));
        result.setCoordinate(Geometries.getCoordinate((String) source.next()));
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // transcribed name (Mantis #47219)
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        return result;
    }

    private HarbourDTO toHarbourDTO(Iterator<Object> source) {
        HarbourDTO result = DaliBeanFactory.newHarbourDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        return result;
    }

}
