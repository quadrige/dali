package fr.ifremer.dali.dao.data.survey;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.data.survey.SurveyExtendDao;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.system.QualificationHistoryDTO;
import fr.ifremer.dali.dto.system.ValidationHistoryDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>DaliSurveyDao interface.</p>
 *
 * @author Ludovic
 */
public interface DaliSurveyDao extends SurveyExtendDao {

    /**
     * Get all survey corresponding on search parameters If a parameter is null, it means no filter on this criteria
     *  @param campaignId  the campaign id to filter
     * @param programCodes the program code to filter
     * @param locationId  the monitoring location id to filter
     * @param name        the survey name (=mnemonic)
     * @param comment     a {@link String} object.
     * @param stateId     the state id to filter
     * @param sharingId   the sharing id to filter
     * @param startDate   the start date to filter
     * @param endDate     the end date to filter
     * @param strictDate  the indicator for date filtering
*                    <p>
*                    if strictDate is <ul>
*                    <li><b>false</b>: 'moreOrEquals' used for startDate, 'lessOrEquals' used for endDate<br>only if startDate or endDate not null.</li>
*                    <li><b>true</b>: 'more' used for startDate, 'less' used for endDate<br>only if startDate or endDate not null.</li></ul>
*                    Note : if startDate and endDate are both not null and equals, the 'equals' is used if they are not equals, the 'between' is used, regardless
*                    strictDate</p>       @return a {@link List} object.
     */
    List<SurveyDTO> getSurveysByCriteria(
            Integer campaignId,
            Collection<String> programCodes,
            Integer locationId,
            String name, String comment,
            Integer stateId,
            Integer sharingId,
            Date startDate,
            Date endDate,
            boolean strictDate);

    /**
     * <p>countSurveysWithProgramAndLocations.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @param locationIds a {@link java.util.List} object.
     * @return a {@link java.lang.Long} object.
     */
    long countSurveysWithProgramAndLocations(String programCode, List<Integer> locationIds);

    long countSurveysWithProgramLocationAndOutsideDates(String programCode, int appliedStrategyId, int locationId, Date startDate, Date endDate);

    long countSurveysWithProgramLocationAndInsideDates(String programCode, int appliedStrategyId, int locationId, Date startDate, Date endDate);

    /**
     * Get a full survey by its Id
     *
     * @param surveyId        a int.
     * @param dontExcludePmfm a boolean.
     * @return a {@link fr.ifremer.dali.dto.data.survey.SurveyDTO} object.
     */
    SurveyDTO getSurveyById(int surveyId, boolean dontExcludePmfm);

    /**
     * Get the observer list of a survey
     * <p>
     * Used for convenience access to this list without full survey load
     *
     * @param surveyId a int.
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> getObservers(int surveyId);

    /**
     * Save a survey
     *
     * @param bean the survey bean to create
     * @return the created bean
     */
    SurveyDTO save(SurveyDTO bean);

    /**
     * Remove a list of survey
     *
     * @param surveyIds a {@link java.util.List} object.
     */
    void remove(List<Integer> surveyIds);

    /**
     * <p>validate.</p>
     *
     * @param surveyId           a {@link Integer} object.
     * @param validationComment  a {@link String} object.
     * @param validationDate     a {@link Date} object.
     * @param validatorId        a {@link Integer} object.
     * @param readyToSynchronize a boolean.
     */
    void validate(int surveyId, String validationComment, Date validationDate, Integer validatorId, boolean readyToSynchronize);

    /**
     * <p>unvalidate.</p>
     *
     * @param surveyId            a {@link Integer} object.
     * @param unvalidationComment a {@link String} object.
     * @param unvalidationDate    the unvalidation date
     * @param validatorId         a {@link Integer} object.
     */
    void unvalidate(int surveyId, String unvalidationComment, Date unvalidationDate, Integer validatorId);

    void qualify(int surveyId, String qualificationComment, Date qualificationDate, Integer validatorId, String qualityFlagCode);

    List<ValidationHistoryDTO> getValidationHistory(int surveyId);

    List<QualificationHistoryDTO> getQualificationHistory(int surveyId);

    /**
     * <p>updateSurveysControlDate.</p>
     *
     * @param surveyIds   a {@link java.util.Collection} object.
     * @param controlDate a {@link java.util.Date} object.
     */
    void updateSurveysControlDate(Collection<Integer> surveyIds, Date controlDate);

    boolean isQualified(int surveyId);

}
