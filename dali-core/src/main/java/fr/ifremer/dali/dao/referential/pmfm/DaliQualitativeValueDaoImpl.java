package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.QualitativeValueDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.service.StatusFilter;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Dao for QualitativeValue
 * Created by Ludovic on 22/09/2015.
 */
@Repository("daliQualitativeValueDao")
public class DaliQualitativeValueDaoImpl extends QualitativeValueDaoImpl implements DaliQualitativeValueDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliQualitativeValueDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<QualitativeValueDTO> getQualitativeValuesByParameterCode(String parameterCode) {

        Cache cacheById = cacheService.getCache(QUALITATIVE_VALUE_BY_ID_CACHE);
        List<QualitativeValueDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(
                createQuery("allQualitativeValuesByParameterCode", "parameterCode", StringType.INSTANCE, parameterCode),
                StatusFilter.ALL.toStatusCodes());

        while (it.hasNext()) {
            Object[] source = it.next();
            QualitativeValueDTO qualitativeValue = toQualitativeValueDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            result.add(qualitativeValue);
            cacheById.put(qualitativeValue.getId(), qualitativeValue);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<QualitativeValueDTO> getQualitativeValuesByPmfmId(Integer pmfmId) {

        List<QualitativeValueDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(
                createQuery("allQualitativeValuesByPmfmId", "pmfmId", IntegerType.INSTANCE, pmfmId),
                StatusFilter.ACTIVE.toStatusCodes());

        while (it.hasNext()) {
            Object[] source = it.next();
            QualitativeValueDTO qualitativeValue = toQualitativeValueDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            result.add(qualitativeValue);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public QualitativeValueDTO getQualitativeValueById(int qualitativeValueId) {
        Object[] source = queryUnique("qualitativeValueById", "qualitativeValueId", IntegerType.INSTANCE, qualitativeValueId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load qualitative value with id = " + qualitativeValueId);
        }

        return toQualitativeValueDTO(Arrays.asList(source).iterator(), getTranscribingNames());
    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForQualitativeValueNm());
    }

    private QualitativeValueDTO toQualitativeValueDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        QualitativeValueDTO result = DaliBeanFactory.newQualitativeValueDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));

        // transcribed name
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        return result;
    }

}
