package fr.ifremer.dali.dao.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.UnitDTO;
import fr.ifremer.quadrige3.core.dao.referential.UnitDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Unit DAO
 * <p/>
 * Created by Ludovic on 28/07/2015.
 */
@Repository("daliUnitDao")
public class DaliUnitDaoImpl extends UnitDaoImpl implements DaliUnitDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliUnitDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public UnitDTO getUnitById(int unitId) {

        Object[] source = queryUnique("unitById", "unitId", IntegerType.INSTANCE, unitId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load unit with id = " + unitId);
        }

        return toUnitDTO(Arrays.asList(source).iterator(), getTranscribingNames(), getTranscribingSymbols());
    }

    /** {@inheritDoc} */
    @Override
    public List<UnitDTO> getAllUnits(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(UNIT_BY_ID_CACHE);
        List<UnitDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();
        Map<Integer, String> transcribingSymbolsById = getTranscribingSymbols();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allUnits"), statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            UnitDTO unit = toUnitDTO(Arrays.asList(source).iterator(), transcribingNamesById, transcribingSymbolsById);
            if (unit != null) {
                result.add(unit);

                // update cache
                cacheById.put(unit.getId(), unit);
            }
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<UnitDTO> findUnits(Integer unitId, List<String> statusCodes) {

        List<UnitDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();
        Map<Integer, String> transcribingSymbolsById = getTranscribingSymbols();

        Query query = createQuery("unitsByCriteria", "unitId", IntegerType.INSTANCE, unitId);
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            UnitDTO unit = toUnitDTO(Arrays.asList(source).iterator(), transcribingNamesById, transcribingSymbolsById);
            if (unit != null) result.add(unit);
        }

        return ImmutableList.copyOf(result);
    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForUnitNm());
    }

    private Map<Integer, String> getTranscribingSymbols() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForUnitSymbol());
    }

    private UnitDTO toUnitDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById, Map<Integer, String> transcribingSymbolsById) {
        UnitDTO result = DaliBeanFactory.newUnitDTO();
        result.setId((Integer) source.next());
        if (result.getId() == null) {
            source.next();
            source.next();
            source.next();
            return null;
        }
        result.setName((String) source.next());
        result.setSymbol((String) source.next());
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // transcribed name
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        // transcribed symbol
        result.setSymbol(transcribingSymbolsById.getOrDefault(result.getId(), result.getSymbol()));

        return result;
    }

}
