package fr.ifremer.dali.dao.administration.user;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentExtendDao;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>DaliDepartmentDao interface.</p>
 *
 */
public interface DaliDepartmentDao extends DepartmentExtendDao {

    /** Constant <code>ALL_DEPARTMENTS_CACHE="allDepartments"</code> */
    String ALL_DEPARTMENTS_CACHE = "all_departments";
    /** Constant <code>DEPARTMENT_BY_ID_CACHE="departmentById"</code> */
    String DEPARTMENT_BY_ID_CACHE = "department_by_id";
    /** Constant <code>DEPARTMENTS_BY_IDS_CACHE="departmentsByIds"</code> */
    String DEPARTMENTS_BY_IDS_CACHE = "departments_by_ids";
    String INHERITED_DEPARTMENT_IDS_CACHE = "inherited_department_ids";

    /**
     * <p>getAllDepartments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_DEPARTMENTS_CACHE)
    List<DepartmentDTO> getAllDepartments(List<String> statusCodes);

    /**
     * <p>getDepartmentById.</p>
     *
     * @param departmentId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.DepartmentDTO} object.
     */
    @Cacheable(value = DEPARTMENT_BY_ID_CACHE)
    DepartmentDTO getDepartmentById(int departmentId);

    /**
     * <p>getDepartmentsByIds.</p>
     *
     * @param departmentIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = DEPARTMENTS_BY_IDS_CACHE)
    List<DepartmentDTO> getDepartmentsByIds(List<Integer> departmentIds);

    /**
     * <p>findDepartmentsByCodeAndName.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     * @param strictName a boolean.
     * @param parentId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<DepartmentDTO> findDepartmentsByCodeAndName(String code, String name, boolean strictName, Integer parentId, List<String> statusCodes);

    @Cacheable(value = INHERITED_DEPARTMENT_IDS_CACHE)
    List<Integer> getInheritedRecorderDepartmentIds(int departmentId);

}
