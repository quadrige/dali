package fr.ifremer.dali.dao.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.*;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>DaliReferentialDao interface.</p>
 *
 */
public interface DaliReferentialDao {

    String ALL_STATUS_CACHE = "all_status";
    String STATUS_BY_CODE_CACHE = "status_by_code";

    String DEPTH_LEVEL_BY_ID_CACHE = "depth_level_by_id";

    String POSITIONING_SYSTEM_BY_ID_CACHE = "positioning_system_by_id";

    String QUALITY_FLAG_BY_CODE_CACHE = "quality_flag_by_code";
    String ALL_QUALITY_FLAGS_CACHE = "all_quality_flags";

    String ALL_TAXONOMIC_LEVELS_CACHE = "all_taxonomic_levels";
    String TAXONOMIC_LEVEL_BY_CODE_CACHE = "taxonomic_level_by_code";

    @Cacheable(value = ALL_STATUS_CACHE)
    List<StatusDTO> getAllStatus();

    @Cacheable(value = STATUS_BY_CODE_CACHE)
    StatusDTO getStatusByCode(String statusCode);

    List<StatusDTO> getStatusByCodes(List<String> statusCodes);


    /**
     * <p>getAllDepthLevels.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = "all_depth_levels")
    List<LevelDTO> getAllDepthLevels();

    /**
     * <p>getDepthLevelById.</p>
     *
     * @param depthLevelId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.LevelDTO} object.
     */
    @Cacheable(value = DEPTH_LEVEL_BY_ID_CACHE)
    LevelDTO getDepthLevelById(int depthLevelId);

    /**
     * <p>getAllQualityFlags.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_QUALITY_FLAGS_CACHE)
    List<QualityLevelDTO> getAllQualityFlags(List<String> statusCodes);

    /**
     * <p>getQualityFlagByCode.</p>
     *
     * @param qualityFlagCode a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.dali.dto.referential.QualityLevelDTO} object.
     */
    @Cacheable(value = QUALITY_FLAG_BY_CODE_CACHE)
    QualityLevelDTO getQualityFlagByCode(String qualityFlagCode);

    /**
     * <p>getAllPositioningSystems.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = "all_positioning_systems")
    List<PositioningSystemDTO> getAllPositioningSystems();

    /**
     * <p>getPositioningSystemById.</p>
     *
     * @param posSystemId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.PositioningSystemDTO} object.
     */
    @Cacheable(value = POSITIONING_SYSTEM_BY_ID_CACHE)
    PositioningSystemDTO getPositioningSystemById(int posSystemId);

    /**
     * <p>getAllGroupingTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = "all_grouping_types")
    List<GroupingTypeDTO> getAllGroupingTypes();

    /**
     * <p>getGroupingsByType.</p>
     *
     * @param groupingType a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = "groupings_by_type")
    List<GroupingDTO> getGroupingsByType(String groupingType);

    /**
     * <p>getAllCitations.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = "all_citations")
    List<CitationDTO> getAllCitations();

    /**
     * <p>getAllPhotoTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PhotoTypeDTO> getAllPhotoTypes();

    /**
     * <p>getPhotoTypeByCode.</p>
     *
     * @param photoTypeCode a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.dali.dto.referential.PhotoTypeDTO} object.
     */
    PhotoTypeDTO getPhotoTypeByCode(String photoTypeCode);

    @Cacheable(value = ALL_TAXONOMIC_LEVELS_CACHE)
    List<TaxonomicLevelDTO> getAllTaxonomicLevels();

    @Cacheable(value = TAXONOMIC_LEVEL_BY_CODE_CACHE)
    TaxonomicLevelDTO getTaxonomicLevelByCode(String code);

}
