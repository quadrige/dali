package fr.ifremer.dali.dao.system.extraction;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.administration.program.DaliProgramDao;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.administration.user.DaliQuserDao;
import fr.ifremer.dali.dao.data.survey.DaliCampaignDao;
import fr.ifremer.dali.dao.referential.DaliAnalysisInstrumentDao;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.DaliSamplingEquipmentDao;
import fr.ifremer.dali.dao.referential.monitoringLocation.DaliMonitoringLocationDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliPmfmDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonGroupDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonNameDao;
import fr.ifremer.dali.dao.system.filter.DaliFilterDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.enums.ExtractionFilterTypeValues;
import fr.ifremer.dali.dto.referential.GroupingTypeDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionParameterDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.service.DaliBusinessException;
import fr.ifremer.dali.service.DaliDataContext;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.vo.ExtractionParameterVO;
import fr.ifremer.dali.vo.ExtractionVO;
import fr.ifremer.dali.vo.FilterVO;
import fr.ifremer.dali.vo.PresetVO;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.ProjectionSystemImpl;
import fr.ifremer.quadrige3.core.dao.system.extraction.*;
import fr.ifremer.quadrige3.core.dao.system.filter.Filter;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterTypeId;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.gson.Gsons;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>DaliExtractionDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("daliExtractionDao")
public class DaliExtractionDaoImpl extends ExtractFilterDaoImpl implements DaliExtractionDao, InitializingBean {

//    private static final Log log = LogFactory.getLog(DaliExtractionDaoImpl.class);

    private static final String FILE_PATTERN = "Extraction_%s.json";
    public static final String DATE_FORMAT = "ddMMyyyy";
    public static final String DATE_SEPARATOR = "_";

    @Resource(name = "daliDataContext")
    private DaliDataContext dataContext;

    @Resource(name = "daliQuserDao")
    private DaliQuserDao quserDao;

    @Resource(name = "daliFilterDao")
    private DaliFilterDao filterDao;

    @Resource(name = "daliReferentialDao")
    private DaliReferentialDao referentialDao;

    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;

    @Resource(name = "daliAnalysisInstrumentDao")
    private DaliAnalysisInstrumentDao analysisInstrumentDao;

    @Resource(name = "daliSamplingEquipmentDao")
    private DaliSamplingEquipmentDao samplingEquipmentDao;

    @Resource(name = "daliTaxonNameDao")
    private DaliTaxonNameDao taxonNameDao;

    @Resource(name = "daliTaxonGroupDao")
    private DaliTaxonGroupDao taxonGroupDao;

    @Resource(name = "daliMonitoringLocationDao")
    private DaliMonitoringLocationDao locationDao;

    @Resource(name = "daliProgramDao")
    private DaliProgramDao programDao;

    @Resource(name = "daliCampaignDao")
    private DaliCampaignDao campaignDao;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Autowired
    protected DaliConfiguration config;

    @Autowired
    protected CacheService cacheService;

    private String defaultFileTypeCd;
    private String defaultGroupTypePmfmCd;
    private String defaultTableTypeCd;
    private String defaultProjectionSystemCd;

    /**
     * <p>Constructor for DaliExtractionDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliExtractionDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() {
        initConstants();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionDTO> getAllExtractions() {
        return getAllExtractions(true);
    }

    @Override
    public List<ExtractionDTO> getAllLightExtractions() {
        return getAllExtractions(false);
    }

    private List<ExtractionDTO> getAllExtractions(boolean withFiltersAndConfiguration) {
        Iterator<Object[]> it = queryIterator("allExtractions");

        List<ExtractionDTO> extractions = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            ExtractionDTO extraction = toExtractionDTO(Arrays.asList(row).iterator());
            if (withFiltersAndConfiguration) {
                loadFilters(extraction);
                loadParameter(extraction);
            }
            extractions.add(extraction);
        }

        return extractions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtractionDTO getExtractionById(int extractionId) {

        Object[] row = queryUnique("extractionById", "extractionId", IntegerType.INSTANCE, extractionId);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load extraction with id = " + extractionId);
        }

        ExtractionDTO extraction = toExtractionDTO(Arrays.asList(row).iterator());
        loadFilters(extraction);
        loadParameter(extraction);
        return extraction;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionDTO> searchExtractionByProgram(String programCode) {
        Assert.notNull(programCode);

        Iterator<Object[]> it = queryIterator("extractionsByProgramCode",
                "programFilterTypeId", IntegerType.INSTANCE, FilterTypeId.PROGRAM.getValue(),
                "programCode", StringType.INSTANCE, programCode);

        List<ExtractionDTO> extractions = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            ExtractionDTO extraction = toExtractionDTO(Arrays.asList(row).iterator());
            loadFilters(extraction);
            loadParameter(extraction);
            extractions.add(extraction);
        }

        return extractions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveExtraction(ExtractionDTO extraction) {
        Assert.notNull(extraction);
        Assert.notNull(extraction.getName());
        Assert.notEmpty(extraction.getFilters());

        ExtractFilter entity;
        boolean isNew = false;
        if (extraction.getId() == null) {
            entity = ExtractFilter.Factory.newInstance();
            isNew = true;
        } else {
            entity = get(extraction.getId());
            if (entity == null) {
                throw new DataRetrievalFailureException("Could not retrieve extraction with id=" + extraction.getId());
            }
        }

        beanToEntity(extraction, entity);

        if (isNew) {

            getSession().save(entity);
            extraction.setId(entity.getExtractFilterId());
        } else {
            getSession().update(entity);
        }

        savePeriods(extraction, entity);

        saveFilters(extraction, entity);

        saveParameter(extraction);

        getSession().flush();
        getSession().clear();
    }

    @Override
    public void exportExtraction(ExtractionDTO extraction, File exportFile) {

        // Convert to VO
        ExtractionVO extractionVO = toExtractionVO(extraction);

        // Serialize to exportFile
        Gsons.serializeToFile(extractionVO, exportFile);

    }

    @Override
    public ExtractionDTO importExtraction(File importFile) {

        // Deserialize from importFile
        ExtractionVO extractionVO = Gsons.deserializeFile(importFile, ExtractionVO.class);

        // Convert to DTO
        return toExtractionDTO(extractionVO);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(ExtractFilter extractFilter) {

        extractFilter.getExtractSurveyPeriods().clear();
        extractFilter.getFilters().clear();

        super.remove(extractFilter);
    }

    // PRIVATE METHODS

    private void beanToEntity(ExtractionDTO extraction, ExtractFilter entity) {

        // mandatory attribute
        entity.setExtractFilterNm(extraction.getName());

        // add user
        if (extraction.getUser() == null) {
            int userId = dataContext.getPrincipalUserId();
            extraction.setUser(quserDao.getUserById(userId));
            entity.setQuser(quserDao.get(userId));
        }

        // optional attributes
        entity.setExtractFilterInSitu(Daos.convertToString(false));
        entity.setExtractFilterIsQualif(Daos.convertToString(false));

        // file type (= Text only)
        if (entity.getExtractFileTypes() != null) {
            if (entity.getExtractFileTypes().size() != 1 || !Objects.equals(entity.getExtractFileTypes().iterator().next().getExtractFileTypeCd(), defaultFileTypeCd)) {
                // bad file type, so recreate it
                entity.getExtractFileTypes().clear();
                entity.addExtractFileTypes(load(ExtractFileTypeImpl.class, defaultFileTypeCd));
            }
        } else {
            // no file type, so create it
            entity.setExtractFileTypes(Lists.newArrayList(load(ExtractFileTypeImpl.class, defaultFileTypeCd)));
        }

        // group type pmfm (=PMFM)
        if (entity.getExtractGroupTypePmfms() != null) {
            if (entity.getExtractGroupTypePmfms().size() != 1 || !Objects.equals(entity.getExtractGroupTypePmfms().iterator().next().getExtractGroupTypePmfmCd(), defaultGroupTypePmfmCd)) {
                // bad group type, so recreate it
                entity.getExtractGroupTypePmfms().clear();
                entity.addExtractGroupTypePmfms(load(ExtractGroupTypePmfmImpl.class, defaultGroupTypePmfmCd));
            }
        } else {
            // no group type, so create it
            entity.setExtractGroupTypePmfms(Lists.newArrayList(load(ExtractGroupTypePmfmImpl.class, defaultGroupTypePmfmCd)));
        }

        // table type (=inline)
        entity.setExtractTableType(load(ExtractTableTypeImpl.class, defaultTableTypeCd));
        // no aggregation level
        entity.setExtractAgregationLevel(null);

        // projection system
        entity.setProjectionSystem(load(ProjectionSystemImpl.class, defaultProjectionSystemCd));

    }

    private void savePeriods(ExtractionDTO extraction, ExtractFilter entity) {

        // Get existing periods
        Map<Integer, ExtractSurveyPeriod> existingExtractSurveyPeriods = DaliBeans.mapByProperty(entity.getExtractSurveyPeriods(), "extractSurveyPeriodId");

        // find period filter
        FilterDTO periodFilter = DaliBeans.findByProperty(extraction.getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID, ExtractionFilterTypeValues.PERIOD.getFilterTypeId());
        Assert.notNull(periodFilter);
        Assert.notEmpty(periodFilter.getElements());

        for (QuadrigeBean bean : periodFilter.getElements()) {
            ExtractionPeriodDTO period = (ExtractionPeriodDTO) bean;
            Assert.notNull(period.getStartDate());
            Assert.notNull(period.getEndDate());

            boolean isNew = false;
            ExtractSurveyPeriod extractSurveyPeriod = existingExtractSurveyPeriods.remove(period.getId());
            if (extractSurveyPeriod == null) {
                extractSurveyPeriod = ExtractSurveyPeriod.Factory.newInstance();
                extractSurveyPeriod.setExtractFilter(entity);
                isNew = true;
            }
            extractSurveyPeriod.setExtractSurveyPeriodStartDt(Dates.convertToDate(period.getStartDate(), config.getDbTimezone()));
            extractSurveyPeriod.setExtractSurveyPeriodEndDt(Dates.convertToDate(period.getEndDate(), config.getDbTimezone()));

            if (isNew) {
                getSession().save(extractSurveyPeriod);
                period.setId(extractSurveyPeriod.getExtractSurveyPeriodId());
            } else {
                getSession().update(extractSurveyPeriod);
            }
        }

        // remove remaining
        if (!existingExtractSurveyPeriods.isEmpty()) {
            entity.getExtractSurveyPeriods().removeAll(existingExtractSurveyPeriods.values());
        }
    }

    private void saveFilters(ExtractionDTO extraction, ExtractFilter entity) {

        int userId = extraction.getUser().getId();
        Map<Integer, Filter> existingFilters = DaliBeans.mapByProperty(entity.getFilters(), "filterId");

        if (CollectionUtils.isNotEmpty(extraction.getFilters())) {
            for (FilterDTO filter : extraction.getFilters()) {

                // don't save period as filter
                if (ExtractionFilterTypeValues.getExtractionFilterType(filter.getFilterTypeId()) == ExtractionFilterTypeValues.PERIOD) {
                    continue;
                }

                // compute a specific name for each filter
                filter.setName(String.format("EXT_%d_%s", extraction.getId(), ExtractionFilterTypeValues.getExtractionFilterType(filter.getFilterTypeId())));

                if (existingFilters.remove(filter.getId()) == null) {
                    // new filter
                    Filter newFilter = filterDao.saveFilter(filter, userId);
                    newFilter.setExtractFilter(entity);
                    getSession().update(newFilter);
                    entity.addFilters(newFilter);
                } else if (filter.isDirty()) {
                    // update filter
                    filterDao.saveFilter(filter, userId);
                }
            }
        }

        if (!existingFilters.isEmpty()) {
            // remove remaining
            for (Filter filterToRemove : existingFilters.values()) {
                entity.removeFilters(filterToRemove);
            }
        }
    }

    private ExtractionDTO toExtractionDTO(Iterator<Object> iterator) {
        ExtractionDTO extraction = DaliBeanFactory.newExtractionDTO();
        extraction.setId((Integer) iterator.next());
        extraction.setName((String) iterator.next());

        Integer userId = (Integer) iterator.next();
        if (userId != null) {
            extraction.setUser(quserDao.getUserById(userId));
        }

        return extraction;
    }

    private void loadFilters(ExtractionDTO extraction) {

        extraction.setFilters(getFilters(extraction.getId()));

        // get the orderItemType filter and load the groupingType now
        FilterDTO orderItemTypeFilter = DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.ORDER_ITEM_TYPE);
        if (orderItemTypeFilter != null) {
            if (!orderItemTypeFilter.isFilterLoaded()) {
                List<String> groupingTypeCodes = filterDao.getFilteredElementsByFilterId(orderItemTypeFilter.getId());
                orderItemTypeFilter.setElements(getGroupingType(groupingTypeCodes));
                orderItemTypeFilter.setFilterLoaded(true);
            }
        }

        // add extraction periods in filters
        FilterDTO periodFilter = DaliBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterTypeValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(getPeriods(extraction.getId()));
        periodFilter.setFilterLoaded(true);
        extraction.addFilters(periodFilter);

    }

    private List<GroupingTypeDTO> getGroupingType(Collection<String> groupingTypeCodes) {

        if (groupingTypeCodes != null) {
            if (CollectionUtils.size(groupingTypeCodes) > 1) {
                throw new DaliTechnicalException("Wrong number of grouping type code, must be <= 1");
            }

            return Collections.singletonList(DaliBeans.findByProperty(
                    referentialDao.getAllGroupingTypes(),
                    GroupingTypeDTO.PROPERTY_CODE,
                    groupingTypeCodes.iterator().next()));
        }
        return null;
    }

    private Collection<FilterDTO> getFilters(Integer extractionId) {

        List<FilterDTO> filters = Lists.newArrayList();
        Iterator<Integer> rows = queryIteratorTyped("extractionFilterIdByExtractionById", "extractionId", IntegerType.INSTANCE, extractionId);
        Cache filterCache = cacheService.getCache(DaliFilterDao.FILTER_BY_ID_CACHE);
        while (rows.hasNext()) {
            Integer filterId = rows.next();

            // must evict actual filter from cache because it continues to be linked even the extraction is not saved
            filterCache.evict(filterId);

            // get the filter but don't load elements
            filters.add(filterDao.getFilterById(filterId));
        }
        return filters;
    }

    private List<ExtractionPeriodDTO> getPeriods(int extractionId) {

        Iterator<Object[]> rows = queryIterator("extractionPeriodByExtractionById", "extractionId", IntegerType.INSTANCE, extractionId);
        List<ExtractionPeriodDTO> periods = Lists.newArrayList();

        while (rows.hasNext()) {
            Object[] row = rows.next();
            ExtractionPeriodDTO period = DaliBeanFactory.newExtractionPeriodDTO();
            period.setId((Integer) row[0]);
            period.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(row[1]), config.getDbTimezone()));
            period.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(row[2]), config.getDbTimezone()));
            periods.add(period);
        }
        return periods;
    }

    private void loadParameter(ExtractionDTO extraction) {

        Assert.notNull(extraction);
        Assert.notNull(extraction.getId());

        ExtractionParameterVO parameter = Gsons.deserializeFile(getParameterFile(extraction.getId()), ExtractionParameterVO.class);
        extraction.setParameter(toExtractionParameterDTO(parameter));
    }

    private void saveParameter(ExtractionDTO extraction) {

        Assert.notNull(extraction);
        Assert.notNull(extraction.getId());

        ExtractionParameterVO parameterVO = toExtractionParameterVO(extraction.getParameter());
        Gsons.serializeToFile(parameterVO, getParameterFile(extraction.getId()));

    }

    private File getParameterFile(int extractionId) {

        File dir = config.getExtractionConfigDirectory();
        if (!dir.isDirectory()) dir.mkdir();

        return new File(dir, String.format(FILE_PATTERN, extractionId));

    }

    private ExtractionVO toExtractionVO(ExtractionDTO extraction) {
        ExtractionVO extractionVO = new ExtractionVO();
        extractionVO.setName(extraction.getName());
        extractionVO.setParameter(toExtractionParameterVO(extraction.getParameter()));

        for (FilterDTO filter : extraction.getFilters()) {
            FilterVO filterVO = new FilterVO();
            filterVO.setType(filter.getFilterTypeId());
            if (filterVO.getType() == ExtractionFilterTypeValues.PERIOD.getFilterTypeId()) {
                filterVO.setElementIds(filter.getElements().stream()
                        .map((Function<QuadrigeBean, ExtractionPeriodDTO>) ExtractionPeriodDTO.class::cast)
                        .map(period -> Dates.formatDate(period.getStartDate(), DATE_FORMAT) + DATE_SEPARATOR + Dates.formatDate(period.getEndDate(), DATE_FORMAT))
                        .collect(Collectors.toList()));
            } else {
                filterVO.setElementIds(DaliBeans.collectStringIds(filter.getElements()));
            }
            extractionVO.addFilter(filterVO);
        }

        return extractionVO;
    }

    private ExtractionParameterVO toExtractionParameterVO(ExtractionParameterDTO extractionParameter) {
        ExtractionParameterVO parameterVO = new ExtractionParameterVO();

        if (extractionParameter != null) {

            // set fill zero parameter
            parameterVO.setFillZero(extractionParameter.isFillZero());

            // set pmfm preset
            parameterVO.setPreset(toPresetVO(extractionParameter.getPmfmPresets()));

            // set result pmfm Ids
            parameterVO.setResultPmfmIds(DaliBeans.collectIds(extractionParameter.getPmfmResults()));
        }
        return parameterVO;
    }

    private PresetVO toPresetVO(Collection<PmfmPresetDTO> pmfmPresets) {
        if (CollectionUtils.isEmpty(pmfmPresets)) return null;
        PresetVO presetVO = new PresetVO();
        for (PmfmPresetDTO pmfmPreset : pmfmPresets) {
            presetVO.addPmfmPreset(pmfmPreset.getPmfm().getId(), DaliBeans.collectIds(pmfmPreset.getQualitativeValues()));
        }
        return presetVO;
    }

    private ExtractionParameterDTO toExtractionParameterDTO(ExtractionParameterVO parameter) {
        ExtractionParameterDTO extractionParameterDTO = DaliBeanFactory.newExtractionParameterDTO();

        if (parameter != null) {
            extractionParameterDTO.setFillZero(parameter.isFillZero());

            // get pmfm presets and get beans
            extractionParameterDTO.setPmfmPresets(toPmfmPresetDTOs(parameter.getPreset()));

            // get result pmfms
            if (CollectionUtils.isNotEmpty(parameter.getResultPmfmIds())) {

                extractionParameterDTO.setPmfmResults(parameter.getResultPmfmIds().stream().map(pmfmId -> pmfmDao.getPmfmById(pmfmId)).collect(Collectors.toList()));
            }
        }
        return extractionParameterDTO;
    }

    private List<PmfmPresetDTO> toPmfmPresetDTOs(PresetVO preset) {
        if (preset == null) return null;
        List<PmfmPresetDTO> pmfmPresets = new ArrayList<>();
        for (Integer pmfmId : preset.getPmfmIds()) {

            PmfmPresetDTO pmfmPreset = DaliBeanFactory.newPmfmPresetDTO();
            PmfmDTO pmfm = pmfmDao.getPmfmById(pmfmId);
            pmfmPreset.setPmfm(pmfm);
            Collection<Integer> qvIds = preset.getQualitativeValueIds(pmfmId);
            pmfmPreset.setQualitativeValues(pmfm.getQualitativeValues().stream().filter(qv -> qvIds.contains(qv.getId())).collect(Collectors.toList()));

            pmfmPresets.add(pmfmPreset);
        }
        return pmfmPresets;
    }

    private ExtractionDTO toExtractionDTO(ExtractionVO extractionVO) {

        ExtractionDTO extraction = DaliBeanFactory.newExtractionDTO();
        // Find existing extraction filter with this name
        extraction.setName(getNextName(extractionVO.getName()));
        try {
            extraction.setParameter(toExtractionParameterDTO(extractionVO.getParameter()));
        } catch (DataRetrievalFailureException e) {
            throw new DaliBusinessException(t("dali.error.import.referentialNotFound.message", ExtractionFilterTypeValues.PMFM.getLabel()));
        }

        for (FilterVO filterVO : extractionVO.getFilters()) {
            FilterDTO filter = DaliBeanFactory.newFilterDTO();
            filter.setFilterTypeId(filterVO.getType());
            ExtractionFilterTypeValues filterType = ExtractionFilterTypeValues.getExtractionFilterType(filter.getFilterTypeId());
            Assert.notNull(filterType);
            List<? extends QuadrigeBean> elements = null;
            switch (filterType) {
                case PERIOD:
                    List<ExtractionPeriodDTO> periods = new ArrayList<>();
                    for (String dates : filterVO.getElementIds()) {
                        ExtractionPeriodDTO period = DaliBeanFactory.newExtractionPeriodDTO();
                        period.setStartDate(Dates.safeParseLocalDate(dates.substring(0, dates.indexOf(DATE_SEPARATOR)), DATE_FORMAT));
                        period.setEndDate(Dates.safeParseLocalDate(dates.substring(dates.indexOf(DATE_SEPARATOR) + 1), DATE_FORMAT));
                        periods.add(period);
                    }
                    elements = periods;
                    break;
                case PROGRAM:
                    elements = programDao.getProgramsByCodes(filterVO.getElementIds());
                    break;
                case LOCATION:
                    elements = locationDao.getLocationsByIds(filterVO.getElementIds().stream().map(Integer::valueOf).collect(Collectors.toList()));
                    break;
                case CAMPAIGN:
                    elements = campaignDao.getCampaignsByIds(filterVO.getElementIds().stream().map(Integer::valueOf).collect(Collectors.toList()));
                    break;
                case SAMPLING_EQUIPMENT:
                    elements = samplingEquipmentDao.getSamplingEquipmentsByIds(filterVO.getElementIds().stream().map(Integer::valueOf).collect(Collectors.toList()));
                    break;
                case TAXON:
                    elements = taxonNameDao.getTaxonNamesByIds(filterVO.getElementIds().stream().map(Integer::valueOf).collect(Collectors.toList()));
                    break;
                case TAXON_GROUP:
                    elements = taxonGroupDao.getTaxonGroupsByIds(filterVO.getElementIds().stream().map(Integer::valueOf).collect(Collectors.toList()));
                    break;
                case DEPARTMENT:
                    elements = departmentDao.getDepartmentsByIds(filterVO.getElementIds().stream().map(Integer::valueOf).collect(Collectors.toList()));
                    break;
                case PMFM:
                    elements = pmfmDao.getPmfmsByIds(filterVO.getElementIds().stream().map(Integer::valueOf).collect(Collectors.toList()));
                    break;
                case ORDER_ITEM_TYPE:
                    elements = getGroupingType(filterVO.getElementIds());
                    break;
            }

            if (CollectionUtils.size(filterVO.getElementIds()) != CollectionUtils.size(elements)) {
                throw new DaliBusinessException(t("dali.error.import.referentialNotFound.message", filterType.getLabel()));
            }

            filter.setElements(elements);

            filter.setFilterLoaded(true);
            extraction.addFilters(filter);
        }

        return extraction;
    }

    /**
     * Get next unused name. Compute "name (auto_inc_suffix)"
     *
     * @param name current name
     * @return the next unused name
     */
    private String getNextName(String name) {
        List<String> names = getAllLightExtractions().stream().map(ExtractionDTO::getName).collect(Collectors.toList());
        Pattern namePattern = Pattern.compile("(.*\\()([0-9]+)(\\)$)");
        int suffix = 0;

        while (names.contains(name)) {
            Matcher matcher = namePattern.matcher(name);
            boolean hasSuffix = matcher.matches();

            if (hasSuffix) {
                String group = matcher.group(2);
                suffix = Integer.valueOf(group);
                name = String.format("%s%d%s", matcher.group(1), ++suffix, matcher.group(3));
            } else {
                name = String.format("%s (%d)", name, ++suffix);
            }
        }

        return name;
    }

    private void initConstants() {

        defaultFileTypeCd = config.getExtractionFileTypeCode();
        defaultGroupTypePmfmCd = config.getExtractionGroupTypePmfmCode();
        defaultTableTypeCd = config.getExtractionTableTypeCode();
        defaultProjectionSystemCd = config.getExtractionProjectionSystemCode();

        // check constants in DB
        if (config.isDbCheckConstantsEnable()) {
            checkDbConstants();
        }

    }

    private void checkDbConstants() {

        Session session = getSessionFactory().openSession();
        try {
            Assert.notNull(session.get(ExtractFileTypeImpl.class, defaultFileTypeCd));
            Assert.notNull(session.get(ExtractGroupTypePmfmImpl.class, defaultGroupTypePmfmCd));
            Assert.notNull(session.get(ExtractTableTypeImpl.class, defaultTableTypeCd));
            Assert.notNull(session.get(ProjectionSystemImpl.class, defaultProjectionSystemCd));
        } finally {
            Daos.closeSilently(session);
        }
    }

}
