package fr.ifremer.dali.dao.data.survey;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.administration.program.DaliProgramDao;
import fr.ifremer.dali.dao.administration.strategy.DaliStrategyDao;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.administration.user.DaliQuserDao;
import fr.ifremer.dali.dao.data.measurement.DaliMeasurementDao;
import fr.ifremer.dali.dao.data.photo.DaliPhotoDao;
import fr.ifremer.dali.dao.data.samplingoperation.DaliSamplingOperationDao;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.monitoringLocation.DaliMonitoringLocationDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliPmfmDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dao.technical.Geometries;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.photo.PhotoDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.data.survey.OccasionDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.enums.StateValues;
import fr.ifremer.dali.dto.enums.SynchronizationStatusValues;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.QualificationHistoryDTO;
import fr.ifremer.dali.dto.system.ValidationHistoryDTO;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.quadrige3.core.dao.administration.program.Program;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.Quser;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.CampaignImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.OccasionImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.Survey;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyDaoImpl;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationImpl;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.PositionningSystemImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.PmfmImpl;
import fr.ifremer.quadrige3.core.dao.system.*;
import fr.ifremer.quadrige3.core.dao.system.synchronization.SynchronizationStatus;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>DaliSurveyDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("daliSurveyDao")
public class DaliSurveyDaoImpl extends SurveyDaoImpl implements DaliSurveyDao, InitializingBean {

    @Resource(name = "daliReferentialDao")
    private DaliReferentialDao referentialDao;

    @Resource(name = "daliMonitoringLocationDao")
    private DaliMonitoringLocationDao monitoringLocationDao;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Resource(name = "daliProgramDao")
    private DaliProgramDao programDao;

    @Resource(name = "daliStrategyDao")
    private DaliStrategyDao strategyDao;

    @Resource(name = "daliSamplingOperationDao")
    private DaliSamplingOperationDao samplingOperationDao;

    @Resource(name = "daliMeasurementDao")
    private DaliMeasurementDao measurementDao;

    @Resource(name = "daliPhotoDao")
    private DaliPhotoDao photoDao;

    @Resource(name = "daliQuserDao")
    private DaliQuserDao quserDao;

    @Resource(name = "objectTypeDao")
    private ObjectTypeDao objectTypeDao;

    @Resource(name = "validationHistoryDao")
    private ValidationHistoryExtendDao validationHistoryDao;

    @Resource(name = "qualificationHistoryDao")
    private QualificationHistoryDao qualificationHistoryDao;

    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;

    @Resource(name = "daliProgramStrategyService")
    protected ProgramStrategyService programStrategyService;

    @Resource
    protected DaliConfiguration config;

    private int pmfmIdSurveyCalculatedLength;
    private int unitIdMeter;
    private List<Integer[]> trawlAreaPmfmIdsTriplets;

    /**
     * <p>Constructor for DaliSurveyDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliSurveyDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() {
        initConstants();
    }

    private void initConstants() {

        pmfmIdSurveyCalculatedLength = config.getSurveyCalculatedLengthPmfmId();
        unitIdMeter = UnitId.METER.getValue();
        trawlAreaPmfmIdsTriplets = config.getCalculatedTrawlAreaPmfmTriplets();

        if (config.isDbCheckConstantsEnable()) {
            Session session = getSessionFactory().openSession();
            try {
                if (pmfmIdSurveyCalculatedLength > 0) {
                    Assert.notNull(session.get(PmfmImpl.class, pmfmIdSurveyCalculatedLength));
                }
                Assert.notNull(session.get(UnitImpl.class, unitIdMeter));
                trawlAreaPmfmIdsTriplets.forEach(
                        triplet -> Arrays.stream(triplet).forEach(
                                pmfmId -> Assert.notNull(session.get(PmfmImpl.class, pmfmId)))
                );
            } finally {
                Daos.closeSilently(session);
            }

        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<SurveyDTO> getSurveysByCriteria(Integer campaignId, Collection<String> programCodes, Integer locationId, String name, String comment, Integer stateId, Integer synchronizationStatusId,
                                                Date startDate, Date endDate, boolean strictDate) {

        if (CollectionUtils.isEmpty(programCodes))
            return new ArrayList<>();

        // load query from named query
        StringBuilder queryString = new StringBuilder(getSession().getNamedQuery("surveysByCriteria").getQueryString());

        // add more predicates
        if (stateId != null) {
            if (stateId.equals(StateValues.CONTROLLED.ordinal())) {
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyControlDt is not null AND s.surveyValidDt is null");
            } else if (stateId.equals(StateValues.VALIDATED.ordinal())) {
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyValidDt is not null AND s.surveyQualifDt is null");
            } else if (stateId.equals(StateValues.QUALIFIED.ordinal())) {
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyQualifDt is not null");
            }
        }

        // synchronization status
        if (synchronizationStatusId != null) {
            SynchronizationStatusValues synchronizationStatus = SynchronizationStatusValues.fromOrdinal(synchronizationStatusId);
            // Special case if DIRTY : add also READY_TO_SYNC (see mantis #26500)
            if (synchronizationStatus == SynchronizationStatusValues.DIRTY) {
                queryString.append(System.lineSeparator())
                        .append("AND s.synchronizationStatus IN ('")
                        .append(synchronizationStatus.getCode())
                        .append("','")
                        .append(SynchronizationStatusValues.READY_TO_SYNCHRONIZE.getCode())
                        .append("')");
            } else {
                queryString.append(System.lineSeparator())
                        .append("AND s.synchronizationStatus = '")
                        .append(synchronizationStatus.getCode())
                        .append('\'');
            }
        } else {
            // ignore DELETED rows by default
            queryString.append(System.lineSeparator())
                    .append("AND s.synchronizationStatus <> '")
                    .append(SynchronizationStatus.DELETED.getValue())
                    .append('\'');
        }

        if (startDate != null && endDate != null) {
            if (startDate == endDate) {
                // equals
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyDt = ");
                queryString.append(Daos.convertDateOnlyToSQLString(startDate));
            } else {
                // between
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyDt >=  ");
                queryString.append(Daos.convertDateOnlyToSQLString(startDate));
                queryString.append("AND s.surveyDt <=  ");
                queryString.append(Daos.convertDateOnlyToSQLString(endDate));
            }
        } else if (startDate != null) {
            // >= or >
            queryString.append(System.lineSeparator());
            queryString.append("AND s.surveyDt ");
            queryString.append((strictDate ? "> " : ">= "));
            queryString.append(Daos.convertDateOnlyToSQLString(startDate));
        } else if (endDate != null) {
            // <= or <
            queryString.append(System.lineSeparator());
            queryString.append("AND s.surveyDt ");
            queryString.append((strictDate ? "< " : "<= "));
            queryString.append(Daos.convertDateOnlyToSQLString(endDate));
        }

        // crete new query from query string
        Query q = getSession().createQuery(queryString.toString());
        setQueryParams(q, "surveysByCriteria",
                "campaignId", IntegerType.INSTANCE, campaignId,
                "locationId", IntegerType.INSTANCE, locationId,
                "name", StringType.INSTANCE, name,
                "comment", StringType.INSTANCE, comment);
        q.setParameterList("programCodes", programCodes);

        Iterator<Object[]> it = q.iterate();

        List<SurveyDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toSurveyDTO(Arrays.asList(source).iterator()));
        }

        // Load all geometries
        loadGeometries(result);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long countSurveysWithProgramAndLocations(String programCode, List<Integer> locationIds) {

        Query query;
        Set<Integer> locationIdsSet = locationIds != null ? locationIds.stream().filter(Objects::nonNull).collect(Collectors.toSet()) : null;
        if (CollectionUtils.isNotEmpty(locationIdsSet)) {
            query = createQuery("countSurveysByProgramAndLocations", "programCode", StringType.INSTANCE, programCode);
            query.setParameterList("locationIds", locationIdsSet);
        } else {
            query = createQuery("countSurveysByProgram", "programCode", StringType.INSTANCE, programCode);
        }
        return queryCount(query);
    }

    @Override
    public long countSurveysWithProgramLocationAndOutsideDates(String programCode, int appliedStrategyId, int locationId, Date startDate, Date endDate) {

        return queryCount("countSurveysByProgramLocationAndOutsideDates",
                "programCode", StringType.INSTANCE, programCode,
                "locationId", IntegerType.INSTANCE, locationId,
                "appliedStrategyId", IntegerType.INSTANCE, appliedStrategyId,
                "startDate", DateType.INSTANCE, startDate,
                "endDate", DateType.INSTANCE, endDate,
                "synchronizationStatusToIgnore", StringType.INSTANCE, SynchronizationStatus.DELETED.getValue()
        );
    }

    @Override
    public long countSurveysWithProgramLocationAndInsideDates(String programCode, int appliedStrategyId, int locationId, Date startDate, Date endDate) {

        return queryCount("countSurveysByProgramLocationAndInsideDates",
                "programCode", StringType.INSTANCE, programCode,
                "locationId", IntegerType.INSTANCE, locationId,
                "appliedStrategyId", IntegerType.INSTANCE, appliedStrategyId,
                "startDate", DateType.INSTANCE, startDate,
                "endDate", DateType.INSTANCE, endDate,
                "synchronizationStatusToIgnore", StringType.INSTANCE, SynchronizationStatus.DELETED.getValue()
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO getSurveyById(int surveyId, boolean dontExcludePmfm) {

        Object[] row = queryUnique("surveyById",
                "surveyId", IntegerType.INSTANCE, surveyId);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load survey with id = " + surveyId);
        }

        SurveyDTO survey = toSurveyDTO(Arrays.asList(row).iterator());

        // get observers
        survey.setObservers(getObservers(survey.getId()));
        survey.setObserversLoaded(true);

        // Get geometry
        loadGeometry(survey);

        // Get measurements
        loadMeasurements(survey);

        // Add sampling operations
        survey.setSamplingOperations(samplingOperationDao.getSamplingOperationsBySurveyId(survey.getId(), true));
        survey.setSamplingOperationsLoaded(true);

        // Add photos
        survey.setPhotos(photoDao.getPhotosBySurveyId(survey.getId()));
        survey.setPhotosLoaded(true);

        // Associate Sampling Operations in Photos
        if (!survey.isPhotosEmpty() && !survey.isSamplingOperationsEmpty()) {
            Map<Integer, SamplingOperationDTO> samplingOperationMap = DaliBeans.mapById(survey.getSamplingOperations());
            for (PhotoDTO photo : survey.getPhotos()) {
                if (photo.getSamplingOperation() != null && photo.getSamplingOperation().getId() != null) {
                    // photo.getSamplingOperation() has only an Id
                    SamplingOperationDTO samplingOperation = samplingOperationMap.get(photo.getSamplingOperation().getId());
                    if (samplingOperation == null) {
                        throw new DataRetrievalFailureException(
                                String.format("the sampling operation (id=%s) associated with the photo (id=%s) has not been loaded",
                                        photo.getSamplingOperation().getId(), photo.getId()));
                    }
                    photo.setSamplingOperation(samplingOperation);
                }
            }
        }

        return survey;
    }

    private void loadGeometries(List<SurveyDTO> surveys) {

        Map<Integer, String> geometriesById = new HashMap<>();
        Map<Integer, SurveyDTO> surveysById = new HashMap<>(surveys.size());
        // Collect surveys to load
        surveys.forEach(survey -> {
            if (survey.isActualPosition()) {
                surveysById.put(survey.getId(), survey);
            } else {
                // Reset positioning system
                survey.setPositioning(null);
            }
        });

        List<List<Integer>> partitions = ListUtils.partition(ImmutableList.copyOf(surveysById.keySet()), 1000);
        partitions.forEach(partition -> {
            Query query = createSQLQuery("surveyGeometries");
            query.setParameterList("surveyIds", partition);
            @SuppressWarnings("unchecked") List<Object[]> rows = query.list();
            if (CollectionUtils.isNotEmpty(rows)) {
                rows.stream()
                    .filter(Objects::nonNull)
                    .forEach(row -> geometriesById.put((Integer) row[0], (String) row[1]));
            }
        });
        // Fill each survey
        geometriesById.forEach((surveyId, geometry) -> surveysById.get(surveyId).setCoordinate(Geometries.getCoordinate(geometry)));
    }

    private void loadGeometry(SurveyDTO survey) {

        if (survey.isActualPosition()) {
            // Load geometry
            String geometry = queryUniqueTyped("surveyGeometry", "surveyId", IntegerType.INSTANCE, survey.getId());
            survey.setCoordinate(Geometries.getCoordinate(geometry));
        } else {
            // Reset positioning system
            survey.setPositioning(null);
        }
    }

    private void loadMeasurements(SurveyDTO survey, Integer... excludePmfmId) {

        List<MeasurementDTO> allMeasurements = measurementDao.getMeasurementsBySurveyId(survey.getId(), excludePmfmId);
        if (CollectionUtils.isNotEmpty(allMeasurements)) {

            // Iterate on all measurement to split them
            for (MeasurementDTO measurement : allMeasurements) {

                if (measurement.getIndividualId() != null) {
                    // If measurement has an individualId, split to other list
                    survey.addIndividualMeasurements(measurement);
                } else {
                    survey.addMeasurements(measurement);
                }
            }
        }

        survey.setMeasurementsLoaded(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonDTO> getObservers(int surveyId) {

        List<PersonDTO> observers = Lists.newArrayList();
        Iterator<Integer> it = queryIteratorTyped("quserIdsBySurveyId", "surveyId", IntegerType.INSTANCE, surveyId);
        while (it.hasNext()) {
            Integer quserId = it.next();
            if (quserId != null) {
                observers.add(quserDao.getUserById(quserId));
            }
        }
        return observers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO save(SurveyDTO bean) {
        Assert.notNull(bean);
        Assert.notNull(bean.getLocation());
        Assert.notNull(bean.getLocation().getId());
        Assert.notNull(bean.getRecorderDepartment());
        Assert.notNull(bean.getRecorderDepartment().getId());
        Assert.notNull(bean.getProgram());
        Assert.notNull(bean.getProgram().getCode());
        Assert.notNull(bean.getDate());

        Survey entity;
        boolean programChanged = false;
        if (bean.getId() == null) {
            entity = Survey.Factory.newInstance();
        } else {
            entity = get(bean.getId());
            if (entity == null) {
                throw new DataRetrievalFailureException("Could not retrieve survey with id=" + bean.getId());
            }

            // Check if program changed (if so, will update children)
            programChanged = !hasSameProgram(bean, entity);
        }

        beanToEntity(bean, entity);
        update(entity);

        // save coordinates
        boolean geometrySaved = saveGeometry(bean, entity);

        // if a geometry have been saved, calculate length
        if (geometrySaved && pmfmIdSurveyCalculatedLength > 0) {

            // Get pmfms from strategy
            Set<PmfmStrategyDTO> pmfmStrategies = strategyDao.getPmfmStrategiesByProgramCodeAndLocation(
                    bean.getProgram().getCode(), bean.getLocation().getId(), bean.getDate());

            // Find the pmfm for calculated length
            if (pmfmStrategies.stream().anyMatch(pmfmStrategy ->
                    pmfmStrategy.getPmfm().getId() == pmfmIdSurveyCalculatedLength
                            && pmfmStrategy.isSurvey() && !pmfmStrategy.isSampling() && !pmfmStrategy.isGrouping())) {

                // load all measurements first
                if (!bean.isMeasurementsLoaded())
                    loadMeasurements(bean);
                // calculate the length
                calculateLength(bean, pmfmIdSurveyCalculatedLength);
            }
        }

        // if trawl area configuration is set, try to calculate it (Mantis #42640)
        if (CollectionUtils.isNotEmpty(trawlAreaPmfmIdsTriplets)) {

            for (Integer[] triplet : trawlAreaPmfmIdsTriplets) {
                // should be a valid triplet
                int trawlAreaPmfmId = triplet[0];
                int lengthPmfmId = triplet[1];
                int trawlOpeningPmfmId = triplet[2];

                if (!isPmfmUnitValidForAreaCalculation(trawlAreaPmfmId, ImmutableList.of(lengthPmfmId, trawlOpeningPmfmId))) {
                    // calculation impossible (not compatible unit or not existing pmfm), try with next triplet
                    continue;
                }

                // check all 3 pmfms are in pmfm strategies
                Set<PmfmStrategyDTO> pmfmStrategies = strategyDao.getPmfmStrategiesByProgramCodeAndLocation(
                        bean.getProgram().getCode(), bean.getLocation().getId(), bean.getDate());
                List<PmfmStrategyDTO> surveyStrategies = pmfmStrategies.stream()
                        .filter(pmfmStrategy -> pmfmStrategy.isSurvey() && !pmfmStrategy.isSampling() && !pmfmStrategy.isGrouping()).collect(Collectors.toList());

                if (surveyStrategies.stream().anyMatch(pmfmStrategy -> pmfmStrategy.getPmfm().getId() == trawlAreaPmfmId)
                        && surveyStrategies.stream().anyMatch(pmfmStrategy -> pmfmStrategy.getPmfm().getId() == lengthPmfmId)
                        && surveyStrategies.stream().anyMatch(pmfmStrategy -> pmfmStrategy.getPmfm().getId() == trawlOpeningPmfmId)) {

                    // load all measurements first
                    if (!bean.isMeasurementsLoaded())
                        loadMeasurements(bean);

                    // calculate the area
                    calculateArea(bean, trawlAreaPmfmId, lengthPmfmId, trawlOpeningPmfmId);

                    // stop iteration
                    break;
                }
            }
        }

        // save measurements
        if (bean.isMeasurementsLoaded()) {
            List<MeasurementDTO> allMeasurements = Lists.newArrayList();
            allMeasurements.addAll(bean.getMeasurements());
            allMeasurements.addAll(bean.getIndividualMeasurements());
            measurementDao.saveMeasurementsBySurveyId(bean.getId(), allMeasurements/*, pmfmIdDepthValues*/);
        }

        // save sampling operations
        if (bean.isSamplingOperationsLoaded()) {
            samplingOperationDao.saveSamplingOperationsBySurveyId(bean.getId(), bean.getSamplingOperations());
        }

        // save photos
        if (bean.isPhotosLoaded()) {
            photoDao.savePhotosBySurveyId(bean.getId(), bean.getPhotos());
        }

        // Update program on children entities (mantis #37520)
        if (programChanged) {
            applySurveyProgramsToChildren(bean.getId());
        }

        getSession().flush();
        getSession().clear();

        // Update flags has_meas on survey and sampling operations (mantis #28257)
        updateHasMeasFlag(bean.getId());
        samplingOperationDao.updateHasMeasFlagBySurveyId(bean.getId());

        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(List<Integer> surveyIds) {
        if (CollectionUtils.isNotEmpty(surveyIds)) {
            for (Integer surveyId : surveyIds) {
                remove(surveyId);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(Integer surveyId) {

        if (surveyId != null) {
            // if needed, do some removes here before remove main entity

            photoDao.removeBySurveyId(surveyId);
            measurementDao.removeAllMeasurementsBySurveyId(surveyId);
            samplingOperationDao.removeBySurveyId(surveyId);

            super.remove(surveyId);
            getSession().flush();
            getSession().clear();
        } // else this is temporary unsaved survey, no need to remove from DB
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(int surveyId, String validationComment, Date validationDate, Integer validatorId, boolean readyToSynchronize) {
        Survey survey = get(surveyId);

        // validation date
        survey.setSurveyValidDt(validationDate);

        // qualification comment
        String oldSurveyValidCm = survey.getSurveyValidCm();
        if (StringUtils.isBlank(oldSurveyValidCm)) {
            // try get old comment from history (mantis #45569)
            oldSurveyValidCm = validationHistoryDao.getLastValidationComment(surveyId, ObjectTypeCode.SURVEY.getValue());
        }
        survey.setSurveyValidCm(validationComment);

        // synchronization status
        survey.setSynchronizationStatus(readyToSynchronize
                ? SynchronizationStatus.READY_TO_SYNCHRONIZE.getValue()
                : SynchronizationStatus.DIRTY.getValue());

        // scope (mantis #28257)
        survey.setSurveyScope(Daos.convertToString(true));

        // geometry validation date (mantis #28257)
        survey.setSurveyGeometryValidDt(validationDate);

        // insert a validation history line
        ValidationHistory history = ValidationHistory.Factory.newInstance(
                surveyId,
                objectTypeDao.get(ObjectTypeCode.SURVEY.getValue()),
                quserDao.get(validatorId)
        );
        history.setValidHistPreviousCm(oldSurveyValidCm);
        history.setValidHistOperationCm(validationComment);
        history.setUpdateDt(new Timestamp(validationDate.getTime()));

        validationHistoryDao.create(history);

        update(survey);
        getSession().flush();
        getSession().clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unvalidate(int surveyId, String unvalidationComment, Date unvalidationDate, Integer validatorId) {
        Survey survey = get(surveyId);

        // Reset Validation date
        survey.setSurveyValidDt(null);

        // Reset Qualification date & comment (mantis #0026463)
        boolean wasQualified = QualityFlagCode.NOT_QUALIFIED.getValue().equals(survey.getQualityFlag().getQualFlagCd());
        survey.setSurveyQualifDt(null);
        survey.setSurveyQualifCm(null);

        // Quality flag
        QualityFlag oldQualityFlag = survey.getQualityFlag();
        survey.setQualityFlag(getDefaultQualityFlag());

        String oldSurveyValidCm = survey.getSurveyValidCm();
        survey.setSurveyValidCm(null);

        // Synchronization status
        survey.setSynchronizationStatus(SynchronizationStatus.DIRTY.getValue());

        // Reset geometry validation date (mantis 28257)
        survey.setSurveyGeometryValidDt(null);

        // insert a validation history line
        ValidationHistory validationHistory = ValidationHistory.Factory.newInstance(
                surveyId,
                objectTypeDao.get(ObjectTypeCode.SURVEY.getValue()),
                quserDao.get(validatorId)
        );
        validationHistory.setValidHistPreviousCm(oldSurveyValidCm);
        validationHistory.setValidHistOperationCm(unvalidationComment);
        validationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
        validationHistoryDao.create(validationHistory);

        // find any qualified data
        wasQualified |= isQualified(surveyId);

        // insert qualification history line
        QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(surveyId),
                objectTypeDao.get(ObjectTypeCode.SURVEY.getValue()),
                quserDao.get(validatorId));
        qualificationHistory.setQualHistOperationCm(
            wasQualified
                ? t("dali.service.observation.qualificationHistory.unqualify.message")
                : t("dali.service.observation.qualificationHistory.unvalidate.message")
        );
        // save previous quality flag
        qualificationHistory.setQualityFlag(oldQualityFlag);
        qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));

        qualificationHistoryDao.create(qualificationHistory);

        update(survey);
    }

    @Override
    public void qualify(int surveyId, String qualificationComment, Date qualificationDate, Integer validatorId, String qualityFlagCode) {
        Survey survey = get(surveyId);

        // qualification date
        survey.setSurveyQualifDt(qualificationDate);

        // qualification comment
        String oldSurveyQualifCm = survey.getSurveyQualifCm();
        survey.setSurveyQualifCm(qualificationComment);

        // quality flag
        QualityFlag qualifiedQualityFlag = load(QualityFlagImpl.class, qualityFlagCode);
        survey.setQualityFlag(qualifiedQualityFlag);

        // synchronization status (Mantis #39454)
        survey.setSynchronizationStatus(SynchronizationStatus.READY_TO_SYNCHRONIZE.getValue());

        // scope (mantis #28257)
        survey.setSurveyScope(Daos.convertToString(true));

        // insert a validation history line
        QualificationHistory history = QualificationHistoryImpl.Factory.newInstance(String.valueOf(surveyId),
                objectTypeDao.get(ObjectTypeCode.SURVEY.getValue()),
                quserDao.get(validatorId));
        history.setQualHistPreviousCm(oldSurveyQualifCm);
        history.setQualHistOperationCm(qualificationComment);
        history.setUpdateDt(new Timestamp(qualificationDate.getTime()));
        history.setQualityFlag(qualifiedQualityFlag);

        qualificationHistoryDao.create(history);

        update(survey);
        getSession().flush();
        getSession().clear();
    }

    @Override
    public List<ValidationHistoryDTO> getValidationHistory(int surveyId) {
        List<ValidationHistoryDTO> result = new ArrayList<>();
        Iterator<Object[]> rows = queryIterator("validationHistoryBySurveyId",
                "objectTypeCd", StringType.INSTANCE, ObjectTypeCode.SURVEY.getValue(),
                "surveyId", IntegerType.INSTANCE, surveyId);
        while (rows.hasNext()) {
            result.add(toValidationHistoryDTO(Arrays.asList(rows.next()).iterator()));
        }
        return result;
    }

    @Override
    public List<QualificationHistoryDTO> getQualificationHistory(int surveyId) {
        List<QualificationHistoryDTO> result = new ArrayList<>();
        Iterator<Object[]> rows = queryIterator("qualificationHistoryBySurveyId",
                "objectTypeCd", StringType.INSTANCE, ObjectTypeCode.SURVEY.getValue(),
                "surveyId", StringType.INSTANCE, String.valueOf(surveyId));
        while (rows.hasNext()) {
            result.add(toQualificationHistoryDTO(Arrays.asList(rows.next()).iterator()));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateSurveysControlDate(Collection<Integer> surveyIds, Date controlDate) {
        createQuery("updateSurveyControlDate")
                .setParameterList("surveyIds", surveyIds)
                .setParameter("controlDate", controlDate).executeUpdate();
    }

    @Override
    public boolean isQualified(int surveyId) {

        return
            // count qualified measurements
            !measurementDao.getQualifiedMeasurementBySurveyId(surveyId).isEmpty()
            || !measurementDao.getQualifiedTaxonMeasurementBySurveyId(surveyId).isEmpty()
            // count qualified photos
            || !photoDao.getQualifiedPhotoBySurveyId(surveyId).isEmpty()
            // count qualified sampling operations
            || !samplingOperationDao.getQualifiedSamplingOperationBySurveyId(surveyId, true).isEmpty();
    }

    // ------------------------------------------------
    // ---------------PRIVATE METHODS------------------
    // ------------------------------------------------
    private SurveyDTO toSurveyDTO(Iterator<Object> source) {
        SurveyDTO result = DaliBeanFactory.newSurveyDTO();
        result.setId((Integer) source.next());

        // location
        Integer locationId = (Integer) source.next();
        result.setLocation(monitoringLocationDao.getLocationById(locationId));

        // department
        Integer departmentId = (Integer) source.next();
        result.setRecorderDepartment(departmentDao.getDepartmentById(departmentId));
        result.setInheritedRecorderDepartmentIds(departmentDao.getInheritedRecorderDepartmentIds(departmentId));

        // Apply DB timezone to survey date (Mantis Reefdb #41597)
        result.setDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), config.getDbTimezone()));

        result.setTime(Daos.convertToInteger((Number) source.next()));
        result.setName((String) source.next());
        result.setComment((String) source.next());
        result.setControlDate(Daos.convertToDate(source.next()));
        result.setValidationDate(Daos.convertToDate(source.next()));
        result.setValidationComment((String) source.next());
        result.setQualificationDate(Daos.convertToDate(source.next()));
        result.setQualificationComment((String) source.next());
        result.setUpdateDate(Daos.convertToDate(source.next()));
        result.setBottomDepth(Daos.convertToDouble((Float) source.next()));

        // program
        String programCode = (String) source.next();
        if (StringUtils.isNotBlank(programCode)) {
            result.setProgram(programDao.getProgramByCode(programCode));
        }

        // campaign
        CampaignDTO campaign = DaliBeanFactory.newCampaignDTO();
        campaign.setId((Integer) source.next());
        campaign.setName((String) source.next());
        campaign.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), config.getDbTimezone()));
        campaign.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), config.getDbTimezone()));
        if (campaign.getId() != null) {
            result.setCampaign(campaign);
        }

        // get positioning system id
        Integer positioningSystemId = (Integer) source.next();
        if (positioningSystemId != null) {
            result.setPositioning(referentialDao.getPositioningSystemById(positioningSystemId));
        }
        result.setPositioningComment((String) source.next());

        // occasion
        OccasionDTO occasion = DaliBeanFactory.newOccasionDTO();
        occasion.setId((Integer) source.next());
        occasion.setName((String) source.next());
        if (occasion.getId() != null) {
            result.setOccasion(occasion);
        }

        // is actual position ?
        result.setActualPosition(Daos.safeConvertToBoolean(source.next(), false));

        // synchronization status
        result.setSynchronizationStatus(SynchronizationStatusValues.toSynchronizationStatusDTO((String) source.next()));

        // Quality flag
        result.setQualityLevel(referentialDao.getQualityFlagByCode((String) source.next()));

        return result;
    }

    private ValidationHistoryDTO toValidationHistoryDTO(Iterator<Object> source) {
        ValidationHistoryDTO result = DaliBeanFactory.newValidationHistoryDTO();
        result.setDate(Daos.convertToDate(source.next()));
        result.setComment((String) source.next());
        result.setRecorderPerson(quserDao.getUserById((Integer) source.next()));
        return result;
    }

    private QualificationHistoryDTO toQualificationHistoryDTO(Iterator<Object> source) {
        QualificationHistoryDTO result = DaliBeanFactory.newQualificationHistoryDTO();
        result.setDate(Daos.convertToDate(source.next()));
        result.setComment((String) source.next());
        String qualityCode = (String) source.next();
        if (qualityCode != null) result.setQualityLevel(referentialDao.getQualityFlagByCode(qualityCode));
        result.setRecorderPerson(quserDao.getUserById((Integer) source.next()));
        return result;
    }

    private void beanToEntity(SurveyDTO bean, Survey entity) {

        // mandatory attributes

        // quality flag
        if (entity.getQualityFlag() == null) {
            // load a default quality flag
            entity.setQualityFlag(getDefaultQualityFlag());
        }

        // monitoring location
        if (entity.getMonitoringLocation() == null
                || entity.getMonitoringLocation().getMonLocId() == null
                || !entity.getMonitoringLocation().getMonLocId().equals(bean.getLocation().getId())) {
            entity.setMonitoringLocation(load(MonitoringLocationImpl.class, bean.getLocation().getId()));
        }

        // UT format from monitoring location
        /* TODO update UT format depending on DST of location and date
         Get with a shape file containing the zone ids , ans query it by lat/long of location
         with tz_world.shp , query the TZID  

         And get the information with :
         ZoneId.of( "America/Montreal" )
            .getRules()
            .isDaylightSavings(
                Instant.now() =replace by the survey date
            )
        */
        entity.setSurveyUtFormat(entity.getMonitoringLocation().getMonLocUtFormat());

        // Recorder Department (Mantis #42614 Only if REC_DEP_ID is null)
        if (entity.getRecorderDepartment() == null) {
            entity.setRecorderDepartment(load(DepartmentImpl.class, bean.getRecorderDepartment().getId()));
        }

        // date
        Date beanDate = Dates.convertToDate(bean.getDate(), config.getDbTimezone());
        if (entity.getSurveyDt() == null || !entity.getSurveyDt().equals(beanDate)) {
            entity.setSurveyDt(beanDate);
        }

        // program
        if (bean.getProgram() != null) {
            // create a list to trace not updated items, to be able to removeMeasurementsByIds them later
            Set<Program> remainingPrograms = Sets.newHashSet(entity.getPrograms());
            // load item to store
            Program program = load(ProgramImpl.class, bean.getProgram().getCode());
            if (!entity.getPrograms().contains(program)) {
                entity.addPrograms(program);
            }
            remainingPrograms.remove(program);
            // removeMeasurementsByIds unchanged item
            entity.getPrograms().removeAll(remainingPrograms);
        } else {
            entity.getPrograms().clear();
        }

        // synchronization status: force to DIRTY
        entity.setSynchronizationStatus(SynchronizationStatus.DIRTY.getValue());
        // update also on source bean (Mantis #40769)
        bean.setSynchronizationStatus(SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO());

        // first save if new entity
        if (entity.getSurveyId() == null) {
            create(entity);
            bean.setId(entity.getSurveyId());
        }

        // optional attributes
        entity.setSurveyTime(bean.getTime());
        entity.setSurveyLb(bean.getName());
        entity.setSurveyCm(bean.getComment());
        entity.setSurveyControlDt(bean.getControlDate());

        // Bottom depth and unit
        if (bean.getBottomDepth() == null) {
            entity.setSurveyBottomDepth(null);
            // Remove unit
            entity.setBottomDepthUnit(null);
        } else {
            // TODO fix mantis 28678 (conversion error : use double instead ? e.g. 1.4 -> 1.39999)
            entity.setSurveyBottomDepth(bean.getBottomDepth().floatValue());

            // Set unit as meter (see mantis #28678)
            entity.setBottomDepthUnit(load(UnitImpl.class, unitIdMeter));
        }

        // campaign
        if (bean.getCampaign() != null) {
            entity.setCampaign(load(CampaignImpl.class, bean.getCampaign().getId()));
        } else {
            entity.setCampaign(null);
        }

        // positioning system
        if (bean.getPositioning() != null) {
            entity.setPositionningSystem(load(PositionningSystemImpl.class, bean.getPositioning().getId()));
        } else {
            entity.setPositionningSystem(null);
        }
        entity.setSurveyPositionCm(bean.getPositioningComment());

        // occasion
        if (bean.getOccasion() != null) {
            entity.setOccasion(load(OccasionImpl.class, bean.getOccasion().getId()));
        } else {
            entity.setOccasion(null);
        }

        // observers
        if (bean.isObserversLoaded()) {
            if (!bean.isObserversEmpty()) {

                Map<Integer, Quser> remainingUsers = DaliBeans.mapByProperty(entity.getQusers(), "quserId");

                for (PersonDTO observer : bean.getObservers()) {
                    if (remainingUsers.remove(observer.getId()) == null) {
                        // add user
                        Quser quser = load(QuserImpl.class, observer.getId());
                        entity.addQusers(quser);
                    }
                }

                if (!remainingUsers.isEmpty()) {
                    entity.getQusers().removeAll(remainingUsers.values());
                }

            } else if (entity.getQusers() != null) {
                entity.getQusers().clear();
            }
        }

        // Reset control date (Mantis #59449)
        if (bean.getControlDate() == null) {
            entity.setSurveyControlDt(null);
        }
    }

    private boolean hasSameProgram(SurveyDTO bean, Survey entity) {

        String beanProgCd = bean.getProgram() != null ? bean.getProgram().getCode() : null;

        List<String> entityProgCds = CollectionUtils.isNotEmpty(entity.getPrograms())
                ? DaliBeans.collectProperties(entity.getPrograms(), "progCd")
                : null;

        return (beanProgCd == null && entityProgCds == null)
                || (CollectionUtils.size(entityProgCds) == 1 && Objects.equals(beanProgCd, IterableUtils.get(entityProgCds, 0)));
    }

    private boolean saveGeometry(SurveyDTO bean, Survey entity) {

        boolean geometrySaved = false;

        // If user has filled coordinates
        if (Geometries.isValid(bean.getCoordinate())) {

            if (Geometries.isPoint(bean.getCoordinate())) {

                // point
                if (CollectionUtils.size(entity.getSurveyPoints()) == 1) {

                    SurveyPoint surveyPointToUpdate = CollectionUtils.extractSingleton(entity.getSurveyPoints());
                    if (!Geometries.equals(bean.getCoordinate(), surveyPointToUpdate.getSurveyPosition())) {
                        surveyPointToUpdate.setSurveyPosition(Geometries.getPosition(bean.getCoordinate()));
                        getSession().update(surveyPointToUpdate);
                    }

                } else {
                    // create a survey point
                    SurveyPoint surveyPoint = SurveyPoint.Factory.newInstance();
                    surveyPoint.setSurvey(entity);
                    surveyPoint.setSurveyPosition(Geometries.getPosition(bean.getCoordinate()));
                    getSession().save(surveyPoint);
                    entity.getSurveyPoints().clear();
                    entity.addSurveyPoints(surveyPoint);
                }

                // Clear Lines
                entity.getSurveyLines().clear();

            }

            // Line geometry
            else {

                // Survey has an existing line: update it
                if (CollectionUtils.size(entity.getSurveyLines()) == 1) {

                    SurveyLine surveyLine = CollectionUtils.extractSingleton(entity.getSurveyLines());
                    if (!Geometries.equals(bean.getCoordinate(), surveyLine.getSurveyPosition())) {
                        surveyLine.setSurveyPosition(Geometries.getPosition(bean.getCoordinate()));
                        getSession().update(surveyLine);
                    }
                }

                // Survey has NO line: create new
                else {
                    // create a survey line
                    SurveyLine surveyLine = SurveyLine.Factory.newInstance();
                    surveyLine.setSurvey(entity);
                    surveyLine.setSurveyPosition(Geometries.getPosition(bean.getCoordinate()));
                    getSession().save(surveyLine);
                    entity.getSurveyLines().clear();
                    entity.addSurveyLines(surveyLine);
                }

                // Clear Points
                entity.getSurveyPoints().clear();
            }

            // Update flag to known is coordinate are filled by user or not (mantis #28257)
            entity.setSurveyActualPosition(Daos.convertToString(true));

            // Clear unused coordinates (Areas)
            entity.getSurveyAreas().clear();

            geometrySaved = true;
        }

        // If a location has been define
        else if (bean.getLocation() != null && bean.getLocation().getId() != null) {

            // Copy geom from this location (mantis #28688)
            geometrySaved = saveGeometryAndPositioningFromLocation(entity, bean.getLocation().getId());
        }

        // No geometry could be set: reset all geometry data
        else {
            removeGeometry(entity);
        }

        // Update survey
        // This is need because geometries list could have change (e.g. entity.getSurveyAreas().clear())
        update(entity);

        return geometrySaved;
    }

    private boolean saveGeometryAndPositioningFromLocation(Survey entity, int monLocId) {

        boolean geometrySaved = false;

        // Retrieve the location entity
        MonitoringLocation location = load(MonitoringLocationImpl.class, monLocId);

        // Point geometry
        if (CollectionUtils.size(location.getMonLocPoints()) == 1) {
            MonLocPoint locationPoint = CollectionUtils.extractSingleton(location.getMonLocPoints());

            // Survey has already a point, so update it
            if (CollectionUtils.size(entity.getSurveyPoints()) == 1) {
                SurveyPoint surveyPoint = CollectionUtils.extractSingleton(entity.getSurveyPoints());
                if (!Objects.equals(locationPoint.getMonLocPosition(), surveyPoint.getSurveyPosition())) {
                    surveyPoint.setSurveyPosition(locationPoint.getMonLocPosition());
                    getSession().update(surveyPoint);
                    geometrySaved = true;
                }
            }

            // No existing point: create new
            else {
                SurveyPoint surveyPoint = SurveyPoint.Factory.newInstance();
                surveyPoint.setSurvey(entity);
                surveyPoint.setSurveyPosition(locationPoint.getMonLocPosition());
                getSession().save(surveyPoint);
                entity.getSurveyPoints().clear();
                entity.addSurveyPoints(surveyPoint);
                geometrySaved = true;
            }

            // Clean unused
            entity.getSurveyLines().clear();
            entity.getSurveyAreas().clear();
        }

        // Line geometry
        else if (CollectionUtils.size(location.getMonLocLines()) == 1) {
            MonLocLine locationLine = CollectionUtils.extractSingleton(location.getMonLocLines());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSurveyLines()) == 1) {
                SurveyLine surveyLine = CollectionUtils.extractSingleton(entity.getSurveyLines());
                if (!Objects.equals(locationLine.getMonLocPosition(), surveyLine.getSurveyPosition())) {
                    surveyLine.setSurveyPosition(locationLine.getMonLocPosition());
                    getSession().update(surveyLine);
                    geometrySaved = true;
                }
            }

            // No existing area: create new
            else {
                SurveyLine surveyLine = SurveyLine.Factory.newInstance();
                surveyLine.setSurvey(entity);
                surveyLine.setSurveyPosition(locationLine.getMonLocPosition());
                getSession().save(surveyLine);
                entity.getSurveyLines().clear();
                entity.addSurveyLines(surveyLine);
                geometrySaved = true;
            }

            // Clean unused
            entity.getSurveyPoints().clear();
            entity.getSurveyAreas().clear();
        }

        // Area geometry
        else if (CollectionUtils.size(location.getMonLocAreas()) == 1) {
            MonLocArea locationArea = CollectionUtils.extractSingleton(location.getMonLocAreas());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSurveyAreas()) == 1) {
                SurveyArea surveyArea = CollectionUtils.extractSingleton(entity.getSurveyAreas());
                if (!Objects.equals(locationArea.getMonLocPosition(), surveyArea.getSurveyPosition())) {
                    surveyArea.setSurveyPosition(locationArea.getMonLocPosition());
                    getSession().update(surveyArea);
                    geometrySaved = true;
                }
            }

            // No existing area: create new
            else {
                SurveyArea surveyArea = SurveyArea.Factory.newInstance();
                surveyArea.setSurvey(entity);
                surveyArea.setSurveyPosition(locationArea.getMonLocPosition());
                getSession().save(surveyArea);
                entity.getSurveyAreas().clear();
                entity.addSurveyAreas(surveyArea);
                geometrySaved = true;
            }

            // Clean unused
            entity.getSurveyPoints().clear();
            entity.getSurveyLines().clear();
        }

        // Update flag to known is coordinate is not filled by user or not (mantis #28257)
        entity.setSurveyActualPosition(Daos.convertToString(false));

        // Apply positioning system from location (see mantis #28706)
        entity.setPositionningSystem(location.getPositionningSystem());

        return geometrySaved;
    }

    private void removeGeometry(Survey entity) {

        entity.getSurveyPoints().clear();
        entity.getSurveyLines().clear();
        entity.getSurveyAreas().clear();

        // Update flag to known is coordinate is not filled by user or not (mantis #28257)
        entity.setSurveyActualPosition(Daos.convertToString(false));
    }

    private void calculateLength(SurveyDTO survey, int calculatedLengthPmfmId) {

        PmfmDTO pmfm = pmfmDao.getPmfmById(calculatedLengthPmfmId);
        Assert.notNull(pmfm);
        BigDecimal length = null;

        Double lengthInMeter = Geometries.getDistanceInMeter(survey.getCoordinate());
        if (lengthInMeter != null) {
            // Convert to the correct unit
            length = DaliBeans.convertLengthValue(
                    BigDecimal.valueOf(lengthInMeter),
                    UnitId.METER.getValue(),
                    pmfm.getUnit().getId());
        }

        // get the existing measurement
        MeasurementDTO measurement = DaliBeans.getMeasurementByPmfmId(survey, calculatedLengthPmfmId);

        if (length != null) {
            if (measurement == null) {
                // create new
                measurement = DaliBeanFactory.newMeasurementDTO();
                measurement.setPmfm(pmfm);
                survey.addMeasurements(measurement);
            }

            // trick the scale to remove unwanted decimal for some unit
            if (Objects.equals(pmfm.getUnit().getId(), UnitId.METER.getValue())
                    || Objects.equals(pmfm.getUnit().getId(), UnitId.CENTIMENTER.getValue())
                    || Objects.equals(pmfm.getUnit().getId(), UnitId.MILLIMETER.getValue())
                    || Objects.equals(pmfm.getUnit().getId(), UnitId.MICROMETER.getValue())) {

                // default precision is the meter
                length = length.setScale(0, BigDecimal.ROUND_HALF_EVEN);
                measurement.setDigitNb(0);

            } else {
                // if other unit used, set to 3 decimals (ie KILOMETER is rounded to meter)
                length = length.setScale(3, BigDecimal.ROUND_HALF_EVEN);
                measurement.setDigitNb(3);
            }

            // set value and the caller method do the save
            measurement.setNumericalValue(length);
            measurement.setComment(t("dali.service.data.survey.calculatedLength.comment"));

            // Set default analyst (Mantis #47285) Only if empty (Mantis #64133)
            if (measurement.getAnalyst() == null)
                measurement.setAnalyst(programStrategyService.getAnalysisDepartmentOfAppliedStrategyBySurvey(survey));

        } else if (measurement != null) {

            // remove measurement if no valid length
            survey.removeMeasurements(measurement);
        }
    }

    private void calculateArea(SurveyDTO survey, int areaPmfmId, int lengthPmfmId, int openingPmfmId) {

        MeasurementDTO lengthMeasurement = DaliBeans.getMeasurementByPmfmId(survey, lengthPmfmId);
        MeasurementDTO openingMeasurement = DaliBeans.getMeasurementByPmfmId(survey, openingPmfmId);

        BigDecimal area = null;
        if (!DaliBeans.isNumericalMeasurementEmpty(lengthMeasurement) && !DaliBeans.isNumericalMeasurementEmpty(openingMeasurement)) {
            // calculate
            area = lengthMeasurement.getNumericalValue().multiply(openingMeasurement.getNumericalValue());
        }

        // get the existing measurement
        MeasurementDTO measurement = DaliBeans.getMeasurementByPmfmId(survey, areaPmfmId);

        if (area != null) {

            if (measurement == null) {
                // create new
                measurement = DaliBeanFactory.newMeasurementDTO();
                measurement.setPmfm(pmfmDao.getPmfmById(areaPmfmId));
                survey.addMeasurements(measurement);
            }

            // default precision is the meter
            area = area.setScale(0, BigDecimal.ROUND_HALF_EVEN);
            measurement.setDigitNb(0);

            // set value and the caller method do the save
            measurement.setNumericalValue(area);
            measurement.setComment(t("dali.service.data.survey.calculatedArea.comment"));

            // Set default analyst (Mantis #47285)
            measurement.setAnalyst(programStrategyService.getAnalysisDepartmentOfAppliedStrategyBySurvey(survey));

        } else if (measurement != null) {

            // remove measurement if no valid area
            survey.removeMeasurements(measurement);
        }
    }

    private boolean isPmfmUnitValidForAreaCalculation(Integer areaPmfmId, Collection<Integer> lengthPmfmIds) {

        Assert.notNull(areaPmfmId);
        Assert.notEmpty(lengthPmfmIds);

        List<PmfmDTO> pmfms = pmfmDao.getPmfmsByIds(ImmutableList.of(areaPmfmId));
        if (pmfms.isEmpty()) {
            // area pmfm not found, return silently
            return false;
        }

        Integer areaUnitId = pmfms.get(0).getUnit().getId();
        Assert.notNull(areaUnitId);

        // get compatibility map from config option
        Map<Integer, Integer> compatibleUnits = config.getCompatibleLengthUnitIdByAreaUnitId();

        if (!compatibleUnits.containsKey(areaUnitId)) {
            return false; // this area unit id is not in compatibility map
        }

        pmfms = pmfmDao.getPmfmsByIds(lengthPmfmIds);
        if (pmfms.size() != lengthPmfmIds.size()) {
            return false; // not all length pmfm exists in PMFM referential
        }

        // get unique length unit
        if (pmfms.stream().map(pmfmDTO -> pmfmDTO.getUnit().getId()).distinct().count() > 1) {
            return false; // not all length unit are equals
        }

        Integer lengthUnitId = pmfms.get(0).getUnit().getId();
        Assert.notNull(lengthUnitId);

        // return true if the length unit is is compatible with the area
        return lengthUnitId.equals(compatibleUnits.get(areaUnitId));

    }

    /**
     * return the default quality flag
     */
    private QualityFlag getDefaultQualityFlag() {
        return load(QualityFlagImpl.class, QualityFlagCode.NOT_QUALIFIED.getValue()); // = non qualifié
    }

}
