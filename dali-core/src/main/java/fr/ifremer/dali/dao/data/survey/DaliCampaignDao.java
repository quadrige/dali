package fr.ifremer.dali.dao.data.survey;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dao.administration.program.DaliProgramDao;
import fr.ifremer.dali.dao.referential.monitoringLocation.DaliMonitoringLocationDao;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.data.survey.OccasionDTO;
import fr.ifremer.quadrige3.core.dao.data.survey.CampaignDao;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.Date;
import java.util.List;

/**
 * Campaigns DAO
 *
 * @author Ludovic
 */
public interface DaliCampaignDao extends CampaignDao {

    String ALL_CAMPAIGNS_CACHE = "all_campaigns";

    /**
     * Return the list of all campaigns
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_CAMPAIGNS_CACHE)
    List<CampaignDTO> getAllCampaigns();

    List<CampaignDTO> getCampaignsByIds(List<Integer> campaignIds);

    List<CampaignDTO> getCampaignsByCriteria(String name, Date startDate1, Date startDate2, boolean strictStartDate, Date endDate1, Date endDate2, boolean strictEndDate, boolean canEndDateBeNull);

    List<CampaignDTO> getCampaignsByName(String name);

    @CacheEvict(value = {
            ALL_CAMPAIGNS_CACHE,
            DaliProgramDao.PROGRAMS_BY_CAMPAIGN_ID_CACHE,
            DaliMonitoringLocationDao.LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE
    }, allEntries = true)
    void saveCampaign(CampaignDTO campaign);

    @CacheEvict(value = {
            ALL_CAMPAIGNS_CACHE,
            DaliProgramDao.PROGRAMS_BY_CAMPAIGN_ID_CACHE,
            DaliMonitoringLocationDao.LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE
    }, allEntries = true)
    @Override
    void remove(Integer campaignId);

    /**
     * Return the list of all occasions
     *
     * @return a {@link java.util.List} object.
     */
    List<OccasionDTO> getAllOccasions();

}
