package fr.ifremer.dali.dao.referential.taxon;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.TaxonGroupTypeCode;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonGroupDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>DaliTaxonGroupDaoImpl class.</p>
 *
 */
@Repository("daliTaxonGroupDao")
public class DaliTaxonGroupDaoImpl extends TaxonGroupDaoImpl implements DaliTaxonGroupDao {

    @Resource
    protected CacheService cacheService;

    @Resource(name = "daliTaxonNameDao")
    private DaliTaxonNameDao taxonNameDao;
    @Resource(name = "daliTaxonGroupDao")
    private DaliTaxonGroupDao loopbackTaxonGroupDao;
    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * <p>Constructor for DaliTaxonGroupDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliTaxonGroupDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonGroupDTO> getAllTaxonGroups() {

        // get all needed cache to update
        Cache cacheById = cacheService.getCache(TAXON_GROUP_BY_ID_CACHE);
        Cache allTaxonNamesCache = cacheService.getCache(DaliTaxonNameDao.ALL_TAXON_NAMES_CACHE);
        Cache taxonCache = cacheService.getCache(DaliTaxonNameDao.TAXON_NAME_BY_ID_CACHE);
        Cache referenceTaxonCache = cacheService.getCache(DaliTaxonNameDao.TAXON_NAME_BY_REFERENCE_ID_CACHE);
        Cache taxonNameByTaxonGroupIdCache = cacheService.getCache(DaliTaxonNameDao.TAXON_NAME_BY_TAXON_GROUP_ID_CACHE);

        // get all taxon names
        List<TaxonDTO> allTaxonNames = taxonNameDao.getAllTaxonNames();
        // get all taxon names map
        LocalDate taxonRefDate = LocalDate.now();
        Multimap<Integer, TaxonDTO> taxonMap = taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(taxonRefDate);

        Query query = createQuery("allTaxonGroup",
                "taxonGroupTypeCode", StringType.INSTANCE, TaxonGroupTypeCode.IDENTIFICATION.getValue());
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query,
                ImmutableList.of(StatusCode.ENABLE.getValue(), StatusCode.TEMPORARY.getValue()));

        // TODO : control TAXON_GROUP_EXCLUS, TAXON_GROUP_UPDATE ?
        List<TaxonGroupDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonGroupDTO taxonGroup = toTaxonGroupDTO(Arrays.asList(source).iterator());

            // Clone this taxon group and remove parent & children
            TaxonGroupDTO lightTaxonGroup = getLightTaxonGroup(taxonGroup);

            // add this taxon group to all taxon
            Collection<TaxonDTO> taxons = taxonMap.get(taxonGroup.getId());
            for (TaxonDTO taxon : taxons) {

                // add this taxon group
                if (!taxon.containsTaxonGroups(lightTaxonGroup)) {
                    taxon.addTaxonGroups(lightTaxonGroup);
                }

                // need to update taxonNameById cache
                {
                    TaxonDTO cachedTaxon = taxonCache.get(taxon.getId(), TaxonDTO.class);
                    if (cachedTaxon != null && !cachedTaxon.containsTaxonGroups(lightTaxonGroup)) {
                        cachedTaxon.addTaxonGroups(lightTaxonGroup);
                        taxonCache.evict(taxon.getId());
                        taxonCache.put(taxon.getId(), cachedTaxon);
                    }
                }

                // need to update reference taxon cache
                {
                    TaxonDTO cachedReferenceTaxon = referenceTaxonCache.get(taxon.getReferenceTaxonId(), TaxonDTO.class);
                    if (cachedReferenceTaxon != null && !cachedReferenceTaxon.containsTaxonGroups(lightTaxonGroup)) {
                        cachedReferenceTaxon.addTaxonGroups(lightTaxonGroup);
                        referenceTaxonCache.evict(taxon.getReferenceTaxonId());
                        referenceTaxonCache.put(taxon.getReferenceTaxonId(), cachedReferenceTaxon);
                    }
                }

                // need to update allTaxonNames cache also
                {
                    TaxonDTO cachedTaxon = DaliBeans.findById(allTaxonNames, taxon.getId());
                    if (!cachedTaxon.containsTaxonGroups(lightTaxonGroup)) {
                        cachedTaxon.addTaxonGroups(lightTaxonGroup);
                    }
                }

                // add taxon parent and referent
                taxonNameDao.fillParentAndReferent(taxon);
            }

            // add taxon names
            taxonGroup.addAllTaxons(taxons);

            // add to cache by id
            cacheById.put(taxonGroup.getId(), taxonGroup);

            result.add(taxonGroup);
        }

        // update taxonNameByTaxonGroupId cache
        taxonNameByTaxonGroupIdCache.clear();
        taxonNameByTaxonGroupIdCache.put(taxonRefDate, taxonMap);

        // update allTaxonNames cache
        allTaxonNamesCache.put(SimpleKey.EMPTY, allTaxonNames);

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonGroupDTO getTaxonGroupById(int taxonGroupId) {

        // TODO : control TAXON_GROUP_EXCLUS, TAXON_GROUP_UPDATE ?
        Object[] source = queryUnique("taxonGroupById", "taxonGroupId", IntegerType.INSTANCE, taxonGroupId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load taxon group with id = " + taxonGroupId);
        }

        TaxonGroupDTO taxonGroup = toTaxonGroupDTO(Arrays.asList(source).iterator());

        // Clone this taxon group and remove parent & children
        TaxonGroupDTO lightTaxonGroup = getLightTaxonGroup(taxonGroup);

        Collection<TaxonDTO> taxons = taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now()).get(taxonGroupId);
        for (TaxonDTO taxon : taxons) {
            if (!taxon.containsTaxonGroups(lightTaxonGroup)) {
                taxon.addTaxonGroups(lightTaxonGroup);
            }
        }

        taxonGroup.addAllTaxons(taxons);
        return taxonGroup;
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonGroupDTO> getTaxonGroupsByIds(List<Integer> taxonGroupIds) {

        if (CollectionUtils.isEmpty(taxonGroupIds)) return new ArrayList<>();

        return loopbackTaxonGroupDao.getAllTaxonGroups().stream().filter(taxonGroup -> taxonGroupIds.contains(taxonGroup.getId())).collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonGroupDTO> findTaxonGroups(Integer parentTaxonGroupId, String label, String name, boolean isStrictName, List<String> statusCodes) {
        // get all taxon names map
        Multimap<Integer, TaxonDTO> taxonMap = taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now());

        Query query = createQuery("taxonGroupsByCriteria",
                "parentTaxonGroupId", IntegerType.INSTANCE, parentTaxonGroupId,
                "label", StringType.INSTANCE, label,
                "name", StringType.INSTANCE, isStrictName ? null : name,
                "strictName", StringType.INSTANCE, isStrictName ? name : null,
                "taxonGroupTypeCode", StringType.INSTANCE, TaxonGroupTypeCode.IDENTIFICATION.getValue());

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<TaxonGroupDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonGroupDTO taxonGroup = toTaxonGroupDTO(Arrays.asList(source).iterator());
            TaxonGroupDTO lightTaxonGroup = getLightTaxonGroup(taxonGroup);

            // add this taxon group to all taxon
            Collection<TaxonDTO> taxons = taxonMap.get(taxonGroup.getId());
            for (TaxonDTO taxon : taxons) {
                if (!taxon.containsTaxonGroups(lightTaxonGroup)) {
                    taxon.addTaxonGroups(lightTaxonGroup);
                }
            }

            taxonGroup.addAllTaxons(taxons);
            result.add(taxonGroup);
        }

        // Mantis #0027042: when parentTaxonGroupId is set, do a recursive search to populate sub groups
        if (CollectionUtils.isNotEmpty(result) && parentTaxonGroupId != null) {
            Set<TaxonGroupDTO> subTaxonGroups = Sets.newHashSet();
            for (TaxonGroupDTO taxonGroup : result) {
                // populate sub groups
                subTaxonGroups.addAll(findTaxonGroups(taxonGroup.getId(), label, name, isStrictName, statusCodes));
            }
            result.addAll(subTaxonGroups);
        }

        return ImmutableList.copyOf(result);
    }

    private TaxonGroupDTO toTaxonGroupDTO(Iterator<Object> source) {
        TaxonGroupDTO result = DaliBeanFactory.newTaxonGroupDTO();
        result.setId((Integer) source.next());
        result.setLabel((String) source.next());
        result.setName((String) source.next());
        result.setComment((String) source.next());
        result.setExclusive(Daos.safeConvertToBoolean(source.next()));
        result.setUpdate(Daos.safeConvertToBoolean(source.next()));
        result.setType((String) source.next());

        // parent
        Integer parentId = (Integer) source.next();
        String parentLabel = (String) source.next();
        String parentName = (String) source.next();

        if (parentId != null) {
            TaxonGroupDTO parent = DaliBeanFactory.newTaxonGroupDTO();
            parent.setId(parentId);
            parent.setLabel(parentLabel);
            parent.setName(parentName);

            result.setParentTaxonGroup(parent);
        }

        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        return result;
    }

    private TaxonGroupDTO getLightTaxonGroup(TaxonGroupDTO taxonGroup) {
        TaxonGroupDTO result = DaliBeans.clone(taxonGroup);
        result.setTaxons(null);
        result.setParentTaxonGroup(null);
        return result;
    }

}
