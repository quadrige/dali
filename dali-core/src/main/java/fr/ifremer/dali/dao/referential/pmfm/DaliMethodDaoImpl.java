package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.MethodDTO;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.MethodDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Method DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("daliMethodDao")
public class DaliMethodDaoImpl extends MethodDaoImpl implements DaliMethodDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * <p>Constructor for DaliMethodDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliMethodDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<MethodDTO> getAllMethods(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(METHOD_BY_ID_CACHE);
        List<MethodDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allMethods"), statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            MethodDTO method = toMethodDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            result.add(method);

            cacheById.put(method.getId(), method);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public MethodDTO getMethodById(int methodId) {

        Object[] source = queryUnique("methodById", "methodId", IntegerType.INSTANCE, methodId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load method with id = " + methodId);
        }

        return toMethodDTO(Arrays.asList(source).iterator(), getTranscribingNames());
    }

    /** {@inheritDoc} */
    @Override
    public List<MethodDTO> findMethods(Integer methodId, List<String> statusCodes) {
        List<MethodDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Query query = createQuery("methodsByCriteria", "methodId", IntegerType.INSTANCE, methodId);
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            MethodDTO method = toMethodDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            result.add(method);
        }

        return ImmutableList.copyOf(result);
    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForMethodNm());
    }

    private MethodDTO toMethodDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        MethodDTO result = DaliBeanFactory.newMethodDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setReference((String) source.next());
        result.setNumber((String) source.next());
        result.setDescriptionPackaging((String) source.next());
        result.setDescriptionPreparation((String) source.next());
        result.setDescriptionPreservation((String) source.next());
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // transcribed name
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        return result;
    }

}
