package fr.ifremer.dali.dao.system.rule;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.FunctionDTO;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleDao;
import org.apache.commons.lang3.mutable.MutableBoolean;

import java.util.Date;
import java.util.List;

/**
 * <p>DaliRuleDao interface.</p>
 *
 * @author blavenie
 */
public interface DaliRuleDao extends RuleDao {

    /**
     * <p>getRulesByRuleListCode.</p>
     *
     * @param ruleListCode a {@link java.lang.String} object.
     * @param onlyActive   a boolean.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> getRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule);

    /**
     * <p>getPreconditionedRulesByRuleListCode.</p>
     *
     * @param ruleListCode a {@link java.lang.String} object.
     * @param onlyActive   a boolean.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> getPreconditionedRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule);

    /**
     * <p>findActiveRules from 3 criterias:date, program code and department id</p>
     * returns only active rules used for control (without preconditioned rules)
     *
     * @param date         a {@link java.util.Date} object.
     * @param programCode  a {@link java.lang.String} object.
     * @param departmentId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> findActiveRules(Date date, String programCode, Integer departmentId);

    /**
     * <p>findPreconditionedRules.</p>
     *
     * @param date         a {@link java.util.Date} object.
     * @param programCode  a {@link java.lang.String} object.
     * @param departmentId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> findActivePreconditionedRules(Date date, String programCode, Integer departmentId);

    /**
     * <p>findPreconditionedRules.</p>
     *
     * @param programCodes  a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> findActivePreconditionedRules(List<String> programCodes);

    /**
     * Save rule
     *
     * @param rule a {@link ControlRuleDTO} object.
     */
    void save(ControlRuleDTO rule, String ruleListCd);

    /**
     * <p>getAllFunction.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<FunctionDTO> getAllFunction();

    boolean ruleExists(String ruleCode);
}
