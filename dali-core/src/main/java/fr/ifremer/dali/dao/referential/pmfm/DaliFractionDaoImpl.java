package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.FractionDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Fraction DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("daliFractionDao")
public class DaliFractionDaoImpl extends FractionDaoImpl implements DaliFractionDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliMatrixDao")
    private DaliMatrixDao matrixDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliFractionDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<FractionDTO> getAllFractions(List<String> statusCodes) {

        List<FractionDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allFractions"), statusCodes);

        FractionDTO fraction = null;
        while (it.hasNext()) {
            Object[] row = it.next();
            Iterator<Object> rowIt = Arrays.asList(row).iterator();

            if (fraction == null || !fraction.getId().equals(row[0])) {
                fraction = toFractionDTO(rowIt, transcribingNamesById);
                result.add(fraction);
            } else {
                // dummy iteration
                toFractionDTO(rowIt, transcribingNamesById);
            }

            // the last element is the associated matrix
            Integer matrixId = (Integer) rowIt.next();
            if (matrixId != null) {
                MatrixDTO matrix = matrixDao.getMatrixById(matrixId);
                fraction.addMatrixes(matrix);
                matrix.addFractions(fraction);
            }
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public FractionDTO getFractionById(int fractionId) {

        Iterator<Object[]> it = queryIterator("fractionById", "fractionId", IntegerType.INSTANCE, fractionId);
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        if (!it.hasNext()) {
            throw new DataRetrievalFailureException("can't load fraction with id = " + fractionId);
        }

        FractionDTO fraction = null;
        while (it.hasNext()) {
            Object[] row = it.next();
            Iterator<Object> rowIt = Arrays.asList(row).iterator();
            if (fraction == null) {
                fraction = toFractionDTO(rowIt, transcribingNamesById);
            } else {
                toFractionDTO(rowIt, transcribingNamesById);
            }
            // the last element is the associated matrix
            Integer matrixId = (Integer) rowIt.next();
            if (matrixId != null) {
                MatrixDTO matrix = matrixDao.getMatrixById(matrixId);
                fraction.addMatrixes(matrix);
                matrix.addFractions(fraction);
            }
        }
        return fraction;
    }

    /** {@inheritDoc} */
    @Override
    public List<FractionDTO> getFractionsByMatrixId(Integer matrixId) {
        List<FractionDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = queryIterator("fractionsByMatrixId", "matrixId", IntegerType.INSTANCE, matrixId);

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toFractionDTO(Arrays.asList(row).iterator(), transcribingNamesById));
        }

        return ImmutableList.copyOf(result);

    }

    /** {@inheritDoc} */
    @Override
    public List<FractionDTO> findFractions(Integer fractionId, List<String> statusCodes) {
        List<FractionDTO> result = Lists.newArrayList();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Query query = createQuery("fractionByCriteria", "fractionId", IntegerType.INSTANCE, fractionId);
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        FractionDTO fraction = null;
        while (it.hasNext()) {
            Object[] row = it.next();
            Iterator<Object> rowIt = Arrays.asList(row).iterator();

            if (fraction == null || !fraction.getId().equals(row[0])) {
                fraction = toFractionDTO(rowIt, transcribingNamesById);
                result.add(fraction);
            } else {
                // dummy iteration
                toFractionDTO(rowIt, transcribingNamesById);
            }

            // the last element is the associated matrix
            Integer matrixId = (Integer) rowIt.next();
            if (matrixId != null) {
                MatrixDTO matrix = matrixDao.getMatrixById(matrixId);
                fraction.addMatrixes(matrix);
                matrix.addFractions(fraction);
            }
        }

        return ImmutableList.copyOf(result);

    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForFractionNm());
    }

    private FractionDTO toFractionDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        FractionDTO result = DaliBeanFactory.newFractionDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // transcribed name
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        return result;
    }

}
