package fr.ifremer.dali.dao.technical;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.data.photo.PhotoDTO;
import fr.ifremer.quadrige3.core.dao.data.photo.Photo;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.n;

/**
 * Helper class for all DAO operations
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>
 * @since 1.0
 */
public class Daos extends fr.ifremer.quadrige3.core.dao.technical.Daos {

    // TODO get this constant by configuration
    /** Constant <code>SURVEY_OBJECT_TYPE="PASS"</code> */
    public final static String SURVEY_OBJECT_TYPE = "PASS";
    /** Constant <code>SAMPLING_OPERATION_OBJECT_TYPE="PREL"</code> */
    public final static String SAMPLING_OPERATION_OBJECT_TYPE = "PREL";

    static {
        // reserve I18n entries
        for (StatusCode statusCode : StatusCode.values()) {
            String nameKey = String.format("dali.persistence.status.%s.name", statusCode.name());
            n(nameKey);
        }
    }

    /**
     * <p>Constructor for Daos.</p>
     */
    protected Daos() {
        // Helper class = protected constructor
    }

    /**
     * <p>withStatus.</p>
     *
     * @param query a {@link Query} object.
     * @param statusCodes a {@link List} object.
     * @return a {@link Query} object.
     */
    public static Query withStatus(Query query, List<String> statusCodes) {
        Assert.notEmpty(statusCodes);
        Assert.nonNullElement(statusCodes);
        query.setParameterList("statusCodes", statusCodes);
        return query;
    }

    /**
     * <p>queryIteratorWithStatus.</p>
     *
     * @param query a {@link Query} object.
     * @param statusCodes a {@link List} object.
     * @return a {@link Iterator} object.
     */
    @SuppressWarnings("unchecked")
    public static Iterator<Object[]> queryIteratorWithStatus(Query query, List<String> statusCodes) {
        return withStatus(query, statusCodes).iterate();
    }

    /**
     * <p>convertToInteger.</p>
     *
     * @param numericalValue a {@link Float} object.
     * @return a {@link Integer} object.
     */
    public static Integer convertToInteger(Number numericalValue) {
        if (numericalValue == null) {
            return null;
        }
        return numericalValue.intValue();
    }

    /**
     * <p>convertDateOnlyToSQLString.</p>
     *
     * @param date a {@link Date} object.
     * @return a {@link String} object.
     */
    public static String convertDateOnlyToSQLString(Date date) {
        Assert.notNull(date);
        java.sql.Date jdbcDate = date instanceof java.sql.Date
            ? (java.sql.Date) date
            : new java.sql.Date(date.getTime());

        return "\'" + jdbcDate.toString() + " 00:00:00\'";
    }

    /**
     * <p>convertToBigDecimal.</p>
     *
     * @param value a {@link Double} object.
     * @param digitNumber a {@link Integer} object.
     * @return a {@link BigDecimal} object.
     */
    public static BigDecimal convertToBigDecimal(Double value, Integer digitNumber) {

        if (value == null) {
            return null;
        }

        int digitNb = digitNumber == null ? 0 : digitNumber;
        return new BigDecimal(String.format(Locale.US, "%." + digitNb + "f", value));
    }

    /* -- internal methods -- */
    /**
     * Build a file path for a Photo like Q² <br/>
     * ex: PASS/OBJ60092549/PASS-OBJ60092549-60000320.jpg for the survey id=60092549 and photo id=60000320 <br/>
     * or: PREL/OBJ60165512/PREL-OBJ60165512-60003120.jpg for the sampling operation id=60165512 and photo id=60003120
     *
     * Note: the ids are local only, need to recompute with remote ids for synchronization
     *
     * @param source the source photo to obtain file extension
     * @param target the target photo (the entity must be persisted before calling this method)
     * @return the local file path
     */
    public static String computePhotoFilePath(PhotoDTO source, Photo target) {
        if (source == null || StringUtils.isBlank(source.getFullPath())) {
            return null;
        }
        if (target == null || target.getPhotoId() == null || target.getObjectType() == null || target.getObjectId() == null) {
            return null;
        }

        return computePhotoFilePath(
            target.getObjectType().getObjectTypeCd(),
            target.getObjectId(),
            target.getPhotoId(),
            FilenameUtils.getExtension(source.getFullPath()));
    }

}
