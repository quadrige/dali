package fr.ifremer.dali.dao.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.quadrige3.core.dao.referential.SamplingEquipmentDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * Sampling Equipment DAO
 * <p/>
 * Created by Ludovic on 17/07/2015.
 */

@Repository("daliSamplingEquipmentDao")
public class DaliSamplingEquipmentDaoImpl extends SamplingEquipmentDaoImpl implements DaliSamplingEquipmentDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliUnitDao")
    private DaliUnitDao unitDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliSamplingEquipmentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<SamplingEquipmentDTO> getAllSamplingEquipments(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(SAMPLING_EQUIPMENT_BY_ID_CACHE);
        List<SamplingEquipmentDTO> result = new ArrayList<>();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allSamplingEquipments"), statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            SamplingEquipmentDTO samplingEquipment = toSamplingEquipmentDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            result.add(samplingEquipment);

            // update cache
            cacheById.put(samplingEquipment.getId(), samplingEquipment);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public SamplingEquipmentDTO getSamplingEquipmentById(int samplingEquipmentId) {

        Object[] source = queryUnique("samplingEquipmentById", "samplingEquipmentId", IntegerType.INSTANCE, samplingEquipmentId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load sampling equipment with id = " + samplingEquipmentId);
        }

        return toSamplingEquipmentDTO(Arrays.asList(source).iterator(), getTranscribingNames());
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<SamplingEquipmentDTO> getSamplingEquipmentsByIds(List<Integer> samplingEquipmentIds) {

        List<SamplingEquipmentDTO> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(samplingEquipmentIds))
            return result;

        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = createQuery("samplingEquipmentsByIds")
                .setParameterList("samplingEquipmentIds", samplingEquipmentIds)
                .iterate();

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toSamplingEquipmentDTO(Arrays.asList(row).iterator(), transcribingNamesById));
        }

        return result;

    }

    /** {@inheritDoc} */
    @Override
    public List<SamplingEquipmentDTO> findSamplingEquipments(List<String> statusCodes, Integer samplingEquipmentId, Integer unitId) {

        List<SamplingEquipmentDTO> result = new ArrayList<>();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Query query = createQuery("samplingEquipmentsByCriteria",
                "samplingEquipmentId", IntegerType.INSTANCE, samplingEquipmentId,
                "unitId", IntegerType.INSTANCE, unitId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toSamplingEquipmentDTO(Arrays.asList(source).iterator(), transcribingNamesById));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<SamplingEquipmentDTO> findSamplingEquipmentsByName(List<String> statusCodes, String samplingEquipmentName) {

        List<SamplingEquipmentDTO> result = new ArrayList<>();
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Query query = createQuery("samplingEquipmentsByName",
                "samplingEquipmentName", StringType.INSTANCE, samplingEquipmentName);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toSamplingEquipmentDTO(Arrays.asList(source).iterator(), transcribingNamesById));
        }

        return ImmutableList.copyOf(result);

    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForSamplingEquipmentNm());
    }

    private SamplingEquipmentDTO toSamplingEquipmentDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        SamplingEquipmentDTO result = DaliBeanFactory.newSamplingEquipmentDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setSize(Daos.convertToDouble((Float) source.next()));
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        Integer unitId = (Integer) source.next();
        if (unitId != null) result.setUnit(unitDao.getUnitById(unitId));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // transcribed name
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        return result;
    }

}
