package fr.ifremer.dali.dao.system.extraction;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterDao;

import java.io.File;
import java.util.List;

/**
 * <p>DaliExtractionDao interface.</p>
 *
 * @author Ludovic
 */
public interface DaliExtractionDao extends ExtractFilterDao {

    /**
     * <p>getAllExtraction.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<ExtractionDTO> getAllExtractions();

    /**
     * <p>getAllLightExtraction.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<ExtractionDTO> getAllLightExtractions();

    /**
     * <p>getExtractionById.</p>
     *
     * @param extractionId a int.
     * @return a {@link fr.ifremer.dali.dto.system.extraction.ExtractionDTO} object.
     */
    ExtractionDTO getExtractionById(int extractionId);

    /**
     * <p>searchExtractionByProgram.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<ExtractionDTO> searchExtractionByProgram(String programCode);

    /**
     * <p>saveExtraction.</p>
     *
     * @param extraction a {@link fr.ifremer.dali.dto.system.extraction.ExtractionDTO} object.
     */
    void saveExtraction(ExtractionDTO extraction);

    /**
     * Export extraction definition and parameter to a file
     *
     * @param extraction extraction to export
     * @param exportFile destination file
     */
    void exportExtraction(ExtractionDTO extraction, File exportFile);

    /**
     * Import an extraction from a file
     *
     * @param importFile source file
     * @return the imported extraction (already saved in db if valid)
     */
    ExtractionDTO importExtraction(File importFile);

}
