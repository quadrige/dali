package fr.ifremer.dali.dao.referential.monitoringLocation;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.HarbourDTO;
import fr.ifremer.dali.dto.referential.LocationDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>DaliMonitoringLocationDao interface.</p>
 *
 */
public interface DaliMonitoringLocationDao {

    /** Constant <code>ALL_LOCATIONS_CACHE="allLocations"</code> */
    String ALL_LOCATIONS_CACHE = "all_locations";
    /** Constant <code>LOCATION_BY_ID_CACHE="locationById"</code> */
    String LOCATION_BY_ID_CACHE = "location_by_id";
    /** Constant <code>LOCATIONS_BY_IDS_CACHE="locationsByIds"</code> */
    String LOCATIONS_BY_IDS_CACHE = "locations_by_ids";
    /** Constant <code>LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE="locationsByCampaignAndProgram"</code> */
    String LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE = "locations_by_campaign_and_program";
    /** Constant <code>ALL_HARBOURS_CACHE="allHarbours"</code> */
    String ALL_HARBOURS_CACHE = "all_harbours";

    /**
     * <p>getAllLocations.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_LOCATIONS_CACHE)
    List<LocationDTO> getAllLocations(List<String> statusCodes);

    /**
     * <p>getLocationById.</p>
     *
     * @param locationId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.LocationDTO} object.
     */
    @Cacheable(value = LOCATION_BY_ID_CACHE)
    LocationDTO getLocationById(int locationId);

    /**
     * <p>getLocationsByIds.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = LOCATIONS_BY_IDS_CACHE)
    List<LocationDTO> getLocationsByIds(List<Integer> locationIds);

    /**
     * <p>getLocationsByCampaignAndProgram.</p>
     *
     * @param campaignId  a {@link Integer} object.
     * @param programCode a {@link String} object.
     * @param statusCodes
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE)
    List<LocationDTO> getLocationsByCampaignAndProgram(Integer campaignId, String programCode, List<String> statusCodes);

    /**
     * <p>findLocations.</p>
     *
     * @param statusCodes       a {@link List} object.
     * @param orderItemTypeCode a {@link String} object.
     * @param orderItemId       a {@link Integer} object.
     * @param programCode       a {@link String} object.
     * @param label             a {@link String} object.
     * @param name              a {@link String} object.
     * @param isStrictName      a boolean.
     * @return a {@link java.util.List} object.
     */
    List<LocationDTO> findLocations(List<String> statusCodes, String orderItemTypeCode, Integer orderItemId, String programCode, String label, String name, boolean isStrictName);

    /**
     * <p>getAllHarbours.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_HARBOURS_CACHE)
    List<HarbourDTO> getAllHarbours(List<String> statusCodes);

}
