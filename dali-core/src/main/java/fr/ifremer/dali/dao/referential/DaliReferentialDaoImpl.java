package fr.ifremer.dali.dao.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.pmfm.DaliPmfmDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.*;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.HibernateDaoSupport;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>DaliReferentialDaoImpl class.</p>
 *
 */
@Repository("daliReferentialDao")
public class DaliReferentialDaoImpl extends HibernateDaoSupport implements DaliReferentialDao {

    /**
     * Logger.
     */
//    private static final Log log = LogFactory.getLog(DaliReferentialDaoImpl.class);

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao loopbackDao;

    /**
     * <p>Constructor for DaliReferentialDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliReferentialDaoImpl(SessionFactory sessionFactory) {
        super();
        setSessionFactory(sessionFactory);
    }

    @Override
    public List<StatusDTO> getAllStatus() {
        Cache cacheByCode = cacheService.getCache(STATUS_BY_CODE_CACHE);
        List<StatusDTO> result = new ArrayList<>();
        Iterator<Object[]> it = queryIterator("allStatus");
        Map<String, String> transcribingNames = getTranscribingStatusNamesByCode();
        while (it.hasNext()) {
            Object[] source = it.next();
            StatusDTO status = toStatusDTO(Arrays.asList(source).iterator(), transcribingNames);
            result.add(status);

            cacheByCode.put(status.getCode(), status);
        }
        return result;
    }

    @Override
    public StatusDTO getStatusByCode(String statusCode) {
        Object[] source = queryUnique("statusByCode", "statusCd", StringType.INSTANCE, statusCode);
        if (source == null) {
            throw new DataRetrievalFailureException("can't load status with code = " + statusCode);
        }
        return toStatusDTO(Arrays.asList(source).iterator(), getTranscribingStatusNamesByCode());
    }

    /** {@inheritDoc} */
    @Override
    public List<StatusDTO> getStatusByCodes(List<String> statusCodes) {
        Assert.notNull(statusCodes);
        return statusCodes.stream().map(statusCode -> loopbackDao.getStatusByCode(statusCode)).collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<LevelDTO> getAllDepthLevels() {

        // only enable (=national) depth levels
        String statusCode = StatusCode.ENABLE.getValue();

        Cache cacheById = cacheService.getCache(DEPTH_LEVEL_BY_ID_CACHE);

        Iterator<Object[]> it = queryIterator("allDepthLevels",
                "statusCode", StringType.INSTANCE, statusCode);

        List<LevelDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            LevelDTO depthLevel = toDepthLevelDTO(Arrays.asList(source).iterator(), statusCode);
            result.add(depthLevel);

            // update cache
            cacheById.put(depthLevel.getId(), depthLevel);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public LevelDTO getDepthLevelById(int depthLevelId) {

        // only enable (=national) depth levels
        String statusCode = StatusCode.ENABLE.getValue();

        Object[] source = queryUnique("depthLevelById",
                "statusCode", StringType.INSTANCE, statusCode,
                "depthLevelId", IntegerType.INSTANCE, depthLevelId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load depth level with id = " + depthLevelId);
        }

        return toDepthLevelDTO(Arrays.asList(source).iterator(), statusCode);
    }

    /** {@inheritDoc} */
    @Override
    public List<QualityLevelDTO> getAllQualityFlags(List<String> statusCodes) {

        Cache cacheByCode = cacheService.getCache(QUALITY_FLAG_BY_CODE_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allQualityFlags"), statusCodes);
        Map<String, String> transcribingNamesByCode = getTranscribingQualityLevelNamesByCode();

        List<QualityLevelDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            QualityLevelDTO qualityFlag = toQualityLevelDTO(Arrays.asList(source).iterator(), transcribingNamesByCode);
            result.add(qualityFlag);

            cacheByCode.put(qualityFlag.getCode(), qualityFlag);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public QualityLevelDTO getQualityFlagByCode(String qualityFlagCode) {

        Object[] source = queryUnique("qualityFlagByCode", "qualityFlagCode", StringType.INSTANCE, qualityFlagCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load quality flag with code = " + qualityFlagCode);
        }

        return toQualityLevelDTO(Arrays.asList(source).iterator(), getTranscribingQualityLevelNamesByCode());
    }

    /** {@inheritDoc} */
    @Override
    public List<PositioningSystemDTO> getAllPositioningSystems() {

        Cache cacheById = cacheService.getCache(POSITIONING_SYSTEM_BY_ID_CACHE);

        Iterator<Object[]> it = queryIterator("allPositioningSystems");

        List<PositioningSystemDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            PositioningSystemDTO positioningSystem = toPositioningSystemDTO(Arrays.asList(source).iterator());
            result.add(positioningSystem);

            cacheById.put(positioningSystem.getId(), positioningSystem);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public PositioningSystemDTO getPositioningSystemById(int posSystemId) {
        Object[] source = queryUnique("positioningSystemById", "posSystemId", IntegerType.INSTANCE, posSystemId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load positioning system with id = " + posSystemId);
        }

        return toPositioningSystemDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<GroupingTypeDTO> getAllGroupingTypes() {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allOrderItemTypes"), StatusFilter.ACTIVE.toStatusCodes());

        List<GroupingTypeDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            GroupingTypeDTO groupingType = toGroupingTypeDTO(Arrays.asList(source).iterator());

            groupingType.setGrouping(getGroupingsByType(groupingType.getCode()));

            result.add(groupingType);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<GroupingDTO> getGroupingsByType(String groupingType) {
        Iterator<Object[]> it = queryIterator("allOrderItemsByTypeCode", "typeCode", StringType.INSTANCE, groupingType);

        List<GroupingDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toGroupingDTO(Arrays.asList(source).iterator()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<CitationDTO> getAllCitations() {
        Iterator<Object[]> it = queryIterator("allCitations");

        List<CitationDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toCitationDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<PhotoTypeDTO> getAllPhotoTypes() {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allPhotoTypes"), StatusFilter.ACTIVE.toStatusCodes());

        List<PhotoTypeDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toPhotoTypeDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public PhotoTypeDTO getPhotoTypeByCode(String photoTypeCode) {
        Object[] source = queryUnique("photoTypeByCode", "photoTypeCode", StringType.INSTANCE, photoTypeCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load photo type with id = " + photoTypeCode);
        }

        return toPhotoTypeDTO(Arrays.asList(source).iterator());
    }

    @Override
    public List<TaxonomicLevelDTO> getAllTaxonomicLevels() {
        Cache cacheByCode = cacheService.getCache(TAXONOMIC_LEVEL_BY_CODE_CACHE);

        Iterator<Object[]> it = queryIterator("allTaxonomicLevels");

        List<TaxonomicLevelDTO> result = Lists.newArrayList();
        Map<String, String> transcribingNamesByCode = getTranscribingTaxonomicLevelNamesByCode();

        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonomicLevelDTO taxonomicLevel = toTaxonomicLevelDTO(Arrays.asList(source).iterator(), transcribingNamesByCode);
            result.add(taxonomicLevel);

            cacheByCode.put(taxonomicLevel.getCode(), taxonomicLevel);
        }
        return ImmutableList.copyOf(result);
    }

    @Override
    public TaxonomicLevelDTO getTaxonomicLevelByCode(String code) {
        Object[] source = queryUnique("taxonomicLevelByCode", "taxLevelCd", StringType.INSTANCE, code);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load taxonomic level with code = " + code);
        }

        return toTaxonomicLevelDTO(Arrays.asList(source).iterator(), getTranscribingTaxonomicLevelNamesByCode());
    }

    // INTERNAL METHODS

    private Map<String, String> getTranscribingStatusNamesByCode() {
        return transcribingItemDao.getAllTranscribingItemsByCode(config.getTranscribingItemTypeLbForStatusNm());
    }

    private StatusDTO toStatusDTO(Iterator<Object> source, Map<String, String> transcribingNamesByCode) {
        StatusDTO result = QuadrigeBeanFactory.newStatusDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        // transcribing name (Mantis #47626)
        result.setName(transcribingNamesByCode.getOrDefault(result.getCode(), result.getName()));
        return result;
    }

    private Map<String, String> getTranscribingTaxonomicLevelNamesByCode() {
        return transcribingItemDao.getAllTranscribingItemsByCode(config.getTranscribingItemTypeLbForTaxLevelNm());
    }

    private TaxonomicLevelDTO toTaxonomicLevelDTO(Iterator<Object> source, Map<String, String> transcribingNamesByCode) {
        TaxonomicLevelDTO result = DaliBeanFactory.newTaxonomicLevelDTO();
        result.setCode((String) source.next());
        result.setLabel((String) source.next());
        result.setName((String) source.next());
        result.setNumber((Integer) source.next());
        // transcribed name (Mantis #47626)
        result.setName(transcribingNamesByCode.getOrDefault(result.getCode(), result.getName()));
        return result;
    }

    private LevelDTO toDepthLevelDTO(Iterator<Object> source, String statusCode) {
        LevelDTO result = DaliBeanFactory.newLevelDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(loopbackDao.getStatusByCode(statusCode));
        return result;
    }

    private PhotoTypeDTO toPhotoTypeDTO(Iterator<Object> source) {
        PhotoTypeDTO result = DaliBeanFactory.newPhotoTypeDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(loopbackDao.getStatusByCode((String) source.next()));
        return result;
    }

    private Map<String, String> getTranscribingQualityLevelNamesByCode() {
        return transcribingItemDao.getAllTranscribingItemsByCode(config.getTranscribingItemTypeLbForQualFlagNm());
    }

    private QualityLevelDTO toQualityLevelDTO(Iterator<Object> source, Map<String, String> transcribingNamesByCode) {
        QualityLevelDTO result = DaliBeanFactory.newQualityLevelDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(loopbackDao.getStatusByCode((String) source.next()));
        // transcribed name (Mantis #47330)
        result.setName(transcribingNamesByCode.getOrDefault(result.getCode(), result.getName()));
        return result;
    }

    private PositioningSystemDTO toPositioningSystemDTO(Iterator<Object> source) {
        PositioningSystemDTO result = DaliBeanFactory.newPositioningSystemDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setPrecision((String) source.next());
        // a positioning system is always enable (=national)
        result.setStatus(loopbackDao.getStatusByCode(StatusCode.ENABLE.getValue()));
        return result;
    }

    private GroupingTypeDTO toGroupingTypeDTO(Iterator<Object> source) {
        GroupingTypeDTO result = DaliBeanFactory.newGroupingTypeDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        return result;
    }

    private GroupingDTO toGroupingDTO(Iterator<Object> source) {
        GroupingDTO result = DaliBeanFactory.newGroupingDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        return result;
    }

    private CitationDTO toCitationDTO(Iterator<Object> source) {
        CitationDTO result = DaliBeanFactory.newCitationDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        return result;
    }
}
