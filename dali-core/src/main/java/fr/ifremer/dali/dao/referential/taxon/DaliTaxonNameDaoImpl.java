package fr.ifremer.dali.dao.referential.taxon;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.CitationDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.TaxonGroupTypeCode;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonNameDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>DaliTaxonNameDaoImpl class.</p>
 *
 */
@Repository("daliTaxonNameDao")
public class DaliTaxonNameDaoImpl extends TaxonNameDaoImpl implements DaliTaxonNameDao {

    private static final Log LOG = LogFactory.getLog(DaliTaxonNameDaoImpl.class);

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliReferentialDao")
    private DaliReferentialDao referentialDao;

    @Resource(name = "daliTaxonNameDao")
    private DaliTaxonNameDao loopbackTaxonNameDao;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    /**
     * <p>Constructor for DaliTaxonNameDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliTaxonNameDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonDTO> getAllTaxonNames() {

        Cache cacheByReferenceId = cacheService.getCache(TAXON_NAME_BY_REFERENCE_ID_CACHE);
        Cache cacheById = cacheService.getCache(TAXON_NAME_BY_ID_CACHE);

        Map<Integer, String> taxRefMap = loopbackTaxonNameDao.getTaxRefByTaxonNameId();
        Map<Integer, String> wormsMap = loopbackTaxonNameDao.getWormsByTaxonNameId();

        Iterator<Object[]> it = queryIterator("allTaxonName");

        long nbTaxonName = 0;
        long nbRefTaxon = 0;
        List<TaxonDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonDTO taxon = toTaxonDTO(Arrays.asList(source).iterator());

            // add other references
            taxon.setTaxRef(taxRefMap.get(taxon.getId()));
            taxon.setWormsRef(wormsMap.get(taxon.getId()));

            result.add(taxon);

            // update cache by id
            cacheById.put(taxon.getId(), taxon);
            nbTaxonName++;
            // update cache by reference id
            if (taxon.isReferent()) {
                cacheByReferenceId.put(taxon.getReferenceTaxonId(), taxon);
                nbRefTaxon++;
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("%s Taxon name loaded, %s reference taxon loaded", nbTaxonName, nbRefTaxon));
        }
        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonDTO getTaxonNameByReferenceId(int referenceTaxonId) {

        Object[] source = queryUnique("taxonNameByReferenceId", "taxonReferenceId", IntegerType.INSTANCE, referenceTaxonId);

        if (source == null) {
            throw new DataRetrievalFailureException("Can't load taxon name with reference taxon id = " + referenceTaxonId);
        }

        return toTaxonDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public TaxonDTO getTaxonNameById(int taxonId) {

        Object[] source = queryUnique("taxonNameById", "taxonNameId", IntegerType.INSTANCE, taxonId);

        if (source == null) {
            return null;
        }

        TaxonDTO taxon = toTaxonDTO(Arrays.asList(source).iterator());
        fillReferent(taxon);
        return taxon;
    }

    /** {@inheritDoc} */
    @Override
    public Multimap<Integer, TaxonDTO> getAllTaxonNamesMapByTaxonGroupId(@Nonnull LocalDate date) {

        Iterator<Object[]> it = queryIterator("taxonNameIdsWithTaxonGroupId",
                "referenceDate", DateType.INSTANCE, Dates.convertToDate(date, config.getDbTimezone()),
                "taxonGroupTypeCode", StringType.INSTANCE, TaxonGroupTypeCode.IDENTIFICATION.getValue());

        Multimap<Integer, TaxonDTO> result = ArrayListMultimap.create();
        while (it.hasNext()) {
            Object[] source = it.next();
            Iterator<Object> row = Arrays.asList(source).iterator();
            Integer taxonGroupId = (Integer) row.next();
            result.put(taxonGroupId, loopbackTaxonNameDao.getTaxonNameById((Integer) row.next()));
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonDTO> getTaxonNamesByIds(List<Integer> taxonIds) {

        if (CollectionUtils.isEmpty(taxonIds)) return new ArrayList<>();

        return loopbackTaxonNameDao.getAllTaxonNames().stream().filter(taxon -> taxonIds.contains(taxon.getId())).collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public void fillTaxonsProperties(List<TaxonDTO> taxons) {

        if (CollectionUtils.isEmpty(taxons)) {
            return;
        }

        Map<Integer, String> taxRefMap = loopbackTaxonNameDao.getTaxRefByTaxonNameId();
        Map<Integer, String> wormsMap = loopbackTaxonNameDao.getWormsByTaxonNameId();

        for (TaxonDTO taxon : taxons) {

            // parent taxon and reference taxon
            fillParentAndReferent(taxon);

            // composites
            if (taxon.isVirtual()) {
                taxon.setCompositeTaxons(loopbackTaxonNameDao.getCompositeTaxonNames(taxon.getId()));
            }

            // fill other references like (WORMS and TAXREFv80)
            taxon.setTaxRef(taxRefMap.get(taxon.getId()));
            taxon.setWormsRef(wormsMap.get(taxon.getId()));

        }
    }

    private void fillTaxonProperties(TaxonDTO taxon) {

        // parent taxon and reference taxon
        fillParentAndReferent(taxon);

        // fill other references like (WORMS and TAXREFv80)
        taxon.setTaxRef(loopbackTaxonNameDao.getTaxRefByTaxonNameId().get(taxon.getId()));
        taxon.setWormsRef(loopbackTaxonNameDao.getWormsByTaxonNameId().get(taxon.getId()));

    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonDTO> findFullTaxonNamesByCriteria(String levelCode, String name, boolean isStrictName) {
        List<TaxonDTO> result = findTaxonNamesByCriteria(levelCode, name, isStrictName);

        fillTaxonsProperties(result);

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonDTO> findTaxonNamesByCriteria(String levelCode, String name, boolean isStrictName) {

        List<TaxonDTO> result = new ArrayList<>();

        // for better performance if no parameter
        if (StringUtils.isBlank(levelCode) && StringUtils.isBlank(name)) {

            result.addAll(loopbackTaxonNameDao.getAllTaxonNames());

        } else {

            Iterator<Object[]> it = queryIterator("taxonNamesByCriteria",
                    "levelCd", StringType.INSTANCE, levelCode,
                    "name", StringType.INSTANCE, isStrictName ? null : name,
                    "strictName", StringType.INSTANCE, isStrictName ? name : null);

            while (it.hasNext()) {
                Object[] source = it.next();
                result.add(toTaxonDTO(Arrays.asList(source).iterator()));
            }
        }

        fillReferents(result);

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonDTO> getCompositeTaxonNames(Integer taxonNameId) {
        Iterator<Object[]> it = queryIterator("compositeTaxonNamesByTaxonNameId",
                "taxonNameId", IntegerType.INSTANCE, taxonNameId);

        List<TaxonDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonDTO taxon = toTaxonDTO(Arrays.asList(source).iterator());
            fillTaxonProperties(taxon);
            result.add(taxon);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Map<Integer, String> getTaxRefByTaxonNameId() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForTaxRef());
//        return getAlternateReferencesMap(config.getAlternativeTaxonOriginTaxRef());
    }

    /** {@inheritDoc} */
    @Override
    public Map<Integer, String> getWormsByTaxonNameId() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForWorms());
//        return getAlternateReferencesMap(config.getAlternativeTaxonOriginWorms());
    }

//    private Map<Integer, String> getAlternateReferencesMap(String originCode) {
//
//        Map<Integer, String> result = Maps.newHashMap();
//        if (StringUtils.isNotBlank(originCode)) {
//
//            Iterator<Object[]> rows = queryIterator("alternateTaxonCode",
//                    "originCode", StringType.INSTANCE, originCode);
//
//            while (rows.hasNext()) {
//                Object[] row = rows.next();
//                result.put((Integer) row[0], (String) row[1]);
//            }
//        }
//
//        return result;
//    }

    // INTERNAL METHODS

    private TaxonDTO toTaxonDTO(Iterator<Object> source) {
        TaxonDTO result = DaliBeanFactory.newTaxonDTO();

        // Id
        result.setId((Integer) source.next());

        // Name (complete name)
        result.setName((String) source.next());

        result.setComment((String) source.next());

        result.setReferent((Boolean) source.next());
        result.setVirtual((Boolean) source.next());
        result.setObsolete((Boolean) source.next());
        result.setTemporary((Boolean) source.next());

        result.setReferenceTaxonId((Integer) source.next());
        result.setParentTaxonId((Integer) source.next());

        String levelCode = (String) source.next();
        Integer citationId = (Integer) source.next();
        String citationName = (String) source.next();

        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // taxonomic level
        if (levelCode != null) {
            result.setLevel(referentialDao.getTaxonomicLevelByCode(levelCode));
        }

        // citation
        if (citationId != null) {
            CitationDTO citation = DaliBeanFactory.newCitationDTO();
            citation.setId(citationId);
            citation.setName(citationName);

            result.setCitation(citation);
        }

        // Status
        String statusCode = Boolean.TRUE.equals(result.isTemporary())
                ? StatusCode.TEMPORARY.getValue()
                : StatusCode.ENABLE.getValue();
        result.setStatus(referentialDao.getStatusByCode(statusCode));

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public void fillParentAndReferent(TaxonDTO taxon) {

        // referent Taxon
        fillReferent(taxon);

        // parent Taxon
        if (taxon.getParentTaxonId() != null) {
            if (taxon.getParentTaxonId().equals(taxon.getId())) {
                taxon.setParentTaxon(taxon);
            } else {
                taxon.setParentTaxon(loopbackTaxonNameDao.getTaxonNameById(taxon.getParentTaxonId()));
            }
        }
    }

    @Override
    public void fillReferents(List<TaxonDTO> taxons) {

        if (taxons == null) return;
        taxons.forEach(this::fillReferent);

    }

    private void fillReferent(TaxonDTO taxon) {

        // get referent taxon
        if (taxon.getReferenceTaxon() == null || !Objects.equals(taxon.getReferenceTaxon().getId(), taxon.getReferenceTaxonId())) {
            taxon.setReferenceTaxon(loopbackTaxonNameDao.getTaxonNameByReferenceId(taxon.getReferenceTaxonId()));
        }
    }

}
