package fr.ifremer.dali.dao.referential.transcribing;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.transcribing.TranscribingItemExtendDao;
import org.springframework.cache.annotation.Cacheable;

import java.util.Map;

/**
 * @author peck7 on 02/11/2017.
 */
public interface DaliTranscribingItemDao extends TranscribingItemExtendDao {

    String TRANSCRIBING_ITEMS_BY_ID_CACHE = "transcribing_items_by_id";
    String TRANSCRIBING_ITEMS_BY_CODE_CACHE = "transcribing_items_by_code";


    @Cacheable(value = TRANSCRIBING_ITEMS_BY_ID_CACHE)
    @Override
    Map<Integer, String> getAllTranscribingItemsById(String transcribingItemTypeLabel);

    @Cacheable(value = TRANSCRIBING_ITEMS_BY_CODE_CACHE)
    @Override
    Map<String, String> getAllTranscribingItemsByCode(String transcribingItemTypeLabel);
}
