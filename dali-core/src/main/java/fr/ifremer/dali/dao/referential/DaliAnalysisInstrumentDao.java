package fr.ifremer.dali.dao.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.dto.referential.AnalysisInstrumentDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 17/07/2015.
 */
public interface DaliAnalysisInstrumentDao {

    /** Constant <code>ANALYSIS_INSTRUMENT_BY_ID_CACHE="analysisInstrumentById"</code> */
    String ANALYSIS_INSTRUMENT_BY_ID_CACHE = "analysis_instrument_by_id";
    /** Constant <code>ALL_ANALYSIS_INSTRUMENTS_CACHE="allAnalysisInstruments"</code> */
    String ALL_ANALYSIS_INSTRUMENTS_CACHE = "all_analysis_instruments";
    /** Constant <code>ANALYSIS_INSTRUMENTS_BY_IDS_CACHE="analysisInstrumentsByIds"</code> */
    String ANALYSIS_INSTRUMENTS_BY_IDS_CACHE = "analysis_instruments_by_ids";

    /**
     * <p>getAllAnalysisInstruments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_ANALYSIS_INSTRUMENTS_CACHE)
    List<AnalysisInstrumentDTO> getAllAnalysisInstruments(List<String> statusCodes);

    /**
     * <p>getAnalysisInstrumentById.</p>
     *
     * @param analysisInstrumentId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.AnalysisInstrumentDTO} object.
     */
    @Cacheable(value = ANALYSIS_INSTRUMENT_BY_ID_CACHE)
    AnalysisInstrumentDTO getAnalysisInstrumentById(int analysisInstrumentId);

    /**
     * <p>getAnalysisInstrumentsByIds.</p>
     *
     * @param analysisInstrumentsIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ANALYSIS_INSTRUMENTS_BY_IDS_CACHE)
    List<AnalysisInstrumentDTO> getAnalysisInstrumentsByIds(List<Integer> analysisInstrumentsIds);

    /**
     * <p>findAnalysisInstruments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param analysisInstrumentId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<AnalysisInstrumentDTO> findAnalysisInstruments(List<String> statusCodes, Integer analysisInstrumentId);

    /**
     * <p>findAnalysisInstrumentsByName.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param analysisInstrumentName a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<AnalysisInstrumentDTO> findAnalysisInstrumentsByName(List<String> statusCodes, String analysisInstrumentName);

}
