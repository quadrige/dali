package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.DaliUnitDao;
import fr.ifremer.dali.dao.referential.transcribing.DaliTranscribingItemDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.referential.pmfm.FractionDTO;
import fr.ifremer.dali.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.PmfmDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>DaliPmfmDaoImpl class.</p>
 */
@Repository("daliPmfmDao")
public class DaliPmfmDaoImpl extends PmfmDaoImpl implements DaliPmfmDao {

    @Resource
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliParameterDao")
    private DaliParameterDao parameterDao;

    @Resource(name = "daliMethodDao")
    private DaliMethodDao methodDao;

    @Resource(name = "daliFractionDao")
    private DaliFractionDao fractionDao;

    @Resource(name = "daliMatrixDao")
    private DaliMatrixDao matrixDao;

    @Resource(name = "daliUnitDao")
    private DaliUnitDao unitDao;

    @Resource(name = "daliTranscribingItemDao")
    private DaliTranscribingItemDao transcribingItemDao;

    @Resource(name = "daliQualitativeValueDao")
    private DaliQualitativeValueDao qualitativeValueDao;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * <p>Constructor for DaliPmfmDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliPmfmDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getAllPmfms(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(PMFM_BY_ID_CACHE);

        // Read all transcribing names
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allPmfms"), statusCodes);

        List<PmfmDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();

            // Create the VO
            PmfmDTO pmfm = toPmfmDTO(Arrays.asList(source).iterator(), transcribingNamesById);

            result.add(pmfm);
            cacheById.put(pmfm.getId(), pmfm);
        }

        return ImmutableList.copyOf(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> findPmfms(String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, List<String> statusCodes) {
        // Read all transcribing names
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Query query = createQuery("pmfmsByCriteria",
                "parameterCode", StringType.INSTANCE, parameterCode,
                "matrixId", IntegerType.INSTANCE, matrixId,
                "fractionId", IntegerType.INSTANCE, fractionId,
                "methodId", IntegerType.INSTANCE, methodId,
                "unitId", IntegerType.INSTANCE, unitId,
                "pmfmName", StringType.INSTANCE, pmfmName); // todo LP 07/02/2020 : the transcribing name is not handled (see Mantis #49923)

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        // other solution: use criteria API and findByExample
        List<PmfmDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();

            // Create the VO
            PmfmDTO pmfm = toPmfmDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            result.add(pmfm);
        }

        return ImmutableList.copyOf(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PmfmDTO getPmfmById(int pmfmId) {
        Object[] source = queryUnique("pmfmById",
                "pmfmId", IntegerType.INSTANCE, pmfmId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load pmfm with id = " + pmfmId);
        }

        return toPmfmDTO(Arrays.asList(source).iterator(), getTranscribingNames());
    }

    /**
     * {@inheritDoc}
     *
     * @param pmfmIds
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<PmfmDTO> getPmfmsByIds(Collection<Integer> pmfmIds) {

        List<PmfmDTO> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(pmfmIds))
            return result;

        // Read all transcribing names
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = createQuery("pmfmByIds")
                .setParameterList("pmfmIds", pmfmIds)
                .iterate();

        while (it.hasNext()) {
            Object[] source = it.next();

            // Create the VO
            result.add(toPmfmDTO(Arrays.asList(source).iterator(), transcribingNamesById));
        }

        return result;
    }

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForPmfmNm());
    }

    private PmfmDTO toPmfmDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        PmfmDTO result = DaliBeanFactory.newPmfmDTO();

        result.setId((Integer) source.next());

        // max nb decimals
        result.setMaxDecimals((Short) source.next());

        // nb significant figures
        result.setFiguresNumber((Short) source.next());

        // status
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));

        // parameter by cache
        result.setParameter(parameterDao.getParameterByCode((String) source.next()));

        // matrix by cache
        MatrixDTO matrix = DaliBeans.clone(matrixDao.getMatrixById((int) source.next()));
        // remove some redundant data
        if (matrix != null) matrix.setFractions(null);
        result.setMatrix(matrix);

        // fraction by cache
        FractionDTO fraction = DaliBeans.clone(fractionDao.getFractionById((int) source.next()));
        // remove some redundant data
        if (fraction != null) fraction.setMatrixes(null);
        result.setFraction(fraction);

        // method by cache
        result.setMethod(methodDao.getMethodById((int) source.next()));

        // unit by cache
        result.setUnit(unitDao.getUnitById((int) source.next()));

        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // name
        result.setName(transcribingNamesById.getOrDefault(result.getId(), result.getName()));

        // QV
        result.setQualitativeValues(qualitativeValueDao.getQualitativeValuesByPmfmId(result.getId()));

        return result;
    }

}
