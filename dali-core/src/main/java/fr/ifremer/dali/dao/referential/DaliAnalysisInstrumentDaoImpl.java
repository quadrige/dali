package fr.ifremer.dali.dao.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.quadrige3.core.dao.referential.AnalysisInstrumentDaoImpl;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Analysis Instrument DAO
 * <p/>
 * Created by Ludovic on 17/07/2015.
 */
@Repository("daliAnalysisInstrumentDao")
public class DaliAnalysisInstrumentDaoImpl extends AnalysisInstrumentDaoImpl implements DaliAnalysisInstrumentDao {

    @Resource
    protected CacheService cacheService;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliAnalysisInstrumentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<AnalysisInstrumentDTO> getAllAnalysisInstruments(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(ANALYSIS_INSTRUMENT_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allAnalysisInstruments"), statusCodes);

        List<AnalysisInstrumentDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            AnalysisInstrumentDTO analysisInstrument = toAnalysisInstrumentDTO(Arrays.asList(source).iterator());
            result.add(analysisInstrument);

            cacheById.put(analysisInstrument.getId(), analysisInstrument);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<AnalysisInstrumentDTO> findAnalysisInstruments(List<String> statusCodes, Integer analysisInstrumentId) {

        Query query = createQuery("analysisInstrumentsByCriteria",
                "analysisInstrumentId", IntegerType.INSTANCE, analysisInstrumentId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<AnalysisInstrumentDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toAnalysisInstrumentDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<AnalysisInstrumentDTO> findAnalysisInstrumentsByName(List<String> statusCodes, String analysisInstrumentName) {
        Query query = createQuery("analysisInstrumentsByName",
                "analysisInstrumentName", StringType.INSTANCE, analysisInstrumentName);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<AnalysisInstrumentDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toAnalysisInstrumentDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public AnalysisInstrumentDTO getAnalysisInstrumentById(int analysisInstrumentId) {

        Object[] source = queryUnique("analysisInstrumentById", "analysisInstrumentId", IntegerType.INSTANCE, analysisInstrumentId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load analysis instrument with id = " + analysisInstrumentId);
        }

        return toAnalysisInstrumentDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<AnalysisInstrumentDTO> getAnalysisInstrumentsByIds(List<Integer> analysisInstrumentIds) {

        List<AnalysisInstrumentDTO> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(analysisInstrumentIds))
            return result;

        Iterator<Object[]> it = createQuery("analysisInstrumentsByIds")
                .setParameterList("analysisInstrumentIds", analysisInstrumentIds)
                .iterate();

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toAnalysisInstrumentDTO(Arrays.asList(row).iterator()));
        }

        return result;

    }


    /* private methods */

    private AnalysisInstrumentDTO toAnalysisInstrumentDTO(Iterator<Object> source) {
        AnalysisInstrumentDTO result = DaliBeanFactory.newAnalysisInstrumentDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
