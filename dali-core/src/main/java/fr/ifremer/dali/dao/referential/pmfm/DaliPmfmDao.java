package fr.ifremer.dali.dao.referential.pmfm;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.Collection;
import java.util.List;

/**
 * <p>DaliPmfmDao interface.</p>
 *
 */
public interface DaliPmfmDao {

    /** Constant <code>ALL_PMFMS_CACHE="allPmfms"</code> */
    String ALL_PMFMS_CACHE = "all_pmfms";
    /** Constant <code>PMFM_BY_ID_CACHE="pmfmById"</code> */
    String PMFM_BY_ID_CACHE = "pmfm_by_id";
    /** Constant <code>PMFMS_BY_IDS_CACHE="pmfmsByIds"</code> */
    String PMFMS_BY_IDS_CACHE = "pmfms_by_ids";
    String PMFMS_BY_CRITERIA_CACHE = "pmfms_by_criteria";

    /**
     * <p>getAllPmfms.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_PMFMS_CACHE)
    List<PmfmDTO> getAllPmfms(List<String> statusCodes);

    /**
     * <p>getPmfmById.</p>
     *
     * @param pmfmId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.PmfmDTO} object.
     */
    @Cacheable(value = PMFM_BY_ID_CACHE)
    PmfmDTO getPmfmById(int pmfmId);

    /**
     * <p>getPmfmsByIds.</p>
     *
     * @param pmfmIds a {@link List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = PMFMS_BY_IDS_CACHE)
    List<PmfmDTO> getPmfmsByIds(Collection<Integer> pmfmIds);

    /**
     * <p>findPmfms.</p>
     *
     * @param parameterCode a {@link String} object.
     * @param matrixId a {@link Integer} object.
     * @param fractionId a {@link Integer} object.
     * @param methodId a {@link Integer} object.
     * @param unitId
     * @param pmfmName a {@link String} object.
     * @param statusCodes a {@link List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = PMFMS_BY_CRITERIA_CACHE)
    List<PmfmDTO> findPmfms(String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, List<String> statusCodes);

}
