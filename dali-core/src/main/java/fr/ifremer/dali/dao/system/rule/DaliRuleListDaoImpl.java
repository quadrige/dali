package fr.ifremer.dali.dao.system.rule;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.dali.dto.configuration.control.RuleListDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.dali.service.system.SystemService;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.Quser;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserImpl;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleList;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Beans;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;

/**
 * <p>DaliRuleListDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("daliRuleListDao")
public class DaliRuleListDaoImpl extends RuleListDaoImpl implements DaliRuleListDao {

    private static final Log log = LogFactory.getLog(DaliRuleListDaoImpl.class);

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliRuleDao")
    private DaliRuleDao ruleDao;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Resource(name = "daliProgramStrategyService")
    private ProgramStrategyService programStrategyService;

    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;

    @Resource(name = "daliSystemService")
    protected SystemService systemService;

    /**
     * <p>Constructor for DaliRuleListDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public DaliRuleListDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RuleListDTO> getAllRuleLists() {

        Iterator<Object[]> it = queryIterator("allRuleList");
        return toRuleListDTOs(it);
    }

    @Override
    public List<RuleListDTO> getRuleListsForProgram(String programCode) {

        Iterator<Object[]> it = queryIterator("allRuleListWithProgramCode",
                "programCode", StringType.INSTANCE, programCode);
        return toRuleListDTOs(it);
    }

    @Override
    public boolean ruleListExists(String ruleListCode) {
        Assert.notBlank(ruleListCode);

        return queryUnique("ruleListByCode", "ruleListCode", StringType.INSTANCE, ruleListCode) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleListDTO getRuleList(String ruleListCode) {
        Assert.notBlank(ruleListCode);

        Object[] source = queryUnique("ruleListByCode", "ruleListCode", StringType.INSTANCE, ruleListCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load rule list with code = " + ruleListCode);
        }
        TimeZone dbTimezone = config.getDbTimezone();
        RuleListDTO result = toRuleListDTO(Arrays.asList(source).iterator(), dbTimezone);

        if (fillAndValidRuleList(result,
                programStrategyService.getManagedProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId()),
                programStrategyService.getWritableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId())))
            // return the valid rule list
            return result;

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRuleList(RuleListDTO source, Integer quserId) {
        Assert.notNull(source);
        Assert.notNull(source.getCode());
        Assert.notNull(quserId);

        RuleList target = get(source.getCode());
        boolean isNew = false;
        if (target == null) {
            target = RuleList.Factory.newInstance();
            target.setRuleListCd(source.getCode());
            isNew = true;
        }

        // DTO -> Entity
        beanToEntity(source, target, quserId, config.getDbTimezone());

        // Save it
        if (isNew) {
            getSession().save(target);
        } else {
            getSession().update(target);
        }

        // Save rules
        final List<String> rulesCdsToRemove = DaliBeans.collectProperties(target.getRules(), "ruleCd");

        // Save rules
        if (CollectionUtils.isNotEmpty(source.getControlRules())) {
            source.getControlRules().forEach(controlRule -> {
                ruleDao.save(controlRule, source.getCode());
                rulesCdsToRemove.remove(controlRule.getCode());
                if (!controlRule.isPreconditionsEmpty()) {
                    for (PreconditionRuleDTO preconditionRule : controlRule.getPreconditions()) {
                        rulesCdsToRemove.remove(preconditionRule.getBaseRule().getCode());
                        rulesCdsToRemove.remove(preconditionRule.getUsedRule().getCode());
                    }
                }
            });
        }

        getSession().flush();
        getSession().clear();

        // remove unused rules
        if (CollectionUtils.isNotEmpty(rulesCdsToRemove)) {
            ruleDao.removeByCds(Beans.asStringArray(rulesCdsToRemove));
            // flush again because another get or load can read the removes rules
            getSession().flush();
            getSession().clear();
        }
    }

    private boolean fillAndValidRuleList(RuleListDTO ruleList, Set<String> managedProgramCodes, Set<String> writableProgramCodes) {

        // get programs
        List<String> programCodes = getProgramCodesByRuleListCode(ruleList.getCode());

        if (CollectionUtils.isEmpty(programCodes)) {
            // skip this rule list
            return false;
        }

        // Check program privilege
        boolean canRead = programCodes.stream().anyMatch(writableProgramCodes::contains);
        if (!canRead) {
            if (log.isWarnEnabled()) {
                log.warn(String.format("The rule list '%s' is not compatible (writable program list is empty for the current user)", ruleList.getCode()));
            }
            // skip this rule list
            return false;
        }

        // the current user is not manager of all programs, set this rule list as read only
        boolean canWrite = SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.ADMIN) || managedProgramCodes.containsAll(programCodes);
        ruleList.setReadOnly(!canWrite);

        // add programs
        ruleList.setPrograms(programStrategyService.getProgramsByCodes(programCodes));

        // add services
        ruleList.setDepartments(getControlledDepartmentsByRuleListCode(ruleList.getCode()));

        // valid services
//        if (ruleList.isDepartmentsEmpty()) {
//            if (log.isWarnEnabled()) {
//                log.warn(String.format("The rule list '%s' is not compatible (service list is empty)", ruleList.getCode()));
//            }
//            // skip this rule list
//            return false;
//        }

        // add rules
        MutableBoolean incompatibleRule = new MutableBoolean(false);
        ruleList.addAllControlRules(ruleDao.getRulesByRuleListCode(ruleList.getCode(), false /*active and non-active*/, incompatibleRule));

        // add preconditioned rules
        ruleList.addAllControlRules(ruleDao.getPreconditionedRulesByRuleListCode(ruleList.getCode(), false, incompatibleRule));

        // valid rules
        if (ruleList.isControlRulesEmpty()) {
            if (log.isWarnEnabled()) {
                log.warn(String.format("The rule list '%s' is not compatible (rule list is empty)", ruleList.getCode()));
            }
            // skip this rule list
            return false;
        }
        // set the rule list as read only if at least one rule is not compatible
        if (incompatibleRule.booleanValue()) ruleList.setReadOnly(true);

        return true;
    }

    private List<String> getProgramCodesByRuleListCode(String ruleListCode) {
        Assert.notBlank(ruleListCode);

        return queryListTyped("programCodesByRuleListCode",
                "ruleListCode", StringType.INSTANCE, ruleListCode);
    }

    /**
     * <p>getControlledDepartmentsByRuleListCode.</p>
     *
     * @param ruleListCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    private List<DepartmentDTO> getControlledDepartmentsByRuleListCode(String ruleListCode) {
        Assert.notBlank(ruleListCode);

        List<Integer> depIds = queryListTyped("departmentIdsByRuleListCode",
                "ruleListCode", StringType.INSTANCE, ruleListCode);

        List<DepartmentDTO> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(depIds)) {
            for (Integer depId : new HashSet<>(depIds)) {
                result.add(departmentDao.getDepartmentById(depId));
            }
        }
        return result;
    }

    // INTERNAL METHODS

    private List<RuleListDTO> toRuleListDTOs(Iterator<Object[]> it) {

        Set<String> managedProgramCodes = programStrategyService.getManagedProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());
        Set<String> writableProgramCodes = programStrategyService.getWritableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());

        TimeZone dbTimezone = config.getDbTimezone();
        List<RuleListDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            RuleListDTO ruleList = toRuleListDTO(Arrays.asList(source).iterator(), dbTimezone);

            if (fillAndValidRuleList(ruleList, managedProgramCodes, writableProgramCodes))
                // add the valid rule list to result
                result.add(ruleList);
        }

        return result;
    }

    private RuleListDTO toRuleListDTO(Iterator<Object> source, TimeZone dbTimezone) {
        RuleListDTO result = DaliBeanFactory.newRuleListDTO();
        result.setCode((String) source.next());
        result.setActive(Daos.safeConvertToBoolean(source.next()));

        LocalDate startDate = Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone);
        result.setStartMonth(
            Optional.ofNullable(startDate)
                .map(LocalDate::getMonthValue)
                .flatMap(month -> systemService.getMonths().stream().filter(monthDTO -> monthDTO.getId().equals(month)).findFirst())
                .orElse(null)
        );

        LocalDate endDate = Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone);
        result.setEndMonth(
            Optional.ofNullable(endDate)
                .map(LocalDate::getMonthValue)
                .flatMap(month -> systemService.getMonths().stream().filter(monthDTO -> monthDTO.getId().equals(month)).findFirst())
                .orElse(null)
        );

        result.setDescription((String) source.next());
        result.setStatus(referentialDao.getStatusByCode((String) source.next()));
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        return result;
    }

    private void beanToEntity(RuleListDTO source, RuleList target, int quserId, TimeZone dbTimezone) {

        target.setRuleListDc(source.getDescription());
        target.setRuleListIsActive(Daos.convertToString(source.isActive()));

        // convert month to date
        LocalDate startDate = Optional.ofNullable(source.getStartMonth())
            .map(monthDTO -> LocalDate.of(LocalDate.now().getYear(), monthDTO.getId(), 1))
            .orElse(null);
        target.setRuleListFirstMonth(Dates.convertToDate(startDate, dbTimezone));

        LocalDate endDate = Optional.ofNullable(source.getEndMonth())
            .map(monthDTO -> LocalDate.of(LocalDate.now().getYear(), monthDTO.getId(), 1).plusMonths(1).minusDays(1))
            .orElse(null);
        target.setRuleListLastMonth(Dates.convertToDate(endDate, dbTimezone));

        // update date (always null, as set by server)
        // target.setUpdateDt(newUpdateTimestamp());

        // creation date
        if (target.getRuleListCreationDt() == null) {
            target.setRuleListCreationDt(newCreateDate());
        }

        // manager user
        if (CollectionUtils.isEmpty(target.getQusers())) {
            // add current user as unique
            Quser quser = load(QuserImpl.class, quserId);
            target.setQusers(Sets.newHashSet(quser));
        } else if (DaliBeans.findByProperty(target.getQusers(), "quserId", quserId) == null) {
            // add current user to collection
            Quser quser = load(QuserImpl.class, quserId);
            target.getQusers().add(quser);
        }

        // manager department
        if (CollectionUtils.isEmpty(target.getRespDepartments())) {
            // add current user department as unique
            Quser quser = load(QuserImpl.class, quserId);
            target.setRespDepartments(Sets.newHashSet(quser.getDepartment()));
        } else {
            // add current user's department to collection
            Quser quser = load(QuserImpl.class, quserId);
            if (DaliBeans.findByProperty(target.getRespDepartments(), "depId", quser.getDepartment().getDepId()) == null) {
                target.getRespDepartments().add(quser.getDepartment());
            }
        }

        // programs
        if (source.getPrograms() == null) {
            target.getPrograms().clear();
        } else {
            Daos.replaceEntities(target.getPrograms(),
                    source.getPrograms(),
                    vo -> load(ProgramImpl.class, Objects.requireNonNull(vo).getCode()));
        }

        // do not remove remaining unused programs: links to programs will be automatically deleted when ruleList entity will be saved

        // departments
        if (source.getDepartments() == null) {
            target.getControledDepartments().clear();
        } else {
            Daos.replaceEntities(target.getControledDepartments(),
                    source.getDepartments(),
                    vo -> load(DepartmentImpl.class, Objects.requireNonNull(vo).getId()));
        }

        // status
        target.setStatus(load(StatusImpl.class, StatusCode.ENABLE.getValue()));

    }

}
