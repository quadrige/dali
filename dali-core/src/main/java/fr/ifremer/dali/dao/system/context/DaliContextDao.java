package fr.ifremer.dali.dao.system.context;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.dao.system.filter.DaliFilterDao;
import fr.ifremer.dali.dto.configuration.context.ContextDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>DaliContextDao interface.</p>
 *
 * @author Ludovic
 */
public interface DaliContextDao {

    /** Constant <code>ALL_CONTEXT_CACHE="allContext"</code> */
    String ALL_CONTEXT_CACHE = "all_context";
    /** Constant <code>CONTEXT_BY_ID_CACHE="contextById"</code> */
    String CONTEXT_BY_ID_CACHE = "context_by_id";

    /**
     * <p>getAllContext.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_CONTEXT_CACHE)
    List<ContextDTO> getAllContext();

    /**
     * <p>getContextById.</p>
     *
     * @param contextId a {@link java.lang.Integer} object.
     * @return a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    @Cacheable(value = CONTEXT_BY_ID_CACHE)
    ContextDTO getContextById(Integer contextId);

    /**
     * <p>saveContext.</p>
     *
     * @param context a {@link ContextDTO} object.
     * @return a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    @CacheEvict(value = {
            ALL_CONTEXT_CACHE,
            CONTEXT_BY_ID_CACHE,
            DaliFilterDao.ALL_FILTERS_CACHE}, allEntries = true)
    void saveContext(ContextDTO context);

    /**
     * <p>deleteContexts.</p>
     *
     * @param contextIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_CONTEXT_CACHE,
            CONTEXT_BY_ID_CACHE,
            DaliFilterDao.ALL_FILTERS_CACHE}, allEntries = true)
    void deleteContexts(List<Integer> contextIds);
    
}
