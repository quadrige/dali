package fr.ifremer.dali.config;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.QuadrigeEnumerationDef;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.net.URL;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>DaliConfigurationOption class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public enum DaliConfigurationOption implements ConfigOptionDef {

    /**
     * -------- READ-ONLY OPTIONS -----------
     */
    BASEDIR(
            "dali.basedir",
            n("dali.config.option.basedir.description"),
            "${user.home}/.dali",
            File.class),
    HELP_DIRECTORY(
            "dali.help.directory",
            n("dali.config.option.help.directory.description"),
            "${dali.basedir}/help",
            File.class),
    /*-----------------------------------------------------------------
     *-- DB OPTIONS ---------------------------------------------------
     *-----------------------------------------------------------------*/
    CACHE_ENABLED_AT_STARTUP(
            "dali.persistence.cache.enabled",
            n("dali.persistence.cache.enabled.description"),
            "true",
            Boolean.class,
            false),
    CACHE_CLEAN_AT_STARTUP(
            "dali.persistence.cache.clean",
            n("dali.persistence.cache.clean.description"),
            "false",
            Boolean.class),
    DB_CHECK_CONSTANTS(
            "dali.persistence.checkConstants.enable",
            n("dali.config.option.persistence.checkConstants.description"),
            String.valueOf(false),
            Boolean.class, false),
    VERSION(
            "dali.version",
            n("dali.config.option.version.description"),
            "",
            Version.class),
    SITE_URL(
            "dali.site.url",
            n("dali.config.option.site.url.description"),
            "http://doc.e-is.pro/dali",
            URL.class, false),
    ORGANIZATION_NAME(
            "dali.organizationName",
            n("dali.config.option.organizationName.description"),
            "",
            String.class),
    INCEPTION_YEAR(
            "dali.inceptionYear",
            n("dali.config.option.inceptionYear.description"),
            "2014",
            Integer.class),

    /*------------------------------------------------------------------------
     *-- READ-WRITE OPTIONS --------------------------------------------------
     *------------------------------------------------------------------------*/
    TIMEZONE(
            "dali.timezone",
            n("dali.config.option.timezone.description"),
            "",
            String.class,
            false),
    DB_BACKUP_EXTERNAL_DIRECTORY(
            "dali.persistence.db.backup.external.directory",
            n("dali.config.option.persistence.db.backup.external.directory.description"),
            null,
            File.class,
            false),
    DB_ENUMERATION_RESOURCE(
            "dali.persistence.db.enumeration.path",
            n("dali.config.option.persistence.db.enumeration.path.description"),
            "classpath*:quadrige3-db-enumerations.properties,classpath*:dali-db-enumerations.properties",
            String.class,
            false),
    DB_TIMEZONE(
            "dali.persistence.db.timezone",
            n("dali.config.option.persistence.db.timezone.description"),
            "", // no default, this option must be provided in config file (Mantis #46390)
            String.class,
            false),
    DB_VALIDATION_BULK_SIZE(
            "dali.persistence.db.validation.bulkSize",
            n("dali.config.option.persistence.db.validation.bulkSize.description"),
            "100",
            Integer.class,
            false),
    UNKNOWN_RECORDER_DEPARTMENT(
            "dali.department.recorder.default.id",
            n("dali.config.option.department.recorder.default.id.description"),
            "0",
            String.class,
            false),

    // UI RELATED
    UI_CONFIG_FILE(
            "dali.ui.config.file",
            n("dali.config.option.ui.config.file.description"),
            "${quadrige3.data.directory}/daliUI.xml",
            File.class,
            false),
    UI_RECOMMENDED_WIDTH(
            "dali.ui.recommended.width",
            n("dali.config.option.ui.recommended.width.description"),
            String.valueOf(1280),
            Integer.class,
            false),
    UI_RECOMMENDED_HEIGHT(
            "dali.ui.recommended.height",
            n("dali.config.option.ui.recommended.height.description"),
            String.valueOf(1024),
            Integer.class,
            false),
    TABLES_CHECKBOX(
            "dali.ui.table.showCheckbox",
            n("dali.config.option.ui.table.showCheckbox.description"),
            String.valueOf(false),
            Boolean.class,
            false),
    AUTO_POPUP_NUMBER_EDITOR(
            "dali.ui.autoPopupNumberEditor",
            n("dali.config.option.ui.autoPopupNumberEditor.description"),
            String.valueOf(false),
            Boolean.class,
            false),
    SHOW_NUMBER_EDITOR_BUTTON(
            "dali.ui.showNumberEditorButton",
            n("dali.config.option.ui.showNumberEditorButton.description"),
            String.valueOf(true),
            Boolean.class,
            false),
    COLOR_ROW_READ_ONLY(
            "dali.ui.color.rowReadOnly",
            n("dali.config.option.ui.color.rowReadOnly.description"),
            new Color(230, 230, 230).toString(),
            Color.class,
            false),
    COLOR_ROW_INVALID(
            "dali.ui.color.rowInvalid",
            n("dali.config.option.ui.color.rowInvalid.description"),
            new Color(255, 204, 153).toString(),
            Color.class,
            false),
    COLOR_CELL_WITH_VALUE(
            "dali.ui.color.cellWithValue",
            n("dali.config.option.ui.color.cellWithValue.description"),
            new Color(128, 255, 128).toString(),
            Color.class,
            false),
    COLOR_ALTERNATE_ROW(
            "dali.ui.color.alternateRow",
            n("dali.config.option.ui.color.alternateRow.description"),
            new Color(255, 255, 232).toString(),
            Color.class,
            false),
    COLOR_SELECTED_ROW(
            "dali.ui.color.selectedRow",
            n("dali.config.option.ui.color.selectedRow.description"),
            new Color(0, 0, 128).toString(),
            Color.class,
            false),
    COLOR_BLOCKING_LAYER(
            "dali.ui.color.blockingLayer",
            n("dali.config.option.ui.color.blockingLayer.description"),
            Color.BLACK.toString(),
            Color.class,
            false),
    COLOR_COMPUTED_WEIGHTS(
            "dali.ui.color.computedWeights",
            n("dali.config.option.ui.color.computedWeights.description"),
            Color.BLUE.toString(),
            Color.class,
            false),
    COLOR_SELECTED_CELL(
            "dali.ui.color.selectedCell",
            n("dali.config.option.ui.color.selectedCell.description"),
            Color.BLACK.toString(),
            Color.class,
            false),
    COLOR_SELECTION_PANEL_BACKGROUND(
            "dali.ui.color.selectionPanelBackground",
            n("dali.config.option.ui.color.selectionPanelBackground.description"),
            new Color(144, 211, 253).toString(),
            Color.class,
            false),
    COLOR_CONTEXT_PANEL_BACKGROUND(
            "dali.ui.color.contextPanelBackground",
            n("dali.config.option.ui.color.contextPanelBackground.description"),
            new Color(136, 208, 238).toString(),
            Color.class,
            false),
    COLOR_EDITION_PANEL_BACKGROUND(
            "dali.ui.color.editionPanelBackground",
            n("dali.config.option.ui.color.editionPanelBackground.description"),
            new Color(210, 237, 254).toString(),
            Color.class,
            false),
    COLOR_EDITION_PANEL_BORDER(
            "dali.ui.color.editionPanelBorder",
            n("dali.config.option.ui.color.editionPanelBorder.description"),
            new Color(0, 144, 188).toString(),
            Color.class,
            false),
    COLOR_THEMATIC_LABEL(
            "dali.ui.color.thematicLabel",
            n("dali.config.option.ui.color.thematicLabel.description"),
            new Color(0, 0, 128).toString(),
            Color.class,
            false),
    COLOR_HIGHLIGHT_BUTTON_BORDER(
            "dali.ui.color.highlightButtonBorder",
            n("dali.config.option.ui.color.highlightButtonBorder.description"),
            new Color(209, 0, 209).toString(),
            Color.class,
            false),
    COLOR_UNUSED_EDITOR_BACKGROUND(
            "dali.ui.color.unusedEditorBackground",
            n("dali.config.option.ui.color.unusedEditorBackground.description"),
            new Color(245, 245, 245).toString(),
            Color.class,
            false),
    SHORTCUT_CLOSE_POPUP(
            "dali.ui.shortcut.closePopup",
            n("dali.config.option.ui.shortcut.closePopup.description"),
            "alt pressed F",
            KeyStroke.class,
            false),
    DATE_FORMAT(
            "dali.ui.dateFormat",
            n("dali.config.option.ui.dateFormat.description"),
            "dd/MM/yyyy",
            String.class,
            false),
    DATE_TIME_FORMAT(
            "dali.ui.dateTimeFormat",
            n("dali.config.option.ui.dateTimeFormat.description"),
            "dd/MM/yyyy HH:mm",
            String.class,
            false),
    LAST_SURVEY_ID(
            "dali.lastSurveyId",
            n("dali.config.option.lastSurveyId.description"),
            null,
            Integer.class,
            false),
    LAST_CONTEXT_ID(
            "dali.lastContextId",
            n("dali.config.option.lastContextId.description"),
            null,
            Integer.class,
            false),

    /* CONSTANT ENUMERATIONS */
    UNIT_ID_NO_UNIT(
            "dali.unit.id.noUnit",
            n("dali.config.option.unit.id.noUnit.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "UnitId.NO_UNIT"), // 99
            Integer.class,
            false),
    PMFM_ID_SURVEY_CALCULATED_LENGTH(
            "dali.pmfm.id.surveyCalculatedLength",
            n("dali.config.option.pmfm.id.surveyCalculatedLength.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmId.SURVEY_CALCULATED_LENGTH"),
            Integer.class,
            false),
    PMFM_IDS_CALCULATED_TRAWL_AREA(
            "dali.pmfm.ids.calculatedTrawlArea",
            n("dali.config.option.pmfm.ids.calculatedTrawlArea.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmIds.CALCULATED_TRAWL_AREA"),
            String.class,
            false),
    UNIT_IDS_COMPATIBLE_LENGTH_BY_AREA(
            "dali.unit.ids.compatibleLengthByArea",
            n("dali.config.option.unit.ids.compatibleLengthByArea.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "unitIds.COMPATIBLE_LENGTH_BY_AREA"),
            String.class,
            false),

    /* ------------ TRANSCRIBING --------------*/
    TRANSCRIBING_ITEM_TYPE_LB_PMFM_NM(
            "dali.transcribingItemType.label.pmfmNm",
            n("dali.config.option.transcribingItemType.label.pmfmNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_PMFM_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_PMFM_EXTRACTION(
            "dali.transcribingItemType.label.pmfmExtraction",
            n("dali.config.option.transcribingItemType.label.pmfmExtraction.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_PMFM_EXTRACTION"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_QUALITATIVE_VALUE_NM(
            "dali.transcribingItemType.label.qualitativeValueNm",
            n("dali.config.option.transcribingItemType.label.qualitativeValueNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_QUALITATIVE_VALUE_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_QUALITATIVE_VALUE_EXTRACTION(
            "dali.transcribingItemType.label.qualitativeValueExtraction",
            n("dali.config.option.transcribingItemType.label.qualitativeValueExtraction.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_QUALITATIVE_VALUE_EXTRACTION"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_SAMPLING_EQUIPMENT_NM(
            "dali.transcribingItemType.label.samplingEquipmentNm",
            n("dali.config.option.transcribingItemType.label.samplingEquipmentNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_SAMPLING_EQUIPMENT_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_SAMPLING_EQUIPMENT_EXTRACTION(
            "dali.transcribingItemType.label.samplingEquipmentExtraction",
            n("dali.config.option.transcribingItemType.label.samplingEquipmentExtraction.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_SAMPLING_EQUIPMENT_EXTRACTION"),
            String.class,
            false),

    TRANSCRIBING_ITEM_TYPE_LB_PARAMETER_NM(
            "dali.transcribingItemType.label.parameterNm",
            n("dali.config.option.transcribingItemType.label.parameterNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_PARAMETER_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_PARAMETER_CD(
            "dali.transcribingItemType.label.parameterCd",
            n("dali.config.option.transcribingItemType.label.parameterCd.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_PARAMETER_CD"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_MATRIX_NM(
            "dali.transcribingItemType.label.matrixNm",
            n("dali.config.option.transcribingItemType.label.matrixNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_MATRIX_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_FRACTION_NM(
            "dali.transcribingItemType.label.fractionNm",
            n("dali.config.option.transcribingItemType.label.fractionNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_FRACTION_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_METHOD_NM(
            "dali.transcribingItemType.label.methodNm",
            n("dali.config.option.transcribingItemType.label.methodNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_METHOD_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_UNIT_NM(
            "dali.transcribingItemType.label.unitNm",
            n("dali.config.option.transcribingItemType.label.unitNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_UNIT_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_UNIT_SYMBOL(
            "dali.transcribingItemType.label.unitSymbol",
            n("dali.config.option.transcribingItemType.label.unitSymbol.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_UNIT_SYMBOL"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_MON_LOC_NM(
            "dali.transcribingItemType.label.monLocNm",
            n("dali.config.option.transcribingItemType.label.monLocNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_MON_LOC_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_QUAL_FLAG_NM(
            "dali.transcribingItemType.label.qualFlagNm",
            n("dali.config.option.transcribingItemType.label.qualFlagNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_QUAL_FLAG_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_TAX_LEVEL_NM(
            "dali.transcribingItemType.label.taxLevelNm",
            n("dali.config.option.transcribingItemType.label.taxLevelNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_TAX_LEVEL_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_STATUS_NM(
            "dali.transcribingItemType.label.statusNm",
            n("dali.config.option.transcribingItemType.label.statusNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.DALI_STATUS_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_WORMS(
            "dali.transcribingItemType.label.worms",
            n("dali.config.option.transcribingItemType.label.worms.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.WORMS"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_TAXREF(
            "dali.transcribingItemType.label.taxRef",
            n("dali.config.option.transcribingItemType.label.taxRef.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.TAXREF"),
            String.class,
            false),


    /* ------------ ALTERNATIVE_TAXON --------------*/
//    ALTERNATIVE_TAXON_ORIGIN_TAXREF("dali.alternativeTaxonOrigin.taxRef",
//            n("dali.config.option.alternativeTaxonOrigin.taxRef.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "AlternTaxonOriginCd.TAXREF"),
//            String.class,
//            false),
//    ALTERNATIVE_TAXON_ORIGIN_WORMS("dali.alternativeTaxonOrigin.worms",
//            n("dali.config.option.alternativeTaxonOrigin.worms.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "AlternTaxonOriginCd.WORMS"),
//            String.class,
//            false),

    // EXTRACTION
    EXTRACTION_FILE_EXTENSION(
            "dali.extraction.file.extension",
            n("dali.config.option.extraction.file.extension.description"),
            "dat",
            String.class,
            false),
    EXTRACTION_RESULT_FILE_EXTENSION(
            "dali.extraction.result.file.extension",
            n("dali.config.option.extraction.result.file.extension.description"),
            "csv",
            String.class,
            false),
    EXTRACTION_FILE_TYPE_CODE(
            "dali.extraction.fileType.code",
            n("dali.config.option.extraction.fileType.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "fileTypeCd.DALI_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_TABLE_TYPE_CODE(
            "dali.extraction.tableType.code",
            n("dali.config.option.extraction.tableType.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "tableTypeCd.DALI_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_GROUP_TYPE_PMFM_CODE(
            "dali.extraction.groupTypePmfm.code",
            n("dali.config.option.extraction.groupTypePmfm.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "groupTypePmfmCd.DALI_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_PROJECTION_SYSTEM_CODE(
            "dali.extraction.projectionSystem.code",
            n("dali.config.option.extraction.projectionSystem.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "projectionSystemCd.DALI_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_DEFAULT_ORDER_ITEM_TYPE_CODE(
            "dali.extraction.default.orderItemType.code",
            n("dali.config.option.extraction.default.orderItemType.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "orderItemTypeCd.DALI_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_IGNORE_UNIT_IDS(
            "dali.extraction.ignore.unit.ids",
            n("dali.config.option.extraction.ignore.unit.ids.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "unitId.DALI_EXTRACTION_EXCLUSION"),
            String.class,
            false),

    SISMER_WEBSITE_URL(
            "dali.campaign.sismer.url",
            n("dali.config.option.campaign.sismer.url.description"),
            "http://dx.doi.org/",
            URL.class,
            false),

    //////////////////////////////
    // MAP HANDLING AND CONTROL //
    //////////////////////////////
    CONTROL_SURVEY_LOCATION_MINIMUM_DISTANCE_IN_METER(
            "dali.control.survey.location.minDistance.meter",
            n("dali.config.option.control.survey.location.minDistance.meter.description"),
            "10",
            Integer.class,
            false),
    MAP_BASE_LAYER_DEFAULT(
            "dali.survey.map.baseLayer.default",
            n("dali.config.option.survey.map.baseLayer.default.description"),
            "WMTS_SEXTANT", //MapMode.SEXTANT_WMTS_MAP_MODE.getCode()
            String.class,
            false),
    MAP_PROJECTION_CODE(
            "dali.survey.map.projection.code",
            n("dali.config.option.survey.map.projection.code.description"),
            "EPSG:3857", //MapProjection.WGS84_PSEUDO_MERCATOR.getCode()
            String.class,
            false),
    MAP_SEXTANT_WMS_URL(
            "dali.survey.map.sextantWMS.url",
            n("dali.config.option.survey.map.sextantWMS.url.description"),
            "http://www.ifremer.fr/services/wms1",
            String.class,
            false),
    MAP_SEXTANT_WMTS_URL(
            "dali.survey.map.sextantWMTS.url",
            n("dali.config.option.survey.map.sextantWMTS.url.description"),
            "https://sextant.ifremer.fr/geowebcache/service/wmts",
            String.class,
            false),
    MAP_OSM_URL(
            "dali.survey.map.openStreetMap.url",
            n("dali.config.option.survey.map.openStreetMap.url.description"),
            "https://tile.openstreetmap.org/",
            String.class,
            false),
    MAP_OTM_URL(
            "dali.survey.map.openTopoMap.url",
            n("dali.config.option.survey.map.openTopoMap.url.description"),
            "https://tile.opentopomap.org/",
            String.class,
            false),
    MAP_CARTO_BASE_URL(
            "dali.survey.map.cartoBase.url",
            n("dali.config.option.survey.map.cartoBase.url.description"),
            "https://basemaps.cartocdn.com/rastertiles/voyager/",
            String.class,
            false),
    MAP_SATELLITE_URL(
            "dali.survey.map.satellite.url",
            n("dali.config.option.survey.map.satellite.url.description"),
            "https://api.maptiler.com/tiles/satellite/",
            String.class,
            false),
    MAP_MAX_SELECTION(
            "dali.survey.map.maxSelection",
            n("dali.config.option.survey.map.maxSelection.description"),
            "50",
            Integer.class,
            false),

    // OTHER OPTIONS

    SAMPLING_OPERATION_AUTO_LABEL_ENABLE(
            "dali.samplingOperation.autoLabel.enable",
            n("dali.config.option.samplingOperation.autoLabel.enable.description"),
            String.valueOf(false),
            Boolean.class,
            false),
    GRID_INITIALIZATION_MAX_COMBINATION_COUNT(
            "dali.measurements.gridInitialization.maxCombinationCount",
            n("dali.config.option.measurements.gridInitialization.maxCombinationCount.description"),
            "1000",
            Integer.class,
            false);

    /**
     * Configuration key.
     */
    private final String key;

    /**
     * I18n key of option description
     */
    private final String description;

    /**
     * Type of option
     */
    private final Class<?> type;

    /**
     * Default value of option.
     */
    private String defaultValue;

    /**
     * Flag to not keep option value on disk
     */
    private final boolean isTransient;

    /**
     * Flag to not allow option value modification
     */
    private final boolean isFinal;

    DaliConfigurationOption(String key,
                            String description,
                            String defaultValue,
                            Class<?> type,
                            boolean isTransient) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isTransient = isTransient;
        this.isFinal = isTransient;
    }

    DaliConfigurationOption(String key,
                            String description,
                            String defaultValue,
                            Class<?> type) {
        this(key, description, defaultValue, type, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getKey() {
        return key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<?> getType() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTransient() {
        return isTransient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFinal() {
        return isFinal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTransient(boolean bln) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFinal(boolean bln) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
