package fr.ifremer.dali.config;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.common.synchro.config.SynchroConfigurationOption;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.map.MapMode;
import fr.ifremer.dali.map.MapProjection;
import fr.ifremer.quadrige3.core.config.QuadrigeConfigurationOption;
import fr.ifremer.quadrige3.core.config.QuadrigeConfigurations;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfiguration;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfigurationOption;
import fr.ifremer.quadrige3.core.dao.technical.enumeration.Enums;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.DateType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.version.Version;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Dali Configuration
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public final class DaliConfiguration extends QuadrigeCoreConfiguration {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(DaliConfiguration.class);
    /**
     * Singleton pattern
     */
    private static DaliConfiguration instance;

    /**
     * <p>Constructor for DaliConfiguration.</p>
     *
     * @param applicationConfig a {@link org.nuiton.config.ApplicationConfig} object.
     */
    public DaliConfiguration(ApplicationConfig applicationConfig) {
        super(applicationConfig);
    }

    /**
     * <p>Constructor for DaliConfiguration.</p>
     *
     * @param file a {@link java.lang.String} object.
     * @param args a {@link java.lang.String} object.
     */
    public DaliConfiguration(String file, String... args) {
        super(file, args);

        // Verify base directory
        File daliBasedir = getBaseDirectory();
        if (daliBasedir == null) {
            daliBasedir = new File("");
        }
        if (!daliBasedir.isAbsolute()) {
            daliBasedir = new File(daliBasedir.getAbsolutePath());
        }
        if ("..".equals(daliBasedir.getName())) {
            daliBasedir = daliBasedir.getParentFile().getParentFile();
        }
        if (".".equals(daliBasedir.getName())) {
            daliBasedir = daliBasedir.getParentFile();
        }
        if (LOG.isInfoEnabled()) {
            LOG.info("Application basedir: " + daliBasedir);
        }
        applicationConfig.setOption(
                DaliConfigurationOption.BASEDIR.getKey(),
                daliBasedir.getAbsolutePath());

        // Init timezone (see mantis Allegro-ObsDeb #24623)
        initTimeZone();
    }

    @Override
    public File getBaseDirectory() {
        return applicationConfig.getOptionAsFile(DaliConfigurationOption.BASEDIR.getKey());
    }

    /**
     * <p>Getter for the field <code>instance</code>.</p>
     *
     * @return a {@link DaliConfiguration} object.
     */
    public static DaliConfiguration getInstance() {
        return instance;
    }

    /**
     * <p>Setter for the field <code>instance</code>.</p>
     *
     * @param instance a {@link DaliConfiguration} object.
     */
    public static void setInstance(DaliConfiguration instance) {
        DaliConfiguration.instance = instance;
        QuadrigeCoreConfiguration.setInstance(instance);
    }

    @Override
    protected void overrideExternalModulesDefaultOptions() {
        super.overrideExternalModulesDefaultOptions();

        // Override quadrige3 BASE_DIRECTORY
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.BASEDIR.getKey(),
                String.format("${%s}", DaliConfigurationOption.BASEDIR.getKey()));

        // Override Sites
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.SITE_URL.getKey(),
                String.format("${%s}", DaliConfigurationOption.SITE_URL.getKey()));
        // Remap :
        //  - quadrige3.update.XXX to dali.update.XXX
        //  - quadrige3.install.XXX to dali.install.XXX
        //  - quadrige3.authentication.XXX to dali.authentication.XXX
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.update",
                DaliConfigurationOption.values(),
                "dali.update");
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.install",
                DaliConfigurationOption.values(),
                "dali.install");
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.authentication",
                DaliConfigurationOption.values(),
                "dali.authentication");

        // Override Hibernate queries file
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.HIBERNATE_CLIENT_QUERIES_FILE.getKey(),
                "dali-queries.hbm.xml");

        // Override enumeration files
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.DB_ENUMERATION_RESOURCE.getKey(),
                String.format("${%s}", DaliConfigurationOption.DB_ENUMERATION_RESOURCE.getKey()));

        // Override Db timezone
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.DB_TIMEZONE.getKey(),
                String.format("${%s}", DaliConfigurationOption.DB_TIMEZONE.getKey()));
        applicationConfig.setDefaultOption(
                SynchroConfigurationOption.DB_TIMEZONE.getKey(),
                String.format("${%s}", DaliConfigurationOption.DB_TIMEZONE.getKey()));

        // Override ALL synchronization options
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "synchro",
                DaliConfigurationOption.values(),
                "dali.synchro");
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.synchro",
                DaliConfigurationOption.values(),
                "dali.synchro");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getApplicationName() {
        return t("dali.application.name");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Version getVersion() {
        return applicationConfig.getOptionAsVersion(DaliConfigurationOption.VERSION.getKey());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public KeyStroke getShortcutClosePopup() {
        return applicationConfig.getOptionAsKeyStroke(DaliConfigurationOption.SHORTCUT_CLOSE_POPUP.getKey());
    }

    /**
     * <p>getHelpResourceWithLocale.</p>
     *
     * @param value a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String getHelpResourceWithLocale(String value) {
        File directory = new File(getHelpDirectory(),
                getI18nLocale().getLanguage());
        File result = new File(directory, value);
        return result.toString();
    }

    /**
     * Initialization default timezone, from configuration (mantis #24623)
     */
    private void initTimeZone() {

        String timeZone = applicationConfig.getOption(DaliConfigurationOption.TIMEZONE.getKey());
        if (StringUtils.isNotBlank(timeZone)) {
            if (LOG.isInfoEnabled()) {
                LOG.info(String.format("Using timezone [%s]", timeZone));
            }
            TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
            System.setProperty("user.timezone", timeZone);
        } else if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Using default timezone [%s]", System.getProperty("user.timezone")));
        }

        // Warn only and use user timezone if dali.persistence.db.timezone is not set (Mantis #46390)
        String dbTimeZoneOption = applicationConfig.getOption(DaliConfigurationOption.DB_TIMEZONE.getKey());
        TimeZone dbTimeZone;
        if (StringUtils.isBlank(dbTimeZoneOption)) {
            LOG.warn("Timezone for database is not set in configuration, now using user timezone");
            dbTimeZone = TimeZone.getDefault();
        } else {
            dbTimeZone = TimeZone.getTimeZone(dbTimeZoneOption);
        }
        if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Using timezone [%s] for database", dbTimeZone.toZoneId()));
        }
        DateType.setTimeZone(dbTimeZone);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    //------------------------------------------------------------------------//
    //--- Data context-related ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * <p>getLastSurveyId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getLastSurveyId() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.LAST_SURVEY_ID.getKey());
    }

    /**
     * <p>setLastSurveyId.</p>
     *
     * @param lastSurveyId a {@link java.lang.Integer} object.
     */
    public void setLastSurveyId(Integer lastSurveyId) {
        applicationConfig.setOption(DaliConfigurationOption.LAST_SURVEY_ID.getKey(), lastSurveyId != null ? lastSurveyId.toString() : "");
    }

    /**
     * <p>getLastContextId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getLastContextId() {
        int lastContextId = applicationConfig.getOptionAsInt(DaliConfigurationOption.LAST_CONTEXT_ID.getKey());
        return lastContextId == 0 ? null : lastContextId;
    }

    /**
     * <p>setLastContextId.</p>
     *
     * @param contextId a {@link java.lang.Integer} object.
     */
    public void setLastContextId(Integer contextId) {
        String value = "";
        if (contextId != null) {
            value = contextId.toString();
        }
        applicationConfig.setOption(DaliConfigurationOption.LAST_CONTEXT_ID.getKey(), value);
    }

    //------------------------------------------------------------------------//
    //--- DB-related ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * <p>getDbBackupExternalDirectory.</p>
     *
     * @return a {@link java.io.File} object.
     */
    public File getDbBackupExternalDirectory() {
        return applicationConfig.getOptionAsFile(DaliConfigurationOption.DB_BACKUP_EXTERNAL_DIRECTORY.getKey());
    }

    /**
     * <p>isDbCheckConstantsEnable.</p>
     *
     * @return a boolean.
     */
    public boolean isDbCheckConstantsEnable() {
        return applicationConfig.getOptionAsBoolean(DaliConfigurationOption.DB_CHECK_CONSTANTS.getKey());
    }

    /**
     * <p>setDbCheckConstantsEnable.</p>
     *
     * @param check a boolean.
     */
    public void setDbCheckConstantsEnable(boolean check) {
        applicationConfig.setOption(DaliConfigurationOption.DB_CHECK_CONSTANTS.getKey(), Boolean.toString(check));
    }

    /**
     * <p>getDbDirectory.</p>
     *
     * @return a {@link File} object.
     */
    public TimeZone getDbTimezone() {
        String timezone = applicationConfig.getOption(DaliConfigurationOption.DB_TIMEZONE.getKey());
        return StringUtils.isNotBlank(timezone) ? TimeZone.getTimeZone(timezone) : TimeZone.getDefault();
    }

    /**
     * <p>isCacheEnabledAtStartup.</p>
     *
     * @return a boolean.
     */
    public boolean isCacheEnabledAtStartup() {
        return applicationConfig.getOptionAsBoolean(DaliConfigurationOption.CACHE_ENABLED_AT_STARTUP.getKey());
    }

    /**
     * <p>isCleanCacheAtStartup.</p>
     *
     * @return a boolean.
     */
    public boolean isCleanCacheAtStartup() {
        return applicationConfig.getOptionAsBoolean(DaliConfigurationOption.CACHE_CLEAN_AT_STARTUP.getKey());
    }

    /**
     * <p>getSiteUrl.</p>
     *
     * @return {@link DaliConfigurationOption#SITE_URL} value
     */
    public URL getSiteUrl() {
        return applicationConfig.getOptionAsURL(DaliConfigurationOption.SITE_URL.getKey());
    }

    /**
     * <p>getOrganizationName.</p>
     *
     * @return {@link DaliConfigurationOption#ORGANIZATION_NAME} value
     */
    public String getOrganizationName() {
        return applicationConfig.getOption(DaliConfigurationOption.ORGANIZATION_NAME.getKey());
    }

    /**
     * <p>getInceptionYear.</p>
     *
     * @return {@link DaliConfigurationOption#INCEPTION_YEAR} value
     */
    public int getInceptionYear() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.INCEPTION_YEAR.getKey());
    }

    //------------------------------------------------------------------------//
    //--- extraction-related --------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * <p>getExtractionResultFileExtension.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExtractionFileExtension() {
        return applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_FILE_EXTENSION.getKey());
    }

    /**
     * <p>getExtractionResultFileExtension.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExtractionResultFileExtension() {
        return applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_RESULT_FILE_EXTENSION.getKey());
    }

    /**
     * <p>getExtractionFileTypeCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExtractionFileTypeCode() {
        return applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_FILE_TYPE_CODE.getKey());
    }

    /**
     * <p>getExtractionGroupTypePmfmCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExtractionGroupTypePmfmCode() {
        return applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_GROUP_TYPE_PMFM_CODE.getKey());
    }

    /**
     * <p>getExtractionTableTypeCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExtractionTableTypeCode() {
        return applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_TABLE_TYPE_CODE.getKey());
    }

    /**
     * <p>getExtractionProjectionSystemCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExtractionProjectionSystemCode() {
        return applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_PROJECTION_SYSTEM_CODE.getKey());
    }

    /**
     * <p>getExtractionDefaultOrderItemTypeCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getExtractionDefaultOrderItemTypeCode() {
        return applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_DEFAULT_ORDER_ITEM_TYPE_CODE.getKey());
    }

    /**
     * <p>getExtractionUnitIdsToIgnore.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Integer> getExtractionUnitIdsToIgnore() {
        List<Integer> ids = Lists.newArrayList();
        String excludeUnitIds = applicationConfig.getOption(DaliConfigurationOption.EXTRACTION_IGNORE_UNIT_IDS.getKey());
        for (String id : DaliBeans.split(excludeUnitIds, ",")) {
            if (StringUtils.isNumeric(id)) {
                ids.add(Integer.valueOf(id));
            }
        }
        return ids;
    }

    //------------------------------------------------------------------------//
    //--- UI-related ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * <p>getUIConfigFile.</p>
     *
     * @return {@link DaliConfigurationOption#UI_CONFIG_FILE} value
     */
    public File getUIConfigFile() {
        return applicationConfig.getOptionAsFile(DaliConfigurationOption.UI_CONFIG_FILE.getKey());
    }

    /**
     * <p>getUIRecommendedWidth.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getUIRecommendedWidth() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.UI_RECOMMENDED_WIDTH.getKey());
    }

    /**
     * <p>getUIRecommendedHeight.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getUIRecommendedHeight() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.UI_RECOMMENDED_HEIGHT.getKey());
    }

    /**
     * <p>isShowTableCheckbox.</p>
     *
     * @return {@link DaliConfigurationOption#TABLES_CHECKBOX} value
     */
    public boolean isShowTableCheckbox() {
        return applicationConfig.getOptionAsBoolean(DaliConfigurationOption.TABLES_CHECKBOX.getKey());
    }

    /**
     * <p>isAutoPopupNumberEditor.</p>
     *
     * @return a boolean.
     */
    public boolean isAutoPopupNumberEditor() {
        return applicationConfig.getOptionAsBoolean(DaliConfigurationOption.AUTO_POPUP_NUMBER_EDITOR.getKey());
    }

    /**
     * <p>isShowNumberEditorButton.</p>
     *
     * @return a boolean.
     */
    public boolean isShowNumberEditorButton() {
        return applicationConfig.getOptionAsBoolean(DaliConfigurationOption.SHOW_NUMBER_EDITOR_BUTTON.getKey());
    }

    /**
     * <p>getColorSelectedCell.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorSelectedCell() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_SELECTED_CELL.getKey());
    }

    /**
     * <p>getColorRowInvalid.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorRowInvalid() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_ROW_INVALID.getKey());
    }

    /**
     * <p>getColorRowReadOnly.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorRowReadOnly() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_ROW_READ_ONLY.getKey());
    }

    /**
     * <p>getColorCellWithValue.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorCellWithValue() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_CELL_WITH_VALUE.getKey());
    }

    /**
     * <p>getColorComputedWeights.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorComputedWeights() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_COMPUTED_WEIGHTS.getKey());
    }

    /**
     * <p>getColorComputedRow.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorComputedRow() {
        // same color as COLOR_COMPUTED_WEIGHTS
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_COMPUTED_WEIGHTS.getKey());
    }

    /**
     * <p>getColorBlockingLayer.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorBlockingLayer() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_BLOCKING_LAYER.getKey());
    }

    /**
     * <p>getColorAlternateRow.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorAlternateRow() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_ALTERNATE_ROW.getKey());
    }

    /**
     * <p>getColorSelectedRow.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorSelectedRow() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_SELECTED_ROW.getKey());
    }

    /**
     * <p>getColorSelectionPanelBackground.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorSelectionPanelBackground() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_SELECTION_PANEL_BACKGROUND.getKey());
    }

    /**
     * <p>getColorContextPanelBackground.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorContextPanelBackground() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_CONTEXT_PANEL_BACKGROUND.getKey());
    }

    /**
     * <p>getColorEditionPanelBackground.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorEditionPanelBackground() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_EDITION_PANEL_BACKGROUND.getKey());
    }

    /**
     * <p>getColorEditionPanelBorder.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorEditionPanelBorder() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_EDITION_PANEL_BORDER.getKey());
    }

    /**
     * <p>getColorThematicLabel.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorThematicLabel() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_THEMATIC_LABEL.getKey());
    }

    /**
     * <p>getColorHighlightButtonBorder.</p>
     *
     * @return a {@link java.awt.Color} object.
     */
    public Color getColorHighlightButtonBorder() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_HIGHLIGHT_BUTTON_BORDER.getKey());
    }

    public Color getColorUnusedEditorBackground() {
        return applicationConfig.getOptionAsColor(DaliConfigurationOption.COLOR_UNUSED_EDITOR_BACKGROUND.getKey());
    }

    /**
     * <p>getShortCut.</p>
     *
     * @param actionName a {@link java.lang.String} object.
     * @return a {@link javax.swing.KeyStroke} object.
     */
    public KeyStroke getShortCut(String actionName) {
        return applicationConfig.getOptionAsKeyStroke("dali.ui." + actionName);
    }

    /**
     * <p>getDateFormat.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDateFormat() {
        return applicationConfig.getOption(DaliConfigurationOption.DATE_FORMAT.getKey());
    }

    /**
     * <p>getDateTimeFormat.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDateTimeFormat() {
        return applicationConfig.getOption(DaliConfigurationOption.DATE_TIME_FORMAT.getKey());
    }

    /*
     ----  File and Directory methods ---
     */

    /**
     * <p>getNewTmpDirectory.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link java.io.File} object.
     */
    public File getNewTmpDirectory(String name) {
        return new File(getTempDirectory(), name + "_" + System.nanoTime());
    }

    /**
     * <p>getHelpDirectory.</p>
     *
     * @return a {@link java.io.File} object.
     */
    public File getHelpDirectory() {
        return applicationConfig.getOptionAsFile(
                DaliConfigurationOption.HELP_DIRECTORY.getKey());
    }

    /**
     * <p>getUnknownRecorderDepartmentId.</p>
     *
     * @return a int.
     */
    public int getUnknownRecorderDepartmentId() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.UNKNOWN_RECORDER_DEPARTMENT.getKey());
    }

    public int getMassiveProcessChunkSize() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.DB_VALIDATION_BULK_SIZE.getKey());
    }

    // TODO les methodes suivantes sont-elles toujours utiles ?

    /**
     * <p>getTemporaryStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTemporaryStatusCode() {
        return "TEMPORARY";
    }

    /**
     * <p>getEnableStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEnableStatusCode() {
        return "ENABLE";
    }

    /**
     * <p>getDisableStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDisableStatusCode() {
        return "DISABLE";
    }

    /**
     * <p>getDeletedStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDeletedStatusCode() {
        return "DELETED";
    }

    /**
     * <p>getDirtySynchronizationStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDirtySynchronizationStatusCode() {
        return "DIRTY";
    }

    /**
     * <p>getReadySynchronizationStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getReadySynchronizationStatusCode() {
        return "READY_TO_SYNCHRONIZE";
    }

    /**
     * <p>getSynchronizedSynchronizationStatusCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSynchronizedSynchronizationStatusCode() {
        return "SYNCHRONIZED";
    }

    /*
     * Constants
     */
    public int getSurveyCalculatedLengthPmfmId() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.PMFM_ID_SURVEY_CALCULATED_LENGTH.getKey());
    }

    public List<Integer[]> getCalculatedTrawlAreaPmfmTriplets() {

        List<Integer[]> result = new ArrayList<>();
        String configOption = applicationConfig.getOption(DaliConfigurationOption.PMFM_IDS_CALCULATED_TRAWL_AREA.getKey());
        if (StringUtils.isBlank(configOption)) return result;

        List<String> triplets = DaliBeans.split(configOption, ";");
        for (String triplet : triplets) {
            List<String> pmfmsIdsStr = DaliBeans.split(triplet, ",");
            if (CollectionUtils.size(pmfmsIdsStr) == 3) {
                try {
                    Integer[] pmfmIds = new Integer[3];
                    pmfmIds[0] = Integer.parseInt(pmfmsIdsStr.get(0));
                    pmfmIds[1] = Integer.parseInt(pmfmsIdsStr.get(1));
                    pmfmIds[2] = Integer.parseInt(pmfmsIdsStr.get(2));

                    if (pmfmIds[0] < 0 || pmfmIds[1] < 0 || pmfmIds[2] < 0) continue;

                    result.add(pmfmIds);
                } catch (NumberFormatException e) {
                    LOG.warn(String.format("Problem parsing '%s' option value : %s", DaliConfigurationOption.PMFM_IDS_CALCULATED_TRAWL_AREA.getKey(), triplet));
                }
            } else {
                LOG.warn(String.format("Malformed '%s' option value : %s", DaliConfigurationOption.PMFM_IDS_CALCULATED_TRAWL_AREA.getKey(), triplet));
            }
        }

        return result;
    }

    public Map<Integer, Integer> getCompatibleLengthUnitIdByAreaUnitId() {

        Map<Integer, Integer> result = new HashMap<>();
        String configOption = applicationConfig.getOption(DaliConfigurationOption.UNIT_IDS_COMPATIBLE_LENGTH_BY_AREA.getKey());
        if (StringUtils.isBlank(configOption)) return result;

        List<String> unitsList = DaliBeans.split(configOption, ";");
        for (String units : unitsList) {
            List<String> unitIdsStr = DaliBeans.split(units, ",");
            if (CollectionUtils.size(unitIdsStr) == 2) {
                try {
                    result.put(Integer.parseInt(unitIdsStr.get(0)), Integer.parseInt(unitIdsStr.get(1)));
                } catch (NumberFormatException e) {
                    LOG.warn(String.format("Problem parsing '%s' option value : %s", DaliConfigurationOption.UNIT_IDS_COMPATIBLE_LENGTH_BY_AREA.getKey(), units));
                }
            } else {
                LOG.warn(String.format("Malformed '%s' option value : %s", DaliConfigurationOption.UNIT_IDS_COMPATIBLE_LENGTH_BY_AREA.getKey(), units));
            }
        }

        return result;
    }

    /**
     * <p>getNoUnitId.</p>
     *
     * @return a int.
     */
    public int getNoUnitId() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.UNIT_ID_NO_UNIT.getKey());
    }

    public String getFallbackLocale() {
        return QuadrigeCoreConfigurationOption.I18N_LOCALE.getDefaultValue();
    }

    /**
     * <p>getTranscribingItemTypeLbForPmfmNm.</p>
     * en_GB
     * fr_FR
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTranscribingItemTypeLbForPmfmNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_PMFM_NM.getKey());
    }

    public String getTranscribingItemTypeLbForPmfmExtraction() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_PMFM_EXTRACTION.getKey());
    }

    public String getTranscribingItemTypeLbForQualitativeValueNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_QUALITATIVE_VALUE_NM.getKey());
    }

    public String getTranscribingItemTypeLbForQualitativeValueExtraction() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_QUALITATIVE_VALUE_EXTRACTION.getKey());
    }

    public String getTranscribingItemTypeLbForSamplingEquipmentNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_SAMPLING_EQUIPMENT_NM.getKey());
    }

    public String getTranscribingItemTypeLbForSamplingEquipmentExtraction() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_SAMPLING_EQUIPMENT_EXTRACTION.getKey());
    }

    public String getTranscribingItemTypeLbForParameterNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_PARAMETER_NM.getKey());
    }

    public String getTranscribingItemTypeLbForParameterCd() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_PARAMETER_CD.getKey());
    }

    public String getTranscribingItemTypeLbForMatrixNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_MATRIX_NM.getKey());
    }

    public String getTranscribingItemTypeLbForFractionNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_FRACTION_NM.getKey());
    }

    public String getTranscribingItemTypeLbForMethodNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_METHOD_NM.getKey());
    }

    public String getTranscribingItemTypeLbForUnitNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_UNIT_NM.getKey());
    }

    public String getTranscribingItemTypeLbForUnitSymbol() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_UNIT_SYMBOL.getKey());
    }

    public String getTranscribingItemTypeLbForMonLocNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_MON_LOC_NM.getKey());
    }

    public String getTranscribingItemTypeLbForQualFlagNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_QUAL_FLAG_NM.getKey());
    }

    public String getTranscribingItemTypeLbForTaxLevelNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_TAX_LEVEL_NM.getKey());
    }

    public String getTranscribingItemTypeLbForStatusNm() {
        return getTranscribingItemTypeLbOptionByCurrentLocale(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_STATUS_NM.getKey());
    }

    private String getTranscribingItemTypeLbOptionByCurrentLocale(String optionKey) {
        Map<String, String> optionsByLocale = DaliBeans.splitAndMap(applicationConfig.getOption(optionKey), ";", ":");
        return optionsByLocale.get(getI18nLocale().toString());
    }

    public String getTranscribingItemTypeLbForTaxRef() {
        return applicationConfig.getOption(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_TAXREF.getKey());
    }

    public String getTranscribingItemTypeLbForWorms() {
        return applicationConfig.getOption(DaliConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_WORMS.getKey());
    }

    /**
     * <p>getAlternativeTaxonOriginTaxRef.</p>
     *
     * @return a {@link java.lang.String} object.
     */
//    public String getAlternativeTaxonOriginTaxRef() {
//        return applicationConfig.getOption(DaliConfigurationOption.ALTERNATIVE_TAXON_ORIGIN_TAXREF.getKey());
//    }

    /**
     * <p>getAlternativeTaxonOriginWorms.</p>
     *
     * @return a {@link java.lang.String} object.
     */
//    public String getAlternativeTaxonOriginWorms() {
//        return applicationConfig.getOption(DaliConfigurationOption.ALTERNATIVE_TAXON_ORIGIN_WORMS.getKey());
//    }

    public URL getSismerURL() {
        return applicationConfig.getOptionAsURL(DaliConfigurationOption.SISMER_WEBSITE_URL.getKey());
    }

    public int getControlSurveyLocationMinDistanceInMeter() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.CONTROL_SURVEY_LOCATION_MINIMUM_DISTANCE_IN_METER.getKey());
    }

    public MapMode getPreferredMapMode() {
        return Enums.fromCode(MapMode.class, applicationConfig.getOption(DaliConfigurationOption.MAP_BASE_LAYER_DEFAULT.getKey()));
    }

    public MapProjection getMapProjection() {
        return Enums.fromCode(MapProjection.class, applicationConfig.getOption(DaliConfigurationOption.MAP_PROJECTION_CODE.getKey()));
    }

    public String getMapSextantWMSUrl() {
        return applicationConfig.getOption(DaliConfigurationOption.MAP_SEXTANT_WMS_URL.getKey());
    }

    public String getMapSextantWMTSUrl() {
        return applicationConfig.getOption(DaliConfigurationOption.MAP_SEXTANT_WMTS_URL.getKey());
    }

    public String getMapOSMUrl() {
        return applicationConfig.getOption(DaliConfigurationOption.MAP_OSM_URL.getKey());
    }

    public String getMapOTMUrl() {
        return applicationConfig.getOption(DaliConfigurationOption.MAP_OTM_URL.getKey());
    }

    public String getMapCartoBaseUrl() {
        return applicationConfig.getOption(DaliConfigurationOption.MAP_CARTO_BASE_URL.getKey());
    }

    public String getMapSatelliteUrl() {
        return applicationConfig.getOption(DaliConfigurationOption.MAP_SATELLITE_URL.getKey());
    }

    public int getMaxSurveyInMap() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.MAP_MAX_SELECTION.getKey());
    }

    public boolean isSamplingOperationAutoLabelEnable() {
        return applicationConfig.getOptionAsBoolean(DaliConfigurationOption.SAMPLING_OPERATION_AUTO_LABEL_ENABLE.getKey());
    }

    public int getGridInitializationMaxCombinationCount() {
        return applicationConfig.getOptionAsInt(DaliConfigurationOption.GRID_INITIALIZATION_MAX_COMBINATION_COUNT.getKey());
    }
}
