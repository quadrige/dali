package fr.ifremer.dali.service.control;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.system.rule.DaliRuleDao;
import fr.ifremer.dali.dao.system.rule.DaliRuleListDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.dali.dto.configuration.control.RuleListDTO;
import fr.ifremer.dali.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.enums.ControlElementValues;
import fr.ifremer.dali.dto.enums.ControlFeatureMeasurementValues;
import fr.ifremer.dali.dto.enums.ControlFunctionValues;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.service.DaliDataContext;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.dali.vo.PresetVO;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.core.dao.technical.factorization.CombinationList;
import fr.ifremer.quadrige3.core.dao.technical.factorization.Factorizations;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.AllowedQualitativeValuesMap;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.QualitativeValuesCombinationValidator;
import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.quadrige3.core.vo.system.rule.RuleListVO;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRestClientService;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation rules control service.
 */
@Service("daliRuleListService")
public class RuleListServiceImpl implements RuleListService {

    private static final Log LOG = LogFactory.getLog(RuleListServiceImpl.class);

    @Resource(name = "daliRuleListDao")
    private DaliRuleListDao ruleListDao;
    @Resource(name = "daliRuleDao")
    private DaliRuleDao ruleDao;
    @Resource(name = "daliDataContext")
    private DaliDataContext dataContext;
    @Resource(name = "synchroRestClientService")
    private SynchroRestClientService synchroRestClientService;
    @Resource(name = "daliReferentialService")
    private ReferentialService referentialService;
    @Resource(name = "daliControlRuleService")
    private ControlRuleService controlRuleService;
    @Autowired
    protected DaliConfiguration configuration;

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleListDTO getRuleList(String ruleListCode) {
        Assert.notBlank(ruleListCode);
        return ruleListDao.getRuleList(ruleListCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RuleListDTO> getAllRuleLists() {
        return ruleListDao.getAllRuleLists();
    }

    @Override
    public List<RuleListDTO> getRuleListsForProgram(String programCode) {
        return ruleListDao.getRuleListsForProgram(programCode);
    }

    @Override
    public boolean ruleListCodeExists(String ruleListCode) {
        return ruleListDao.ruleListExists(ruleListCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRuleLists(AuthenticationInfo authenticationInfo, List<? extends RuleListDTO> ruleLists) {

        Assert.notNull(ruleLists);

        // get rule lists to save
        List<RuleListDTO> dirtyRuleList = ruleLists.stream().filter(RuleListDTO::isDirty).collect(Collectors.toList());

        // nothing to save
        if (CollectionUtils.isEmpty(dirtyRuleList)) return;

        // Collect rule list codes
        Set<String> ruleListCodes = dirtyRuleList.stream().map(ruleList -> {

            // preconditions
            Assert.notBlank(ruleList.getCode());
            Assert.notEmpty(ruleList.getPrograms());

            // save locally
            ruleListDao.saveRuleList(ruleList, dataContext.getRecorderPersonId());

            return ruleList.getCode();

        }).collect(Collectors.toSet());

        // Save remote rule lists
        saveRuleListsOnServer(authenticationInfo, ruleListCodes);

        // Reset newCode flags
        dirtyRuleList.forEach(ruleList -> {
            ruleList.setDirty(false);
            ruleList.setNewCode(false);
            ruleList.getControlRules().forEach(controlRule -> controlRule.setNewCode(false));
        });

    }

    private void saveRuleListsOnServer(AuthenticationInfo authenticationInfo, Set<String> ruleListCodes) {

        if (CollectionUtils.isEmpty(ruleListCodes)) return;

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Sending rule lists [%s] to server", Joiner.on(',').join(ruleListCodes)));
        }

        // get VOs
        List<RuleListVO> ruleListToSend = ruleListCodes.stream()
            .map(ruleListCode -> (RuleListVO) ruleListDao.load(RuleListDao.TRANSFORM_RULELISTVO, ruleListCode))
            .collect(Collectors.toList());

        // send to server
        List<RuleListVO> savedRuleLists = synchroRestClientService.saveRuleLists(authenticationInfo, ruleListToSend);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Saving rule lists [%s] from server response", Joiner.on(',').join(savedRuleLists)));
        }

        // save locally (update)
        for (RuleListVO savedRuleList : savedRuleLists) {
            ruleListDao.save(savedRuleList);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteRuleLists(AuthenticationInfo authenticationInfo, List<String> ruleListCodes) {
        if (CollectionUtils.isEmpty(ruleListCodes)) return;

        // delete locally
        for (String ruleListCode : ruleListCodes) {
            ruleListDao.remove(ruleListCode);
        }

        deleteRuleListsOnServer(authenticationInfo, ruleListCodes);
    }

    private void deleteRuleListsOnServer(AuthenticationInfo authenticationInfo, List<String> ruleListCodes) {

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Delete rule lists [%s] from server", Joiner.on(',').join(ruleListCodes)));
        }

        // Check if each ruleLists exists on server (it happens that a deleted ruleList had not been saved before)
        Set<String> ruleListCodesToDelete = ruleListCodes.stream()
            .filter(ruleListCode -> {
                if (synchroRestClientService.getRuleListByCode(authenticationInfo, ruleListCode) != null) {
                    return true;
                } else {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug(String.format("Rule List [%s] doesn't exists on server, skip it", ruleListCode));
                    }
                    return false;
                }
            })
            .collect(Collectors.toSet());

        if (CollectionUtils.isNotEmpty(ruleListCodesToDelete)) {

            // remote delete
            synchroRestClientService.deleteRuleLists(authenticationInfo, ruleListCodesToDelete);

            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Rule lists [%s] deleted from server", Joiner.on(',').join(ruleListCodesToDelete)));
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleListDTO duplicateRuleList(RuleListDTO ruleList, String newCode, boolean isDuplicateRulesEnabled) {

        RuleListDTO duplicatedRuleList = DaliBeans.clone(ruleList);

        duplicatedRuleList.setCode(newCode);
        duplicatedRuleList.setNewCode(true);
        duplicatedRuleList.setDepartments(DaliBeans.clone(ruleList.getDepartments()));
        duplicatedRuleList.setPrograms(DaliBeans.clone(ruleList.getPrograms()));
        duplicatedRuleList.setErrors(new ArrayList<>());

        duplicatedRuleList.setControlRules(new ArrayList<>());
        if (isDuplicateRulesEnabled && CollectionUtils.isNotEmpty(ruleList.getControlRules())) {
            MutableInt suffixIndex = getUniqueMutableIndex();
            for (ControlRuleDTO controlRule : ruleList.getControlRules()) {
                ControlRuleDTO duplicatedControlRule = duplicateControlRule(controlRule, duplicatedRuleList);
                // clone preconditions if exists
                if (!controlRule.isPreconditionsEmpty()) {
                    duplicatePreconditions(controlRule, duplicatedControlRule, suffixIndex);
                }
                duplicatedRuleList.addControlRules(duplicatedControlRule);
            }
        }

        return duplicatedRuleList;
    }

    private ControlRuleDTO duplicateControlRule(ControlRuleDTO controlRule, RuleListDTO ruleList) {

        ControlRuleDTO duplicatedControlRule = DaliBeans.clone(controlRule);

        duplicatedControlRule.setCode(ruleList != null ? getNextRuleCode(ruleList) : null);
        duplicatedControlRule.setNewCode(true);
        duplicatedControlRule.setErrors(null);
        duplicatedControlRule.setPreconditions(null);
        duplicatedControlRule.setRulePmfms(null);

        // clone pmfm list and reset RULE_PMFM ids
        for (RulePmfmDTO rulePmfm : controlRule.getRulePmfms()) {
            RulePmfmDTO duplicatedRulePmfm = DaliBeans.clone(rulePmfm);
            duplicatedRulePmfm.setId(null);
            duplicatedControlRule.addRulePmfms(duplicatedRulePmfm);
        }

        return duplicatedControlRule;
    }

    private void duplicatePreconditions(ControlRuleDTO controlRule, ControlRuleDTO duplicatedControlRule, MutableInt suffixIndex) {

        for (PreconditionRuleDTO precondition : controlRule.getPreconditions()) {
            PreconditionRuleDTO duplicatedPrecondition = DaliBeans.clone(precondition);
            duplicatedPrecondition.setId(null);
            duplicatedPrecondition.setRule(duplicatedControlRule);
            duplicatedPrecondition.setName(duplicatedControlRule.getCode());
            duplicatedPrecondition.setBaseRule(duplicateControlRule(precondition.getBaseRule(), null));
            duplicatedPrecondition.getBaseRule().setCode(getNextRuleCode(duplicatedControlRule.getCode(), suffixIndex));
            duplicatedPrecondition.setUsedRule(duplicateControlRule(precondition.getUsedRule(), null));
            duplicatedPrecondition.getUsedRule().setCode(getNextRuleCode(duplicatedControlRule.getCode(), suffixIndex));
            duplicatedControlRule.addPreconditions(duplicatedPrecondition);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControlRuleDTO newControlRule(RuleListDTO ruleList) {

        Assert.notNull(ruleList);
        Assert.notBlank(ruleList.getCode());

        ControlRuleDTO rule = DaliBeanFactory.newControlRuleDTO();

        rule.setCode(getNextRuleCode(ruleList));
        rule.setNewCode(true);
        rule.setActive(true);
        rule.setBlocking(false);

        return rule;
    }

    @Override
    public boolean ruleCodeExists(String ruleCode) {
        Assert.notBlank(ruleCode);

        return ruleDao.ruleExists(ruleCode);
    }

    @Override
    public List<ControlRuleDTO> getPreconditionedControlRulesForSurvey(SurveyDTO survey) {
        Assert.notNull(survey);
        Assert.notNull(survey.getDate());
        Assert.notNull(survey.getProgram());
        Assert.notNull(survey.getRecorderDepartment());

        List<ControlRuleDTO> preconditionedRules = ruleDao.findActivePreconditionedRules(
            Dates.convertToDate(survey.getDate(), configuration.getDbTimezone()),
            survey.getProgram().getCode(),
            survey.getRecorderDepartment().getId());
        buildPmfmInformation(preconditionedRules);
        return preconditionedRules;
    }

    @Override
    public List<ControlRuleDTO> getPreconditionedControlRulesForProgramCodes(List<String> programCodes) {
        Assert.notEmpty(programCodes);

        List<ControlRuleDTO> preconditionedRules = ruleDao.findActivePreconditionedRules(programCodes);
        buildPmfmInformation(preconditionedRules);
        return preconditionedRules;

    }

    @Override
    public void buildAllowedValuesByPmfmId(int sourcePmfmId, Object sourceValue, Collection<Integer> targetPmfmIds,
                                           Collection<PreconditionRuleDTO> preconditions,
                                           AllowedQualitativeValuesMap allowedQualitativeValuesMap) {

        for (PreconditionRuleDTO precondition : preconditions) {

            ControlRuleDTO sourceRule;
            ControlRuleDTO targetRule;

            // determine base and used rule
            if (!precondition.isBidirectional() || Objects.equals(precondition.getBaseRule().getRulePmfms(0).getPmfm().getId(), sourcePmfmId)) {
                // simple case for mono directional rule or if the base rule pmfm is the actual pmfm
                sourceRule = precondition.getBaseRule();
                targetRule = precondition.getUsedRule();
            } else {
                // source and target are switched
                sourceRule = precondition.getUsedRule();
                targetRule = precondition.getBaseRule();
            }

            // find now the target column, and stop here if not found
            int targetPmfmId = targetRule.getRulePmfms(0).getPmfm().getId();
            if (!targetPmfmIds.contains(targetPmfmId)) continue; // if the target pmfm is not part of target pmfms, continue to next precondition

            // valid source && target rules
            if (DaliBeans.isQualitativeControlRule(targetRule)) {

                if (DaliBeans.isQualitativeControlRule(sourceRule)) {

                    BaseReferentialDTO qualitativeValue = (BaseReferentialDTO) sourceValue;

                    // control the value on qualitative source rule
                    if (controlRuleService.controlUniqueObject(sourceRule, qualitativeValue)) {

                        // add allowed values from the target rule
                        allowedQualitativeValuesMap.addTargetValues(targetPmfmId, sourcePmfmId, qualitativeValue.getId(), getAllowedValueIds(targetRule));

                    } else {

                        // if this rule is invalid, try to reset allowed values if possible
                        AllowedQualitativeValuesMap.AllowedValues allowedValues =
                            allowedQualitativeValuesMap.getAllowedValues(targetPmfmId, sourcePmfmId, qualitativeValue != null ? qualitativeValue.getId() : null);
                        if (qualitativeValue == null) {
                            allowedValues.setAllAllowed();
                        }

                    }

                } else {
                    // invalid source rule
                    LOG.warn(String.format("invalid source rule %s", sourceRule.getCode()));
                }

            } else {

                // invalid target rule
                LOG.warn(String.format("invalid target rule %s", targetRule.getCode()));
            }

        }
    }

    private Set<Integer> getAllowedValueIds(ControlRuleDTO rule) {
        return DaliBeans.getIntegerSetFromString(rule.getAllowedValues(), configuration.getValueSeparator());
    }

    @Override
    public Multimap<QualitativeValueDTO, QualitativeValueDTO> buildQualitativeValueMapFromPreconditions(Collection<PreconditionRuleDTO> preconditions) {
        Multimap<QualitativeValueDTO, QualitativeValueDTO> multimap = HashMultimap.create();
        for (PreconditionRuleDTO precondition : preconditions) {

            List<QualitativeValueDTO> baseValues = referentialService.getQualitativeValues(
                DaliBeans.getIntegerSetFromString(precondition.getBaseRule().getAllowedValues(), configuration.getValueSeparator()));
            for (QualitativeValueDTO baseValue : baseValues) {
                multimap.putAll(
                    baseValue,
                    referentialService.getQualitativeValues(
                        DaliBeans.getIntegerSetFromString(precondition.getUsedRule().getAllowedValues(), configuration.getValueSeparator()))
                );
            }
        }
        return multimap;

    }

    @Override
    public void buildPreconditionsFromQualitativeValueMap(ControlRuleDTO rule, Multimap<QualitativeValueDTO, QualitativeValueDTO> multimap) {

        Map<String, PreconditionRuleDTO> existingRulePreconditionsByBaseValues = getRulePreconditionsByBaseValues(rule);

        MutableInt suffixIndex = getUniqueMutableIndex();

        for (QualitativeValueDTO baseValue : multimap.keySet()) {
            PreconditionRuleDTO precondition = existingRulePreconditionsByBaseValues.remove(baseValue.getId().toString());
            if (precondition == null) {
                precondition = createPreconditionRule(rule);
            }
            precondition.setActive(rule.isActive());
            precondition.setName(rule.getCode());

            ControlRuleDTO baseRule = precondition.getBaseRule();
            if (baseRule == null) {
                baseRule = createQualitativeControlRule(rule, suffixIndex);
                baseRule.addRulePmfms(cloneRulePmfm(rule.getRulePmfms(0)));
                baseRule.setAllowedValues(baseValue.getId().toString());
                precondition.setBaseRule(baseRule);
            }

            ControlRuleDTO usedRule = precondition.getUsedRule();
            if (usedRule == null) {
                usedRule = createQualitativeControlRule(rule, suffixIndex);
                usedRule.addRulePmfms(cloneRulePmfm(rule.getRulePmfms(1)));
                precondition.setUsedRule(usedRule);
            }
            usedRule.setAllowedValues(DaliBeans.joinIds(multimap.get(baseValue), configuration.getValueSeparator()));

        }

        // remove unused preconditions
        rule.removeAllPreconditions(existingRulePreconditionsByBaseValues.values());
    }

    @Override
    public CombinationList buildAndFactorizeAllowedValues(PresetVO preset, Multimap<Integer, PreconditionRuleDTO> preconditionRulesByPmfmId, int maxCombinationCount) {
        // build allowed values by target pmfm id
        AllowedQualitativeValuesMap allowedQualitativeValuesMap = new AllowedQualitativeValuesMap();
        long start = System.currentTimeMillis();
        for (Integer pmfmId : preset.getPmfmIds()) {
            for (Integer qvId : preset.getQualitativeValueIds(pmfmId)) {
                buildAllowedValuesByPmfmId(
                    pmfmId,
                    DaliBeans.newDummyBaseReferentialBean(qvId),
                    preset.getPmfmIds(),
                    preconditionRulesByPmfmId.get(pmfmId),
                    allowedQualitativeValuesMap
                );
            }
        }
        if (LOG.isDebugEnabled())
            LOG.debug(String.format("allowed values built in %s", Times.durationToString(System.currentTimeMillis() - start)));

        // factorize all combinations with a predicate ti filter allowed values only
        // and a maximum of combinations count (1000 by default)
        start = System.currentTimeMillis();
        CombinationList allPmfmValues = Factorizations.factorize(
            preset.getPmfmPresets(),
            new QualitativeValuesCombinationValidator(maxCombinationCount, allowedQualitativeValuesMap));
        if (LOG.isDebugEnabled())
            LOG.debug(String.format("combinations built in %s", Times.durationToString(System.currentTimeMillis() - start)));

        return allPmfmValues;
    }

    @Override
    public CombinationList buildAndFactorizeAllowedValues(Collection<PmfmPresetDTO> pmfmPresets, Multimap<Integer, PreconditionRuleDTO> preconditionRulesByPmfmId, int maxCombinationCount) {
        // Convert the PmfmPresetDTO collection to a PresetVO
        return buildAndFactorizeAllowedValues(toPresetVO(pmfmPresets), preconditionRulesByPmfmId, maxCombinationCount);
    }

    private PresetVO toPresetVO(Collection<PmfmPresetDTO> pmfmPresets) {
        PresetVO preset = new PresetVO();
        pmfmPresets.forEach(
            pmfmPreset -> preset.addPmfmPreset(
                pmfmPreset.getPmfm().getId(),
                pmfmPreset.getQualitativeValues().stream().map(QualitativeValueDTO::getId).collect(Collectors.toList())));
        return preset;
    }

    private Map<String, PreconditionRuleDTO> getRulePreconditionsByBaseValues(ControlRuleDTO rule) {
        return DaliBeans.mapByProperty(rule.getPreconditions(), PreconditionRuleDTO.PROPERTY_BASE_RULE + "." + ControlRuleDTO.PROPERTY_ALLOWED_VALUES);
    }

    private PreconditionRuleDTO createPreconditionRule(ControlRuleDTO parentRule) {
        PreconditionRuleDTO precondition = DaliBeanFactory.newPreconditionRuleDTO();
        precondition.setRule(parentRule);
        precondition.setBidirectional(true); // by default
        parentRule.addPreconditions(precondition);
        return precondition;
    }

    private ControlRuleDTO createQualitativeControlRule(ControlRuleDTO parentRule, MutableInt suffixIndex) {
        ControlRuleDTO rule = DaliBeanFactory.newControlRuleDTO();
        rule.setCode(getNextRuleCode(parentRule.getCode(), suffixIndex));
        rule.setFunction(ControlFunctionValues.IS_AMONG.toFunctionDTO());
        rule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        rule.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());
        rule.setActive(false); // must be false
        return rule;
    }

    private RulePmfmDTO cloneRulePmfm(RulePmfmDTO rulePmfm) {
        RulePmfmDTO result = DaliBeanFactory.newRulePmfmDTO();
        result.setPmfm(DaliBeans.clone(rulePmfm.getPmfm()));
        return result;
    }

    private void buildPmfmInformation(List<ControlRuleDTO> controlRules) {
        if (controlRules != null) {
            for (ControlRuleDTO preconditionedRule : controlRules) {
                // Affect all pmfmId with the real pmfm Id from the referential: Allow better handling in UI
                for (RulePmfmDTO rulePmfm : preconditionedRule.getRulePmfms()) {
                    rulePmfm.getPmfm().setId(referentialService.getUniquePmfmIdFromPmfm(rulePmfm.getPmfm()));
                }
                for (PreconditionRuleDTO precondition : preconditionedRule.getPreconditions()) {
                    precondition.getBaseRule().getRulePmfms(0).getPmfm().setId(referentialService.getUniquePmfmIdFromPmfm(precondition.getBaseRule().getRulePmfms(0).getPmfm()));
                    precondition.getUsedRule().getRulePmfms(0).getPmfm().setId(referentialService.getUniquePmfmIdFromPmfm(precondition.getUsedRule().getRulePmfms(0).getPmfm()));
                }
            }
        }
    }

    /**
     * rule code = <rule_list_code>_<auto_incremental_number>
     *
     * @param ruleList the rule list
     * @return the next rule code
     */
    private String getNextRuleCode(RuleListDTO ruleList) {
        Assert.notNull(ruleList);
        int maxSuffix = 0;

        if (CollectionUtils.isNotEmpty(ruleList.getControlRules())) {

            for (ControlRuleDTO r : ruleList.getControlRules()) {
                String ruleCode = r.getCode();

                if (StringUtils.isNotBlank(ruleCode)
                    && ruleCode.startsWith(ruleList.getCode())) {
                    int lastIndex = ruleCode.lastIndexOf('_');
                    if (lastIndex == -1) {
                        lastIndex = ruleCode.lastIndexOf('-');
                    }

                    if (lastIndex != -1 && lastIndex != ruleCode.length() - 1) {
                        String suffix = ruleCode.substring(lastIndex + 1);
                        try {
                            maxSuffix = Math.max(maxSuffix, Integer.parseInt(suffix));
                        } catch (NumberFormatException e) {
                            // skip this rule code
                        }
                    }
                }
            }
        }

        // return (max + 1)
        return String.format("%s_%d", ruleList.getCode(), maxSuffix + 1);
    }

    @Override
    public MutableInt getUniqueMutableIndex() {
        return new MutableInt(System.currentTimeMillis() / 1000);
    }

    /**
     * @param targetName  the target rule name
     * @param suffixIndex incremental suffix
     * @return the next non existing rule code
     */
    @Override
    public String getNextRuleCode(String targetName, MutableInt suffixIndex) {
        String ruleCode;
        do {
            // trim to 40 characters
            String suffix = Integer.toString(suffixIndex.getAndIncrement());
            int suffixLength = suffix.length() + 1;
            int totalLength = targetName.length() + suffixLength;
            if (totalLength > 40) {
                targetName = targetName.substring(0, 40 - suffixLength);
            }
            ruleCode = String.format("%s_%s", targetName, suffix);
        } while (ruleCodeExists(ruleCode));
        return ruleCode;
    }

}
