package fr.ifremer.dali.service.administration.program;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ifremer.dali.dao.administration.program.DaliProgramDao;
import fr.ifremer.dali.dao.administration.strategy.DaliStrategyDao;
import fr.ifremer.dali.dao.referential.monitoringLocation.DaliMonitoringLocationDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.*;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.dali.service.DaliDataContext;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramServiceImpl;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRestClientService;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeans;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service need for programs and strategy.
 */
@Service("daliProgramStrategyService")
public class ProgramStrategyServiceImpl
        extends ProgramServiceImpl
        implements ProgramStrategyService {

    /* Logger */
    private static final Log LOG = LogFactory.getLog(ProgramStrategyServiceImpl.class);
    private static final Predicate<PmfmStrategyDTO> PMFM_FOR_SURVEY_PREDICATE = object -> object.isSurvey() && !object.isGrouping();
    private static final Predicate<PmfmStrategyDTO> GROUPED_PMFM_FOR_SURVEY_PREDICATE = object -> object.isSurvey() && object.isGrouping();
    private static final Predicate<PmfmStrategyDTO> PMFM_FOR_SAMPLING_OPERATION_PREDICATE = object -> object.isSampling() && !object.isGrouping();
    private static final Predicate<PmfmStrategyDTO> GROUPED_PMFM_FOR_SAMPLING_OPERATION_PREDICATE = object -> object.isSampling() && object.isGrouping();
    @Resource(name = "daliProgramDao")
    private DaliProgramDao programDao;
    @Resource(name = "daliStrategyDao")
    private DaliStrategyDao strategyDao;
    @Resource(name = "daliMonitoringLocationDao")
    private DaliMonitoringLocationDao locationDao;
    @Resource(name = "synchroRestClientService")
    private SynchroRestClientService synchroRestClientService;

    @Resource(name = "daliDataContext")
    protected DaliDataContext dataContext;

    @Override
    public ProgramDTO getReadableProgramByCode(String programCode) {
        return filterReadableProgram(programDao.getProgramByCode(programCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProgramDTO getWritableProgramByCode(String programCode) {
        return filterWritableProgram(programDao.getProgramByCode(programCode));
    }

    @Override
    public List<ProgramDTO> getReadableProgramsByStatus(StatusFilter statusFilter) {
        return filterReadablePrograms(
            programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), null, null)
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getWritableProgramsByStatus(StatusFilter statusFilter) {
        return filterWritablePrograms(
                programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), null, null)
        );
    }

    @Override
    public List<ProgramDTO> getReadableProgramsByLocationAndDate(int locationId, Date date) {
        return filterReadablePrograms(programDao.findProgramsByLocationAndDate(StatusFilter.ACTIVE.toStatusCodes(), locationId, date));
    }

    @Override
    public List<ProgramDTO> getWritableProgramsByLocationAndDate(int locationId, Date date) {
        return filterWritablePrograms(programDao.findProgramsByLocationAndDate(StatusFilter.ACTIVE.toStatusCodes(), locationId, date));
    }

    @Override
    public List<ProgramDTO> getReadablePrograms() {
        // Return programs with read rights for current user
        return filterReadablePrograms(programDao.getAllPrograms());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getWritablePrograms() {
        // Return programs with write rights for current user
        return filterWritablePrograms(programDao.getAllPrograms());
    }

    @Override
    public List<ProgramDTO> getWritableProgramsByUser(int userId) {
        return filterWritablePrograms(userId, programDao.getAllPrograms());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getWritableProgramsByUser(int userId, StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return filterWritablePrograms(userId, programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), null, null));
    }

    @Override
    public List<ProgramDTO> getManagedPrograms() {
        return getManagedProgramsByUser(SecurityContextHelper.getQuadrigeUserId());
    }

    @Override
    public List<ProgramDTO> getManagedProgramsByUser(int userId) {
        return filterManagedPrograms(userId, programDao.getAllPrograms());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getManagedProgramsByUser(int userId, StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return filterManagedPrograms(userId, programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), null, null));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveStrategiesByProgramAndLocation(AuthenticationInfo authenticationInfo, List<ProgStratDTO> strategies, int locationId) {

        if (CollectionUtils.isEmpty(strategies)) {
            return;
        }

        Set<String> remoteProgramCodes = Sets.newHashSet();
        for (ProgStratDTO strategy : strategies) {

            // Store remote program codes
            boolean isRemoteProgram = !QuadrigeBeans.isLocalStatus(strategy.getProgram().getStatus());
            if (isRemoteProgram) {
                if (authenticationInfo == null) {
                    throw new DaliTechnicalException("Could not save a remote program: authentication is required to connect to synchro server");
                }

                remoteProgramCodes.add(strategy.getProgram().getCode());
            }

            // save strategy
            strategyDao.saveStrategyByLocation(strategy, locationId);
        }

        // Save remote programs, using synchro server
        if (CollectionUtils.isNotEmpty(remoteProgramCodes)) {
            saveProgramOnServer(authenticationInfo, remoteProgramCodes);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadStrategiesAndLocations(ProgramDTO program) {
        Assert.notNull(program);
        Assert.notNull(program.getCode());

        // Load strategies
        if (!program.isStrategiesLoaded()) {
            List<StrategyDTO> strategies = new ArrayList<>(strategyDao.getStrategiesByProgramCode(program.getCode()));
            program.setStrategies(strategies);
            program.setStrategiesLoaded(true);

            // Load applied strategies
            if (!program.isStrategiesEmpty()) {
                Multimap<Integer, AppliedStrategyDTO> allAppliedPeriods = strategyDao.getAllAppliedPeriodsByProgramCode(program.getCode());
                for (StrategyDTO strategy : program.getStrategies()) {
                    if (strategy.getId() != null && !strategy.isAppliedStrategiesLoaded()) {
                        Collection<AppliedStrategyDTO> appliedStrategies = allAppliedPeriods.get(strategy.getId());
                        strategy.setAppliedStrategies(new ArrayList<>(appliedStrategies));
                        strategy.setAppliedStrategiesLoaded(true);
                    }
                }
            }
        }

        // Load program's location
        if (!program.isLocationsLoaded()) {
            List<LocationDTO> locations = new ArrayList<>(locationDao.getLocationsByCampaignAndProgram(null, program.getCode(), StatusFilter.ALL.toStatusCodes()));
            program.setLocations(locations);
            program.setLocationsLoaded(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void savePrograms(List<? extends ProgramDTO> programs) {
        savePrograms(null, programs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void savePrograms(AuthenticationInfo authenticationInfo, List<? extends ProgramDTO> programs) {

        if (CollectionUtils.isEmpty(programs)) {
            return;
        }

        Set<String> remoteProgramCodes = Sets.newHashSet();
        for (ProgramDTO programToSave : programs) {

            // don't try to save an unmodified program
            if (!programToSave.isDirty()) {
                continue;
            }

            // Store remote program codes
            boolean isRemoteProgram = !QuadrigeBeans.isLocalStatus(programToSave.getStatus());
            if (isRemoteProgram) {
                if (authenticationInfo == null) {
                    throw new DaliTechnicalException("Could not save a remote program: authentication is required to connect to synchro server");
                }

                remoteProgramCodes.add(programToSave.getCode());
            }

            try {
                // save
                programDao.saveProgram(programToSave);

                // reset dirty flag
                programToSave.setDirty(false);
                programToSave.setNewCode(false);

            } finally {
                // set children unloaded to force reload
                programToSave.setStrategiesLoaded(false);
                programToSave.setLocationsLoaded(false);
            }
        }

        // Save remote programs, using synchro server
        if (CollectionUtils.isNotEmpty(remoteProgramCodes)) {
            saveProgramOnServer(authenticationInfo, remoteProgramCodes);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadAppliedPeriodsAndPmfmStrategies(ProgramDTO program, StrategyDTO strategy) {
        Assert.notNull(program);
        Assert.notNull(strategy);

        List<AppliedStrategyDTO> appliedStrategies;
        Map<Integer, LocationDTO> missingProgramMonitoringLocationIds = DaliBeans.mapById(program.getLocations());

        // Load pmfm strategies
        if (strategy.getId() != null && !strategy.isPmfmStrategiesLoaded()) {
            List<PmfmStrategyDTO> pmfms = strategyDao.getPmfmsStrategies(strategy.getId());
            strategy.setPmfmStrategies(new ArrayList<>(pmfms));
            strategy.setPmfmStrategiesLoaded(true);
        }

        // If first load of this strategy
        if (strategy.getId() != null && !strategy.isAppliedStrategiesLoaded()) {
            // Load strategy's appliedPeriods
            appliedStrategies = new ArrayList<>(strategyDao.getAppliedPeriodsByStrategyId(strategy.getId()));

            // Add missing program's locations
            for (AppliedStrategyDTO appliedStrategy : appliedStrategies) {
                missingProgramMonitoringLocationIds.remove(appliedStrategy.getId());
            }

        }

        // Refresh the strategy (program could have new location)
        else {
            appliedStrategies = Lists.newArrayList();

            // Detect new/removed program's location
            for (AppliedStrategyDTO appliedStrategy : strategy.getAppliedStrategies()) {
                Integer monitoringLocationId = appliedStrategy.getId();

                // Only add location that exists in program's locations
                if (missingProgramMonitoringLocationIds.containsKey(monitoringLocationId)) {
                    appliedStrategies.add(appliedStrategy);
                    missingProgramMonitoringLocationIds.remove(appliedStrategy.getId());
                }
            }

        }

        // Add locations that exist in program but not in strategy
        if (!missingProgramMonitoringLocationIds.isEmpty()) {
            for (LocationDTO missingLocation : missingProgramMonitoringLocationIds.values()) {
                AppliedStrategyDTO newAppliedStrategy = DaliBeans.locationToAppliedStrategyDTO(missingLocation);
                // applied strategy ID, and dates remain null
                appliedStrategies.add(newAppliedStrategy);
            }
        }

        // Apply to the strategy
        strategy.setAppliedStrategies(appliedStrategies);

        // Update loaded applied strategies, set analysis department
        appliedStrategies.forEach(appliedStrategy -> appliedStrategy.setAnalysisDepartment(appliedStrategy.getAppliedStrategyId() != null
            ? strategyDao.getAnalysisDepartmentByAppliedStrategyId(appliedStrategy.getAppliedStrategyId())
            : null));

        strategy.setAppliedStrategiesLoaded(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmStrategyDTO> getPmfmStrategiesBySurvey(SurveyDTO survey) {

        if (survey == null || survey.getProgram() == null || StringUtils.isBlank(survey.getProgram().getCode())
                || survey.getLocation() == null || survey.getLocation().getId() == null
                || survey.getDate() == null) {
            return null;
        }

        Set<PmfmStrategyDTO> pmfmStrategies = strategyDao.getPmfmStrategiesByProgramCodeAndLocation(survey.getProgram().getCode(), survey.getLocation().getId(), survey.getDate());

        // Replace qualitative value in pmfm by those restricted by pmfm strategy
        pmfmStrategies.forEach(pmfmStrategyDTO -> {
            PmfmDTO pmfmForDataInput = DaliBeans.clone(pmfmStrategyDTO.getPmfm());
            pmfmForDataInput.setQualitativeValues(pmfmStrategyDTO.getQualitativeValues());
            pmfmStrategyDTO.setPmfm(pmfmForDataInput);
        });

        // Sort by rankOrder
        return pmfmStrategies.stream()
                .sorted(Comparator.nullsLast(Comparator.comparingInt(PmfmStrategyDTO::getRankOrder)))
                .collect(Collectors.toList());
    }

    @Override
    public Set<PmfmStrategyDTO> getPmfmStrategiesByProgramsAndDates(Collection<String> programCodes, LocalDate startDate, LocalDate endDate) {

        if (CollectionUtils.isEmpty(programCodes) || startDate == null || endDate == null) return new HashSet<>();

        return strategyDao.getPmfmStrategiesByProgramCodesAndDates(programCodes, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DepartmentDTO getSamplingDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey) {

        if (survey == null || survey.getProgram() == null || StringUtils.isBlank(survey.getProgram().getCode())
                || survey.getLocation() == null || survey.getLocation().getId() == null
                || survey.getDate() == null) {
            return null;
        }

        List<ProgStratDTO> appliedStrategies = strategyDao.getAppliedStrategiesByProgramCodeAndMonitoringLocationId(survey.getProgram().getCode(), survey.getLocation().getId());

        for (ProgStratDTO appliedStrategy : appliedStrategies) {
            // also filter on date
            if (appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null
                    && Dates.isBetween(survey.getDate(), appliedStrategy.getStartDate(), appliedStrategy.getEndDate())
                    && appliedStrategy.getDepartment() != null) {

                // return first not null department found
                return appliedStrategy.getDepartment();
            }
        }

        return null;
    }

    @Override
    public DepartmentDTO getAnalysisDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey) {

        if (survey == null || survey.getProgram() == null || StringUtils.isBlank(survey.getProgram().getCode())
                || survey.getLocation() == null || survey.getLocation().getId() == null
                || survey.getDate() == null) {
            return null;
        }

        List<ProgStratDTO> appliedStrategies = strategyDao.getAppliedStrategiesByProgramCodeAndMonitoringLocationId(survey.getProgram().getCode(), survey.getLocation().getId());

        for (ProgStratDTO appliedStrategy : appliedStrategies) {
            // also filter on date
            if (appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null
                    && Dates.isBetween(survey.getDate(), appliedStrategy.getStartDate(), appliedStrategy.getEndDate())) {

                DepartmentDTO analysisDepartment = strategyDao.getAnalysisDepartmentByAppliedStrategyId(appliedStrategy.getAppliedStrategyId());

                // return first not null department found
                if (analysisDepartment != null)
                    return analysisDepartment;
            }
        }

        return null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfmsForSurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, PMFM_FOR_SURVEY_PREDICATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getGroupedPmfmsForSurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, GROUPED_PMFM_FOR_SURVEY_PREDICATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfmsForSamplingOperationBySurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, PMFM_FOR_SAMPLING_OPERATION_PREDICATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getGroupedPmfmsForSamplingOperationBySurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, GROUPED_PMFM_FOR_SAMPLING_OPERATION_PREDICATE);
    }

    private List<PmfmDTO> getPredicatedPmfmsBySurvey(SurveyDTO survey, Predicate<PmfmStrategyDTO> predicate) {

        Assert.notNull(survey);

        List<PmfmDTO> result = Lists.newArrayList();

        List<PmfmStrategyDTO> pmfmStrategies = getPmfmStrategiesBySurvey(survey);

        if (CollectionUtils.isEmpty(pmfmStrategies)) {
            return result;
        }

        for (PmfmStrategyDTO p : pmfmStrategies) {
            if (predicate.evaluate(p)) {
                result.add(p.getPmfm());
            }
        }

        return result;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgStratDTO> getStrategyUsageByLocationId(Integer locationId) {
        return strategyDao.getAppliedStrategiesByProgramCodeAndMonitoringLocationId(null, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgStratDTO> getStrategyUsageByProgramAndLocationId(String programCode, Integer locationId) {
        return strategyDao.getAppliedStrategiesByProgramCodeAndMonitoringLocationId(programCode, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StrategyDTO duplicateStrategy(final StrategyDTO strategy, ProgramDTO targetProgram, boolean skipLocalReferential) {
        Assert.notNull(strategy);
        Assert.notNull(targetProgram);

        // Strategy duplicated
        final StrategyDTO duplicateStrategy = DaliBeans.clone(strategy);
        duplicateStrategy.setAppliedStrategies(DaliBeans.clone(strategy.getAppliedStrategies()));
        duplicateStrategy.setPmfmStrategies(DaliBeans.clone(strategy.getPmfmStrategies()));

        // Remove ID and unused properties
        duplicateStrategy.setId(null);
        duplicateStrategy.setName(null);
        duplicateStrategy.setErrors(null);

        // Add locations to Program
        if (!duplicateStrategy.isAppliedStrategiesEmpty()) {

            List<Integer> programLocationIds = DaliBeans.collectIds(targetProgram.getLocations());
            ListIterator<AppliedStrategyDTO> appliedStrategyIterator = duplicateStrategy.getAppliedStrategies().listIterator();
            while (appliedStrategyIterator.hasNext()) {
                AppliedStrategyDTO appliedStrategy = appliedStrategyIterator.next();

                if (skipLocalReferential) {
                    // remove local location
                    if (QuadrigeBeans.isLocalStatus(appliedStrategy.getStatus())) {
                        appliedStrategyIterator.remove();
                        continue;
                    }

                    // reset local departments
                    if (appliedStrategy.getSamplingDepartment() != null && QuadrigeBeans.isLocalStatus(appliedStrategy.getSamplingDepartment().getStatus())) {
                        appliedStrategy.setSamplingDepartment(null);
                    }
                    if (appliedStrategy.getAnalysisDepartment() != null && QuadrigeBeans.isLocalStatus(appliedStrategy.getAnalysisDepartment().getStatus())) {
                        appliedStrategy.setAnalysisDepartment(null);
                    }

                }

                // reset properties
                appliedStrategy.setAppliedStrategyId(null);
                appliedStrategy.setStartDate(null);
                appliedStrategy.setEndDate(null);
                appliedStrategy.setPreviousStartDate(null);
                appliedStrategy.setPreviousEndDate(null);
                appliedStrategy.setSamplingDepartment(null);
                appliedStrategy.setAnalysisDepartment(null);

                // add to program location
                if (!programLocationIds.contains(appliedStrategy.getId())) {
                    targetProgram.addLocations(appliedStrategy);
                }

            }

        }

        if (!duplicateStrategy.isPmfmStrategiesEmpty()) {
            ListIterator<PmfmStrategyDTO> pmfmStrategyIterator = duplicateStrategy.getPmfmStrategies().listIterator();
            while (pmfmStrategyIterator.hasNext()) {
                PmfmStrategyDTO pmfmStrategy = pmfmStrategyIterator.next();

                if (skipLocalReferential) {
                    // remove this pmfm strategy if the pmfm is local
                    if (pmfmStrategy.getPmfm() != null && QuadrigeBeans.isLocalStatus(pmfmStrategy.getPmfm().getStatus())) {
                        pmfmStrategyIterator.remove();
                        continue;
                    }
                }
                pmfmStrategy.setId(null);
            }
        }

        // Return value duplicated
        return duplicateStrategy;
    }

    @Override
    public List<ProgramDTO> getRemoteReadableProgramsByUser(AuthenticationInfo authenticationInfo) {
        List<ProgramVO> programs = synchroRestClientService.getReadableProgramsForUser(authenticationInfo);
        return toProgramDTOs(programs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getRemoteWritableProgramsByUser(AuthenticationInfo authenticationInfo) {
        List<ProgramVO> programs = synchroRestClientService.getWritableProgramsForUser(authenticationInfo);
        return toProgramDTOs(programs);
    }

    @Override
    public boolean hasRemoteReadOnProgram(AuthenticationInfo authenticationInfo) {
        List<ProgramVO> programs = synchroRestClientService.getReadableProgramsForUser(authenticationInfo);
        return CollectionUtils.isNotEmpty(programs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasRemoteWriteOnProgram(AuthenticationInfo authenticationInfo) {
        List<ProgramVO> programs = synchroRestClientService.getWritableProgramsForUser(authenticationInfo);
        return CollectionUtils.isNotEmpty(programs);
    }

    @Override
    public List<ProgramDTO> getProgramsByCodes(List<String> programCodes) {
        Assert.notEmpty(programCodes);
        List<ProgramDTO> programs = programDao.getProgramsByCodes(programCodes);
        programs.forEach(program -> program.getErrors().clear());
        return programs;
    }

    @Override
    public boolean hasPotentialMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters) {
        return !getMoratoriums(programCodes, periodFilters, true).isEmpty();
    }

    @Override
    public List<MoratoriumDTO> getMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters) {
        return getMoratoriums(programCodes, periodFilters, false);
    }

    /* -- Internal methods -- */

    private List<MoratoriumDTO> getMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters, boolean onlyPotential) {
        List<MoratoriumDTO> result = new ArrayList<>();
        // Get program by calling get method for each, using cache
        List<ProgramDTO> programs = programCodes.stream().map(programCode -> programDao.getProgramByCode(programCode)).collect(Collectors.toList());

        programs.stream()
            .filter(program -> !program.isMoratoriumsEmpty())
            // if only potential, filter by user privilege
            .filter(program -> !onlyPotential
                || dataContext.isRecorderAdministrator()
                || DaliBeans.isProgramFullyReadable(program, dataContext.getRecorderPersonId(), dataContext.getRecorderDepartmentId())
            )
            .flatMap(program -> program.getMoratoriums().stream())
            // A moratorium period overlaps an extraction period
            .filter(moratorium ->
                moratorium.getPeriods().stream()
                    .anyMatch(moratoriumPeriod -> periodFilters.stream()
                        .anyMatch(extractionPeriod ->
                            !extractionPeriod.getStartDate().isAfter(moratoriumPeriod.getEndDate()) && !extractionPeriod.getEndDate().isBefore(moratoriumPeriod.getStartDate())                            ))
            )
            .forEach(result::add);

        return result;
    }

    /**
     * <p>toProgramDTO.</p>
     *
     * @param source a {@link fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO} object.
     * @return a {@link fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO} object.
     */
    private ProgramDTO toProgramDTO(ProgramVO source) {
        ProgramDTO target = DaliBeanFactory.newProgramDTO();
        target.setCode(source.getProgCd());
        target.setName(source.getProgNm());

        return target;
    }

    private List<ProgramDTO> toProgramDTOs(Collection<ProgramVO> sources) {
        return CollectionUtils.isEmpty(sources)
            ? null
            : sources.stream().map(this::toProgramDTO).collect(Collectors.toList());
    }

    /**
     * Save remote programs, using synchro server
     *
     * @param remoteProgramCodes a {@link java.util.Set} object.
     * @param authenticationInfo a {@link fr.ifremer.quadrige3.core.security.AuthenticationInfo} object.
     */
    private void saveProgramOnServer(AuthenticationInfo authenticationInfo, Set<String> remoteProgramCodes) {
        // check not empty
        if (CollectionUtils.isEmpty(remoteProgramCodes)) {
            return;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Sending programs [%s] to server", Joiner
                    .on(',').join(remoteProgramCodes)));
        }

        // Convert to VOs
        List<ProgramVO> programsToSend = remoteProgramCodes.stream()
                .map(progCd -> (ProgramVO) programDao.load(ProgramDao.TRANSFORM_PROGRAMVO, progCd))
                .collect(Collectors.toList());

        // Do a remote save
        List<ProgramVO> savedPrograms = synchroRestClientService.savePrograms(
                authenticationInfo,
                programsToSend);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Updating programs [%s] from server response", Joiner
                    .on(',').join(remoteProgramCodes)));
        }
        for (ProgramVO savedProgram : savedPrograms) {
            programDao.save(savedProgram);
        }
    }

    private List<ProgramDTO> filterReadablePrograms(List<ProgramDTO> programs) {

        return filterReadablePrograms(SecurityContextHelper.getQuadrigeUserId(), programs);
    }

    private List<ProgramDTO> filterWritablePrograms(List<ProgramDTO> programs) {

        return filterWritablePrograms(SecurityContextHelper.getQuadrigeUserId(), programs);
    }

    private List<ProgramDTO> filterReadablePrograms(int userId, List<ProgramDTO> programs) {

        final Set<String> allowedProgramCodes = getReadableProgramCodesByQuserId(userId);
        if (CollectionUtils.isEmpty(programs) || CollectionUtils.isEmpty(allowedProgramCodes)) {
            return new ArrayList<>();
        }

        // Filter programs with read rights for user
        return programs.stream()
                .filter(program -> program != null && program.getCode() != null && allowedProgramCodes.contains(program.getCode()))
                .collect(Collectors.toList());

    }

    private List<ProgramDTO> filterWritablePrograms(int userId, List<ProgramDTO> programs) {

        final Set<String> allowedProgramCodes = getWritableProgramCodesByQuserId(userId);
        if (CollectionUtils.isEmpty(programs) || CollectionUtils.isEmpty(allowedProgramCodes)) {
            return new ArrayList<>();
        }

        // Filter programs with write rights for user
        return programs.stream()
                .filter(program -> program != null && program.getCode() != null && allowedProgramCodes.contains(program.getCode()))
                .collect(Collectors.toList());

    }

    private ProgramDTO filterReadableProgram(ProgramDTO program) {

        final Set<String> allowedProgramCodes = getReadableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());
        if (program == null || CollectionUtils.isEmpty(allowedProgramCodes)) {
            return null;
        }
        return allowedProgramCodes.contains(program.getCode()) ? program : null;
    }

    private ProgramDTO filterWritableProgram(ProgramDTO program) {

        final Set<String> allowedProgramCodes = getWritableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());
        if (program == null || CollectionUtils.isEmpty(allowedProgramCodes)) {
            return null;
        }
        return allowedProgramCodes.contains(program.getCode()) ? program : null;
    }

    private List<ProgramDTO> filterManagedPrograms(List<ProgramDTO> programs) {

        return filterManagedPrograms(SecurityContextHelper.getQuadrigeUserId(), programs);
    }

    private List<ProgramDTO> filterManagedPrograms(int userId, List<ProgramDTO> programs) {

        final Set<String> allowedProgramCodes = getManagedProgramCodesByQuserId(userId);
        if (CollectionUtils.isEmpty(programs) || CollectionUtils.isEmpty(allowedProgramCodes)) {
            return new ArrayList<>();
        }

        // Filter programs with manager rights for user (or local program)
        return programs.stream()
                .filter(program -> program != null
                                && (
                                (program.getCode() != null && allowedProgramCodes.contains(program.getCode()))
                                        || QuadrigeBeans.isLocalStatus(program.getStatus())
                        )
                )
                .collect(Collectors.toList());

    }
}
