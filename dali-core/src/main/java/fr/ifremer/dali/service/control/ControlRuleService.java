package fr.ifremer.dali.service.control;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Rules control service.
 */
public interface ControlRuleService {

    /**
     * <p>controlSurveys.</p>
     *
     * @param surveys a {@link Collection} object.
     * @param updateControlDateWhenSucceed Should update control date, when control succeed for a survey ?
     * @param resetControlDateWhenFailed Should reset to NULL the control date, when control failed for a survey ?
     * @param progressionModel progression model
     * @return a {@link fr.ifremer.dali.service.control.ControlRuleMessagesBean} object.
     */
    @Transactional()
    ControlRuleMessagesBean controlSurveys(Collection<? extends SurveyDTO> surveys,
                                           boolean updateControlDateWhenSucceed,
                                           boolean resetControlDateWhenFailed, ProgressionCoreModel progressionModel);

    /**
     * <p>controlSurvey.</p>
     *
     * @param survey a {@link fr.ifremer.dali.dto.data.survey.SurveyDTO} object.
     * @param updateControlDateWhenSucceed Should update control date, when control succeed ?
     * @param resetControlDateWhenFailed Should reset to NULL the control date, when control failed ?
     * @return a {@link fr.ifremer.dali.service.control.ControlRuleMessagesBean} object.
     */
    @Transactional()
    ControlRuleMessagesBean controlSurvey(SurveyDTO survey,
                                          boolean updateControlDateWhenSucceed,
                                          boolean resetControlDateWhenFailed);

    boolean controlUniqueObject(ControlRuleDTO rule, Object objectToControl);
}
