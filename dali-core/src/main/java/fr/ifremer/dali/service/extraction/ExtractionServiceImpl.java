package fr.ifremer.dali.service.extraction;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.system.extraction.DaliExtractionDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.enums.ExtractionFilterTypeValues;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.dali.service.administration.context.ContextService;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.util.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Extraction Service
 * <p/>
 * Created by Ludovic on 02/12/2015.
 */
@Service("daliExtractionService")
public class ExtractionServiceImpl implements ExtractionService {

    @Resource(name = "daliContextService")
    private ContextService contextService;

    @Resource(name = "daliExtractionDao")
    private DaliExtractionDao extractionDao;

    @Autowired
    protected DaliConfiguration config;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterTypeDTO> getFilterTypes() {
        List<FilterTypeDTO> result = Lists.newArrayList();

        // add list from ExtractionFilterTypeValues
        for (ExtractionFilterTypeValues extractionFilter : ExtractionFilterTypeValues.values()) {
            if (extractionFilter.isHidden()) continue;
            FilterTypeDTO filterType = DaliBeanFactory.newFilterTypeDTO();
            filterType.setId(extractionFilter.getFilterTypeId());
            filterType.setName(extractionFilter.getLabel());
            result.add(filterType);
        }

        return result;
    }

    @Override
    public List<ExtractionDTO> getAllLightExtractions() {
        return extractionDao.getAllLightExtractions();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionDTO> getExtractions(Integer extractionId, String programCode) {
        if (extractionId != null) {
            return Lists.newArrayList(extractionDao.getExtractionById(extractionId));
        } else if (programCode != null) {
            return extractionDao.searchExtractionByProgram(programCode);
        } else {
            return extractionDao.getAllExtractions();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadFilteredElements(ExtractionDTO extraction) {
        Assert.notNull(extraction);

        extraction.getFilters().stream()
                .filter(filter -> filter.getFilterTypeId() >= 0 && !filter.isFilterLoaded())
                .forEach(filter -> contextService.loadFilteredElements(filter));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveExtractions(Collection<? extends ExtractionDTO> extractions) {

        if (CollectionUtils.isEmpty(extractions)) return;

        extractions.stream().filter(ExtractionDTO::isDirty).forEach(this::saveExtraction);

    }

    private void saveExtraction(ExtractionDTO extraction) {
        Assert.notNull(extraction);
        Assert.notEmpty(extraction.getFilters());
        Assert.notNull(DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.PERIOD));
        Assert.notNull(DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.PROGRAM));
        Assert.notNull(DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.ORDER_ITEM_TYPE));

        // save extraction
        extractionDao.saveExtraction(extraction);

        extraction.setDirty(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteExtractions(List<Integer> idExtractionToDelete) {

        if (CollectionUtils.isEmpty(idExtractionToDelete)) return;
        idExtractionToDelete.stream().filter(Objects::nonNull).distinct().forEach(id -> extractionDao.remove(id));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtractionDTO duplicateExtraction(ExtractionDTO extraction) throws CloneNotSupportedException {
        ExtractionDTO duplicatedExtraction = DaliBeanFactory.newExtractionDTO();

        duplicatedExtraction.setUser(extraction.getUser());
        duplicatedExtraction.setDirty(true);

        // filters
        for (FilterDTO filter: extraction.getFilters()) {
            duplicatedExtraction.addFilters(contextService.duplicateFilter(filter));
        }

        // result configuration
        duplicatedExtraction.setParameter(ObjectUtil.deepClone(extraction.getParameter()));

        return duplicatedExtraction;
    }

    @Override
    public void exportExtraction(ExtractionDTO extraction, File exportFile) {

        Assert.notNull(extraction);
        Assert.notNull(exportFile);
        exportFile.getParentFile().mkdirs();

        extractionDao.exportExtraction(extraction, exportFile);
    }

    @Override
    public ExtractionDTO importExtraction(File importFile) {

        Assert.notNull(importFile);

        return extractionDao.importExtraction(importFile);
    }

}
