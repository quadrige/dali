package fr.ifremer.dali.service.administration.user;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.PrivilegeDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * <p>UserService interface.</p>
 *
 */
@Transactional(readOnly = true)
public interface UserService extends fr.ifremer.quadrige3.core.service.administration.user.UserService {

    /**
     * <p>getDepartmentIdByUserId.</p>
     *
     * @param userId a int.
     * @return a {@link java.lang.Integer} object.
     */
    Integer getDepartmentIdByUserId(int userId);

    /**
     * <p>getActiveUsers.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> getActiveUsers();

    PersonDTO getUser(int userId);

    /**
     * Search User.
     *
     * @param searchCriteria Search parameter
     * @return User list
     */
    List<PersonDTO> searchUser(PersonCriteriaDTO searchCriteria);
    
    /**
     * <p>getAllPrivileges.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PrivilegeDTO> getAllPrivileges();

    /**
     * <p>getAvailablePrivileges.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PrivilegeDTO> getAvailablePrivileges();

    /**
     * <p>getPrivilegesByUser.</p>
     *
     * @param userId a {@link java.lang.Integer} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<PrivilegeDTO> getPrivilegesByUser(Integer userId);

}
