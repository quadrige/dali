package fr.ifremer.dali.service.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.ParameterTypeDTO;
import fr.ifremer.dali.dto.configuration.filter.department.DepartmentCriteriaDTO;
import fr.ifremer.dali.dto.configuration.filter.location.LocationCriteriaDTO;
import fr.ifremer.dali.dto.configuration.filter.taxon.TaxonCriteriaDTO;
import fr.ifremer.dali.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO;
import fr.ifremer.dali.dto.referential.*;
import fr.ifremer.dali.dto.referential.pmfm.*;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Les sevices pour les donnees de reference.
 */
@Transactional(readOnly = true)
public interface ReferentialService {

    // ---------- SamplingEquipmentDTO ----------

    /**
     * La liste des engins du référentiel.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return La liste des engins
     */
    List<SamplingEquipmentDTO> getSamplingEquipments(StatusFilter statusFilter);

    /**
     * The search service for sampling equipment
     *
     * @param statusFilter        a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param samplingEquipmentId a {@link java.lang.Integer} object.
     * @param statusCode          a {@link java.lang.String} object.
     * @param unitId              a {@link java.lang.Integer} object.
     * @return A list of sampling equipment
     */
    List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, Integer samplingEquipmentId, String statusCode, Integer unitId);

    /**
     * <p>searchSamplingEquipments.</p>
     *
     * @param statusFilter  a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param equipmentName a {@link java.lang.String} object.
     * @param statusCode    a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, String equipmentName, String statusCode);

    // ---------- UnitDTO ----------

    /**
     * La liste des unites du referential.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return La liste des unites du referential
     */
    List<UnitDTO> getUnits(StatusFilter statusFilter);

    /**
     * The search service for unit
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param unitId       a {@link java.lang.Integer} object.
     * @param statusCode   a {@link java.lang.String} object.
     * @return A list of unit
     */
    List<UnitDTO> searchUnits(StatusFilter statusFilter, Integer unitId, String statusCode);

    // ---------- PersonDTO ----------

    /**
     * wrapped method used in referential replacement
     *
     * @param person a {@link fr.ifremer.dali.dto.referential.PersonDTO} object.
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> getUsersInSameDepartment(PersonDTO person);

    // ---------- DepartmentDTO ----------

    /**
     * <p>getDepartments.</p>
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return a {@link java.util.List} object.
     */
    List<DepartmentDTO> getDepartments(StatusFilter statusFilter);

    /**
     * <p>getDepartmentById.</p>
     *
     * @param departmentId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.DepartmentDTO} object.
     */
    DepartmentDTO getDepartmentById(int departmentId);

    /**
     * Search department.
     *
     * @param searchCriteria a {@link fr.ifremer.dali.dto.configuration.filter.department.DepartmentCriteriaDTO} object.
     * @return Department list
     */
    List<DepartmentDTO> searchDepartments(DepartmentCriteriaDTO searchCriteria);

    // ---------- LocationDTO ----------

    /**
     * Recupere la liste des lieux du referential
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return une liste de lieux du referential
     */
    List<LocationDTO> getLocations(StatusFilter statusFilter);

    /**
     * le lieu selon sont identifiant
     *
     * @param locationId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.LocationDTO} object.
     */
    LocationDTO getLocation(int locationId);

    /**
     * La liste des lieu selectionne suivant une campagne et un programme.
     *
     * @param campaignId  identifiant de la camapgne
     * @param programCode code du programme selectionne
     * @param activeOnly
     * @return La liste des lieux selectionnes
     */
    List<LocationDTO> getLocations(Integer campaignId, String programCode, boolean activeOnly);

    /**
     * <p>searchLocations.</p>
     *
     * @param searchCriteria a {@link fr.ifremer.dali.dto.configuration.filter.location.LocationCriteriaDTO} object.
     * @return A list of Referential Location
     */
    List<LocationDTO> searchLocations(LocationCriteriaDTO searchCriteria);

    /**
     * <p>getHarbours.</p>
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return a {@link java.util.List} object.
     */
    List<HarbourDTO> getHarbours(StatusFilter statusFilter);

    // ---------- TaxonGroupDTO ----------

    /**
     * La liste des groupes de taxons.
     *
     * @return La liste des groupe de taxons
     */
    List<TaxonGroupDTO> getTaxonGroups();

    /**
     * <p>getTaxonGroup.</p>
     *
     * @param taxonGroupId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.TaxonGroupDTO} object.
     */
    TaxonGroupDTO getTaxonGroup(int taxonGroupId);

    /**
     * Reload the initial taxon group list from cache (or db)
     *
     * @param taxonGroups a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<TaxonGroupDTO> getFullTaxonGroups(List<TaxonGroupDTO> taxonGroups);

    /**
     * <p>searchTaxonGroups.</p>
     *
     * @param searchCriteria a {@link fr.ifremer.dali.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO} object.
     * @return a {@link java.util.List} object.
     */
    List<TaxonGroupDTO> searchTaxonGroups(TaxonGroupCriteriaDTO searchCriteria);

    // ---------- TaxonDTO ----------

    /**
     * List of taxon name. Could be filtered by parent taxon group
     *
     * @param taxonGroupId the parent taxon group, or null for all taxons
     * @return list of taxon names
     */
    List<TaxonDTO> getTaxons(Integer taxonGroupId);

    /**
     * <p>getTaxon.</p>
     *
     * @param taxonId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.TaxonDTO} object.
     */
    TaxonDTO getTaxon(int taxonId);

    /**
     * <p>searchTaxons.</p>
     *
     * @param taxonCriteria search criteria
     * @return a {@link java.util.List} object.
     */
    List<TaxonDTO> searchTaxons(TaxonCriteriaDTO taxonCriteria);

    /**
     * Fill all properties of the taxon list
     *
     * @param taxons a {@link java.util.List} object.
     */
    void fillTaxonsProperties(List<TaxonDTO> taxons);

    /**
     * Fill referent taxon of the taxon list
     *
     * @param taxons a {@link java.util.List} object.
     */
    void fillReferentTaxons(List<TaxonDTO> taxons);

    /**
     * <p>getTaxonomicLevels.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<TaxonomicLevelDTO> getTaxonomicLevels();

    /**
     * <p>getCitations.</p>
     *
     * @return list of taxon citations
     */
    List<CitationDTO> getCitations();

    // ---------- ParameterGroupDTO ----------

    /**
     * All Pmfm parameter group.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return Pmfm parameter group list
     */
    List<ParameterGroupDTO> getParameterGroup(StatusFilter statusFilter);

    // ---------- ParameterDTO ----------

    /**
     * La liste des parametres.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return Les parametres
     */
    List<ParameterDTO> getParameters(StatusFilter statusFilter);

    /**
     * All Pmfm parameter.
     *
     * @param statusFilter   a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param parameterCode  a {@link java.lang.String} object.
     * @param statusCode     a {@link java.lang.String} object.
     * @param parameterGroup a {@link fr.ifremer.dali.dto.referential.pmfm.ParameterGroupDTO} object.
     * @return Pmfm Parameter list
     */
    List<ParameterDTO> searchParameters(StatusFilter statusFilter, String parameterCode, String statusCode, ParameterGroupDTO parameterGroup);

    /**
     * The parameterType list
     *
     * @return the parameterTypes
     */
    List<ParameterTypeDTO> getParameterTypes();

    // ---------- MatrixDTO ----------

    /**
     * La liste des supports.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return Les supports
     */
    List<MatrixDTO> getMatrices(StatusFilter statusFilter);

    /**
     * All Pmfm matrice.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param matrixId     a {@link java.lang.Integer} object.
     * @param statusCode   a {@link java.lang.String} object.
     * @return Pmfm Matrice list
     */
    List<MatrixDTO> searchMatrices(StatusFilter statusFilter, Integer matrixId, String statusCode);

    // ---------- FractionDTO ----------

    /**
     * La liste des fractions.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return Les fractions
     */
    List<FractionDTO> getFractions(StatusFilter statusFilter);

    /**
     * All Pmfm fraction.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param fractionId   a {@link java.lang.Integer} object.
     * @param statusCode   a {@link java.lang.String} object.
     * @return Pmfm Fraction list
     */
    List<FractionDTO> searchFractions(StatusFilter statusFilter, Integer fractionId, String statusCode);

    // ---------- MethodDTO ----------

    /**
     * La liste des methodes.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return Les methodes
     */
    List<MethodDTO> getMethods(StatusFilter statusFilter);

    /**
     * All Pmfm method.
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param methodId     a {@link java.lang.Integer} object.
     * @param statusCode   a {@link java.lang.String} object.
     * @return Pmfm Method list
     */
    List<MethodDTO> searchMethods(StatusFilter statusFilter, Integer methodId, String statusCode);

    // ---------- PmfmDTO ----------

    /**
     * The quadruplet list
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return the quadruplets
     */
    List<PmfmDTO> getPmfms(StatusFilter statusFilter);

    /**
     * <p>getPmfm.</p>
     *
     * @param pmfmId a int.
     * @return a {@link fr.ifremer.dali.dto.referential.pmfm.PmfmDTO} object.
     */
    PmfmDTO getPmfm(int pmfmId);

    /**
     * <p>searchPmfms.</p>
     *
     * @param statusFilter  a {@link StatusFilter} object.
     * @param parameterCode a {@link String} object.
     * @param matrixId      a {@link Integer} object.
     * @param fractionId    a {@link Integer} object.
     * @param methodId      a {@link Integer} object.
     * @param unitId
     * @param pmfmName      a {@link String} object.
     * @param statusCode    a {@link String} object.
     * @return a {@link java.util.List} object.
     */
    List<PmfmDTO> searchPmfms(StatusFilter statusFilter, String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, String statusCode);

    /**
     * Return the unique pmfm id from the quadruplet in the parameter pmfm
     *
     * @param pmfm the pmfm filter
     * @return the pmfm id
     */
    Integer getUniquePmfmIdFromPmfm(PmfmDTO pmfm);

    /**
     * Return the unique pmfm from the quadruplet in the parameter pmfm
     *
     * @param pmfm the pmfm filter
     * @return the pmfm
     */
    PmfmDTO getUniquePmfmFromPmfm(PmfmDTO pmfm);

    // ---------- QualitativeValueDTO ----------

    /**
     * Get the qualitative value by its Id
     *
     * @param qualitativeValueId the qualitative value id
     * @return Qualitative Value
     */
    QualitativeValueDTO getQualitativeValue(int qualitativeValueId);

    List<QualitativeValueDTO> getQualitativeValues(Collection<Integer> qualitativeValueIds);

    // ---------- AnalysisInstrumentDTO ----------

    /**
     * The list of Analysis Instruments
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return The list of Analysis Instruments
     */
    List<AnalysisInstrumentDTO> getAnalysisInstruments(StatusFilter statusFilter);

    /**
     * The search service for analysis instrument
     *
     * @param statusFilter         a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param analysisInstrumentId a {@link java.lang.Integer} object.
     * @param statusCode           a {@link java.lang.String} object.
     * @return A list of analysis instruments
     */
    List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, Integer analysisInstrumentId, String statusCode);

    /**
     * <p>searchAnalysisInstruments.</p>
     *
     * @param statusFilter   a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @param instrumentName a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, String instrumentName);

    // ---------- MISC. ----------

    /**
     * La liste des systemes de positionnement
     *
     * @return La liste des systemes de positionnement
     */
    List<PositioningSystemDTO> getPositioningSystems();

    /**
     * La liste des niveaux.
     *
     * @return La liste des niveaux
     */
    List<LevelDTO> getLevels();

    /**
     * The list of referential status
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return the list of referential status
     */
    List<StatusDTO> getStatus(StatusFilter statusFilter);

    QualityLevelDTO getNotQualityLevel();

    /**
     * The list of Quality Levels
     *
     * @param statusFilter a {@link fr.ifremer.dali.service.StatusFilter} object.
     * @return The list of Quality Levels
     */
    List<QualityLevelDTO> getQualityLevels(StatusFilter statusFilter);

    /**
     * The service for grouping type
     *
     * @return A list of grouping type
     */
    List<GroupingTypeDTO> getGroupingTypes();

    /**
     * <p>getPhotoTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PhotoTypeDTO> getPhotoTypes();

}
