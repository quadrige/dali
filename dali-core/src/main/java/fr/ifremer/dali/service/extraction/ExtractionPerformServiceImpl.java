package fr.ifremer.dali.service.extraction;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.administration.program.DaliProgramDao;
import fr.ifremer.dali.dao.administration.strategy.DaliStrategyDao;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliPmfmDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliQualitativeValueDao;
import fr.ifremer.dali.dao.system.extraction.DaliExtractionResultDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.dali.dto.configuration.moratorium.MoratoriumPeriodDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.enums.ExtractionFilterTypeValues;
import fr.ifremer.dali.dto.enums.ExtractionOutputType;
import fr.ifremer.dali.dto.referential.GroupingTypeDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionContextDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPmfmInfoDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.service.*;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.dali.service.control.RuleListService;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.core.dao.technical.decorator.DecoratorComparator;
import fr.ifremer.quadrige3.core.dao.technical.factorization.Combination;
import fr.ifremer.quadrige3.core.dao.technical.factorization.CombinationList;
import fr.ifremer.quadrige3.core.dao.technical.factorization.IntegerPair;
import fr.ifremer.quadrige3.core.dao.technical.factorization.MaxCombinationExceededException;
import fr.ifremer.quadrige3.core.dao.technical.xmlQuery.XMLQuery;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Element;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Extraction Perform Service
 *
 * @author peck7 on 23/11/2017.
 */
@Service("daliExtractionPerformService")
public class ExtractionPerformServiceImpl implements ExtractionPerformService {

    private static final Log LOG = LogFactory.getLog(ExtractionPerformServiceImpl.class);

    private static final String TABLE_NAME_PREFIX = "EXT_";
    private static final String BASE_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "B%s";
    private static final String RAW_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "R%s";
    private static final String PMFM_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "P%s_%s";
    private static final String COMMON_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "C%s";
    private static final String RESULT_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "E%s";

    private static final String XML_QUERY_PATH = "xmlQuery/extraction";

    @Resource(name = "daliExtractionService")
    protected ExtractionService extractionService;

    @Resource(name = "daliRuleListService")
    private RuleListService ruleListService;

    @Resource(name = "daliExtractionResultDao")
    private DaliExtractionResultDao extractionResultDao;

    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;

    @Resource(name = "daliQualitativeValueDao")
    private DaliQualitativeValueDao qualitativeValueDao;

    @Resource(name = "daliProgramStrategyService")
    private ProgramStrategyService programStrategyService;

    @Resource(name = "daliProgramDao")
    private DaliProgramDao programDao;

    @Resource(name = "daliStrategyDao")
    private DaliStrategyDao strategyDao;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliDataContext")
    protected DaliDataContext dataContext;

    /**
     * {@inheritDoc}
     */
    @Override
    public void performExtraction(ExtractionDTO extraction, ExtractionOutputType outputType, File outputFile, ProgressionCoreModel progressionModel) {

        Assert.notNull(extraction);
        Assert.notNull(extraction.getId());
        Assert.notEmpty(extraction.getFilters());
        Assert.notNull(DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.PERIOD));
        Assert.notNull(DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.ORDER_ITEM_TYPE));
        Assert.notNull(DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.PROGRAM));
        Assert.notNull(outputType);
        if (isAggregated(outputType)) {
            Assert.notNull(extraction.getParameter());
            Assert.notEmpty(extraction.getParameter().getPmfmPresets());
            Assert.notEmpty(extraction.getParameter().getPmfmResults());
        }
        Assert.notNull(outputFile);

        // ensure all filters are loaded
        extractionService.loadFilteredElements(extraction);

        // init progression model
        progressionModel.setMessage("");
        progressionModel.setTotal(isAggregated(outputType) ? 13 : 11);
        long startTime = System.currentTimeMillis();

        // Get programs
        List<String> programCodes = DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.PROGRAM);
        Assert.notNull(programCodes);
        // Filter by user privileges (Mantis #59187)
        Collection<String> allowedProgramCodes = programStrategyService.getReadableProgramCodesByQuserId(dataContext.getRecorderPersonId());
        programCodes.retainAll(allowedProgramCodes);

        if (programCodes.isEmpty()) {
            throw new DaliBusinessException(t("dali.service.extraction.noData.error"));
        }

        List<ProgramDTO> programs = programCodes.stream()
            // Load program with moratorium
            .map(programCode -> programDao.getProgramByCode(programCode))
            .collect(Collectors.toList());

        // Extraction context
        ExtractionContextDTO context = DaliBeanFactory.newExtractionContextDTO();
        context.setExtraction(extraction);
        context.setPeriods(DaliBeans.getExtractionPeriods(extraction));
        context.setPrograms(programs);
        context.setUniqueId(System.currentTimeMillis());
        context.setBaseTableName(String.format(BASE_TABLE_NAME_PATTERN, context.getUniqueId()));
        context.setRawTableName(String.format(RAW_TABLE_NAME_PATTERN, context.getUniqueId()));
        context.setCommonTableName(String.format(COMMON_TABLE_NAME_PATTERN, context.getUniqueId()));
        context.setResultTableName(String.format(RESULT_TABLE_NAME_PATTERN, context.getUniqueId()));

        if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Beginning a %s extraction (id=%s) with:", outputType, extraction.getId()));
            LOG.info(String.format("\t date ranges: %s", DaliBeans.toString(context.getPeriods())));
            LOG.info(String.format("\tgéo grouping: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.ORDER_ITEM_TYPE)));
            LOG.info(String.format("\t    programs: %s", getProgramCodes(context)));
            LOG.info(String.format("\t   locations: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.LOCATION)));
            LOG.info(String.format("\t   campaigns: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.CAMPAIGN)));
            LOG.info(String.format("\t   equipment: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.SAMPLING_EQUIPMENT)));
            LOG.info(String.format("\t departments: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.DEPARTMENT)));
            LOG.info(String.format("\t      taxons: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.TAXON)));
            LOG.info(String.format("\ttaxon groups: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.TAXON_GROUP)));
            LOG.info(String.format("\t       pmfms: %s", DaliBeans.getFilterElementsIds(extraction, ExtractionFilterTypeValues.PMFM)));
            if (isAggregated(outputType)) {
                LOG.info(String.format("\tpmfms preset: %s", extraction.getParameter().getPmfmPresets().stream()
                        .map(preset -> String.format("pmfm_id:%s with qv_ids:%s", preset.getPmfm().getId(), preset.getQualitativeValues().stream()
                                .map(QualitativeValueDTO::getId).collect(Collectors.toList()))).collect(Collectors.toList())));
                LOG.info(String.format("\tpmfms result: %s", extraction.getParameter().getPmfmResults().stream()
                        .map(PmfmDTO::getId).collect(Collectors.toList())));
            }
        }

        try {

            try {

                // create the concat functions used to concat distinct strings (ex: DEP_NM)
                createConcatDistinctFunction("MEAS_CM", "VARCHAR(2000)");
                createConcatDistinctFunction("DEP_NM", "VARCHAR(255)");

                // STEP 1 : Create the base table
                long nbRowsInserted = createBaseTable(context, outputType);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s sampling operations have to be extract (temp table : %s)", nbRowsInserted, context.getBaseTableName()));
                } else {
                    LOG.info(String.format("%s sampling operations have to be extract", nbRowsInserted));
                }
                if (nbRowsInserted == 0) {
                    throw new DaliBusinessException(t("dali.service.extraction.noData.error"));
                }

                // STEP 1b : Remove surveys under moratorium
                int nbRowsRemoved = cleanSurveysUnderMoratorium(context);
                progressionModel.increments(1);
                nbRowsInserted -= nbRowsRemoved;
                if (LOG.isDebugEnabled() && nbRowsRemoved > 0) {
                    LOG.debug(String.format("%s rows removed from base data which are under moratorium", nbRowsRemoved));
                }
                // if all the rows have been removed because of moratoriums, there is no more data to extract
                if (nbRowsInserted == 0) {
                    throw new DaliBusinessException(t("dali.service.extraction.noData.error"));
                }

                // STEP 2 : Create the raw table with full measurement's data in line
                nbRowsInserted = createRawTable(context, outputType);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s rows of raw data (temp table : %s)", nbRowsInserted, context.getRawTableName()));
                } else {
                    LOG.info(String.format("%s rows of raw data", nbRowsInserted));
                }

                // STEP 5a : Clean data
                nbRowsRemoved = cleanTaxonData(context);
                progressionModel.increments(1);
                nbRowsInserted -= nbRowsRemoved;
                if (LOG.isDebugEnabled() && nbRowsRemoved > 0) {
                    LOG.debug(String.format("%s rows removed from raw data which not corresponding to taxon or taxon group filter", nbRowsRemoved));
                }

                // STEP 5b : Second clean operation: remove surveys from hermetic programs if user is not allowed (Mantis #42817)
                nbRowsRemoved = cleanDataUnderMoratorium(context);
                progressionModel.increments(1);
                nbRowsInserted -= nbRowsRemoved;
                if (LOG.isDebugEnabled() && nbRowsRemoved > 0) {
                    LOG.debug(String.format("%s rows removed from raw data which are under moratorium", nbRowsRemoved));
                }

                // if all the rows have been removed because of filters, there is no more data to extract
                if (nbRowsInserted == 0) {
                    throw new DaliBusinessException(t("dali.service.extraction.noData.error"));
                }

                // STEP 3 : Build PMFM metadata
                buildPmfmInformation(context, outputType);
                progressionModel.increments(1);

                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("list of pmfm ids to split : %s", DaliBeans.collectProperties(context.getPmfmInfos(), ExtractionPmfmInfoDTO.PROPERTY_PMFM_ID)));
                }

                // STEP 4 : Build qualitative values combinations from pmfm presets
                CombinationList combinations = null;
                if (isAggregated(outputType)) {
                    try {
                        combinations = buildCombinations(context);
                    } catch (MaxCombinationExceededException e) {
                        throw new DaliBusinessException(t("dali.service.extraction.maxCombinationsExceeded.error", config.getGridInitializationMaxCombinationCount()));
                    }
                    progressionModel.increments(1);

                    // Now factorizedPmfmPresets contains only valid combinations
                    if (CollectionUtils.isEmpty(combinations)) {
                        throw new DaliBusinessException(t("dali.service.extraction.noCombination.error"));
                    }
                }

                // STEP 6 : Create pmfm tables
                createPmfmTables(context);
                progressionModel.increments(1);

                // STEP 7 : Create tables with pmfm aggregation
                Multimap<ExtractionPmfmInfoDTO, Combination> pmfmResultInfos = null;
                if (isAggregated(outputType)) {
                    pmfmResultInfos = createPmfmResultTables(context, combinations);
                    progressionModel.increments(1);
                }

                Map<String, String> fieldNamesByAlias = Maps.newHashMap();
                Map<String, String> decimalFormats = Maps.newHashMap();
                Map<String, String> dateFormats = Maps.newHashMap();

                // STEP 8 : Create common table (with non individual measurements)
                createCommonTable(context, outputType, fieldNamesByAlias, decimalFormats, dateFormats);
                progressionModel.increments(1);

                // STEP 9 : Create result table
                nbRowsInserted = isAggregated(outputType)
                        ? createAggregatedResultTable(context, pmfmResultInfos, fieldNamesByAlias, decimalFormats, dateFormats)
                        : createResultTable(context, fieldNamesByAlias, decimalFormats, dateFormats);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s rows to write (result table : %s)", nbRowsInserted, context.getResultTableName()));
                } else {
                    LOG.info(String.format("%s rows to write", nbRowsInserted));
                }

                // STEP 9b : Add moratorium info
                int nbRowsUpdated = updateResultTable(context);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s rows updated with moratorium information", nbRowsUpdated));
                }

                // STEP 10 : Final query and write to csv file
                writeExtraction(context, outputType, fieldNamesByAlias, decimalFormats, dateFormats, outputFile);
                progressionModel.increments(1);

                if (LOG.isInfoEnabled()) {
                    long time = System.currentTimeMillis() - startTime;
                    LOG.info(String.format("Extraction %s performed in %s. result file is : %s", outputType, Times.durationToString(time), outputFile.getAbsolutePath()));
                }

            } catch (DaliBusinessException e) {
                throw e; // throw directly
            } catch (Exception e) {
                throw new DaliTechnicalException(t("dali.service.extraction.error"), e);
            }
        } finally {

            // drop concat functions
            dropConcatDistinctFunction("MEAS_CM");
            dropConcatDistinctFunction("DEP_NM");
        }
    }

    private void buildPmfmInformation(ExtractionContextDTO context, ExtractionOutputType outputType) {

        // Gather pmfm from raw table
        List<ExtractionPmfmInfoDTO> pmfmInfos = getPmfmInfo(context);

        // Get pmfm ids from extraction parameter
        Set<Integer> pmfmIdsFromParameter = new HashSet<>();
        if (isAggregated(outputType)) {
            pmfmIdsFromParameter.addAll(context.getExtraction().getParameter().getPmfmPresets().stream().map(pmfmPreset -> pmfmPreset.getPmfm().getId()).collect(Collectors.toSet()));
            pmfmIdsFromParameter.addAll(context.getExtraction().getParameter().getPmfmResults().stream().map(PmfmDTO::getId).collect(Collectors.toSet()));
        }

        // Build pmfm filter depending on output type
        Predicate<ExtractionPmfmInfoDTO> pmfmInfoPredicate = isAggregated(outputType)
                ?
                pmfmInfo -> {
                    // Remove pmfm on sampling operation not in pmfm preset
                    // And clean potential error on non individual pmfm without individual number
                    // for example survey_lb='NH2014-02' from 'DECHETS_FLOTTANTS' have pmfm=13171 (nb déchets) on sampling operation without MEAS_INDIV_ID
                    return
                            // keep survey non individual measurements
                            (pmfmInfo.isSurvey() && !pmfmInfo.isIndividual())
                                    // keep sampling operation individual measurements allowed by parameter
                                    || (!pmfmInfo.isSurvey() && pmfmInfo.isIndividual() && pmfmIdsFromParameter.contains(pmfmInfo.getPmfmId()))
                                    //  keep sampling operation non individual measurements not preset in parameter
                                    || (!pmfmInfo.isSurvey() && !pmfmInfo.isIndividual() && !pmfmIdsFromParameter.contains(pmfmInfo.getPmfmId()));
                }
                :
                pmfmInfo -> {
                    return
                            // keep survey non individual measurements and all sampling operation measurements
                            !pmfmInfo.isSurvey() || !pmfmInfo.isIndividual();
                };

        // Filter pmfms
        pmfmInfos = pmfmInfos.stream().filter(pmfmInfoPredicate).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(pmfmInfos)) {
            throw new DaliBusinessException(t("dali.service.extraction.noPmfm.error"));
        }

        // Affect to context
        context.setPmfmInfos(pmfmInfos);

        // Add missing pmfm info from extraction parameter
        pmfmIdsFromParameter.forEach(pmfmId -> {
            ExtractionPmfmInfoDTO pmfmInfo = findPmfmInfo(context.getPmfmInfos(), pmfmId);
            if (pmfmInfo == null) {
                context.addPmfmInfos(newPmfmInfo(context, pmfmId, false, true));
            }
        });

        // Get PMFM strategies corresponding to extraction filters and compute pmfm sort order
        Set<PmfmStrategyDTO> allPmfmStrategies = context.getPeriods().stream()
                .collect(HashSet::new,
                        (pmfmStrategies, period) ->
                                pmfmStrategies.addAll(strategyDao.getPmfmStrategiesByProgramCodesAndDates(getProgramCodes(context), period.getStartDate(), period.getEndDate())),
                        HashSet::addAll);

        Map<Integer, Integer> rankOrdersByPmfmId = allPmfmStrategies.stream()
                .collect(HashMap::new, (map, pmfmStrategy) -> map.put(pmfmStrategy.getPmfm().getId(), pmfmStrategy.getRankOrder()), HashMap::putAll);

        // Affect rank order
        context.getPmfmInfos().forEach(pmfmInfo -> {
                    Integer rankOrder = rankOrdersByPmfmId.get(pmfmInfo.getPmfmId());
                    // put the pmfm outside a strategy at the end (Mantis #43695)
                    pmfmInfo.setRankOrder(rankOrder != null ? rankOrder : Integer.MAX_VALUE);
                }
        );
    }

    private CombinationList buildCombinations(ExtractionContextDTO context) throws MaxCombinationExceededException {

        // Sort the Pmfm presets
        List<PmfmPresetDTO> orderedPmfmPresets = context.getExtraction().getParameter().getPmfmPresets().stream()
                .filter(pmfmPreset -> !pmfmPreset.isQualitativeValuesEmpty())
                .sorted(Comparator.comparingInt(preset -> getPmfmInfo(context.getPmfmInfos(), preset.getPmfm().getId()).getRankOrder()))
                .collect(Collectors.toList());

        // Sort the qualitative values in each orderedPmfmPresets
        DecoratorService decoratorService = DaliServiceLocator.instance().getDecoratorService();
        if (decoratorService != null)
            orderedPmfmPresets.forEach(pmfmPresetDTO -> pmfmPresetDTO.getQualitativeValues().sort(
                new DecoratorComparator<>(decoratorService.getDecoratorByType(QualitativeValueDTO.class))));

        // Abort if nothing to do
        if (CollectionUtils.isEmpty(orderedPmfmPresets)) return null;

        // Get preconditioned rules for programs
        List<ControlRuleDTO> preconditionedRules = ruleListService.getPreconditionedControlRulesForProgramCodes(getProgramCodes(context));
        Multimap<Integer, PreconditionRuleDTO> preconditionRulesByPmfmId = HashMultimap.create();

        // Build a map of all preconditions by pmfm id
        if (CollectionUtils.isNotEmpty(preconditionedRules)) {
            for (ControlRuleDTO preconditionedRule : preconditionedRules) {
                for (PreconditionRuleDTO precondition : preconditionedRule.getPreconditions()) {

                    int basePmfmId = precondition.getBaseRule().getRulePmfms(0).getPmfm().getId();
                    int usedPmfmId = precondition.getUsedRule().getRulePmfms(0).getPmfm().getId();

                    preconditionRulesByPmfmId.put(basePmfmId, precondition);
                    if (precondition.isBidirectional())
                        preconditionRulesByPmfmId.put(usedPmfmId, precondition);
                }

            }
        }

        CombinationList combinations = ruleListService.buildAndFactorizeAllowedValues(
                orderedPmfmPresets, preconditionRulesByPmfmId,
                config.getGridInitializationMaxCombinationCount());

        // Add unique combination id
        AtomicInteger combinationId = new AtomicInteger();
        combinations.forEach(combination -> combination.setId(combinationId.incrementAndGet()));

        return combinations;
    }

    private long createBaseTable(ExtractionContextDTO context, ExtractionOutputType outputType) {

        XMLQuery xmlQuery = createXMLQuery("createBaseTable");
        xmlQuery.bind("baseTableName", context.getBaseTableName());
        xmlQuery.bind("orderItemTypeCode", getOrderItemTypeCode(context.getExtraction()));

        // active groups depending the output type
        xmlQuery.setGroup("complete", isComplete(outputType));

        // add mandatory period filter
        Element periodFilter = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, "periodFilter");
        Assert.notNull(periodFilter);
        for (int i = 0; i < context.getPeriods().size(); i++) {
            XMLQuery periodFilterQuery = createXMLQuery("injectionPeriodFilter");
            if (i > 0) {
                periodFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            String periodAlias = "PERIOD" + i;
            periodFilterQuery.replaceAllBindings("PERIOD", periodAlias);
            periodFilter.addContent(periodFilterQuery.getDocument().getRootElement().detach());
            xmlQuery.bind(periodAlias + "_startDate", formatDate(context.getPeriods().get(i).getStartDate()));
            xmlQuery.bind(periodAlias + "_endDate", formatDate(context.getPeriods().get(i).getEndDate()));
        }

        // add mandatory program filter
        xmlQuery.bind("progCodes", Daos.getInStatementFromStringCollection(getProgramCodes(context)));

        // add monitoring location filter
        List<Integer> locationIds = DaliBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterTypeValues.LOCATION);
        xmlQuery.setGroup("locationFilter", CollectionUtils.isNotEmpty(locationIds));
        xmlQuery.bind("monLocIds", Daos.getInStatementFromIntegerCollection(locationIds));

        // add campaign filter
        List<Integer> campaignIds = DaliBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterTypeValues.CAMPAIGN);
        xmlQuery.setGroup("campaignFilter", CollectionUtils.isNotEmpty(campaignIds));
        xmlQuery.bind("campaignIds", Daos.getInStatementFromIntegerCollection(campaignIds));

        // add department filter
        List<Integer> departmentIds = DaliBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterTypeValues.DEPARTMENT);
        xmlQuery.setGroup("departmentFilter", CollectionUtils.isNotEmpty(departmentIds));
        xmlQuery.bind("depIds", Daos.getInStatementFromIntegerCollection(departmentIds));

        // add sampling equipment filter
        List<Integer> equipmentIds = DaliBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterTypeValues.SAMPLING_EQUIPMENT);
        xmlQuery.setGroup("equipmentFilter", CollectionUtils.isNotEmpty(equipmentIds));
        xmlQuery.bind("equipmentIds", Daos.getInStatementFromIntegerCollection(equipmentIds));

        // add pmfm filter
        List<Integer> pmfmIds = DaliBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterTypeValues.PMFM);
        xmlQuery.setGroup("pmfmFilter", CollectionUtils.isNotEmpty(pmfmIds));
        xmlQuery.bind("pmfmIds", Daos.getInStatementFromIntegerCollection(pmfmIds));

        // add referential transcribing type labels
        xmlQuery.bind("samplingEquipmentTranscribingTypeLb", Optional.ofNullable(config.getTranscribingItemTypeLbForSamplingEquipmentNm()).orElse(""));
        xmlQuery.bind("samplingEquipmentTranscribingTypeLbForExtraction", Optional.ofNullable(config.getTranscribingItemTypeLbForSamplingEquipmentExtraction()).orElse(""));
        xmlQuery.bind("qualityFlagTranscribingTypeLb", Optional.ofNullable(config.getTranscribingItemTypeLbForQualFlagNm()).orElse(""));
        xmlQuery.bind("monitoringLocationTranscribingTypeLb", Optional.ofNullable(config.getTranscribingItemTypeLbForMonLocNm()).orElse(""));

        // execute insertion
        execute(xmlQuery);

        return countFrom(context.getBaseTableName());
    }

    private List<ExtractionPmfmInfoDTO> getPmfmInfo(ExtractionContextDTO context) {

        XMLQuery xmlQuery = createXMLQuery("pmfmInfo");
        xmlQuery.bind("rawTableName", context.getRawTableName());

        return extractionResultDao.query(xmlQuery.getSQLQueryAsString(), null, (resultSet, i) -> newPmfmInfo(context,
                resultSet.getInt(1),
                resultSet.getBoolean(2),
                resultSet.getBoolean(3)));
    }

    private ExtractionPmfmInfoDTO newPmfmInfo(ExtractionContextDTO context, int pmfmId, boolean isSurvey, boolean isIndividual) {

        ExtractionPmfmInfoDTO pmfmInfo = DaliBeanFactory.newExtractionPmfmInfoDTO();
        pmfmInfo.setPmfmId(pmfmId);
        pmfmInfo.setSurvey(isSurvey);
        pmfmInfo.setIndividual(isIndividual);

        // compute alias
        String safePmfmId = pmfmInfo.getPmfmId() < 0 ? "M" : "" + pmfmInfo.getPmfmId();
        pmfmInfo.setAlias((pmfmInfo.isSurvey() ? "SU" : "SO") + (pmfmInfo.isIndividual() ? "I" : "") + safePmfmId);
        // Generate pmfm table name
        pmfmInfo.setTableName(String.format(PMFM_TABLE_NAME_PATTERN, context.getUniqueId(), pmfmInfo.getAlias()));

        return pmfmInfo;
    }

    private int cleanSurveysUnderMoratorium(ExtractionContextDTO context) {

        List<MoratoriumDTO> globalMoratoriums = getMoratoriums(context, true, true);

        if (globalMoratoriums.isEmpty()) return 0;
        int nbRemoves = 0;
        // Iterate over global moratoriums to remove surveys
        for (MoratoriumDTO moratorium : globalMoratoriums) {
            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("cleanSurveysUnderMoratorium");
                xmlQuery.bind("tableName", context.getBaseTableName());
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                nbRemoves += execute(xmlQuery);
            }
        }

        return nbRemoves;
    }

    private long createRawTable(ExtractionContextDTO context, ExtractionOutputType outputType) {

        XMLQuery xmlQuery = createXMLQuery("createRawTable");
        xmlQuery.bind("rawTableName", context.getRawTableName());
        xmlQuery.bind("baseTableName", context.getBaseTableName());

        // active groups depending the output type
        xmlQuery.setGroup("complete", isComplete(outputType));

        execute(xmlQuery);

        return countFrom(context.getRawTableName());
    }

    private int cleanTaxonData(ExtractionContextDTO context) {

        XMLQuery xmlQuery = createXMLQuery("cleanTaxonData");
        xmlQuery.bind("tableName", context.getRawTableName());

        // if a taxon or taxon group filter is defined, will remove raw line not corresponding to filter (avoid blank cells in split table)
        List<Integer> taxonGroupIds = DaliBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterTypeValues.TAXON_GROUP);
        List<Integer> taxonNameIds = DaliBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterTypeValues.TAXON);

        if (CollectionUtils.isEmpty(taxonGroupIds) && CollectionUtils.isEmpty(taxonNameIds))
            // Nothing to delete
            return 0;

        if (CollectionUtils.isNotEmpty(taxonGroupIds)) {
            xmlQuery.setGroup("taxonGroup", true);
            xmlQuery.bind("taxonGroupIds", Daos.getInStatementFromIntegerCollection(taxonGroupIds));
        } else {
            xmlQuery.setGroup("taxonGroup", false);
        }

        if (CollectionUtils.isNotEmpty(taxonNameIds)) {
            xmlQuery.setGroup("taxonName", true);
            xmlQuery.bind("taxonNameIds", Daos.getInStatementFromIntegerCollection(taxonNameIds));
        } else {
            xmlQuery.setGroup("taxonName", false);
        }

        return execute(xmlQuery);

    }

    private int cleanDataUnderMoratorium(ExtractionContextDTO context) {

        List<MoratoriumDTO> partialMoratoriums = getMoratoriums(context, false, true);

        if (partialMoratoriums.isEmpty()) return 0;
        int nbRemoves = 0;

        // Iterate over partiel moratoriums to remove measurements
        for (MoratoriumDTO moratorium : partialMoratoriums) {

            // Find all pmfms corresponding to moratorium
            Set<Integer> pmfmIds = moratorium.getPmfms().stream()
                .flatMap(moratoriumPmfm -> pmfmDao.findPmfms(
                        moratoriumPmfm.getParameterCode(),
                        moratoriumPmfm.getMatrixId(),
                        moratoriumPmfm.getFractionId(),
                        moratoriumPmfm.getMethodId(),
                        moratoriumPmfm.getUnitId(),
                        null,
                        StatusFilter.ALL.toStatusCodes())
                    .stream())
                .map(PmfmDTO::getId)
                .collect(Collectors.toSet());

            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("cleanDataUnderMoratorium");
                xmlQuery.bind("tableName", context.getRawTableName());
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                xmlQuery.bind("pmfmIds", Daos.getInStatementFromIntegerCollection(pmfmIds));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                addPartialMoratoriumFilter(xmlQuery, moratorium);

                nbRemoves += execute(xmlQuery);
            }
        }

        return nbRemoves;
    }

    private void createPmfmTables(ExtractionContextDTO context) {

        for (ExtractionPmfmInfoDTO pmfmInfo : context.getPmfmInfos()) {

            XMLQuery xmlQuery = createXMLQuery("createPmfmTable");
            xmlQuery.bind("pmfmTableName", pmfmInfo.getTableName());
            xmlQuery.bind("rawTableName", context.getRawTableName());
            xmlQuery.bind("parentId", pmfmInfo.isSurvey() ? "SURVEY_ID" : "SAMPLING_OPER_ID");
            xmlQuery.bind("pmfmId", String.valueOf(pmfmInfo.getPmfmId()));
            xmlQuery.bind("isSurveyMeas", pmfmInfo.isSurvey() ? "1" : "0");
            xmlQuery.bind("measIndivId", pmfmInfo.isIndividual() ? "NOT NULL" : "NULL");
            // Add transcribing for qualitative values (Mantis #48699)
            xmlQuery.bind("transcribingItemTypeLb", Optional.ofNullable(config.getTranscribingItemTypeLbForQualitativeValueNm()).orElse(""));
            xmlQuery.bind("transcribingItemTypeLbForExtraction", Optional.ofNullable(config.getTranscribingItemTypeLbForQualitativeValueExtraction()).orElse(""));

            execute(xmlQuery);

            if (LOG.isDebugEnabled()) {

                // Count inserted data
                long nbRowsInserted = countFrom(pmfmInfo.getTableName());
                LOG.debug(String.format("%s rows of pmfm raw data inserted into %s", nbRowsInserted, pmfmInfo.getTableName()));
            }
        }
    }

    private Multimap<ExtractionPmfmInfoDTO, Combination> createPmfmResultTables(ExtractionContextDTO context, CombinationList combinations) {

        Multimap<ExtractionPmfmInfoDTO, Combination> pmfmResultInfos = ArrayListMultimap.create();

        // Create a table for each result PMFMs
        for (PmfmDTO pmfmResult : context.getExtraction().getParameter().getPmfmResults()) {

            ExtractionPmfmInfoDTO pmfmResultInfo = getPmfmInfo(context.getPmfmInfos(), pmfmResult.getId());

            for (Combination combination : combinations) {

                // Table name use the result pmfm table name _ the combination id
                String pmfmResultTableName = getPmfmResultTableName(pmfmResultInfo, combination);

                if (LOG.isDebugEnabled())
                    LOG.debug(String.format("pmfmResultTableName = %s ; combination = %s", pmfmResultTableName, combination));

                XMLQuery xmlQuery = createXMLQuery("createPmfmResultTable");
                xmlQuery.bind("pmfmResultTableName", pmfmResultTableName);
                xmlQuery.bind("pmfmTableName", pmfmResultInfo.getTableName());

                // Add injection query for each item in combination (replacing all bindings by the pmfm alias)
                for (IntegerPair pair : combination) {

                    ExtractionPmfmInfoDTO pmfmInfo = getPmfmInfo(context.getPmfmInfos(), pair.getKey());
                    xmlQuery.injectQuery(getXMLQueryFile("injectionPmfmAggregation"), "PMFM_ALIAS", pmfmInfo.getAlias());

                    // Bind table name and qualitative value
                    xmlQuery.bind(pmfmInfo.getAlias() + "_pmfmTableName", pmfmInfo.getTableName());
                    xmlQuery.bind(pmfmInfo.getAlias() + "_qualValueId", String.valueOf(pair.getValue()));

                }

                execute(xmlQuery);

                boolean approved = context.getExtraction().getParameter().isFillZero();

                // If fill zeros is not set, try to evict empty result (Mantis #39558)
                if (!approved) {

                    // Check if pmfm result exists
                    long nbRowsInserted = countFrom(pmfmResultTableName);

                    // combination is approved if result exists
                    approved = nbRowsInserted > 0;
                }

                // Store in map if combination approved
                if (approved)
                    pmfmResultInfos.put(pmfmResultInfo, combination);

            }
        }

        return pmfmResultInfos;
    }

    private void createCommonTable(ExtractionContextDTO context, ExtractionOutputType outputType,
                                   Map<String, String> fieldNamesByAlias, Map<String, String> decimalFormats, Map<String, String> dateFormats) {

        XMLQuery xmlQuery = createXMLQuery("createCommonTable");
        xmlQuery.bind("commonTableName", context.getCommonTableName());
        xmlQuery.bind("sourceTableName", context.getRawTableName());

        // Active groups depending the output type
        xmlQuery.setGroup("complete", isComplete(outputType));
        xmlQuery.setGroup("individual", !isAggregated(outputType));

        // Add all survey non individual (ordered) measurements (is exists)
        List<ExtractionPmfmInfoDTO> surveyNonIndividualMeasurements = context.getPmfmInfos().stream()
                .filter(pmfmInfo -> !pmfmInfo.isIndividual() && pmfmInfo.isSurvey())
                .sorted(Comparator.comparingInt(ExtractionPmfmInfoDTO::getRankOrder))
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(surveyNonIndividualMeasurements)) {

            // Add them in injection point (if survey measurements)
            xmlQuery.setGroup("surveyMeasurements", true);

            surveyNonIndividualMeasurements.forEach(pmfmInfo -> {

                PmfmDTO pmfm = pmfmDao.getPmfmById(pmfmInfo.getPmfmId());

                // Add injection query for pmfm table (replacing all bindings by the pmfm alias)
                xmlQuery.injectQuery(getXMLQueryFile("injectionPmfm"),
                        "PMFM_ALIAS",
                        pmfmInfo.getAlias(),
                        // Use this injection point
                        "surveyMeasurements");

                // Bind table names
                xmlQuery.bind(pmfmInfo.getAlias() + "_pmfmTableName", pmfmInfo.getTableName());

                // Active value
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_numerical_without_zero", !pmfm.getParameter().isQualitative());
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_numerical_with_zero", false);
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_qualitative", pmfm.getParameter().isQualitative());

                // Active join link
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_surveyJoin", true);
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_samplingOperationJoin", false);
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_surveyJoin_individual", false);
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_samplingOperationJoin_individual", false);

                // Build output name
                String unitName = extractionResultDao.getPmfmUnitNameForExtraction(pmfm);
                String pmfmName = String.format("%s%s", extractionResultDao.getPmfmNameForExtraction(pmfm), StringUtils.isNotBlank(unitName) ? "_" + unitName : "");
                fieldNamesByAlias.put(pmfmInfo.getAlias(), DaliBeans.toFullySecuredString(pmfmName)); // no prefix

                // Build number format
                if (!pmfm.getParameter().isQualitative()) {
                    decimalFormats.put(pmfmInfo.getAlias(), getNumericFormat(pmfm));
                }
            });

            // Add analyst from survey non individual measurements
            xmlQuery.injectQuery(
                    getXMLQueryFile("injectionAnalyst"),
                    "surveyMeasurements"
            );
            xmlQuery.setGroup("survey", true);
            xmlQuery.setGroup("samplingOperation", false);
            xmlQuery.bind("depNmFields", getAliasedFields(surveyNonIndividualMeasurements, "DEP_NM"));

        } else {

            // Disable the injection point (see Mantis #40480)
            xmlQuery.setGroup("surveyMeasurements", false);
        }

        // Compute field alias and format
        prepare(xmlQuery, fieldNamesByAlias, decimalFormats, dateFormats);

        execute(xmlQuery);

        if (LOG.isDebugEnabled()) {

            // Count inserted data
            long nbRowsInserted = countFrom(context.getCommonTableName());
            LOG.debug(String.format("%s rows of common data inserted into %s", nbRowsInserted, context.getCommonTableName()));
        }

    }

    private long createResultTable(ExtractionContextDTO context,
                                   Map<String, String> fieldNamesByAlias, Map<String, String> decimalFormats, Map<String, String> dateFormats) {

        XMLQuery xmlQuery = createXMLQuery("createResultTable");
        xmlQuery.bind("resultTableName", context.getResultTableName());
        xmlQuery.bind("sourceTableName", context.getCommonTableName());

        // Active individual group
        xmlQuery.setGroup("individual", true);

        // Add sampling operations non individual and individual measurements
        List<ExtractionPmfmInfoDTO> individualMeasurements = context.getPmfmInfos().stream()
                .filter(pmfmInfo -> !pmfmInfo.isSurvey())
                // sort non individual first then by rank order
                .sorted(((Comparator<ExtractionPmfmInfoDTO>) (pmfmInfo1, pmfmInfo2) -> Boolean.compare(pmfmInfo1.isIndividual(), pmfmInfo2.isIndividual()))
                        .thenComparingInt(ExtractionPmfmInfoDTO::getRankOrder))
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(individualMeasurements)) {
            individualMeasurements.forEach(pmfmInfo -> {

                PmfmDTO pmfm = pmfmDao.getPmfmById(pmfmInfo.getPmfmId());

                // Add injection query for pmfm table (replacing all bindings by the pmfm alias)
                xmlQuery.injectQuery(getXMLQueryFile("injectionPmfm"), "PMFM_ALIAS", pmfmInfo.getAlias());

                // Bind table names
                xmlQuery.bind(pmfmInfo.getAlias() + "_pmfmTableName", pmfmInfo.getTableName());

                // Active value
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_numerical_without_zero", !pmfm.getParameter().isQualitative());
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_numerical_with_zero", false);
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_qualitative", pmfm.getParameter().isQualitative());

                // Active join link
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_surveyJoin", false);
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_samplingOperationJoin", !pmfmInfo.isIndividual());
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_surveyJoin_individual", false);
                xmlQuery.setGroup(pmfmInfo.getAlias() + "_samplingOperationJoin_individual", pmfmInfo.isIndividual());

                // Build output name
                String unitName = extractionResultDao.getPmfmUnitNameForExtraction(pmfm);
                String pmfmName = String.format("%s%s", extractionResultDao.getPmfmNameForExtraction(pmfm), StringUtils.isNotBlank(unitName) ? "_" + unitName : "");
                String formattedPmfmName = String.format("%s%s",
                        t("dali.service.extraction.fieldNamePrefix.MEAS"),
                        pmfmName);
                fieldNamesByAlias.put(pmfmInfo.getAlias(), DaliBeans.toFullySecuredString(formattedPmfmName));

                // Build number format
                if (!pmfm.getParameter().isQualitative()) {
                    decimalFormats.put(pmfmInfo.getAlias(), getNumericFormat(pmfm));
                }
            });

            // Add measurement comment
            xmlQuery.injectQuery(getXMLQueryFile("injectionComment"));
            xmlQuery.bind("measCmFields", getAliasedFields(individualMeasurements, "MEAS_CM"));

            // Add analyst from individual measurements
            xmlQuery.injectQuery(getXMLQueryFile("injectionAnalyst"));
            xmlQuery.setGroup("survey", false);
            xmlQuery.setGroup("samplingOperation", true);
            xmlQuery.bind("depNmFields", getAliasedFields(individualMeasurements, "DEP_NM"));

            // Add rows without individual measurements (with NULL columns in union sub query)
            for (int i = 0; i < individualMeasurements.size() + 3 /* Add also 3 NULL columns for comment, analyst and moratorium */; i++) {
                xmlQuery.addSubSelect("groupedMeasurements", getXMLQueryFile("subSelectNull"));
            }

        }

        // inject empty column for moratorium, null by default, then update it with specific query
        xmlQuery.injectQuery(getXMLQueryFile("injectionUnderMoratorium"));

        prepare(xmlQuery, fieldNamesByAlias, decimalFormats, dateFormats);

        execute(xmlQuery);
        return countFrom(context.getResultTableName());
    }

    private long createAggregatedResultTable(ExtractionContextDTO context, Multimap<ExtractionPmfmInfoDTO, Combination> pmfmResultInfos,
                                             Map<String, String> fieldNamesByAlias, Map<String, String> decimalFormats, Map<String, String> dateFormats) {

        XMLQuery xmlQuery = createXMLQuery("createResultTable");
        xmlQuery.bind("resultTableName", context.getResultTableName());
        xmlQuery.bind("sourceTableName", context.getCommonTableName());

        // Disable individual group
        xmlQuery.setGroup("individual", false);

        // Add all pmfm results (already ordered by combination)
        if (pmfmResultInfos != null)
            pmfmResultInfos.forEach((pmfmResultInfo, combination) -> {

                PmfmDTO pmfmResult = pmfmDao.getPmfmById(pmfmResultInfo.getPmfmId());

                String pmfmResultAlias = getPmfmResultAlias(pmfmResultInfo, combination);
                String pmfmResultTableName = getPmfmResultTableName(pmfmResultInfo, combination);

                // Add injection query for pmfm result table (replacing all bindings by the pmfm result alias)
                xmlQuery.injectQuery(getXMLQueryFile("injectionPmfm"), "PMFM_ALIAS", pmfmResultAlias);

                // Bind table names
                xmlQuery.bind(pmfmResultAlias + "_pmfmTableName", pmfmResultTableName);

                // Active value
                xmlQuery.setGroup(pmfmResultAlias + "_numerical_without_zero", !context.getExtraction().getParameter().isFillZero());
                xmlQuery.setGroup(pmfmResultAlias + "_numerical_with_zero", context.getExtraction().getParameter().isFillZero());
                xmlQuery.setGroup(pmfmResultAlias + "_qualitative", false);

                // Active join link
                xmlQuery.setGroup(pmfmResultAlias + "_surveyJoin", false);
                xmlQuery.setGroup(pmfmResultAlias + "_samplingOperationJoin", true);
                xmlQuery.setGroup(pmfmResultAlias + "_surveyJoin_individual", false);
                xmlQuery.setGroup(pmfmResultAlias + "_samplingOperationJoin_individual", false);

                // Build output name
                String pmfmResultName = DaliBeans.toFullySecuredString(extractionResultDao.getPmfmNameForExtraction(pmfmResult));
                String unitName = DaliBeans.toFullySecuredString(extractionResultDao.getPmfmUnitNameForExtraction(pmfmResult));
                StringJoiner combinationName = new StringJoiner("_");
                combination.forEach(pair -> combinationName.add(DaliBeans.toFullySecuredString(
                        extractionResultDao.getQualitativeValueNameForExtraction(qualitativeValueDao.getQualitativeValueById(pair.getValue())))));

                String formattedPmfmName = String.format("%s%s_%s%s",
                        t("dali.service.extraction.fieldNamePrefix.MEAS"),
                        pmfmResultName, combinationName, StringUtils.isNotBlank(unitName) ? "_" + unitName : "");
                fieldNamesByAlias.put(pmfmResultAlias, formattedPmfmName);
                decimalFormats.put(pmfmResultAlias, getNumericFormat(pmfmResult));

            });

        // inject empty column for moratorium, null by default, then update it with specific query
        xmlQuery.injectQuery(getXMLQueryFile("injectionUnderMoratorium"));

        prepare(xmlQuery, fieldNamesByAlias, decimalFormats, dateFormats);
        execute(xmlQuery);

        return countFrom(context.getResultTableName());
    }

    private int updateResultTable(ExtractionContextDTO context) {

        int nbUpdates = 0;
        List<MoratoriumDTO> globalMoratoriums = getMoratoriums(context, true, false);

        // Iterate over global moratoriums to update moratorium column
        for (MoratoriumDTO moratorium : globalMoratoriums) {
            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("updateGlobalMoratorium");
                xmlQuery.bind("tableName", context.getResultTableName());
                xmlQuery.bind("underMoratorium", t("dali.service.extraction.moratorium.yes"));
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                nbUpdates += execute(xmlQuery);
            }
        }

        List<MoratoriumDTO> partialMoratoriums = getMoratoriums(context, false, false);

        // Iterate over partial moratoriums to update moratorium column
        for (MoratoriumDTO moratorium : partialMoratoriums) {

            // Find all pmfms corresponding to moratorium
            Set<Integer> pmfmIds = moratorium.getPmfms().stream()
                .flatMap(moratoriumPmfm -> pmfmDao.findPmfms(
                        moratoriumPmfm.getParameterCode(),
                        moratoriumPmfm.getMatrixId(),
                        moratoriumPmfm.getFractionId(),
                        moratoriumPmfm.getMethodId(),
                        moratoriumPmfm.getUnitId(),
                        null,
                        StatusFilter.ALL.toStatusCodes())
                    .stream())
                .map(PmfmDTO::getId)
                .collect(Collectors.toSet());

            if (context.getPmfmInfos().stream().noneMatch(pmfmInfo -> pmfmIds.contains(pmfmInfo.getPmfmId()))) {
                // Skip this moratorium because none of the pmfm ids are extracted
                continue;
            }

            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("updatePartialMoratorium");
                xmlQuery.bind("tableName", context.getResultTableName());
                xmlQuery.bind("underMoratorium", t("dali.service.extraction.moratorium.partial"));
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                addPartialMoratoriumFilter(xmlQuery, moratorium);

                nbUpdates += execute(xmlQuery);
            }
        }

        // Update remaining rows
        XMLQuery xmlQuery = createXMLQuery("updateNoMoratorium");
        xmlQuery.bind("tableName", context.getResultTableName());
        xmlQuery.bind("underMoratorium", t("dali.service.extraction.moratorium.no"));
        execute(xmlQuery);

        return nbUpdates;
    }

    private void addMoratoriumFilter(ExtractionContextDTO context, XMLQuery xmlQuery, MoratoriumDTO moratorium) {

        // Check if user is program recorder
        ProgramDTO program = getProgram(context, moratorium.getProgramCode());
        boolean isRecorder = DaliBeans.isProgramRecorderOrViewerOnly(program, dataContext.getRecorderPersonId(), dataContext.getRecorderDepartmentId());
        List<Integer> recorderDepartmentIds = new ArrayList<>();
        if (isRecorder) {
            recorderDepartmentIds.add(dataContext.getRecorderDepartmentId());
            List<Integer> inheritedDepartmentIds = departmentDao.getInheritedRecorderDepartmentIds(dataContext.getRecorderDepartmentId());
            if (CollectionUtils.isNotEmpty(inheritedDepartmentIds)) {
                recorderDepartmentIds.addAll(inheritedDepartmentIds);
            }
        }

        // Recorder department filter
        if (CollectionUtils.isNotEmpty(recorderDepartmentIds)) {
            xmlQuery.setGroup("recorderDepartmentFilter", true);
            xmlQuery.bind("recorderDepartmentIds", Daos.getInStatementFromIntegerCollection(recorderDepartmentIds));
        } else {
            xmlQuery.setGroup("recorderDepartmentFilter", false);
        }

    }

    private void addPartialMoratoriumFilter(XMLQuery xmlQuery, MoratoriumDTO moratorium) {

        // Moratorium optional components filters
        xmlQuery.setGroup("partialFilter", false);

        if (CollectionUtils.isNotEmpty(moratorium.getLocationIds())) {
            xmlQuery.setGroup("partialFilter", true);
            xmlQuery.setGroup("location", true);
            xmlQuery.bind("locationIds", Daos.getInStatementFromIntegerCollection(moratorium.getLocationIds()));
        } else {
            xmlQuery.setGroup("location", false);
        }

        if (CollectionUtils.isNotEmpty(moratorium.getCampaignIds())) {
            xmlQuery.setGroup("partialFilter", true);
            xmlQuery.setGroup("campaign", true);
            xmlQuery.bind("campaignIds", Daos.getInStatementFromIntegerCollection(moratorium.getCampaignIds()));
        } else {
            xmlQuery.setGroup("campaign", false);
        }

        if (CollectionUtils.isNotEmpty(moratorium.getOccasionIds())) {
            xmlQuery.setGroup("partialFilter", true);
            xmlQuery.setGroup("occasion", true);
            xmlQuery.bind("occasionIds", Daos.getInStatementFromIntegerCollection(moratorium.getOccasionIds()));
        } else {
            xmlQuery.setGroup("occasion", false);
        }
    }

    private void writeExtraction(ExtractionContextDTO context, ExtractionOutputType outputType,
                                 Map<String, String> fieldNamesByAlias,
                                 Map<String, String> decimalFormats,
                                 Map<String, String> dateFormats,
                                 File outputFile)
            throws IOException {

        XMLQuery xmlQuery = createXMLQuery("selectResultTable");
        xmlQuery.bind("resultTableName", context.getResultTableName());

        // Ignore some Fields
        List<String> fieldsToIgnore = new ArrayList<>(ImmutableList.of("SURVEY_ID", "REC_DEP_ID", "SAMPLING_OPER_ID", "MON_LOC_ID", "CAMPAIGN_ID", "OCCAS_ID"));
        if (!isComplete(outputType)) fieldsToIgnore.add("MEAS_INDIV_ID");

        // write result
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("write result into file : %s", outputFile.getAbsolutePath()));
        }
        extractionResultDao.dumpQueryToCSV(outputFile, xmlQuery.getSQLQueryAsString(), fieldNamesByAlias, dateFormats, decimalFormats, fieldsToIgnore);
    }

    private void prepare(XMLQuery xmlQuery, Map<String, String> fieldNamesByAlias, Map<String, String> decimalFormats, Map<String, String> dateFormats) {

        xmlQuery.getFirstQueryTag().getChildren(XMLQuery.TAG_SELECT).forEach(select -> {

            // Compute alias i18n key
            String alias = select.getAttributeValue(XMLQuery.ATTR_ALIAS);
            fieldNamesByAlias.putIfAbsent(alias, t("dali.service.extraction.fieldName." + alias));

            String type = select.getAttributeValue(XMLQuery.ATTR_TYPE);
            if (XMLQuery.TYPE_DATE.equalsIgnoreCase(type)) {
                dateFormats.putIfAbsent(alias, "dd/MM/yyyy");
            } else if (XMLQuery.TYPE_NUMBER.equalsIgnoreCase(type)) {
                decimalFormats.putIfAbsent(alias, getDefaultNumericFormat());
            }

        });
    }

    private int execute(XMLQuery xmlQuery) {

        return extractionResultDao.queryUpdate(xmlQuery.getSQLQueryAsString());
    }

    private long countFrom(String tableName) {

        XMLQuery xmlQuery = createXMLQuery("countFrom");
        xmlQuery.bind("tableName", tableName);
        return extractionResultDao.queryCount(xmlQuery.getSQLQueryAsString());

    }

    private ExtractionPmfmInfoDTO findPmfmInfo(Collection<ExtractionPmfmInfoDTO> pmfmInfos, int pmfmId) {

        Optional<ExtractionPmfmInfoDTO> optional = pmfmInfos.stream().filter(pmfmInfo -> pmfmInfo.getPmfmId() == pmfmId).findFirst();
        return optional.orElse(null);
    }

    private ExtractionPmfmInfoDTO getPmfmInfo(Collection<ExtractionPmfmInfoDTO> pmfmInfos, int pmfmId) {

        ExtractionPmfmInfoDTO pmfmInfo = findPmfmInfo(pmfmInfos, pmfmId);
        if (pmfmInfo == null)
            throw new DaliTechnicalException(String.format("The PMFM id=%s is not part of the extraction context", pmfmId));
        return pmfmInfo;
    }

    private String getPmfmResultTableName(ExtractionPmfmInfoDTO pmfmResultInfo, Combination combination) {
        return String.format("%s_%s", pmfmResultInfo.getTableName(), combination.getId());
    }

    private String getPmfmResultAlias(ExtractionPmfmInfoDTO pmfmResultInfo, Combination combination) {
        return String.format("%s_%s", pmfmResultInfo.getAlias(), combination.getId());
    }

    private String getNumericFormat(PmfmDTO pmfm) {

        if (pmfm.getMaxDecimals() == null) {
            return getDefaultNumericFormat();
        }

        // format with max decimals from pmfm definition (decimal 0 is appended)
        return "#." + StringUtils.repeat("0", pmfm.getMaxDecimals());
    }

    private String getDefaultNumericFormat() {
        return "#." + StringUtils.repeat("#", 8); // 8 decimals should be enough by default (decimal 0 is erased)
    }

    private String getOrderItemTypeCode(ExtractionDTO extraction) {
        FilterDTO orderItemTypeFilter = DaliBeans.getFilterOfType(extraction, ExtractionFilterTypeValues.ORDER_ITEM_TYPE);
        Assert.notNull(orderItemTypeFilter);
        //noinspection ConstantConditions
        Assert.notNull(orderItemTypeFilter.getElements());
        Assert.size(orderItemTypeFilter.getElements(), 1);

        GroupingTypeDTO groupingType = (GroupingTypeDTO) orderItemTypeFilter.getElements().get(0);
        Assert.notNull(groupingType);
        Assert.notBlank(groupingType.getCode());
        return groupingType.getCode();
    }

    private XMLQuery createXMLQuery(String queryName) {
        XMLQuery query = DaliServiceLocator.instance().getService("XMLQuery", XMLQuery.class);
        query.setQuery(getXMLQueryFile(queryName));
        return query;
    }

    private URL getXMLQueryFile(String queryName) {
        URL fileURL = getClass().getClassLoader().getResource(XML_QUERY_PATH + "/" + queryName + ".xml");
        if (fileURL == null)
            throw new DaliTechnicalException(String.format("query '%s' not found in resources", queryName));
        return fileURL;
    }

    private boolean isComplete(ExtractionOutputType outputType) {
        return ExtractionOutputType.COMPLETE.equals(outputType) || ExtractionOutputType.AGGREGATED_COMPLETE.equals(outputType);
    }

    private boolean isAggregated(ExtractionOutputType outputType) {
        return ExtractionOutputType.AGGREGATED_STANDARD.equals(outputType) || ExtractionOutputType.AGGREGATED_COMPLETE.equals(outputType);
    }

    private String getAliasedFields(Collection<ExtractionPmfmInfoDTO> pmfmInfos, final String fieldName) {
        return pmfmInfos.stream().map(pmfmInfo -> String.format("%s.%s", pmfmInfo.getAlias(), fieldName)).collect(Collectors.joining(","));
    }

    private void createConcatDistinctFunction(String functionName, String valueType) {

        dropConcatDistinctFunction(functionName);

        // function with external java method
        String query = "CREATE FUNCTION CONCAT_DISTINCT_" + functionName + "(IN_ARRAY " + valueType + " ARRAY, SEPARATOR VARCHAR(10)) RETURNS LONGVARCHAR " +
                "LANGUAGE JAVA DETERMINISTIC NO SQL EXTERNAL NAME 'CLASSPATH:fr.ifremer.dali.dto.DaliBeans.getUnifiedSQLString';";

        extractionResultDao.queryUpdate(query, null);

    }

    private void dropConcatDistinctFunction(String functionName) {

        extractionResultDao.queryUpdate("DROP FUNCTION CONCAT_DISTINCT_" + functionName + " IF EXISTS", null);
    }

    private String formatDate(LocalDate date) {
        return Dates.formatDate(date, "dd/MM/yyyy");
    }

    private List<String> getProgramCodes(ExtractionContextDTO context) {
        return DaliBeans.transformCollection(context.getPrograms(), ProgramDTO::getCode);
    }

    private ProgramDTO getProgram(ExtractionContextDTO context, String programCode) {
        return context.getPrograms().stream().filter(program -> program.getCode().equals(programCode)).findFirst().orElseThrow(NullPointerException::new);
    }

    private List<MoratoriumDTO> getMoratoriums(ExtractionContextDTO context, boolean global, boolean activeForCurrentUser) {

        return context.getPrograms().stream()
            .filter(program -> !program.isMoratoriumsEmpty())
            // if only active for current user, filter by user privilege
            .filter(program -> !activeForCurrentUser
                // Administrators are not affected by moratoriums (Mantis #59187)
                || (!dataContext.isRecorderAdministrator() && DaliBeans.isProgramRecorderOrViewerOnly(program, dataContext.getRecorderPersonId(), dataContext.getRecorderDepartmentId())))
            .flatMap(program -> program.getMoratoriums().stream())
            .filter(moratorium -> moratorium.isGlobal() == global)
            // A moratorium period overlaps an extraction period
            .filter(moratorium ->
                moratorium.getPeriods().stream()
                    .anyMatch(moratoriumPeriod -> context.getPeriods().stream()
                        .anyMatch(extractionPeriod ->
                            !extractionPeriod.getStartDate().isAfter(moratoriumPeriod.getEndDate()) && !extractionPeriod.getEndDate().isBefore(moratoriumPeriod.getStartDate())                            ))
            )
            .collect(Collectors.toList());
    }
}
