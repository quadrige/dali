package fr.ifremer.dali.service;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.service.administration.campaign.CampaignService;
import fr.ifremer.dali.service.administration.context.ContextService;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.dali.service.administration.user.UserService;
import fr.ifremer.dali.service.control.ControlRuleService;
import fr.ifremer.dali.service.control.RuleListService;
import fr.ifremer.dali.service.extraction.ExtractionPerformService;
import fr.ifremer.dali.service.extraction.ExtractionService;
import fr.ifremer.dali.service.observation.ObservationService;
import fr.ifremer.dali.service.persistence.PersistenceService;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.dali.service.synchro.SynchroClientService;
import fr.ifremer.dali.service.system.SystemService;
import fr.ifremer.quadrige3.core.service.ClientServiceLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
  <p>DaliServiceLocator class.</p>
 */
@Configuration
public class DaliServiceLocator extends ClientServiceLocator {

    static {
        INSTANCE = new DaliServiceLocator();
        initDaliDefault();
    }

    private static final DaliServiceLocator INSTANCE;

    /**
     * <p>initDaliDefault.</p>
     */
    public static void initDaliDefault() {
        INSTANCE.init("daliBeanRefFactory.xml", "daliBeanRefFactory");
        ClientServiceLocator.setInstance(INSTANCE);
    }

    /**
     * <p>instance.</p>
     *
     * @return a {@link DaliServiceLocator} object.
     */
    public static DaliServiceLocator instance() {
        return INSTANCE;
    }


    /**
     * <p>getDataContext.</p>
     *
     * @return a {@link DaliDataContext} object.
     */
    @Bean
    public DaliDataContext getDataContext() {
        return DaliDataContext.instance();
    }

    /**
     * <p>getPersistenceService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.persistence.PersistenceService} object.
     */
    @Override
    public PersistenceService getPersistenceService() {
        return getService("daliPersistenceService", PersistenceService.class);
    }

    /**
     * <p>getSystemService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.system.SystemService} object.
     */
    public SystemService getSystemService() {
        return getService("daliSystemService", SystemService.class);
    }

    /**
     * <p>getObservationService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.observation.ObservationService} object.
     */
    public ObservationService getObservationService() {
        return getService("daliSurveyService", ObservationService.class);
    }

    /**
     * <p>getExtractionService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.extraction.ExtractionService} object.
     */
    public ExtractionService getExtractionService() {
        return getService("daliExtractionService", ExtractionService.class);
    }

    /**
     * <p>getExtractionPerformService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.extraction.ExtractionPerformService} object.
     */
    public ExtractionPerformService getExtractionPerformService() {
        return getService("daliExtractionPerformService", ExtractionPerformService.class);
    }

    /**
     * <p>getReferentialService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.referential.ReferentialService} object.
     */
    public ReferentialService getReferentialService() {
        return getService("daliReferentialService", ReferentialService.class);
    }

    /**
     * <p>getProgramStrategyService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.program.ProgramStrategyService} object.
     */
    public ProgramStrategyService getProgramStrategyService() {
        return getService("daliProgramStrategyService", ProgramStrategyService.class);
    }

    /**
     * <p>getContextService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.context.ContextService} object.
     */
    public ContextService getContextService() {
        return getService("daliContextService", ContextService.class);
    }

    /**
     * <p>getUserService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.user.UserService} object.
     */
    @Override
    public UserService getUserService() {
        return getService("daliUserService", UserService.class);
    }

    /**
     * <p>getRulesControlService.</p>
     *
     * @return a {@link ControlRuleService} object.
     */
    public ControlRuleService getRulesControlService() {
        return getService("daliControlRuleService", ControlRuleService.class);
    }

    public RuleListService getRuleListService() {
        return getService("daliRuleListService", RuleListService.class);
    }

    /**
     * <p>getSynchroClientService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.synchro.SynchroClientService} object.
     */
    @Override
    public SynchroClientService getSynchroClientService() {
        return getService("daliSynchroClientService", SynchroClientService.class);
    }

    public CampaignService getCampaignService() {
        return getService("daliCampaignService", CampaignService.class);
    }

    @Override
    public DecoratorService getDecoratorService() {
        return (DecoratorService) super.getDecoratorService();
    }

}
