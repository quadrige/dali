package fr.ifremer.dali.service;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.decorator.DecoratorService;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.context.ContextDTO;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.service.administration.campaign.CampaignService;
import fr.ifremer.dali.service.administration.context.ContextService;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.dali.service.administration.user.UserService;
import fr.ifremer.dali.service.control.ControlRuleService;
import fr.ifremer.dali.service.control.RuleListService;
import fr.ifremer.dali.service.extraction.ExtractionPerformService;
import fr.ifremer.dali.service.extraction.ExtractionService;
import fr.ifremer.dali.service.observation.ObservationService;
import fr.ifremer.dali.service.persistence.PersistenceService;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.dali.service.system.SystemService;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContext;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.synchro.service.client.SynchroHistoryService;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.beans.AbstractBean;

import java.io.Closeable;
import java.util.Objects;

/**
 * Data context of ui.
 * <p/>
 * All shared data must be there to avoid reloading some stuff.
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>
 */
public class DaliDataContext extends AbstractBean implements Closeable, SecurityContext {

    /**
     * Constant <code>PROPERTY_CONTEXT="context"</code>
     */
    public static final String PROPERTY_CONTEXT = "context";

    // Singleton
    private static final DaliDataContext INSTANCE = new DaliDataContext();
    /**
     * the current recorder user
     */
    private Integer recorderPersonId;
    /**
     * the current recorder department
     */
    private Integer recorderDepartmentId;
    /**
     * the current context
     */
    private ContextDTO context;

    /**
     * <p>Constructor for DaliDataContext.</p>
     */
    private DaliDataContext() {
        // protected constructor, for singleton
    }

    /**
     * <p>instance.</p>
     *
     * @return a {@link DaliDataContext} object.
     */
    public static DaliDataContext instance() {
        return INSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        clearContext();
    }

    /**
     * <p>clearContext.</p>
     */
    public void clearContext() {
        clearContext(false);
    }

    /**
     * <p>clearContextKeepRecorderPerson.</p>
     */
    public void clearContextKeepRecorderPerson() {
        clearContext(true);
    }

    private void clearContext(boolean keepRecorderPerson) {
        if (!keepRecorderPerson) {
            setRecorderPersonId(null);
        }

        // reset local caches
        resetLocalCache();
    }

    /**
     * <p>resetLocalCache.</p>
     */
    public void resetLocalCache() {

    }

    /*
     * recorder person Id
     */

    /**
     * <p>isRecorderPersonFilled.</p>
     *
     * @return a boolean.
     */
    public boolean isRecorderPersonFilled() {
        return recorderPersonId != null;
    }

    /**
     * <p>Getter for the field <code>recorderPersonId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getRecorderPersonId() {
        return recorderPersonId;
    }

    /**
     * <p>Setter for the field <code>recorderPersonId</code>.</p>
     *
     * @param recorderPersonId a {@link java.lang.Integer} object.
     */
    public void setRecorderPersonId(Integer recorderPersonId) {
        this.recorderPersonId = recorderPersonId;
        this.recorderDepartmentId = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPrincipalUserId() {
        return getRecorderPersonId();
    }

    /*
     * recorder department Id
     */

    /**
     * <p>isRecorderDepartmentFilled.</p>
     *
     * @return a boolean.
     */
    public boolean isRecorderDepartmentFilled() {
        return recorderDepartmentId != null;
    }

    /**
     * <p>Getter for the field <code>recorderDepartmentId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getRecorderDepartmentId() {
        if (recorderDepartmentId == null && recorderPersonId != null) {
            recorderDepartmentId = getUserService().getDepartmentIdByUserId(recorderPersonId);
        }
        return recorderDepartmentId;
    }

    /**
     * <p>Setter for the field <code>recorderDepartmentId</code>.</p>
     *
     * @param recorderDepartmentId a {@link java.lang.Integer} object.
     */
    public void setRecorderDepartmentId(Integer recorderDepartmentId) {
//        Integer oldId = getRecorderDepartmentId();
        this.recorderDepartmentId = recorderDepartmentId;
//        firePropertyChange(PROPERTY_RECORDER_DEPARTMENT_ID, oldId, recorderDepartmentId);
    }

    /**
     * <p>isRecorderQualifier.</p>
     *
     * @return a boolean.
     */
    public boolean isRecorderQualifier() {
        return SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.QUALIFIER);
    }

    public boolean isRecorderAdministrator() {
        return SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.ADMIN);
    }

    /**
     * <p>Getter for the field <code>context</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    public ContextDTO getContext() {
        return context;
    }

    /**
     * <p>Setter for the field <code>context</code>.</p>
     *
     * @param context a {@link fr.ifremer.dali.dto.configuration.context.ContextDTO} object.
     */
    public void setContext(ContextDTO context) {
        ContextDTO oldContext = getContext();
        this.context = context;
        firePropertyChange(PROPERTY_CONTEXT, oldContext, context);
    }

    /**
     * <p>getContextId.</p>
     *
     * @return a int.
     */
    public int getContextId() {
        Assert.notNull(getContext());
        return getContext().getId();
    }

    /**
     * <p>isContextFiltered.</p>
     *
     * @param contextFilter a {@link FilterTypeValues} object.
     * @return a boolean.
     */
    public boolean isContextFiltered(FilterTypeValues contextFilter) {
        Assert.notNull(contextFilter);
        if (getContext() == null || CollectionUtils.isEmpty(getContext().getFilters())) {
            return false;
        }
        for (FilterDTO filter : getContext().getFilters()) {
            if (Objects.equals(filter.getFilterTypeId(), contextFilter.getFilterTypeId())) {
                return true;
            }
        }
        return false;
    }

    public boolean isSurveyEditable(SurveyDTO survey) {
        ProgramDTO program = survey.getProgram();
        return
            // User is administrator
            isRecorderAdministrator()
                // User is program manager
                || DaliBeans.isProgramManager(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is program validator
                || DaliBeans.isProgramValidator(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is recorder
                || (
                DaliBeans.isProgramRecorder(program, getRecorderPersonId(), getRecorderDepartmentId())
                    // and data created by same service as user (or by privilege transfer)
                    && (
                    Objects.equals(survey.getRecorderDepartment().getId(), getRecorderDepartmentId())
                        || CollectionUtils.emptyIfNull(survey.getInheritedRecorderDepartmentIds()).contains(getRecorderDepartmentId())
                )
            );
    }

    public boolean isSurveyViewable(SurveyDTO survey) {
        ProgramDTO program = survey.getProgram();
        return
            // User is administrator
            isRecorderAdministrator()
                // User is program manager
                || DaliBeans.isProgramManager(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is full viewer
                || DaliBeans.isProgramFullViewer(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is recorder or program validator (Mantis #59583)
                || (
                (DaliBeans.isProgramRecorder(program, getRecorderPersonId(), getRecorderDepartmentId())
                    || DaliBeans.isProgramViewer(program, getRecorderPersonId(), getRecorderDepartmentId())
                    || DaliBeans.isProgramValidator(program, getRecorderPersonId(), getRecorderDepartmentId()))
                    // and data created by same service as user (or by privilege transfer)
                    && (
                    Objects.equals(survey.getRecorderDepartment().getId(), getRecorderDepartmentId())
                        || CollectionUtils.emptyIfNull(survey.getInheritedRecorderDepartmentIds()).contains(getRecorderDepartmentId())
                )
            );
    }

    // ------------------------------------------------------------------------//
    // -- Services methods --//
    // ------------------------------------------------------------------------//

    /**
     * <p>Getter for the field <code>decoratorService</code>.</p>
     *
     * @return a {@link fr.ifremer.dali.decorator.DecoratorService} object.
     */
    public DecoratorService getDecoratorService() {
        return DaliServiceLocator.instance().getDecoratorService();
    }

    /**
     * <p>getSystemService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.system.SystemService} object.
     */
    public SystemService getSystemService() {
        return DaliServiceLocator.instance().getSystemService();
    }

    /**
     * <p>getReferentialService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.referential.ReferentialService} object.
     */
    public ReferentialService getReferentialService() {
        return DaliServiceLocator.instance().getReferentialService();
    }

    /**
     * <p>getObservationService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.observation.ObservationService} object.
     */
    public ObservationService getObservationService() {
        return DaliServiceLocator.instance().getObservationService();
    }

    /**
     * <p>getExtractionService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.extraction.ExtractionService} object.
     */
    public ExtractionService getExtractionService() {
        return DaliServiceLocator.instance().getExtractionService();
    }

    /**
     * <p>getExtractionPerformService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.extraction.ExtractionPerformService} object.
     */
    public ExtractionPerformService getExtractionPerformService() {
        return DaliServiceLocator.instance().getExtractionPerformService();
    }

    /**
     * <p>getPersistenceService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.persistence.PersistenceService} object.
     */
    public PersistenceService getPersistenceService() {
        return DaliServiceLocator.instance().getPersistenceService();
    }

    /**
     * <p>getUserService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.user.UserService} object.
     */
    public UserService getUserService() {
        return DaliServiceLocator.instance().getUserService();
    }

    /**
     * <p>getSynchroHistoryService.</p>
     *
     * @return a {@link fr.ifremer.quadrige3.synchro.service.client.SynchroHistoryService} object.
     */
    public SynchroHistoryService getSynchroHistoryService() {
        return DaliServiceLocator.instance().getSynchroHistoryService();
    }

    /**
     * <p>getProgramStrategyService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.program.ProgramStrategyService} object.
     */
    public ProgramStrategyService getProgramStrategyService() {
        return DaliServiceLocator.instance().getProgramStrategyService();
    }

    /**
     * <p>getContextService.</p>
     *
     * @return a {@link fr.ifremer.dali.service.administration.context.ContextService} object.
     */
    public ContextService getContextService() {
        return DaliServiceLocator.instance().getContextService();
    }

    /**
     * <p>getRulesControlService.</p>
     *
     * @return a {@link ControlRuleService} object.
     */
    public ControlRuleService getRulesControlService() {
        return DaliServiceLocator.instance().getRulesControlService();
    }

    public RuleListService getRuleListService() {
        return DaliServiceLocator.instance().getRuleListService();
    }

    public CampaignService getCampaignService() {
        return DaliServiceLocator.instance().getCampaignService();
    }
}
