package fr.ifremer.dali.service.extraction;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.enums.ExtractionOutputType;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

/**
 * @author peck7 on 23/11/2017.
 */
@Transactional
public interface ExtractionPerformService {


    /**
     * <p>performExtraction.</p>
     *  @param extraction a {@link ExtractionDTO} object.
     * @param outputType a {@link ExtractionOutputType} object.
     * @param outputFile a {@link File} object.
     * @param progressionModel
     */
    void performExtraction(ExtractionDTO extraction, ExtractionOutputType outputType, File outputFile, ProgressionCoreModel progressionModel);

}
