package fr.ifremer.dali.service.extraction;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.FilterTypeDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ludovic on 02/12/2015.
 */
@Transactional(readOnly = true)
public interface ExtractionService {

    /**
     * <p>getFilterTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<FilterTypeDTO> getFilterTypes();

    /**
     * Return all extraction for combo list (no filter, no parameter)
     *
     * @return a {@link java.util.List} object.
     */
    List<ExtractionDTO> getAllLightExtractions();

    /**
     * <p>getExtractions.</p>
     *
     * @param extractionId a {@link java.lang.Integer} object.
     * @param programCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<ExtractionDTO> getExtractions(Integer extractionId, String programCode);

    /**
     * Load all filtered elements in extraction
     *
     * @param extraction a {@link fr.ifremer.dali.dto.system.extraction.ExtractionDTO} object.
     */
    void loadFilteredElements(ExtractionDTO extraction);

    /**
     * <p>saveExtractions.</p>
     *
     * @param extractions a {@link java.util.Collection} object.
     */
    @Transactional()
    void saveExtractions(Collection<? extends ExtractionDTO> extractions);

    /**
     * <p>deleteExtractions.</p>
     *
     * @param idExtractionToDelete a {@link java.util.List} object.
     */
    @Transactional()
    void deleteExtractions(List<Integer> idExtractionToDelete);

    /**
     * <p>duplicateExtraction.</p>
     *
     * @param extraction a {@link fr.ifremer.dali.dto.system.extraction.ExtractionDTO} object.
     * @return a {@link fr.ifremer.dali.dto.system.extraction.ExtractionDTO} object.
     */
    ExtractionDTO duplicateExtraction(ExtractionDTO extraction) throws CloneNotSupportedException;

    /**
     * Export extraction definition and parameter to a file
     *
     * @param extraction extraction to export
     * @param exportFile destination file
     */
    void exportExtraction(ExtractionDTO extraction, File exportFile);

    /**
     * Import an extraction from a file
     *
     * @param importFile source file
     * @return the imported extraction
     */
    ExtractionDTO importExtraction(File importFile);
}
