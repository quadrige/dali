package fr.ifremer.dali.service.administration.program;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.quadrige3.core.exception.SaveForbiddenException;
import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Les services pour la configuration.
 */
@Transactional(readOnly = true)
public interface ProgramStrategyService extends ProgramService {

    /**
     * Duplicate strategy.
     *
     * @param strategy             Strategy.
     * @param targetProgram        a {@link fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO} object.
     * @param skipLocalReferential a boolean.
     * @return Strategy
     */
    StrategyDTO duplicateStrategy(StrategyDTO strategy, ProgramDTO targetProgram, boolean skipLocalReferential);

    /**
     * Get all programs with write rights fot current user
     *
     * @return programs list
     */
    List<ProgramDTO> getReadablePrograms();

    /**
     * Get all programs with write rights fot current user
     *
     * @return programs list
     */
    List<ProgramDTO> getWritablePrograms();

    ProgramDTO getReadableProgramByCode(final String programCode);

    /**
     * Recherche un programme suivant son identifiant.
     *
     * @param programCode l'identifiant du programme
     * @return Le programme recherche
     */
    ProgramDTO getWritableProgramByCode(final String programCode);

    /**
     * Retrieve programs by status (ALL, LOCAL, ...). See StatusFilter enumeration.
     *
     * @param statusFilter the filter to apply
     * @return list of programs found
     */
    List<ProgramDTO> getReadableProgramsByStatus(StatusFilter statusFilter);

    /**
     * Retrieve programs by status (ALL, LOCAL, ...). See StatusFilter enumeration.
     *
     * @param statusFilter the filter to apply
     * @return list of programs found
     */
    List<ProgramDTO> getWritableProgramsByStatus(StatusFilter statusFilter);

    List<ProgramDTO> getReadableProgramsByLocationAndDate(int locationId, Date date);

    List<ProgramDTO> getWritableProgramsByLocationAndDate(int locationId, Date date);

    /**
     * Retrieve active programs with access rights for the given user
     *
     * @param userId       identifier of the user
     * @return list of programs found
     */
    List<ProgramDTO> getWritableProgramsByUser(int userId);

    /**
     * Retrieve programs by status (ALL, LOCAL, ...), and with access rights for the given user
     *
     * @param userId       identifier of the user
     * @param statusFilter the filter to apply
     * @return list of programs found
     */
    List<ProgramDTO> getWritableProgramsByUser(int userId, StatusFilter statusFilter);

    /**
     * Retrieve active programs with manager rights for the current user
     *
     * @return list of programs found
     */
    List<ProgramDTO> getManagedPrograms();

    /**
     * Retrieve active programs with manager rights for the given user
     *
     * @param userId       identifier of the user
     * @return list of programs found
     */
    List<ProgramDTO> getManagedProgramsByUser(int userId);

    /**
     * Retrieve programs by status (ALL, LOCAL, ...), and with manager rights for the given user
     *
     * @param userId       identifier of the user
     * @param statusFilter the filter to apply
     * @return list of programs found
     */
    List<ProgramDTO> getManagedProgramsByUser(int userId, StatusFilter statusFilter);

    /**
     * Will load (if necessary) program's strategies and locations
     *
     * @param program a not null program
     */
    void loadStrategiesAndLocations(final ProgramDTO program);

    /**
     * Will load (if necessary) strategy's applied periods, AND add all other program's locations
     *
     * @param program  a not null program
     * @param strategy a not null strategy
     */
    void loadAppliedPeriodsAndPmfmStrategies(ProgramDTO program, StrategyDTO strategy);

    /**
     * Get the list of all PMFM strategies for the survey
     *
     * @param survey a {@link SurveyDTO} object.
     * @return a {@link java.util.List} object.
     */
    List<PmfmStrategyDTO> getPmfmStrategiesBySurvey(SurveyDTO survey);

    /**
     * Get the list of all PMFM strategies for programs and applicable in date range
     *
     * @param programCodes program codes
     * @param startDate    start date
     * @param endDate      end date
     * @return a {@link java.util.List} object.
     */
    Set<PmfmStrategyDTO> getPmfmStrategiesByProgramsAndDates(Collection<String> programCodes, LocalDate startDate, LocalDate endDate);

    /**
     * Get the first not null sampling department from applied strategy
     *
     * @param survey a {@link fr.ifremer.dali.dto.data.survey.SurveyDTO} object.
     * @return a {@link fr.ifremer.dali.dto.referential.DepartmentDTO} object.
     */
    DepartmentDTO getSamplingDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey);

    /**
     * Get the first not null analysis department from applied strategy
     *
     * @param survey a {@link fr.ifremer.dali.dto.data.survey.SurveyDTO} object.
     * @return a {@link fr.ifremer.dali.dto.referential.DepartmentDTO} object.
     */
    DepartmentDTO getAnalysisDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey);

    /**
     * Get the list of PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getPmfmsForSurvey(SurveyDTO survey);

    /**
     * Get the list of grouped PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getGroupedPmfmsForSurvey(SurveyDTO survey);

    /**
     * Get the list of PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getPmfmsForSamplingOperationBySurvey(SurveyDTO survey);

    /**
     * Get the list of grouped PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getGroupedPmfmsForSamplingOperationBySurvey(SurveyDTO survey);

    /**
     * Save a list of local program
     *
     * @param programs a {@link java.util.List} object.
     * @throws DaliTechnicalException if one program is not local (add argument authenticationInfo to save it remotely)
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
            RuntimeException.class,
            SaveForbiddenException.class
    })
    void savePrograms(List<? extends ProgramDTO> programs);

    /**
     * Save a list of remote program will be saved to remote server
     *
     * @param programs           a {@link java.util.List} object.
     * @param authenticationInfo a {@link fr.ifremer.quadrige3.core.security.AuthenticationInfo} object.
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
            RuntimeException.class,
            SaveForbiddenException.class
    })
    void savePrograms(AuthenticationInfo authenticationInfo, List<? extends ProgramDTO> programs);

    /**
     * Recherche les strategies pour un lieu
     *
     * @param locationId Identifiant du lieu
     * @return La liste de strategies
     */
    List<ProgStratDTO> getStrategyUsageByLocationId(Integer locationId);

    /**
     * Recherche les strategies pour un programme et un lieu
     *
     * @param programCode Identifiant programme
     * @param locationId  Identifiant lieu
     * @return La liste de strategies
     */
    List<ProgStratDTO> getStrategyUsageByProgramAndLocationId(final String programCode, final Integer locationId);

    /**
     * <p>saveStrategiesByProgramAndLocation.</p>
     *
     * @param strategies a {@link java.util.List} object.
     * @param locationId a int.
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
            RuntimeException.class,
            SaveForbiddenException.class
    })
    void saveStrategiesByProgramAndLocation(AuthenticationInfo authenticationInfo, List<ProgStratDTO> strategies, int locationId);

    /**
     * Get list a program for user (with read access rights), from the remote synchro server
     *
     * @param authenticationInfo Authentication info, to connect to remote server
     * @return a {@link java.util.List} object.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    List<ProgramDTO> getRemoteReadableProgramsByUser(AuthenticationInfo authenticationInfo);

    /**
     * Get list a program for user (with read/write access rights), from the remote synchro server
     *
     * @param authenticationInfo Authentication info, to connect to remote server
     * @return a {@link java.util.List} object.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    List<ProgramDTO> getRemoteWritableProgramsByUser(AuthenticationInfo authenticationInfo);

    /**
     * Get if a user has some read access rights on at least on program, from the remote synchro server
     *
     * @param authenticationInfo authentication info, to connect to remote server
     * @return a boolean.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    boolean hasRemoteReadOnProgram(AuthenticationInfo authenticationInfo);

    /**
     * Get if a user has some write access rights on at least on program, from the remote synchro server
     *
     * @param authenticationInfo authentication info, to connect to remote server
     * @return a boolean.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    boolean hasRemoteWriteOnProgram(AuthenticationInfo authenticationInfo);

    List<ProgramDTO> getProgramsByCodes(List<String> programCodes);

    boolean hasPotentialMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters);

    List<MoratoriumDTO> getMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters);
}

	
