package fr.ifremer.dali.service.observation;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author peck7 on 15/11/2017.
 */
@Transactional
public interface ObservationInternalService extends ObservationService {

    /**
     * Internal service method to validate surveys
     *
     * @param surveyIds survey ids to validate
     * @param validationDate the validation date
     * @param validationComment the validation comment
     */
    void validateSurveys(List<Integer> surveyIds, Date validationDate, String validationComment);

    /**
     * Internal service method to unvalidate surveys
     *
     * @param surveyIds survey ids to unvalidate
     * @param unvalidationDate the unvalidation date
     * @param unvalidationComment the unvalidation comment
     */
    void unvalidateSurveys(List<Integer> surveyIds, Date unvalidationDate, String unvalidationComment);

    /**
     * Internal service method to qualify surveys
     *
     * @param surveyIds survey ids to qualify
     * @param qualificationDate the qualification date
     * @param qualificationComment the qualification comment
     * @param qualityLevelCode the qualification level code
     */
    void qualifySurveys(List<Integer> surveyIds, Date qualificationDate, String qualificationComment, String qualityLevelCode);
}
