package fr.ifremer.dali.service.administration.user;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dao.administration.user.DaliQuserDao;
import fr.ifremer.dali.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.dto.referential.PrivilegeDTO;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.quadrige3.core.dao.administration.user.PrivilegeCode;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

/**
 * <p>UserServiceImpl class.</p>
 *
 */
@Service("daliUserService")
public class UserServiceImpl extends fr.ifremer.quadrige3.core.service.administration.user.UserServiceImpl implements UserService {

    @Autowired
    protected CacheService cacheService;

    @Resource(name = "daliQuserDao")
    private DaliQuserDao quserDao;

    /** {@inheritDoc} */
    @Override
    public Integer getDepartmentIdByUserId(int userId) {
        return quserDao.getDepartmentIdByUserId(userId);
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> getActiveUsers() {
        return quserDao.getAllUsers(StatusFilter.ACTIVE.toStatusCodes());
    }

    @Override
    public PersonDTO getUser(int userId) {
        return quserDao.getUserById(userId);
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> searchUser(PersonCriteriaDTO searchCriteria) {
        List<String> statusCodes = StatusFilter.ALL.intersect(searchCriteria.getStatus());
        Integer departmentId = searchCriteria.getDepartment() == null ?
                null : searchCriteria.getDepartment().getId();
        String privilegeCode = searchCriteria.getPrivilege() == null ?
                null : searchCriteria.getPrivilege().getCode();
        return quserDao.findUsersByCriteria(
                searchCriteria.getName(),
                searchCriteria.getFirstName(),
                searchCriteria.isStrictName(),
                searchCriteria.getLogin(),
                departmentId,
                privilegeCode, statusCodes);
    }

    /** {@inheritDoc} */
    @Override
    public List<PrivilegeDTO> getAllPrivileges() {
        return quserDao.getAllPrivileges();
    }

    /** {@inheritDoc} */
    @Override
    public List<PrivilegeDTO> getAvailablePrivileges() {
        List<PrivilegeDTO> availablePrivileges = Lists.newArrayList();
        for (PrivilegeDTO privilege : getAllPrivileges()) {
            if (PrivilegeCode.VALIDATOR.getValue().equals(privilege.getCode())
                    || PrivilegeCode.QUALIFIER.getValue().equals(privilege.getCode())) {
                availablePrivileges.add(privilege);
            }
        }
        return availablePrivileges;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PrivilegeDTO> getPrivilegesByUser(Integer userId) {
        return quserDao.getPrivilegesByUserId(userId);
    }

}
