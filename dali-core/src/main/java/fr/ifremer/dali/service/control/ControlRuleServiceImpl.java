package fr.ifremer.dali.service.control;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.operation.distance.DistanceOp;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.data.measurement.DaliMeasurementDao;
import fr.ifremer.dali.dao.data.samplingoperation.DaliSamplingOperationDao;
import fr.ifremer.dali.dao.data.survey.DaliSurveyDao;
import fr.ifremer.dali.dao.system.rule.DaliRuleDao;
import fr.ifremer.dali.dao.technical.Geometries;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.ErrorDTO;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.dali.dto.data.Coordinate1DAware;
import fr.ifremer.dali.dto.data.Coordinate2DAware;
import fr.ifremer.dali.dto.data.LocationCoordinateAware;
import fr.ifremer.dali.dto.data.PositioningPrecisionAware;
import fr.ifremer.dali.dto.data.measurement.MeasurementAware;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.enums.*;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.service.observation.ObservationService;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.ui.core.dto.CodeOnly;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Implementation rules control service.
 */
@Service("daliControlRuleService")
public class ControlRuleServiceImpl implements ControlRuleService {

    private static final Log LOG = LogFactory.getLog(ControlRuleServiceImpl.class);

    @Resource(name = "daliRuleDao")
    private DaliRuleDao ruleDao;
    @Resource(name = "daliSurveyDao")
    private DaliSurveyDao surveyDao;
    @Resource(name = "daliSamplingOperationDao")
    private DaliSamplingOperationDao samplingOperationDao;
    @Resource(name = "daliMeasurementDao")
    private DaliMeasurementDao measurementDao;
    @Resource(name = "daliSurveyService")
    private ObservationService observationService;
    @Resource
    protected DaliConfiguration configuration;

    // active control rules temporary loaded in a weak map
    private Map<SurveyDTO, List<ControlRuleDTO>> rulesBySurveyMap = new WeakHashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public ControlRuleMessagesBean controlSurveys(Collection<? extends SurveyDTO> surveys,
                                                  boolean updateControlDateWhenSucceed,
                                                  boolean resetControlDateWhenFailed, ProgressionCoreModel progressionModel) {

        Date controlDate = new Date(System.currentTimeMillis());
        ControlRuleMessagesBean messages = new ControlRuleMessagesBean(controlDate);

        List<Integer> validControlledElements = Lists.newArrayList();
        List<Integer> invalidControlledElements = Lists.newArrayList();

        List<SurveyDTO> surveysToControl = surveys.stream().filter(survey -> !DaliBeans.isSurveyValidated(survey)).collect(Collectors.toList());
        progressionModel.setTotal(surveysToControl.size());

        // Control all observations
        for (final SurveyDTO surveyToControl : surveysToControl) {

            progressionModel.increments(t("dali.service.common.progression",
                    t("dali.service.control"), progressionModel.getCurrent() + 1, progressionModel.getTotal()));

            // The process must control all data but this method is called from HomeUI, the surveys are not fully loaded
            // So load them in separated beans and copy control errors afterwards
            SurveyDTO loadedSurveyToControl = observationService.getSurveyWithoutPmfmFiltering(surveyToControl.getId());

            // process control skipping survey measurements (Mantis #47285)
            // But only if no control date update is asked (Mantis #52518)
            boolean succeed = executeControlOnSurvey(loadedSurveyToControl,
                messages,
                updateControlDateWhenSucceed,
                resetControlDateWhenFailed,
                !updateControlDateWhenSucceed);

            if (succeed && updateControlDateWhenSucceed) {
                validControlledElements.add(surveyToControl.getId());
                // update new controlDate in DTO too
                surveyToControl.setControlDate(controlDate);
            } else if (!succeed && resetControlDateWhenFailed) {
                invalidControlledElements.add(surveyToControl.getId());
                // update new controlDate (null) in DTO too
                surveyToControl.setControlDate(null);
            }

            // set errors from controlled survey and sampling operations
            surveyToControl.setErrors(loadedSurveyToControl.getErrors());
            Map<Integer, SamplingOperationDTO> samplingOperationsMap = DaliBeans.mapById(surveyToControl.getSamplingOperations());
            for (SamplingOperationDTO samplingOperationToControl : loadedSurveyToControl.getSamplingOperations()) {
                SamplingOperationDTO samplingOperation = samplingOperationsMap.get(samplingOperationToControl.getId());
                if (samplingOperation != null) {
                    samplingOperation.setErrors(samplingOperationToControl.getErrors());
                }
            }
        }

        // update in DB control date of controlled surveys
        updateSurveysControlDate(validControlledElements, invalidControlledElements, controlDate);

        // clear temporary rules maps
        rulesBySurveyMap.clear();

        return messages;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControlRuleMessagesBean controlSurvey(SurveyDTO survey,
                                                 boolean updateControlDateWhenSucceed,
                                                 boolean resetControlDateWhenFailed) {

        // don't control if already valid
        if (survey == null || DaliBeans.isSurveyValidated(survey)) {
            return null;
        }

        Date controlDate = new Date(System.currentTimeMillis());
        ControlRuleMessagesBean messages = new ControlRuleMessagesBean(controlDate);

        List<Integer> validControlledElements = Lists.newArrayListWithCapacity(1);
        List<Integer> invalidControlledElements = Lists.newArrayListWithCapacity(1);

        // execute control
        boolean success = executeControlOnSurvey(survey, messages, updateControlDateWhenSucceed, resetControlDateWhenFailed, false);

        if (success && updateControlDateWhenSucceed) {
            validControlledElements.add(survey.getId());
            // update new controlDate in DTO too
            survey.setControlDate(controlDate);
        } else if (!success && resetControlDateWhenFailed) {
            invalidControlledElements.add(survey.getId());
            // update new controlDate (null) in DTO too
            survey.setControlDate(null);
        }

        // update in DB control date of controlled surveys
        updateSurveysControlDate(validControlledElements, invalidControlledElements, controlDate);

        // clear temporary rules maps
        rulesBySurveyMap.clear();

        return messages;
    }

    @Override
    public boolean controlUniqueObject(ControlRuleDTO rule, Object objectToControl) {

        // dummy beans
        ControlRuleMessagesBean messages = new ControlRuleMessagesBean(null);
        ErrorDTO error = DaliBeanFactory.newErrorDTO();

        if (objectToControl instanceof BigDecimal) {
            validBigDecimal((BigDecimal) objectToControl, rule, messages, error, null);
        } else if (objectToControl instanceof Double) {
            validDouble((Double) objectToControl, rule, messages, error);
        } else if (objectToControl instanceof Integer) {
            validInteger((Integer) objectToControl, rule, messages, error);
        } else if (objectToControl instanceof Date) {
            validDate((Date) objectToControl, rule, messages, error);
        } else if (objectToControl instanceof String) {
            validString((String) objectToControl, rule, messages, error);
        } else if (objectToControl instanceof Collection) {
            validCollection((Collection) objectToControl, rule, messages, error);
        } else {
            validObject(objectToControl, rule, messages, error);
        }

        // object if valid if no error or warning
        return !error.isError() && !error.isWarning();
    }

    /**
     * Search all rules for programme and element control.
     *
     * @param survey          Survey identifier
     * @param elementControls Element control.
     * @return All rules
     */
    private List<ControlRuleDTO> getRules(SurveyDTO survey, ControlElementValues... elementControls) {
        return DaliBeans.filterCollection(getRulesForSurvey(survey),
                controlRule -> ArrayUtils.contains(elementControls, ControlElementValues.getByCode(controlRule.getControlElement().getCode())));
    }

    /**
     * All control rules for a survey
     *
     * @param survey the survey
     * @return Rules control list
     */
    private List<ControlRuleDTO> getRulesForSurvey(SurveyDTO survey) {
        Assert.notNull(survey);
        Assert.notNull(survey.getDate());
        Assert.notNull(survey.getProgram());
        Assert.notNull(survey.getRecorderDepartment());

        return rulesBySurveyMap.computeIfAbsent(survey,
                rules -> ruleDao.findActiveRules(
                        Dates.convertToDate(survey.getDate(), configuration.getDbTimezone()),
                        survey.getProgram().getCode(),
                        survey.getRecorderDepartment().getId()));
    }

    private boolean executeControlOnSurvey(SurveyDTO survey, ControlRuleMessagesBean messages,
                                           boolean updateControlDateWhenSucceed,
                                           boolean resetControlDateWhenFailed,
                                           boolean skipSurveyMeasurements) {

        boolean isSurveyValid = true;
        survey.getErrors().clear();

        // All observation rules
        final List<ControlRuleDTO> rules = getRules(survey, ControlElementValues.SURVEY);
        for (final ControlRuleDTO rule : rules) {

            // Enum value
            final ControlFeatureSurveyValues enumValue = ControlFeatureSurveyValues.getByCode(rule.getControlFeature().getCode());
            if (enumValue == null) {
                throw new DaliTechnicalException(String.format("ControlFeatureSurveyValues with code=%s has not been found", rule.getControlFeature().getCode()));
            }
            final ErrorDTO error = newControlError(ControlElementValues.SURVEY);

            // Test all features
            switch (enumValue) {
                case CAMPAIGN:
                    validObject(survey.getCampaign(), rule, messages, error, SurveyDTO.PROPERTY_CAMPAIGN);
                    break;
                case LOCATION:
                    validObject(survey.getLocation(), rule, messages, error, SurveyDTO.PROPERTY_LOCATION);
                    break;
                case PROGRAM:
                    validObject(survey.getProgram(), rule, messages, error, SurveyDTO.PROPERTY_PROGRAM);
                    break;
                case VALIDATION_COMMENT:
                    validString(survey.getValidationComment(), rule, messages, error, SurveyDTO.PROPERTY_VALIDATION_COMMENT);
                    break;
                case QUALIFICATION_COMMENT:
                    validString(survey.getQualificationComment(), rule, messages, error, SurveyDTO.PROPERTY_QUALIFICATION_COMMENT);
                    break;
                case DATE:
                    validLocalDate(survey.getDate(), rule, messages, error, SurveyDTO.PROPERTY_DATE);
                    break;
                case CONTROL_DATE:
                    validDate(survey.getControlDate(), rule, messages, error, SurveyDTO.PROPERTY_CONTROL_DATE);
                    break;
                case UPDATE_DATE:
                    validDate(survey.getUpdateDate(), rule, messages, error, SurveyDTO.PROPERTY_UPDATE_DATE);
                    break;
                case VALIDATION_DATE:
                    validDate(survey.getValidationDate(), rule, messages, error, SurveyDTO.PROPERTY_VALIDATION_DATE);
                    break;
                case QUALIFICATION_DATE:
                    validDate(survey.getQualificationDate(), rule, messages, error, SurveyDTO.PROPERTY_QUALIFICATION_DATE);
                    break;
                case TIME:
                    validInteger(survey.getTime(), rule, messages, error, SurveyDTO.PROPERTY_TIME);
                    break;
                case COMMENT:
                    validString(survey.getComment(), rule, messages, error, SurveyDTO.PROPERTY_COMMENT);
                    break;
                case LATITUDE_REAL_SURVEY:
                    validDouble(survey.getCoordinate() == null ? null : survey.getCoordinate().getMinLatitude(), rule, messages, error, Coordinate1DAware.PROPERTY_LATITUDE);
                    break;
                case LONGITUDE_REAL_SURVEY:
                    validDouble(survey.getCoordinate() == null ? null : survey.getCoordinate().getMinLongitude(), rule, messages, error, Coordinate1DAware.PROPERTY_LONGITUDE);
                    break;
                case NAME:
                    validString(survey.getName(), rule, messages, error, SurveyDTO.PROPERTY_NAME);
                    break;
                case DEPARTMENT:
                    validObject(survey.getRecorderDepartment(), rule, messages, error, SurveyDTO.PROPERTY_RECORDER_DEPARTMENT);
                    break;
                case POSITIONING:
                    validObject(survey.getPositioning(), rule, messages, error, SurveyDTO.PROPERTY_POSITIONING);
                    break;
                case POSITIONING_PRECISION:
                    if (survey.getPositioning() != null) {
                        validString(survey.getPositioning().getPrecision(), rule, messages, error, PositioningPrecisionAware.PROPERTY_POSITIONING_PRECISION);
                    }
                    break;
                case BOTTOM_DEPTH:
                    validDouble(survey.getBottomDepth(), rule, messages, error, SurveyDTO.PROPERTY_BOTTOM_DEPTH);
                    break;
                case LATITUDE_MAX_LOCATION:
                    validDouble(survey.getLocation().getCoordinate() == null ? null : survey.getLocation().getCoordinate().getMaxLatitude(), rule, messages, error, LocationCoordinateAware.PROPERTY_LOCATION_MAX_LATITUDE);
                    break;
                case LATITUDE_MIN_LOCATION:
                    validDouble(survey.getLocation().getCoordinate() == null ? null : survey.getLocation().getCoordinate().getMinLatitude(), rule, messages, error, LocationCoordinateAware.PROPERTY_LOCATION_MIN_LATITUDE);
                    break;
                case LONGITUDE_MAX_LOCATION:
                    validDouble(survey.getLocation().getCoordinate() == null ? null : survey.getLocation().getCoordinate().getMaxLongitude(), rule, messages, error, LocationCoordinateAware.PROPERTY_LOCATION_MAX_LONGITUDE);
                    break;
                case LONGITUDE_MIN_LOCATION:
                    validDouble(survey.getLocation().getCoordinate() == null ? null : survey.getLocation().getCoordinate().getMinLongitude(), rule, messages, error, LocationCoordinateAware.PROPERTY_LOCATION_MIN_LONGITUDE);
                    break;
                case OBSERVERS:
                    validCollection(survey.getObservers(), rule, messages, error, SurveyDTO.PROPERTY_OBSERVERS);
                    break;
                default:
                    break;
            }
            if (error.isError() || error.isWarning()) {
                survey.addErrors(error);
                if (error.isError()) {
                    isSurveyValid = false;
                }
            }

        }

        // Geometries
        controlGeometry(survey, messages);

        // Measurements
        if (!skipSurveyMeasurements && !controlMeasurements(survey,
                getRules(survey, ControlElementValues.MEASUREMENT, ControlElementValues.TAXON_MEASUREMENT),
                messages,
                updateControlDateWhenSucceed,
                resetControlDateWhenFailed)) {
            isSurveyValid = false;
        }

        // SamplingOperations
        List<Integer> validControlledElements = Lists.newArrayList();
        List<Integer> invalidControlledElements = Lists.newArrayList();
        for (final SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {
            boolean success = executeControlOnSamplingOperation(survey, samplingOperation, messages, updateControlDateWhenSucceed, resetControlDateWhenFailed);
            if (success && updateControlDateWhenSucceed) {
                validControlledElements.add(samplingOperation.getId());
                samplingOperation.setControlDate(messages.getControlDate());
            } else if (!success && resetControlDateWhenFailed) {
                invalidControlledElements.add(samplingOperation.getId());
                samplingOperation.setControlDate(null);
            }
        }

        // update control date of controlled sampling operations
        updateSamplingOperationsControlDate(validControlledElements, invalidControlledElements, messages.getControlDate());

        return isSurveyValid && invalidControlledElements.size() == 0;
    }

    private void controlGeometry(SurveyDTO survey, ControlRuleMessagesBean messages) {

        // Get location geometry
        if (Geometries.isValid(survey.getCoordinate())
                && Geometries.isValid(survey.getLocation().getCoordinate())) {

            Geometry surveyGeometry = Geometries.getGeometry(survey.getCoordinate());
            Geometry locationGeometry = Geometries.getGeometry(survey.getLocation().getCoordinate().getWkt()); // Mantis #42564 Use real location geometry
            ErrorDTO error = newControlError(ControlElementValues.SURVEY);
            int minDistance = configuration.getControlSurveyLocationMinDistanceInMeter();

            // warn and skip survey with area geometry
            if (surveyGeometry.getDimension() > 1) {
                LOG.warn("Unable to control a survey with an area geometry");
                return;
            }

            switch (locationGeometry.getDimension()) {
                case 0:
                    // location is a point
                    if (surveyGeometry.getDimension() == 0) {
                        // survey is a point
                        Double distance = Geometries.getDistanceInMeter(surveyGeometry.getCoordinate(), locationGeometry.getCoordinate());
                        if (distance != null && distance > minDistance) {
                            addGeometryMessage(messages, error, t("dali.service.control.geometry.survey.location.aboveMinimum", minDistance),
                                    SurveyDTO.PROPERTY_LOCATION,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MIN, Coordinate2DAware.PROPERTY_LONGITUDE_MIN);
                        }
                    } else if (surveyGeometry.getDimension() == 1) {
                        // survey is a line
                        Coordinate[] coordinates = DistanceOp.nearestPoints(surveyGeometry, locationGeometry);
                        Double distance = Geometries.getDistanceInMeter(coordinates[0], coordinates[1]);
                        if (distance != null && distance > minDistance) {
                            addGeometryMessage(messages, error, t("dali.service.control.geometry.location.survey.aboveMinimum", minDistance),
                                    SurveyDTO.PROPERTY_LOCATION,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MIN, Coordinate2DAware.PROPERTY_LONGITUDE_MIN,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MAX, Coordinate2DAware.PROPERTY_LONGITUDE_MAX);
                        }
                    }
                    break;
                case 1:
                    // location is a line
                    if (surveyGeometry.getDimension() == 0) {
                        // survey is a point
                        Coordinate[] coordinates = DistanceOp.nearestPoints(surveyGeometry, locationGeometry);
                        Double distance = Geometries.getDistanceInMeter(coordinates[0], coordinates[1]);
                        if (distance != null && distance > minDistance) {
                            addGeometryMessage(messages, error, t("dali.service.control.geometry.survey.location.aboveMinimum", minDistance),
                                    SurveyDTO.PROPERTY_LOCATION,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MIN, Coordinate2DAware.PROPERTY_LONGITUDE_MIN);
                        }
                    } else if (surveyGeometry.getDimension() == 1) {
                        // survey is a line
                        Double distanceFromStart = Geometries.getDistanceInMeter(surveyGeometry.getCoordinates()[0], locationGeometry.getCoordinates()[0]);
                        if (distanceFromStart != null && distanceFromStart > minDistance) {
                            addGeometryMessage(messages, error, t("dali.service.control.geometry.survey.location.start.aboveMinimum", minDistance),
                                    SurveyDTO.PROPERTY_LOCATION,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MIN, Coordinate2DAware.PROPERTY_LONGITUDE_MIN);
                        }
                        Double distanceFromEnd = Geometries.getDistanceInMeter(surveyGeometry.getCoordinates()[1], locationGeometry.getCoordinates()[1]);
                        if (distanceFromEnd != null && distanceFromEnd > minDistance) {
                            addGeometryMessage(messages, error, t("dali.service.control.geometry.survey.location.end.aboveMinimum", minDistance),
                                    SurveyDTO.PROPERTY_LOCATION,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MAX, Coordinate2DAware.PROPERTY_LONGITUDE_MAX);
                        }
                    }
                    break;
                default:
                    // location is an area, simply test if survey bounds are included
                    if (surveyGeometry.getDimension() >= 0) {
                        // survey is a point or a line
                        Point point = GeometryFactory.createPointFromInternalCoord(surveyGeometry.getCoordinates()[0], surveyGeometry);
                        if (!locationGeometry.covers(point)) {
                            addGeometryMessage(messages, error, t("dali.service.control.geometry.survey.location.notIncluded"),
                                    SurveyDTO.PROPERTY_LOCATION,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MIN, Coordinate2DAware.PROPERTY_LONGITUDE_MIN);
                        }
                    }
                    if (surveyGeometry.getDimension() == 1) {
                        // survey is a line
                        Point point = GeometryFactory.createPointFromInternalCoord(surveyGeometry.getCoordinates()[1], surveyGeometry);
                        if (!locationGeometry.covers(point)) {
                            addGeometryMessage(messages, error, t("dali.service.control.geometry.survey.location.notIncluded"),
                                    SurveyDTO.PROPERTY_LOCATION,
                                    Coordinate2DAware.PROPERTY_LATITUDE_MAX, Coordinate2DAware.PROPERTY_LONGITUDE_MAX);
                        }
                    }
                    break;
            }

            if (error.isWarning() || error.isError()) {
                survey.addErrors(error);
            }
        }
    }

    private boolean executeControlOnSamplingOperation(SurveyDTO survey, SamplingOperationDTO samplingOperation, ControlRuleMessagesBean messages, boolean updateControlDateWhenSucceed, boolean resetControlDateWhenFailed) {

        boolean isSamplingOperationValid = true;

        // init error list
        samplingOperation.getErrors().clear();

        // All sampling operation rules
        final List<ControlRuleDTO> rules = getRules(survey, ControlElementValues.SAMPLING_OPERATION);
        for (final ControlRuleDTO rule : rules) {

            // Enum value
            final ControlFeatureSamplingOperationValues enumValue = ControlFeatureSamplingOperationValues.getByCode(rule.getControlFeature().getCode());
            if (enumValue == null) {
                throw new DaliTechnicalException(String.format("ControlFeatureSamplingOperationValues with code=%s has not been found", rule.getControlFeature().getCode()));
            }
            final ErrorDTO error = newControlError(ControlElementValues.SAMPLING_OPERATION);

            // Test all features
            switch (enumValue) {
                case TIME:
                    validInteger(samplingOperation.getTime(), rule, messages, error, SamplingOperationDTO.PROPERTY_TIME);
                    break;
                case COMMENT:
                    validString(samplingOperation.getComment(), rule, messages, error, SamplingOperationDTO.PROPERTY_COMMENT);
                    break;
                case DEPTH:
                    validDouble(samplingOperation.getDepth(), rule, messages, error, SamplingOperationDTO.PROPERTY_DEPTH);
                    break;
                case DEPTH_MAX:
                    validDouble(samplingOperation.getMaxDepth(), rule, messages, error, SamplingOperationDTO.PROPERTY_MAX_DEPTH);
                    break;
                case DEPTH_MIN:
                    validDouble(samplingOperation.getMinDepth(), rule, messages, error, SamplingOperationDTO.PROPERTY_MIN_DEPTH);
                    break;
                case LATITUDE_REAL:
                    validDouble(samplingOperation.getCoordinate() == null ? null : samplingOperation.getCoordinate().getMinLatitude(), rule, messages, error, Coordinate1DAware.PROPERTY_LATITUDE);
                    break;
                case LONGITUDE_REAL:
                    validDouble(samplingOperation.getCoordinate() == null ? null : samplingOperation.getCoordinate().getMinLongitude(), rule, messages, error, Coordinate1DAware.PROPERTY_LONGITUDE);
                    break;
                case NAME:
                    validString(samplingOperation.getName(), rule, messages, error, SamplingOperationDTO.PROPERTY_NAME);
                    break;
                case POSITIONING:
                    validObject(samplingOperation.getPositioning(), rule, messages, error, SamplingOperationDTO.PROPERTY_POSITIONING);
                    break;
                case POSITIONING_PRECISION:
                    if (samplingOperation.getPositioning() != null) {
                        validString(samplingOperation.getPositioning().getPrecision(), rule, messages, error, SamplingOperationDTO.PROPERTY_POSITIONING);
                    }
                    break;
                case GEAR:
                    validObject(samplingOperation.getSamplingEquipment(), rule, messages, error, SamplingOperationDTO.PROPERTY_SAMPLING_EQUIPMENT);
                    break;
                case SIZE:
                    validDouble(samplingOperation.getSize(), rule, messages, error, SamplingOperationDTO.PROPERTY_SIZE);
                    break;
                case SIZE_UNIT:
                    validObject(samplingOperation.getSizeUnit(), rule, messages, error, SamplingOperationDTO.PROPERTY_SIZE_UNIT);
                    break;
                case DEPARTMENT:
                    validObject(samplingOperation.getSamplingDepartment(), rule, messages, error, SamplingOperationDTO.PROPERTY_SAMPLING_DEPARTMENT);
                    break;

                default:
                    break;
            }

            if (error.isError() || error.isWarning()) {
                samplingOperation.addErrors(error);
                if (error.isError()) {
                    isSamplingOperationValid = false;
                }
            }

        }

        // Control sampling operation measurements
        boolean isMeasurementsValid = controlMeasurements(samplingOperation,
                getRules(survey, ControlElementValues.MEASUREMENT, ControlElementValues.TAXON_MEASUREMENT),
                messages,
                updateControlDateWhenSucceed,
                resetControlDateWhenFailed);

        return isSamplingOperationValid && isMeasurementsValid;
    }

    private boolean controlMeasurements(MeasurementAware bean, List<ControlRuleDTO> rules, ControlRuleMessagesBean messages,
                                        boolean updateControlDateWhenSucceed, boolean resetControlDateWhenFailed) {

        List<ErrorDTO> errors = Lists.newArrayList();

        // update control date of controlled measurements
        List<Integer> validMeasurementsElements = Lists.newArrayList();
        List<Integer> invalidMeasurementsElements = Lists.newArrayList();
        List<Integer> validTaxonMeasurementsElements = Lists.newArrayList();
        List<Integer> invalidTaxonMeasurementsElements = Lists.newArrayList();

        // rebuild all potential measurement from pmfms lists
        List<MeasurementDTO> measurementsToControl = Lists.newArrayList();
        List<MeasurementDTO> individualMeasurementsToControl = Lists.newArrayList();
        DaliBeans.populateMeasurementsFromPmfms(bean, measurementsToControl, individualMeasurementsToControl);

        if (updateControlDateWhenSucceed) {
            // by default, all non-empty measurements are valid
            for (MeasurementDTO measurement : measurementsToControl) {
                if (!DaliBeans.isMeasurementEmpty(measurement)) {
                    validMeasurementsElements.add(measurement.getId());
                }
            }
            for (MeasurementDTO individualMeasurement : individualMeasurementsToControl) {
                if (!DaliBeans.isMeasurementEmpty(individualMeasurement)) {
                    if (DaliBeans.isTaxonMeasurement(individualMeasurement)) {
                        validTaxonMeasurementsElements.add(individualMeasurement.getId());
                    } else {
                        validMeasurementsElements.add(individualMeasurement.getId());
                    }
                }
            }
        }

        // All measurements
        for (final MeasurementDTO measurement : measurementsToControl) {

            boolean hasPreControlError = false;
            ErrorDTO preError = executePreControlOnMeasurement(measurement, false, messages);
            if (preError.isError() || preError.isWarning()) {
                errors.add(preError);
                hasPreControlError = true;
            }
            if (preError.isError() && resetControlDateWhenFailed) {
                invalidMeasurementsElements.add(measurement.getId());
                measurement.setControlDate(null);
            } else if (!preError.isError() && updateControlDateWhenSucceed) {
                measurement.setControlDate(messages.getControlDate());
            }

            if (!hasPreControlError) {
                for (final ControlRuleDTO rule : rules) {
                    if (isPmfmFoundInRule(measurement.getPmfm(), rule)) {
                        ErrorDTO error = executeControlOnMeasurement(measurement, rule, false, messages);
                        if (error.isError() || error.isWarning()) {
                            errors.add(error);
                        }
                        if (error.isError() && resetControlDateWhenFailed) {
                            invalidMeasurementsElements.add(measurement.getId());
                            measurement.setControlDate(null);
                        } else if (!error.isError() && updateControlDateWhenSucceed) {
                            measurement.setControlDate(messages.getControlDate());
                        }
                    }
                }
            }
        }

        // All individual measurements
        for (final MeasurementDTO measurement : individualMeasurementsToControl) {

            boolean hasPreControlError = false;
            ErrorDTO preError = executePreControlOnMeasurement(measurement, true, messages);
            if (preError.isError() || preError.isWarning()) {
                errors.add(preError);
                hasPreControlError = true;
            }
            if (preError.isError() && resetControlDateWhenFailed) {
                if (DaliBeans.isTaxonMeasurement(measurement)) {
                    invalidTaxonMeasurementsElements.add(measurement.getId());
                } else {
                    invalidMeasurementsElements.add(measurement.getId());
                }
                measurement.setControlDate(null);
            } else if (!preError.isError() && updateControlDateWhenSucceed) {
                measurement.setControlDate(messages.getControlDate());
            }

            if (!hasPreControlError) {
                for (final ControlRuleDTO rule : rules) {
                    if (isPmfmFoundInRule(measurement.getPmfm(), rule)) {
                        ErrorDTO error;
                        if (DaliBeans.isTaxonMeasurement(measurement)) {
                            error = executeControlOnTaxonMeasurement(measurement, rule, messages);
                        } else {
                            error = executeControlOnMeasurement(measurement, rule, true, messages);
                        }
                        if (error.isError() || error.isWarning()) {
                            errors.add(error);
                        }
                        if (error.isError() && resetControlDateWhenFailed) {
                            if (DaliBeans.isTaxonMeasurement(measurement)) {
                                invalidTaxonMeasurementsElements.add(measurement.getId());
                            } else {
                                invalidMeasurementsElements.add(measurement.getId());
                            }
                            measurement.setControlDate(null);
                        } else if (!error.isError() && updateControlDateWhenSucceed) {
                            measurement.setControlDate(messages.getControlDate());
                        }
                    }
                }
            }
        }

        // remove invalid elements from valid lists
        validMeasurementsElements.removeAll(invalidMeasurementsElements);
        validTaxonMeasurementsElements.removeAll(invalidTaxonMeasurementsElements);

        updateMeasurementsControlDate(validMeasurementsElements, invalidMeasurementsElements, messages.getControlDate());
        updateTaxonMeasurementsControlDate(validTaxonMeasurementsElements, invalidTaxonMeasurementsElements, messages.getControlDate());

        // also add measurements errors to bean
        bean.getErrors().addAll(errors);

        return invalidMeasurementsElements.size() + invalidTaxonMeasurementsElements.size() == 0;
    }

    private ErrorDTO executePreControlOnMeasurement(MeasurementDTO measurement,
                                                    boolean isIndividual,
                                                    ControlRuleMessagesBean messages) {

        // Clear
        measurement.getErrors().clear();
        final ErrorDTO error = newControlError(ControlElementValues.MEASUREMENT);

        if (measurement.getAnalyst() == null && !DaliBeans.isMeasurementEmpty(measurement)) {
            // Add specific message if analyst is missing (Mantis #54144)
            error.setPropertyName(ImmutableList.of(MeasurementDTO.PROPERTY_ANALYST));
            error.setPmfmId(measurement.getPmfm().getId());
            error.setError(true);
            if (isIndividual) error.setIndividualId(measurement.getIndividualId());
            String message = t("dali.service.control.measurement.missing.analyst");
            error.setMessage(message);
            messages.addErrorMessage(message);
            // Apply to measurement
            measurement.addErrors(error);
        }

        return error;
    }

    /**
     * Control measurement detail.
     *
     * @param measurement the measurement to control
     * @param rule        Rule
     */
    private ErrorDTO executeControlOnMeasurement(
            MeasurementDTO measurement,
            ControlRuleDTO rule,
            boolean isIndividual,
            ControlRuleMessagesBean messages) {

        // Clear
        measurement.getErrors().clear();

        // Enum value
        final ControlFeatureMeasurementValues enumValue = ControlFeatureMeasurementValues.getByCode(rule.getControlFeature().getCode());
        if (enumValue == null) {
            throw new DaliTechnicalException(String.format("ControlFeatureMeasurementValues with code=%s has not been found", rule.getControlFeature().getCode()));
        }
        final ErrorDTO error = newControlError(ControlElementValues.MEASUREMENT);

        // Test all features
        switch (enumValue) {
            case ANALYST:
                if (!DaliBeans.isMeasurementEmpty(measurement)) {
                    validObject(measurement.getAnalyst(), rule, messages, error, MeasurementDTO.PROPERTY_ANALYST);
                }
                break;
            case PMFM:
                validObject(measurement.getPmfm(), rule, messages, error, measurement.getPmfm().getId(), isIndividual ? SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS : SurveyDTO.PROPERTY_PMFMS);
                break;
            case NUMERICAL_VALUE:
                validBigDecimal(measurement.getNumericalValue(), rule, messages, error, measurement.getPmfm().getId(), isIndividual ? SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS : SurveyDTO.PROPERTY_PMFMS);
                break;
            case QUALITATIVE_VALUE:
                validObject(measurement.getQualitativeValue(), rule, messages, error, measurement.getPmfm().getId(), isIndividual ? SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS : SurveyDTO.PROPERTY_PMFMS);
                break;
            default:
                break;
        }

        if (error.isError() || error.isWarning()) {
            if (isIndividual) {
                error.setIndividualId(measurement.getIndividualId());
            }
            if (measurement.getErrors() == null) {
                measurement.setErrors(new ArrayList<>());
            }
            measurement.addErrors(error);
        }

        return error;
    }

    /**
     * Control measurement detail.
     *
     * @param measurement the measurement to control
     * @param rule        Rule
     */
    private ErrorDTO executeControlOnTaxonMeasurement(
            MeasurementDTO measurement,
            ControlRuleDTO rule,
            ControlRuleMessagesBean messages) {

        // Clear
        measurement.getErrors().clear();

        // Enum value
        final ControlFeatureTaxonMeasurementValues enumValue = ControlFeatureTaxonMeasurementValues.getByCode(rule.getControlFeature().getCode());
        if (enumValue == null) {
            throw new DaliTechnicalException(String.format("ControlFeatureTaxonMeasurementValues with code=%s has not been found", rule.getControlFeature().getCode()));
        }
        final ErrorDTO error = newControlError(ControlElementValues.TAXON_MEASUREMENT);

        // Test all features
        switch (enumValue) {
            case ANALYST:
                if (!DaliBeans.isMeasurementEmpty(measurement)) {
                    validObject(measurement.getAnalyst(), rule, messages, error, MeasurementDTO.PROPERTY_ANALYST);
                }
                break;
            case PMFM:
                validObject(measurement.getPmfm(), rule, messages, error, measurement.getPmfm().getId(), SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS);
                break;
            case NUMERICAL_VALUE:
                validBigDecimal(measurement.getNumericalValue(), rule, messages, error, measurement.getPmfm().getId(), SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS);
                break;
            case QUALITATIVE_VALUE:
                validObject(measurement.getQualitativeValue(), rule, messages, error, measurement.getPmfm().getId(), SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS);
                break;
            case TAXON:
                validObject(measurement.getTaxon(), rule, messages, error, MeasurementDTO.PROPERTY_TAXON);
                break;
            case TAXON_GROUP:
                validObject(measurement.getTaxonGroup(), rule, messages, error, MeasurementDTO.PROPERTY_TAXON_GROUP);
                break;
            default:
                break;
        }

        if (error.isError() || error.isWarning()) {
            error.setIndividualId(measurement.getIndividualId());
            if (measurement.getErrors() == null) {
                measurement.setErrors(new ArrayList<>());
            }
            measurement.addErrors(error);
        }

        return error;
    }

    /**
     * Valid object with rule control.
     *
     * @param object        Object to test
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param propertyNames the property names
     */
    private void validObject(Object object, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, String... propertyNames) {
        validObject(object, rule, messages, error, null, propertyNames);
    }

    /**
     * Valid object with rule control.
     *
     * @param object        Object to test
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param pmfmId        the pmfmId (optional)
     * @param propertyNames the property names
     */
    private void validObject(Object object, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, Integer pmfmId, String... propertyNames) {

        switch (ControlFunctionValues.getFunctionValue(rule.getFunction().getId())) {

            case IS_EMPTY:
                if (object != null) {
                    addMessage(messages, rule, error, pmfmId, propertyNames);
                }
                break;

            case NOT_EMPTY:
                if (object == null) {
                    addMessage(messages, rule, error, pmfmId, propertyNames);
                }
                break;

            case IS_AMONG:
                if (rule.getAllowedValues() != null) {
                    // get all allowed values names
                    List<String> allowedValues = Lists.newArrayList(rule.getAllowedValues().split(configuration.getValueSeparator()));
                    if (allowedValues.isEmpty()) break;

                    if (object instanceof BaseReferentialDTO) {
                        BaseReferentialDTO baseObject = (BaseReferentialDTO) object;

                        // get the type
                        if (StringUtils.isNumeric(allowedValues.get(0))) {

                            if (baseObject instanceof CodeOnly) {

                                // can't test a CodeOnly object against numeric allowed values
                                if (LOG.isDebugEnabled()) {
                                    LOG.debug(String.format("the %s '%s' is not comparable with allowed values %s",
                                            baseObject.getClass(), ((CodeOnly) baseObject).getCode(), allowedValues));
                                }
                                addMessage(messages, rule, error, pmfmId, propertyNames);

                            } else {

                                // the object's id must be in allowed ids
                                if (!allowedValues.contains(baseObject.getId().toString())) {
                                    if (LOG.isDebugEnabled()) {
                                        LOG.debug(String.format("the %s '%s' is not in allowed values %s",
                                                baseObject.getClass(), baseObject.getId(), allowedValues));
                                    }
                                    addMessage(messages, rule, error, pmfmId, propertyNames);
                                }
                            }

                        } else {

                            if (baseObject instanceof CodeOnly) {

                                // the object's code must be in allowed names
                                CodeOnly codeBaseObject = (CodeOnly) baseObject;
                                if (!allowedValues.contains(codeBaseObject.getCode())) {
                                    if (LOG.isDebugEnabled()) {
                                        LOG.debug(String.format("the %s '%s' is not in allowed values %s",
                                                baseObject.getClass(), codeBaseObject.getCode(), allowedValues));
                                    }
                                    addMessage(messages, rule, error, pmfmId, propertyNames);
                                }

                            } else {

                                // the object's name must be in allowed names
                                if (!allowedValues.contains(baseObject.getName())) {
                                    if (LOG.isDebugEnabled()) {
                                        LOG.debug(String.format("the %s '%s' is not in allowed values %s",
                                                baseObject.getClass(), baseObject.getName(), allowedValues));
                                    }
                                    addMessage(messages, rule, error, pmfmId, propertyNames);
                                }
                            }
                        }
                    } else {
                        // the object is not controllable or null
                        addMessage(messages, rule, error, pmfmId, propertyNames);
                    }
                }
                break;
            default:
                // Do nothing
                break;
        }
    }

    private void validCollection(Collection collection, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, String... propertyNames) {

        switch (ControlFunctionValues.getFunctionValue(rule.getFunction().getId())) {

            case IS_EMPTY:
                if (CollectionUtils.isNotEmpty(collection)) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case NOT_EMPTY:
                if (CollectionUtils.isEmpty(collection)) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case IS_AMONG:
                if (rule.getAllowedValues() != null) {
                    if (CollectionUtils.isNotEmpty(collection)) {

                        // control each object in collection with the current IS_AMONG rule
                        for (Object object : collection) {

                            validObject(object, rule, messages, error, propertyNames);

                            // stop loop if error occurs
                            if (error.isWarning() || error.isError()) break;
                        }

                    } else {
                        addMessage(messages, rule, error, null, propertyNames);
                    }
                }
                break;
            default:
                // Do nothing
                break;
        }

    }

    /**
     * Valid date with rule control.
     *
     * @param dateValue     Date
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param propertyNames the property names
     */
    private void validDate(Date dateValue, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, String... propertyNames) {

        switch (ControlFunctionValues.getFunctionValue(rule.getFunction().getId())) {

            case IS_EMPTY:
                if (dateValue != null) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case NOT_EMPTY:
                if (dateValue == null) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case MIN_MAX_DATE:
                if (dateValue == null) {
                    addMessage(messages, rule, error, null, propertyNames);
                } else {

                    // Min value
                    Date minDate = null;
                    if (rule.getMin() != null) {
                        minDate = (Date) rule.getMin();
                    }

                    // Max value
                    Date maxDate = null;
                    if (rule.getMax() != null) {
                        maxDate = (Date) rule.getMax();
                    }

                    // Date between minDate & maxDate
                    if (minDate != null && maxDate != null) {
                        if (dateValue.before(minDate) || dateValue.after(maxDate)) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    } else if (minDate != null) {
                        if (dateValue.before(minDate)) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    } else if (maxDate != null) {
                        if (dateValue.after(maxDate)) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    }
                }
                break;

            default:
                // Do nothing
                break;
        }

    }

    /**
     * Valid local date with rule control.
     *
     * @param dateValue     Date
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param propertyNames the property names
     */
    private void validLocalDate(LocalDate dateValue, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, String... propertyNames) {

        switch (ControlFunctionValues.getFunctionValue(rule.getFunction().getId())) {

            case IS_EMPTY:
                if (dateValue != null) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case NOT_EMPTY:
                if (dateValue == null) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case MIN_MAX_DATE:
                if (dateValue == null) {
                    addMessage(messages, rule, error, null, propertyNames);
                } else {

                    // Min value
                    LocalDate minDate = null;
                    if (rule.getMin() != null) {
                        Object min = rule.getMin();
                        if (min instanceof Date) {
                            minDate = Dates.convertToLocalDate((Date) min, configuration.getDbTimezone());
                        } else if (min instanceof LocalDate) {
                            minDate = (LocalDate) min;
                        } else {
                            throw new DaliTechnicalException(String.format("the min date in rule %s is invalid : %s", rule.getCode(), min));
                        }
                    }

                    // Max value
                    LocalDate maxDate = null;
                    if (rule.getMax() != null) {
                        Object max = rule.getMax();
                        if (max instanceof Date) {
                            maxDate = Dates.convertToLocalDate((Date) max, configuration.getDbTimezone());
                        } else if (max instanceof LocalDate) {
                            maxDate = (LocalDate) max;
                        } else {
                            throw new DaliTechnicalException(String.format("the max date in rule %s is invalid : %s", rule.getCode(), max));
                        }
                    }

                    // Date between minDate & maxDate
                    if (minDate != null && maxDate != null) {
                        if (dateValue.isBefore(minDate) || dateValue.isAfter(maxDate)) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    } else if (minDate != null) {
                        if (dateValue.isBefore(minDate)) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    } else if (maxDate != null) {
                        if (dateValue.isAfter(maxDate)) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    }
                }
                break;

            default:
                // Do nothing
                break;
        }

    }

    /**
     * Valid integer value with rule control.
     *
     * @param integerValue  Integer value to test
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param propertyNames the property names
     */
    @SuppressWarnings("unused")
    private void validInteger(Integer integerValue, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, String... propertyNames) {

        switch (ControlFunctionValues.getFunctionValue(rule.getFunction().getId())) {

            case IS_EMPTY:
                if (integerValue != null) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case NOT_EMPTY:
                if (integerValue == null) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case MIN_MAX:
                if (integerValue == null) {
                    addMessage(messages, rule, error, null, propertyNames);
                } else {

                    // Min value
                    Integer minValue = null;
                    if (rule.getMin() != null) {
                        minValue = (Integer) rule.getMin();
                    }

                    // Max value
                    Integer maxValue = null;
                    if (rule.getMax() != null) {
                        maxValue = (Integer) rule.getMax();
                    }

                    // Integer between minValue & maxValue
                    if (minValue != null && maxValue != null) {
                        if (integerValue < minValue && integerValue > maxValue) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    }
                    if (minValue != null) {
                        if (integerValue < minValue) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    }
                    if (maxValue != null) {
                        if (integerValue > maxValue) {
                            addMessage(messages, rule, error, null, propertyNames);
                        }
                    }
                }
                break;

            case IS_AMONG:
                if (rule.getAllowedValues() != null) {

                    // Integer values
                    final List<Integer> integerValues = new ArrayList<>();

                    // String values
                    final String[] stringValues = rule.getAllowedValues().split(configuration.getValueSeparator());
                    for (final String stringValue : stringValues) {
                        integerValues.add(Integer.parseInt(stringValue));
                    }

                    // integerValue must be into integerValues
                    if (!integerValues.contains(integerValue)) {
                        addMessage(messages, rule, error, null, propertyNames);
                    }
                }
                break;

            default:
                // Do nothing
                break;
        }

    }

    /**
     * Valid double value with rule control.
     *
     * @param doubleValue   Double value to test
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param propertyNames the property names
     */
    private void validDouble(Double doubleValue, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, String... propertyNames) {
        validDouble(doubleValue, rule, messages, error, null, propertyNames);
    }

    /**
     * Valid double value with rule control.
     *
     * @param doubleValue   Double value to test
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param pmfmId        the pmfm id (optional)
     * @param propertyNames the property names
     */
    private void validDouble(Double doubleValue, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, Integer pmfmId, String... propertyNames) {

        switch (ControlFunctionValues.getFunctionValue(rule.getFunction().getId())) {

            case IS_EMPTY:
                // Object must be null
                if (doubleValue != null) {
                    addMessage(messages, rule, error, pmfmId, propertyNames);
                }
                break;

            case NOT_EMPTY:
                // Object must not be null
                if (doubleValue == null) {
                    addMessage(messages, rule, error, pmfmId, propertyNames);
                }
                break;

            case MIN_MAX:
                if (doubleValue == null) {
                    addMessage(messages, rule, error, pmfmId, propertyNames);
                } else {

                    // Min value
                    Double minValue = null;
                    if (rule.getMin() != null) {
                        minValue = (Double) rule.getMin();
                    }

                    // Max value
                    Double maxValue = null;
                    if (rule.getMax() != null) {
                        maxValue = (Double) rule.getMax();
                    }

                    // Double between minValue & maxValue (if exist)
                    if (minValue != null && maxValue != null) {
                        if (doubleValue < minValue || doubleValue > maxValue) {
                            addMessage(messages, rule, error, pmfmId, propertyNames);
                        }
                    } else if (minValue != null) {
                        if (doubleValue < minValue) {
                            addMessage(messages, rule, error, pmfmId, propertyNames);
                        }
                    } else if (maxValue != null) {
                        if (doubleValue > maxValue) {
                            addMessage(messages, rule, error, pmfmId, propertyNames);
                        }
                    }
                }
                break;

            case IS_AMONG:
                if (rule.getAllowedValues() != null) {

                    // Double values
                    final List<Double> doubleValues = new ArrayList<>();

                    // String values
                    final String[] stringValues = rule.getAllowedValues().split(configuration.getValueSeparator());
                    for (final String stringValue : stringValues) {
                        try {
                            doubleValues.add(Double.parseDouble(stringValue));
                        } catch (NumberFormatException nfe) {
                            if (LOG.isErrorEnabled()) {
                                LOG.error(String.format("this value '%s' can't be cast as Double", stringValue));
                            }
                        }
                    }

                    // doubleValue must be into doubleValues
                    if (!doubleValues.contains(doubleValue)) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug(String.format("the double value %s is not in allowed values %s", doubleValue, doubleValues));
                        }

                        addMessage(messages, rule, error, pmfmId, propertyNames);
                    }
                }
                break;

            default:
                // Do nothing
                break;
        }
    }

    /**
     * Valid BigDecimal (as double) value with rule control.
     *
     * @param bigDecimalValue BigDecimal value to test
     * @param rule            Rule apply
     * @param messages        the messages bean
     * @param error           the Error object
     * @param pmfmId          the pmfm Id
     * @param propertyNames   the property names
     */
    private void validBigDecimal(BigDecimal bigDecimalValue, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, Integer pmfmId, String... propertyNames) {

        Double doubleValue = bigDecimalValue == null ? null : bigDecimalValue.doubleValue();
        validDouble(doubleValue, rule, messages, error, pmfmId, propertyNames);
    }

    /**
     * Valid String value with rule control.
     *
     * @param stringValue   String value to test
     * @param rule          Rule apply
     * @param messages      the messages bean
     * @param error         the Error object
     * @param propertyNames the property names
     */
    private void validString(String stringValue, ControlRuleDTO rule, ControlRuleMessagesBean messages, ErrorDTO error, String... propertyNames) {

        switch (ControlFunctionValues.getFunctionValue(rule.getFunction().getId())) {

            case IS_EMPTY:
                // Object have to be null or empty
                if (StringUtils.isNotBlank(stringValue)) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case NOT_EMPTY:
                // Object should not be null or empty
                if (StringUtils.isBlank(stringValue)) {
                    addMessage(messages, rule, error, null, propertyNames);
                }
                break;

            case IS_AMONG:
                if (rule.getAllowedValues() != null) {
                    // String values
                    final List<String> stringValues = new ArrayList<>();

                    // String values
                    final String[] stringTabValues = rule.getAllowedValues().split(configuration.getValueSeparator());
                    Collections.addAll(stringValues, stringTabValues);

                    // stringValue must be into stringValues
                    if (!stringValues.contains(stringValue)) {
                        addMessage(messages, rule, error, null, propertyNames);
                    }
                }
                break;

            default:
                // Do nothing
                break;
        }
    }

    private void addMessage(ControlRuleMessagesBean messages, ControlRuleDTO rule, ErrorDTO error, Integer pmfmId, String... propertyNames) {
        error.setPropertyName(Arrays.asList(propertyNames));
        error.setPmfmId(pmfmId);
        error.setMessage(getMessage(rule));
        if (rule.isBlocking()) {
            error.setError(true);
            messages.addErrorMessage(error.getMessage());
        } else {
            error.setWarning(true);
            messages.addWarningMessage(error.getMessage());
        }
    }

    private void addGeometryMessage(ControlRuleMessagesBean messages, ErrorDTO error, String message, String... propertyNames) {
        error.setPropertyName(Arrays.asList(propertyNames));
        error.setWarning(true);
        error.setMessage(message);
        messages.addWarningMessage(message);
    }

    private String getMessage(ControlRuleDTO rule) {
        if (StringUtils.isNotBlank(rule.getMessage())) {
            return rule.getMessage();
        }

        // compute a generic message
        return t("dali.service.control.invalid.message", rule.getCode());
    }

    private ErrorDTO newControlError(ControlElementValues controlElementValue) {
        ErrorDTO error = DaliBeanFactory.newErrorDTO();
        error.setWarning(false);
        error.setError(false);
        error.setControl(true);
        error.setControlElementCode(controlElementValue.getCode());
        return error;
    }

    private boolean isPmfmFoundInRule(PmfmDTO pmfm, ControlRuleDTO rule) {

        if (rule.isRulePmfmsEmpty()) {
            // don't check if no pmfm in rule
            return true;
        }

        for (RulePmfmDTO rulePmfm : rule.getRulePmfms()) {

            // if the quintuplet is found
            if (rulePmfm.getPmfm().getParameter().equals(pmfm.getParameter())
                    && (rulePmfm.getPmfm().getMatrix() == null || rulePmfm.getPmfm().getMatrix().equals(pmfm.getMatrix()))
                    && (rulePmfm.getPmfm().getFraction() == null || rulePmfm.getPmfm().getFraction().equals(pmfm.getFraction()))
                    && (rulePmfm.getPmfm().getMethod() == null || rulePmfm.getPmfm().getMethod().equals(pmfm.getMethod()))
                    && (rulePmfm.getPmfm().getUnit() == null || rulePmfm.getPmfm().getUnit().equals(pmfm.getUnit()))
            ) {
                return true;
            }
        }

        return false;
    }

    private void updateSurveysControlDate(Collection<Integer> validControlledElementsPks, Collection<Integer> invalidControlledElementsPks, Date controlDate) {

        if (CollectionUtils.isNotEmpty(validControlledElementsPks)) {
            surveyDao.updateSurveysControlDate(validControlledElementsPks, controlDate);
        }

        if (CollectionUtils.isNotEmpty(invalidControlledElementsPks)) {
            surveyDao.updateSurveysControlDate(invalidControlledElementsPks, null);
        }

    }

    private void updateSamplingOperationsControlDate(Collection<Integer> validControlledElementsPks, Collection<Integer> invalidControlledElementsPks, Date controlDate) {

        if (CollectionUtils.isNotEmpty(validControlledElementsPks)) {
            samplingOperationDao.updateSamplingOperationsControlDate(validControlledElementsPks, controlDate);
        }

        if (CollectionUtils.isNotEmpty(invalidControlledElementsPks)) {
            samplingOperationDao.updateSamplingOperationsControlDate(invalidControlledElementsPks, null);
        }

    }

    private void updateMeasurementsControlDate(Collection<Integer> validControlledElementsPks, Collection<Integer> invalidControlledElementsPks, Date controlDate) {

        if (CollectionUtils.isNotEmpty(validControlledElementsPks)) {
            measurementDao.updateMeasurementsControlDate(validControlledElementsPks, controlDate);
        }

        if (CollectionUtils.isNotEmpty(invalidControlledElementsPks)) {
            measurementDao.updateMeasurementsControlDate(invalidControlledElementsPks, null);
        }

    }

    private void updateTaxonMeasurementsControlDate(Collection<Integer> validControlledElementsPks, Collection<Integer> invalidControlledElementsPks, Date controlDate) {

        if (CollectionUtils.isNotEmpty(validControlledElementsPks)) {
            measurementDao.updateTaxonMeasurementsControlDate(validControlledElementsPks, controlDate);
        }

        if (CollectionUtils.isNotEmpty(invalidControlledElementsPks)) {
            measurementDao.updateTaxonMeasurementsControlDate(invalidControlledElementsPks, null);
        }

    }

}
