package fr.ifremer.dali.service.persistence;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.administration.program.DaliProgramDao;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.administration.user.DaliQuserDao;
import fr.ifremer.dali.dao.data.survey.DaliCampaignDao;
import fr.ifremer.dali.dao.referential.DaliAnalysisInstrumentDao;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.DaliSamplingEquipmentDao;
import fr.ifremer.dali.dao.referential.DaliUnitDao;
import fr.ifremer.dali.dao.referential.monitoringLocation.DaliMonitoringLocationDao;
import fr.ifremer.dali.dao.referential.pmfm.*;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonGroupDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonNameDao;
import fr.ifremer.dali.dao.system.context.DaliContextDao;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.technical.DatabaseSchemaDao;
import fr.ifremer.quadrige3.core.exception.DatabaseSchemaUpdateException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.exception.VersionNotFoundException;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.version.Version;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PersistenceServiceImpl class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
@Service("daliPersistenceService")
public class PersistenceServiceImpl implements PersistenceService {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(PersistenceServiceImpl.class);

    @Resource
    protected CacheService cacheService;
    @Resource
    protected DaliConfiguration config;
    @Resource
    protected DatabaseSchemaDao databaseSchemaDao;
    @Resource(name = "daliContextDao")
    protected DaliContextDao contextDao;
    @Resource(name = "daliProgramDao")
    protected DaliProgramDao programDao;
    @Resource(name = "daliCampaignDao")
    protected DaliCampaignDao campaignDao;
    @Resource(name = "daliQuserDao")
    protected DaliQuserDao quserDao;
    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;
    @Resource(name = "daliReferentialDao")
    protected DaliReferentialDao referentialDao;
    @Resource(name = "daliUnitDao")
    protected DaliUnitDao unitDao;
    @Resource(name = "daliMonitoringLocationDao")
    protected DaliMonitoringLocationDao monitoringLocationDao;
    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;
    @Resource(name = "daliParameterDao")
    protected DaliParameterDao parameterDao;
    @Resource(name = "daliMatrixDao")
    protected DaliMatrixDao matrixDao;
    @Resource(name = "daliFractionDao")
    protected DaliFractionDao fractionDao;
    @Resource(name = "daliMethodDao")
    protected DaliMethodDao methodDao;
    @Resource(name = "daliTaxonGroupDao")
    protected DaliTaxonGroupDao taxonGroupDao;
    @Resource(name = "daliTaxonNameDao")
    protected DaliTaxonNameDao taxonNameDao;
    @Resource(name = "daliAnalysisInstrumentDao")
    private DaliAnalysisInstrumentDao analysisInstrumentDao;
    @Resource(name = "daliSamplingEquipmentDao")
    private DaliSamplingEquipmentDao samplingEquipmentDao;

    private boolean compactDatabaseOnClose = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() {
        if (config.isCleanCacheAtStartup()) {
            clearAllCaches();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        shutdownDatabase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Version getDbVersion() {
        try {
            if (!databaseSchemaDao.isDbLoaded()) {
                throw new VersionNotFoundException("db is not open");
            }
            return databaseSchemaDao.getSchemaVersion();
        } catch (VersionNotFoundException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error("Could not find db version", e);
            }
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Version getApplicationVersion() {
        return databaseSchemaDao.getSchemaVersionIfUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateSchema() {
        try {
            databaseSchemaDao.updateSchema();
        } catch (DatabaseSchemaUpdateException e) {
            throw new ApplicationTechnicalException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void compactDb() {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Compacting database");
        }

        Daos.compactDatabase(getDataSource());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearAllCaches() {
        cacheService.clearAllCaches();

        // wait a while to be sure all caches are cleared
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignored) {
        }

        System.gc();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadDefaultCaches(ProgressionCoreModel progressionModel) {
        if (config.isCacheEnabledAtStartup()) {

            if (LOG.isDebugEnabled()) {
                LOG.debug("Loading default caches...");
            }

            loadReferentialCaches(progressionModel);

            if (LOG.isDebugEnabled()) {
                LOG.debug("Loading default caches... [OK]");
            }
        }
    }

    @Override
    public void enableMassiveUpdate() {

        if (!Daos.isHsqlFileDatabase(config.getJdbcUrl())) return;

        if (LOG.isDebugEnabled()) {
            LOG.debug("Enable massive update behavior [CHECKPOINT first]");
        }

        // checkpoint first
        Daos.sqlUpdate(getDataSource(), "CHECKPOINT");

        if (LOG.isDebugEnabled()) {
            LOG.debug("Enable massive update behavior");
        }

        // set incremental backup disabled
        Daos.sqlUpdate(getDataSource(), "SET FILES BACKUP INCREMENT FALSE");

    }

    @Override
    public void disableMassiveUpdate() {

        if (!Daos.isHsqlFileDatabase(config.getJdbcUrl())) return;

        if (LOG.isDebugEnabled()) {
            LOG.debug("Disable massive update behavior [CHECKPOINT first]");
        }

        // checkpoint first
        Daos.sqlUpdate(getDataSource(), "CHECKPOINT");

        if (LOG.isDebugEnabled()) {
            LOG.debug("Disable massive update behavior");
        }

        // set incremental backup enabled (default)
        Daos.sqlUpdate(getDataSource(), "SET FILES BACKUP INCREMENT TRUE");

    }

    @Override
    public void compactDatabaseOnClose() {
        this.compactDatabaseOnClose = true;
    }

    /* -- internal methods -- */

    /**
     * <p>loadReferentialCaches.</p>
     *
     * @param progressionModel a {@link ProgressionCoreModel} object.
     */
    private void loadReferentialCaches(ProgressionCoreModel progressionModel) {

        List<CacheLoaderTask> tasks = new ArrayList<>();
        tasks.add(new CacheLoaderTask("All status", () -> referentialDao.getAllStatus()));
        tasks.add(new CacheLoaderTask("All contexts", () -> contextDao.getAllContext()));
        tasks.add(new CacheLoaderTask("Active departments", () -> departmentDao.getAllDepartments(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active users", () -> quserDao.getAllUsers(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("All programs", () -> programDao.getAllPrograms()));
        tasks.add(new CacheLoaderTask("All campaigns", () -> campaignDao.getAllCampaigns()));
        tasks.add(new CacheLoaderTask("All depth levels", () -> referentialDao.getAllDepthLevels()));
        tasks.add(new CacheLoaderTask("All grouping types", () -> referentialDao.getAllGroupingTypes()));
        tasks.add(new CacheLoaderTask("All positioning systems", () -> referentialDao.getAllPositioningSystems()));
        tasks.add(new CacheLoaderTask("Active quality flags", () -> referentialDao.getAllQualityFlags(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active analysis instruments", () -> analysisInstrumentDao.getAllAnalysisInstruments(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active sampling equipments", () -> samplingEquipmentDao.getAllSamplingEquipments(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active units", () -> unitDao.getAllUnits(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("All locations", () -> monitoringLocationDao.getAllLocations(StatusFilter.ALL.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active locations", () -> monitoringLocationDao.getAllLocations(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active harbours", () -> monitoringLocationDao.getAllHarbours(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("All parameter groups", () -> parameterDao.getAllParameterGroups(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active parameters", () -> parameterDao.getAllParameters(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active Matrices", () -> matrixDao.getAllMatrices(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active fractions", () -> fractionDao.getAllFractions(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active methods", () -> methodDao.getAllMethods(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active pmfmus", () -> pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("All taxonomic levels", () -> referentialDao.getAllTaxonomicLevels()));
        tasks.add(new CacheLoaderTask("All taxon names and reference taxons", () -> taxonNameDao.fillReferents(taxonNameDao.getAllTaxonNames())));
        tasks.add(new CacheLoaderTask("All taxon groups", () -> {
            // read previous reference date in taxon group - taxon map
            Cache cache = cacheService.getCache(DaliTaxonNameDao.TAXON_NAME_BY_TAXON_GROUP_ID_CACHE);
            if (cache != null && cache.get(LocalDate.now()) == null) {
                // if the cache doesn't hold this reference date, clear the taxon groups cache
                cacheService.clearCache(DaliTaxonGroupDao.ALL_TAXON_GROUPS_CACHE);
            }
            taxonGroupDao.getAllTaxonGroups();
        }));

        // Run the garbage collector before (Mantis #59518)
        System.gc();

        // run all
        progressionModel.setTotal(tasks.size());
        tasks.forEach(task -> {
            try {
                try {
                    task.run();
                } catch (OutOfMemoryError error) {
                    LOG.warn(String.format("Try to resolve a memory exception when loading '%s': Run the garbage collector then retry.", task.getName()));
                    System.gc();
                    Thread.sleep(1000);
                    task.run();
                }
            } catch (Throwable t) {
                throw new QuadrigeTechnicalException(String.format("Error when loading '%s'", task.getName()), t);
            }
            progressionModel.increments(1);
        });
    }

    /**
     * <p>shutdownDatabase.</p>
     */
    private void shutdownDatabase() {
        // Do not shutdown if database run as server
        if (!Daos.isHsqlFileDatabase(config.getJdbcUrl())) {
            return;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("shutting down database");
        }
        Daos.shutdownDatabase(getDataSource(), compactDatabaseOnClose);
        if (LOG.isDebugEnabled()) {
            LOG.debug("database down");
        }
    }

    /**
     * <p>getDataSource.</p>
     *
     * @return a {@link javax.sql.DataSource} object.
     */
    protected DataSource getDataSource() {
        return DaliServiceLocator.instance().getService("dataSource", DataSource.class);
    }

    static class CacheLoaderTask implements Runnable {
        private final String name;
        private final Runnable runnable;

        CacheLoaderTask(String name, Runnable runnable) {
            this.name = name;
            this.runnable = runnable;
        }

        public String getName() {
            return name;
        }

        @Override
        public void run() {
            this.runnable.run();
        }
    }
}
