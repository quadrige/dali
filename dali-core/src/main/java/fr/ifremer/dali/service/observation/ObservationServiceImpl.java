package fr.ifremer.dali.service.observation;

        /*
         * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
         */

import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.data.measurement.DaliMeasurementDao;
import fr.ifremer.dali.dao.data.photo.DaliPhotoDao;
import fr.ifremer.dali.dao.data.samplingoperation.DaliSamplingOperationDao;
import fr.ifremer.dali.dao.data.survey.DaliSurveyDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.dali.dto.configuration.moratorium.MoratoriumPmfmDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.data.survey.SurveyFilterDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.dto.enums.SearchDateValues;
import fr.ifremer.dali.dto.enums.SynchronizationStatusValues;
import fr.ifremer.dali.dto.referential.*;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.system.QualificationHistoryDTO;
import fr.ifremer.dali.dto.system.ValidationHistoryDTO;
import fr.ifremer.dali.service.DaliDataContext;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.service.administration.campaign.CampaignService;
import fr.ifremer.dali.service.administration.context.ContextService;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.dali.service.administration.user.UserService;
import fr.ifremer.dali.service.persistence.PersistenceService;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.referential.QualityFlagCode;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ObservationServiceImpl class.</p>
 *
 * @author Ludovic
 */
@Service("daliSurveyService")
public class ObservationServiceImpl implements ObservationInternalService {

    public static final Log LOG = LogFactory.getLog(ObservationServiceImpl.class);

    @Resource(name = "daliSurveyService")
    private ObservationInternalService loopBackService;

    @Resource(name = "daliPersistenceService")
    private PersistenceService persistenceService;

    @Resource(name = "daliDataContext")
    private DaliDataContext dataContext;

    @Resource(name = "daliContextService")
    private ContextService contextService;

    @Resource(name = "daliReferentialService")
    private ReferentialService referentialService;

    @Resource(name = "daliUserService")
    private UserService userService;

    @Resource(name = "daliProgramStrategyService")
    private ProgramStrategyService programStrategyService;

    @Resource(name = "daliCampaignService")
    private CampaignService campaignService;

    @Resource(name = "daliSurveyDao")
    private DaliSurveyDao surveyDao;

    @Resource(name = "daliSamplingOperationDao")
    private DaliSamplingOperationDao samplingOperationDao;

    @Resource(name = "daliMeasurementDao")
    private DaliMeasurementDao measurementDao;

    @Resource(name = "daliPhotoDao")
    private DaliPhotoDao photoDao;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Autowired
    protected DaliConfiguration config;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SurveyDTO> getSurveys(SurveyFilterDTO surveyFilter) {
        Integer campaignId = surveyFilter.getCampaignId();
        String programCode = surveyFilter.getProgramCode();
        Integer locationId = surveyFilter.getLocationId();
        Integer stateId = surveyFilter.getStateId();
        Integer synchronizationStatusId = surveyFilter.getShareId();
        String name = surveyFilter.getName();
        String comment = surveyFilter.getComment();
        LocalDate startDate = null;
        LocalDate endDate = null;
        boolean strictDate = false;
        if (surveyFilter.getSearchDateId() != null) {
            SearchDateValues searchDateValue = SearchDateValues.values()[surveyFilter.getSearchDateId()];
            switch (searchDateValue) {
                case EQUALS:
                    startDate = surveyFilter.getDate1();
                    endDate = startDate;
                    strictDate = true;
                    break;
                case BETWEEN:
                    startDate = surveyFilter.getDate1();
                    endDate = surveyFilter.getDate2();
                    break;
                case BEFORE:
                    endDate = surveyFilter.getDate1();
                    strictDate = true;
                    break;
                case BEFORE_OR_EQUALS:
                    endDate = surveyFilter.getDate1();
                    break;
                case AFTER:
                    startDate = surveyFilter.getDate1();
                    strictDate = true;
                    break;
                case AFTER_OR_EQUALS:
                    startDate = surveyFilter.getDate1();
                    break;
            }
        }
        Collection<String> programCodes;
        // if no filter on single program, force filter on readable programs
        if (StringUtils.isBlank(programCode)) {
            programCodes = programStrategyService.getReadableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());
        } else {
            programCodes = Collections.singleton(programCode);
        }

        long start = System.currentTimeMillis();
        List<SurveyDTO> surveys = surveyDao.getSurveysByCriteria(
                campaignId,
                programCodes,
                locationId,
                name,
                comment,
                stateId,
                synchronizationStatusId,
                Dates.convertToDate(startDate, config.getDbTimezone()),
                Dates.convertToDate(endDate, config.getDbTimezone()),
                strictDate);

        LOG.debug(String.format("%s surveys loaded in %s", surveys.size(), Times.durationToString(System.currentTimeMillis() - start)));

        filterSurveysUnderMoratorium(surveys);

        return surveys;
    }

    private void filterSurveysUnderMoratorium(List<SurveyDTO> surveys) {

        Integer userId = dataContext.getRecorderPersonId();
        Assert.notNull(userId);
        Integer departmentId = dataContext.getRecorderDepartmentId();
        Assert.notNull(departmentId);
        boolean trace = LOG.isTraceEnabled();

        if (trace) {
            LOG.trace(String.format("%s surveys to filter", surveys.size()));
        }

        surveys.removeIf(survey -> {

            ProgramDTO program = survey.getProgram();

            // No moratorium: keep it
            if (program.isMoratoriumsEmpty()) {
                if (trace) {
                    LOG.trace("Survey not removed: no moratorium");
                }
                return false;
            }

            // User is viewer or not recorder
            if (!dataContext.isSurveyViewable(survey)) {

                // Survey is in global moratorium period: remove it
                boolean remove = program.getMoratoriums().stream()
                    .filter(MoratoriumDTO::isGlobal)
                    .flatMap(moratorium -> moratorium.getPeriods().stream())
                    .anyMatch(period -> !survey.getDate().isAfter(period.getEndDate()) && !survey.getDate().isBefore(period.getStartDate()));
                if (trace) {
                    LOG.trace(String.format("Survey %s on date %s", remove ? "REMOVED":"KEPT", survey.getDate()));
                }
                return remove;
            }

            // default: survey is not removed
            if (trace) {
                LOG.trace(String.format("Survey VIEWABLE on date %s", survey.getDate()));
            }
            return false;

        });

        if (trace) {
            LOG.trace(String.format("%s surveys after filter", surveys.size()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadSamplingOperationsFromSurvey(SurveyDTO survey, boolean withIndividualMeasurements) {

        if (survey == null || survey.getId() == null) {
            return;
        }

        if (!survey.isSamplingOperationsLoaded()) {

            // load and affect sampling operations
            survey.setSamplingOperations(samplingOperationDao.getSamplingOperationsBySurveyId(survey.getId(), withIndividualMeasurements));
            survey.setSamplingOperationsLoaded(true);

            if (!survey.isSamplingOperationsEmpty()) {

                // collect measurements (ungrouped only)
                for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {

                    // affect all pmfms to each sampling operation, even if it don't have measurement with all pmfms
                    if (samplingOperation.getPmfms() == null) {
                        samplingOperation.setPmfms(new ArrayList<>());
                    }

                    if (withIndividualMeasurements) {
                        if (samplingOperation.getIndividualPmfms() == null) {
                            samplingOperation.setIndividualPmfms(new ArrayList<>());
                        }
                    } else {
                        // clear individual pmfms and measurements
                        samplingOperation.setIndividualPmfms(null);
                        samplingOperation.setIndividualMeasurements(null);
                        samplingOperation.setIndividualMeasurementsLoaded(false); // should already be set to false
                    }

                    // add PMFMs from existing ungrouped measurements
                    DaliBeans.populatePmfmsFromMeasurements(samplingOperation);

                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SamplingOperationDTO newSamplingOperation(SurveyDTO survey, List<PmfmDTO> pmfms) {
        Assert.notNull(survey);
        Assert.notNull(pmfms);

        SamplingOperationDTO result = DaliBeanFactory.newSamplingOperationDTO();

        // add empty measurements
        for (final PmfmDTO pmfm : pmfms) {
            final MeasurementDTO measurement = DaliBeanFactory.newMeasurementDTO();
            measurement.setPmfm(pmfm);
            result.addMeasurements(measurement);
        }
        result.addAllPmfms(pmfms);

        // add department from strategy
        result.setSamplingDepartment(programStrategyService.getSamplingDepartmentOfAppliedStrategyBySurvey(survey));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO getSurvey(Integer surveyId) {
        return getSurvey(surveyId, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO getSurveyWithoutPmfmFiltering(Integer surveyId) {
        return getSurvey(surveyId, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveSurveys(Collection<? extends SurveyDTO> surveys, ProgressionCoreModel progressionModel) {

        List<SurveyDTO> surveysToSave = surveys.stream().filter(SurveyDTO::isDirty).collect(Collectors.toList());
        progressionModel.setTotal(surveysToSave.size());

        for (SurveyDTO survey : surveysToSave) {
            progressionModel.increments(t("dali.service.common.progression",
                    t("dali.service.observation.save"), progressionModel.getCurrent() + 1, progressionModel.getTotal()));

            if (survey.getId() == null) {

                // add recorderDepartment if any
                if (survey.getRecorderDepartment() == null) {
                    // get department from authenticated user
                    Integer depId = dataContext.getRecorderDepartmentId();
                    if (depId == null) {
                        throw new DaliTechnicalException("no RecorderDepartmentId found in data context");
                    }
                    survey.setRecorderDepartment(departmentDao.getDepartmentById(depId));
                }
            }

            saveSurvey(survey);
            survey.setDirty(false);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveSurvey(SurveyDTO survey) {
        // when saving, reset control date - see mantis #26451
        if (survey.isDirty() || survey.getControlDate() != null) {
            survey.setControlDate(null);
        }

        // Apply default analysis department, from strategy (mantis #28257)
        applyDefaultAnalysisDepartment(survey);

        // Persist the update survey
        surveyDao.save(survey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSurveys(List<Integer> surveyIds) {
        if (CollectionUtils.isEmpty(surveyIds)) return;
        Integer recorderUserId = dataContext.getRecorderPersonId();

        // Do not delete, but insert into DeletedItemHistory
        surveyIds.stream().filter(Objects::nonNull).distinct().forEach(surveyId -> surveyDao.removeUsingDeletedItemHistory(surveyId, recorderUserId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO duplicateSurvey(SurveyDTO survey, boolean fullDuplication, boolean copyCoordinates) {

        // Duplicate survey
        SurveyDTO duplicateSurvey = DaliBeans.clone(survey);
        // Remove ID
        duplicateSurvey.setId(null);
        duplicateSurvey.setDirty(true);

        // Remove location if disabled
        if (DaliBeans.isDisabledStatus(survey.getLocation().getStatus())) {
            duplicateSurvey.setLocation(null);
        }

        // duplicated data to keep:
        if (survey.isObserversEmpty() && survey.getId() != null) {
            duplicateSurvey.setObservers(surveyDao.getObservers(survey.getId()));
        } else {
            duplicateSurvey.setObservers(DaliBeans.clone(survey.getObservers()));
        }

        // reset other survey data
        duplicateSurvey.setPhotos(null);
        duplicateSurvey.setPhotosLoaded(false);
        duplicateSurvey.setMeasurements(null);
        duplicateSurvey.setIndividualMeasurements(null);
        duplicateSurvey.setMeasurementsLoaded(false);
        duplicateSurvey.setPmfms(null);
        duplicateSurvey.setIndividualPmfms(null);
        duplicateSurvey.setComment(null);
        duplicateSurvey.setQualificationComment(null);
        duplicateSurvey.setValidationDate(null);
        duplicateSurvey.setUpdateDate(null);
        duplicateSurvey.setControlDate(null);
        duplicateSurvey.setSynchronizationStatus(null);
        duplicateSurvey.setErrors(null);

        if (copyCoordinates) {
            duplicateSurvey.setCoordinate(DaliBeans.clone(survey.getCoordinate()));
            // and positioning system
            duplicateSurvey.setPositioning(survey.getPositioning());
            // keep positioning comment if we keep same positioning ?
            duplicateSurvey.setPositioningComment(survey.getPositioningComment());
        } else {
            duplicateSurvey.setCoordinate(null);
            duplicateSurvey.setPositioning(null);
            duplicateSurvey.setPositioningComment(null);
        }

        // same occasion ? or just when full duplication ?
        if (survey.getOccasion() != null) {
            duplicateSurvey.setOccasion(DaliBeans.clone(survey.getOccasion()));
            duplicateSurvey.getOccasion().setId(null);
        }

        if (fullDuplication) {

            // ensure sampling operations are loaded
            loadSamplingOperationsFromSurvey(survey, true);

            // sampling operations
            List<SamplingOperationDTO> duplicateSamplingOperations = Lists.newArrayList();
            if (!survey.isSamplingOperationsEmpty()) {
                for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {
                    SamplingOperationDTO duplicateSamplingOperation = DaliBeans.clone(samplingOperation);
                    duplicateSamplingOperation.setId(null);
                    duplicateSamplingOperation.setMeasurements(null);
                    duplicateSamplingOperation.setIndividualMeasurements(null);
                    duplicateSamplingOperation.setMeasurementsLoaded(false);
                    duplicateSamplingOperation.setIndividualMeasurementsLoaded(false);
                    duplicateSamplingOperation.setComment(null);
                    duplicateSamplingOperation.setErrors(null);

                    if (copyCoordinates) {
                        duplicateSamplingOperation.setCoordinate(samplingOperation.getCoordinate());
                        duplicateSamplingOperation.setPositioning(samplingOperation.getPositioning());
                    } else {
                        duplicateSamplingOperation.setCoordinate(null);
                        duplicateSamplingOperation.setPositioning(null);
                    }

                    duplicateSamplingOperations.add(duplicateSamplingOperation);
                }
            }

            duplicateSurvey.setSamplingOperations(duplicateSamplingOperations);
            duplicateSurvey.setSamplingOperationsLoaded(true);

        } else {
            // Remove sampling operations
            duplicateSurvey.setSamplingOperations(null);
            duplicateSurvey.setSamplingOperationsLoaded(false);
        }

        return duplicateSurvey;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getAvailablePrograms(Integer locationId, LocalDate date, boolean forceNoContext, boolean writableOnly) {
        List<ProgramDTO> result;
        if (dataContext.isContextFiltered(FilterTypeValues.PROGRAM) && !forceNoContext) {
            result = contextService.getFilteredPrograms(dataContext.getContextId());
            // context filtered programs can eventually out of user rights, filter them
            Set<String> programCodes = writableOnly
                ? programStrategyService.getWritableProgramCodesByQuserId(dataContext.getPrincipalUserId())
                : programStrategyService.getReadableProgramCodesByQuserId(dataContext.getPrincipalUserId());
            result = result.stream().filter(programDTO -> programCodes.contains(programDTO.getCode())).collect(Collectors.toList());
        } else {
            result = writableOnly ? programStrategyService.getWritablePrograms() : programStrategyService.getReadablePrograms();
        }

        // Filter result with provided location and date
        if (CollectionUtils.isNotEmpty(result) && locationId != null && date != null) {
            List<ProgramDTO> filteredPrograms = writableOnly
                ? programStrategyService.getWritableProgramsByLocationAndDate(locationId, Dates.convertToDate(date, config.getDbTimezone()))
                : programStrategyService.getReadableProgramsByLocationAndDate(locationId, Dates.convertToDate(date, config.getDbTimezone()));
            return (List<ProgramDTO>) CollectionUtils.intersection(result, filteredPrograms);
        }

        return result;
    }

    @Override
    public List<CampaignDTO> getAvailableCampaigns(LocalDate date, boolean forceNoContext) {
        List<CampaignDTO> result;
        if (dataContext.isContextFiltered(FilterTypeValues.CAMPAIGN) && !forceNoContext) {
            result = contextService.getFilteredCampaigns(dataContext.getContextId());
        } else {
            result = new ArrayList<>(campaignService.getAllCampaigns()); // put in a new List because the DAO returns an immutable list
        }

        if (CollectionUtils.isNotEmpty(result) && date != null) {
            List<CampaignDTO> filteredCampaigns = campaignService.findCampaignsIncludingDate(Dates.convertToDate(date, config.getDbTimezone()));
            return (List<CampaignDTO>) CollectionUtils.intersection(result, filteredCampaigns);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> getAvailableAnalysisInstruments(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.ANALYSIS_INSTRUMENT) && !forceNoContext) {
            return contextService.getFilteredAnalysisInstruments(dataContext.getContextId());
        } else {
            return referentialService.getAnalysisInstruments(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> getAvailableSamplingEquipments(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.SAMPLING_EQUIPMENT) && !forceNoContext) {
            return contextService.getFilteredSamplingEquipments(dataContext.getContextId());
        } else {
            return referentialService.getSamplingEquipments(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getAvailableLocations(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.LOCATION) && !forceNoContext) {
            return contextService.getFilteredLocations(dataContext.getContextId());
        } else {
            return referentialService.getLocations(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getAvailableLocations(Integer campaignId, String programCode, boolean forceNoContext, boolean activeOnly) {
        List<LocationDTO> locations = referentialService.getLocations(campaignId, programCode, activeOnly);
        if (CollectionUtils.isNotEmpty(locations)) {
            if (dataContext.isContextFiltered(FilterTypeValues.LOCATION) && !forceNoContext) {
                List<LocationDTO> filteredLocations = contextService.getFilteredLocations(dataContext.getContextId());
                if (CollectionUtils.isNotEmpty(filteredLocations)) {
                    return (List<LocationDTO>) CollectionUtils.intersection(locations, filteredLocations);
                }
            }
        }
        return locations;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> getAvailableTaxonGroups(TaxonDTO taxon, boolean forceNoContext) {
        List<TaxonGroupDTO> result;
        if (taxon == null) {
            return getAvailableTaxonGroups(forceNoContext);
        } else {
            result = taxon.getTaxonGroups();
            if (CollectionUtils.isNotEmpty(result)) {
                if (dataContext.isContextFiltered(FilterTypeValues.TAXON_GROUP) && !forceNoContext) {
                    List<TaxonGroupDTO> filteredTaxonGroups = contextService.getFilteredTaxonGroups(dataContext.getContextId());
                    if (CollectionUtils.isNotEmpty(filteredTaxonGroups)) {
                        result = (List<TaxonGroupDTO>) CollectionUtils.intersection(result, filteredTaxonGroups);
                    }
                }
            }
        }
        return referentialService.getFullTaxonGroups(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getAvailableTaxons(TaxonGroupDTO taxonGroup, boolean forceNoContext) {
        if (taxonGroup == null) {
            return getAvailableTaxons(forceNoContext);
        } else {
            // Mantis #0029620 don't use taxonGroup.getTaxons() because ehcache don't persist correctly the taxons and taxonGroups
            List<TaxonDTO> availableTaxons = referentialService.getTaxons(taxonGroup.getId());

            // Remove obsolete taxons
            availableTaxons = availableTaxons.stream().filter(taxon->!taxon.isObsolete()).collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(availableTaxons)) {
                if (dataContext.isContextFiltered(FilterTypeValues.TAXON) && !forceNoContext) {
                    List<TaxonDTO> filteredTaxons = contextService.getFilteredTaxons(dataContext.getContextId());
                    if (CollectionUtils.isNotEmpty(filteredTaxons)) {
                        availableTaxons = (List<TaxonDTO>) CollectionUtils.intersection(availableTaxons, filteredTaxons);
                    }
                }
            }
            referentialService.fillReferentTaxons(availableTaxons);
            return availableTaxons;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getAvailablePmfms(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.PMFM) && !forceNoContext) {
            return contextService.getFilteredPmfms(dataContext.getContextId());
        } else {
            return referentialService.getPmfms(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> getAvailableDepartments(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.DEPARTMENT) && !forceNoContext) {
            return contextService.getFilteredDepartments(dataContext.getContextId());
        } else {
            return referentialService.getDepartments(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonDTO> getAvailableUsers(boolean forceNoFilter) {
        if (dataContext.isContextFiltered(FilterTypeValues.USER) && !forceNoFilter) {
            return contextService.getFilteredUsers(dataContext.getContextId());
        } else {
            return userService.getActiveUsers();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateSurveys(Collection<? extends SurveyDTO> surveys, String validationComment, ProgressionCoreModel progressionModel) {

        if (CollectionUtils.isEmpty(surveys)) return;

        // compute validation date
        Date validationDate = new Date(System.currentTimeMillis());

        // collect surveys to validate
        List<SurveyDTO> surveysToValidate = surveys.stream()
                .filter(survey -> !survey.isDirty() && survey.getControlDate() != null && survey.getValidationDate() == null)
                .collect(Collectors.toList());

        // collect survey ids
        List<Integer> allSurveyIds = surveysToValidate.stream().map(SurveyDTO::getId).distinct().collect(Collectors.toList());
        int chunkSize = config.getMassiveProcessChunkSize();
        List<List<Integer>> chunkSurveyIds = Lists.partition(allSurveyIds, chunkSize);
        boolean enableMassiveUpdate = chunkSurveyIds.size() > 1;

        if (enableMassiveUpdate) {
            // enable massive update
            persistenceService.enableMassiveUpdate();
        }

        long start = System.currentTimeMillis();
        try {
            progressionModel.setTotal(chunkSurveyIds.size());
            for (int chunk = 0; chunk < chunkSurveyIds.size(); chunk++) {

                List<Integer> surveyIds = chunkSurveyIds.get(chunk);
                progressionModel.increments(t("dali.service.observation.validation.progression",
                        allSurveyIds.size(), chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size()));
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("Validating %s-%s of %s surveys", chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size(), allSurveyIds.size()));
                }
                loopBackService.validateSurveys(surveyIds, validationDate, validationComment);
            }

        } finally {

            if (enableMassiveUpdate) {
                // disable massive update (default)
                persistenceService.disableMassiveUpdate();
            }
        }

        // perform update on DTO
        surveysToValidate.forEach(survey -> {

            // update survey bean validation data
            survey.setValidationDate(validationDate);
            survey.setValidationComment(validationComment);

            // update synchronization status
            survey.setSynchronizationStatus(SynchronizationStatusValues.READY_TO_SYNCHRONIZE.toSynchronizationStatusDTO());

            // reset dirty flag
            survey.setDirty(false);

        });

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Validation total time = %s", Times.durationToString(System.currentTimeMillis() - start)));
        }
    }

    @Override
    public void validateSurveys(List<Integer> surveyIds, Date validationDate, String validationComment) {

        Assert.notEmpty(surveyIds);
        Assert.notNull(validationDate);

        // Trim to null, to avoid empty comment
        validationComment = StringUtils.trimToNull(validationComment);

        long start = System.currentTimeMillis();
        for (Integer surveyId : surveyIds) {

            // update survey validation date, set comment and update validation history (validator, date, comment, save old comment)
            surveyDao.validate(surveyId, validationComment, validationDate, dataContext.getRecorderPersonId(), true);

        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("validation of %d survey(s) in %s",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }

        start = System.currentTimeMillis();
        int nbSamplingOperations = samplingOperationDao.validateBySurveyIds(surveyIds, validationDate);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("validation of %d survey(s) in %s : %s sampling operations",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbSamplingOperations));
        }

        start = System.currentTimeMillis();
        int nbMeasurements = measurementDao.validateAllMeasurementsBySurveyIds(surveyIds, validationDate);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("validation of %d survey(s) in %s : %s (taxon)measurements",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbMeasurements));
        }

        start = System.currentTimeMillis();
        int nbPhotos = photoDao.validateBySurveyIds(surveyIds, validationDate);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("validation of %d survey(s) in %s : %s photos",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbPhotos));
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unvalidateSurveys(Collection<? extends SurveyDTO> surveys, String unvalidationComment, ProgressionCoreModel progressionModel) {

        if (CollectionUtils.isEmpty(surveys)) return;

        // compute unvalidation date
        Date unvalidationDate = new Date(System.currentTimeMillis());

        // collect surveys to unvalidate
        List<SurveyDTO> surveysToUnvalidate = surveys.stream()
                .filter(survey -> survey.getId() != null && survey.getValidationDate() != null)
                .collect(Collectors.toList());

        // collect survey ids
        List<Integer> allSurveyIds = surveysToUnvalidate.stream().map(SurveyDTO::getId).distinct().collect(Collectors.toList());
        int chunkSize = config.getMassiveProcessChunkSize();
        List<List<Integer>> chunkSurveyIds = Lists.partition(allSurveyIds, chunkSize);
        boolean enableMassiveUpdate = chunkSurveyIds.size() > 1;

        if (enableMassiveUpdate) {
            // enable massive update
            persistenceService.enableMassiveUpdate();
        }

        long start = System.currentTimeMillis();
        try {
            progressionModel.setTotal(chunkSurveyIds.size());
            for (int chunk = 0; chunk < chunkSurveyIds.size(); chunk++) {

                List<Integer> surveyIds = chunkSurveyIds.get(chunk);
                progressionModel.increments(t("dali.service.observation.unvalidation.progression",
                        allSurveyIds.size(), chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size()));
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("Unvalidating %s-%s of %s surveys", chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size(), allSurveyIds.size()));
                }
                loopBackService.unvalidateSurveys(surveyIds, unvalidationDate, unvalidationComment);
            }

        } finally {

            if (enableMassiveUpdate) {
                // disable massive update (default)
                persistenceService.disableMassiveUpdate();
            }
        }

        // perform update on DTO
        surveysToUnvalidate.forEach(survey -> {

            // update survey bean validation data
            survey.setValidationDate(null);
            survey.setValidationComment(null);
            survey.setQualificationDate(null);
            survey.setQualificationComment(null);
            survey.setQualityLevel(referentialService.getNotQualityLevel());

            // update synchronization status
            survey.setSynchronizationStatus(SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO());

            // reset dirty flag
            survey.setDirty(false);

        });

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Unvalidation total time = %s", Times.durationToString(System.currentTimeMillis() - start)));
        }
    }

    /**
     * New method for unvalidate surveys
     *
     * @param surveyIds           survey ids to unvalidate
     * @param unvalidationComment the comment for unvalidation
     * @param unvalidationDate    the date of unvalidation
     */
    @Override
    public void unvalidateSurveys(List<Integer> surveyIds, Date unvalidationDate, String unvalidationComment) {

        Assert.notEmpty(surveyIds);
        Assert.notNull(unvalidationDate);
        Assert.notBlank(unvalidationComment);
        int validatorId = dataContext.getRecorderPersonId();

        // Trim to null, to avoid empty comment
        unvalidationComment = StringUtils.trimToNull(unvalidationComment);

        long start = System.currentTimeMillis();
        for (Integer surveyId : surveyIds) {

            // update survey validation date, set comment and update validation history (validator, date, comment, save old comment)
            surveyDao.unvalidate(surveyId, unvalidationComment, unvalidationDate, validatorId);

        }
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("unvalidation of %d survey(s) in %s",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }

        // unvalidateMeasurementsBySurveyId
        start = System.currentTimeMillis();
        int nbMeasurements = measurementDao.unvalidateAllMeasurementsBySurveyIds(surveyIds, unvalidationDate, validatorId);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("unvalidation of %d survey(s) in %s : %s survey's (taxon)measurements",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbMeasurements));
        }

        // unvalidatePhotosBySurveyId
        start = System.currentTimeMillis();
        int nbPhotos = photoDao.unvalidateBySurveyIds(surveyIds, unvalidationDate, validatorId);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("unvalidation of %d survey(s) in %s : %s survey's photos",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbPhotos));
        }

        // unvalidateSamplingOperationsBySurveyId
        start = System.currentTimeMillis();
        int nbSamplingOperations = samplingOperationDao.unvalidateBySurveyIds(surveyIds, unvalidationDate, validatorId);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("unvalidation of %d survey(s) in %s : %s sampling operations",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbSamplingOperations));
        }

        // Unvalidate all measurements and photos
        List<Integer> samplingOperationIds = surveyIds.stream().flatMap(surveyId -> samplingOperationDao.getSamplingOperationIdsBySurveyId(surveyId).stream()).collect(Collectors.toList());

        if (samplingOperationIds.isEmpty()) return;

        start = System.currentTimeMillis();
        nbMeasurements = measurementDao.unvalidateAllMeasurementsBySamplingOperationIds(samplingOperationIds, unvalidationDate, validatorId);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("unvalidation of %d survey(s) in %s : %s sampling operation's (taxon)measurements",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbMeasurements));
        }

        start = System.currentTimeMillis();
        nbPhotos = photoDao.unvalidateBySamplingOperationIds(samplingOperationIds, unvalidationDate, validatorId);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("unvalidation of %d survey(s) in %s : %s sampling operation's photos",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbPhotos));
        }

    }

    @Override
    public boolean isQualified(SurveyDTO survey) {
        Assert.notNull(survey);
        Assert.notNull(survey.getId());

        return survey.getQualificationDate() != null
               // Go deeper to find qualified data
               || surveyDao.isQualified(survey.getId());
    }

    @Override
    public void qualifySurveys(Collection<? extends SurveyDTO> surveys, String qualificationComment, QualityLevelDTO qualityLevel, ProgressionCoreModel progressionModel) {

        if (CollectionUtils.isEmpty(surveys)) return;
        Assert.notNull(qualityLevel);

        // compute qualification date
        Date qualificationDate = new Date(System.currentTimeMillis());

        // collect surveys to qualify
        List<SurveyDTO> surveysToQualify = surveys.stream()
                .filter(survey -> !survey.isDirty() && survey.getValidationDate() != null)
                .collect(Collectors.toList());

        // collect survey ids
        List<Integer> allSurveyIds = surveysToQualify.stream().map(SurveyDTO::getId).distinct().collect(Collectors.toList());
        int chunkSize = config.getMassiveProcessChunkSize();
        List<List<Integer>> chunkSurveyIds = Lists.partition(allSurveyIds, chunkSize);
        boolean enableMassiveUpdate = chunkSurveyIds.size() > 1;

        if (enableMassiveUpdate) {
            // enable massive update
            persistenceService.enableMassiveUpdate();
        }

        long start = System.currentTimeMillis();
        try {
            progressionModel.setTotal(chunkSurveyIds.size());
            for (int chunk = 0; chunk < chunkSurveyIds.size(); chunk++) {

                List<Integer> surveyIds = chunkSurveyIds.get(chunk);
                progressionModel.increments(t("dali.service.observation.qualification.progression",
                        allSurveyIds.size(), chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size()));
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("Qualifying %s-%s of %s surveys", chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size(), allSurveyIds.size()));
                }
                loopBackService.qualifySurveys(surveyIds, qualificationDate, qualificationComment, qualityLevel.getCode());
            }

        } finally {

            if (enableMassiveUpdate) {
                // disable massive update (default)
                persistenceService.disableMassiveUpdate();
            }
        }

        // perform update on DTO
        surveysToQualify.forEach(survey -> {

            // update survey bean qualification date
            survey.setQualificationDate(qualificationDate);
            survey.setQualificationComment(qualificationComment);
            survey.setQualityLevel(qualityLevel);

            // reset dirty flag
            survey.setDirty(false);

        });

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Qualification total time = %s", Times.durationToString(System.currentTimeMillis() - start)));
        }

    }

    @Override
    public void qualifySurveys(List<Integer> surveyIds, Date qualificationDate, String qualificationComment, String qualityLevelCode) {

        Assert.notEmpty(surveyIds);
        Assert.notNull(qualificationDate);
        Assert.notBlank(qualityLevelCode);
        // For GOOD quality flag, comment is optional
        if (!QualityFlagCode.GOOD.getValue().equals(qualityLevelCode)) {
            Assert.notBlank(qualificationComment);
        }

        // Trim to null, to avoid empty comment
        qualificationComment = StringUtils.trimToNull(qualificationComment);

        long start = System.currentTimeMillis();
        for (Integer surveyId : surveyIds) {

            // update survey qualification date, set comment and update qualification history (validator, date, comment, save old comment)
            surveyDao.qualify(surveyId, qualificationComment, qualificationDate, dataContext.getRecorderPersonId(), qualityLevelCode);

        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("qualification of %d survey(s) in %s",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }

        start = System.currentTimeMillis();
        int nbSamplingOperations = samplingOperationDao.qualifyBySurveyIds(surveyIds, qualificationDate, qualityLevelCode);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("qualification of %d survey(s) in %s : %s sampling operations",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbSamplingOperations));
        }

        start = System.currentTimeMillis();
        int nbMeasurements = measurementDao.qualifyAllMeasurementsBySurveyIds(surveyIds, qualificationDate);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("qualification of %d survey(s) in %s : %s (taxon)measurements",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbMeasurements));
        }

        start = System.currentTimeMillis();
        int nbPhotos = photoDao.qualifyBySurveyIds(surveyIds, qualificationDate);
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("qualification of %d survey(s) in %s : %s photos",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbPhotos));
        }
    }

    @Override
    public List<ValidationHistoryDTO> getValidationHistory(int surveyId) {
        return surveyDao.getValidationHistory(surveyId);
    }

    @Override
    public List<QualificationHistoryDTO> getQualificationHistory(int surveyId) {
        return surveyDao.getQualificationHistory(surveyId);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public long countSurveysWithProgramAndLocations(String programCode, List<Integer> locationIds) {
        Assert.notBlank(programCode);

        return surveyDao.countSurveysWithProgramAndLocations(programCode, locationIds);
    }

    @Override
    public long countSurveysWithProgramLocationAndOutsideDates(String programCode, int appliedStrategyId, int locationId, LocalDate startDate, LocalDate endDate) {
        Assert.notBlank(programCode);
        Assert.notNull(startDate);
        Assert.notNull(endDate);

        return surveyDao.countSurveysWithProgramLocationAndOutsideDates(
                programCode,
                appliedStrategyId,
                locationId,
                Dates.convertToDate(startDate, config.getDbTimezone()),
                Dates.convertToDate(endDate, config.getDbTimezone()));
    }

    @Override
    public long countSurveysWithProgramLocationAndInsideDates(String programCode, int appliedStrategyId, int locationId, LocalDate startDate, LocalDate endDate) {
        Assert.notBlank(programCode);
        Assert.notNull(startDate);
        Assert.notNull(endDate);

        return surveyDao.countSurveysWithProgramLocationAndInsideDates(
                programCode,
                appliedStrategyId,
                locationId,
                Dates.convertToDate(startDate, config.getDbTimezone()),
                Dates.convertToDate(endDate, config.getDbTimezone()));
    }

    @Override
    public long countSurveysWithProgramLocationAndOutsideDates(ProgStratDTO progStrat, int locationId) {
        Assert.notNull(progStrat);

        return surveyDao.countSurveysWithProgramLocationAndOutsideDates(
                progStrat.getProgram().getCode(),
                progStrat.getAppliedStrategyId(),
                locationId,
                Dates.convertToDate(progStrat.getStartDate(), config.getDbTimezone()),
                Dates.convertToDate(progStrat.getEndDate(), config.getDbTimezone()));
    }

    @Override
    public long countSurveysWithCampaign(int campaignId) {

        return campaignService.countSurveysWithCampaign(campaignId);
    }

    @Override
    public List<QualityLevelDTO> getQualityLevels() {

        // Get all quality flags except 'not qualified'
        List<QualityLevelDTO> result = new ArrayList<>(referentialService.getQualityLevels(StatusFilter.ACTIVE));
        result.remove(referentialService.getNotQualityLevel());
        return result;
    }

    @Override
    public Set<MoratoriumPmfmDTO> getPmfmsUnderMoratorium(SurveyDTO survey) {
        if (survey == null || survey.getProgram() == null || survey.getDate() == null || survey.getLocation() == null)
            return null;
        ProgramDTO program = survey.getProgram();
        Set<MoratoriumPmfmDTO> result = new HashSet<>();

        if (!program.isMoratoriumsEmpty()
            && !dataContext.isSurveyViewable(survey)
        ) {
            // Populate partial moratoriums
            program.getMoratoriums().stream()
                .filter(moratorium -> !moratorium.isGlobal())
                .filter(moratorium -> moratorium.getPeriods().stream()
                    .anyMatch(period -> !survey.getDate().isAfter(period.getEndDate()) && !survey.getDate().isBefore(period.getStartDate()))
                )
                .filter(moratorium -> {
                        if (CollectionUtils.isEmpty(moratorium.getLocationIds())
                            && CollectionUtils.isEmpty(moratorium.getCampaignIds())
                            && CollectionUtils.isEmpty(moratorium.getOccasionIds())) {
                            return true;
                        }
                        boolean ok = false;
                        if (CollectionUtils.isNotEmpty(moratorium.getLocationIds())) {
                            ok = moratorium.getLocationIds().contains(survey.getLocation().getId());
                        }
                        if (CollectionUtils.isNotEmpty(moratorium.getCampaignIds()) && survey.getCampaign() != null) {
                            ok |= moratorium.getCampaignIds().contains(survey.getCampaign().getId());
                        }
                        if (CollectionUtils.isNotEmpty(moratorium.getOccasionIds()) && survey.getOccasion() != null) {
                            ok |= moratorium.getOccasionIds().contains(survey.getOccasion().getId());
                        }
                        return ok;
                    }
                )
                .forEach(moratorium -> result.addAll(moratorium.getPmfms()));
        }

        return result;
    }

    /*
        PRIVATE METHODS
     */

    private SurveyDTO getSurvey(Integer surveyId, boolean withoutFilter) {
        Assert.notNull(surveyId);

        SurveyDTO survey = surveyDao.getSurveyById(surveyId, withoutFilter);

        // Load Pmfm strategies
        survey.setPmfmStrategies(programStrategyService.getPmfmStrategiesBySurvey(survey));

        {
            List<PmfmDTO> pmfms = Lists.newArrayList();
            List<PmfmDTO> individualPmfms = Lists.newArrayList();

            // Collect Pmfms for survey
            for (PmfmStrategyDTO p : survey.getPmfmStrategies()) {
                if (p.isSurvey()) {
                    if (p.isGrouping()) {
                        individualPmfms.add(p.getPmfm());
                    } else {
                        pmfms.add(p.getPmfm());
                    }
                }
            }

            // Add Pmfm strategies on survey
            survey.setPmfms(pmfms);
            // if needed, filter out some predefined pmfm
//            final List<Integer> pmfmIdsToIgnore = ImmutableList.of(config.getDepthValuesPmfmId());
//            survey.setPmfms(DaliBeans.filterPmfm(pmfms, pmfmIdsToIgnore));
            survey.setIndividualPmfms(individualPmfms);
        }

        // Collect Pmfms from existing measurements
        DaliBeans.populatePmfmsFromMeasurements(survey);

        if (!survey.isSamplingOperationsEmpty()) {

            // Collect Pmfms for sampling operations
            List<PmfmDTO> pmfms = Lists.newArrayList();
            List<PmfmDTO> individualPmfms = Lists.newArrayList();
            for (PmfmStrategyDTO p : survey.getPmfmStrategies()) {
                if (p.isSampling()) {
                    if (p.isGrouping()) {
                        individualPmfms.add(p.getPmfm());
                    } else {
                        pmfms.add(p.getPmfm());
                    }
                }
            }

            for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {

                // Add Pmfm strategies on sampling operations
                samplingOperation.setPmfms(pmfms);
                samplingOperation.setIndividualPmfms(individualPmfms);

                // Collect Pmfms from existing measurements
                DaliBeans.populatePmfmsFromMeasurements(samplingOperation);

            }

        }

        return survey;

    }

    private List<TaxonGroupDTO> getAvailableTaxonGroups(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.TAXON_GROUP) && !forceNoContext) {
            return contextService.getFilteredTaxonGroups(dataContext.getContextId());
        } else {
            return referentialService.getTaxonGroups();
        }
    }

    private List<TaxonDTO> getAvailableTaxons(boolean forceNoContext) {
        List<TaxonDTO> availableTaxons;
        if (dataContext.isContextFiltered(FilterTypeValues.TAXON) && !forceNoContext) {
            availableTaxons = contextService.getFilteredTaxons(dataContext.getContextId());
        } else {
            availableTaxons = referentialService.getTaxons(null);
        }

        // Remove obsolete taxons
        availableTaxons = availableTaxons.stream().filter(taxon->!taxon.isObsolete()).collect(Collectors.toList());

        referentialService.fillReferentTaxons(availableTaxons);
        return availableTaxons;
    }

    /**
     * Apply default analysis department on all measurements from a survey, from strategy
     *
     * @param survey the survey
     */
    private void applyDefaultAnalysisDepartment(SurveyDTO survey) {
        Assert.notNull(survey);

        // Get analysis departments from apply strategies
        DepartmentDTO analysisDepartment = programStrategyService.getSamplingDepartmentOfAppliedStrategyBySurvey(survey);

        // Nothing to apply: skip
        if (analysisDepartment == null) {
            return;
        }

        // If measurement loaded
        if (survey.isMeasurementsLoaded()) {
            // For each measurement, apply the default analysis department (if not set)
            for (MeasurementDTO measurement : survey.getMeasurements()) {
                if (measurement.getAnalyst() == null) {
                    measurement.setAnalyst(analysisDepartment);
                }
            }

            // For each individual measurement, apply the default analysis department (if not set)
            for (MeasurementDTO measurement : survey.getIndividualMeasurements()) {
                if (measurement.getAnalyst() == null) {
                    measurement.setAnalyst(analysisDepartment);
                }
            }
        }

        // if sampling operations are loaded
        if (survey.isSamplingOperationsLoaded()
                && CollectionUtils.isNotEmpty(survey.getSamplingOperations())) {

            // Apply on each sampling operation measurements
            for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {
                if (samplingOperation.isMeasurementsLoaded()) {
                    // For each measurement, apply the default analysis department (if not set)
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        if (measurement.getAnalyst() == null) {
                            measurement.setAnalyst(analysisDepartment);
                        }
                    }
                }

                if (samplingOperation.isIndividualMeasurementsLoaded()) {
                    // For each measurement, apply the default analysis department (if not set)
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        if (measurement.getAnalyst() == null) {
                            measurement.setAnalyst(analysisDepartment);
                        }
                    }
                }
            }
        }
    }

}
