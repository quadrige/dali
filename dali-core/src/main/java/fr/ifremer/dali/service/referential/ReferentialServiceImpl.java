package fr.ifremer.dali.service.referential;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.referential.DaliAnalysisInstrumentDao;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.DaliSamplingEquipmentDao;
import fr.ifremer.dali.dao.referential.DaliUnitDao;
import fr.ifremer.dali.dao.referential.monitoringLocation.DaliMonitoringLocationDao;
import fr.ifremer.dali.dao.referential.pmfm.*;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonGroupDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonNameDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.ParameterTypeDTO;
import fr.ifremer.dali.dto.configuration.filter.department.DepartmentCriteriaDTO;
import fr.ifremer.dali.dto.configuration.filter.location.LocationCriteriaDTO;
import fr.ifremer.dali.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.dali.dto.configuration.filter.taxon.TaxonCriteriaDTO;
import fr.ifremer.dali.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO;
import fr.ifremer.dali.dto.enums.ParameterTypeValues;
import fr.ifremer.dali.dto.referential.*;
import fr.ifremer.dali.dto.referential.pmfm.*;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.dao.referential.QualityFlagCode;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implémentation des services pour les données de reference
 */
@Service("daliReferentialService")
public class ReferentialServiceImpl implements ReferentialService {

    @Autowired
    protected CacheService cacheService;

    @Resource
    protected DaliConfiguration config;

    @Resource(name = "daliReferentialDao")
    private DaliReferentialDao referentialDao;

    @Resource(name = "daliDepartmentDao")
    protected DaliDepartmentDao departmentDao;

    @Resource(name = "daliMonitoringLocationDao")
    private DaliMonitoringLocationDao monitoringLocationDao;

    @Resource(name = "daliTaxonGroupDao")
    private DaliTaxonGroupDao taxonGroupDao;

    @Resource(name = "daliTaxonNameDao")
    private DaliTaxonNameDao taxonNameDao;

    @Resource(name = "daliPmfmDao")
    protected DaliPmfmDao pmfmDao;

    @Resource(name = "daliQualitativeValueDao")
    private DaliQualitativeValueDao qualitativeValueDao;

    @Resource(name = "daliParameterDao")
    private DaliParameterDao parameterDao;

    @Resource(name = "daliMatrixDao")
    private DaliMatrixDao matrixDao;

    @Resource(name = "daliFractionDao")
    private DaliFractionDao fractionDao;

    @Resource(name = "daliMethodDao")
    private DaliMethodDao methodDao;

    @Resource(name = "daliAnalysisInstrumentDao")
    private DaliAnalysisInstrumentDao analysisInstrumentDao;

    @Resource(name = "daliSamplingEquipmentDao")
    private DaliSamplingEquipmentDao samplingEquipmentDao;

    @Resource(name = "daliUnitDao")
    private DaliUnitDao unitDao;

    @Resource(name = "daliUserService")
    private UserService userService;

    private List<ParameterTypeDTO> parameterTypeList;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> getSamplingEquipments(StatusFilter statusFilter) {
        return new ArrayList<>(samplingEquipmentDao.getAllSamplingEquipments(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UnitDTO> getUnits(StatusFilter statusFilter) {
        return new ArrayList<>(unitDao.getAllUnits(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> getDepartments(StatusFilter statusFilter) {
        return new ArrayList<>(departmentDao.getAllDepartments(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DepartmentDTO getDepartmentById(int departmentId) {
        return departmentDao.getDepartmentById(departmentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> getAnalysisInstruments(StatusFilter statusFilter) {
        return new ArrayList<>(analysisInstrumentDao.getAllAnalysisInstruments(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StatusDTO> getStatus(StatusFilter statusFilter) {
        return referentialDao.getStatusByCodes(statusFilter.toStatusCodes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getLocations(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(monitoringLocationDao.getAllLocations(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocationDTO getLocation(int locationId) {
        return monitoringLocationDao.getLocationById(locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getLocations(Integer campaignId, String programCode, boolean activeOnly) {
        StatusFilter statusFilter = activeOnly ? StatusFilter.ACTIVE : StatusFilter.ALL;
        if (campaignId == null && programCode == null) {
            return getLocations(statusFilter);
        }
        return new ArrayList<>(monitoringLocationDao.getLocationsByCampaignAndProgram(campaignId, programCode, statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<HarbourDTO> getHarbours(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return monitoringLocationDao.getAllHarbours(statusFilter.toStatusCodes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> getTaxonGroups() {
        return new ArrayList<>(taxonGroupDao.getAllTaxonGroups());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaxonGroupDTO getTaxonGroup(int taxonGroupId) {
        return taxonGroupDao.getTaxonGroupById(taxonGroupId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> getFullTaxonGroups(List<TaxonGroupDTO> taxonGroups) {
        List<TaxonGroupDTO> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(taxonGroups)) {
            for (TaxonGroupDTO taxonGroup : taxonGroups) {
                if (taxonGroup.getId() == null) {
                    // if happens, add it directly
                    result.add(taxonGroup);
                } else {

                    // reload this taxon group from cache (or get it from db)
                    result.add(taxonGroupDao.getTaxonGroupById(taxonGroup.getId()));
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getTaxons(Integer taxonGroupId) {
        if (taxonGroupId == null) {
            return new ArrayList<>(taxonNameDao.getAllTaxonNames());
        } else {
            return new ArrayList<>(taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now()).get(taxonGroupId));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaxonDTO getTaxon(int taxonId) {
        return taxonNameDao.getTaxonNameById(taxonId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillTaxonsProperties(List<TaxonDTO> taxons) {
        taxonNameDao.fillTaxonsProperties(taxons);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillReferentTaxons(List<TaxonDTO> taxons) {
        taxonNameDao.fillReferents(taxons);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonomicLevelDTO> getTaxonomicLevels() {
        return new ArrayList<>(referentialDao.getAllTaxonomicLevels());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CitationDTO> getCitations() {
        return new ArrayList<>(referentialDao.getAllCitations());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PositioningSystemDTO> getPositioningSystems() {
        return new ArrayList<>(referentialDao.getAllPositioningSystems());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterGroupDTO> getParameterGroup(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(parameterDao.getAllParameterGroups(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterDTO> getParameters(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(parameterDao.getAllParameters(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterDTO> searchParameters(StatusFilter statusFilter, String parameterCode, String statusCode, ParameterGroupDTO parameterGroup) {
        Assert.notNull(statusFilter);
        Integer parameterGroupId = parameterGroup == null ? null : parameterGroup.getId();
        return parameterDao.findParameters(parameterCode, parameterGroupId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MatrixDTO> getMatrices(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(matrixDao.getAllMatrices(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MatrixDTO> searchMatrices(StatusFilter statusFilter, Integer matrixId, String statusCode) {
        Assert.notNull(statusFilter);
        return matrixDao.findMatrices(matrixId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FractionDTO> getFractions(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(fractionDao.getAllFractions(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FractionDTO> searchFractions(StatusFilter statusFilter, Integer fractionId, String statusCode) {
        Assert.notNull(statusFilter);
        return fractionDao.findFractions(fractionId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MethodDTO> getMethods(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(methodDao.getAllMethods(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MethodDTO> searchMethods(StatusFilter statusFilter, Integer methodId, String statusCode) {
        Assert.notNull(statusFilter);
        return methodDao.findMethods(methodId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfms(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(pmfmDao.getAllPmfms(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PmfmDTO getPmfm(int pmfmId) {
        return pmfmDao.getPmfmById(pmfmId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> searchPmfms(StatusFilter statusFilter, String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, String statusCode) {
        return pmfmDao.findPmfms(parameterCode, matrixId, fractionId, methodId, unitId, pmfmName, statusFilter.intersect(statusCode));
    }

    @Override
    public Integer getUniquePmfmIdFromPmfm(PmfmDTO pmfm) {
        List<PmfmDTO> pmfms = pmfmDao.findPmfms(pmfm.getParameter().getCode(),
                pmfm.getMatrix() != null ? pmfm.getMatrix().getId() : null,
                pmfm.getFraction() != null ? pmfm.getFraction().getId() : null,
                pmfm.getMethod() != null ? pmfm.getMethod().getId() : null,
                pmfm.getUnit() != null ? pmfm.getUnit().getId() : null,
                null, StatusFilter.ALL.toStatusCodes());
        if (CollectionUtils.size(pmfms) != 1) {
            throw new DaliTechnicalException("Should found only 1 PMFM");
        }
        return pmfms.get(0).getId();
    }

    @Override
    public PmfmDTO getUniquePmfmFromPmfm(PmfmDTO pmfm) {
        return getPmfm(getUniquePmfmIdFromPmfm(pmfm));
    }

    @Override
    public QualitativeValueDTO getQualitativeValue(int qualitativeValueId) {
        return qualitativeValueDao.getQualitativeValueById(qualitativeValueId);
    }

    @Override
    public List<QualitativeValueDTO> getQualitativeValues(Collection<Integer> qualitativeValueIds) {
        if (CollectionUtils.isEmpty(qualitativeValueIds)) return new ArrayList<>();
        return qualitativeValueIds.stream().map(qvId -> qualitativeValueDao.getQualitativeValueById(qvId)).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LevelDTO> getLevels() {
        return new ArrayList<>(referentialDao.getAllDepthLevels());
    }

    @Override
    public QualityLevelDTO getNotQualityLevel() {
        return referentialDao.getQualityFlagByCode(QualityFlagCode.NOT_QUALIFIED.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<QualityLevelDTO> getQualityLevels(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(referentialDao.getAllQualityFlags(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, Integer analysisInstrumentId, String statusCode) {
        Assert.notNull(statusFilter);
        return analysisInstrumentDao.findAnalysisInstruments(statusFilter.intersect(statusCode), analysisInstrumentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, Integer samplingEquipmentId, String statusCode, Integer unitId) {
        Assert.notNull(statusFilter);
        return samplingEquipmentDao.findSamplingEquipments(statusFilter.intersect(statusCode), samplingEquipmentId, unitId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, String equipmentName, String statusCode) {
        return samplingEquipmentDao.findSamplingEquipmentsByName(statusFilter.intersect(statusCode), equipmentName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UnitDTO> searchUnits(StatusFilter statusFilter, Integer unitId, String statusCode) {
        Assert.notNull(statusFilter);
        return unitDao.findUnits(unitId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonDTO> getUsersInSameDepartment(PersonDTO person) {
        PersonCriteriaDTO searchCriteria = DaliBeanFactory.newPersonCriteriaDTO();
        if (person != null && person.getDepartment() != null) {
            searchCriteria.setDepartment(person.getDepartment());
        }
        return userService.searchUser(searchCriteria);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GroupingTypeDTO> getGroupingTypes() {
        return new ArrayList<>(referentialDao.getAllGroupingTypes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> searchLocations(LocationCriteriaDTO searchCriteria) {
        List<String> statusCodes = StatusFilter.ALL.intersect(searchCriteria.getStatus());
        String orderItemTypeCode = searchCriteria.getGroupingType() == null ? null : searchCriteria.getGroupingType().getCode();
        Integer orderItemId = searchCriteria.getGrouping() == null ? null : searchCriteria.getGrouping().getId();
        String programCode = searchCriteria.getProgram() == null ? null : searchCriteria.getProgram().getCode();

        return monitoringLocationDao.findLocations(statusCodes,
                orderItemTypeCode,
                orderItemId,
                programCode,
                searchCriteria.getLabel(),
                searchCriteria.getName(),
                searchCriteria.isStrictName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> searchDepartments(DepartmentCriteriaDTO searchCriteria) {
        List<String> statusCodes = StatusFilter.ALL.intersect(searchCriteria.getStatus());
        Integer parentId = searchCriteria.getParentDepartment() == null ?
                null : searchCriteria.getParentDepartment().getId();
        return departmentDao.findDepartmentsByCodeAndName(
                searchCriteria.getCode(),
                searchCriteria.getName(),
                searchCriteria.isStrictName(),
                parentId,
                statusCodes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, String instrumentName) {
        return analysisInstrumentDao.findAnalysisInstrumentsByName(statusFilter.toStatusCodes(), instrumentName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> searchTaxons(TaxonCriteriaDTO taxonCriteria) {
        String levelCode = taxonCriteria.getLevel() == null ? null : taxonCriteria.getLevel().getCode();
        String name = StringUtils.isBlank(taxonCriteria.getName()) ? null : taxonCriteria.getName();
        if (taxonCriteria.isFullProperties()) {
            return taxonNameDao.findFullTaxonNamesByCriteria(levelCode, name, taxonCriteria.isStrictName());
        } else {
            return taxonNameDao.findTaxonNamesByCriteria(levelCode, name, taxonCriteria.isStrictName());
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> searchTaxonGroups(TaxonGroupCriteriaDTO searchCriteria) {
        List<String> statusCodes = StatusFilter.ALL.intersect(searchCriteria.getStatus());
        Integer parentTaxonGroupId = searchCriteria.getParentTaxonGroup() == null ? null : searchCriteria.getParentTaxonGroup().getId();
        return taxonGroupDao.findTaxonGroups(parentTaxonGroupId,
                searchCriteria.getLabel(),
                searchCriteria.getName(),
                searchCriteria.isStrictName(),
                statusCodes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PhotoTypeDTO> getPhotoTypes() {
        return new ArrayList<>(referentialDao.getAllPhotoTypes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterTypeDTO> getParameterTypes() {
        // If list not exist, create it
        if (parameterTypeList == null) {
            parameterTypeList = Lists.newArrayList();

            // Add all values
            for (ParameterTypeValues parameterTypeValue : ParameterTypeValues.values()) {

                // DTO
                ParameterTypeDTO parameterType = DaliBeanFactory.newParameterTypeDTO();

                // Add ID
                parameterType.setId(parameterTypeValue.ordinal());

                // Add parameterType label
                parameterType.setKeyLabel(parameterTypeValue.getLabel());

                // Add to list
                parameterTypeList.add(parameterType);
            }
        }
        return parameterTypeList;
    }

}
