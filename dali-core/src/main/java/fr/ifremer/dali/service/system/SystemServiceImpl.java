package fr.ifremer.dali.service.system;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dto.FunctionDTO;
import fr.ifremer.dali.dto.SearchDateDTO;
import fr.ifremer.dali.dto.StateDTO;
import fr.ifremer.dali.dto.SynchronizationStatusDTO;
import fr.ifremer.dali.dto.configuration.control.ControlElementDTO;
import fr.ifremer.dali.dto.configuration.control.ControlFeatureDTO;
import fr.ifremer.dali.dto.enums.*;
import fr.ifremer.quadrige3.core.dao.system.synchronization.SynchronizationStatus;
import fr.ifremer.quadrige3.ui.core.dto.MonthDTO;
import fr.ifremer.quadrige3.ui.core.dto.month.MonthValues;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SystemService implementation.
 */
@Service("daliSystemService")
public class SystemServiceImpl implements SystemService {

    private List<SynchronizationStatusDTO> synchronizationStatusList = null;
    private List<FunctionDTO> functionControlList = null;
    private List<ControlElementDTO> elementControlList = null;
    private List<ControlFeatureDTO> featureControlMeasurementList = null;
    private List<ControlFeatureDTO> featureControlTaxonMeasurementList = null;
    private List<ControlFeatureDTO> featureControlObservationList = null;
    private List<ControlFeatureDTO> featureControlSamplingOperationList = null;
    private List<SearchDateDTO> searchDateList = null;
    private List<StateDTO> stateList = null;
    private List<MonthDTO> monthList = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SearchDateDTO> getSearchDates() {

        // If list not exist, create it
        if (searchDateList == null) {
            searchDateList = Arrays.stream(SearchDateValues.values()).map(SearchDateValues::toSearchDateDTO).collect(Collectors.toList());
        }
        return searchDateList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StateDTO> getStates() {

        // If list not exist, create it
        if (stateList == null) {
            stateList = Arrays.stream(StateValues.values()).map(StateValues::toStateDTO).collect(Collectors.toList());
        }
        return stateList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SynchronizationStatusDTO getLocalShare() {
        return SynchronizationStatusValues.toSynchronizationStatusDTO(SynchronizationStatus.DIRTY.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SynchronizationStatusDTO> getAllSynchronizationStatus(boolean withReadyToSyncStatus) {

        // If list not exist, create it
        if (synchronizationStatusList == null) {
            synchronizationStatusList = Arrays.stream(SynchronizationStatusValues.values())
                    .filter(value -> withReadyToSyncStatus || value != SynchronizationStatusValues.READY_TO_SYNCHRONIZE)
                    .map(SynchronizationStatusValues::toSynchronizationStatusDTO)
                    .collect(Collectors.toList());
        }
        return synchronizationStatusList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FunctionDTO> getFunctionsControlSystem() {

        // If list not exist, create it
        if (functionControlList == null) {

            functionControlList = Arrays.stream(ControlFunctionValues.values())
                    .map(ControlFunctionValues::toFunctionDTO)
                    .collect(Collectors.toList());
        }
        return functionControlList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ControlElementDTO> getControlElements() {

        // If list not exist, create it
        if (elementControlList == null) {
            elementControlList = Arrays.stream(ControlElementValues.values())

                    // TODO Les mesures sur taxons ne sont pas encore prises en charge
                    .filter(value -> value != ControlElementValues.TAXON_MEASUREMENT)

                    .map(ControlElementValues::toControlElementDTO)
                    .collect(Collectors.toList());
        }
        return elementControlList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ControlFeatureDTO> getControlFeatures(final ControlElementDTO controlElementDTO) {

        // Element control
        final ControlElementValues elementControl = ControlElementValues.getByCode(controlElementDTO.getCode());
        List<ControlFeatureDTO> features = new ArrayList<>();

        if (elementControl == null) {
            return features;
        }

        switch (elementControl) {
            case MEASUREMENT:
                return getFeatureControlMeasurementList();
            // TODO Les mesures sur taxons ne sont pas encore prises en charge
//			case TAXON_MEASUREMENT:
//				return getFeatureControlTaxonMeasurementList();
            case SURVEY:
                return getFeatureControlObservationList();
            case SAMPLING_OPERATION:
                return getFeatureControlSamplingOperationList();
            default:
                return features;
        }

    }

    @Override
    public List<MonthDTO> getMonths() {
        // If list not exist, create it
        if (monthList == null) {
            monthList = Arrays.stream(MonthValues.values()).map(MonthValues::toDTO).collect(Collectors.toList());
        }
        return monthList;
    }

    @Override
    public void clearCaches() {

        synchronizationStatusList = null;
        functionControlList = null;
        elementControlList = null;
        featureControlMeasurementList = null;
        featureControlTaxonMeasurementList = null;
        featureControlObservationList = null;
        featureControlSamplingOperationList = null;
        searchDateList = null;
        stateList = null;
    }

    /**
     * Only one list of counting.
     *
     * @return ControlFeatureDTO measures list
     */
    private List<ControlFeatureDTO> getFeatureControlMeasurementList() {

        // If list not exist, create it
        if (featureControlMeasurementList == null) {
            featureControlMeasurementList = Arrays.stream(ControlFeatureMeasurementValues.values())
                    .map(ControlFeatureMeasurementValues::toControlFeatureDTO)
                    .collect(Collectors.toList());
        }
        return featureControlMeasurementList;
    }

    private List<ControlFeatureDTO> getFeatureControlTaxonMeasurementList() {

        // If list not exist, create it
        if (featureControlTaxonMeasurementList == null) {
            featureControlTaxonMeasurementList = Arrays.stream(ControlFeatureTaxonMeasurementValues.values())
                    .map(ControlFeatureTaxonMeasurementValues::toControlFeatureDTO)
                    .collect(Collectors.toList());
        }
        return featureControlTaxonMeasurementList;
    }

    /**
     * Only one list of counting.
     *
     * @return ControlFeatureDTO observation list
     */
    private List<ControlFeatureDTO> getFeatureControlObservationList() {

        // If list not exist, create it
        if (featureControlObservationList == null) {
            featureControlObservationList = Arrays.stream(ControlFeatureSurveyValues.values())
                    .map(ControlFeatureSurveyValues::toControlFeatureDTO)
                    .collect(Collectors.toList());
        }
        return featureControlObservationList;
    }

    /**
     * Only one list of counting.
     *
     * @return ControlFeatureDTO prelevement list
     */
    private List<ControlFeatureDTO> getFeatureControlSamplingOperationList() {

        // If list not exist, create it
        if (featureControlSamplingOperationList == null) {
            featureControlSamplingOperationList = Arrays.stream(ControlFeatureSamplingOperationValues.values())
                    .map(ControlFeatureSamplingOperationValues::toControlFeatureDTO)
                    .collect(Collectors.toList());
        }
        return featureControlSamplingOperationList;
    }
}
