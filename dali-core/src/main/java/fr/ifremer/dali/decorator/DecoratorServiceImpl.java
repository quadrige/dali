package fr.ifremer.dali.decorator;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dto.*;
import fr.ifremer.dali.dto.configuration.context.ContextDTO;
import fr.ifremer.dali.dto.configuration.control.RuleListDTO;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.data.survey.OccasionDTO;
import fr.ifremer.dali.dto.referential.*;
import fr.ifremer.dali.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.dali.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.dali.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.quadrige3.core.dao.technical.AlphanumericComparator;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.core.dao.technical.decorator.Decorator;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.core.vo.data.survey.LightSurveyVO;
import fr.ifremer.quadrige3.ui.core.dto.CodeOnly;
import fr.ifremer.quadrige3.ui.core.dto.MonthDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.nuiton.i18n.I18n.t;

/**
 * Dali decorator service.
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 * @since 1.0
 */
public class DecoratorServiceImpl extends fr.ifremer.quadrige3.core.service.decorator.DecoratorServiceImpl
        implements DecoratorService {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onLoadDecorators(fr.ifremer.quadrige3.core.dao.technical.decorator.DecoratorProvider provider) {

        // referential
        provider.registerDecorator(AnalysisInstrumentDTO.class, CODE, "${id}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(AnalysisInstrumentDTO.class, NAME, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(DepartmentDTO.class, "${code}$s#${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(DepartmentDTO.class, CODE, "${code}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(DepartmentDTO.class, NAME, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(LocationDTO.class, "${label}$s#${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(LocationDTO.class, CODE, "${label}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(LocationDTO.class, NAME, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(HarbourDTO.class, "${code}$s#${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(ParameterDTO.class, "${transcribedCode}$s", TOKEN_SEPARATOR, SEPARATOR); // use transcribed code (Mantis #47330)
        provider.registerDecorator(ParameterDTO.class, NAME, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(ParameterDTO.class, CODE_NAME, "${transcribedCode}$s#${name}$s", TOKEN_SEPARATOR, SEPARATOR); // use transcribed code (Mantis #47330)
        provider.registerDecorator(PersonDTO.class, "${firstName}$s#${name}$s - ${department/name}$s", TOKEN_SEPARATOR, " ");
        provider.registerDecorator(PrivilegeDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(new PMFMDecorator());
        provider.registerDecorator(NAME, new PMFMNameDecorator());
        provider.registerDecorator(NAME_WITH_UNIT, new PMFMNameWithUnitDecorator());
        provider.registerDecorator(NAME_WITH_ID, new PMFMNameWithIdDecorator());
        provider.registerDecorator(new SortedQualitativeValueDecorator());
        provider.registerDecorator(UnitDTO.class, WITH_SYMBOL, "${name}$s (${symbol}$s)", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(new TaxonDecorator());
        provider.registerDecorator(TaxonDTO.class, CODE, "${id}$s", TOKEN_SEPARATOR, SEPARATOR); // TODO: where is it used ?
        provider.registerDecorator(WITH_CITATION, new TaxonWithCitationDecorator());
        provider.registerDecorator(WITH_REFERENT, new TaxonWithReferentDecorator());
        provider.registerDecorator(WITH_CITATION_AND_REFERENT, new TaxonWithCitationAndReferentDecorator());
        provider.registerDecorator(TaxonGroupDTO.class, "${label}$s#${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(TaxonGroupDTO.class, NAME, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(SamplingEquipmentDTO.class, CODE, "${id}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(SamplingEquipmentDTO.class, NAME, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(StatusDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(PositioningSystemDTO.class, CODE_NAME, "${code}$s#${name}$s", TOKEN_SEPARATOR, SEPARATOR);

        // extraction
        provider.registerDecorator(ExtractionDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(FilterTypeDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(ExtractionPeriodDTO.class, "${startDate}$td/%1$tm/%1$tY#${endDate}$td/%2$tm/%2$tY", TOKEN_SEPARATOR, " => ");

        // configuration
        provider.registerDecorator(ContextDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(FilterDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(ProgramDTO.class, "${code}$s#${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(ProgramDTO.class, NAME, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(ProgramDTO.class, CODE, "${code}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(StrategyDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);

        // rules
        provider.registerDecorator(RuleListDTO.class, "${code}$s", TOKEN_SEPARATOR, SEPARATOR);

        // Base decorator for object extending BaseReferentialDTO
        provider.registerDecorator(BaseReferentialDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        // Base decorator for object extending CodeOnly
        provider.registerDecorator(CodeOnly.class, "${code}$s", TOKEN_SEPARATOR, SEPARATOR);

        // data
        provider.registerDecorator(OccasionDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(SamplingOperationDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(CONCAT, new SamplingOperationDecorator());
        provider.registerDecorator(new LightSurveyVODecorator());

        // system
        provider.registerDecorator(ParameterTypeDTO.class, "${keyLabel}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(StateDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(FunctionDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(SynchronizationStatusDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(SearchDateDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);
        provider.registerDecorator(MonthDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR);

        // synchro
        provider.registerDecorator(new SynchroTableWithCountDecorator());
        provider.registerDecorator(WITH_COUNT, new SynchroTableWithCountDecorator(WITH_COUNT));

        // misc
        provider.registerDecorator(COLLECTION_SIZE, new CollectionSizeDecorator());
        provider.registerDecorator(DURATION_IN_MINUTES, new DurationDecorator(DURATION_IN_MINUTES));
        provider.registerDecorator(DURATION_IN_DECIMAL_HOURS, new DurationDecorator(DURATION_IN_DECIMAL_HOURS));
        provider.registerDecorator(TIME_IN_HOURS_MINUTES, new TimeDecorator());
        provider.registerDecorator(File.class, "${absolutePath}$s", TOKEN_SEPARATOR, SEPARATOR);
    }

    private class TaxonDecorator extends Decorator<TaxonDTO> {

        TaxonDecorator() {
            super(TaxonDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR, false);
            setUseCache(true);
        }
    }

    private class TaxonWithCitationDecorator extends Decorator<TaxonDTO> {

        TaxonWithCitationDecorator() {
            super(TaxonDTO.class, "${name}$s#${citation}$s", TOKEN_SEPARATOR, " ", false);
            setUseCache(true);
        }

        @Override
        protected Object getValue(TaxonDTO bean, String token) {

            if ("citation".equals(token)) {
                if (bean.getCitation() == null) {
                    return "";
                }
                String citation = bean.getCitation().getName();
                if (citation.startsWith("(")) {
                    return citation;
                }
                return String.format("(%s)", citation);
            }

            return super.getValue(bean, token);
        }
    }

    private class TaxonWithReferentDecorator extends Decorator<TaxonDTO> {

        TaxonWithReferentDecorator() {
            super(TaxonDTO.class, "${name}$s#${referent}$s", TOKEN_SEPARATOR, "", false);
            setUseCache(true);
        }

        @Override
        protected Object getValue(TaxonDTO bean, String token) {

            if ("referent".equals(token)) {

                if (bean.isReferent()) {
                    return "";
                }

                return String.format(" = %s", bean.getReferenceTaxon() == null ? "[unknown referent]" : bean.getReferenceTaxon().getName());
            }

            return super.getValue(bean, token);
        }
    }

    private class TaxonWithCitationAndReferentDecorator extends Decorator<TaxonDTO> {

        TaxonWithCitationAndReferentDecorator() {
            super(TaxonDTO.class, "${name}$s#${citation}$s#${referent}$s", TOKEN_SEPARATOR, " ", false);
            setUseCache(true);
        }

        @Override
        protected Object getValue(TaxonDTO bean, String token) {

            if ("citation".equals(token)) {
                if (bean.getCitation() == null) {
                    return "";
                }
                String citation = bean.getCitation().getName();
                if (citation.startsWith("(")) {
                    return citation;
                }
                return String.format("(%s)", citation);

            } else if ("referent".equals(token)) {

                if (bean.isReferent()) {
                    return "";
                }

                return String.format("= %s", bean.getReferenceTaxon() == null ? "[unknown referent]" : bean.getReferenceTaxon().getName());
            }


            return super.getValue(bean, token);
        }
    }

    class PMFMNameDecorator extends Decorator<PmfmDTO> {

        static final String TOKEN_NAME = "name";

        PMFMNameDecorator() {
            this("${" + TOKEN_NAME + "}$s");
        }

        PMFMNameDecorator(String expression) {
            this(expression, SEPARATOR);
        }

        PMFMNameDecorator(String expression, String separatorReplacement) {
            super(PmfmDTO.class, expression, TOKEN_SEPARATOR, separatorReplacement, true);
        }

        @Override
        protected Object getValue(PmfmDTO bean, String token) {

            if (TOKEN_NAME.equals(token)) {

                // return parameter name if transcribed name is not set
                if (StringUtils.isBlank(bean.getName())) {
                    return bean.getParameter() != null ? bean.getParameter().getName() : null;
                }

                return bean.getName();
            }

            return super.getValue(bean, token);
        }
    }

    private class PMFMDecorator extends PMFMNameDecorator {

        PMFMDecorator() {
            super("${" + TOKEN_NAME + "}$s#${parameter/transcribedCode}$s#${matrix/name}$s#${fraction/name}$s#${method/name}$s#${unit/symbol}$s");
        }
    }

    private class PMFMNameWithIdDecorator extends PMFMNameDecorator {

        PMFMNameWithIdDecorator() {
            // The id is put at the end of the string to preserve alphabetic order (Mantis #46672)
            super("${" + TOKEN_NAME + "}$s#${parameter/transcribedCode}$s#${matrix/name}$s#${fraction/name}$s#${method/name}$s#${unit/symbol}$s#${id}$s");
        }
    }

    private class PMFMNameWithUnitDecorator extends PMFMNameDecorator {

        int noUnitId;
        boolean checkConfig = false;

        PMFMNameWithUnitDecorator() {
            super("${" + TOKEN_NAME + "}$s#${unit/symbol}$s", "");
        }

        @Override
        protected Object getValue(PmfmDTO bean, String token) {

            if ("unit/symbol".equals(token)) {

                if (!checkConfig) {
                    DaliConfiguration config = DaliConfiguration.getInstance();
                    noUnitId = config.getNoUnitId();
                    checkConfig = true;
                }

                // check if unit is 'no unit'
                if (bean.getUnit() == null || bean.getUnit().getId() == noUnitId) {
                    return "";
                }

                return String.format(" (%s)", super.getValue(bean, token));
            }

            return super.getValue(bean, token);
        }
    }

    /**
     * QualitativeValue decorator with enhanced sorting comparator
     * fixme maybe to deprecate ?
     */
    private class SortedQualitativeValueDecorator extends Decorator<QualitativeValueDTO> {

        SortedQualitativeValueDecorator() {
            super(QualitativeValueDTO.class, "${name}$s", TOKEN_SEPARATOR, SEPARATOR, false);

            context.setComparator((o1, o2) ->
                    AlphanumericComparator.instance().compare(
                            SortedQualitativeValueDecorator.this.toString(o1),
                            SortedQualitativeValueDecorator.this.toString(o2)
                    ));
        }
    }

    /**
     * Survey decorator.
     */
    private class LightSurveyVODecorator extends Decorator<LightSurveyVO> {

        private static final String SEPARATOR = " - ";

        /**
         * Constructor.
         */
        LightSurveyVODecorator() {
            super(LightSurveyVO.class, "${surveyId}$s", "", "", false);
        }

        @Override
        protected Object getValue(final LightSurveyVO bean, final String token) {
            final StringBuilder result = new StringBuilder(20);

            // Add location
            if (bean.getMonitoringLocation() != null) {
                result.append(bean.getMonitoringLocation().getLabel());
                result.append(SEPARATOR);
                result.append(bean.getMonitoringLocation().getName());
                result.append(SEPARATOR);
            }

            // Add survey date
            String dateStr = Dates.formatDate(bean.getSurveyDt(), DaliConfiguration.getInstance().getDateFormat());
            result.append(dateStr);

            // Add programs
            if (ArrayUtils.isNotEmpty(bean.getPrograms())) {
                for (ProgramVO program : bean.getPrograms()) {
                    result.append(SEPARATOR);
                    result.append(program.getProgCd());
                }
            }

            return result.toString();
        }
    }

    /**
     * Sampling Operation decorator.
     */
    private class SamplingOperationDecorator extends Decorator<SamplingOperationDTO> {

        private static final String SEPARATOR = " - ";

        /**
         * Constructor.
         */
        SamplingOperationDecorator() {
            super(SamplingOperationDTO.class, "${name}$s", "", "", false);
        }

        @Override
        protected Object getValue(final SamplingOperationDTO bean, final String token) {

            return bean.getName() +
                    SEPARATOR +
                    Times.secondsToString(bean.getTime()) +
                    SEPARATOR +
                    bean.getSamplingEquipment().getName();
        }
    }

    /**
     * Time decoration.
     */
    private class TimeDecorator extends Decorator<Integer> {

        TimeDecorator() {
            super(Integer.class, "${time}$s", "", "", false);
        }

        @Override
        protected Object getValue(final Integer bean, final String token) {
            return Times.secondsToString(bean);
        }
    }

    /**
     * Decorator display duration (Integer representing number of minutes) to days/hours/minutes readable format
     */
    private class DurationDecorator extends Decorator<Number> {

        private final String type;

        DurationDecorator(String type) {
            super(Number.class, "${duration}$s", "", "", false);
            this.type = type;
        }

        @Override
        protected Object getValue(final Number number, final String token) {
            // return string representation of a duration
            if (number == null) {
                return "null";
            }
            // convert the bean vale to the correct number of minutes
            int duration = 0;
            switch (type) {
                case DURATION_IN_MINUTES:
                    // standard case (number should be an Integer)
                    duration = number.intValue();
                    break;

                case DURATION_IN_DECIMAL_HOURS:
                    // convert decimal hours in minutes (number can be a Double)
                    duration = (int) Math.round(number.doubleValue() * 60);
                    break;

                default:
                    break;
            }

            if (duration < 0) {
                return "< 0";
            }
            long days = TimeUnit.MINUTES.toDays(duration);
            duration -= TimeUnit.DAYS.toMinutes(days);
            long hours = TimeUnit.MINUTES.toHours(duration);
            duration -= TimeUnit.HOURS.toMinutes(hours);
            long minutes = TimeUnit.MINUTES.toMinutes(duration);

            StringBuilder sb = new StringBuilder(64);
            if (days > 0) {
                sb.append(days);
                sb.append("j ");
            }
            if (hours > 0) {
                sb.append(hours);
                sb.append("h ");
            } else if (minutes > 0) {
                sb.append("0h ");
            }
            sb.append(minutes);
            sb.append("m");
            return sb.toString();
        }
    }

    private class SynchroTableWithCountDecorator extends Decorator<SynchroTableDTO> {

        private final String type;

        SynchroTableWithCountDecorator(String type) {
            super(SynchroTableDTO.class, "${name}$s", "", "", false);
            this.type = type;
            Assert.isTrue(type == null || type.equals(WITH_COUNT), "unknown type for this decorator. Expected: null or 'withCount'");
        }

        SynchroTableWithCountDecorator() {
            super(SynchroTableDTO.class, "${name}$s", "", "", false);
            this.type = null;
        }

        @Override
        protected Object getValue(SynchroTableDTO bean, String token) {

            // Translate table name
            String decoratedTableName = t("quadrige3.table." + bean.getName());
            if (decoratedTableName.startsWith("quadrige3.table")) {
                // Use table name as default translation
                decoratedTableName = bean.getName();
            }

            if (type == null) {
                return decoratedTableName;
            }

            // Count nb rows and nb resolve rows
            int nbTotal = bean.sizeRows();

            return String.format("%s (%s)",
                    decoratedTableName,
                    nbTotal);
        }

    }

    // Decorator for collection of BaseReferentialDTO
    private class CollectionSizeDecorator extends Decorator<BaseReferentialDTO> {

        CollectionSizeDecorator() {
            super(BaseReferentialDTO.class, "${size}$s", "", "", false);
        }

        @Override
        public String toString(Object object) {
            // return directly the collection size
            return String.valueOf(CollectionUtils.size(object));
        }

        @Override
        protected Object onNullValue(BaseReferentialDTO bean, String token) {
            return "0";
        }
    }

}
