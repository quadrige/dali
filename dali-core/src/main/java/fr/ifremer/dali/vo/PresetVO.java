package fr.ifremer.dali.vo;

/*-
 * #%L
 * Dali :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author peck7 on 21/07/2017.
 */
public class PresetVO {

    private String programCode;

    private Multimap<Integer, Integer> pmfmPresets;

    public PresetVO() {
        this(null);
    }

    public PresetVO(String programCode) {
        this.programCode = programCode;
        this.pmfmPresets = LinkedListMultimap.create();
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public Multimap<Integer, Integer> getPmfmPresets() {
        return pmfmPresets;
    }

    public void setPmfmPresets(Multimap<Integer, Integer> pmfmPresets) {
        this.pmfmPresets = pmfmPresets;
    }

    public Set<Integer> getPmfmIds() {
        return pmfmPresets.keySet();
    }

    public Collection<Integer> getQualitativeValueIds(int pmfmId) {
        return pmfmPresets.get(pmfmId);
    }

    public void addPmfmPreset(Integer pmfmId, List<Integer> qualitativeValuesIds) {
        pmfmPresets.putAll(pmfmId, qualitativeValuesIds);
    }

    public void clearPmfmPresets() {
        pmfmPresets.clear();
    }
}
