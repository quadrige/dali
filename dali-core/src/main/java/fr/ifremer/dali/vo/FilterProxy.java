package fr.ifremer.dali.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Proxy used to Serialize/Deserialize Filters
 *
 * @author peck7 on 18/09/2019.
 */
public class FilterProxy implements Serializable {

    public static final String CURRENT_VERSION = "1.0";
    private final String version;
    private final List<? extends FilterVO> filters;

    public FilterProxy(List<? extends FilterVO> filters) {
        this.version = CURRENT_VERSION;
        this.filters = filters;
    }

    public final String getVersion() {
        return version;
    }

    public List<? extends FilterVO> getFilters() {
        return filters;
    }

}
