package fr.ifremer.dali.vo;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @author peck7 on 21/11/2017.
 */
public class ExtractionParameterVO {

    private PresetVO preset;

    private boolean fillZero;

    private List<Integer> resultPmfmIds;

    public ExtractionParameterVO() {
        resultPmfmIds = new ArrayList<>();
    }

    public PresetVO getPreset() {
        return preset;
    }

    public void setPreset(PresetVO preset) {
        this.preset = preset;
    }

    public boolean isFillZero() {
        return fillZero;
    }

    public void setFillZero(boolean fillZero) {
        this.fillZero = fillZero;
    }

    public List<Integer> getResultPmfmIds() {
        return resultPmfmIds;
    }

    public void setResultPmfmIds(List<Integer> resultPmfmIds) {
        this.resultPmfmIds = resultPmfmIds;
    }

    public void addResultPmfmId(Integer pmfmId) {
        resultPmfmIds.add(pmfmId);
    }
}
