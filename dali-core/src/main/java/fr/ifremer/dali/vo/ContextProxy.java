package fr.ifremer.dali.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Proxy used to Serialize/Deserialize Contexts
 *
 * @author peck7 on 18/09/2019.
 */
public class ContextProxy implements Serializable {

    public static final String CURRENT_VERSION = "1.0";
    private final String version;
    private final List<? extends ContextVO> contexts;

    public ContextProxy(List<? extends ContextVO> contexts) {
        this.version = CURRENT_VERSION;
        this.contexts = contexts;
    }

    public final String getVersion() {
        return version;
    }

    public List<? extends ContextVO> getContexts() {
        return contexts;
    }

}
