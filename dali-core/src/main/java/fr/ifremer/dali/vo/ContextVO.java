package fr.ifremer.dali.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author peck7 on 08/10/2018.
 */
public class ContextVO {

    private String name;

    private String description;

    private List<FilterVO> filters;

    public ContextVO() {
        filters = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FilterVO> getFilters() {
        return filters;
    }

    public void setFilters(List<FilterVO> filters) {
        this.filters = filters;
    }
}
