package fr.ifremer.dali.security;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>DaliPermissionEvaluator class.</p>
 *
 * This class is not used because Dali has less restrictions
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
@Service("daliPermissionEvaluator")
public class DaliPermissionEvaluator implements PermissionEvaluator {

//    @Autowired
//    @Lazy
//    protected ProgramStrategyService programStrategyService;
//
//    @Autowired
//    @Lazy
//    protected CampaignService campaignService;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        return hasPermission(authentication, targetDomainObject instanceof Serializable ? (Serializable) targetDomainObject : null, null, permission);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {

        // basic permission
        if (!SecurityContextHelper.hasAuthority(authentication, permission)) {
            return false;
        }

        // code below is not 100% functional
        return true;

        /*
        boolean result = false;

        // specific permission for program related objects
        if (StringUtils.isNotBlank(targetType) && targetId != null) {

            switch (targetType) {
                case DaliPermissionType.SAVE_PROGRAM: {

                    // targetId could be a collection of ProgramDTO
                    Collection<String> programCodes = new ArrayList<>();
                    if (targetId instanceof Collection) {
                        programCodes.addAll(((Collection<ProgramDTO>) targetId).stream().map(ProgramDTO::getCode).collect(Collectors.toList()));
                    } else if (targetId instanceof ProgramDTO) {
                        programCodes.add(((ProgramDTO) targetId).getCode());
                    }
                    result = userHasWriteAccessOnPrograms(programCodes);
                }
                break;

                case DaliPermissionType.SAVE_PROGRAM_STRATEGY: {

                    // targetId should be a Collection of ProgStratDTO
                    Collection<String> programCodes = new ArrayList<>();
                    if (targetId instanceof Collection) {
                        programCodes.addAll(((Collection<ProgStratDTO>) targetId).stream().map(progStratDTO -> progStratDTO.getProgram().getCode()).collect(Collectors.toList()));
                    }
                    result = userHasWriteAccessOnPrograms(programCodes);
                }
                break;

                case DaliPermissionType.SAVE_CAMPAIGN: {

                    // targetId should be a collection of CampaignDTO
                    List<Integer> campaignIds = new ArrayList<>();
                    if (targetId instanceof Collection) {
                        campaignIds.addAll(((Collection<CampaignDTO>)targetId).stream().filter(campaignDTO -> campaignDTO.getId()!=null).map(CampaignDTO::getId).collect(Collectors.toList()));
                    }
                    result = userHasWriteAccessOnCampaigns(campaignIds);

                }
                break;

                case DaliPermissionType.DELETE_CAMPAIGN: {

                    // targetId should be a collection of Integer
                    result = userHasWriteAccessOnCampaigns((Collection<Integer>) targetId);
                }
                break;

                case DaliPermissionType.RULE_LIST:
                    break;

                default:
                    result = false;

            }

        }

        return result;
        */
    }

//    private boolean userHasWriteAccessOnPrograms(Collection<String> programCodes) {
//
//        List<ProgramVO> writablePrograms = programStrategyService.getWritableProgramsByQuserId(SecurityContextHelper.getQuadrigeUserId());
//        List<String> writableProgramCodes = writablePrograms.stream().map(ProgramVO::getProgCd).collect(Collectors.toList());
//
//        return writableProgramCodes.containsAll(programCodes);
//    }
//
//    private boolean userHasWriteAccessOnCampaigns(Collection<Integer> campaignIds) {
//
//        return campaignService.getWritableCampaignForUser(SecurityContextHelper.getQuadrigeUserId()).containsAll(campaignIds);
//    }
}
