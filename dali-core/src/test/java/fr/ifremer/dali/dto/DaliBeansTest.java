package fr.ifremer.dali.dto;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.dali.dao.technical.Daos;
import fr.ifremer.quadrige3.core.dao.referential.UnitId;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ludovic.pecquot@e-is.pro on 13/04/2017.
 */
public class DaliBeansTest {

    /**
     * Test de conversion d'unité de longueur
     */
    @Test
    public void testConvertLengthValue() {

        BigDecimal result = DaliBeans.convertLengthValue(BigDecimal.valueOf(5), UnitId.METER.getValue(), UnitId.CENTIMENTER.getValue());
        Assert.assertEquals(500, result.intValue());

        result = DaliBeans.convertLengthValue(BigDecimal.valueOf(2), UnitId.METER.getValue(), UnitId.MILLIMETER.getValue());
        Assert.assertEquals(2000, result.intValue());

        result = DaliBeans.convertLengthValue(BigDecimal.valueOf(6000), UnitId.CENTIMENTER.getValue(), UnitId.METER.getValue());
        Assert.assertEquals(60, result.intValue());

    }

    @Test
    public void testStringJoiner() {
        Assert.assertEquals("'A','B','C'", Daos.getInStatementFromStringCollection(ImmutableList.of("A","B","A","C")));
        Assert.assertEquals("", Daos.getInStatementFromStringCollection(new ArrayList<>()));
        Assert.assertEquals("", Daos.getInStatementFromStringCollection(null));
    }

    @Test
    public void testSplit() {
        Assert.assertArrayEquals(new String[] {"a","a1","b","b123"}, DaliBeans.split("a,a1,b,b123", ",").toArray());
        Assert.assertArrayEquals(new String[] {"a","a1","b","b123"}, DaliBeans.split("a|a1|b|b123", "|").toArray());
    }

    @Test
    public void testSplitAndMap() {
        Map<String,String> expectedMap = new LinkedHashMap<>();
        expectedMap.put("key1", "value1");
        expectedMap.put("key2", "value2");
        expectedMap.put("key3", "value3");

        Assert.assertEquals(expectedMap, DaliBeans.splitAndMap("key1:value1;key2:value2;key3:value3", ";", ":"));
        Assert.assertEquals(expectedMap, DaliBeans.splitAndMap("key1|value1;key2|value2;key3|value3", ";", "|"));
        Assert.assertEquals(expectedMap, DaliBeans.splitAndMap("key1=value1|key2=value2|key3=value3", "|", "="));

        try {
            DaliBeans.splitAndMap("key1:value1;key1:value2;key3:value3", ";", ":");
            Assert.fail("should throw IllegalStateException");
        } catch (IllegalStateException e) {
            Assert.assertNotNull(e);
        }
    }
}
