package fr.ifremer.dali;

/*-
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfigurationOption;
import fr.ifremer.quadrige3.core.test.InitTests;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.service.DaliServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.List;

/**
 * @author peck7 on 13/10/2017.
 */
public class InitDaliTests extends InitTests {

    private static final Log LOG = LogFactory.getLog(InitDaliTests.class);

    public static void main(String[] args) {
        InitDaliTests initDaliTests = new InitDaliTests();
        try {
            initDaliTests.before();
        } catch (Throwable t) {
            LOG.error(t.getLocalizedMessage(), t);
        }
    }

    public InitDaliTests() {
        super();
        setTargetDbDirectory(DaliDatabaseResource.HSQLDB_SRC_DATABASE_DIRECTORY);
    }

    @Override
    protected void initServiceLocator() {
        DaliServiceLocator.initDaliDefault();
    }

    protected String getModuleName() {
        return DaliDatabaseResource.MODULE_NAME;
    }

    @Override
    protected String getDbEnumerationResource() {
        return "classpath*:quadrige3-db-enumerations.properties,classpath*:dali-db-test-enumerations.properties";
    }

    @Override
    protected String[] getConfigArgs() {
        List<String> args = Lists.newArrayList();
        args.addAll(Arrays.asList(super.getConfigArgs()));
        args.addAll(ImmutableList.of("--option", QuadrigeCoreConfigurationOption.AUTHENTICATION_DISABLED.getKey(), Boolean.toString(true)));

        return args.toArray(new String[args.size()]);
    }


    @Override
    protected QuadrigeConfiguration createConfig() {

        DaliConfiguration daliConfiguration = new DaliConfiguration(getModuleName() + "-test-write.properties",
                getConfigArgs());
        DaliConfiguration.setInstance(daliConfiguration);

        return QuadrigeConfiguration.getInstance();
    }

}
