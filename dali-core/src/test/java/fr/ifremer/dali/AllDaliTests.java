package fr.ifremer.dali;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2012 - 2013 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dao.DaoTestSuite;
import fr.ifremer.dali.dto.DaliBeansTest;
import fr.ifremer.dali.service.ServiceTestsSuite;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DaoTestSuite.class,
        ServiceTestsSuite.class,
        DaliBeansTest.class
})
public class AllDaliTests {

    @ClassRule
    public static InitDaliTests initTests = new InitDaliTests();
}
