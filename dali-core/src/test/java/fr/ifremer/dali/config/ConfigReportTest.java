package fr.ifremer.dali.config;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dao.data.photo.PhotoDaoWriteTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.nuiton.config.ApplicationConfigProvider;

import java.util.ServiceLoader;

public class ConfigReportTest {

    private static final Log log = LogFactory.getLog(PhotoDaoWriteTest.class);

    @Test
    public void loadApplicationConfigProviders() {

        ServiceLoader<ApplicationConfigProvider>
            loader = ServiceLoader.load(ApplicationConfigProvider.class);

        for (ApplicationConfigProvider configProvider : loader) {
            log.debug(configProvider.getName());
        }

    }
}
