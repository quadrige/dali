package fr.ifremer.dali.dao.data.samplingoperation;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.DaliSamplingEquipmentDao;
import fr.ifremer.dali.dao.referential.DaliUnitDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliPmfmDao;
import fr.ifremer.dali.dao.technical.Geometries;
import fr.ifremer.dali.dto.CoordinateDTO;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SamplingOperationDaoWriteTest extends AbstractDaoTest {

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();

    private DaliReferentialDao refDao;
    private DaliPmfmDao pmfmDao;
    private DaliSamplingOperationDao samplingOperationDao;
    private DaliDepartmentDao departmentDao;
    private DaliSamplingEquipmentDao samplingEquipmentDao;
    private DaliUnitDao unitDao;

    private static final int SURVEY_ID = 101;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        refDao = DaliServiceLocator.instance().getService("daliReferentialDao", DaliReferentialDao.class);
        pmfmDao = DaliServiceLocator.instance().getService("daliPmfmDao", DaliPmfmDao.class);
        samplingOperationDao = DaliServiceLocator.instance().getService("daliSamplingOperationDao", DaliSamplingOperationDao.class);
        departmentDao = DaliServiceLocator.instance().getService("daliDepartmentDao", DaliDepartmentDao.class);
        samplingEquipmentDao = DaliServiceLocator.instance().getService("daliSamplingEquipmentDao", DaliSamplingEquipmentDao.class);
        unitDao = DaliServiceLocator.instance().getService("daliUnitDao", DaliUnitDao.class);
        setCommitOnTearDown(true);
    }

    @Test
    public void createUpdateSamplingOperation() {

        List<SamplingOperationDTO> samplingOperations = samplingOperationDao.getSamplingOperationsBySurveyId(SURVEY_ID, true);
        assertNotNull(samplingOperations);
        assertEquals(0, samplingOperations.size());

        // create one
        SamplingOperationDTO samplingOperation = DaliBeanFactory.newSamplingOperationDTO();
        samplingOperation.setName("1");
        samplingOperation.setTime(1200);
        samplingOperation.setSize(10d);
        samplingOperation.setSizeUnit(unitDao.getUnitById(12));
        samplingOperation.setComment("comment 1");
        samplingOperation.setSamplingEquipment(samplingEquipmentDao.getSamplingEquipmentById(4));
        samplingOperation.setPmfms(null);
        samplingOperation.setSamplingDepartment(departmentDao.getDepartmentById(1));
        samplingOperation.setDepthLevel(refDao.getDepthLevelById(1));
        samplingOperation.setPositioning(refDao.getPositioningSystemById(1));
        samplingOperation.setDirty(true);
        samplingOperations = Lists.newArrayList(samplingOperation);

        samplingOperationDao.saveSamplingOperationsBySurveyId(SURVEY_ID, samplingOperations);

        // check ID
        assertNotNull(samplingOperation.getId());
        Integer samplingOperationId = samplingOperation.getId();

        // reload
        samplingOperations = samplingOperationDao.getSamplingOperationsBySurveyId(SURVEY_ID, true);
        assertNotNull(samplingOperations);
        assertEquals(1, samplingOperations.size());
        SamplingOperationDTO reloadedSamplingOperation = samplingOperations.get(0);
        assertSamplingOperationEquals(samplingOperation, reloadedSamplingOperation);

        // modify the first one
        samplingOperation.setComment("new comment 1");
        // add pmfms and measurements
        PmfmDTO pmfm1 = pmfmDao.getPmfmById(3);
        assertNotNull(pmfm1);
        MeasurementDTO m1 = DaliBeanFactory.newMeasurementDTO();
        m1.setNumericalValue(BigDecimal.valueOf(2.5));
        m1.setPrecision(2);
        m1.setDigitNb(1);
        m1.setComment("numeric measurement on pmfm 143 (sampling operation)");
        m1.setPmfm(pmfm1);
        MeasurementDTO m2 = DaliBeanFactory.newMeasurementDTO();
        m2.setNumericalValue(BigDecimal.valueOf(1.28));
        m2.setPrecision(3);
        m2.setDigitNb(2);
        m2.setComment("numeric measurement 2 on pmfm 143 (sampling operation)");
        m2.setPmfm(pmfm1);
//        samplingOperation.setPmfms(Lists.newArrayList(pmfm1));
        samplingOperation.setMeasurements(Lists.newArrayList(m1, m2));
        samplingOperation.setSamplingEquipment(samplingEquipmentDao.getSamplingEquipmentById(1));
        samplingOperation.setDirty(true);

        // create another one
        SamplingOperationDTO samplingOperation2 = DaliBeanFactory.newSamplingOperationDTO();
        samplingOperation2.setName("2");
        samplingOperation2.setTime(260);
        samplingOperation2.setSize(0.5);
        samplingOperation2.setSizeUnit(unitDao.getUnitById(13));
        samplingOperation2.setComment("comment 2");
        samplingOperation2.setSamplingEquipment(samplingEquipmentDao.getSamplingEquipmentById(5));
        samplingOperation2.setPmfms(null);
        samplingOperation2.setSamplingDepartment(departmentDao.getDepartmentById(2));
        samplingOperation2.setDepthLevel(refDao.getDepthLevelById(2));
        samplingOperation2.setPositioning(refDao.getPositioningSystemById(2));
        samplingOperation2.setDirty(true);
        samplingOperations = Lists.newArrayList(samplingOperation, samplingOperation2);

        // save
        samplingOperationDao.saveSamplingOperationsBySurveyId(SURVEY_ID, samplingOperations);
        // check ID
        assertNotNull(samplingOperation2.getId());
        assertNotEquals(samplingOperationId, samplingOperation2.getId());
        assertEquals(samplingOperationId, samplingOperation.getId());

        // reload
        samplingOperations = samplingOperationDao.getSamplingOperationsBySurveyId(SURVEY_ID, true);
        assertNotNull(samplingOperations);
        assertEquals(2, samplingOperations.size());

        Map<Integer, SamplingOperationDTO> map = DaliBeans.mapById(samplingOperations);

        assertSamplingOperationEquals(samplingOperation, map.get(samplingOperation.getId()));
        assertSamplingOperationEquals(samplingOperation2, map.get(samplingOperation2.getId()));

        // remove a sampling operation
        samplingOperationDao.remove(samplingOperation.getId());

//        // reload
//        samplingOperations = samplingOperationDao.getSamplingOperationsBySurveyId(SURVEY_ID, true);
//        assertNotNull(samplingOperations);
//        assertEquals(1, samplingOperations.size());
//        assertEquals(samplingOperation2.getId(), samplingOperations.get(0).getId());

        // reload as detail
        List<SamplingOperationDTO> detailedOperations = samplingOperationDao.getSamplingOperationsBySurveyId(SURVEY_ID, true);
        assertNotNull(detailedOperations);
        assertEquals(1, detailedOperations.size());
        SamplingOperationDTO detailedOperation = detailedOperations.get(0);
        assertSamplingOperationEquals(samplingOperation2, detailedOperation);

        // affect more properties
        detailedOperation.setDepth(12.);
        detailedOperation.setMinDepth(10.1);
        detailedOperation.setMaxDepth(15.5);
        detailedOperation.setIndividualCount(5);
        CoordinateDTO coordinate = DaliBeanFactory.newCoordinateDTO();
        coordinate.setMinLongitude(3.348000);
        coordinate.setMinLatitude(48.8984100);
        coordinate.setMaxLongitude(3.655000);
        coordinate.setMaxLatitude(49.019200);
        detailedOperation.setCoordinate(coordinate);
        detailedOperation.setDirty(true);

        // save
        samplingOperationDao.saveSamplingOperationsBySurveyId(SURVEY_ID, detailedOperations);

        // reload and compare
        List<SamplingOperationDTO> reloadedDetailedOperations = samplingOperationDao.getSamplingOperationsBySurveyId(SURVEY_ID, true);
        assertNotNull(reloadedDetailedOperations);
        assertEquals(1, reloadedDetailedOperations.size());
        assertSamplingOperationEquals(detailedOperation, reloadedDetailedOperations.get(0));

    }

    @Test
    public void deleteAllSamplingOperations() {
        samplingOperationDao.removeBySurveyId(SURVEY_ID);
    }

    private void assertSamplingOperationEquals(SamplingOperationDTO expectedSamplingOperation, SamplingOperationDTO samplingOperationToTest) {
        assertTrue(expectedSamplingOperation != samplingOperationToTest);
        assertEquals(expectedSamplingOperation.getId(), samplingOperationToTest.getId());
        assertEquals(expectedSamplingOperation.getComment(), samplingOperationToTest.getComment());
        assertCoordinateEquals(expectedSamplingOperation.getCoordinate(), samplingOperationToTest.getCoordinate());
        assertEquals(expectedSamplingOperation.getSamplingEquipment(), samplingOperationToTest.getSamplingEquipment());
        assertEquals(expectedSamplingOperation.getTime(), samplingOperationToTest.getTime());
        assertEquals(expectedSamplingOperation.getDepth(), samplingOperationToTest.getDepth());
        assertEquals(expectedSamplingOperation.getMaxDepth(), samplingOperationToTest.getMaxDepth());
        assertEquals(expectedSamplingOperation.getMinDepth(), samplingOperationToTest.getMinDepth());
        assertEquals(expectedSamplingOperation.getName(), samplingOperationToTest.getName());
        assertEquals(expectedSamplingOperation.getIndividualCount(), samplingOperationToTest.getIndividualCount());
        assertEquals(expectedSamplingOperation.getDepthLevel(), samplingOperationToTest.getDepthLevel());
        assertEquals(expectedSamplingOperation.getSamplingDepartment(), samplingOperationToTest.getSamplingDepartment());
        assertEquals(expectedSamplingOperation.getSize(), samplingOperationToTest.getSize());
        assertEquals(expectedSamplingOperation.getSizeUnit(), samplingOperationToTest.getSizeUnit());
        assertPmfmsEquals(expectedSamplingOperation.getPmfms(), samplingOperationToTest.getPmfms());
        assertPmfmsEquals(expectedSamplingOperation.getIndividualPmfms(), samplingOperationToTest.getIndividualPmfms());
    }

    private void assertCoordinateEquals(CoordinateDTO coordinateBase, CoordinateDTO coordinateToControl) {
        if (coordinateBase == null) {
            assertFalse(Geometries.isValid(coordinateToControl));
            return;
        } else {
            assertNotNull(coordinateToControl);
        }
        assertTrue(Geometries.equals(coordinateBase, coordinateToControl));
    }

    private void assertPmfmsEquals(Collection<PmfmDTO> expectedPmfms, Collection<PmfmDTO> pmfmsToTest) {
        assertTrue(expectedPmfms != pmfmsToTest);
        assertEquals(expectedPmfms.size(), pmfmsToTest.size());
        Map<Integer, PmfmDTO> map = DaliBeans.mapById(pmfmsToTest);
        for (PmfmDTO expectedPmfm : expectedPmfms) {
            PmfmDTO pmfmToTest = map.get(expectedPmfm.getId());
            assertNotNull(pmfmToTest);
            // useless to go deeper in measurement (see MeasurementDaoWriteTest)
        }
    }
}
