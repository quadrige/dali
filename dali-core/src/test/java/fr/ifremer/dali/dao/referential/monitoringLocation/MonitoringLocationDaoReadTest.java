package fr.ifremer.dali.dao.referential.monitoringLocation;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.service.StatusFilter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MonitoringLocationDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(MonitoringLocationDaoReadTest.class);
    
    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.readDb();
    
    private DaliMonitoringLocationDao mlDao;
    
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        mlDao = DaliServiceLocator.instance().getService("daliMonitoringLocationDao", DaliMonitoringLocationDao.class);
    }
    
    @Test
    public void getAllLocationsByProgramCode() {
        List<LocationDTO> monitoringLocations  = mlDao.getLocationsByCampaignAndProgram(null, null, StatusFilter.ALL.toStatusCodes());
        assertNotNull(monitoringLocations);
        assertEquals(4, monitoringLocations.size());
        if (log.isDebugEnabled()) {
            for (LocationDTO dto: monitoringLocations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        
        monitoringLocations  = mlDao.getLocationsByCampaignAndProgram(null, "REMIS", StatusFilter.ALL.toStatusCodes());
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());

        monitoringLocations  = mlDao.getLocationsByCampaignAndProgram(1, "REMIS", StatusFilter.ALL.toStatusCodes());
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());
    }

    @Test
    public void getAllReferentialLocations() {
        // national
        List<LocationDTO> monitoringLocations  = mlDao.getAllLocations(StatusFilter.ALL.toStatusCodes());
        assertNotNull(monitoringLocations);
        assertEquals(4, monitoringLocations.size());
        if (log.isDebugEnabled()) {
            for (LocationDTO dto: monitoringLocations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        
    }

    @Test
    public void getAllReferentialLocationByCriteria() {
        // national
        List<LocationDTO> monitoringLocations  = mlDao.findLocations(StatusFilter.ALL.toStatusCodes(), null, null, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(4, monitoringLocations.size());
        if (log.isDebugEnabled()) {
            for (LocationDTO dto: monitoringLocations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.ALL.toStatusCodes(), "ZONESMARINES", null, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(3, monitoringLocations.size());
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.ALL.toStatusCodes(), "ZONESMARINES", 9, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(1, monitoringLocations.size());
        assertEquals((Integer)1, monitoringLocations.get(0).getId());
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.ALL.toStatusCodes(), null, 9, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(1, monitoringLocations.size());
        assertEquals((Integer)1, monitoringLocations.get(0).getId());
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.ALL.toStatusCodes(), null, null, "REMIS", null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());

    }


}
