package fr.ifremer.dali.dao.system.rule;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.dali.dto.configuration.control.RuleListDTO;
import fr.ifremer.dali.dto.enums.ControlElementValues;
import fr.ifremer.dali.dto.enums.ControlFeatureMeasurementValues;
import fr.ifremer.dali.dto.enums.ControlFunctionValues;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.*;

public class RuleListDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(RuleListDaoWriteTest.class);

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();

    private DaliRuleListDao ruleListDao;
    private ReferentialService referentialService;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        ruleListDao = DaliServiceLocator.instance().getService("daliRuleListDao", DaliRuleListDao.class);
        referentialService = DaliServiceLocator.instance().getReferentialService();
    }

    @Test
    public void saveRuleListWithPreconditionedRules() {

        // RULELIST2 comes from test dataset only
        RuleListDTO ruleList = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(1));
        assertNotNull(ruleList);
        assertEquals("rule list with preconditions", ruleList.getDescription());

        assertEquals(1, ruleList.sizeControlRules());
        ControlRuleDTO controlRule = ruleList.getControlRules(0);
        assertTrue(controlRule.isActive());
        assertEquals("PRECOND1", controlRule.getCode());
        assertTrue(ControlFunctionValues.PRECONDITION.equals(controlRule.getFunction()));
        assertTrue(ControlElementValues.MEASUREMENT.equals(controlRule.getControlElement()));
        assertTrue(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(controlRule.getControlFeature()));

        assertEquals(2, controlRule.sizeRulePmfms());
        assertEquals(4, referentialService.getUniquePmfmIdFromPmfm(controlRule.getRulePmfms(0).getPmfm()).intValue());
        assertEquals(24, referentialService.getUniquePmfmIdFromPmfm(controlRule.getRulePmfms(1).getPmfm()).intValue());

        assertEquals(2, controlRule.sizePreconditions());
        PreconditionRuleDTO precondition1 = DaliBeans.findById(controlRule.getPreconditions(), 1);
        assertNotNull(precondition1);
        assertEquals(1, precondition1.getId().intValue());
        assertTrue(precondition1.isActive());
        assertTrue(precondition1.isBidirectional());
        assertEquals("SALMOI1", precondition1.getBaseRule().getCode());
        assertEquals("2", precondition1.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO1", precondition1.getUsedRule().getCode());
        assertEquals("4", precondition1.getUsedRule().getAllowedValues());

        PreconditionRuleDTO precondition2 = DaliBeans.findById(controlRule.getPreconditions(), 2);
        assertNotNull(precondition2);
        assertEquals(2, precondition2.getId().intValue());
        assertTrue(precondition2.isActive());
        assertFalse(precondition2.isBidirectional());
        assertEquals("SALMOI2", precondition2.getBaseRule().getCode());
        assertEquals("3", precondition2.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO2", precondition2.getUsedRule().getCode());
        assertEquals("5,6", precondition2.getUsedRule().getAllowedValues());

        // modify the last value
        precondition2.getUsedRule().setAllowedValues("5");

        // save
        ruleListDao.saveRuleList(ruleList, dbResource.getFixtures().getUserIdWithDataForSynchro());

        // reload
        RuleListDTO reloadedRuleList = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(1));
        assertNotNull(reloadedRuleList);
        assertEquals("rule list with preconditions", reloadedRuleList.getDescription());

        assertEquals(1, reloadedRuleList.sizeControlRules());
        ControlRuleDTO reloadedControlRule = reloadedRuleList.getControlRules(0);
        assertTrue(reloadedControlRule.isActive());
        assertEquals("PRECOND1", reloadedControlRule.getCode());
        assertTrue(ControlFunctionValues.PRECONDITION.equals(reloadedControlRule.getFunction()));
        assertTrue(ControlElementValues.MEASUREMENT.equals(reloadedControlRule.getControlElement()));
        assertTrue(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(reloadedControlRule.getControlFeature()));

        assertEquals(2, reloadedControlRule.sizeRulePmfms());
        assertEquals(4, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRule.getRulePmfms(0).getPmfm()).intValue());
        assertEquals(24, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRule.getRulePmfms(1).getPmfm()).intValue());

        assertEquals(2, reloadedControlRule.sizePreconditions());
        PreconditionRuleDTO reloadedPrecondition1 = DaliBeans.findById(reloadedControlRule.getPreconditions(), 1);
        assertNotNull(reloadedPrecondition1);
        assertEquals(1, reloadedPrecondition1.getId().intValue());
        assertTrue(reloadedPrecondition1.isActive());
        assertTrue(reloadedPrecondition1.isBidirectional());
        assertEquals("SALMOI1", reloadedPrecondition1.getBaseRule().getCode());
        assertEquals("2", reloadedPrecondition1.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO1", reloadedPrecondition1.getUsedRule().getCode());
        assertEquals("4", reloadedPrecondition1.getUsedRule().getAllowedValues());

        PreconditionRuleDTO reloadedPrecondition2 = DaliBeans.findById(reloadedControlRule.getPreconditions(), 2);
        assertNotNull(reloadedPrecondition2);
        assertEquals(2, reloadedPrecondition2.getId().intValue());
        assertTrue(reloadedPrecondition2.isActive());
        assertFalse(reloadedPrecondition2.isBidirectional());
        assertEquals("SALMOI2", reloadedPrecondition2.getBaseRule().getCode());
        assertEquals("3", reloadedPrecondition2.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO2", reloadedPrecondition2.getUsedRule().getCode());
        assertEquals("5", reloadedPrecondition2.getUsedRule().getAllowedValues());

        // remove a precondition
        reloadedControlRule.removePreconditions(reloadedPrecondition1);

        // save
        ruleListDao.saveRuleList(reloadedRuleList, dbResource.getFixtures().getUserIdWithDataForSynchro());

        // reload
        RuleListDTO reloadedRuleListBis = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(1));
        assertNotNull(reloadedRuleListBis);
        assertEquals("rule list with preconditions", reloadedRuleListBis.getDescription());

        assertEquals(1, reloadedRuleListBis.sizeControlRules());
        ControlRuleDTO reloadedControlRuleBis = reloadedRuleListBis.getControlRules(0);
        assertTrue(reloadedControlRuleBis.isActive());
        assertEquals("PRECOND1", reloadedControlRuleBis.getCode());
        assertTrue(ControlFunctionValues.PRECONDITION.equals(reloadedControlRuleBis.getFunction()));
        assertTrue(ControlElementValues.MEASUREMENT.equals(reloadedControlRuleBis.getControlElement()));
        assertTrue(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(reloadedControlRuleBis.getControlFeature()));

        assertEquals(2, reloadedControlRuleBis.sizeRulePmfms());
        assertEquals(4, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRuleBis.getRulePmfms(0).getPmfm()).intValue());
        assertEquals(24, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRuleBis.getRulePmfms(1).getPmfm()).intValue());

        assertEquals(1, reloadedControlRuleBis.sizePreconditions());
        PreconditionRuleDTO reloadedPrecondition2Bis = DaliBeans.findById(reloadedControlRuleBis.getPreconditions(), 2);
        assertNotNull(reloadedPrecondition2Bis);
        assertEquals(2, reloadedPrecondition2Bis.getId().intValue());
        assertTrue(reloadedPrecondition2Bis.isActive());
        assertFalse(reloadedPrecondition2Bis.isBidirectional());
        assertEquals("SALMOI2", reloadedPrecondition2Bis.getBaseRule().getCode());
        assertEquals("3", reloadedPrecondition2Bis.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO2", reloadedPrecondition2Bis.getUsedRule().getCode());
        assertEquals("5", reloadedPrecondition2Bis.getUsedRule().getAllowedValues());

    }

}
