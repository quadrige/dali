package fr.ifremer.dali.dao.data.measurement;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dao.referential.pmfm.DaliPmfmDao;
import fr.ifremer.dali.dao.referential.pmfm.DaliQualitativeValueDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonGroupDao;
import fr.ifremer.dali.dao.referential.taxon.DaliTaxonNameDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.measurement.MeasurementDTO;
import fr.ifremer.dali.dto.referential.TaxonDTO;
import fr.ifremer.dali.dto.referential.TaxonGroupDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import org.apache.commons.collections4.ListUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MeasurementDaoWriteTest extends AbstractDaoTest {

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();

    private DaliPmfmDao pmfmDao;
    private DaliMeasurementDao measurementDao;
    private DaliTaxonGroupDao taxonGroupDao;
    private DaliTaxonNameDao taxonNameDao;
    private DaliQualitativeValueDao qualitativeValueDao;

    private static final int SURVEY_ID = 101;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        pmfmDao = DaliServiceLocator.instance().getService("daliPmfmDao", DaliPmfmDao.class);
        measurementDao = DaliServiceLocator.instance().getService("daliMeasurementDao", DaliMeasurementDao.class);
        taxonGroupDao = DaliServiceLocator.instance().getService("daliTaxonGroupDao", DaliTaxonGroupDao.class);
        taxonNameDao = DaliServiceLocator.instance().getService("daliTaxonNameDao", DaliTaxonNameDao.class);
        qualitativeValueDao = DaliServiceLocator.instance().getService("daliQualitativeValueDao", DaliQualitativeValueDao.class);
        setCommitOnTearDown(true);
    }

    @Test
    public void createUpdateSurveyMeasurement() {

        // load taxons caches first to avoid exception on taxon_name 17 who is an obsolete referent
        taxonGroupDao.getAllTaxonGroups();

        List<MeasurementDTO> measurements = Lists.newArrayList();

        // non individual pmfm with numerical value
        PmfmDTO pmfm1 = pmfmDao.getPmfmById(11);
        assertNotNull(pmfm1);
        MeasurementDTO m1 = DaliBeanFactory.newMeasurementDTO();
        m1.setNumericalValue(BigDecimal.valueOf(2.5));
        m1.setPrecision(2);
        m1.setDigitNb(1);
        m1.setComment("(m1) numeric measurement on pmfm 151");
        MeasurementDTO m2 = DaliBeanFactory.newMeasurementDTO();
        m2.setNumericalValue(BigDecimal.valueOf(1.28));
        m2.setPrecision(3);
        m2.setDigitNb(2);
        m2.setComment("(m2) numeric measurement 2 on pmfm 151");
        m1.setPmfm(pmfm1);
        m2.setPmfm(pmfm1);
        measurements.add(m1);
        measurements.add(m2);

        // non individual pmfm with qualitative value
        PmfmDTO pmfm2 = pmfmDao.getPmfmById(4);
        assertNotNull(pmfm2);
        MeasurementDTO m3 = DaliBeanFactory.newMeasurementDTO();
        assertEquals(1, pmfm2.sizeQualitativeValues());
        QualitativeValueDTO qualVal1 = pmfm2.getQualitativeValues(0);
        assertNotNull(qualVal1);
        m3.setQualitativeValue(qualVal1);
        m3.setComment("(m3) qualitative measurement on pmfm 144");
        m3.setPmfm(pmfm2);
        measurements.add(m3);

        // individual pmfm with numerical value
        PmfmDTO pmfm3 = pmfmDao.getPmfmById(23);
        assertNotNull(pmfm3);
        MeasurementDTO m4 = DaliBeanFactory.newMeasurementDTO();
        m4.setNumericalValue(BigDecimal.valueOf(17.35)); // Value from mantis #37438
        m4.setPrecision(4);
        m4.setDigitNb(2);
        m4.setComment("(m4) numeric measurement on individual pmfm 157");
        TaxonGroupDTO taxonGroup1 = taxonGroupDao.getTaxonGroupById(3);
        assertNotNull(taxonGroup1);
        m4.setTaxonGroup(taxonGroup1);
        m4.setPmfm(pmfm3);
        m4.setIndividualId(4);
        measurements.add(m4);

        // individual pmfm with qualitative value
        PmfmDTO pmfm4 = pmfmDao.getPmfmById(24);
        assertNotNull(pmfm4);
        assertEquals(2, pmfm4.sizeQualitativeValues());
        QualitativeValueDTO qualVal2 = pmfm4.getQualitativeValues(0);
        assertNotNull(qualVal2);
        MeasurementDTO m5 = DaliBeanFactory.newMeasurementDTO();
        m5.setQualitativeValue(qualVal2);
        m5.setComment("(m5) qualitative measurement 1 on pmfm 158");
        TaxonDTO taxon1 = taxonNameDao.getTaxonNameByReferenceId(7);
        assertNotNull(taxon1);
        m5.setTaxon(taxon1);
        m5.setPmfm(pmfm4);
        m5.setIndividualId(6);
        measurements.add(m5);

        MeasurementDTO m6 = DaliBeanFactory.newMeasurementDTO();
        QualitativeValueDTO qualVal3 = pmfm4.getQualitativeValues(1);
        assertNotNull(qualVal3);
        m6.setQualitativeValue(qualVal3);
        m6.setComment("(m6) qualitative measurement 2 on pmfm 158");
        TaxonGroupDTO taxonGroup2 = taxonGroupDao.getTaxonGroupById(9);
        assertNotNull(taxonGroup2);
        m6.setTaxonGroup(taxonGroup2);
        List<TaxonDTO> taxons = (List<TaxonDTO>) taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now()).get(taxonGroup2.getId());
        assertNotNull(taxons);
        assertTrue(taxons.size() > 0);
        TaxonDTO taxon2 = taxons.get(0);
        m6.setTaxon(taxon2);
        m6.setPmfm(pmfm4);
        m6.setIndividualId(7);
        measurements.add(m6);

        // save all
        measurementDao.saveMeasurementsBySurveyId(SURVEY_ID, measurements);

        // read all
        List<MeasurementDTO> reloadedMeasurements = measurementDao.getMeasurementsBySurveyId(SURVEY_ID);
        assertNotNull(reloadedMeasurements);

        // compare
        assertMeasurementsEquals(measurements, reloadedMeasurements);

        // update some values
        m1.setNumericalValue(BigDecimal.valueOf(12.3));
        m1.setDigitNb(null);
        m2.setPrecision(null);
        m2.setNumericalValue(null);
        m3.setQualitativeValue(qualitativeValueDao.getQualitativeValuesByParameterCode(pmfm2.getParameter().getCode()).get(1)); // set a qualitative value not in pmfm
        m3.setComment(null);
        m4.setComment("(m4) new comment");
        m4.setTaxonGroup(taxonGroupDao.getTaxonGroupById(4));
        m5.setQualitativeValue(qualitativeValueDao.getQualitativeValuesByParameterCode(pmfm4.getParameter().getCode()).get(1)); // set a qualitative value not in pmfm
        m5.setTaxonGroup(null);
        m6.setTaxon(null);
        m6.setQualitativeValue(null);

        // save all again
        measurementDao.saveMeasurementsBySurveyId(SURVEY_ID, measurements);

        // read all again
        reloadedMeasurements = measurementDao.getMeasurementsBySurveyId(SURVEY_ID);
        assertNotNull(reloadedMeasurements);

        // compare measurements size (m2 & m6 should be deleted)
        assertEquals(measurements.size() - 2 /* == 4 */, reloadedMeasurements.size());

        // remove m3 and m4
        // use remove by index because remove by object use the dto hashCode (m1.id=m4.id, m2.id=m5.id, m3.id=m6.id)
        measurements.remove(2);
        // save all again
        measurementDao.saveMeasurementsBySurveyId(SURVEY_ID, measurements);
        // read all again
        reloadedMeasurements = measurementDao.getMeasurementsBySurveyId(SURVEY_ID);
        assertNotNull(reloadedMeasurements);
        assertEquals(measurements.size() - 2 /* == 3 */, reloadedMeasurements.size());

        // delete some measurements
        measurementDao.removeMeasurementsByIds(ImmutableList.of(m1.getId()));
        measurementDao.removeTaxonMeasurementsByIds(ImmutableList.of(m5.getId()));

        // read all again
        reloadedMeasurements = measurementDao.getMeasurementsBySurveyId(SURVEY_ID);
        assertNotNull(reloadedMeasurements);
        assertEquals(1, reloadedMeasurements.size());

    }

    private void assertMeasurementsEquals(List<MeasurementDTO> expectedMeasurements, List<MeasurementDTO> measurementsToTest) {

        assertEquals(expectedMeasurements.size(), measurementsToTest.size());

        // split into 2 lists : individual and non-individual measurements
        Predicate<MeasurementDTO> individualPredicate = input -> input.getIndividualId() != null;
        List<MeasurementDTO> expectedIndividualMeasurements = DaliBeans.filterCollection(expectedMeasurements, individualPredicate);
        List<MeasurementDTO> expectedNonIndividualMeasurements = ListUtils.removeAll(expectedMeasurements, expectedIndividualMeasurements);
        List<MeasurementDTO> individualMeasurementsToTest = DaliBeans.filterCollection(measurementsToTest, individualPredicate);
        List<MeasurementDTO> nonIndividualMeasurementsToTest = ListUtils.removeAll(measurementsToTest, individualMeasurementsToTest);

        assertEquals(expectedIndividualMeasurements.size(), individualMeasurementsToTest.size());
        assertEquals(expectedNonIndividualMeasurements.size(), nonIndividualMeasurementsToTest.size());

        // individual
        Map<Integer, MeasurementDTO> measurementToTestMap = DaliBeans.mapById(individualMeasurementsToTest);
        for (MeasurementDTO expectedMeasurement : expectedIndividualMeasurements) {
            assertNotNull(expectedMeasurement.getId());
            MeasurementDTO measurementToTest = measurementToTestMap.get(expectedMeasurement.getId());
            assertNotNull(measurementToTest);
            assertMeasurementEquals(expectedMeasurement, measurementToTest);
        }

        // non-individual
        measurementToTestMap = DaliBeans.mapById(nonIndividualMeasurementsToTest);
        for (MeasurementDTO expectedMeasurement : expectedNonIndividualMeasurements) {
            assertNotNull(expectedMeasurement.getId());
            MeasurementDTO measurementToTest = measurementToTestMap.get(expectedMeasurement.getId());
            assertNotNull(measurementToTest);
            assertMeasurementEquals(expectedMeasurement, measurementToTest);
        }

    }

    private void assertMeasurementEquals(MeasurementDTO expectedMeasurement, MeasurementDTO measurementToTest) {
        assertTrue(expectedMeasurement != measurementToTest);
        assertEquals(expectedMeasurement.getPmfm(), measurementToTest.getPmfm());
        assertEquals(expectedMeasurement.getId(), measurementToTest.getId());
        assertEquals(expectedMeasurement.getNumericalValue(), measurementToTest.getNumericalValue());
        assertEquals(expectedMeasurement.getPrecision(), measurementToTest.getPrecision());
        assertEquals(expectedMeasurement.getDigitNb(), measurementToTest.getDigitNb());
        assertEquals(expectedMeasurement.getControlDate(), measurementToTest.getControlDate());
        assertEquals(expectedMeasurement.getValidationDate(), measurementToTest.getValidationDate());
        assertEquals(expectedMeasurement.getQualificationDate(), measurementToTest.getQualificationDate());
        assertEquals(expectedMeasurement.getQualificationComment(), measurementToTest.getQualificationComment());
        assertEquals(expectedMeasurement.getTaxonGroup(), measurementToTest.getTaxonGroup());
        assertEquals(expectedMeasurement.getTaxon(), measurementToTest.getTaxon());
        assertEquals(expectedMeasurement.getComment(), measurementToTest.getComment());
        assertEquals(expectedMeasurement.getQualitativeValue(), measurementToTest.getQualitativeValue());
        assertEquals(expectedMeasurement.getIndividualId(), measurementToTest.getIndividualId());
    }

    @Test
    public void deleteAllMeasurements() {
        measurementDao.removeAllMeasurementsBySurveyId(SURVEY_ID);
    }

}
