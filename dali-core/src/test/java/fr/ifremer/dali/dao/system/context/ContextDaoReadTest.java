package fr.ifremer.dali.dao.system.context;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dto.configuration.context.ContextDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.List;

import static org.junit.Assert.*;

public class ContextDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(ContextDaoReadTest.class);

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.readDb();

    private DaliContextDao contextDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        contextDao = DaliServiceLocator.instance().getService("daliContextDao", DaliContextDao.class);
    }

    @Test
    public void getContext() {

        List<ContextDTO> localContexts = contextDao.getAllContext();
        assertNotNull(localContexts);
        assertEquals(2, localContexts.size());
        if (log.isDebugEnabled()) {
            for (ContextDTO dto : localContexts) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        ContextDTO localContext = contextDao.getContextById(1001);
        assertNotNull(localContext);
        assertEquals("context local 1", localContext.getName());
        
        try {
            contextDao.getContextById(10);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }

    }
    
}
