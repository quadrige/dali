package fr.ifremer.dali.dao.data.survey;

import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dao.administration.user.DaliQuserDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.data.survey.CampaignDTO;
import fr.ifremer.dali.dto.referential.PersonDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

public class CampaignDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(CampaignDaoWriteTest.class);

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();

    private DaliCampaignDao campaignDao;
    private DaliQuserDao quserDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        campaignDao = DaliServiceLocator.instance().getService("daliCampaignDao", DaliCampaignDao.class);
        quserDao = DaliServiceLocator.instance().getService("daliQuserDao", DaliQuserDao.class);
    }

    @Test
    public void saveCampaign() {
        CampaignDTO campaign = DaliBeanFactory.newCampaignDTO();
        String testName = "campaign test";
        campaign.setName(testName);
        LocalDate testDate = LocalDate.of(2019, 1, 1);
        campaign.setStartDate(testDate);
        PersonDTO testUser = quserDao.getUserById(dbResource.getFixtures().getUserIdWithDataForSynchro());
        campaign.setManager(testUser);
        campaign.setDirty(true);

        campaignDao.saveCampaign(campaign);
        Assert.assertNotNull(campaign.getId());

        List<CampaignDTO> campaigns = campaignDao.getCampaignsByName(testName);
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(1, campaigns.size());
        campaign = campaigns.get(0);

        Assert.assertEquals(testName, campaign.getName());
        Assert.assertEquals(testDate, campaign.getStartDate());
        Assert.assertNull(campaign.getEndDate());
        Assert.assertNotNull(campaign.getManager());
        Assert.assertEquals(dbResource.getFixtures().getUserIdWithDataForSynchro(), campaign.getManager().getId().intValue());
        Assert.assertNotNull(campaign.getRecorderDepartment());
        Assert.assertEquals(2, campaign.getRecorderDepartment().getId().intValue());

    }

}
