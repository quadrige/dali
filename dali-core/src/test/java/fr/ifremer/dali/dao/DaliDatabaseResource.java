package fr.ifremer.dali.dao;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.config.DaliConfigurationOption;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.core.service.ServiceLocator;
import fr.ifremer.quadrige3.core.test.DatabaseResource;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.List;

public class DaliDatabaseResource extends DatabaseResource {

    public static final String MODULE_NAME = "dali-core";
    public static final String HSQLDB_SRC_DATABASE_DIRECTORY = String.format(HSQLDB_SRC_DATABASE_DIRECTORY_PATTERN, MODULE_NAME);

    private final DaliDatabaseFixtures fixtures = new DaliDatabaseFixtures();

    public static DaliDatabaseResource readDb() {
        return new DaliDatabaseResource("");
    }

    public static DaliDatabaseResource readDb(String dbName, boolean enableServices) {
        if (!enableServices) {
            return new DaliDatabaseResource(dbName, "beanRefFactoryWithNoService.xml", "beanRefFactoryWithNoService", false);
        }
        return new DaliDatabaseResource(dbName);
    }

    public static DaliDatabaseResource writeDb() {
        return new DaliDatabaseResource("", true);
    }

    public static DaliDatabaseResource writeDb(String dbName) {
        return new DaliDatabaseResource(dbName, true);
    }

    public static DaliDatabaseResource writeDb(String dbName, boolean enableServices) {
        if (!enableServices) {
            return new DaliDatabaseResource(dbName, "beanRefFactoryWithNoService.xml", "beanRefFactoryWithNoService", true);
        }
        return new DaliDatabaseResource(dbName, true);
    }

    public static DaliDatabaseResource noDb() {
        return new DaliDatabaseResource(
                "", "beanRefFactoryWithNoDb.xml", "beanRefFactoryWithNoDb");
    }

    protected DaliDatabaseResource(String dbName) {
        super(dbName, null, null, false);
    }

    protected DaliDatabaseResource(String dbName, boolean writeDb) {
        super(dbName, null, null, writeDb);
    }

    protected DaliDatabaseResource(String dbName, String beanFactoryReferenceLocation,
                                   String beanRefFactoryReferenceId) {
        super(dbName, beanFactoryReferenceLocation, beanRefFactoryReferenceId, false);
    }

    protected DaliDatabaseResource(String dbName, String beanFactoryReferenceLocation,
                                   String beanRefFactoryReferenceId,
                                   boolean writeDb) {
        super(dbName, beanFactoryReferenceLocation, beanRefFactoryReferenceId, writeDb);
    }

    @Override
    protected String getConfigFilesPrefix() {
        return MODULE_NAME + "-test";
    }

    @Override
    protected String getI18nBundleName() {
        return MODULE_NAME + "-i18n";
    }

    @Override
    protected String getModuleDirectory() {
        return MODULE_NAME;
    }

    @Override
    public String getBuildEnvironment() {
        return BUILD_ENVIRONMENT_DEFAULT;
    }

    public DaliDatabaseFixtures getFixtures() {
        return fixtures;
    }

    @Override
    protected void before(Description description) throws Throwable {
        // Call inherited method
        super.before(description);

        // Initialize default context
        if (getBeanFactoryReferenceLocation() == null) {
            ServiceLocator.setInstance(DaliServiceLocator.instance());
        }

        // Set Authentication (fake user)
        SecurityContextHelper.authenticate("demo", "demo");

        DaliServiceLocator.instance().getDataContext().setRecorderPersonId(1001);
        DaliServiceLocator.instance().getDataContext().setRecorderDepartmentId(2);

        log.debug(DaliConfiguration.getInstance().getJdbcUrl());
    }

    @Override
    protected String[] getConfigArgs() {
        List<String> configArgs = Lists.newArrayList(Arrays.asList(super.getConfigArgs()));
        configArgs.addAll(Lists.newArrayList(
                "--option", DaliConfigurationOption.BASEDIR.getKey(), getResourceDirectory().getAbsolutePath()
//                , "--option", QuadrigeCoreConfigurationOption.AUTHENTICATION_DISABLED.getKey(), Boolean.toString(true)
                , "--option", DaliConfigurationOption.DB_ENUMERATION_RESOURCE.getKey(), "classpath*:quadrige3-db-enumerations.properties,classpath*:dali-db-test-enumerations.properties"
        ));
        return configArgs.toArray(new String[configArgs.size()]);
    }

    /**
     * Convenience methods that could be override to initialize other configuration
     *
     * @param configFilename configuration file name
     */
    @Override
    protected void initConfiguration(String configFilename) {
        String[] configArgs = getConfigArgs();
        DaliConfiguration daliConfiguration = new DaliConfiguration(configFilename, configArgs);
        DaliConfiguration.setInstance(daliConfiguration);
    }

}
