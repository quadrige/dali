package fr.ifremer.dali.dao.system.rule;

/*
 * #%L
 * Dali :: Core
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dto.FunctionDTO;
import fr.ifremer.dali.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.dali.dto.enums.ControlFunctionValues;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;


/**
 * @author peck7 on 26/10/2017.
 */
public class RuleDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(RuleDaoReadTest.class);

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.readDb();

    private DaliRuleDao ruleDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        ruleDao = DaliServiceLocator.instance().getService("daliRuleDao", DaliRuleDao.class);
    }

    @Test
    public void getAllFunction() {

        List<FunctionDTO> functions = ruleDao.getAllFunction();
        Assert.assertNotNull(functions);
        Assert.assertEquals(5, functions.size());
        if (log.isDebugEnabled()) {
            for (FunctionDTO dto : functions) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
    }

    @Test
    public void getRulesByRuleListCode() {

        // RULELIST1
        MutableBoolean incompatibleRule = new MutableBoolean();
        List<ControlRuleDTO> rules = ruleDao.getRulesByRuleListCode(dbResource.getFixtures().getRuleListCode(0), false, incompatibleRule);
        Assert.assertNotNull(rules);
        // this rule list is not compatible at all (some attributes comes from old Dali referential)
        Assert.assertTrue(incompatibleRule.booleanValue());
        Assert.assertEquals(1, rules.size());

        // RULELIST2
        incompatibleRule.setFalse();
        rules = ruleDao.getRulesByRuleListCode(dbResource.getFixtures().getRuleListCode(1), false, incompatibleRule);
        Assert.assertNotNull(rules);
        Assert.assertFalse(incompatibleRule.booleanValue());
        Assert.assertEquals(0, rules.size());
        rules = ruleDao.getRulesByRuleListCode(dbResource.getFixtures().getRuleListCode(1), true, incompatibleRule);
        Assert.assertNotNull(rules);
        Assert.assertFalse(incompatibleRule.booleanValue());
        Assert.assertEquals(0, rules.size());
    }

    @Test
    public void getPreconditionedRulesByRuleListCode() {

        MutableBoolean incompatibleRule = new MutableBoolean();
        List<ControlRuleDTO> rules = ruleDao.getPreconditionedRulesByRuleListCode(dbResource.getFixtures().getRuleListCode(1), false, incompatibleRule);
        Assert.assertNotNull(rules);
        Assert.assertFalse(incompatibleRule.booleanValue());
        Assert.assertEquals(1, rules.size());
        ControlRuleDTO preconditionedRule = rules.get(0);
        Assert.assertEquals("PRECOND1", preconditionedRule.getCode());
        Assert.assertTrue(ControlFunctionValues.PRECONDITION.equals(preconditionedRule.getFunction()));
        Assert.assertEquals(2, preconditionedRule.sizePreconditions());
        Assert.assertEquals(2, preconditionedRule.sizeRulePmfms());
    }
}
