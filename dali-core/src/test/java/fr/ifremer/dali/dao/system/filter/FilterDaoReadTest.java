/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.dali.dao.system.filter;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.enums.FilterTypeValues;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class FilterDaoReadTest extends AbstractDaoTest {
    
    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.readDb();

    private DaliFilterDao filterDao;
    
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        filterDao = DaliServiceLocator.instance().getService("daliFilterDao", DaliFilterDao.class);
    }
    
    @Test
    public void getAllFilter() {
        List<FilterDTO> locFilters = filterDao.getAllContextFilters(1001, FilterTypeValues.LOCATION.getFilterTypeId()); // context 1
        assertNotNull(locFilters);
        assertEquals(1, locFilters.size());
        
        locFilters = filterDao.getAllContextFilters(1002, FilterTypeValues.LOCATION.getFilterTypeId()); // context 2
        Assert.assertTrue(locFilters == null || locFilters.isEmpty());
        
        List<FilterDTO> progFilters = filterDao.getAllContextFilters(null, FilterTypeValues.PROGRAM.getFilterTypeId());
        
        assertEquals(1, progFilters.size());
        assertEquals("Program Filter", progFilters.get(0).getName());
        
    }
    
    @Test
    public void getFilterById() {
        FilterDTO f = filterDao.getFilterById(1001); // filter 1000
        Assert.assertNotNull(f);
        Assert.assertEquals(FilterTypeValues.LOCATION.getFilterTypeId(), f.getFilterTypeId());

        FilterDTO fp = filterDao.getFilterById(1002);
        Assert.assertNotNull(fp);
        Assert.assertEquals(FilterTypeValues.PROGRAM.getFilterTypeId(), fp.getFilterTypeId());

    }
    
}
