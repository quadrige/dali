package fr.ifremer.dali.dao.data.survey;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dao.administration.program.DaliProgramDao;
import fr.ifremer.dali.dao.administration.user.DaliDepartmentDao;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dao.referential.monitoringLocation.DaliMonitoringLocationDao;
import fr.ifremer.dali.dao.technical.Geometries;
import fr.ifremer.dali.dto.CoordinateDTO;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.data.survey.SurveyDTO;
import fr.ifremer.dali.dto.enums.SynchronizationStatusValues;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SurveyDaoWriteTest extends AbstractDaoTest {

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();

    private DaliSurveyDao surveyDao;
    private DaliCampaignDao campaignDao;
    private DaliProgramDao programDao;
    private DaliMonitoringLocationDao mlDao;
    private DaliReferentialDao refDao;
    private DaliDepartmentDao departmentDao;

    private Integer testSurveyId;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        surveyDao = DaliServiceLocator.instance().getService("daliSurveyDao", DaliSurveyDao.class);
        campaignDao = DaliServiceLocator.instance().getService("daliCampaignDao", DaliCampaignDao.class);
        programDao = DaliServiceLocator.instance().getService("daliProgramDao", DaliProgramDao.class);
        mlDao = DaliServiceLocator.instance().getService("daliMonitoringLocationDao", DaliMonitoringLocationDao.class);
        refDao = DaliServiceLocator.instance().getService("daliReferentialDao", DaliReferentialDao.class);
        departmentDao = DaliServiceLocator.instance().getService("daliDepartmentDao", DaliDepartmentDao.class);
        setCommitOnTearDown(true);
    }

    @Test
    public void createUpdateSaveSurvey() {

        // create a survey bean
        SurveyDTO survey = DaliBeanFactory.newSurveyDTO();

        survey.setName("test survey");
        survey.setComment("test survey comment");
        survey.setDate(LocalDate.of(2015,2,5));
        survey.setTime(1252);
        survey.setBottomDepth(15.2);
        survey.setCampaign(campaignDao.getAllCampaigns().get(0));
        survey.setProgram(programDao.getProgramByCode("REMIS"));
        survey.setOccasion(null);
        survey.setLocation(mlDao.getLocationById(1));
        survey.setPositioning(refDao.getAllPositioningSystems().get(0));
        CoordinateDTO surveyCoordinate = DaliBeanFactory.newCoordinateDTO();
        surveyCoordinate.setMinLongitude(2.3488000);
        surveyCoordinate.setMinLatitude(48.8534100);
        survey.setCoordinate(surveyCoordinate);
        survey.setRecorderDepartment(departmentDao.getDepartmentById(1));
        survey.setSynchronizationStatus(SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO());

        SurveyDTO surveyBase = DaliBeans.clone(survey);

        // save
        survey = surveyDao.save(survey);

        assertNotNull(survey);
        assertNotNull(survey.getId());
        testSurveyId = survey.getId();

        // assert all values after save
        assertSurveyEquals(surveyBase, survey, false);

        // reload
        survey = getSurveyTest();
        assertNotNull(survey);

        // assert all values
        assertSurveyEquals(surveyBase, survey, false);

        // modify all values
        survey.setName("test survey 2");
        survey.setComment("test survey comment 2");
        survey.setDate(LocalDate.of(2015,1,1));
        survey.setTime(1252);
        survey.setBottomDepth(15.2);
        survey.setControlDate(Date.valueOf("2015-03-08"));
//        survey.setValidationDate(Date.valueOf("2015-04-16"));
//        survey.setQualificationComment("validation date comment 2");
        survey.setCampaign(campaignDao.getAllCampaigns().get(1));
        survey.setProgram(programDao.getProgramByCode("REBENT"));
        survey.setOccasion(null);
        survey.setLocation(mlDao.getLocationById(2));
        survey.setPositioning(refDao.getAllPositioningSystems().get(1));
        surveyCoordinate.setMinLongitude(2.3488000);
        surveyCoordinate.setMinLatitude(48.8534100);
        surveyCoordinate.setMaxLongitude(2.3555000);
        surveyCoordinate.setMaxLatitude(48.8539200);
        survey.setCoordinate(surveyCoordinate);
        survey.setRecorderDepartment(departmentDao.getDepartmentById(3));
        survey.setSynchronizationStatus(SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO());

        surveyBase = DaliBeans.clone(survey);

        // update
        survey = surveyDao.save(survey);

        assertNotNull(survey);
        assertNotNull(survey.getId());
        assertEquals(testSurveyId, survey.getId());

        // assert all values before save
        assertSurveyEquals(surveyBase, survey, false);

        // reload
        survey = getSurveyTest();
        assertNotNull(survey);

        // assert all values
        assertSurveyEquals(surveyBase, survey, true);

        /*
         * DETAILED PART
         */
        SurveyDTO surveyDetail = surveyDao.getSurveyById(testSurveyId, false);
        assertNotNull(surveyDetail);
        assertEquals(testSurveyId, surveyDetail.getId()); // ça c'est du test ^^

        // check base fields
        assertSurveyEquals(surveyBase, surveyDetail, true);

        // set other detailed fields
        surveyDetail.setPositioningComment("positioning comment");

        // coordinate
        surveyDetail.setCoordinate(null);
        surveyDetail.setPositioning(null);

        // clone
        SurveyDTO surveyDetailBase = DaliBeans.clone(surveyDetail);

        // save
        surveyDetail = surveyDao.save(surveyDetail);
        assertNotNull(surveyDetail);
        assertNotNull(surveyDetail.getId());
        assertEquals(testSurveyId, surveyDetail.getId());

        // compare to base
        assertSurveyEquals(surveyDetailBase, surveyDetail, false);

        // reload and compare
        surveyDetail = surveyDao.getSurveyById(testSurveyId, false);

        assertSurveyEquals(surveyDetailBase, surveyDetail, false);

        // modify all fields
        surveyDetail.setName("test survey 3");
        surveyDetail.setComment("test survey comment 3");
        surveyDetail.setDate(LocalDate.of(2015,4,7));
        surveyDetail.setTime(1953);
        surveyDetail.setBottomDepth(12.90);
        surveyDetail.setControlDate(Date.valueOf("2015-05-20"));
//        surveyDetail.setValidationDate(Date.valueOf("2015-05-21"));
//        surveyDetail.setQualificationComment("validation date comment 3");
        surveyDetail.setCampaign(campaignDao.getAllCampaigns().get(2));
        surveyDetail.setProgram(programDao.getProgramByCode("RNOHYD"));
        surveyDetail.setOccasion(null);
        surveyDetail.setLocation(mlDao.getLocationById(3));
        surveyDetail.setPositioning(null);
        surveyDetail.setCoordinate(null);
        surveyDetail.setPositioningComment("positioning comment 2");

        // clone
        surveyDetailBase = DaliBeans.clone(surveyDetail);

        // save, reload and compare
        surveyDao.save(surveyDetail);
        surveyDetail = surveyDao.getSurveyById(testSurveyId, false);
        assertSurveyEquals(surveyDetailBase, surveyDetail, false);

        // some nulls
        surveyDetail.setControlDate(null);
        surveyDetail.setPositioningComment(null);

        // clone, save, reload and compare
        surveyDetailBase = DaliBeans.clone(surveyDetail);
        surveyDao.save(surveyDetail);
        surveyDetail = surveyDao.getSurveyById(testSurveyId, false);
        assertSurveyEquals(surveyDetailBase, surveyDetail, false);
        
        // delete
        surveyDao.remove(Lists.newArrayList(testSurveyId));
        
        // reload survey by loading all surveys
        assertNull(getSurveyTest());
    }

    private SurveyDTO getSurveyTest() {

        // reload survey by loading all surveys
        List<SurveyDTO> surveys = surveyDao.getSurveysByCriteria(null, Arrays.asList("REMIS","RNOHYD","REBENT"), null, null, null, null, null, null, null, true);
        assertNotNull(surveys);

        SurveyDTO survey = null;
        for (SurveyDTO s : surveys) {
            if (testSurveyId.equals(s.getId())) {
                survey = s;
                break;
            }
        }
        return survey;
    }

    private void assertSurveyEquals(SurveyDTO surveyBase, SurveyDTO surveyToControl, boolean exceptDepartment) {

        assertEquals(surveyBase.getName(), surveyToControl.getName());
        assertEquals(surveyBase.getComment(), surveyToControl.getComment());
        assertEquals(surveyBase.getDate(), surveyToControl.getDate());
        assertEquals(surveyBase.getTime(), surveyToControl.getTime());
        assertEquals(surveyBase.getBottomDepth(), surveyToControl.getBottomDepth());
        assertEquals(surveyBase.getControlDate(), surveyToControl.getControlDate());
        assertEquals(surveyBase.getValidationDate(), surveyToControl.getValidationDate());
        assertEquals(surveyBase.getQualificationComment(), surveyToControl.getQualificationComment());
        assertEquals(surveyBase.getCampaign(), surveyToControl.getCampaign());
        assertEquals(surveyBase.getProgram(), surveyToControl.getProgram());
        assertEquals(surveyBase.getOccasion(), surveyToControl.getOccasion());
        assertEquals(surveyBase.getLocation(), surveyToControl.getLocation());
        assertEquals(surveyBase.getPositioning(), surveyToControl.getPositioning());
        assertEquals(surveyBase.getPositioningComment(), surveyToControl.getPositioningComment());
        assertCoordinateEquals(surveyBase.getCoordinate(), surveyToControl.getCoordinate());
        assertEquals(surveyBase.getSynchronizationStatus(), surveyToControl.getSynchronizationStatus());

        if (exceptDepartment) {
            assertNotEquals(surveyBase.getRecorderDepartment(), surveyToControl.getRecorderDepartment());
        } else {
            assertEquals(surveyBase.getRecorderDepartment(), surveyToControl.getRecorderDepartment());
        }

    }

    private void assertCoordinateEquals(CoordinateDTO coordinateBase, CoordinateDTO coordinateToControl) {
        if (coordinateBase == null) {
            assertFalse(Geometries.isValid(coordinateToControl));
            return;
        } else {
            assertNotNull(coordinateToControl);
        }
        assertTrue(Geometries.equals(coordinateBase, coordinateToControl));
    }

}
