package fr.ifremer.dali.service.extraction;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dao.system.extraction.DaliExtractionResultDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.filter.FilterDTO;
import fr.ifremer.dali.dto.enums.ExtractionFilterTypeValues;
import fr.ifremer.dali.dto.enums.ExtractionOutputType;
import fr.ifremer.dali.dto.system.extraction.ExtractionDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionParameterDTO;
import fr.ifremer.dali.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.dali.dto.system.extraction.PmfmPresetDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.service.DaliTechnicalException;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.service.administration.program.ProgramStrategyService;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.technical.xmlQuery.XMLQuery;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Extraction Service Test Class
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public class ExtractionServiceTest {

    private static final Log LOG = LogFactory.getLog(ExtractionServiceTest.class);

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();

    private ExtractionService service;

    private ExtractionPerformService performService;

    private ProgramStrategyService programStrategyService;

    private ReferentialService referentialService;

    private DaliConfiguration config;

    private XMLQuery xmlQuery;

    private DaliExtractionResultDao resultDao;

    @Before
    public void setUp() {
        service = DaliServiceLocator.instance().getExtractionService();
        performService = DaliServiceLocator.instance().getExtractionPerformService();
        programStrategyService = DaliServiceLocator.instance().getProgramStrategyService();
        referentialService = DaliServiceLocator.instance().getReferentialService();
        config = DaliConfiguration.getInstance();
        xmlQuery = DaliServiceLocator.instance().getService("XMLQuery", XMLQuery.class);
        resultDao = DaliServiceLocator.instance().getService("daliExtractionResultDao", DaliExtractionResultDao.class);
    }

    @Test
    public void createExtraction() {

        ExtractionDTO extraction = DaliBeanFactory.newExtractionDTO();
        extraction.setName("extraction test");

        // add periods
        ExtractionPeriodDTO period1 = DaliBeanFactory.newExtractionPeriodDTO();
        period1.setStartDate(LocalDate.of(2015,1,1));
        period1.setEndDate(LocalDate.of(2015,12,31));
        ExtractionPeriodDTO period2 = DaliBeanFactory.newExtractionPeriodDTO();
        period2.setStartDate(LocalDate.of(2014,1,1));
        period2.setEndDate(LocalDate.of(2014,12,31));
        FilterDTO periodFilter = DaliBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterTypeValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(ImmutableList.of(period1, period2));
        extraction.addFilters(periodFilter);

        // add filters
        FilterDTO filter1 = DaliBeanFactory.newFilterDTO();
        filter1.setFilterTypeId(ExtractionFilterTypeValues.PROGRAM.getFilterTypeId());
        filter1.setElements(programStrategyService.getWritablePrograms()); // 2 programs
        filter1.setFilterLoaded(true);
        extraction.addFilters(filter1);

        // add grouping
        FilterDTO groupingFilter = DaliBeanFactory.newFilterDTO();
        groupingFilter.setFilterTypeId(ExtractionFilterTypeValues.ORDER_ITEM_TYPE.getFilterTypeId());
        groupingFilter.setElements(Collections.singletonList(referentialService.getGroupingTypes().get(0)));
        groupingFilter.setFilterLoaded(true);
        extraction.addFilters(groupingFilter);

        ExtractionDTO reloadedExtraction = saveAndReload(extraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // modify date range
        period1.setStartDate(LocalDate.of(2015,3,2));
        period1.setEndDate(LocalDate.of(2015,8,28));
        reloadedExtraction = saveAndReload(extraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // add another filter
        FilterDTO filter2 = DaliBeanFactory.newFilterDTO();
        filter2.setFilterTypeId(ExtractionFilterTypeValues.PMFM.getFilterTypeId());
        filter2.setElements(referentialService.getPmfms(StatusFilter.ACTIVE).subList(2, 5)); // 3 pmfms
        filter2.setFilterLoaded(true);
        extraction.addFilters(filter2);

        reloadedExtraction = saveAndReload(extraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // remove first filter
        extraction.removeFilters(filter2);
        reloadedExtraction = saveAndReload(extraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // delete extraction
        service.deleteExtractions(Lists.newArrayList(extraction.getId()));
    }

    @Test
    public void performExtraction() {

        // create extraction
        ExtractionDTO extraction = DaliBeanFactory.newExtractionDTO();
        extraction.setId(-2);
        extraction.setName("extraction test");

        // add periods
        ExtractionPeriodDTO period = DaliBeanFactory.newExtractionPeriodDTO();
        period.setStartDate(LocalDate.of(2014,1,1));
        period.setEndDate(LocalDate.of(2016,1,1));
        FilterDTO periodFilter = DaliBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterTypeValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(Collections.singletonList(period));
        extraction.addFilters(periodFilter);

        // add grouping
        FilterDTO groupingFilter = DaliBeanFactory.newFilterDTO();
        groupingFilter.setFilterTypeId(ExtractionFilterTypeValues.ORDER_ITEM_TYPE.getFilterTypeId());
        groupingFilter.setElements(Collections.singletonList(referentialService.getGroupingTypes().get(0)));
        groupingFilter.setFilterLoaded(true);
        extraction.addFilters(groupingFilter);

        // add filters
        FilterDTO filter1 = DaliBeanFactory.newFilterDTO();
        filter1.setFilterTypeId(ExtractionFilterTypeValues.PROGRAM.getFilterTypeId());
        filter1.setElements(ImmutableList.of(programStrategyService.getWritableProgramByCode("REMIS")));
        filter1.setFilterLoaded(true);
        extraction.addFilters(filter1);

        // add another filter
        FilterDTO filter2 = DaliBeanFactory.newFilterDTO();
        filter2.setFilterTypeId(ExtractionFilterTypeValues.LOCATION.getFilterTypeId());
        filter2.setElements(referentialService.getLocations(StatusFilter.ALL));
        filter2.setFilterLoaded(true);
        extraction.addFilters(filter2);

        // add parameter
        ExtractionParameterDTO parameter = DaliBeanFactory.newExtractionParameterDTO();
        parameter.setFillZero(true);
        PmfmPresetDTO preset1 = DaliBeanFactory.newPmfmPresetDTO();
        preset1.setPmfm(referentialService.getPmfm(4));
        preset1.addQualitativeValues(referentialService.getQualitativeValue(2));
        preset1.addQualitativeValues(referentialService.getQualitativeValue(3));
        PmfmPresetDTO preset2 = DaliBeanFactory.newPmfmPresetDTO();
        preset2.setPmfm(referentialService.getPmfm(24));
        preset2.addQualitativeValues(referentialService.getQualitativeValue(4));
        preset2.addQualitativeValues(referentialService.getQualitativeValue(5));
        preset2.addQualitativeValues(referentialService.getQualitativeValue(6));
        parameter.setPmfmPresets(ImmutableList.of(preset1, preset2));
        // result pmfm
        parameter.setPmfmResults(ImmutableList.of(referentialService.getPmfm(1)));

        extraction.setParameter(parameter);

        // perform
        File outputFile1 = new File(config.getTempDirectory(), "extraction1.csv");
        performService.performExtraction(extraction, ExtractionOutputType.AGGREGATED_STANDARD, outputFile1, new ProgressionCoreModel());
        assertCsvFileNbLines(outputFile1, 4); // 1 header + 3 results

        // add pmfm filter
        FilterDTO filter3 = DaliBeanFactory.newFilterDTO();
        filter3.setFilterTypeId(ExtractionFilterTypeValues.PMFM.getFilterTypeId());
        filter3.setElements(Lists.<QuadrigeBean>newArrayList(referentialService.getPmfm(21))); // only 21
        filter3.setFilterLoaded(true);
        extraction.addFilters(filter3);

        File outputFile2 = new File(config.getTempDirectory(), "extraction2.csv");
        performService.performExtraction(extraction, ExtractionOutputType.AGGREGATED_STANDARD, outputFile2, new ProgressionCoreModel());
        assertCsvFileNbLines(outputFile2, 3); // 1 header + 2 results

        // add taxon group filter
        FilterDTO filter4 = DaliBeanFactory.newFilterDTO();
        filter4.setFilterTypeId(ExtractionFilterTypeValues.TAXON_GROUP.getFilterTypeId());
        filter4.setElements(Lists.<QuadrigeBean>newArrayList(referentialService.getTaxonGroup(1))); // only Groupe trophique benthos
        filter4.setFilterLoaded(true);
        extraction.addFilters(filter4);

        File outputFile3 = new File(config.getTempDirectory(), "extraction3.csv");
        performService.performExtraction(extraction, ExtractionOutputType.AGGREGATED_STANDARD, outputFile3, new ProgressionCoreModel());
        assertCsvFileNbLines(outputFile3, 3); // 1 header + 2 results

        // add filter taxon
        FilterDTO filter5 = DaliBeanFactory.newFilterDTO();
        filter5.setFilterTypeId(ExtractionFilterTypeValues.TAXON.getFilterTypeId());
        filter5.setElements(Lists.<QuadrigeBean>newArrayList(referentialService.getTaxon(1))); // dummy taxon
        filter5.setFilterLoaded(true);
        extraction.addFilters(filter5);

        File outputFile4 = new File(config.getTempDirectory(), "extraction4.csv");
        try {
            performService.performExtraction(extraction, ExtractionOutputType.AGGREGATED_STANDARD, outputFile4, new ProgressionCoreModel());
            fail("should throw exception");
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    /**
     * Test used with local data
     */
    @Ignore
    @Test
    public void performExtractionNew() {

        // create extraction
        ExtractionDTO extraction = DaliBeanFactory.newExtractionDTO();
        extraction.setId(-3);
        extraction.setName("extraction test (par xml)");

        // add periods
        ExtractionPeriodDTO period1 = DaliBeanFactory.newExtractionPeriodDTO();
        period1.setStartDate(LocalDate.of(2016,1,1));
        period1.setEndDate(LocalDate.of(2020,12,31));
        ExtractionPeriodDTO period2 = DaliBeanFactory.newExtractionPeriodDTO();
        period2.setStartDate(LocalDate.of(2000,1,1));
        period2.setEndDate(LocalDate.of(2010,12,31));
        FilterDTO periodFilter = DaliBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterTypeValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(ImmutableList.of(period1, period2));
        extraction.addFilters(periodFilter);

        // add grouping
        FilterDTO groupingFilter = DaliBeanFactory.newFilterDTO();
        groupingFilter.setFilterTypeId(ExtractionFilterTypeValues.ORDER_ITEM_TYPE.getFilterTypeId());
        groupingFilter.setElements(Collections.singletonList(referentialService.getGroupingTypes().get(0)));
        groupingFilter.setFilterLoaded(true);
        extraction.addFilters(groupingFilter);

        // add filters
        FilterDTO filter1 = DaliBeanFactory.newFilterDTO();
        filter1.setFilterTypeId(ExtractionFilterTypeValues.PROGRAM.getFilterTypeId());
        filter1.setElements(ImmutableList.of(programStrategyService.getWritableProgramByCode("DECHETS_PLAGES")));
        filter1.setFilterLoaded(true);
        extraction.addFilters(filter1);

        // add another filter
//        FilterDTO filter2 = DaliBeanFactory.newFilterDTO();
//        filter2.setFilterTypeId(ExtractionFilterTypeValues.LOCATION.getFilterTypeId());
//        filter2.setElements(referentialService.getLocations(StatusFilter.ALL));
//        filter2.setFilterLoaded(true);
//        extraction.addFilters(filter2);

        // add parameter
        ExtractionParameterDTO parameter = DaliBeanFactory.newExtractionParameterDTO();
        parameter.setFillZero(false);
        PmfmPresetDTO preset1 = DaliBeanFactory.newPmfmPresetDTO();
        preset1.setPmfm(referentialService.getPmfm(13314)); // typologie
        preset1.setQualitativeValues(ImmutableList.of(
                referentialService.getQualitativeValue(60001599), // ballon
                referentialService.getQualitativeValue(60001601), // botte
                referentialService.getQualitativeValue(60001602), // gant
                referentialService.getQualitativeValue(60001603), // pneu
                referentialService.getQualitativeValue(60001610), // chaussure
                referentialService.getQualitativeValue(60001611), // vêtement
                referentialService.getQualitativeValue(60001618), // Autre cannette alimentaire, emballage
                referentialService.getQualitativeValue(60001620), // boite de conserve
                referentialService.getQualitativeValue(60001623) // cannette
        ));
        PmfmPresetDTO preset2 = DaliBeanFactory.newPmfmPresetDTO();
        preset2.setPmfm(referentialService.getPmfm(13322)); // categorie
        preset2.setQualitativeValues(ImmutableList.of(
                referentialService.getQualitativeValue(60001580), // caoutchouc
                referentialService.getQualitativeValue(60001586), // divers
                referentialService.getQualitativeValue(60001589) // métal
        ));
        List<PmfmPresetDTO> pmfmPresets = ImmutableList.of(preset1, preset2);
        parameter.setPmfmPresets(pmfmPresets);
        // result pmfm
        parameter.setPmfmResults(ImmutableList.of(referentialService.getPmfm(13171))); // nb dechet

        extraction.setParameter(parameter);

        // output
        File outputFile1 = new File("target", "extractionViaXML-" + System.currentTimeMillis() + ".csv");
        performService.performExtraction(extraction, ExtractionOutputType.AGGREGATED_COMPLETE, outputFile1, new ProgressionCoreModel());

    }

    @Test
    public void countDataWithXMLQuery() {

        xmlQuery.setQuery(getXMLQueryFile("countFrom"));
        xmlQuery.bind("tableName", "SURVEY");

        xmlQuery.addWhere(getXMLQueryFile("testWhere"));
        xmlQuery.bind("condition", "1=1");

        String query = xmlQuery.getSQLQueryAsString();

        Long count = resultDao.queryCount(query, null);

        Assert.assertNotNull(count);

        System.out.println("result count = " + count);

    }

    private File getXMLQueryFile(String queryName) {

        URL fileURL = getClass().getClassLoader().getResource("xmlQuery/extraction/" + queryName + ".xml");
        try {
            return new File(Objects.requireNonNull(fileURL).toURI());
        } catch (URISyntaxException | NullPointerException e) {
            throw new DaliTechnicalException(String.format("query '%s' not found in resources", queryName));
        }

    }

    private void assertCsvFileNbLines(File csvFile, int expectedNbLines) {

        assertTrue(csvFile.canRead());
        try {
            BufferedReader reader = Files.newBufferedReader(csvFile.toPath(), StandardCharsets.UTF_8);
            int nbLines = 0;
            String line = readLine(reader);
            while (line != null) {
                nbLines++;
                line = readLine(reader);
            }
            assertEquals(expectedNbLines, nbLines);

        } catch (IOException e) {
            fail(e.getLocalizedMessage());
        }
    }

    private String readLine(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        if (LOG.isDebugEnabled()) {
            LOG.debug(line);
        }
        return line;
    }

    private ExtractionDTO saveAndReload(ExtractionDTO extraction) {
        extraction.setDirty(true);
        service.saveExtractions(Lists.newArrayList(extraction));

        assertNotNull(extraction.getId());
        assertFalse(extraction.isDirty());

        // reload
        List<ExtractionDTO> extractions = service.getExtractions(extraction.getId(), null);

        assertNotNull(extractions);
        assertEquals(1, extractions.size());

        ExtractionDTO reloadedExtraction = extractions.get(0);
        service.loadFilteredElements(reloadedExtraction);

        return reloadedExtraction;
    }

    private void assertExtractionEquals(ExtractionDTO expectedExtraction, ExtractionDTO extractionToTest) {
        assertNotNull(expectedExtraction);
        assertNotNull(extractionToTest);
        assertEquals(expectedExtraction.getId(), extractionToTest.getId());
        assertEquals(expectedExtraction.getName(), extractionToTest.getName());
        assertEquals(expectedExtraction.getUser(), extractionToTest.getUser());
        assertEquals(expectedExtraction.isDirty(), extractionToTest.isDirty());
        assertFiltersEquals(expectedExtraction.getFilters(), extractionToTest.getFilters());
    }

    private void assertFiltersEquals(Collection<FilterDTO> expectedFilters, Collection<FilterDTO> filtersToTest) {
        if (CollectionUtils.isEmpty(expectedFilters)) {
            assertTrue(CollectionUtils.isEmpty(filtersToTest));
            return;
        } else {
            assertTrue(CollectionUtils.isNotEmpty(filtersToTest));
            assertEquals(expectedFilters.size(), filtersToTest.size());
        }

        Map<Integer, FilterDTO> filtersToTestMap = DaliBeans.mapByProperty(filtersToTest, FilterDTO.PROPERTY_FILTER_TYPE_ID);
        for (FilterDTO expectedFilter : expectedFilters) {
            FilterDTO filterToTest = filtersToTestMap.get(expectedFilter.getFilterTypeId());
            assertNotNull(filterToTest);
            assertEquals(expectedFilter.getFilterTypeId(), filterToTest.getFilterTypeId());
            assertEquals(expectedFilter.getName(), filterToTest.getName());

            if (CollectionUtils.isEmpty(expectedFilter.getElements())) {
                assertTrue(CollectionUtils.isEmpty(filterToTest.getElements()));
            } else {
                assertTrue(CollectionUtils.isNotEmpty(filterToTest.getElements()));
                assertEquals(expectedFilter.getElements().size(), filterToTest.getElements().size());

                QuadrigeBean[] expectedElements = expectedFilter.getElements().toArray(new QuadrigeBean[0]);
                QuadrigeBean[] elementsToTest = filterToTest.getElements().toArray(new QuadrigeBean[0]);
                Arrays.sort(expectedElements);
                Arrays.sort(elementsToTest);
                assertArrayEquals(expectedElements, elementsToTest);
            }
        }
    }

}
