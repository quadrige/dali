package fr.ifremer.dali.service.administration.program;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dao.referential.DaliReferentialDao;
import fr.ifremer.dali.dto.DaliBeanFactory;
import fr.ifremer.dali.dto.DaliBeans;
import fr.ifremer.dali.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.dali.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.dali.dto.referential.DepartmentDTO;
import fr.ifremer.dali.dto.referential.LocationDTO;
import fr.ifremer.dali.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.dali.service.StatusFilter;
import fr.ifremer.dali.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author Ludovic
 */
public class ProgramStrategyServiceWriteTest {

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();
    private static final Log log = LogFactory.getLog(ProgramStrategyServiceWriteTest.class);
    private static final String PROGRAM_CODE = "TEST";
    private ProgramStrategyService service;
    private ReferentialService referentialService;
    private DaliReferentialDao referentialDao;

    @Before
    public void setUp() throws Exception {
        service = DaliServiceLocator.instance().getProgramStrategyService();
        referentialService = DaliServiceLocator.instance().getReferentialService();
        referentialDao = DaliServiceLocator.instance().getService("daliReferentialDao", DaliReferentialDao.class);
    }

    /**
     * Test ignored because Q3-Déchets can't handle local programs
     */
    @Ignore
    @Test
    public void createProgramAndStrategies() {

        if (log.isDebugEnabled()) {
            log.debug("createProgramAndStrategies start");
        }

        ProgramDTO program = DaliBeanFactory.newProgramDTO();
        List<ProgramDTO> programs = Lists.newArrayList(program);
        program.setCode(PROGRAM_CODE);
        program.setName("program test");
        program.setComment("comment");
        program.setStatus(referentialDao.getStatusByCode(StatusCode.LOCAL_ENABLE.getValue()));

        // add locations
        program.setLocations(referentialService.getLocations(StatusFilter.ACTIVE));

        program.setDirty(true);
        service.savePrograms(programs);

        // create a strategy
        StrategyDTO strategy1 = DaliBeanFactory.newStrategyDTO();
        strategy1.setName("strategy 1 test");
        strategy1.setComment("comment");
        for (LocationDTO location : program.getLocations()) {
            AppliedStrategyDTO as = DaliBeanFactory.newAppliedStrategyDTO();
            as.setId(location.getId());
            as.setLabel(location.getLabel());
            as.setName(location.getName());
            as.setComment(location.getComment());
            strategy1.addAppliedStrategies(as);
        }
        program.addStrategies(strategy1);

        if (log.isDebugEnabled()) {
            log.debug("save a program");
        }
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        ProgramDTO reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);


        // Add a strategy with another location list , add applied periods and pmfmStrategy
        StrategyDTO strategy2 = DaliBeanFactory.newStrategyDTO();
        strategy2.setName("strategy 2 test");
        strategy2.setComment("with pmfm strategies");
        for (LocationDTO location : program.getLocations()) {
            AppliedStrategyDTO as = DaliBeanFactory.newAppliedStrategyDTO();
            as.setId(location.getId());
            as.setLabel(location.getLabel());
            as.setName(location.getName());
            as.setComment(location.getComment());
            strategy2.addAppliedStrategies(as);
        }
        // remove a location
        strategy2.getAppliedStrategies().remove(0);
        
        // add applied period
        List<DepartmentDTO> allDepartments = referentialService.getDepartments(StatusFilter.ALL);
        for (AppliedStrategyDTO location : strategy2.getAppliedStrategies()) {
            LocalDate startDate = LocalDate.of(2014,RandomUtils.nextInt(1, 13),RandomUtils.nextInt(1, 31));
            location.setStartDate(startDate);
            location.setEndDate(startDate.plusDays(40));
            location.setSamplingDepartment(allDepartments.get(RandomUtils.nextInt(0, allDepartments.size())));
            location.setAnalysisDepartment(allDepartments.get(RandomUtils.nextInt(0, allDepartments.size())));
        }
        program.addStrategies(strategy2);

        // add 10 Pmfm
        List<PmfmDTO> allPmfms = new ArrayList<>(referentialService.getPmfms(StatusFilter.ACTIVE));
        for (int i = 0; i < 10; i++) {
            PmfmStrategyDTO psfmProgStrat = DaliBeanFactory.newPmfmStrategyDTO();
            psfmProgStrat.setPmfm(allPmfms.remove(RandomUtils.nextInt(0, allPmfms.size())));
            psfmProgStrat.setSurvey(RandomUtils.nextInt(0, 2) == 1);
            psfmProgStrat.setSampling(RandomUtils.nextInt(0, 2) == 1);
            psfmProgStrat.setGrouping(RandomUtils.nextInt(0, 2) == 1);
            psfmProgStrat.setUnique(RandomUtils.nextInt(0, 2) == 1);

            strategy2.addPmfmStrategies(psfmProgStrat);
        }
        strategy2.setPmfmStrategiesLoaded(true);

        // change program property
        program.setComment("with a second strategy with 10 pmfm strategies");

        if (log.isDebugEnabled()) {
            log.debug("update program with pmfm strategies");
        }
        // save
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);


        // update/remove some properties
        strategy1.getAppliedStrategies().remove(2);
        strategy1.setComment("1 location removed");
        strategy2.getPmfmStrategies().remove(0);
        strategy2.getPmfmStrategies().remove(2);
        strategy2.getPmfmStrategies().remove(6);
        strategy2.getPmfmStrategies(0).setSampling(true);
        strategy2.getPmfmStrategies(0).setSurvey(true);
        strategy2.getPmfmStrategies(0).setUnique(true);
        strategy2.getPmfmStrategies(0).setGrouping(true);
        strategy2.getPmfmStrategies(1).setSampling(false);
        strategy2.getPmfmStrategies(1).setSurvey(false);
        strategy2.getPmfmStrategies(1).setUnique(false);
        strategy2.getPmfmStrategies(1).setGrouping(false);
        strategy2.getPmfmStrategies(2).setPmfm(allPmfms.remove(0));
        strategy2.getAppliedStrategies(0).setStartDate(null);
        strategy2.getAppliedStrategies(0).setEndDate(null);
        strategy2.getAppliedStrategies(1).setSamplingDepartment(null);
        strategy2.getAppliedStrategies(2).setStartDate(strategy2.getAppliedStrategies(2).getStartDate().plusDays(2));

        if (log.isDebugEnabled()) {
            log.debug("update program with updated strategies, applied strategies & pmfm strategies");
        }
        // save
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);


        // remove a monLocProg
        program.getLocations().remove(0);

        if (log.isDebugEnabled()) {
            log.debug("update program with removing 1 location on program");
        }

        // save
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);

        assertEquals(reloadedProgram.sizeLocations(), reloadedProgram.getStrategies(0).sizeAppliedStrategies());
        assertEquals(reloadedProgram.sizeLocations(), reloadedProgram.getStrategies(1).sizeAppliedStrategies());

        if (log.isDebugEnabled()) {
            log.debug("createProgramAndStrategies end");
        }

    }

    /**
     * Test ignored because Q3-Déchets can't handle local programs
     */
    @Ignore
    @Test
    public void savePrograms() {
        ProgramDTO programDTO = loadFullProgram(dbResource.getFixtures().getProgramCode());
        Assume.assumeTrue(programDTO.getStrategies().size() > 1);

        StatusDTO status = QuadrigeBeanFactory.newStatusDTO();
        status.setCode(StatusCode.LOCAL_ENABLE.getValue());
        programDTO.setCode(PROGRAM_CODE);
        programDTO.setStatus(status);
        programDTO.setDirty(true);
        service.savePrograms(Lists.newArrayList(programDTO));

        // Keep only the first strategy
        programDTO.setStrategies(Lists.newArrayList(programDTO.getStrategies().iterator().next()));

        programDTO.setDirty(true);
        service.savePrograms(Lists.newArrayList(programDTO));
    }

    private ProgramDTO loadFullProgram(String programCode) {

        ProgramDTO program = service.getWritableProgramByCode(programCode);

        // program has minimum properties
        assertFalse(program.isStrategiesLoaded());
        assertFalse(program.isLocationsLoaded());
        assertTrue(program.isStrategiesEmpty());
        assertTrue(program.isLocationsEmpty());

        // load strategies and applied strategies (=locations)
        service.loadStrategiesAndLocations(program);
        assertTrue(program.isStrategiesLoaded());
        assertTrue(program.isLocationsLoaded());
//        assertFalse(program.isStrategieEmpty());
//        assertFalse(program.isLieuEmpty());

        // load applied periods (=applied strategies) for each strategy
        for (StrategyDTO strategyProgStrat : program.getStrategies()) {
//            assertFalse(strategyProgStrat.isAppliedStrategiesLoaded());
//            assertTrue(strategyProgStrat.isAppliedStrategiesEmpty());
            assertFalse(strategyProgStrat.isPmfmStrategiesLoaded());
            assertTrue(strategyProgStrat.isPmfmStrategiesEmpty());
            service.loadAppliedPeriodsAndPmfmStrategies(program, strategyProgStrat);
            assertTrue(strategyProgStrat.isAppliedStrategiesLoaded());
//            assertFalse(strategyProgStrat.isLieuEmpty());
            assertTrue(strategyProgStrat.isPmfmStrategiesLoaded());
//            assertFalse(strategyProgStrat.isPsfmEmpty());
        }

        return program;
    }

    private void assertProgramEquals(ProgramDTO expectedProgram, ProgramDTO programToTest) {
        assertEquals(expectedProgram.getCode(), programToTest.getCode());
        assertEquals(expectedProgram.getName(), programToTest.getName());
        assertEquals(expectedProgram.getComment(), programToTest.getComment());
        assertEquals(expectedProgram.getStatus().getCode(), programToTest.getStatus().getCode());

        assertStrategiesEquals(expectedProgram.getStrategies(), programToTest.getStrategies());
        assertLocationsEquals(expectedProgram.getLocations(), programToTest.getLocations());
    }

    private void assertStrategiesEquals(Collection<StrategyDTO> expectedStrategies, Collection<StrategyDTO> strategiesToTest) {
        assertNotNull(expectedStrategies);
        assertNotNull(strategiesToTest);
        assertNotSame(expectedStrategies, strategiesToTest);
        assertEquals(expectedStrategies.size(), strategiesToTest.size());
        Map<Integer, StrategyDTO> strategiesToTestMap = DaliBeans.mapById(strategiesToTest);
        for (StrategyDTO expectedStrategy : expectedStrategies) {
            StrategyDTO strategyToTest = strategiesToTestMap.get(expectedStrategy.getId());
            assertNotNull(strategyToTest);
            assertEquals(expectedStrategy.getName(), strategyToTest.getName());
            assertEquals(expectedStrategy.getComment(), strategyToTest.getComment());

            assertAppliedStrategiesEquals(expectedStrategy.getAppliedStrategies(), strategyToTest.getAppliedStrategies());
            assertPmfmStrategiesEquals(expectedStrategy.getPmfmStrategies(), strategyToTest.getPmfmStrategies());
        }
    }
    
    private void assertLocationsEquals(Collection<LocationDTO> expectedLocations, Collection<LocationDTO> locationsToTest) {
        assertNotNull(expectedLocations);
        assertNotNull(locationsToTest);
        assertNotSame(expectedLocations, locationsToTest);
        assertTrue(expectedLocations.size() <= locationsToTest.size());
        Map<Integer, LocationDTO> map = DaliBeans.mapById(locationsToTest);
        for (LocationDTO expectedLocation : expectedLocations) {
            LocationDTO appliedLocationToTest = map.get(expectedLocation.getId());
            assertNotNull(appliedLocationToTest);
            assertEquals(expectedLocation.getLabel(), appliedLocationToTest.getLabel());
            assertEquals(expectedLocation.getName(), appliedLocationToTest.getName());
        }
    }

    private void assertAppliedStrategiesEquals(Collection<AppliedStrategyDTO> expectedAppliedStrategies, Collection<AppliedStrategyDTO> appliedStrategiesToTest) {
        assertNotNull(expectedAppliedStrategies);
        assertNotNull(appliedStrategiesToTest);
        assertNotSame(expectedAppliedStrategies, appliedStrategiesToTest);
        assertTrue(expectedAppliedStrategies.size() <= appliedStrategiesToTest.size());
        Map<Integer, AppliedStrategyDTO> map = DaliBeans.mapById(appliedStrategiesToTest);
        for (AppliedStrategyDTO expectedAppliedStrategy : expectedAppliedStrategies) {
            AppliedStrategyDTO appliedStrategyToTest = map.get(expectedAppliedStrategy.getId());
            assertNotNull(appliedStrategyToTest);
            assertEquals(expectedAppliedStrategy.getAppliedStrategyId(), appliedStrategyToTest.getAppliedStrategyId());
            assertEquals(expectedAppliedStrategy.getLabel(), appliedStrategyToTest.getLabel());
            assertEquals(expectedAppliedStrategy.getName(), appliedStrategyToTest.getName());
            assertEquals(expectedAppliedStrategy.getComment(), appliedStrategyToTest.getComment());
            assertEquals(expectedAppliedStrategy.getSamplingDepartment(), appliedStrategyToTest.getSamplingDepartment());
            assertEquals(expectedAppliedStrategy.getAnalysisDepartment(), appliedStrategyToTest.getAnalysisDepartment());
            assertEquals(expectedAppliedStrategy.getStartDate(), appliedStrategyToTest.getStartDate());
            assertEquals(expectedAppliedStrategy.getEndDate(), appliedStrategyToTest.getEndDate());
        }
    }

    private void assertPmfmStrategiesEquals(Collection<PmfmStrategyDTO> expectedPmfmStrategies, Collection<PmfmStrategyDTO> pmfmStrategiesToTest) {
        assertNotNull(expectedPmfmStrategies);
        assertNotNull(pmfmStrategiesToTest);
        assertNotSame(expectedPmfmStrategies, pmfmStrategiesToTest);
        assertEquals(expectedPmfmStrategies.size(), pmfmStrategiesToTest.size());
        Map<Integer, PmfmStrategyDTO> map = DaliBeans.mapById(pmfmStrategiesToTest);
        for (PmfmStrategyDTO expectedPmfmStrategy : expectedPmfmStrategies) {
            PmfmStrategyDTO pmfmStrategyToTest = map.get(expectedPmfmStrategy.getId());
            assertNotNull(pmfmStrategyToTest);
            assertEquals(expectedPmfmStrategy.getPmfm(), pmfmStrategyToTest.getPmfm());
            assertEquals(expectedPmfmStrategy.isSurvey(), pmfmStrategyToTest.isSurvey());
            assertEquals(expectedPmfmStrategy.isSampling(), pmfmStrategyToTest.isSampling());
            assertEquals(expectedPmfmStrategy.isGrouping(), pmfmStrategyToTest.isGrouping());
            assertEquals(expectedPmfmStrategy.isUnique(), pmfmStrategyToTest.isUnique());
        }
    }
}
