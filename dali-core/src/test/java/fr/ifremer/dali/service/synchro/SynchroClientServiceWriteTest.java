package fr.ifremer.dali.service.synchro;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.DaliDatabaseFixtures;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.dto.system.synchronization.SynchroChangesDTO;
import fr.ifremer.dali.service.DaliServiceLocator;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.synchro.vo.SynchroImportContextVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;

import java.io.File;

/**
 *
 * @author Ludovic
 */
public class SynchroClientServiceWriteTest {

    private static final Log log = LogFactory.getLog(SynchroClientServiceWriteTest.class);
    
    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.writeDb();

    private SynchroClientService service;
    
    private DaliConfiguration config;
    
    @Before
    public void setUp() throws Exception {
        config = DaliConfiguration.getInstance();
        service = DaliServiceLocator.instance().getSynchroClientService();
    }
    
    @Test
    public void getImportFileChangesAsDTO() {
        DaliDatabaseFixtures fixtures = dbResource.getFixtures();
        int userId = fixtures.getUserIdWithDataForSynchro();

        File importedFile = getImportTestFile();

        SynchroImportContextVO context = new SynchroImportContextVO();
        context.setWithReferential(true);
        context.setWithData(true);

        SynchroChangesDTO changes = service.getImportFileChangesAsDTO(userId,
                importedFile,
                context,
                newApplicationProgressionModel(),
                100);

        Assert.assertNotNull(changes);
    }

	/* -- Internal methods -- */

    protected File getImportTestFile() {
        File importFile = new File("src/test/misc/importFromFile.zip");
        Assume.assumeTrue("Could not found test file: " + importFile.getPath(), importFile.exists());

        return importFile;
    }

    protected ProgressionCoreModel newApplicationProgressionModel() {
        ProgressionCoreModel progressionModel = new ProgressionCoreModel();
        // Init progression model at 0/100
        progressionModel.setTotal(100);
        progressionModel.setCurrent(0);

        return progressionModel;
    }
}
