package fr.ifremer.dali.service.control;

import fr.ifremer.dali.config.DaliConfiguration;
import fr.ifremer.dali.dao.DaliDatabaseResource;
import fr.ifremer.dali.service.DaliServiceLocator;
import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

/**
 * @author peck7 on 02/01/2019.
 */
public class RuleListServiceTest {

    @ClassRule
    public static final DaliDatabaseResource dbResource = DaliDatabaseResource.readDb();

    private RuleListService service;

    private DaliConfiguration config;

    static final String SMALL_STRING = "SMALL_STRING";
    static final String LONG_STRING = "LONG_STRING_123456789012346579801234567890";

    @Before
    public void setUp() {
        service = DaliServiceLocator.instance().getRuleListService();
        config = DaliConfiguration.getInstance();
    }

    @Test
    public void getNextRuleCode() {

        String smallString = SMALL_STRING;
        String longString = LONG_STRING;

        MutableInt index = service.getUniqueMutableIndex();
        String newCode = service.getNextRuleCode(smallString, index);
        Assert.assertEquals(SMALL_STRING, smallString);
        Assert.assertNotNull(newCode);
        Assert.assertTrue(newCode.length() < 40);
        Assert.assertEquals(String.format("%S_%s", smallString, index.getValue() - 1), newCode);

        newCode = service.getNextRuleCode(longString, index);
        Assert.assertEquals(LONG_STRING, longString);
        Assert.assertNotNull(newCode);
        Assert.assertEquals(40, newCode.length());
        Assert.assertEquals(String.format("%s_%s", longString.substring(0, 29), index.getValue() - 1), newCode);
    }

}
