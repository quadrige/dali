package fr.ifremer.dali.util;

/*
 * #%L
 * Dali :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class TimeUtilsTest {

    private static final Log log = LogFactory.getLog(TimeUtilsTest.class);

    private static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy HH:mm";
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN);

    private static final String DATE1 = "16/09/2015 12:00";
    private static final String DATE2 = "25/12/2015 12:00";

    @Test
    public void testTimeZone() {

        try {
            Date javaDate1 = DateUtils.parseDate(DATE1, Locale.FRANCE, DATE_FORMAT_PATTERN);
            log.info(javaDate1);

            LocalDateTime dateTime1 = LocalDateTime.parse(DATE1, DATE_FORMAT);
            log.info(dateTime1);
            LocalDateTime dateTime2 = LocalDateTime.parse(DATE2, DATE_FORMAT);
            log.info(dateTime2);

            // with +2
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN).withZone(ZoneOffset.ofHours(1));
            ZonedDateTime dateTime1a = ZonedDateTime.parse(DATE1, dateTimeFormatter);
            log.info(dateTime1a);
            ZonedDateTime dateTime2a = ZonedDateTime.parse(DATE2, dateTimeFormatter);
            log.info(dateTime2a);

            // with -4
            dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN).withZone(ZoneOffset.ofHours(-4));
            ZonedDateTime dateTime1b = ZonedDateTime.parse(DATE1, dateTimeFormatter);
            log.info(dateTime1b);
            ZonedDateTime dateTime2b = ZonedDateTime.parse(DATE2, dateTimeFormatter);
            log.info(dateTime2b);


        } catch (ParseException e) {
            Assert.fail(e.getLocalizedMessage());
        }

    }
}
