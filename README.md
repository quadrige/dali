Recompiler le projet
--------------------
Pour reconstuire le projet dali :
```
mvn clean install
```
 
ATTENTION: La toute première fois, un message vous indiquera de relancer 
que les librairies magicdraw ont été installées dans le repository maven local.
Vous devrez donc relancer la compilation une seconde fois.
   
Faire une nouvelle version mineure
----------------------------------

Cette release va déployer sur le dépot nexus les artifacts.
Elle va également générer et deployer les fichiers utiles pour la  mise à jour automatique, et les déployer sur le site distant.

IMPORTANT: Pour déployer à l'Ifremer (la configuration par défaut) il faut donc être connecté via domicile.ifremer.fr et avoir lancer la redirection "Java Secure Application Manager".

```
mvn release:prepare -Darguments="-DperformRelease -DskipTests"
```
```
mvn release:perform -Darguments="-DperformRelease -DperformFullRelease -DperformFullRelease64 -DskipTests -Dmaven.deploy.skip=false -Dsource.skip=false" -B
```
 
Note la javadoc n'est pas générée par défaut, ajouter -Dmaven.javadoc.skip=false aux arguments 

 
Déploiement sur le repository EIS
---------------------------------

```
mvn release:prepare -Darguments="-Dmaven.test.skip"
```
```
mvn release:perform -Darguments="-DperformRelease -DperformFullRelease -DperformFullRelease64 -Dmaven.test.skip -Dmaven.deploy.skip=false -Dsource.skip=false -Peis-deploy" -B
```

sans le déploiement des updates 
```
mvn release:perform -Darguments="-DperformRelease -DperformFullRelease -DperformFullRelease64 -Dmaven.test.skip -Dmaven.deploy.skip=false -Dsource.skip=false -Peis-deploy,-deploy-dali-application-update,-deploy-dali-config-update,-deploy-dali-data-update,-deploy-dali-db-update" -B
```
 
Génération des assembly
-----------------------

From checkout dir:
```
mvn install -DperformRelease -DperformFullRelease -DperformFullRelease64 -Dmaven.test.skip
```

Génération du site web
---------------------- 

```
mvn site-deploy -DperformRelease -Peis-deploy
```

Mise à jour des i18n depuis le fichier csv
------------------------------------------

Pour fusionner les mises à jour de traductions dans les resources:
```
mvn i18n:merge-back-csv-bundle -pl (dali-core|dali-ui-swing) -Di18n.bundleCsvFile=<path>/dali-i18n.csv
```